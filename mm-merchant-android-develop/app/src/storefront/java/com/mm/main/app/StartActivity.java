package com.mm.main.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.mm.main.app.activity.merchant.user.UserActivity;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.global.MmGlobal;
import com.mm.storefront.app.wxapi.WXEntryActivity;
import com.umeng.analytics.MobclickAgent;


public class StartActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        getSupportActionBar().hide();
//        if (MmGlobal.getRemember()) {
//            viewUser();
//        } else {
//            startActivity(new Intent(this, SplashActivity.class));
//            finish();
//        }

//        startActivity(new Intent(this,ProductListActivity.class));
        if (MmGlobal.isLogin()) {
            startActivity(new Intent(this, StorefrontMainActivity.class));
        } else {
            startActivity(new Intent(this, WXEntryActivity.class));
        }

        finish();
    }

    private void viewUser() {
        Intent intent = new Intent(StartActivity.this, UserActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
