package com.mm.main.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import android.util.Log;

import com.alexbbb.uploadservice.MultipartUploadRequest;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.response.UploadImageResponse;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static junit.framework.Assert.assertTrue;

/**
 * Login Test Android
 */
@RunWith(AndroidJUnit4.class)
public class ImageServiceTest {
    private CountDownLatch lock = new CountDownLatch(1);
    private String tokenString;
    private MyApplication myApplication;
    private boolean imageUploadValid = false;

    public static final String TEST_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOjM2NiwiVXNlclR5cGVJZCI6MSwiTWVyY2hhbnRJZCI6MCwiRmlyc3ROYW1lIjoiVmluY2VudCIsIkxhc3ROYW1lIjoiZnRlc3RlcjUiLCJNaWRkbGVOYW1lIjoiQW5kcmV3IiwiRGlzcGxheU5hbWUiOiJBbmRyZXcgQ2hhbiIsIkVtYWlsIjoiYWRtaW5AZW1haWwuY29tIiwiTW9iaWxlQ29kZSI6Iis4NTIiLCJNb2JpbGVOdW1iZXIiOiI5MDQzMzY3MyIsIlByb2ZpbGVJbWFnZSI6ImMzNTg3ZjlhOWY4ZjBiYWM0MzBjY2MwYjRjOWQxNzNlIiwiU3RhdHVzSWQiOjIsIkxhc3RMb2dpbiI6IjIwMTUtMTAtMjlUMDg6MjQ6MzUuMDAwWiIsIkxhc3RNb2RpZmllZCI6IjIwMTUtMTAtMjlUMDg6MjQ6MzUuMDAwWiIsIkN1bHR1cmVDb2RlIjoiRU4iLCJVc2VyU2VjdXJpdHlHcm91cEFycmF5IjpbMywyXSwiaWF0IjoxNDQ2MTA3MDc1LCJleHAiOjE0NDY3MTE4NzV9._7Vh64qvTE1qFw5yCAX1MKbnQVk7AKz3HyXuKKdeI4E";

    @Test
    public void uploadImage() throws InterruptedException {
        Bitmap image = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
        image.eraseColor(android.graphics.Color.GREEN);
//        Bitmap image = BitmapFactory.decodeResource(MyApplication.getContext().getResources(), R.drawable.test);
        String name = System.currentTimeMillis()+"";
        String fileName = name+".jpg";
        File file = new File(MyApplication.getContext().getCacheDir(), fileName);
        ByteArrayOutputStream stream = null;
        try {
            OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            image.compress(Bitmap.CompressFormat.JPEG, 10, os);
            os.close();

//            stream = new ByteArrayOutputStream();
//            image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        }catch (Exception e) {
            Logger.d("upload image error: ", e.getMessage());
        }
        MediaType mediaType = MediaType.parse("image/jpeg");
//        RequestBody fbody = RequestBody.create(MediaType.parse("image/jpeg"), file);

        Map<String, String> map  = new HashMap<>();
        map.put("Content-Disposition", "form-data; name=\"file\"; fileName=\""+fileName+"\"");
//        map.put("Content-Type", "image/jpeg");
        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
//                .addFormDataPart("f®ile", fileName)
                .addPart(
//                        Headers.of("Content-Disposition", "form-data; name=\"test\"; fileName=\"test_copy.jpg\"", "Content-Type", "image/jpeg"),
                        Headers.of(map),
                        RequestBody.create(mediaType, file))
                .build();

//        Call<UploadImageResponse> call = service.uploadImage(requestBody, "file", fileName, "image/jpeg");
        Call<UploadImageResponse> call = APIManager.getInstance().getImageService().uploadImage(requestBody);
        call.enqueue(new Callback<UploadImageResponse>() {
            @Override
            public void onResponse(Response<UploadImageResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    imageUploadValid = !TextUtils.isEmpty(response.body().getImageKey());
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertTrue(imageUploadValid);

    }

    @Test
    public void uploadMultipart() {
        Bitmap image = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
        image.eraseColor(android.graphics.Color.GREEN);
        String name = System.currentTimeMillis()+"";
        String fileName = name+".jpg";
        File file = new File(MyApplication.getContext().getCacheDir(), fileName);
        ByteArrayOutputStream stream = null;
        try {
            OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            image.compress(Bitmap.CompressFormat.JPEG, 10, os);
            os.close();

//            stream = new ByteArrayOutputStream();
//            image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        }catch (Exception e) {
            Logger.d("upload image error: ", e.getMessage());
        }
        MediaType mediaType = MediaType.parse("image/jpeg");

        Context context = MyApplication.getContext();
        final MultipartUploadRequest request =
                new MultipartUploadRequest(context,
                        "file",
                        Constant.getApiURL() +"/image/upload");

    /*
     * parameter-name: is the name of the parameter that will contain file's data.
     * Pass "uploaded_file" if you're using the test PHP script
     *
     * custom-file-name.extension: is the file name seen by the server.
     * E.g. value of $_FILES["uploaded_file"]["name"] of the test PHP script
     */
        request.addFileToUpload(file.getPath(),
                "file",
                fileName,
                "image/jpeg" );

        //You can add your own custom headers
        request.addHeader("Authorization", TEST_TOKEN);

        //and parameters
//        request.addParameter("parameter-name", "parameter-value");

        //If you want to add a parameter with multiple values, you can do the following:
//        request.addParameter("array-parameter-name", "value1");
//        request.addParameter("array-parameter-name", "value2");
//        request.addParameter("array-parameter-name", "valueN");

        //or
//        String[] values = new String[] {"value1", "value2", "valueN"};
//        request.addArrayParameter("array-parameter-name", values);

        //or
//        List<String> valuesList = new ArrayList<String>();
//        valuesList.add("value1");
//        valuesList.add("value2");
//        valuesList.add("valueN");
//        request.addArrayParameter("array-parameter-name", valuesList);

        //configure the notification
        request.setNotificationConfig(android.R.drawable.ic_menu_upload,
                "notification title",
                "upload in progress text",
                "upload completed successfully text",
                "upload error text",
                false);

        // set a custom user agent string for the upload request
        // if you comment the following line, the system default user-agent will be used
        request.setCustomUserAgent("UploadServiceDemo/1.0");

        // set the intent to perform when the user taps on the upload notification.
        // currently tested only with intents that launches an activity
        // if you comment this line, no action will be performed when the user taps
        // on the notification
        request.setNotificationClickIntent(new Intent(context, StartActivity.class));

        // set the maximum number of automatic upload retries on error
        request.setMaxRetries(2);

        try {
            //Start upload service and display the notification
            request.startUpload();

        } catch (Exception exc) {
            //You will end up here only if you pass an incomplete upload request
            Log.e("AndroidUploadService", exc.getLocalizedMessage(), exc);
            assertTrue(imageUploadValid);
        }
    }


}