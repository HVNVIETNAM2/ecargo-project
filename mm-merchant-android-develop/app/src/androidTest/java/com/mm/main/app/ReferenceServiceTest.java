package com.mm.main.app;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.schema.LanguageList;
import com.mm.main.app.service.ReferenceService;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

import static junit.framework.Assert.assertTrue;

/**
 * Login Test Android
 */
@RunWith(AndroidJUnit4.class)
public class ReferenceServiceTest {
    private CountDownLatch lock = new CountDownLatch(1);
    private String tokenString;
    private MyApplication myApplication;
    private boolean listLanguageValid = false;

    public static final String TEST_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOjM3MCwiVXNlclR5cGVJZCI6MiwiTWVyY2hhbnRJZCI6MSwiSW52ZW50b3J5TG9jYXRpb25JZCI6MiwiRmlyc3ROYW1lIjoiTWVyY2giLCJMYXN0TmFtZSI6IkFudCIsIk1pZGRsZU5hbWUiOiIiLCJEaXNwbGF5TmFtZSI6IkRvZXIiLCJFbWFpbCI6Im1lcmNoQGVjYXJnby5jb20iLCJNb2JpbGVDb2RlIjoiKzg1MiIsIk1vYmlsZU51bWJlciI6IjkxOTE5MTk5IiwiUHJvZmlsZUltYWdlIjoiNjU5MGVmZWM5ZDdhZWIwMDg1ZjE5ZmIzYzBiNjU5ZjgiLCJTdGF0dXNJZCI6MiwiTGFzdExvZ2luIjoiMjAxNS0xMS0wMlQwMTozMzoyNS4wMDBaIiwiTGFzdE1vZGlmaWVkIjoiMjAxNS0xMS0wMlQwOTozMzoyNS4wMDBaIiwiQ3VsdHVyZUNvZGUiOiJFTiIsIlVzZXJTZWN1cml0eUdyb3VwQXJyYXkiOls2XSwiaWF0IjoxNDQ2NDU2ODA1LCJleHAiOjE0NDcwNjE2MDV9.OjTZoI3o6EbU15fA11JQRttzDtE089r4AnAuHJ9DbH8";

    @Test
    public void listLanguage() throws InterruptedException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ReferenceService service = retrofit.create(ReferenceService.class);
        Call<LanguageList> call = service.listLanguage();
        call.enqueue(new Callback<LanguageList>() {
            @Override
            public void onResponse(Response<LanguageList> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    listLanguageValid = response.body().getLanguageList().size() > 0;
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertTrue(listLanguageValid);
    }

}