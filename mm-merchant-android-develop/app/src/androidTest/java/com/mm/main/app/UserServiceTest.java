package com.mm.main.app;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import android.util.Log;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.factory.RetrofitFactory;
import com.mm.main.app.schema.response.SaveUserResponse;
import com.mm.main.app.schema.Token;
import com.mm.main.app.schema.User;
import com.mm.main.app.schema.request.UserChangeLanguageRequest;
import com.mm.main.app.schema.request.UserResendLinkRequest;
import com.mm.main.app.service.UserService;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static junit.framework.Assert.assertTrue;

/**
 * Login Test Android
 */
@RunWith(AndroidJUnit4.class)
public class UserServiceTest {
    private CountDownLatch lock = new CountDownLatch(1);
    private String tokenString;
    private MyApplication myApplication;
    private boolean viewUserValid = false;
    private boolean referenceUserValid = false;
    private boolean saveUserValid = false;
    private boolean changeUserStatusValid = false;
    private boolean changeUserPasswordValid = false;
    private boolean changeUserEmailValid = false;
    private boolean resendLinkValid = false;
    private boolean changeLanguageValid = false;
    private int numberOfUser = 0;

    public static final String TEST_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOjM2NiwiVXNlclR5cGVJZCI6MSwiTWVyY2hhbnRJZCI6MCwiRmlyc3ROYW1lIjoiVmluY2VudCIsIkxhc3ROYW1lIjoiZnRlc3RlcjUiLCJNaWRkbGVOYW1lIjoiQW5kcmV3IiwiRGlzcGxheU5hbWUiOiJBbmRyZXcgQ2hhbiIsIkVtYWlsIjoiYWRtaW5AZW1haWwuY29tIiwiTW9iaWxlQ29kZSI6Iis4NTIiLCJNb2JpbGVOdW1iZXIiOiI5MDQzMzY3MyIsIlByb2ZpbGVJbWFnZSI6ImMzNTg3ZjlhOWY4ZjBiYWM0MzBjY2MwYjRjOWQxNzNlIiwiU3RhdHVzSWQiOjIsIkxhc3RMb2dpbiI6IjIwMTUtMTAtMjhUMDE6MTc6NTQuMDAwWiIsIkxhc3RNb2RpZmllZCI6IjIwMTUtMTAtMjhUMDE6MTc6NTQuMDAwWiIsIkN1bHR1cmVDb2RlIjoiRU4iLCJVc2VyU2VjdXJpdHlHcm91cEFycmF5IjpbMywyXSwiaWF0IjoxNDQ1OTk1MDc0LCJleHAiOjE0NDY1OTk4NzR9.yfXLUG8NvKaDjEqhdD097fHX6F6I17Jre56Auyjikig";

    @Test
    public void listUser() throws InterruptedException {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
        settings.edit().putString(Constant.TOKEN, TEST_TOKEN).commit();

        Retrofit retrofit = RetrofitFactory.create();
        UserService service = retrofit.create(UserService.class);
        Call<List<User>> call = service.list();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Response<List<User>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    numberOfUser = response.body().size();
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertTrue(numberOfUser > 0);

    }

    @Test
    public void viewUser() throws InterruptedException {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
        settings.edit().putString(Constant.TOKEN, TEST_TOKEN).commit();

        Retrofit retrofit = RetrofitFactory.create();
        UserService service = retrofit.create(UserService.class);
        Call<User> call = service.viewUser(366);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Response<User> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    viewUserValid = response.body().getUserId().equals("366");
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertTrue(viewUserValid);

    }

    //removed on server side
//    @Test
//    public void referenceUser() throws InterruptedException {
//        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
//        settings.edit().putString(Constant.TOKEN, TEST_TOKEN).commit();
//
//        Retrofit retrofit = RetrofitFactory.create();
//        UserService service = retrofit.create(UserService.class);
//        Call<User> call = service.userReference("366");
//        call.enqueue(new Callback<User>() {
//            @Override
//            public void onResponse(Response<User> response, Retrofit retrofit) {
//                if (response.isSuccess()) {
//                    referenceUserValid = response.body()!=null;
//                    lock.countDown();
//                } else {
//                    Log.i("Unit Test", response.toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                t.printStackTrace();
//            }
//        });
//        lock.await(20, TimeUnit.SECONDS);
//        assertTrue(referenceUserValid);
//
//    }

    @Test
    public void saveUser() throws InterruptedException {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
        settings.edit().putString(Constant.TOKEN, TEST_TOKEN).commit();

        Retrofit retrofit = RetrofitFactory.create();
        UserService service = retrofit.create(UserService.class);
        User user = new User();
        String randomId = new Random(System.currentTimeMillis()).nextInt(1000)+"";
        final String email = "tester+"+randomId+"@wb.com";
        user.setEmail(email);
        user.setFirstName("tester" + randomId);
        user.setLanguageId(1);
        user.setLastName("haha");
        user.setMobileCode("+852");
        user.setMobileNumber("9876" + randomId);
        user.setTimeZoneId("1");
        user.setUserSecurityGroupArray(new Integer[]{3, 2});
        user.setUserTypeId(1);

        Call<SaveUserResponse> call = service.save(user);
        call.enqueue(new Callback<SaveUserResponse>() {
            @Override
            public void onResponse(Response<SaveUserResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    saveUserValid = email.equals(response.body().getUser().getEmail());
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(25, TimeUnit.SECONDS);
        assertTrue(saveUserValid);

    }

    @Test
    public void updateUser() throws InterruptedException {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
        settings.edit().putString(Constant.TOKEN, TEST_TOKEN).commit();

        Retrofit retrofit = RetrofitFactory.create();
        UserService service = retrofit.create(UserService.class);
        User user = new User();
        String randomId = new Random(System.currentTimeMillis()).nextInt(1000)+"";
//        final String email = "tester+"+randomId+"@wb.com";
//        user.setEmail(email);
        user.setUserId(366);
        user.setFirstName("testUPDATE");
        user.setLanguageId(1);
        user.setLastName("byAndroidbyAndrew");
//        user.setMobileCode("+852");
//        user.setMobileNumber("9876" + randomId);
        user.setTimeZoneId("1");
        user.setUserSecurityGroupArray(new Integer[]{3, 2});
        user.setUserTypeId(1);

        Call<SaveUserResponse> call = service.save(user);
        call.enqueue(new Callback<SaveUserResponse>() {
            @Override
            public void onResponse(Response<SaveUserResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    saveUserValid = "366".equals(response.body().getUser().getUserId());
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(25, TimeUnit.SECONDS);
        assertTrue(saveUserValid);

    }

//    @Test
//    public void userStatusUpdate() throws InterruptedException {
//        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
//        settings.edit().putString(Constant.TOKEN, TEST_TOKEN).commit();
//
//        Retrofit retrofit = RetrofitFactory.create();
//        UserService service = retrofit.create(UserService.class);
//        UserStatus userStatus = new UserStatus("349", "Active");
//
//        Call<User> call = service.changeStatus(userStatus);
//        call.enqueue(new Callback<User>() {
//            @Override
//            public void onResponse(Response<User> response, Retrofit retrofit) {
//                if (response.isSuccess()) {
//                    changeUserStatusValid = "349".equals(response.body().getUserId());
//                    lock.countDown();
//                } else {
//                    Log.i("Unit Test", response.toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                t.printStackTrace();
//            }
//        });
//        lock.await(20, TimeUnit.SECONDS);
//        assertTrue(changeUserStatusValid);
//
//    }

//    @Test
//    public void userChangePassword() throws InterruptedException {
//        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
//        settings.edit().putString(Constant.TOKEN, TEST_TOKEN).commit();
//
//        Retrofit retrofit = RetrofitFactory.create();
//        UserService service = retrofit.create(UserService.class);
//
//        User userDetail = new User();
//        userDetail.setUserId("366");
//
//        Call<User> call = service.changeUserPassword(userDetail);
//        call.enqueue(new Callback<User>() {
//            @Override
//            public void onResponse(Response<User> response, Retrofit retrofit) {
//                if (response.isSuccess()) {
//                    changeUserPasswordValid = "366".equals(response.body().getUserId());
//                    lock.countDown();
//                } else {
//                    Log.i("Unit Test", response.toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                t.printStackTrace();
//            }
//        });
//        lock.await(20, TimeUnit.SECONDS);
//        assertTrue(changeUserPasswordValid);
//
//    }

//    @Test
//    public void userChangeEmail() throws InterruptedException {
//        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
//        settings.edit().putString(Constant.TOKEN, TEST_TOKEN).commit();
//
//        Retrofit retrofit = RetrofitFactory.create();
//        UserService service = retrofit.create(UserService.class);
//
//        User userDetail = new User();
//        userDetail.setUserId("366");
//        userDetail.setUserId("admin@email.com");
//
//        Call<User> call = service.changeUserPassword(userDetail);
//        call.enqueue(new Callback<User>() {
//            @Override
//            public void onResponse(Response<User> response, Retrofit retrofit) {
//                if (response.isSuccess()) {
//                    changeUserEmailValid = "366".equals(response.body().getUserId());
//                    lock.countDown();
//                } else {
//                    Log.i("Unit Test", response.toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                t.printStackTrace();
//            }
//        });
//        lock.await(20, TimeUnit.SECONDS);
//        assertTrue(changeUserEmailValid);
//
//    }

    @Test
    public void userResendLink() throws InterruptedException {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
        settings.edit().putString(Constant.TOKEN, TEST_TOKEN).commit();

        Retrofit retrofit = RetrofitFactory.create();
        UserService service = retrofit.create(UserService.class);

        UserResendLinkRequest userResendLinkRequest = new UserResendLinkRequest();
        userResendLinkRequest.setUserId("358");
        userResendLinkRequest.setLink("Email");

        Call<Boolean> call = service.resendLink(userResendLinkRequest);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Response<Boolean> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    resendLinkValid = response.body().booleanValue();
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertTrue(resendLinkValid);

    }

    @Test
    public void userChangeLanguage() throws InterruptedException {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext());
        settings.edit().putString(Constant.TOKEN, TEST_TOKEN).commit();

        Retrofit retrofit = RetrofitFactory.create();
        UserService service = retrofit.create(UserService.class);

        UserChangeLanguageRequest userChangeLanguageRequest = new UserChangeLanguageRequest();
        userChangeLanguageRequest.setUserId("359");
        userChangeLanguageRequest.setCultureCode("CHS");

        Call<Token> call = service.changeLanguage(userChangeLanguageRequest);
        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Response<Token> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    changeLanguageValid = !TextUtils.isEmpty(response.body().getToken());
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertTrue(changeLanguageValid);

    }
}