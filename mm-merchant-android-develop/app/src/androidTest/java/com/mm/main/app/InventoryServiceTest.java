package com.mm.main.app;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.schema.InventoryLocation;
import com.mm.main.app.schema.request.InventoryStatusChangeRequest;
import com.mm.main.app.service.InventoryService;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

import static junit.framework.Assert.assertNotNull;

/**
 * Login Test Android
 */
@RunWith(AndroidJUnit4.class)
public class InventoryServiceTest {
    private CountDownLatch lock = new CountDownLatch(1);
    private String tokenString;
    private MyApplication myApplication;
    private boolean listLocationValid = false;
    private boolean saveLocationValid = false;
    private boolean viewLocationValid = false;
    private boolean changeLocationStatusValid = false;

    public static final String TEST_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOjM2NiwiVXNlclR5cGVJZCI6MSwiTWVyY2hhbnRJZCI6MCwiRmlyc3ROYW1lIjoiQWRtaW4iLCJMYXN0TmFtZSI6IkFkbWluIiwiTWlkZGxlTmFtZSI6Ik1pZCIsIkRpc3BsYXlOYW1lIjoiQWRtaW4iLCJFbWFpbCI6ImFkbWluQGVtYWlsLmNvbSIsIk1vYmlsZUNvZGUiOiIrODUyIiwiTW9iaWxlTnVtYmVyIjoiOTk4NDUzMzIiLCJQcm9maWxlSW1hZ2UiOiI0MTdhZDZhNDRjNGQzYjUzMmFkNDM1NzZkN2FkMWQ4ZCIsIlN0YXR1c0lkIjoyLCJMYXN0TG9naW4iOiIyMDE1LTEwLTI2VDAzOjM5OjAyLjAwMFoiLCJMYXN0TW9kaWZpZWQiOiIyMDE1LTEwLTI2VDAzOjM5OjAyLjAwMFoiLCJDdWx0dXJlQ29kZSI6IkNIUyIsIlVzZXJTZWN1cml0eUdyb3VwQXJyYXkiOlsyLDQsMywxXSwiaWF0IjoxNDQ1ODMwNzQyLCJleHAiOjE0NDY0MzU1NDJ9.4Tmr8NeSU7cKDS4ChblXrtAIDU4HWHCbJ1B7olcb3T4";

    @Test
    public void listLoaction() throws InterruptedException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        InventoryService service = retrofit.create(InventoryService.class);
        Call<List<InventoryLocation>> call = service.listLocation(1);
        call.enqueue(new Callback<List<InventoryLocation>>() {
            @Override
            public void onResponse(Response<List<InventoryLocation>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    listLocationValid = response.body().size() > 0;
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertNotNull(listLocationValid);

    }

    @Test
    public void saveLoaction() throws InterruptedException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        InventoryService service = retrofit.create(InventoryService.class);
        InventoryLocation inventoryLocation = new InventoryLocation();
        inventoryLocation.setInventoryLocationId(2);
        inventoryLocation.setMerchantId(1);
        inventoryLocation.setInventoryLocationId(1);
        inventoryLocation.setLocationExternalCode("34567");
        inventoryLocation.setLocationName("UPdate me");
        inventoryLocation.setGeoCountryId("48");
        inventoryLocation.setGeoIdProvince("2035607");
        inventoryLocation.setGeoIdCity("1806529");
        inventoryLocation.setStatusId("4");
        inventoryLocation.setInventoryLocationTypeName("店");
        inventoryLocation.setStatusNameInvariant("Inactive");
        inventoryLocation.setGeoCountryName("中国");
        inventoryLocation.setGeoProvinceName("内蒙古自治区");
        inventoryLocation.setGeoCityName("鄂尔多斯市");


        Call<InventoryLocation> call = service.saveLocation(inventoryLocation);
        call.enqueue(new Callback<InventoryLocation>() {
            @Override
            public void onResponse(Response<InventoryLocation> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    saveLocationValid = response.body().getInventoryLocationId().equals("2");
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertNotNull(saveLocationValid);

    }

    @Test
    public void viewLoaction() throws InterruptedException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        InventoryService service = retrofit.create(InventoryService.class);

        Call<InventoryLocation> call = service.viewLocation(2);
        call.enqueue(new Callback<InventoryLocation>() {
            @Override
            public void onResponse(Response<InventoryLocation> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    viewLocationValid = response.body().getInventoryLocationId().equals("2");
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertNotNull(viewLocationValid);

    }

    @Test
    public void changeLoactionStatus() throws InterruptedException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        InventoryService service = retrofit.create(InventoryService.class);
        InventoryStatusChangeRequest inventoryStatusChangeRequest = new InventoryStatusChangeRequest();
        inventoryStatusChangeRequest.setInventoryLocationId("2");
        inventoryStatusChangeRequest.setStatus("Active");


        Call<InventoryLocation> call = service.changeLocationStatus(inventoryStatusChangeRequest);
        call.enqueue(new Callback<InventoryLocation>() {
            @Override
            public void onResponse(Response<InventoryLocation> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    changeLocationStatusValid = response.body().getInventoryLocationId().equals("2");
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertNotNull(changeLocationStatusValid);

    }
}