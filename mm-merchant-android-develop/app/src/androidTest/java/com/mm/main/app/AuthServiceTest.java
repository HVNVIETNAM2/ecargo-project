package com.mm.main.app;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.schema.response.LoginResponse;
import com.mm.main.app.service.AuthService;

import org.junit.Test;
import org.junit.runner.RunWith;
import retrofit.*;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Login Test Android
 */
@RunWith(AndroidJUnit4.class)
public class AuthServiceTest {
    private CountDownLatch lock = new CountDownLatch(1);
    private String tokenString;
    private MyApplication myApplication;
    private int numberOfUser = 0;
    private boolean forgotPasswordValid = false;

    public static final String TEST_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOjM2NiwiVXNlclR5cGVJZCI6MSwiTWVyY2hhbnRJZCI6MCwiRmlyc3ROYW1lIjoiQWRtaW4iLCJMYXN0TmFtZSI6IkFkbWluIiwiTWlkZGxlTmFtZSI6Ik1pZCIsIkRpc3BsYXlOYW1lIjoiQWRtaW4iLCJFbWFpbCI6ImFkbWluQGVtYWlsLmNvbSIsIk1vYmlsZUNvZGUiOiIrODUyIiwiTW9iaWxlTnVtYmVyIjoiOTk4NDUzMzIiLCJQcm9maWxlSW1hZ2UiOiI0MTdhZDZhNDRjNGQzYjUzMmFkNDM1NzZkN2FkMWQ4ZCIsIlN0YXR1c0lkIjoyLCJMYXN0TG9naW4iOiIyMDE1LTEwLTI2VDAzOjM5OjAyLjAwMFoiLCJMYXN0TW9kaWZpZWQiOiIyMDE1LTEwLTI2VDAzOjM5OjAyLjAwMFoiLCJDdWx0dXJlQ29kZSI6IkNIUyIsIlVzZXJTZWN1cml0eUdyb3VwQXJyYXkiOlsyLDQsMywxXSwiaWF0IjoxNDQ1ODMwNzQyLCJleHAiOjE0NDY0MzU1NDJ9.4Tmr8NeSU7cKDS4ChblXrtAIDU4HWHCbJ1B7olcb3T4";

    @Test
    public void verifyLogin() throws InterruptedException {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AuthService service = retrofit.create(AuthService.class);
        Call<LoginResponse> call = service.login("admin@email.com", "pass");
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    tokenString = response.body().getToken();
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertNotNull(tokenString);

    }

//    @Test
//    public void forgotPassword() throws InterruptedException {
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constant.API_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        AuthService service = retrofit.create(AuthService.class);
//        Call<Boolean> call = service.forgotPassword(new ForgotPasswordRequest("admin@email.com"));
//        call.enqueue(new Callback<Boolean>() {
//            @Override
//            public void onResponse(Response<Boolean> response, Retrofit retrofit) {
//                if (response.isSuccess()) {
//                    forgotPasswordValid = response.body().booleanValue();
//                    lock.countDown();
//                } else {
//                    Log.i("Unit Test", response.toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                t.printStackTrace();
//            }
//        });
//        lock.await(20, TimeUnit.SECONDS);
//        assertTrue(forgotPasswordValid);
//
//    }

}