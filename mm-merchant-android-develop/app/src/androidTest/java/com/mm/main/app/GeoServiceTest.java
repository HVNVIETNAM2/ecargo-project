package com.mm.main.app;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.schema.Country;
import com.mm.main.app.schema.Geo;
import com.mm.main.app.service.GeoService;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

import static junit.framework.Assert.assertNotNull;

/**
 * Login Test Android
 */
@RunWith(AndroidJUnit4.class)
public class GeoServiceTest {
    private CountDownLatch lock = new CountDownLatch(1);
    private String tokenString;
    private MyApplication myApplication;
    private boolean listCountryValid = false;
    private boolean listProvinceValid = false;
    private boolean listCityValid = false;

    public static final String TEST_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOjM2NiwiVXNlclR5cGVJZCI6MSwiTWVyY2hhbnRJZCI6MCwiRmlyc3ROYW1lIjoiQWRtaW4iLCJMYXN0TmFtZSI6IkFkbWluIiwiTWlkZGxlTmFtZSI6Ik1pZCIsIkRpc3BsYXlOYW1lIjoiQWRtaW4iLCJFbWFpbCI6ImFkbWluQGVtYWlsLmNvbSIsIk1vYmlsZUNvZGUiOiIrODUyIiwiTW9iaWxlTnVtYmVyIjoiOTk4NDUzMzIiLCJQcm9maWxlSW1hZ2UiOiI0MTdhZDZhNDRjNGQzYjUzMmFkNDM1NzZkN2FkMWQ4ZCIsIlN0YXR1c0lkIjoyLCJMYXN0TG9naW4iOiIyMDE1LTEwLTI2VDAzOjM5OjAyLjAwMFoiLCJMYXN0TW9kaWZpZWQiOiIyMDE1LTEwLTI2VDAzOjM5OjAyLjAwMFoiLCJDdWx0dXJlQ29kZSI6IkNIUyIsIlVzZXJTZWN1cml0eUdyb3VwQXJyYXkiOlsyLDQsMywxXSwiaWF0IjoxNDQ1ODMwNzQyLCJleHAiOjE0NDY0MzU1NDJ9.4Tmr8NeSU7cKDS4ChblXrtAIDU4HWHCbJ1B7olcb3T4";

    @Test
    public void listCountry() throws InterruptedException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GeoService service = retrofit.create(GeoService.class);
        Call<List<Country>> call = service.listCountry();
        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Response<List<Country>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    listCountryValid = response.body().size() > 0;
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertNotNull(listCountryValid);

    }

    @Test
    public void listProvince() throws InterruptedException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GeoService service = retrofit.create(GeoService.class);
        Call<List<Geo>> call = service.listProvince("CHS", "48");
        call.enqueue(new Callback<List<Geo>>() {
            @Override
            public void onResponse(Response<List<Geo>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    listProvinceValid = response.body().size() > 0;
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertNotNull(listProvinceValid);

    }

    @Test
    public void listCity() throws InterruptedException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GeoService service = retrofit.create(GeoService.class);
        Call<List<Geo>> call = service.listCity("CHS", "1796231");
        call.enqueue(new Callback<List<Geo>>() {
            @Override
            public void onResponse(Response<List<Geo>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    listCityValid = response.body().size() > 0;
                    lock.countDown();
                } else {
                    Log.i("Unit Test", response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertNotNull(listCityValid);

    }

}