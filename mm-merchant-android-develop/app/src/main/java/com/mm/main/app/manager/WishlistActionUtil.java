package com.mm.main.app.manager;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.product.ProductDetailPageActivity;
import com.mm.main.app.activity.storefront.product.ProductListActivity;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.schema.CartItem;
import com.mm.main.app.schema.request.CartItemRemoveRequest;
import com.mm.main.app.schema.Style;
import com.mm.main.app.schema.WishList;
import com.mm.main.app.schema.request.WishListItemAddRequest;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;

import java.util.Timer;
import java.util.TimerTask;

import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by thienchaud on 29-Jan-16.
 */
public class WishlistActionUtil {

    public static void actionWishlist(final View heartIcon, Style style, String colorKey, final Context context, final WishListActionListener listener) {
        String wishListKey = MmGlobal.anonymousWishListKey();
        CartItem cartItem = CacheManager.getInstance().wishlistItemForStyle(style);

        if (cartItem != null) {
            // action remove
            heartIcon.setSelected(false);
            APIManager.getInstance().getWishListService().removeWishListItem(new CartItemRemoveRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKeyForCartRequest(), MmGlobal.getWishlistKeyForRequest(), cartItem.getCartItemId()))
                    .enqueue(new MmCallBack<WishList>(context) {
                        @Override
                        public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            super.onFailure(t);
                            MmStatusAlertUtil.show(context, MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR,
                                    context.getString(R.string.LB_CA_CART_ITEM_FAILED), null);
                            heartIcon.setSelected(true);
                            notifyComplete(false, listener);
                        }

                        @Override
                        public void onResponse(Response<WishList> response, Retrofit retrofit) {
                            WishList wishlist = response.body();
                            if (wishlist != null) {
                                CacheManager.getInstance().setWishlist(wishlist);
                                ((Activity) context).invalidateOptionsMenu();
                                notifyComplete(true, listener);
                            } else {
                                MmStatusAlertUtil.show(context, MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR,
                                        context.getString(R.string.LB_CA_CART_ITEM_FAILED), null);
                                heartIcon.setSelected(true);
                                notifyComplete(false, listener);
                            }
                        }
                    });
        } else {
            // action add to wish list
            heartIcon.setSelected(true);
            final Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                public void run() {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            animateHeartIcon(context, heartIcon);
                        }
                    });
                }

            }, 150);

            APIManager.getInstance().getWishListService().addWishListItem(new WishListItemAddRequest(LanguageManager.getInstance().getCurrentCultureCode(), style.getStyleCode(), MmGlobal.getUserKeyForCartRequest(), getWishlistKey(), 0, colorKey, style.getMerchantId()))
                    .enqueue(new MmCallBack<WishList>(context) {
                        @Override
                        public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            super.onFailure(t);
                            MmStatusAlertUtil.show(context, MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR,
                                    context.getString(R.string.LB_CA_MOVE2WISHLIST_FAILED), null);
                            heartIcon.setSelected(false);
                            notifyComplete(false, listener);
                        }

                        @Override
                        public void onResponse(Response<WishList> response, Retrofit retrofit) {
                            WishList wishlist = response.body();
                            if (wishlist != null) {
                                MmGlobal.setAnonymousWishListKey(wishlist.getCartKey());
                                CacheManager.getInstance().setWishlist(wishlist);
//                                ((Activity) context).invalidateOptionsMenu();
                                //for heart in action bar
                                if (context instanceof ProductListActivity) {
                                    ((ProductListActivity) context).animateHeartIcon();
                                } else if (context instanceof ProductDetailPageActivity) {
                                    ((ProductDetailPageActivity) context).animateHeartIcon();
                                }
                                notifyComplete(true, listener);
                            } else {
                                MmStatusAlertUtil.show(context, MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR,
                                        context.getString(R.string.LB_CA_MOVE2WISHLIST_FAILED), null);
                                heartIcon.setSelected(false);
                                notifyComplete(false, listener);
                            }
                        }
                    });
        }
    }

    private static void notifyComplete(boolean isSuccess, WishListActionListener listener) {
        if (listener != null) {
            listener.completedAction(isSuccess);
        }
    }

    public interface WishListActionListener {
        void completedAction(boolean isSuccess);
    }

    public static String getWishlistKey() {
        if (MmGlobal.getUserKeyForCartRequest() != null) {
            return null;
        }
        if (MmGlobal.anonymousWishListKey().isEmpty()) {
            return "0";
        }
        return MmGlobal.anonymousWishListKey();
    }

    public static void animateHeartIcon(Context context, final View view) {
        Animation zoomIn = AnimationUtils.loadAnimation(context, R.anim.zoom_in_heart);
        final Animation zoomOut = AnimationUtils.loadAnimation(context, R.anim.zoom_out_heart);

        view.startAnimation(zoomIn);
        zoomIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.startAnimation(zoomOut);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}


