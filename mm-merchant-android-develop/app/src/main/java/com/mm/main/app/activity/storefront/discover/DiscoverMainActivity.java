package com.mm.main.app.activity.storefront.discover;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.mm.main.app.activity.storefront.product.ProductListActivity;
import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.activity.storefront.cart.ShoppingCartActivity;
import com.mm.main.app.activity.storefront.search.AllBrandSearchActivity;
import com.mm.main.app.activity.storefront.search.NoSearchResultActivity;
import com.mm.main.app.activity.storefront.search.ProductListSearchActivity;
import com.mm.main.app.activity.storefront.wishlist.WishListActivity;
import com.mm.main.app.blurbehind.BlurBehind;
import com.mm.main.app.blurbehind.OnBlurCompleteListener;
import com.mm.main.app.fragment.DiscoverBrandsFragment;
import com.mm.main.app.fragment.DiscoverCategoriesFragment;
import com.mm.main.app.helper.ProductFilterHelper;
import com.mm.main.app.layout.SlidingTabLayout;
import com.mm.main.app.listitem.DiscoverBannerListItem;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.schema.Category;
import com.mm.main.app.schema.CategoryBrandMerchant;
import com.mm.main.app.schema.response.SearchResponse;
import com.mm.main.app.schema.Style;
import com.mm.main.app.service.SearchServiceBuilder;
import com.mm.main.app.utils.Controllable;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.ActivityView;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

public class DiscoverMainActivity extends AppCompatActivity
        implements DiscoverBrandsFragment.OnListFragmentInteractionListener,
        DiscoverCategoriesFragment.OnListFragmentInteractionListener, Controllable {

    final static int REQUEST_SEARCH = DiscoverGroupActivity.REQUEST_DISCOVER_MAIN + 1;
    final static int REQUEST_NO_RESULT_PAGE = REQUEST_SEARCH + 1;
    final static int REQUEST_ALL_BRAND = REQUEST_NO_RESULT_PAGE + 1;

    Toolbar toolbar;
    MyPageAdapter pageAdapter;
    List<Fragment> fragments;

    boolean searchPageResultOpened;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeNoActionBar);
        setContentView(R.layout.activity_discover_main);
        ButterKnife.bind(this);
        setupToolbar();

        setupFragments();
    }

    @OnClick(R.id.tvSearchWord)
    public void startSearchPage() {
//        Intent intent = new Intent(this, FilterActivity.class);
//
//        // Create the view using FirstGroup's LocalActivityManager
//        View view = DiscoverGroupActivity.group.getLocalActivityManager()
//                .startActivity("product_list", intent
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
//                .getDecorView();
//        // Again, replace the view
//        DiscoverGroupActivity.group.replaceView(new ActivityView(this, view));

        final Intent intent = new Intent(this, ProductListSearchActivity.class);
        BlurBehind.getInstance().execute(this, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                StorefrontMainActivity.animationType = StorefrontMainActivity.AnimationType.NONE;
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                getParent().startActivityForResult(intent, REQUEST_SEARCH);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.d("DiscoverMain", requestCode + " : " + requestCode);
        switch (requestCode) {
            case REQUEST_SEARCH:
                if (resultCode == Activity.RESULT_OK) {
                    if (SearchStyle.getInstance().getCategoryid() != null ||
                            SearchStyle.getInstance().getBrandid() != null ||
                            SearchStyle.getInstance().getMerchantid() != null ||
                            !TextUtils.isEmpty(SearchStyle.getInstance().getQueryString())) {
                        searchProduct(true, false);
                    }
                }
                break;
            case REQUEST_NO_RESULT_PAGE:
                if (resultCode == Activity.RESULT_OK) {
                    if (SearchStyle.getInstance().getCategoryid() != null ||
                            SearchStyle.getInstance().getBrandid() != null ||
                            SearchStyle.getInstance().getMerchantid() != null ||
                            !TextUtils.isEmpty(SearchStyle.getInstance().getQueryString())) {
                        searchProduct(false, false);
                    }
                }
                break;
            case REQUEST_ALL_BRAND:
                if (resultCode == Activity.RESULT_OK) {
                    searchProduct(true, true);
                }
                break;
        }
    }

    private void searchProduct(final boolean needNoResultPage, final boolean allBrandFilter) {
            MmProgressDialog.show(this);
            SearchServiceBuilder.searchStyleWithFilter(new MmCallBack<SearchResponse>(this) {
                @Override
                public void onSuccess(Response<SearchResponse> response, Retrofit retrofit) {
                    List<Style> productList = response.body().getPageData();
                    if (productList.size() == 0) {
                        if (needNoResultPage) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            startNoSearchResultPage();
                                        }
                                    });
                                }
                            }).start();
                        }
                    } else {
                        startProductListPage(null, allBrandFilter ? false: true);
                    }

                }
            });

    }

    private void startNoSearchResultPage() {
        final Intent intent = new Intent(this, NoSearchResultActivity.class);
        BlurBehind.getInstance().execute(this, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                StorefrontMainActivity.animationType = StorefrontMainActivity.AnimationType.NONE;
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                getParent().startActivityForResult(intent, REQUEST_NO_RESULT_PAGE);
            }
        });
    }

    private List<Fragment> getFragments() {

        List<Fragment> fList = new ArrayList<Fragment>();
        fList.add(DiscoverBrandsFragment.newInstance(1));
        fList.add(DiscoverCategoriesFragment.newInstance(1));

        return fList;

    }

    private void setupFragments() {
        cleanFragments();
        fragments = getFragments();
        pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
        ViewPager pager = (ViewPager) findViewById(R.id.viewpager);

        pager.setAdapter(pageAdapter);


        // Assiging the Sliding Tab Layout View
        SlidingTabLayout tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Size for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.primary1);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

    }

    private void cleanFragments() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        if (fragmentManager.getFragments() != null) {
            fragmentManager.getFragments().clear();
        }
        if (pageAdapter != null) {
            pageAdapter.notifyDataSetChanged();
        }
    }

    public void startProductListPage(Category category, boolean isFromSearchPage) {
        ProductFilterHelper.setFullStyleList(null);
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra(ProductListActivity.EXTRA_CATEGORY, category);
        intent.putExtra(ProductListActivity.EXTRA_FROM_SEARCH_PAGE, isFromSearchPage);

        // Create the view using FirstGroup's LocalActivityManager
        View view = DiscoverGroupActivity.group.getLocalActivityManager()
                .startActivity(DiscoverGroupActivity.ACTIVITY_ID_PRODUCT_LIST, intent
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                .getDecorView();
        // Again, replace the view
        DiscoverGroupActivity.group.replaceView(new ActivityView(this, view), DiscoverGroupActivity.ACTIVITY_ID_DISCOVER_MAIN);
        searchPageResultOpened = isFromSearchPage;
    }

    public void startAllBrandPage() {
        ProductFilterHelper.setFullStyleList(null);
        final Intent intent = new Intent(this, AllBrandSearchActivity.class);
        getParent().startActivityForResult(intent, REQUEST_ALL_BRAND);
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_discover, menu);

        //for heart icon
        MenuItem item = menu.findItem(R.id.action_like);
        MenuItemCompat.setActionView(item, R.layout.badge_button_like);

        RelativeLayout badgeLayout = (RelativeLayout) MenuItemCompat.getActionView(item);
        View badge = (View) badgeLayout.findViewById(R.id.btnBadge);

        if(CacheManager.getInstance().isWishlistHasItem())
        {
            badge.setVisibility(View.VISIBLE);
        }
        else {
            badge.setVisibility(View.GONE);
        }

        item.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DiscoverMainActivity.this, WishListActivity.class);
                startActivity(i);
            }
        });


        //for cart icon
        MenuItem cartItem = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(cartItem, R.layout.badge_button_cart);

        RelativeLayout badgeCartLayout = (RelativeLayout) MenuItemCompat.getActionView(cartItem);
        View cartBadge = (View) badgeCartLayout.findViewById(R.id.btnBadge);

        if(CacheManager.getInstance().isCartHasItem())
        {
            cartBadge.setVisibility(View.VISIBLE);
        }
        else {
            cartBadge.setVisibility(View.GONE);
        }

        cartItem.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DiscoverMainActivity.this, ShoppingCartActivity.class);
                startActivity(i);
            }
        });


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cart) {
            Intent i = new Intent(this,ShoppingCartActivity.class);
            startActivity(i);
        }
        else if(id == R.id.action_like)
        {
            Intent i = new Intent(this,WishListActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(DiscoverBannerListItem item) {

    }

    @Override
    public void onDiscoverCategoryFragmentInteraction(Category item) {

    }

    @Override
    public void onDiscoverCategoryBrandMerchantFragmentInteraction(Category category, CategoryBrandMerchant item) {

    }

    @Override
    public void resume() {
        onResume();
    }

    class MyPageAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;

        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return getResources().getString(R.string.LB_AC_BRAND);
            } else {
                return getResources().getString(R.string.LB_CA_CATEGORY_BRAND);
            }
        }

    }

    @Override
    protected void onResume(){
        super.onResume();
        for (int i = 0; i < fragments.size(); i++) {
            if (fragments.get(i) instanceof Controllable) {
                ((Controllable) (fragments.get(i))).resume();
            }
        }

        this.invalidateOptionsMenu();
    }

    public void setSearchPageResultOpened(boolean opened){
        searchPageResultOpened = opened;
    }

}
