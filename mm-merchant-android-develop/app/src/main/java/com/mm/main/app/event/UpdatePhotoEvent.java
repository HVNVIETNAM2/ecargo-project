package com.mm.main.app.event;

/**
 * Created by andrew on 6/11/15.
 */
public class UpdatePhotoEvent {
    public final String message;
    public String profileImage;
    public String coverImage;
    public boolean isSuccess;

    public UpdatePhotoEvent(String message, String profileImage, String coverImage) {
        this.message = message;
        this.profileImage = profileImage;
        this.coverImage = coverImage;
    }

    public UpdatePhotoEvent(String message, boolean isSuccess) {
        this.message = message;
        this.isSuccess = isSuccess;
    }
}
