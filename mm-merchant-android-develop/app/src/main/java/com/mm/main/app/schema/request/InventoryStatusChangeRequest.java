package com.mm.main.app.schema.request;

/**
 * Created by henrytung on 26/10/15.
 */
public class InventoryStatusChangeRequest {
    String InventoryLocationId;
    String Status;

    public InventoryStatusChangeRequest() {
    }

    public InventoryStatusChangeRequest(String inventoryLocationId, String status) {
        InventoryLocationId = inventoryLocationId;
        Status = status;
    }

    public String getInventoryLocationId() {
        return InventoryLocationId;
    }

    public void setInventoryLocationId(String inventoryLocationId) {
        InventoryLocationId = inventoryLocationId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
