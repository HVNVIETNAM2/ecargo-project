package com.mm.main.app.schema.request;

/**
 * Created by ductranvt on 1/21/2016.
 */
public class CartItemRemoveRequest {
    String CultureCode;
    String UserKey;
    String CartKey;
    Integer CartItemId;

    public CartItemRemoveRequest(String cultureCode, String userKey, String cartKey, Integer cartItemId) {
        CultureCode = cultureCode;
        UserKey = userKey;
        CartKey = cartKey;
        CartItemId = cartItemId;
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getCartKey() {
        return CartKey;
    }

    public void setCartKey(String cartKey) {
        CartKey = cartKey;
    }

    public Integer getCartItemId() {
        return CartItemId;
    }

    public void setCartItemId(Integer cartItemId) {
        CartItemId = cartItemId;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }
}
