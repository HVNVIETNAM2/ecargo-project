package com.mm.main.app.adapter.strorefront.product;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.product.PhotosViewerActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.schema.ImageData;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nhutphan on 3/2/2016.
 */
public class ProductImagePagerAdapter extends PagerAdapter implements View.OnClickListener{

    public static int LOOPS_COUNT = 100;
    private List<ImageData> mValues;
    private Activity context;

    public void setContext(Activity context) {
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        int virtualPosition = position % getRealCount();

        LayoutInflater layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View page = layoutInflater.inflate(R.layout.image_default, container, false);
        page.setOnClickListener(this);
        page.setTag(virtualPosition);
        ImageView defaultImage = (ImageView) page.findViewById(R.id.defaultImage);
        int screenWidth = UiUtil.getDisplayWidth();
        defaultImage.getLayoutParams().width = screenWidth;
        defaultImage.getLayoutParams().height = UiUtil.getHeightByWidthWithRatio(screenWidth, 1.0f);
        defaultImage.requestLayout();
        Picasso.with(MyApplication.getContext()).load(PathUtil.getProductImageUrl(mValues.get(virtualPosition).getImageKey())).into(defaultImage);
        ((ViewPager) container).addView(page);
        return page;
    }

    public void setmValues(List<ImageData> mValues) {
        this.mValues = mValues;
    }

    @Override
    public int getCount() {
        if (getRealCount() == 0) {
            return 0;
        }

        return getRealCount() * LOOPS_COUNT;
    }

    public int getRealCount() {
        return mValues.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context, PhotosViewerActivity.class);
        intent.putExtra(PhotosViewerActivity.IMAGE_DATA_KEY, (Serializable) mValues);
        intent.putExtra(PhotosViewerActivity.CURRENT_POS_KEY, (int) v.getTag());
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }
}
