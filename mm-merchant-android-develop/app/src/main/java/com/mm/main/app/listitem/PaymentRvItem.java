package com.mm.main.app.listitem;

import com.mm.main.app.schema.Payment;

/**
 * Created by thienchaud on 26-Feb-16.
 */
public class PaymentRvItem {

    Payment payment;
    boolean isSelected;

    public PaymentRvItem(Payment payment, boolean isSelected) {
        this.payment = payment;
        this.isSelected = isSelected;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
