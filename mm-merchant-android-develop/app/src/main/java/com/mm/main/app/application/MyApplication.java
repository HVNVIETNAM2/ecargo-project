package com.mm.main.app.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;
import android.widget.Toast;

//import com.activeandroid.ActiveAndroid;
import com.alexbbb.uploadservice.AbstractUploadServiceReceiver;
import com.alexbbb.uploadservice.UploadService;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.mm.main.app.R;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.event.UpdatePhotoEvent;
import com.mm.main.app.facade.SettingServiceFacade;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.manager.ImManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.schema.response.UploadIDImageResponse;
import com.mm.main.app.schema.response.UploadImageResponse;
import com.mm.main.app.schema.User;
import com.mm.main.app.view.MmProgressDialog;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Override for Active Android and get MmGlobal easily
 */
public class MyApplication extends Application implements SettingServiceFacade.SettingServiceCallBack {

    private static Context mContext;
    String imageKey;
    String coverKey;
    public static final String TAG = "My Application Log: ";
    public static UploadImageResponse uploadImageResponse;
    public final AbstractUploadServiceReceiver uploadReceiver =
            new AbstractUploadServiceReceiver() {

                // you can override this progress method if you want to get
                // the completion progress in percent (0 to 100)
                // or if you need to know exactly how many bytes have been transferred
                // override the method below this one
                @Override
                public void onProgress(String uploadId, int progress) {
                    Logger.i(TAG, "The progress of the upload with ID "
                            + uploadId + " is: " + progress);
                    Logger.i(TAG, "This is the App Receiver");
                }

                @Override
                public void onProgress(final String uploadId,
                                       final long uploadedBytes,
                                       final long totalBytes) {
                    Logger.i(TAG, "Upload with ID "
                            + uploadId + " uploaded bytes: " + uploadedBytes
                            + ", total: " + totalBytes);
                }

                @Override
                public void onError(String uploadId, Exception exception) {
                    Logger.e(TAG, "Error in upload with ID: " + uploadId + ". "
                            + exception.getLocalizedMessage(), exception);

                }

                @Override
                public void onCompleted(final String uploadId,
                                        int serverResponseCode,
                                        String serverResponseMessage) {
                    Logger.i(TAG, "Upload with ID " + uploadId
                            + " has been completed with HTTP " + serverResponseCode
                            + ". Response from server: " + serverResponseMessage);

                    Gson gson = new Gson();
                    if (Constant.UploadImageId.ID_PHOTO.equals(uploadId)) {
                        if (serverResponseMessage.equals("true\n")) {
                            EventBus.getDefault().post(new UpdatePhotoEvent(serverResponseMessage, true));
                        } else {
                            UploadIDImageResponse response = gson.fromJson(serverResponseMessage, UploadIDImageResponse.class);
                            EventBus.getDefault().post(new UpdatePhotoEvent(response.getMessage(), false));
                        }
                    } else {
                        uploadImageResponse = gson.fromJson(serverResponseMessage, UploadImageResponse.class);
                        imageKey = uploadImageResponse.getImageKey();
                        coverKey = uploadImageResponse.getCoverImage();
                        APIManager.getInstance().getUserService().viewUser(MmGlobal.getUserId())
                                .enqueue(new Callback<User>() {
                                    @Override
                                    public void onResponse(Response<User> response, Retrofit retrofit) {
                                        if (response.isSuccess()) {
                                            User user = response.body();
                                            user.setProfileImage(imageKey);
                                            user.setCoverImage(coverKey);
                                            if (Constant.UploadImageId.STORE_FRONT_PROFILE_PHOTO.equals(uploadId)) {
                                                onSuccess(user);
                                            } else {
                                                try {
                                                    SettingServiceFacade.update(user, MyApplication.this);
                                                } catch (InterruptedException e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                        } else {
                                            Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.MSG_ERR_USER_UNAUTHORIZED),
                                                    Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Throwable t) {
                                        Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.MSG_ERR_NETWORK_FAIL),
                                                Toast.LENGTH_LONG).show();
                                    }
                                });
                    }
                }

            };

//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//        MultiDex.install(this);
//    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mContext = getApplicationContext();
//        ActiveAndroid.initialize(this);
        Parse.initialize(this, "tSF6abJ4ZwUM6pjpVhDF4poWYmxbPQQg7DbYh8GS", "qO75K5Zk7jk3sEYxr0i5hX78Ri35Fh4LZf2MxpsN");
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                String deviceToken = (String) ParseInstallation.getCurrentInstallation().get(Constant.DEVICE_TOKEN);
                PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().
                        putString(Constant.DEVICE_TOKEN, deviceToken).commit();
                Logger.i(TAG, "My deviceToken for Parse Push is : " + deviceToken);
            }
        });

        UploadService.NAMESPACE = "com.mm.merchant.app";
        ImManager.getInstance();
        // register to be informed of activities starting up
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity activity,
                                          Bundle savedInstanceState) {

                LanguageManager.getInstance().updateDeviceLocale();

                // new activity created; force its orientation to portrait
                activity.setRequestedOrientation(
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                if (activity instanceof ActionBarActivity) {
                    ((ActionBarActivity) activity).supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
                }
            }

            @Override
            public void onActivityStarted(Activity activity) {
                uploadReceiver.register(activity);
            }

            @Override
            public void onActivityResumed(Activity activity) {
                uploadReceiver.register(activity);
                mContext = activity.getBaseContext();
                LanguageManager.getInstance().updateDeviceLocale();
                //TODO
                CacheManager.getInstance().updateAllCache(); //need to check performance here, because call many times ??
            }

            @Override
            public void onActivityPaused(Activity activity) {
                uploadReceiver.unregister(activity);
                MmProgressDialog.dismiss();
            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }

        });
    }

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onSuccess(Object content) {
        Logger.i(TAG, "Success");
        EventBus.getDefault().post(new UpdatePhotoEvent("Hello everyone!", uploadImageResponse.getProfileImage(), uploadImageResponse.getCoverImage()));
    }

    @Override
    public void onFailure(String message) {
        Logger.i(TAG, "Failure");

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}