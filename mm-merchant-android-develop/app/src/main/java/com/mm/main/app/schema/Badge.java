
package com.mm.main.app.schema;

public class Badge {

    private Integer BadgeId;
    private String BadgeNameInvariant;
    private String BadgeCode;
    private Integer BadgeCultureId;
    private String CultureCode;
    private String BadgeName;

    public Badge(String badgeName) {
        BadgeName = badgeName;
    }

    /**
     * 
     * @return
     *     The BadgeId
     */
    public Integer getBadgeId() {
        return BadgeId;
    }

    /**
     * 
     * @param BadgeId
     *     The BadgeId
     */
    public void setBadgeId(Integer BadgeId) {
        this.BadgeId = BadgeId;
    }

    /**
     * 
     * @return
     *     The BadgeNameInvariant
     */
    public String getBadgeNameInvariant() {
        return BadgeNameInvariant;
    }

    /**
     * 
     * @param BadgeNameInvariant
     *     The BadgeNameInvariant
     */
    public void setBadgeNameInvariant(String BadgeNameInvariant) {
        this.BadgeNameInvariant = BadgeNameInvariant;
    }

    /**
     * 
     * @return
     *     The BadgeCode
     */
    public String getBadgeCode() {
        return BadgeCode;
    }

    /**
     * 
     * @param BadgeCode
     *     The BadgeCode
     */
    public void setBadgeCode(String BadgeCode) {
        this.BadgeCode = BadgeCode;
    }

    /**
     * 
     * @return
     *     The BadgeCultureId
     */
    public Integer getBadgeCultureId() {
        return BadgeCultureId;
    }

    /**
     * 
     * @param BadgeCultureId
     *     The BadgeCultureId
     */
    public void setBadgeCultureId(Integer BadgeCultureId) {
        this.BadgeCultureId = BadgeCultureId;
    }

    /**
     * 
     * @return
     *     The CultureCode
     */
    public String getCultureCode() {
        return CultureCode;
    }

    /**
     * 
     * @param CultureCode
     *     The CultureCode
     */
    public void setCultureCode(String CultureCode) {
        this.CultureCode = CultureCode;
    }

    /**
     * 
     * @return
     *     The BadgeName
     */
    public String getBadgeName() {
        return BadgeName;
    }

    /**
     * 
     * @param BadgeName
     *     The BadgeName
     */
    public void setBadgeName(String BadgeName) {
        this.BadgeName = BadgeName;
    }

}
