package com.mm.main.app.activity.storefront.friend;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.im.UserChatActivity;
import com.mm.main.app.adapter.strorefront.friend.ImLandingRvAdapter;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.ImManager;
import com.mm.main.app.record.ImEvent;
import com.mm.main.app.schema.Conv;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.MmCallBack;
import de.greenrobot.event.EventBus;
import retrofit.Response;
import retrofit.Retrofit;

import static com.google.common.base.Predicates.not;

public class ImLandingActivity extends AppCompatActivity {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerViewChat;

    ImLandingRvAdapter adapter;
    ArrayList<Conv> convs;
    ArrayList<User> users;
    List<User> friends;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_im_landing);
        ButterKnife.bind(this);
        getFriends();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void setUpAdapter() {

        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerViewChat.setLayoutManager(manager);
        recyclerViewChat.setHasFixedSize(true);
        adapter = new ImLandingRvAdapter(getApplication(), convs, users, new ImLandingRvAdapter.LandingCallBack() {
            @Override
            public void delete(final int position) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        convs.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                },500);
            }

            @Override
            public void goChatRoom(int position) {
                Intent i = new Intent(ImLandingActivity.this,UserChatActivity.class);
                i.putExtra("ConversationObject", convs.get(position));
                startActivity(i);

            }
        });
        recyclerViewChat.setAdapter(adapter);
    }


    @OnClick(R.id.tvShowFriendList)
    public void onClickShowFriendList() {
        Intent showListFriend = new Intent(ImLandingActivity.this, FriendListActivity.class);
        startActivity(showListFriend);
    }

    @OnClick(R.id.imgAddNewFriend)
    public void onClickAddNewFriend() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final CharSequence[] items = {getString(R.string.LB_CA_IM_FIND_USER_ADD),
                getString(R.string.LB_CA_IM_SCAN_QR),
                getString(R.string.LB_CA_CANCEL)};

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {
                    case 0:
                        Intent intentFind = new Intent(ImLandingActivity.this, FindNewUserActivity.class);
                        startActivity(intentFind);
                        break;

                    case 1:
                        Intent intentScan = new Intent(ImLandingActivity.this, QRScanActivity.class);
                        startActivity(intentScan);
                        break;

                    case 3:
                        dialog.dismiss();
                        break;
                }

                dialog.dismiss();
            }
        });

        builder.show();
    }

    public void getListChat() {

        convs = (ArrayList<Conv>) ImManager.getInstance().convList;
        users = new ArrayList<>();
        for (Conv conv : convs){
            List<String> userKeyList = conv.getUserList();
            final String toUserKey = Iterables.find(
                    userKeyList, not(Predicates.containsPattern(MmGlobal.getUserKey())));
            User user = Iterables.find(friends, new Predicate<User>() {
                @Override
                public boolean apply(User friend) {
                    return friend.getUserKey().equals(toUserKey);
                }
            }, new User());
            users.add(user);
        }
        setUpAdapter();

    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(ImEvent event) {
        switch (event.getMyActionType()) {

            case UPDATE_CONV:
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        getFriends();
                    }
                });
                break;
        }
    }

    private void getFriends() {
        friends = new ArrayList<>();
        APIManager.getInstance().getFriendService().listFriend(MmGlobal.getUserKey(), MmGlobal.getUserKey())
                .enqueue(new MmCallBack<List<User>>(ImLandingActivity.this) {
                    @Override
                    public void onSuccess(Response<List<User>> response, Retrofit retrofit) {
                        friends = response.body();
                        getListChat();
                    }
                });
    }
}
