package com.mm.main.app.utils;

import android.app.Activity;

import com.mm.main.app.activity.storefront.StorefrontMainActivity;

/**
 * Created by henrytung on 22/12/2015.
 */
public class StoreFrontUtil {

    public static void setMasterTabVisibility(Activity activity, boolean show) {
        if (activity == null) {
            return;
        }

        if (activity instanceof StorefrontMainActivity) {
            ((StorefrontMainActivity) (activity)).openTabHost(show);
        } else {
            setMasterTabVisibility(activity.getParent(), show);
        }

    }
}
