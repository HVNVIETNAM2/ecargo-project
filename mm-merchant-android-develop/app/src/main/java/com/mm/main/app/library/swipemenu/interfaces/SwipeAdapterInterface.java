package com.mm.main.app.library.swipemenu.interfaces;

public interface SwipeAdapterInterface {

    int getSwipeLayoutResourceId(int position);

    void notifyDatasetChanged();

}
