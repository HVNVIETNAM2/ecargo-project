package com.mm.main.app.manager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.mm.main.app.activity.merchant.login.LoginActivity;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.helper.CartHelper;
import com.mm.main.app.helper.WishlistHelper;
import com.mm.main.app.schema.Cart;
import com.mm.main.app.schema.WishList;
import com.mm.main.app.schema.response.LoginResponse;
import com.umeng.analytics.MobclickAgent;

import retrofit.Response;

/**
 * Created by henrytung on 9/11/15.
 */
public class LifeCycleManager {

    public static void login(Response<LoginResponse> response) {
        login(response, null, true);
    }

    public static void login(Response<LoginResponse> response, String userName, boolean rememberMe) {
        String tokenString = response.body().getToken();
        Integer userId = response.body().getUserId();
        String userKey = response.body().getUserKey();
        MmGlobal.setToken(tokenString);
        MmGlobal.setUserId(userId);
        MmGlobal.setUserKey(userKey);
        MmGlobal.setRemember(rememberMe);
        MmGlobal.setLogin(true);
        if (!TextUtils.isEmpty(userName)) {
            MmGlobal.setUserName(userName);
            MobclickAgent.onProfileSignIn(userName);
        }

        CartHelper.checkCartExist();
        WishlistHelper.checkWishlistExist();
        CacheManager.getInstance().updateAllCache();
        ImManager.getInstance().sendAnnounce();

    }

    public static void cleanStart(Activity currentActivity, Class afterLogoutActivity, Bundle bundle) {
        Intent loginIntent = new Intent(currentActivity, afterLogoutActivity);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        if (bundle != null) {
            loginIntent.putExtras(bundle);
        }
        currentActivity.startActivity(loginIntent);
        currentActivity.finish();
    }

    public static void logout(Activity currentActivity, Class afterLogoutActive) {
        MmGlobal.setRemember(false);
        LanguageManager.clearLanguageList();
        cleanStart(currentActivity, afterLogoutActive, null);
    }

    public static void storeFrontLogout(Activity currentActivity, Class afterLogoutActive) {
        storeFrontLogout(currentActivity, afterLogoutActive, true);
    }

    public static void storeFrontLogout(Activity currentActivity, Class afterLogoutActive, boolean wipeData) {
        MmGlobal.setLogin(false);
        if (wipeData) {
            MmGlobal.setToken("");
            MmGlobal.setUserId(0);
            MmGlobal.setUserKey("");
            MmGlobal.setAnonymousWishListKey("");
            MmGlobal.setAnonymousShoppingCartKey("");

            CacheManager.getInstance().setCart(new Cart());
            CacheManager.getInstance().setWishlist(new WishList());
        }
        logout(currentActivity, afterLogoutActive);
    }

    public static void relogin(Activity currentActivity) {
        logout(currentActivity, LoginActivity.class);
    }

}
