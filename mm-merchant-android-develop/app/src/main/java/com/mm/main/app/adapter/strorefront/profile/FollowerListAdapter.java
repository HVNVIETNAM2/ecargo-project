package com.mm.main.app.adapter.strorefront.profile;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.activity.storefront.interest.InterestMappingBrandMerchantsActivity;
import com.mm.main.app.activity.storefront.profile.StoreFrontUserProfileActivity;
import com.mm.main.app.adapter.strorefront.filter.UserFilterSelectionListAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.ProfileLifeCycleManager;
import com.mm.main.app.schema.Follow;
import com.mm.main.app.schema.FriendRequest;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by binhtruongp on 3/11/2016.
 */
public class FollowerListAdapter extends UserFilterSelectionListAdapter {

    HashMap<Integer, Integer> hashMapFollowed = new HashMap<>();
    HashMap<Integer, Integer> hashMapFriend = new HashMap<>();

    public FollowerListAdapter(Activity context, List<FilterListItem<User>> originalData) {
        super(context, originalData);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final User currentUser = filteredData.get(position).getT();
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.user_follow_item, null, true);
            ((ViewGroup)convertView).setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        }
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        tvName.setText(currentUser.getDisplayName());

        CircleImageView ivProfilePic = (CircleImageView) convertView.findViewById(R.id.ivProfilePic);
        String url = PathUtil.getUserImageUrl(currentUser.getProfileImage());
        Picasso.with(MyApplication.getContext()).load(url).into(ivProfilePic);

        TextView tvFollowerCount = (TextView) convertView.findViewById(R.id.tvFollowerCount);
        tvFollowerCount.setText(currentUser.getFollowerCount() + " " + context.getString(R.string.LB_CA_NO_OF_FOLLOWER));

        ImageView imgIsCurator = (ImageView) convertView.findViewById(R.id.imgIsCurator);

        if (currentUser.getIsCuratorUser() != null && currentUser.getIsCuratorUser() == 1) {
            ivProfilePic.setBorderColor(context.getResources().getColor(R.color.mm_red));
            imgIsCurator.setVisibility(View.VISIBLE);
        } else {
            ivProfilePic.setBorderColor(context.getResources().getColor(R.color.white));
            imgIsCurator.setVisibility(View.GONE);
        }

        Button btnAddFriend = (Button) convertView.findViewById(R.id.btnAdd);
        btnAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFriendRequest(v, position);
            }
        });
        Button btnFollow = (Button) convertView.findViewById(R.id.btnFollow);
        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFollow(v, position);
            }
        });

        if (currentUser.getIsCuratorUser() != null && currentUser.getIsCuratorUser() == 1) {
            ivProfilePic.setBorderColor(context.getResources().getColor(R.color.mm_red));
            imgIsCurator.setVisibility(View.VISIBLE);
        } else {
            ivProfilePic.setBorderColor(context.getResources().getColor(R.color.white));
            imgIsCurator.setVisibility(View.GONE);
        }

        return convertView;
    }

    private void saveFollow(final View v, int position) {
        User user = filteredData.get(position).getT();
        final Integer userID = user.getUserId();
        Follow follow = new Follow();
        follow.setUserKey(MmGlobal.getUserKey());
        follow.setToUserKey(user.getUserKey());
        final Button btnFollow = (Button) v;
        if (!hashMapFollowed.containsKey(userID)) {
            APIManager.getInstance().getFollowService().saveFollowCurator(follow)
                    .enqueue(new MmCallBack<Boolean>(context) {
                        @Override
                        public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                            if (response.body().booleanValue()) {
                                hashMapFollowed.put(userID, userID);
                                btnFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ok, 0, 0, 0);
                                btnFollow.setText(context.getString(R.string.LB_CA_FOLLOWED));
                                MmStatusAlertUtil.show(context, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                        context.getString(R.string.MSG_SUC_FOLLOWED), null);
                            }
                        }
                    });
        } else {
            APIManager.getInstance().getFollowService().deleteFollow(follow)
                    .enqueue(new MmCallBack<Boolean>(context) {
                        @Override
                        public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                            if (response.body().booleanValue()) {
                                hashMapFollowed.remove(userID);
                                btnFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_add, 0, 0, 0);
                                btnFollow.setText(context.getString(R.string.LB_CA_FOLLOW));
                                MmStatusAlertUtil.show(context, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                        context.getString(R.string.MSG_SUC_UNFOLLOWED), null);
                            }
                        }
                    });
        }
    }

    private void sendFriendRequest(final View v, int position) {
        MmProgressDialog.show(context);
        final FriendRequest friendRequest = new FriendRequest();
        friendRequest.setUserKey(MmGlobal.getUserKey());
        friendRequest.setToUserKey(filteredData.get(position).getT().getUserKey());
        final Button btnAddFriend = (Button) v;
        User user = filteredData.get(position).getT();
        final Integer userID = user.getUserId();

        if (!hashMapFriend.containsKey(userID)) {
            APIManager.getInstance().getFriendService().request(friendRequest)
                    .enqueue(new MmCallBack<Boolean>(context) {
                        @Override
                        public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                            hashMapFriend.put(userID, userID);
                            MmProgressDialog.dismiss();
                            MmStatusAlertUtil.show(context, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK, context.getString(R.string.MSG_SUC_FRIEND_REQ_SENT), null);
                            btnAddFriend.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ok, 0, 0, 0);
                            btnAddFriend.setText(context.getString(R.string.LB_CA_BEFRIENDED));
                        }
                    });
        } else {
            String confirmationContent = context.getString(R.string.LB_CA_REMOVE_FRD_CONF).replace("{0}", filteredData.get(position).getT().getDisplayName());
            ErrorUtil.showTwoOptionsDialog(context, "", confirmationContent, context.getString(R.string.LB_YES), context.getString(R.string.LB_NO), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    MmProgressDialog.dismiss();
                    APIManager.getInstance().getFriendService().deleteRequest(friendRequest).enqueue(new MmCallBack<Boolean>(context) {
                        @Override
                        public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                            hashMapFriend.remove(userID);
                            MmProgressDialog.dismiss();
                            btnAddFriend.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_add, 0, 0, 0);
                            btnAddFriend.setText(context.getString(R.string.LB_CA_ADD_FRIEND));
                        }
                    });

                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    MmProgressDialog.dismiss();
                }
            });
        }
    }
}
