package com.mm.main.app.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ductranvt on 1/15/2016.
 */
public class WishList implements Serializable {
    String CartKey;
    Integer UserId;
    Integer StatusId;
    Integer CartTypeId;
    Date LastCreated;
    Date LastModified;
    List<CartItem> CartItems = new ArrayList<CartItem>();


    public String getCartKey() {
        return CartKey;
    }

    public Integer getUserId() {
        return UserId;
    }

    public Date getLastCreated() {
        return LastCreated;
    }

    public Date getLastModified() {
        return LastModified;
    }

    public Integer getStatusId() {
        return StatusId;
    }

    public List<CartItem> getCartItems() {
        return CartItems;
    }

    public Integer getCartTypeId() {
        return CartTypeId;
    }
}
