package com.mm.main.app.schema.request;

/**
 * Schema class for token
 */
public class UserChangeLanguageRequest {

    String UserId;
    String CultureCode;

    public UserChangeLanguageRequest() {
    }

    public UserChangeLanguageRequest(String userId, String cultureCode) {
        UserId = userId;
        CultureCode = cultureCode;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }
}
