package com.mm.main.app.activity.storefront.checkout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.checkout.AddressSelectionRVAdapter;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.AddressRvItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.Address;
import com.mm.main.app.schema.request.DefaultAddressSaveRequest;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

public class AddressSelectionActivity extends AppCompatActivity implements AddressSelectionRVAdapter.OnSelectAddressListener {

    final public static String SELECTED_ADDRESS_KEY = "SELECTED_ADDRESS_KEY";
    final public static int ADD_ADDRESS_CODE_REQUEST = 3000;


    @Bind(R.id.recyclerAddress)
    RecyclerView recyclerAddress;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    AddressSelectionRVAdapter addressSelectionRVAdapter;
    List<AddressRvItem> addressRvItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_selection);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        recyclerAddress.setHasFixedSize(true);
        recyclerAddress.setLayoutManager(new LinearLayoutManager(this));

        loadAddress();
    }

    private void loadAddress() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getAddressService().listAddressByUserKey(MmGlobal.getUserKey())
                .enqueue(new MmCallBack<List<Address>>(AddressSelectionActivity.this) {
                    @Override
                    public void onSuccess(Response<List<Address>> response, Retrofit retrofit) {
                        ArrayList<Address> addresses = (ArrayList<Address>) response.body();
                        initItemsData(addresses);
                        addressSelectionRVAdapter = new AddressSelectionRVAdapter(AddressSelectionActivity.this, addressRvItems);
                        addressSelectionRVAdapter.setOnSelectAddressListener(AddressSelectionActivity.this);
                        recyclerAddress.setAdapter(addressSelectionRVAdapter);
                    }
                });
    }

    void initItemsData(ArrayList<Address> addresses) {
        String selectedAddressKey = getIntent().getStringExtra(SELECTED_ADDRESS_KEY);
        addressRvItems = new ArrayList<AddressRvItem>();
        for (int i = 0; i < addresses.size(); i++) {
            AddressRvItem item = new AddressRvItem(addresses.get(i), !TextUtils.isEmpty(selectedAddressKey) && addresses.get(i).getUserAddressKey().equals(selectedAddressKey));
            addressRvItems.add(item);
        }
    }

    @OnClick(R.id.btnAddAddress)
    public void addNewAddress() {
        Intent intent = new Intent(this, AddAddressActivity.class);
        startActivityForResult(intent, ADD_ADDRESS_CODE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_ADDRESS_CODE_REQUEST) {
                Address address = (Address) data.getExtras().get(Constant.RESULT);
                selectAddress(address);
            }
        }
    }

    @Override
    public void onSelectAddress(Address address) {
        selectAddress(address);
    }

    private void selectAddress(Address address) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constant.RESULT, address);
        setResult(Activity.RESULT_OK, returnIntent);

        //set default api
        APIManager.getInstance().getAddressService().saveDefaultAddress(new DefaultAddressSaveRequest(MmGlobal.getUserKey(), address.getUserAddressKey()))
                .enqueue(new MmCallBack<String>(AddressSelectionActivity.this) {
                    @Override
                    public void onSuccess(Response<String> response, Retrofit retrofit) {
                        Log.i("save_address", response.toString());
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        super.onFailure(t);
                    }

                    @Override
                    public void onResponse(Response<String> response, Retrofit retrofit) {
                        super.onResponse(response, retrofit);
                        Log.i("save_address", response.toString());
                    }
                });

        finish();
    }
}
