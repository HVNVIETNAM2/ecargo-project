package com.mm.main.app.utils;

import com.mm.main.app.constant.Constant;

/**
 * Created by henrytung on 27/10/15.
 */
public class PathUtil {

    public static int DEFAULT_PROFILE_PIC_SIZE = 200;

    public static String getProfileImageUrl(String key, int size) {
        String tail = "";
        if( size > 0) {
            tail =  "&s=" + size;
        }

        return Constant.getApiURL() + "image/view?key=" + key + tail;
    }

    public static String getProfileImageUrl(String key) {
        return getProfileImageUrl(key, 0);
    }

    public static String getLocalVideo(String packageName, int resId) {
        return "android.resource://" + packageName + "/"
                + resId;
    }

//    public static String getProductImageUrl(String key) {
//        return Constant.API_URL + "productimage/view?key=" + key;
//    }

    public static String getProductImageUrl(String key) {
        return getProductImageUrl(key, 500);
    }

    public static String getCategoryImageUrl(String key) {
        return getCategoryImageUrl(key, 500);
    }

    public static String getBrandImageUrl(String key) {
        return getBrandImageUrl(key, 500);
    }

    public static String getColorImageUrl(String key) {
        return getColorImageUrl(key, 500);
    }

    public static String getMerchantImageUrl(String key) {
        return getMerchantImageUrl(key, 500);
    }

    public static String getUserImageUrl(String key) {
        return getUserImageUrl(key, 200);
    }

    public static String getProductImageUrl(String key, int width) {
        return getReszierImageUrl(key, width, "productimages");
    }

    public static String getBrandImageUrl(String key, int width) {
        return getReszierImageUrl(key, width, "brandimages");
    }

    public static String getCategoryImageUrl(String key, int width) {
        return getReszierImageUrl(key, width, "merchantimages");
    }

    public static String getMerchantImageUrl(String key, int width) {
        return getReszierImageUrl(key, width, "merchantimages");
    }

    public static String getColorImageUrl(String key, int width) {
        return getReszierImageUrl(key, width, "colorimages");
    }

    public static String getReszierImageUrl(String key, int width, String type) {
        return Constant.getApiURL() + "resizer/view?key="+key+"&w="+width+"&b=" + type;
    }

    public static String getUserImageUrl(String key, int width) {
        return getReszierImageUrl(key, width, "userimages");
    }
}
