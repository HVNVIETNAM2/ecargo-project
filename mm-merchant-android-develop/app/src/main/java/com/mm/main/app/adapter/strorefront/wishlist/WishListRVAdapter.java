package com.mm.main.app.adapter.strorefront.wishlist;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.main.app.activity.storefront.checkout.CheckoutActivity;
import com.mm.main.app.activity.storefront.product.ProductDetailPageActivity;
import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.wishlist.WishListActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.library.swipemenu.SwipeDefaultActionListener;
import com.mm.main.app.library.swipemenu.SwipeLayout;
import com.mm.main.app.library.swipemenu.adapters.RecyclerSwipeAdapter;
import com.mm.main.app.listitem.WishlistRvItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.record.StylesRecord;
import com.mm.main.app.schema.Cart;
import com.mm.main.app.schema.request.CartAddRequest;
import com.mm.main.app.schema.CartItem;
import com.mm.main.app.schema.request.CartItemRemoveRequest;
import com.mm.main.app.schema.Sku;
import com.mm.main.app.schema.Style;
import com.mm.main.app.schema.WishList;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class WishListRVAdapter extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    List<WishlistRvItem> items;
    Activity activity;
    String wishlistKey;

    public WishListRVAdapter(Activity activity, List<WishlistRvItem> items, String cartKey) {
        this.activity = activity;
        this.items = items;
        this.wishlistKey = cartKey;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wishlist_item_view, parent, false);
        viewHolder = new WishlistItemViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        final WishlistRvItem item = items.get(position);

        final WishlistItemViewHolder productViewHolder = (WishlistItemViewHolder) viewHolder;

        int skuId = item.getProduct().getSkuId();
        Sku imageSku = null;

        if (item.getProduct().getStyle().getSkuList() != null) {
            if (skuId != 0) {
                for (int i = 0; i < item.getProduct().getStyle().getSkuList().size(); i++) {
                    Sku sku = item.getProduct().getStyle().getSkuList().get(i);
                    if (skuId == sku.getSkuId()) {
                        imageSku = sku;
                        break;
                    }
                }
            }

            if (imageSku == null) {
                if (item.getProduct().getStyle().getSkuList().size() > 0) { //sku list in style is available
                    imageSku = item.getProduct().getStyle().getSkuList().get(0);
                }
            }

            if (imageSku != null) {
                productViewHolder.productName.setText(imageSku.getSkuName());
            } else if (item.getProduct().getStyle().getSkuName() != null) {
                productViewHolder.productName.setText(item.getProduct().getStyle().getSkuName());
            } else {
                productViewHolder.productName.setText("");
            }

        } else if (item.getProduct().getStyle().getSkuName() != null) {
            productViewHolder.productName.setText(item.getProduct().getStyle().getSkuName());
        }


        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CHINA);
        format.setMaximumFractionDigits(0);

        /*
        Setting prices
         */

        Sku targetSku;
        Style style = item.getProduct().getStyle();
        if (item.getProduct().getSkuId() != 0) {  //For SKU level item (added to wishlist from shopping cart)
            targetSku = style.findSkuBySkuId(item.getProduct().getSkuId());
            if (targetSku == null) {
                targetSku = style.getLowestPriceSku();
            }
        } else {
            targetSku = style.getLowestPriceSku();
        }

        double retailPrice = targetSku.getPriceRetail();
        double salePrice = targetSku.getPriceSale();

        if (salePrice > 0) { //exits sale price
            productViewHolder.productPrice.setText(format.format(salePrice));
            productViewHolder.productPrice.setTextColor(activity.getResources().getColor(R.color.primary1));
            productViewHolder.originalPrice.setVisibility(View.VISIBLE);
            productViewHolder.originalPrice.setText(format.format(retailPrice));
            productViewHolder.originalPrice.setPaintFlags(productViewHolder.originalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            productViewHolder.productPrice.setText(format.format(retailPrice));
            productViewHolder.productPrice.setTextColor(activity.getResources().getColor(R.color.secondary2));
            productViewHolder.originalPrice.setVisibility(View.GONE);
        }

        /*
        end setting price
         */

        String imageUrl = style.getImageBaseColor(item.getProduct().getColorKey());
        if (TextUtils.isEmpty(imageUrl)) {
            productViewHolder.productImage.setImageResource(R.drawable.spacer);
        } else {
            Picasso.with(MyApplication.getContext()).load(imageUrl).into(productViewHolder.productImage);
        }

        productViewHolder.productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartItem cartItem = item.getProduct();
                Intent intent = new Intent(activity, ProductDetailPageActivity.class);
                intent.putExtra(ProductDetailPageActivity.STYLE_CODE_DATA, cartItem.getStyleCode());
                activity.startActivity(intent);
            }
        });

        if (item.getProduct().getStyle().getBrandHeaderLogoImage() != null) {
            Picasso.with(MyApplication.getContext()).load(PathUtil.getBrandImageUrl(item.getProduct().getStyle().getBrandHeaderLogoImage())).into(productViewHolder.brandImage);
        }

        productViewHolder.linearDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAction(item.getProduct().getCartItemId());
            }
        });

        productViewHolder.linearAddToCart.setTag(position);
        productViewHolder.linearAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemManger.removeShownLayouts(productViewHolder.swipeLayout);
                showCheckout(item.getProduct());
            }
        });

        productViewHolder.addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCheckout(item.getProduct());
            }
        });

        SwipeLayout swipeLayout = productViewHolder.swipeLayout;
        swipeLayout.setDefaultViewRightID(R.id.linearDelete);
        swipeLayout.setDefaultViewLeftID(R.id.linearAddToCart);
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        swipeLayout.addDrag(SwipeLayout.DragEdge.Right, swipeLayout.findViewById(R.id.rightMenu));
        swipeLayout.addDrag(SwipeLayout.DragEdge.Left, swipeLayout.findViewById(R.id.leftMenu));
        swipeLayout.setTag(position);
        swipeLayout.setmDefaultActionListener(swipeDefaultActionListener);

        mItemManger.bind(viewHolder.itemView, position);
        setAppiumId(productViewHolder, position);
    }

    private void setAppiumId(WishlistItemViewHolder viewHolder, int position) {
        WishListActivity mainActivity = (WishListActivity) activity;
        WishlistRvItem item = items.get(position);
        String indexString = "-" + String.valueOf(position + 1);

        viewHolder.productImage.setContentDescription(mainActivity.getContentDescription("UIIMG_ITEM_COVER" + indexString));
        viewHolder.brandImage.setContentDescription(mainActivity.getContentDescription("UIIMG_BRAND_LOGO" + indexString));
        viewHolder.productName.setContentDescription(mainActivity.getContentDescription("UILB_PRODUCT_NAME" + indexString));
        viewHolder.originalPrice.setContentDescription(mainActivity.getContentDescription("UILB_RETAIL_PRICE_SH" + indexString));
        viewHolder.productPrice.setContentDescription(mainActivity.getContentDescription("UILB_SALES_PRICE" + indexString));
        viewHolder.addToCartBtn.setContentDescription(mainActivity.getContentDescription("UIBT_ADD2CART" + indexString));
        viewHolder.linearAddToCart.setContentDescription(mainActivity.getContentDescription("UIBT_ADD2CART_2" + indexString));
        viewHolder.linearDelete.setContentDescription(mainActivity.getContentDescription("UIBT_DELETE" + indexString));
    }

    private boolean isItemSoldOut(WishlistRvItem item) {
        if (item.getProduct().getStyle().getSkuList().get(0).getQtyAts() == null || item.getProduct().getStyle().getSkuList().get(0).getQtyAts() == null) {
            return false;
        }
        try {
            if (item.getProduct().getStyle().getSkuList().get(0).getQtyAts() < item.getProduct().getStyle().getSkuList().get(0).getQtySafetyThreshold()) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return false;
    }

    private SwipeDefaultActionListener swipeDefaultActionListener = new SwipeDefaultActionListener() {
        @Override
        public void UpdateDefaultActionUI(boolean isRightMenu) {
        }

        @Override
        public void CancelDefaultActionUI(boolean isRightMenu) {
        }

        @Override
        public void ActionDefault(boolean isRightMenu, SwipeLayout swipeLayout) {
            int pos = (int) swipeLayout.getTag();
            mItemManger.removeShownLayouts(swipeLayout);

            if (isRightMenu) {
                deleteAction((items.get(pos)).getProduct().getCartItemId());
            } else { //left
                showCheckout((items.get(pos)).getProduct());
            }
        }
    };

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeLayout;
    }

    private void deleteAction(final Integer cartItemID) {
        ErrorUtil.showConfirmDialog(activity, activity.getString(R.string.MSG_CA_CONFIRM_REMOVE), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                deleteItem(cartItemID);
            }
        });
    }

    private void deleteItem(Integer cartItemID) {
        MmProgressDialog.show(activity);
        APIManager.getInstance().getWishListService().removeWishListItem(new CartItemRemoveRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKeyForCartRequest(), MmGlobal.getWishlistKeyForRequest(), cartItemID))
                .enqueue(new MmCallBack<WishList>(activity) {
                    @Override
                    public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                        MmProgressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        super.onFailure(t);
                        MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR,
                                activity.getString(R.string.LB_CA_CART_ITEM_FAILED), null);
                        MmProgressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(Response<WishList> response, Retrofit retrofit) {
                        WishList cart = response.body();

                        if (cart != null) {
                            MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                    activity.getString(R.string.LB_CA_DEL_CART_ITEM_SUCCESS), null);
                            ((WishListActivity) activity).loadDataForList(cart);
                            notifyDataSetChanged();
                            CacheManager.getInstance().setWishlist(cart);
                            activity.invalidateOptionsMenu();

                        } else {
                            MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR,
                                    activity.getString(R.string.LB_CA_CART_ITEM_FAILED), null);
                        }
                        MmProgressDialog.dismiss();
                    }
                });
    }

    private void showCheckout(final CartItem cartItem) {
        final Intent intent = new Intent(activity, CheckoutActivity.class);
        intent.putExtra(CheckoutActivity.STYLE_CODE_DATA, cartItem.getStyleCode());
        intent.putExtra(CheckoutActivity.ADD_PRODUCT_TO_CART_FLAG, true);
        intent.putExtra(CheckoutActivity.SELECTED_DATA_KEY, StylesRecord.getSelectionFromWishList(cartItem));
        showCheckoutScreen(intent);
    }

    public void showCheckoutScreen(final Intent intent) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                activity.startActivityForResult(intent, CheckoutActivity.CHECK_OUT_REQUEST);
                activity.overridePendingTransition(R.anim.bottom_to_top, R.anim.not_move);
            }
        });
    }

    private void addToCart(CartItem item) {
        MmProgressDialog.show(activity);

        APIManager.getInstance().getCartService().addCartItem(new CartAddRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKeyForCartRequest(), MmGlobal.getCartKeyForCartRequest(), item.getSkuId(), item.getQty()))
                .enqueue(new MmCallBack<Cart>(activity) {
                    @Override
                    public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                        MmProgressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        super.onFailure(t);
                        MmProgressDialog.dismiss();
                        MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                activity.getString(R.string.LB_CA_ADD2CART_FAIL), null);
                    }

                    @Override
                    public void onResponse(Response<Cart> response, Retrofit retrofit) {
                        Cart cart = response.body();
                        if (cart != null) {
                            CacheManager.getInstance().setCart(cart);
                            MmGlobal.setAnonymousShoppingCartKey(cart.getCartKey());
                            MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                    activity.getString(R.string.LB_CA_ADD2CART_SUCCESS), null);
                            activity.invalidateOptionsMenu();
                        } else {
                            MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                    activity.getString(R.string.LB_CA_ADD2CART_FAIL), null);
                        }
                        MmProgressDialog.dismiss();
                    }
                });
    }

    public static class WishlistItemViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.swipeLayout)
        SwipeLayout swipeLayout;

        @Bind(R.id.product_name)
        public TextView productName;

        @Bind(R.id.product_image)
        ImageView productImage;

        @Bind(R.id.product_brand_image)
        ImageView brandImage;

        @Bind(R.id.product_price)
        TextView productPrice;

        @Bind(R.id.original_price)
        TextView originalPrice;

        @Bind(R.id.linearDelete)
        LinearLayout linearDelete;

        @Bind(R.id.linearAddToCart)
        LinearLayout linearAddToCart;

        @Bind(R.id.add_to_cart_button)
        Button addToCartBtn;

        public WishlistItemViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            ButterKnife.bind(this, itemLayoutView);
        }
    }
}




