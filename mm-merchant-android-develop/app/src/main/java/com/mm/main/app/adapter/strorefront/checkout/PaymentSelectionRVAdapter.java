package com.mm.main.app.adapter.strorefront.checkout;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.listitem.PaymentRvItem;
import com.mm.main.app.schema.Payment;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thienchaud on 26-Feb-16.
 */
public class PaymentSelectionRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    List<PaymentRvItem> items;
    Activity activity;
    OnSelectPaymentListener onSelectPaymentListener;

    public PaymentSelectionRVAdapter(Activity activity, List<PaymentRvItem> items) {
        this.activity = activity;
        this.items = items;
    }

    public void setOnSelectPaymentListener(OnSelectPaymentListener onSelectPaymentListener) {
        this.onSelectPaymentListener = onSelectPaymentListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_selection_item_view, parent, false);
        RecyclerView.ViewHolder viewHolder = new PaymentSelectionViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        PaymentRvItem paymentRvItem = items.get(position);
        PaymentSelectionViewHolder paymentViewHolder = (PaymentSelectionViewHolder) viewHolder;

        paymentViewHolder.parentView.setTag(position);
        paymentViewHolder.parentView.setOnClickListener(this);
        paymentViewHolder.checkbox.setChecked(paymentRvItem.isSelected());
        paymentViewHolder.checkbox.setTag(position);
        paymentViewHolder.checkbox.setOnClickListener(this);

        switch (position) {
            case 0:
                paymentViewHolder.txtName.setText("支付宝");
                break;
            case 1:
                paymentViewHolder.txtName.setText("货到付款");
                break;
        }
    }

    @Override
    public void onClick(View v) {
        int pos = (int) v.getTag();
        processSelectItem(pos, items.get(pos).isSelected());
    }

    void processSelectItem(int pos, boolean isSelected) {
        if (!isSelected) {
            for (int i = 0; i < items.size(); i++) {
                if (i == pos) {
                    items.get(i).setIsSelected(true);
                } else {
                    items.get(i).setIsSelected(false);
                }
            }
        }
        notifyDataSetChanged();
        if (onSelectPaymentListener != null) {
            onSelectPaymentListener.onSelectPayment(items.get(pos).getPayment());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class PaymentSelectionViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.parentView)
        public LinearLayout parentView;

        @Bind(R.id.checkbox)
        public CheckBox checkbox;

        @Bind(R.id.txtName)
        public TextView txtName;

        @Bind(R.id.imgPayment)
        public ImageView imgPayment;

        public PaymentSelectionViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
        }
    }

    public interface OnSelectPaymentListener {
        public void onSelectPayment(Payment payment);
    }
}
