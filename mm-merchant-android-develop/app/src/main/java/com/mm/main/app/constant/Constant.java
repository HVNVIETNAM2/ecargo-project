package com.mm.main.app.constant;

import com.mm.main.app.global.MmGlobal;

/**
 * Constant Storage
 */
public class Constant {
    //local server
//    private static final String HOST = "http://10.1.77.80:3000";
    //AWS-Development server
    public static final String HOST = "http://52.76.112.250";
    public static final String API_PATH = "/api/";
    public static final String PARSE_URL = "https://api.parse.com/1/classes/";
    public static final String REMEMBER = "remember";
    public static final String TOKEN = "token";
    public static final String DEVICE_TOKEN = "deviceToken";
    public static final String USER_ID = "userId";
    public static final String USER_KEY = "userKey";
    public static final String LOGIN_KEY = "loginKey";
    public static final String HOST_KEY = "hostKey";
    public static final String CONFIG_KEY = "configKey";
    public static final String CONFIG_FILE_URL = "config/config.json";
    public static final String SHOPPING_CART_ID = "shoppingCartId";
    public static final String SHOPPING_CART_KEY = "shoppingCartKey";
    public static final String WISHLIST_ID = "wishListId";
    public static final String WISHLIST_KEY = "wishListKey";
    public static final String SEARCH_HISTORY = "searchHistory";
    public static final String LANGUAGE_LIST = "languageList";
    public static final String CURRENT_CULTURE_CODE = "currentCultureCode";
    public static final String USER_NAME = "userName";
    public static final String RESULT = "result";
    public static String LOGIN_IN_USING_APP_KEY = "LOGIN_IN_USING_APP_KEY";
    public static String LOGIN_IN_REQUEST_CODE_KEY = "LOGIN_IN_REQUEST_CODE_KEY";

    public static final String TEXT_REPLACE_FIELD_NAME = "<field name>";
    public static final String TEXT_REPLACE_VALUE_0 = "{0}";
    public static final String APP_ID = "wx5f856e676f618a69";
    public class Extra {
        public static final String EXTRA_USER_DETAIL = "USER_DETAIL";
        public static final String EXTRA_USER_KEY = "UserKey";
        public static final String EXTRA_ACTIVATION_TOKEN = "ActivationToken";
        public static final String EXTRA_LIKED = "liked";
        public static final String EXTRA_SIGNUP_REQUEST = "signupRequest";
    }
    public class UploadImageId {
        public static final String STORE_FRONT_PROFILE_PHOTO = "StorefrontProfilePhoto";
        public static final String ID_PHOTO = "IDPhoto";
    }
    public static String getApiURL() {
        return MmGlobal.getHost() + API_PATH;
    }

    public static int DEMO_FACTOR_FOR_FOLLOWER = 9999;
    public static int MIN_FOLLOWER_COUNT = 3;
    public static int NUM_FOLLOWER_SKIP = 5;
    public static int AUTO_SCROLL_TIMER = 5000;
    public static int BRAND_IMAGE_WIDTH = 680;

    public static String USER_STATUS_ACTIVE = "Active";
    public static String URL_QR_CODE = "https://mymm.com/u/";
    public static String URI_SAVE_CHAT_SOUND = "/mm/voice/";
}