package com.mm.main.app.fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.library.subscaleview.ImageSource;
import com.mm.main.app.library.subscaleview.SubsamplingScaleImageView;
import com.mm.main.app.schema.ImageData;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class PhotoPagerFragment extends Fragment implements SubsamplingScaleImageView.ScaleImageViewListner {

    private static final String BUNDLE_ASSET = "asset";

    public static final int IMAGE_TYPE_PRODUCT = 0;
    public static final int IMAGE_TYPE_PROFILE = 1;

    private ImageData imageData;
    private int imageType;

    public SubsamplingScaleImageView imageView;

    private Target loadtarget;

    public PhotoPagerFragment() {
    }

    public void setImageType(int imageType) {
        this.imageType = imageType;
    }

    public void setImageData(ImageData imageData) {
        this.imageData = imageData;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photo_viewer, container, false);
        if (savedInstanceState != null) {
            if (imageData == null && savedInstanceState.containsKey(BUNDLE_ASSET)) {
                imageData = (ImageData) savedInstanceState.getSerializable(BUNDLE_ASSET);
            }
        }

        loadImage(rootView);

        return rootView;
    }

    private void loadImage(View rootView) {

        imageView = (SubsamplingScaleImageView)rootView.findViewById(R.id.imageView);
        if (loadtarget == null) loadtarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                imageView.setImage(ImageSource.bitmap(bitmap));
                imageView.setMaxScale(5.0f);
                imageView.setDoubleTapZoomScale(5.0f);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        };
        imageView.setScalImageViewListener(this);

        String url = "";
        switch (imageType) {
            case IMAGE_TYPE_PRODUCT:
                url = PathUtil.getProductImageUrl(imageData.getImageKey());
                break;
            case IMAGE_TYPE_PROFILE:
                url = PathUtil.getUserImageUrl(imageData.getImageKey(), UiUtil.getDisplayWidth() / 2);
                break;
        }

        if(!url.equals("")) {
            Picasso.with(MyApplication.getContext()).load(url).into(loadtarget);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        View rootView = getView();
        if (rootView != null) {
            outState.putSerializable(BUNDLE_ASSET, imageData);
        }
    }

    @Override
    public void onDismiss() {
        getActivity().finish();
    }
}