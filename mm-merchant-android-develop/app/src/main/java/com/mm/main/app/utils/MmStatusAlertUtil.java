package com.mm.main.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.activity.storefront.discover.DiscoverMainActivity;
import com.mm.main.app.activity.storefront.product.ProductListActivity;
import com.mm.main.app.R;

/**
 * Created by thienchaud on 28-Jan-16.
 */
public class MmStatusAlertUtil {

    static final int DISMISS_TIME = 1500;

    public enum StatusAlertType {
        StatusAlertType_OK,
        StatusAlertType_ERROR
    }

    public interface OnStatusAlertDismissListener {
        void onDismiss();
    }

    public static void show(Context context, StatusAlertType statusAlertType, String content, final OnStatusAlertDismissListener listener) {
        if (context == null) {
            return;
        }
        if (context instanceof ProductListActivity ||
                context instanceof DiscoverMainActivity) {
            context = ((AppCompatActivity) context).getParent();
        }

        final Dialog dialog = new Dialog(context, R.style.AlertStatusTheme);
        dialog.setContentView(R.layout.alert_status);

        ImageView imgIcon = (ImageView)dialog.findViewById(R.id.imgIcon);
        switch (statusAlertType) {
            case StatusAlertType_OK:
                imgIcon.setImageResource(R.drawable.icon_alert_ok);
                break;
            case StatusAlertType_ERROR:
                imgIcon.setImageResource(R.drawable.icon_alert_error);
                break;
        }
        TextView txtContent = (TextView) dialog.findViewById(R.id.txtContent);
        txtContent.setText(content);

        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        };

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
                if (listener != null) {
                    listener.onDismiss();
                }
            }
        });

        dialog.show();
        handler.postDelayed(runnable, DISMISS_TIME);
    }

    public static void showLongMsg(Context context, StatusAlertType statusAlertType, String content, final OnStatusAlertDismissListener listener) {
        if (context == null) {
            return;
        }

        final Dialog dialog = new Dialog(context, R.style.AlertStatusTheme);
        dialog.setContentView(R.layout.alert_status_long_msg);

        ImageView imgIcon = (ImageView)dialog.findViewById(R.id.imgIcon);
        switch (statusAlertType) {
            case StatusAlertType_OK:
                imgIcon.setImageResource(R.drawable.icon_alert_ok);
                break;
            case StatusAlertType_ERROR:
                imgIcon.setImageResource(R.drawable.icon_alert_error);
                break;
        }
        TextView txtContent = (TextView) dialog.findViewById(R.id.txtContent);
        txtContent.setText(content);

        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        };

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
                if (listener != null) {
                    listener.onDismiss();
                }
            }
        });

        dialog.show();
        handler.postDelayed(runnable, DISMISS_TIME + 500);
    }

}
