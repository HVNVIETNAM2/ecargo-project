package com.mm.main.app.helper;

import com.mm.main.app.schema.Aggregations;
import com.mm.main.app.schema.Badge;
import com.mm.main.app.schema.Brand;
import com.mm.main.app.schema.Category;
import com.mm.main.app.schema.CategoryPath;
import com.mm.main.app.schema.Color;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.Size;
import com.mm.main.app.schema.Style;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by henrytung on 9/12/2015.
 */
public class ProductFilterHelper {

    public static List<Style> fullStyleList = null;
    public static Aggregations aggregations = null;

    public static List<Style> filterStyleByCategoryId(List<Style> fullStyleList, int level, int categoryId) {
        List<Style> filteredStyleList = new ArrayList<>();

        for (Style style : fullStyleList) {
            List<CategoryPath> primaryCategoryPathList = style.getPrimaryCategoryPathList();
            if (primaryCategoryPathList != null) {
                for (int i = 0; i < primaryCategoryPathList.size(); i++) {
                    CategoryPath categoryPath = primaryCategoryPathList.get(i);
                    if (categoryPath.getLevel() == level && categoryPath.getCategoryId() == categoryId) {
                        filteredStyleList.add(style);
                    }
                }

            }

        }

        return filteredStyleList;
    }

    public static List<Badge> filterBadgeId(List<Badge> fullBadgeList, List<Integer> badgeArray) {
        List<Badge> filteredList = new ArrayList<>();

        for (int i = 0; i < badgeArray.size(); i++) {
            Badge badge = getBadgeById(fullBadgeList, badgeArray.get(i));
            if (badge != null) {
                filteredList.add(badge);
            }
        }

        return filteredList;
    }

    public static List<Category> filterCategoryId(List<Category> fullCategoryList, List<Integer> categoryArray) {
        List<Category> filteredList = new ArrayList<>();

        for (int i = 0; i < categoryArray.size(); i++) {
            Category category = getCategoryById(fullCategoryList, categoryArray.get(i));
            if (category != null) {
                filteredList.add(category);
            }
        }

        return filteredList;
    }

    public static List<Size> filterSizeId(List<Size> fullList, List<Integer> itemArray) {
        List<Size> filteredList = new ArrayList<>();

        for (int i = 0; i < itemArray.size(); i++) {
            Size item = getSizeById(fullList, itemArray.get(i));
            if (item != null) {
                filteredList.add(item);
            }
        }

        return filteredList;
    }

    public static List<Color> filterColorId(List<Color> fullList, List<Integer> itemArray) {
        List<Color> filteredList = new ArrayList<>();

        for (int i = 0; i < itemArray.size(); i++) {
            Color item = getColorById(fullList, itemArray.get(i));
            if (item != null) {
                filteredList.add(item);
            }
        }

        return filteredList;
    }

    public static List<Brand> filterBrandId(List<Brand> fullList, List<Integer> itemArray) {
        List<Brand> filteredList = new ArrayList<>();

        for (int i = 0; i < itemArray.size(); i++) {
            Brand item = getBrandById(fullList, itemArray.get(i));
            if (item != null) {
                filteredList.add(item);
            }
        }

        return filteredList;
    }

    public static List<Merchant> filterMerchantId(List<Merchant> fullList, List<Integer> itemArray) {
        List<Merchant> filteredList = new ArrayList<>();

        for (int i = 0; i < itemArray.size(); i++) {
            Merchant item = getMerchantById(fullList, itemArray.get(i));
            if (item != null) {
                filteredList.add(item);
            }
        }

        return filteredList;
    }


    private static Brand getBrandById(List<Brand> fullList, int id) {
        for (Brand brand : fullList) {
            if (brand.getBrandId() == id) {
                return brand;
            }
        }

        return null;
    }

    private static Size getSizeById(List<Size> fullList, int id) {
        for (Size item : fullList) {
            if (item.getSizeId() == id) {
                return item;
            }

        }

        return null;
    }


    private static Color getColorById(List<Color> fullList, int id) {
        for (Color item : fullList) {
            if (item.getColorId() == id) {
                return item;
            }

        }

        return null;
    }

    private static Category getCategoryById(List<Category> fullList, int id) {
        for (Category item : fullList) {
            if (item.getCategoryId() == id) {
                return item;
            }

        }

        return null;
    }

    private static Badge getBadgeById(List<Badge> fullList, int id) {
        for (Badge item : fullList) {
            if (item.getBadgeId() == id) {
                return item;
            }

        }

        return null;
    }

    private static Merchant getMerchantById(List<Merchant> fullList, int id) {
        for (Merchant item : fullList) {
            if (item.getMerchantId() == id) {
                return item;
            }

        }

        return null;
    }

    public static List<Style> filterStyleByCategoryId(List<Style> fullStyleList, int categoryId) {
        return filterStyleByCategoryId(fullStyleList, 2, categoryId);
    }

    public static List<Style> filterStyleByPrimaryCategoryId(List<Style> fullStyleList, int categoryId) {
        return filterStyleByCategoryId(fullStyleList, 1, categoryId);
    }

    public static List<Style> getFullStyleList() {
        return fullStyleList;
    }

    public static void setFullStyleList(List<Style> fullStyleList) {
        ProductFilterHelper.fullStyleList = fullStyleList;
    }

    public static Aggregations getAggregations() {
        return aggregations;
    }

    public static void setAggregations(Aggregations aggregations) {
        ProductFilterHelper.aggregations = aggregations;
    }
}
