package com.mm.main.app.adapter.strorefront.setting;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.setting.UserProfileSettingActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.UserProfileSettingListItem;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nhutphan on 2/26/2016.
 */
public class UserProfileSettingRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity context;
    private List<UserProfileSettingListItem> itemsData;

    public Bitmap getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(Bitmap bitmap) {
        this.userPhoto = bitmap;
    }

    private Bitmap userPhoto;
    private int settingMarginLeft = UiUtil.getPixelFromDp(15);
    private int defaultTextSize = 14;
    private int heightOfDividers = 10;

    public UserProfileSettingRvAdapter(Activity context, List<UserProfileSettingListItem> itemsData){
        this.context = context;
        this.itemsData = itemsData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        UserProfileSettingListItem.ItemType itemType = UserProfileSettingListItem.ItemType.values()[viewType];
        RecyclerView.ViewHolder viewHolder = null;

        switch (itemType) {
            case TYPE_USER_INFO:
                viewHolder = new ViewHolderUserProfileInfo(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.user_profile_setting_info_view, viewGroup, false));
                break;
            case TYPE_TEXT_ARROW:
                viewHolder = new ViewHolderTextArrow(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.setting_text_item_arrow, viewGroup, false));
                break;
            case TYPE_DIVIDER:
                viewHolder = new ViewHolderDivider(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.divider_item, viewGroup, false));
                viewHolder.itemView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, UiUtil.getPixelFromDp(heightOfDividers)));
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        UserProfileSettingListItem settingItem = itemsData.get(position);

        switch (settingItem.getType()){
            case TYPE_USER_INFO:
                ViewHolderUserProfileInfo viewHolderUserProfileInfo = (ViewHolderUserProfileInfo) viewHolder;
                bindUserInfoView(settingItem, viewHolderUserProfileInfo);
                break;
            case TYPE_TEXT_ARROW:
                ViewHolderTextArrow viewHolderTextArrow = (ViewHolderTextArrow) viewHolder;
                bindTextArrowView(settingItem, viewHolderTextArrow);
                break;
        }
    }

    private void bindUserInfoView(UserProfileSettingListItem settingItem, final ViewHolderUserProfileInfo viewHodlerUserProfileInfo){
        String userName = settingItem.getUser().getDisplayName();
        String fullName = settingItem.getUser().getFirstName() + " " + settingItem.getUser().getLastName();
        String profileImage = PathUtil.getUserImageUrl(settingItem.getUser().getProfileImage());

        viewHodlerUserProfileInfo.tvUserName.setText(userName);
        viewHodlerUserProfileInfo.tvDisplayName.setText(fullName);
        viewHodlerUserProfileInfo.tvGenderTitle.setText(context.getString(R.string.LB_CA_GENDER));
        viewHodlerUserProfileInfo.tvGenderContent.setText("Male");
        viewHodlerUserProfileInfo.tvGenderTitle.setLayoutParams(getTextViewLP(settingMarginLeft, 0, 0, 0));

        setTextSize(viewHodlerUserProfileInfo.tvGenderTitle, defaultTextSize);
        setTextSize(viewHodlerUserProfileInfo.tvGenderContent, defaultTextSize);

        if (getUserPhoto() == null) {
            Picasso.with(MyApplication.getContext()).load(profileImage)
                    .placeholder(R.drawable.placeholder).into(viewHodlerUserProfileInfo.civProfilePhoto);

        } else {
            viewHodlerUserProfileInfo.civProfilePhoto.setImageBitmap(userPhoto);
        }
    }

    private void bindTextArrowView(UserProfileSettingListItem settingItem, ViewHolderTextArrow viewHolderTextArrow){
        viewHolderTextArrow.tvSettingTitle.setLayoutParams(getTextViewLP(settingMarginLeft, 0, 0, 0));
        viewHolderTextArrow.tvSettingTitle.setText(settingItem.getTitle());
        viewHolderTextArrow.tvSettingContent.setText(settingItem.getContent());

        setTextSize(viewHolderTextArrow.tvSettingTitle, defaultTextSize);
        setTextSize(viewHolderTextArrow.tvSettingContent, defaultTextSize);
    }

    private void setTextSize(TextView tv, int size){
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    private RelativeLayout.LayoutParams getTextViewLP(int left, int top, int right, int bottom){
        RelativeLayout.LayoutParams rLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        rLayoutParams.setMargins(left, top, right, bottom);
        return rLayoutParams;
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return itemsData.get(position).getType().ordinal();
    }

    public UserProfileSettingListItem getItem(int position) {
        return itemsData.get(position);
    }

    public class ViewHolderUserProfileInfo extends RecyclerView.ViewHolder{
        @Bind(R.id.tvUserName)
        TextView tvUserName;

        @Bind(R.id.tvDisplayName)
        TextView tvDisplayName;

        @Bind(R.id.civProfilePhoto)
        CircleImageView civProfilePhoto;

        @Bind(R.id.setting_title)
        TextView tvGenderTitle;

        @Bind(R.id.setting_content)
        TextView tvGenderContent;

        public ViewHolderUserProfileInfo(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
        }

        @OnClick(R.id.civProfilePhoto)
        public void onClickPhoto(){
            if(context instanceof UserProfileSettingActivity){
                ((UserProfileSettingActivity) context).onClickUserPhoto();
            }
        }
    }

    public class ViewHolderTextArrow extends RecyclerView.ViewHolder{

        @Bind(R.id.setting_title)
        TextView tvSettingTitle;

        @Bind(R.id.setting_content)
        TextView tvSettingContent;

        public ViewHolderTextArrow(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
        }
    }

    public class ViewHolderDivider extends RecyclerView.ViewHolder{
        public ViewHolderDivider (View itemLayoutView){
            super(itemLayoutView);
        }
    }
}
