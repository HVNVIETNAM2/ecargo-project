package com.mm.main.app.schema;

import java.io.Serializable;

/**
 * Created by thienchaud on 19-Feb-16.
 */
public class Address implements Serializable {

    String UserAddressKey;
    String RecipientName;
    String PhoneCode;
    String PhoneNumber;
    Integer GeoCountryId;
    Integer GeoProvinceId;
    Integer GeoCityId;
    String Country;
    String Province;
    String City;
    String District;
    String PostalCode;
    String Address;
    String CultureCode;
    String LastModified;
    String LastCreated;
    Integer IsDefault;


    public String getRecipientName() {
        return RecipientName;
    }

    public void setRecipientName(String recipientName) {
        RecipientName = recipientName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public Integer getIsDefault() {
        return IsDefault;
    }

    public String getPhoneCode() {
        return PhoneCode;
    }

    public String getAddress() {
        return Address;
    }

    public String getCity() {
        return City;
    }

    public String getProvince() {
        return Province;
    }

    public String getCountry() {
        return Country;
    }

    public String getFullPhoneNumber() {
        return "(" + PhoneCode.substring(1) + ") " + PhoneNumber;
    }

    public String getFullAddress() {
        return Address + ", " + City + ", " + Province + ", " + Country;
    }

    public String getUserAddressKey() {
        return UserAddressKey;
    }
}
