package com.mm.main.app.activity.storefront.filter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.SearchView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.filter.ColorFilterSelectionRVAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.decoration.GridSpacingItemDecoration;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Color;
import com.mm.main.app.utils.UiUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ColorFilterSelectionActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{

    public static final String EXTRA_HEADER = "extra_header";
    public static final String EXTRA_SELECTIONS = "selection";
    public static final String EXTRA_SELECTED = "selected";
    private static final int GRID_PADDING_IN_DP = 60;
    private static final int ITEM_SIZE_IN_DP = 69;

    public static final String EXTRA_RESULT = "result";
    public static final String EXTRA_RESULT_DATA = "resultData";

    @Bind(R.id.recyclerViewFilter)
    RecyclerView recyclerViewFilter;

    @Bind(R.id.searchView)
    SearchView searchView;
    List<FilterListItem<Color>> filterList;

    @Bind(R.id.textViewHeader)
    TextView textViewHeader;

    ColorFilterSelectionRVAdapter listAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeNoActionBar);
        setContentView(R.layout.activity_filter_color_selection);
        ButterKnife.bind(this);
        setupToolbar();

        Bundle bundle = (Bundle)getIntent().getBundleExtra(EXTRA_SELECTIONS);
        renderFilterList((List<FilterListItem<Color>>) bundle.getSerializable(FilterActivity.EXTRA_COLOR_DATA));
//        ActivityUtil.setTitle(this, getIntent().getStringExtra(EXTRA_HEADER));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        textViewHeader.setText(getIntent().getStringExtra(EXTRA_HEADER));
        setupSearchView();

//        searchView.setIconifiedByDefault(false);

        recyclerViewFilter.setLayoutManager(new GridLayoutManager(this, UiUtil.getNumberColumnGrid(GRID_PADDING_IN_DP, ITEM_SIZE_IN_DP)));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void finish() {
        super.finish();
    }

    private void renderFilterList(List<FilterListItem<Color>> filters) {
        filterList = filters;
        listAdapter = new ColorFilterSelectionRVAdapter(this, filterList, -1);
        recyclerViewFilter.setAdapter(listAdapter);

        int spacingInPixels = MyApplication.getContext().getResources().getDimensionPixelSize(R.dimen.color_filter_spacing);
        recyclerViewFilter.addItemDecoration(new GridSpacingItemDecoration(UiUtil.getNumberColumnGrid(GRID_PADDING_IN_DP, ITEM_SIZE_IN_DP), spacingInPixels, true));

        listAdapter.notifyDataSetChanged();
    }

    private void setupSearchView() {
//        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setSubmitButtonEnabled(true);
//        searchView.setQueryHint("<span id="IL_AD7" class="IL_AD">Search</span> Here");
    }


    private void proceedOk() {
        Intent intent = this.getIntent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_RESULT_DATA, createResultList());
        intent.putExtra(EXTRA_RESULT, bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    private ArrayList<Color> createResultList() {
        List<FilterListItem<Color>> filterListItemList = listAdapter.getOriginalData();
        ArrayList<Color> resultList = new ArrayList<>();

        for (int i = 0; i < filterListItemList.size(); i++) {
            if (filterListItemList.get(i).isSelected()) {
                resultList.add(filterListItemList.get(i).getT());
            }
        }

        return resultList;
    }

    private int convertToOriginalPosition(int position) {
        Color color = (Color)listAdapter.getItem(position);

        for (int i = 0; i < filterList.size(); i++) {
            if (color.getColorCode().equals(filterList.get(i).getT().getColorCode())) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_brand_filter_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_ok) {
            proceedOk();
        }

        if (id == R.id.action_reset) {
            resetSelection();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listAdapter.getFilter().filter(newText);
        return true;
    }

    @OnClick(R.id.buttonOk)
    public void okButtonClick() {
        proceedOk();
    }

    private void resetSelection() {

        for (int i = 0; i < filterList.size(); i++) {
            filterList.get(i).setSelected(false);
        }
        listAdapter.notifyDataSetChanged();
    }


}
