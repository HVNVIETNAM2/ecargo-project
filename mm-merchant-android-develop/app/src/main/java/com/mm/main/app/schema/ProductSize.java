
package com.mm.main.app.schema;

import java.io.Serializable;

public class ProductSize implements Serializable{

    private Integer SizeId;
    private String SizeName;

    /**
     * 
     * @return
     *     The SizeId
     */
    public Integer getSizeId() {
        return SizeId;
    }

    /**
     * 
     * @param SizeId
     *     The SizeId
     */
    public void setSizeId(Integer SizeId) {
        this.SizeId = SizeId;
    }

    /**
     * 
     * @return
     *     The SizeName
     */
    public String getSizeName() {
        return SizeName;
    }

    /**
     * 
     * @param SizeName
     *     The SizeName
     */
    public void setSizeName(String SizeName) {
        this.SizeName = SizeName;
    }

}
