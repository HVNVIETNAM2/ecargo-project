
package com.mm.main.app.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CategoryList implements Serializable {

    private List<com.mm.main.app.schema.CategoryPathList> CategoryPathList = new ArrayList<com.mm.main.app.schema.CategoryPathList>();
    private Integer Priority;

    /**
     * 
     * @return
     *     The CategoryPathList
     */
    public List<com.mm.main.app.schema.CategoryPathList> getCategoryPathList() {
        return CategoryPathList;
    }

    /**
     * 
     * @param CategoryPathList
     *     The CategoryPathList
     */
    public void setCategoryPathList(List<com.mm.main.app.schema.CategoryPathList> CategoryPathList) {
        this.CategoryPathList = CategoryPathList;
    }

    /**
     * 
     * @return
     *     The Priority
     */
    public Integer getPriority() {
        return Priority;
    }

    /**
     * 
     * @param Priority
     *     The Priority
     */
    public void setPriority(Integer Priority) {
        this.Priority = Priority;
    }

}
