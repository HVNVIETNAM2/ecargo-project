package com.mm.main.app.service;

import com.mm.main.app.schema.Address;
import com.mm.main.app.schema.request.AddressSaveRequest;
import com.mm.main.app.schema.request.DefaultAddressSaveRequest;
import com.mm.main.app.schema.request.OrderCreateRequest;
import com.mm.main.app.schema.response.Order;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by ductran on 3/7/2016.
 */
public interface OrderService {
    String ORDER_PATH = "order";

    @POST(ORDER_PATH + "/create")
    Call<Order> createOrder(@Body OrderCreateRequest createOrderRequest);
}
