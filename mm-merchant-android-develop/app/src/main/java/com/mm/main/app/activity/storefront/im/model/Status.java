package com.mm.main.app.activity.storefront.im.model;

/**
 * Created by madhur on 17/01/15.
 */
public enum Status {
    SENT,
    DELIVERED
}
