package com.mm.main.app.adapter.merchant.user;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.schema.User;

import java.util.List;

/**
 * Created by andrew on 30/9/15.
 */
public class UserListAdapter extends ArrayAdapter<User> {

    private final Activity context;
    private final List<User> userList;

    public UserListAdapter(Activity context, List<User> userList) {
        super(context, R.layout.user_list_item, userList);
        this.context = context;
        this.userList = userList;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.user_list_item, null, true);
        }
        TextView textView = (TextView) view.findViewById(R.id.txt);
        ImageView imageView = (ImageView) view.findViewById(R.id.img);
        textView.setText(userList.get(position).getFirstName() + " " + userList.get(position).getLastName());
        imageView.setImageResource(R.drawable.logo);
        return view;
    }
}