package com.mm.main.app.schema;

//import com.activeandroid.Model;
//import com.activeandroid.annotation.Column;
//import com.activeandroid.annotation.Table;

/**
 * Schema class for token
 */
//@Table(name = "Tokens")
public class Token {
//public class Token extends Model {
//    @Column(name = "Token", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String Token;
    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public Token(){
        super();
    }
}
