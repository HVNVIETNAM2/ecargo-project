package com.mm.main.app.service;

import com.mm.main.app.schema.Country;
import com.mm.main.app.schema.Geo;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by andrew on 30/9/15.
 */
public interface GeoService {

    String GEO_PATH = "geo";

    @GET(GEO_PATH + "/country")
    Call<List<Country>> listCountry();

    @GET(GEO_PATH + "/province")
    Call<List<Geo>> listProvince(@Query("q") String geoCountryId);

    @GET(GEO_PATH + "/city")
    Call<List<Geo>> listCity(@Query("q") String geoId);

    @GET(GEO_PATH + "/country/storefront")
    Call<List<Country>> storefrontCountry();
}
