package com.mm.main.app.layout;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.uicomponent.CustomSlidingDrawer;
import com.mm.main.app.utils.UiUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by haivu on 1/26/16.
 */
public class RequestSmsSlidingDrawer extends FrameLayout {


    public interface SignUpCallBack {
        void startCountDown();

        void endCountDown();
    }


    private SignUpCallBack callBack;

    @Bind(R.id.image1)
    ImageView image1;

    @Bind(R.id.image2)
    ImageView image2;

    @Bind(R.id.countDownTextView)
    TextView countDownTextView;

    @Bind(R.id.slidingDrawer1)
    CustomSlidingDrawer slidingDrawer;

    @Bind(R.id.handle)
    ImageView handle;

    @Bind(R.id.content)
    ImageView content;

    @Bind(R.id.backgroundNotAlpha)
    ImageView backgroundNotAlpha;

    @Bind(R.id.backgroundFrontText)
    ImageView backgroundFrontText;

    @Bind(R.id.backgroundSliding)
    ImageView backgroundSliding;

    boolean isLocked = false;
    boolean needOpen = true;

    private static float percentSwipeToOpen = 0.3f;
    CountDownTimer countDownTimer;

    public RequestSmsSlidingDrawer(Context context) {
        super(context);

        initView();
    }

    public RequestSmsSlidingDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public RequestSmsSlidingDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    private void initView() {
        View view = inflate(getContext(), R.layout.request_sms_sliding_drawer, null);
        addView(view);
        ButterKnife.bind(this);

        backgroundSliding.setAlpha(0.1f);
        backgroundFrontText.setAlpha(0.1f);
        backgroundNotAlpha.getLayoutParams().width = 0;
        backgroundFrontText.getLayoutParams().width = 0;
    }


    public void updateCountDown(String text, long min) {
        countDownTextView.setText(text + min + ")");
    }

    public void setTextDisplay(String text) {
        countDownTextView.setText(text);
    }

    public void lockSliding() {
        slidingDrawer.lock();
        isLocked = true;
    }

    public void unlockSliding() {
        slidingDrawer.unlock();
        Log.d("SLD", "unlock");
        isLocked = false;
    }

    public void bindRequestSmsDrawer(final int countDowntime, final String notifyText, SignUpCallBack callBack) {

        this.callBack = callBack;
        slidingDrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {
            @Override
            public void onDrawerOpened() {
                if (!needOpen) {
                    slidingDrawer.close();
                    resetView();
                }
            }
        });

        slidingDrawer.setTouchUpListener(new CustomSlidingDrawer.OnTouchUpListener() {
            @Override
            public void onTouchUp() {
                resetView();
            }
        });

        slidingDrawer.setOnChangeListener(
                new CustomSlidingDrawer.OnChangeListener() {
                    @Override
                    public void onChange(float x) {
                        if (isLocked || slidingDrawer.isOpened())
                            return;

                        float position = handle.getLeft();
                        float alpha = Math.max(0.1f, 1 - (position / (slidingDrawer.getWidth() - handle.getWidth())));
                        int width = (int) Math.min(slidingDrawer.getWidth(),
                                (slidingDrawer.getWidth() - x + UiUtil.getPixelFromDp(20)));
                        backgroundSliding.setAlpha(alpha);
                        backgroundFrontText.setAlpha(alpha);
                        countDownTextView.setAlpha(1.0f - alpha);

                        ViewGroup.LayoutParams params = backgroundNotAlpha.getLayoutParams();
                        params.width = width;
                        backgroundFrontText.setLayoutParams(params);
                        ViewGroup.LayoutParams paramsFront = backgroundFrontText.getLayoutParams();
                        paramsFront.width = width;
                        backgroundNotAlpha.setLayoutParams(paramsFront);
                    }
                });

        slidingDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {
            @Override
            public void onDrawerClosed() {
                resetView();
            }
        });

        slidingDrawer.setOnDrawerScrollListener(new SlidingDrawer.OnDrawerScrollListener() {
            @Override
            public void onScrollStarted() {
                needOpen = false;
                handle.setImageResource(R.drawable.circle_request_sms_sliding);
                image1.setVisibility(View.GONE);
                image2.setVisibility(GONE);
                content.setVisibility(View.GONE);
                backgroundSliding.setVisibility(VISIBLE);
                countDownTextView.setTextColor(getResources().getColor(R.color.secondary2));
                backgroundFrontText.setVisibility(VISIBLE);
                backgroundNotAlpha.setVisibility(VISIBLE);

                countDownTextView.setAlpha(1f);
            }

            @Override
            public void onScrollEnded() {

                int position = (handle.getLeft() + handle.getRight()) / 2;
                // 70% but view rotated 180o => < 45%
                if (position < image1.getRight() * percentSwipeToOpen) {
                    needOpen = true;
                    countDown(countDowntime);
                    backgroundFrontText.setAlpha(1f);
                    backgroundSliding.setAlpha(1f);
                    countDownTextView.setTextColor(getResources().getColor(R.color.white));
                    countDownTextView.setAlpha(1f);
                    countDownTextView.setText(notifyText);
                    backgroundFrontText.setVisibility(GONE);
                    backgroundNotAlpha.setVisibility(GONE);
                    //slidingDrawer.lock();
                    lockSliding();
                } else {
                    needOpen = false;
                    resetView();
                }
            }
        });
    }

    public void countDown(int countDowntime) {
        callBack.startCountDown();
        new CountDownTimer((countDowntime + 1) * 1000, 1000) {
            public void onTick(long millisUntilFinished) {

                updateCountDown(getResources().getString(R.string.LB_CA_SR_REQUEST_VERCODE_SENT), (millisUntilFinished / 1000) - 1);
            }

            public void onFinish() {
                resetView();
                callBack.endCountDown();
            }
        }.start();
    }


    public void resetView() {

        backgroundFrontText.setAlpha(0.1f);
        backgroundSliding.setAlpha(0.1f);
        countDownTextView.setText(getResources().getString(R.string.LB_CA_SR_REQUEST_VERCODE));

        ViewGroup.LayoutParams params = backgroundNotAlpha.getLayoutParams();
        params.width = 0;
        backgroundFrontText.setLayoutParams(params);
        ViewGroup.LayoutParams paramsFront = backgroundFrontText.getLayoutParams();
        paramsFront.width = 0;
        backgroundNotAlpha.setLayoutParams(paramsFront);

        countDownTextView.setTextColor(getResources().getColor(R.color.secondary2));
        image1.setVisibility(View.VISIBLE);
        image2.setVisibility(View.VISIBLE);
        backgroundFrontText.setVisibility(GONE);
        backgroundNotAlpha.setVisibility(GONE);
        handle.setImageResource(R.drawable.transparent_box);
        content.setImageResource(R.drawable.transparent_box);
        backgroundSliding.setVisibility(GONE);


        //slidingDrawer.unlock();
        unlockSliding();
        if (slidingDrawer.isOpened()) {
            slidingDrawer.close();
        }
    }
}
