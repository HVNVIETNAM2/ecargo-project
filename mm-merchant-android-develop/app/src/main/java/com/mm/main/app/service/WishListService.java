package com.mm.main.app.service;

import com.mm.main.app.schema.CartMergeRequest;
import com.mm.main.app.schema.CartUserUpdate;
import com.mm.main.app.schema.WishList;
import com.mm.main.app.schema.request.CartCreateRequest;
import com.mm.main.app.schema.request.CartItemRemoveRequest;
import com.mm.main.app.schema.request.WishListItemAddRequest;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by ductranvt on 1/26/2016.
 */
public interface WishListService {
    String WISH_LIST_PATH = "wishlist";

    @GET(WISH_LIST_PATH + "/view")
    Call<WishList> viewWishList(@Query("cartkey") String cartKey);

    @GET(WISH_LIST_PATH + "/view/user")
    Call<WishList> viewWishListByUser(@Query("userkey") String userKey);

    @POST(WISH_LIST_PATH + "/create")
    Call<WishList> createWishList(@Body CartCreateRequest cartCreateRequest);

    @POST(WISH_LIST_PATH + "/user/update")
    Call<WishList> updateWishListUser(@Body CartUserUpdate cartUserUpdateRequest);

    @POST(WISH_LIST_PATH + "/item/remove")
    Call<WishList> removeWishListItem(@Body CartItemRemoveRequest cartRemoveRequest);

    @POST(WISH_LIST_PATH + "/item/add")
    Call<WishList> addWishListItem(@Body WishListItemAddRequest cartAddRequest);

    @POST(WISH_LIST_PATH + "/merge")
    Call<WishList> mergeWishlist(@Body CartMergeRequest cartMergeRequest);
}
