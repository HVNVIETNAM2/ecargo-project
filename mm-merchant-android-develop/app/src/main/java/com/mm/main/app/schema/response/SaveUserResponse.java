package com.mm.main.app.schema.response;

/**
 * Schema class for token
 */
public class SaveUserResponse {

    String Token;
    com.mm.main.app.schema.User User;

    public SaveUserResponse(String token, com.mm.main.app.schema.User user) {
        Token = token;
        User = user;
    }

    public SaveUserResponse() {
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public com.mm.main.app.schema.User getUser() {
        return User;
    }

    public void setUser(com.mm.main.app.schema.User user) {
        User = user;
    }
}
