package com.mm.main.app.schema.request;

/**
 * Created by andrew on 10/11/15.
 */
public class ActivateRequest {
    public String getActivationToken() {
        return ActivationToken;
    }

    public void setActivationToken(String activationToken) {
        ActivationToken = activationToken;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public ActivateRequest(String activationToken, String userKey, String password) {

        ActivationToken = activationToken;
        UserKey = userKey;
        Password = password;
    }

    public ActivateRequest() {

    }

    String ActivationToken;
    String UserKey;
    String Password;


}
