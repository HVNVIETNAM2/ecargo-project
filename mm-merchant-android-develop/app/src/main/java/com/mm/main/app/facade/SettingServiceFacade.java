package com.mm.main.app.facade;

import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.response.SaveUserResponse;
import com.mm.main.app.schema.User;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by henrytung on 27/10/15.
 */
public class SettingServiceFacade {

    public interface SettingServiceCallBack {
        void onSuccess(Object content);

        void onFailure(String message);
    }


    public static void update(User user, final SettingServiceCallBack settingServiceCallBack) throws InterruptedException {
        APIManager.getInstance().getUserService().save(user)
                .enqueue(new Callback<SaveUserResponse>() {
                    @Override
                    public void onResponse(Response<SaveUserResponse> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            settingServiceCallBack.onSuccess(response.body().getUser());
                        } else {
                            Logger.i("Unit Test", response.toString());
                            settingServiceCallBack.onFailure(response.toString());
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        settingServiceCallBack.onFailure(t.getMessage());
                    }
                });
    }
}


