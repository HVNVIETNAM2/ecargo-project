package com.mm.main.app.uicomponent;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by nhutphan on 3/8/2016.
 */
public class CustomScannerView extends ZXingScannerView {

    private Rect mRect;

    public CustomScannerView(Context context) {
        super(context);
    }

    public CustomScannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setRect(Rect mRect){
        this.mRect = mRect;
    }

    public Rect getRect(){
        return mRect;
    }
}
