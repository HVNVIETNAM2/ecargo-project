package com.mm.main.app.schema.response;

/**
 * Created by henrytung on 27/10/15.
 */
public class LoginResponse {
    String Token;
    Integer UserId;
    String UserKey;

    public Boolean getSignup() {
        return IsSignup;
    }

    public void setSignup(Boolean signup) {
        IsSignup = signup;
    }

    Boolean IsSignup;

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer userId) {
        UserId = userId;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }
}
