package com.mm.main.app.adapter.merchant.setting;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.schema.Country;

import java.util.ArrayList;
import java.util.List;

public class CountrySelectListAdapter extends BaseAdapter implements Filterable {

    private final Activity context;
    private final List<Country> itemList;
    private Drawable rightIcon;

    private List<Country> filteredData;
    private ItemFilter mFilter = new ItemFilter();

    public CountrySelectListAdapter(Activity context, List<Country> itemList) {
        this.context = context;
        this.itemList = itemList;
        this.filteredData = itemList;
        rightIcon = context.getResources().getDrawable(R.drawable.ico_tick);
    }

    public List<Country> getFilteredData() {
        return filteredData;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Country currentItem = filteredData.get(position);
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.mobile_conutry_selection_list_item, null, true);
        }
        TextView labelTextView = (TextView) convertView.findViewById(R.id.label);
        labelTextView.setText(currentItem.getGeoCountryName());

        TextView valueTextView = (TextView) convertView.findViewById(R.id.value);
        valueTextView.setText(currentItem.getMobileCode());

        return convertView;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString;
            filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<Country> list = itemList;

            int count = list.size();
            final ArrayList<Country> nlist = new ArrayList<>(count);

            String filterableString ;

            if (!TextUtils.isEmpty(constraint)) {
                for (int i = 0; i < count; i++) {
                    filterableString = "" + list.get(i).getGeoCountryName();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        Country mYourCustomData = list.get(i);
                        nlist.add(mYourCustomData);
                    }
                }
            } else {
                for (int i = 0; i < count; i++) {
                    Country mYourCustomData = list.get(i);
                    nlist.add(mYourCustomData);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Country>) results.values;

            if(results.count > 0)
            {
                notifyDataSetChanged();
            }else{
                notifyDataSetInvalidated();
            }

        }
    }

}
