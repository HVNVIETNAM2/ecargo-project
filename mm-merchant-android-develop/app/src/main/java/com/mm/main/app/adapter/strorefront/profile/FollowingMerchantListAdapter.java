package com.mm.main.app.adapter.strorefront.profile;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.filter.MerchantFilterSelectionListAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.Follow;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.utils.PathUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by tuuphungd on 3/8/2016.
 */
public class FollowingMerchantListAdapter extends MerchantFilterSelectionListAdapter implements View.OnClickListener {
    private HashMap<Integer, Boolean> followStatusList;


    public FollowingMerchantListAdapter(Activity context, List<FilterListItem<Merchant>> itemList) {
        super(context, itemList, 0);
        mapFollowStatusList(itemList);
    }

    private void mapFollowStatusList(List<FilterListItem<Merchant>> itemList) {
        followStatusList = new HashMap<>();
        for (FilterListItem<Merchant> item : itemList) {
            followStatusList.put(item.getT().getMerchantId(), true);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Merchant currentItem = filteredData.get(position).getT();
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.following_merchant_item, null, true);
            ((ViewGroup)convertView).setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            convertView.findViewById(R.id.btnFollow).setOnClickListener(this);
        }

        ImageView ivMerchantLogo = (ImageView) convertView.findViewById(R.id.ivMerchantLogo);
        String url = PathUtil.getMerchantImageUrl(currentItem.getSmallLogoImage());
        Picasso.with(MyApplication.getContext()).load(url).into(ivMerchantLogo);

        TextView textView = (TextView) convertView.findViewById(R.id.tvName1);
        if (!TextUtils.isEmpty(currentItem.getMerchantNameInvariant())) {
            textView.setText(currentItem.getMerchantNameInvariant());
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }

        TextView textView2 = (TextView) convertView.findViewById(R.id.tvName2);
        textView2.setText(currentItem.getMerchantName());

        TextView textView3 = (TextView) convertView.findViewById(R.id.tvName3);
        textView3.setText(String.format("%s %s", currentItem.getFollowerCount(), context.getResources().getString(R.string.LB_CA_NO_OF_FOLLOWER)));

        Button btnFollow = (Button) convertView.findViewById(R.id.btnFollow);
        btnFollow.setTag(position);

        boolean isFollowed = followStatusList.get(currentItem.getMerchantId());

        if (isFollowed) {
            btnFollow.setText(R.string.LB_CA_FOLLOWED);
            btnFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ok, 0, 0, 0);
        } else {
            btnFollow.setText(R.string.LB_CA_FOLLOW);
            btnFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_add, 0, 0, 0);
        }

        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleFollowRequest(v, currentItem.getMerchantId());
            }
        });

        return convertView;
    }

    void handleFollowRequest(View view, final Integer merchantID) {
        final Button btnFollow = (Button) view;
        Follow followRequest = new Follow();
        followRequest.setUserKey(MmGlobal.getUserKey());
        followRequest.setToMerchantId(String.valueOf(merchantID));
        if (followStatusList.get(merchantID)) {
            APIManager.getInstance().getFollowService().deleteFollowMerchant(followRequest).enqueue(new MmCallBack<Boolean>(context) {
                @Override
                public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                    if (response.isSuccess() && response.body().equals(true)) {
                        followStatusList.put(merchantID, false);
                        btnFollow.setText(R.string.LB_CA_FOLLOW);
                        btnFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_add, 0, 0, 0);
                    }
                }
            });
        } else {
            APIManager.getInstance().getFollowService().saveFollowMerchant(followRequest).enqueue(new MmCallBack<Boolean>(context) {
                @Override
                public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                    if (response.isSuccess() && response.body().equals(true)) {
                        followStatusList.put(merchantID, true);
                        btnFollow.setText(R.string.LB_CA_FOLLOWED);
                        btnFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ok, 0, 0, 0);
                    }
                }
            });
        }
    }


    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        Merchant merchant = filteredData.get(position).getT();

        Follow followData = new Follow();
        followData.setUserKey(MmGlobal.getUserKey());
        followData.setToMerchantId(String.valueOf(merchant.getMerchantId()));

        boolean isFollowed = followStatusList.get(merchant.getMerchantId());
        if (isFollowed) {
            requestUnfollow(followData, merchant.getMerchantId());
        } else {
            requestFollow(followData, merchant.getMerchantId());
        }
    }

    private void requestFollow(Follow followData, final int merchantId) {
        APIManager.getInstance().getFollowService().saveFollowMerchant(followData).enqueue(new MmCallBack<Boolean>(context) {
            @Override
            public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Logger.d("SAVE SERVICE", String.valueOf(response.body()));
                    followStatusList.remove(merchantId);
                    followStatusList.put(merchantId, true);
                    notifyDataSetChanged();
                }
            }
        });
    }

    private void requestUnfollow(Follow followData, final int merchantId) {
        APIManager.getInstance().getFollowService().deleteFollowMerchant(followData).enqueue(new MmCallBack<Boolean>(context) {
            @Override
            public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Logger.d("DELETE SERVICE", String.valueOf(response.body()));
                    followStatusList.remove(merchantId);
                    followStatusList.put(merchantId, false);
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ProfileMerchantFilter();
        }
        return mFilter;
    }

    private class ProfileMerchantFilter extends ItemFilter {
        @Override
        protected String getFilterableString(Merchant item) {
            String filterableString = String.format("%s %s", item.getMerchantNameInvariant(), item.getMerchantName());
            return filterableString;
        }
    }
}
