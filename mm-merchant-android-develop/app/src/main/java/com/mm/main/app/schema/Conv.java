package com.mm.main.app.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by andrew on 17/3/2016.
 */
public class Conv implements Serializable {
    String ConvKey;
//    Date Timestamp;
    List<String> UserList = new ArrayList<>();

    public String getConvKey() {
        return ConvKey;
    }

    public void setConvKey(String convKey) {
        ConvKey = convKey;
    }

//    public Date getTimestamp() {
//        return Timestamp;
//    }
//
//    public void setTimestamp(Date timestamp) {
//        Timestamp = timestamp;
//    }

    public List<String> getUserList() {
        return UserList;
    }

    public void setUserList(List<String> userList) {
        UserList = userList;
    }
}
