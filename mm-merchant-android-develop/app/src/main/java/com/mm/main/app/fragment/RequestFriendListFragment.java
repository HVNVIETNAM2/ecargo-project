package com.mm.main.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.friend.AllFriendRvAdapter;
import com.mm.main.app.adapter.strorefront.friend.RequestFriendRvAdapter;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.FriendRequestRvItem;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.FriendRequest;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;
import com.mm.main.app.view.MmSearchBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by haivu on 3/4/2016.
 */
public class RequestFriendListFragment extends Fragment {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    List<FriendRequestRvItem> listData;
    RequestFriendRvAdapter adapter;

    List<FriendRequestRvItem> currentData;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_friend_list, container, false);
        ButterKnife.bind(this, view);

        if (!MmGlobal.getUserKey().equals("")) {
            fetchDataList();
        }
        return view;
    }

    public void searchUser(String s) {
        if (listData != null && listData.size() > 0) {
            currentData.clear();
            if (s.equals("")) {
                currentData.addAll(listData);
            } else {
                for (FriendRequestRvItem item : listData) {
                    if (item.getUser().getDisplayName().toLowerCase().contains(s.toString().toLowerCase())) {
                        currentData.add(item);
                    }
                }
            }

            adapter.notifyDataSetChanged();
        }
    }

    public void upDateData(){
        getData();
    }

    private void fetchDataList() {
        MmProgressDialog.show(getActivity());
        getData();
    }

    private void getData(){
        listData = new ArrayList<>();
        currentData = new ArrayList<>();

        APIManager.getInstance().getFriendService().listRequest(MmGlobal.getUserKey(), MmGlobal.getUserKey())
                .enqueue(new MmCallBack<List<User>>(getActivity()) {
                    @Override
                    public void onSuccess(Response<List<User>> response, Retrofit retrofit) {

                        for (User user : response.body()) {
                            FriendRequestRvItem friendRequestRvItem = new FriendRequestRvItem();
                            friendRequestRvItem.setUser(user);
                            listData.add(friendRequestRvItem);
                        }
                        currentData.addAll(listData);
                        setUpAdapter();
                    }
                });
    }

    private void delete(int position) {

        MmProgressDialog.show(getActivity());
        FriendRequest friendRequest = new FriendRequest();
        friendRequest.setUserKey(MmGlobal.getUserKey());
        friendRequest.setToUserKey(currentData.get(position).getUser().getUserKey());
        friendRequest.setUserkey(MmGlobal.getUserKey());

        APIManager.getInstance().getFriendService().deleteRequest(friendRequest)
                .enqueue(new MmCallBack<Boolean>(getActivity()) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {

                    }
                });
    }

    private void accept(int position) {
        MmProgressDialog.show(getActivity());
        FriendRequest friendRequest = new FriendRequest();
        friendRequest.setUserKey(MmGlobal.getUserKey());
        friendRequest.setToUserKey(currentData.get(position).getUser().getUserKey());
        friendRequest.setUserkey(MmGlobal.getUserKey());

        APIManager.getInstance().getFriendService().acceptRequest(friendRequest)
                .enqueue(new MmCallBack<Boolean>(getActivity()) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {

                    }
                });
    }

    private void setUpAdapter() {

        adapter = new RequestFriendRvAdapter(getActivity(), currentData, new RequestFriendRvAdapter.MyCallBack() {
            @Override
            public void deleteRequest(int position) {
                delete(position);
            }

            @Override
            public void acceptRequest(int position) {
                accept(position);
            }

            @Override
            public void seeProfile(int position) {
                //TODO: Go to profile page
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

}
