package com.mm.main.app.utils;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.mm.main.app.R;

/**
 * Created by andrew on 5/11/15.
 */
public class ActivityUtil {
    public static void setTitle(AppCompatActivity activity, String title) {
        setTitle(activity, title, 0);
    }

    public static void setTitle(AppCompatActivity activity, String title, int extraLeftPadding) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        activity.getSupportActionBar().setDisplayShowCustomEnabled(true);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        View v = inflater.inflate(R.layout.common_layout, null);

        ((TextView) v.findViewById(R.id.title)).setText(title);
        v.setPadding(v.getPaddingLeft() + extraLeftPadding,
                v.getPaddingTop(),
                v.getPaddingRight(),
                v.getPaddingBottom());
        activity.getSupportActionBar().setCustomView(v);
    }

    public static void setBackIcon(AppCompatActivity activity) {
        final Drawable upArrow = activity.getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(activity.getResources().getColor(R.color.mm_input_gray), PorterDuff.Mode.SRC_ATOP);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    public static void closeKeyboard(AppCompatActivity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
