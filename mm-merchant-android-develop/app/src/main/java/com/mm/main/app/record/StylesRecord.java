package com.mm.main.app.record;

import com.mm.main.app.listitem.DynamicAvailableListItem;
import com.mm.main.app.schema.CartItem;
import com.mm.main.app.schema.ProductColor;
import com.mm.main.app.schema.ProductSize;
import com.mm.main.app.schema.Style;
import com.mm.main.app.adapter.strorefront.product.ProductDetailSelectionCollection;

import java.util.Hashtable;

/**
 * Created by henrytung on 10/12/2015.
 */
public class StylesRecord {

    private static StylesRecord myStyleRecord = null;

    public static StylesRecord getInstance() {
        if (myStyleRecord == null) {
            myStyleRecord = new StylesRecord();
        }

        return myStyleRecord;
    }

    public static ProductDetailSelectionCollection getSelectionFromWishList(CartItem cartItem) {
        ProductDetailSelectionCollection selection = new ProductDetailSelectionCollection();

        ProductColor color = cartItem.getSelectedColor();
        ProductSize size = cartItem.getSelectedSize();
        if (color != null) {
            selection.setSelectedColor(new DynamicAvailableListItem<ProductColor>(color));
        }
        if (size != null) {
            selection.setSelectedSize(new DynamicAvailableListItem<ProductSize>(size));
        }

        return selection;
    }

    public static ProductDetailSelectionCollection getSelectionFromShoppingCart(CartItem cartItem) {
        ProductDetailSelectionCollection selection = new ProductDetailSelectionCollection();

        ProductColor color = new ProductColor();
        color.setColorId(cartItem.getColorId());
        color.setColorKey(cartItem.getColorKey());

        ProductSize size = new ProductSize();
        size.setSizeId(cartItem.getSizeId());

        if (color != null) {
            selection.setSelectedColor(new DynamicAvailableListItem<ProductColor>(color));
        }
        if (size != null) {
            selection.setSelectedSize(new DynamicAvailableListItem<ProductSize>(size));
        }

       return selection;
    }
}
