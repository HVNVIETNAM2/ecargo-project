package com.mm.main.app.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mm.main.app.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by thienchaud on 05-Feb-16.
 */
public class MmSearchBar extends LinearLayout {

    @Bind(R.id.searchEditText)
    EditText searchEditText;
    @Bind(R.id.btnCancel)
    Button btnCancel;

    MmSearchBarListener mmSearchBarListener;

    public void setMmSearchBarListener(MmSearchBarListener mmSearchBarListener) {
        this.mmSearchBarListener = mmSearchBarListener;
    }

    public interface MmSearchBarListener {
        void onEnterText(CharSequence s);
        void onCancelSearch();
    }

    public MmSearchBar(Context context) {
        super(context);

        initView();
    }

    public MmSearchBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public MmSearchBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    void initView() {
        View view = inflate(getContext(), R.layout.mm_search_view, null);
        addView(view);
        ButterKnife.bind(this);
    }

    @OnTextChanged(R.id.searchEditText)
    public void onTextChanged(CharSequence text) {
        if (TextUtils.isEmpty(text)) {
            btnCancel.setVisibility(GONE);
        } else {
            btnCancel.setVisibility(VISIBLE);
        }
        if (mmSearchBarListener != null) {
            mmSearchBarListener.onEnterText(text.toString().trim());
        }
    }

    @OnClick(R.id.btnCancel)
    public void cancelSearch() {
        btnCancel.setVisibility(GONE);
        searchEditText.setText("");
        if (mmSearchBarListener != null) {
            mmSearchBarListener.onCancelSearch();
        }
    }

    public void setHint(int resId) {
        searchEditText.setHint(resId);
    }

}

