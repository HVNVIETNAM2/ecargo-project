package com.mm.main.app.service;

import android.text.TextUtils;

import com.mm.main.app.manager.APIManager;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.schema.Badge;
import com.mm.main.app.schema.Brand;
import com.mm.main.app.schema.Category;
import com.mm.main.app.schema.Color;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.response.SearchResponse;
import com.mm.main.app.schema.Size;
import com.mm.main.app.utils.MmCallBack;

import java.util.List;

/**
 * Created by henrytung on 15/12/2015.
 */
public class SearchServiceBuilder {

    final private static int MAX_PRODUCT_NUMBER = 9999;

    public static void searchStyle(MmCallBack<SearchResponse> mmCallBack) {
        searchStyle(mmCallBack, null, null, null, null, null, null, null, null, null, null, null);
    }

    public static void searchStyleWithFilter(MmCallBack<SearchResponse> mmCallBack) {
        searchStyle(mmCallBack, SearchStyle.getInstance().getQueryString(),
                SearchStyle.getInstance().getPricefrom(), SearchStyle.getInstance().getPriceTo(),
                SearchStyle.getInstance().getBrandid(), SearchStyle.getInstance().getCategoryid(),
                SearchStyle.getInstance().getColorid(), SearchStyle.getInstance().getSizeid(),
                SearchStyle.getInstance().getBadgeid(), SearchStyle.getInstance().getMerchantid(),
                SearchStyle.getInstance().getIssale(), SearchStyle.getInstance().getIsnew()
        );
    }

    public static void searchStyleWithFilterAllBrand(MmCallBack<SearchResponse> mmCallBack) {
        searchStyle(mmCallBack, SearchStyle.getInstance().getQueryString(),
                SearchStyle.getInstance().getPricefrom(), SearchStyle.getInstance().getPriceTo(),
                null, SearchStyle.getInstance().getCategoryid(),
                SearchStyle.getInstance().getColorid(), SearchStyle.getInstance().getSizeid(),
                SearchStyle.getInstance().getBadgeid(), SearchStyle.getInstance().getMerchantid(),
                SearchStyle.getInstance().getIssale(), SearchStyle.getInstance().getIsnew(),false
        );
    }

    public static void searchStyleFilteredByEntry(MmCallBack<SearchResponse> mmCallBack) {
        APIManager.getInstance().getSearchService().styleList("*:*", 1, MAX_PRODUCT_NUMBER, null,
                null, null, createBrandQuery(SearchStyle.getInstance().getBrandid()),
                createCategoryQuery(SearchStyle.getInstance().getCategoryid()), null, null, null,
                null, null, null, null, null).enqueue(mmCallBack);
    }

    public static void searchStyle(MmCallBack<SearchResponse> mmCallBack, String queryString,
                                   Integer pricefrom, Integer priceTo,
                                   List<Brand> brandid, List<Category> categoryid,
                                   List<Color> colorid, List<Size> sizeid, List<Badge> badgeid,
                                   List<Merchant> merchantid, Integer issale, Integer isnew){
        searchStyle(mmCallBack, queryString, pricefrom, priceTo, brandid, categoryid, colorid,
                sizeid, badgeid, merchantid, issale, isnew, true);
    }

    public static void searchStyle(MmCallBack<SearchResponse> mmCallBack, String queryString,
                                   Integer pricefrom, Integer priceTo,
                                   List<Brand> brandid, List<Category> categoryid,
                                   List<Color> colorid, List<Size> sizeid, List<Badge> badgeid,
                                   List<Merchant> merchantid,
                                   Integer issale, Integer isnew, boolean save) {

        if (save) {
            SearchStyle.getInstance().setQueryString(queryString);
            SearchStyle.getInstance().setPricefrom(pricefrom);
            SearchStyle.getInstance().setPriceTo(priceTo);
            SearchStyle.getInstance().setBrandid(brandid);
            SearchStyle.getInstance().setCategoryid(categoryid);
            SearchStyle.getInstance().setColorid(colorid);
            SearchStyle.getInstance().setSizeid(sizeid);
            SearchStyle.getInstance().setBadgeid(badgeid);
            SearchStyle.getInstance().setMerchantid(merchantid);
            SearchStyle.getInstance().setIssale(issale);
            SearchStyle.getInstance().setIsnew(isnew);
        }
        APIManager.getInstance().getSearchService().styleList("*:*", 1, MAX_PRODUCT_NUMBER, queryString,
                pricefrom, priceTo, createBrandQuery(brandid), createCategoryQuery(categoryid),
                createColorQuery(colorid), createSizeQuery(sizeid), createBadgeQuery(badgeid),
                SearchStyle.getInstance().getSort(),
                SearchStyle.getInstance().getOrder(),
                createMerchantQuery(SearchStyle.getInstance().getMerchantid()),
                issale, isnew)
                .enqueue(mmCallBack);

    }

    private static String createMerchantQuery(List<Merchant> options) {
        if (options == null || options.size() <= 0) {
            return null;
        }
        String query = "";
        String separator = ",";
        for (int i = 0; i < options.size(); i++) {
            query += options.get(i).getMerchantId();
            query += separator;
        }

        return query.substring(0, query.length() - 1);
    }

    private static String createBrandQuery(List<Brand> options) {
        if (options == null || options.size() <= 0) {
            return null;
        }
        String query = "";
        String separator = ",";
        for (int i = 0; i < options.size(); i++) {
             query += options.get(i).getBrandId();
            query += separator;
        }

        return query.substring(0, query.length() - 1);
    }

    private static String createCategoryQuery(List<Category> options) {
        String query = "";
        if (options != null && options.size() > 0) {
            String separator = ",";
            for (int i = 0; i < options.size(); i++) {
                if (options.get(i) != null) {
                    query += options.get(i).getCategoryId();
                    query += separator;
                }
            }
        }

        if (query.length() > 1) {
            query = query.substring(0, query.length() - 1);
        }

        return createSubCategoryQuery(SearchStyle.getInstance().getSubCategoryid(), query);

    }

    private static String createSubCategoryQuery(List<Category> options, String mainQuery) {
        if (options == null || options.size() <= 0) {
            if (!TextUtils.isEmpty(mainQuery)) {
                return mainQuery;
            } else {
                return null;
            }
        }
        String query = "";
        String separator = ",";
        for (int i = 0; i < options.size(); i++) {
            if (options.get(i) != null) {
                query += options.get(i).getCategoryId();
                query += separator;
            }
        }

        if (TextUtils.isEmpty(mainQuery)) {
            return query.substring(0, query.length() - 1);
        } else {
            return mainQuery + "," + query.substring(0, query.length() - 1);
        }

    }

    private static String createColorQuery(List<Color> options) {
        if (options == null || options.size() <= 0) {
            return null;
        }
        String query = "";
        String separator = ",";
        for (int i = 0; i < options.size(); i++) {
            query += options.get(i).getColorId();
            query += separator;
        }

        return query.substring(0, query.length() - 1);
    }

    private static String createSizeQuery(List<Size> options) {
        if (options == null || options.size() <= 0) {
            return null;
        }
        String query = "";
        String separator = ",";
        for (int i = 0; i < options.size(); i++) {
            query += options.get(i).getSizeId();
            query += separator;
        }

        return query.substring(0, query.length() - 1);
    }

    private static String createBadgeQuery(List<Badge> options) {
        if (options == null || options.size() <= 0) {
            return null;
        }
        String query = "";
        String separator = ",";
        for (int i = 0; i < options.size(); i++) {
            query += options.get(i).getBadgeId();
            query += separator;
        }

        return query.substring(0, query.length() - 1);
    }
}
