package com.mm.main.app.schema;

/**
 * Created by binhtruongp on 2/26/2016.
 */
public class FriendRequest {
    String UserKey;
    String ToUserKey;
    String userkey;


    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public String getToUserKey() {
        return ToUserKey;
    }

    public void setToUserKey(String toUserKey) {
        ToUserKey = toUserKey;
    }


    public String getUserkey() {
        return userkey;
    }

    public void setUserkey(String userkey) {
        this.userkey = userkey;
    }


}
