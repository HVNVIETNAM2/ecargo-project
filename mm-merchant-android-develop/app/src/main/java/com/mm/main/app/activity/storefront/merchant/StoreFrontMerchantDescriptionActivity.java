package com.mm.main.app.activity.storefront.merchant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.cart.ShoppingCartActivity;
import com.mm.main.app.activity.storefront.search.ProductListSearchActivity;
import com.mm.main.app.activity.storefront.wishlist.WishListActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.PathUtil;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by binhtruongp on 3/17/2016.
 */
public class StoreFrontMerchantDescriptionActivity extends AppCompatActivity {

    @Bind(R.id.tvMerchantName)
    TextView tvMerchantName;

    @Bind(R.id.tvMerchantProfileDes)
    TextView tvMerchantProfileDes;

    @Bind(R.id.ivCompanyLogo)
    ImageView ivCompanyLogo;

    @OnClick(R.id.btnSearch)
    public void startSearch() {
        Intent intent = new Intent(this, ProductListSearchActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_description);
        ButterKnife.bind(this);
        setupToolbar();

        getMerchantDetail(getIntent().getIntExtra(StoreFrontMerchantProfileActivity.MERCHANT_ID_KEY, 0));
    }

    private void getMerchantDetail(int merchantID) {
        APIManager.getInstance().getViewService().viewMerchant(merchantID).enqueue(new MmCallBack<Merchant>(this) {
            @Override
            public void onSuccess(Response<Merchant> response, Retrofit retrofit) {
                response.toString();
                if (response.isSuccess()) {
                    Merchant merchant = response.body();

                    String headerUrl = PathUtil.getMerchantImageUrl(merchant.getHeaderLogoImage());
                    Picasso.with(MyApplication.getContext()).load(headerUrl).into(ivCompanyLogo);

                    tvMerchantName.setText(merchant.getMerchantName());
                    tvMerchantProfileDes.setText(merchant.getMerchantDesc());
                }
            }
        });
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        invalidateOptionsMenu();
        toolbar.setContentInsetsAbsolute(0, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_profile, menu);

        //for heart icon
        MenuItem item = menu.findItem(R.id.action_like);
        MenuItemCompat.setActionView(item, R.layout.badge_button_like);

        RelativeLayout badgeLayout = (RelativeLayout) MenuItemCompat.getActionView(item);
        View badge = badgeLayout.findViewById(R.id.btnBadge);
        ImageView heartIcon = (ImageView) badgeLayout.findViewById(R.id.icon_heart_stroke);

        heartIcon.setImageResource(R.drawable.wishlist_grey);

        if (CacheManager.getInstance().isWishlistHasItem()) {
            badge.setVisibility(View.VISIBLE);
        } else {
            badge.setVisibility(View.GONE);
        }

        item.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StoreFrontMerchantDescriptionActivity.this, WishListActivity.class);
                startActivity(i);
            }
        });

        //for cart icon
        MenuItem cartItem = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(cartItem, R.layout.badge_button_cart);

        RelativeLayout badgeCartLayout = (RelativeLayout) MenuItemCompat.getActionView(cartItem);
        View cartBadge = badgeCartLayout.findViewById(R.id.btnBadge);
        ImageView cartIcon = (ImageView) badgeCartLayout.findViewById(R.id.cartIcon);

        cartIcon.setImageResource(R.drawable.cart_grey);

        if (CacheManager.getInstance().isCartHasItem()) {
            cartBadge.setVisibility(View.VISIBLE);
        } else {
            cartBadge.setVisibility(View.GONE);
        }

        cartItem.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StoreFrontMerchantDescriptionActivity.this, ShoppingCartActivity.class);
                startActivity(i);
            }
        });

        return super.onCreateOptionsMenu(menu);
    }
}
