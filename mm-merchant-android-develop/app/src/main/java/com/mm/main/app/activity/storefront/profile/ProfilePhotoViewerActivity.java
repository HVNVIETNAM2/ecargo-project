package com.mm.main.app.activity.storefront.profile;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;

import com.mm.main.app.activity.storefront.product.PhotosViewerActivity;
import com.mm.main.app.fragment.PhotoPagerFragment;
import com.mm.main.app.schema.ImageData;

import java.util.ArrayList;

/**
 * Created by tuuphungd on 3/3/2016.
 */
public class ProfilePhotoViewerActivity extends PhotosViewerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected PagerAdapter initPhotoAdapter() {
        photoList = new ArrayList<>();
        photoList.add((ImageData) getIntent().getSerializableExtra(IMAGE_DATA_KEY));
        currentPos = 0;

        return new PhotosViewerActivity.ScreenSlidePagerAdapter(getSupportFragmentManager(), PhotoPagerFragment.IMAGE_TYPE_PROFILE);
    }
}
