package com.mm.main.app.activity.storefront.interest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.animation.AlphaInAnimationAdapter;
import com.mm.main.app.adapter.strorefront.interest.InterestMappingCuratorsRvAdapter;
import com.mm.main.app.adapter.strorefront.animation.ScaleInAnimationAdapter;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.CuratorListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.Follow;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.CenterLockListener;
import com.mm.main.app.utils.CircleProcessUtil;
import com.mm.main.app.utils.FadeInAnimator;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Response;
import retrofit.Retrofit;


public class InterestMappingCuratorsActivity extends AppCompatActivity {

    @Bind(R.id.numRecommendedTextView)
    TextView numRecommendedTextView;

    @Bind(R.id.nextStepButton)
    Button nextStepButton;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.circleProcess)
    CircleProcessUtil circleProcess;

    @Bind(R.id.centerAnimateView)
    CircleImageView centerAnimateView;


    private ArrayList<CuratorListItem> curatorListItem;
    private InterestMappingCuratorsRvAdapter adapter;
    private int percentProcess;
    private int numSelectedItem;
    private int centerItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_mapping_curators);
        ButterKnife.bind(this);
        getCuratorsService();
        percentProcess = 0;
        numSelectedItem = 0;
        circleProcess.setPercent(percentProcess);
        setUpNextButton(4);
    }

    private void setUpNextButton(int minSelected) {
        if (numSelectedItem >= minSelected) {
            nextStepButton.setEnabled(true);
            nextStepButton.setAlpha(1.0f);
        } else {
            nextStepButton.setEnabled(false);
            nextStepButton.setAlpha(0.5f);
        }
    }

    @OnClick(R.id.nextStepButton)
    public void onClickNextStepButton() {

        String listFollower = "";

        for (int i = 0; i < curatorListItem.size(); i++) {
            if (curatorListItem.get(i).getIsSelected()) {
                listFollower += curatorListItem.get(i).getCurator().getUserKey();
                if (i != curatorListItem.size() - 1) {
                    listFollower += ",";
                }
            }
        }

        saveFollow(listFollower);

    }

    @OnClick(R.id.skipTextView)
    public void onClickSkipTextView() {
        int size = Math.min(Constant.NUM_FOLLOWER_SKIP, curatorListItem.size());

        String listFollower = "";
        for (int i = 0; i < size; i++) {
            listFollower += curatorListItem.get(i).getCurator().getUserKey();
            if (i != size - 1) {
                listFollower += ",";
            }
        }
        saveFollow(listFollower);
    }

    private void saveFollow(String listFollower) {
        MmProgressDialog.show(getApplicationContext());
        Follow follow = new Follow();
        follow.setUserKey(MmGlobal.getUserKey());
        follow.setToUserKeys(listFollower);

        APIManager.getInstance().getFollowService().saveFollowCurator(follow)
                .enqueue(new MmCallBack<Boolean>(this) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                        if (response.body().booleanValue()) {
                            startActivity(new Intent(InterestMappingCuratorsActivity.this, InterestMappingBrandMerchantsActivity.class));
                        }
                    }
                });
    }

    public void getCuratorsService() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getTagService().curator(MmGlobal.getUserKey())
                .enqueue(new MmCallBack<List<User>>(InterestMappingCuratorsActivity.this) {
                    @Override
                    public void onSuccess(Response<List<User>> response, Retrofit retrofit) {
                        curatorListItem = new ArrayList<>();
                        List<User> curators = response.body();
                        for (User item : curators) {
                            curatorListItem.add(new CuratorListItem(item));

                        }
                        setUpView(4);
                    }
                });
    }

    private void setUpView(int minSelected) {

        if (curatorListItem != null && curatorListItem.size() > 0) {
            numRecommendedTextView.setText(curatorListItem.size() + "");
            //Set up adapter.
            if (curatorListItem.size() > 0) {
                setUpAdapter(minSelected);
            }
        }
    }

    private void setUpAdapter(final int minSelected) {

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setItemAnimator(new FadeInAnimator());

        adapter = new InterestMappingCuratorsRvAdapter(getApplicationContext(), curatorListItem, recyclerView, new InterestMappingCuratorsRvAdapter.OnItemListener() {
            @Override
            public void onClick(boolean isSelected, int position) {
                curatorListItem.get(position).setIsSelected(isSelected);

                if (isSelected) {
                    numSelectedItem++;
                } else {
                    numSelectedItem--;
                }
                if (numSelectedItem == 1) {
                    percentProcess = ((360 * 1 / minSelected) * 100) / 360;
                } else {
                    percentProcess = ((Math.min(360 * numSelectedItem / minSelected, 360)) * 100) / 360;
                }
                circleProcess.setPercent(percentProcess);
                setUpNextButton(minSelected);


                int first =layoutManager.findFirstVisibleItemPosition();
                int last = layoutManager.findLastVisibleItemPosition();
                for(int i=first; i <= last; i++){
                    if(i%curatorListItem.size() == position){
                       adapter.updateViewTheSame(isSelected, layoutManager.findViewByPosition(i));
                    }
                }
            }
        });

        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        final ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(alphaAdapter);
        recyclerView.setAdapter(scaleAdapter);

        centerItem = UiUtil.findCenterItemPosition(recyclerView, getApplicationContext(), curatorListItem.size(), InterestMappingCuratorsRvAdapter.MAX_NUMBER_ITEM);

        recyclerView.addOnScrollListener(new CenterLockListener(getApplicationContext(), centerAnimateView, recyclerView, scaleAdapter,
                new CenterLockListener.RecyclerViewCallBack() {
                    @Override
                    public void updateView(View newView, View oldView, int oldCenterPosition, int newCenterPosition) {
                        if (oldCenterPosition >= 0) {
                            if (oldCenterPosition != newCenterPosition) {
                                adapter.updateCenterView(oldCenterPosition, oldView, false);
                                adapter.updateCenterView(newCenterPosition, newView, true);
                            } else {
                                adapter.updateCenterView(newCenterPosition, newView, true);
                            }
                        }
                    }
                }));

        recyclerView.scrollToPosition(centerItem);
    }
}
