package com.mm.main.app.schema;

import java.io.Serializable;
import java.util.Date;

/**
 * User class for both Retrofit server exchange schema, and ORM model for Active Android
 */

public class User implements Serializable {


    Integer FollowerCount;
    Integer FriendCount;
    Integer FollowingUserCount;
    Integer FollowingMerchantCount;
    Integer FollowingBrandCount;

    public Integer getFollowingCuratorCount() {
        return FollowingCuratorCount;
    }

    public void setFollowingCuratorCount(Integer followingCuratorCount) {
        FollowingCuratorCount = followingCuratorCount;
    }

    Integer FollowingCuratorCount;
    Integer UserId;
    Integer UserTypeId;
    Integer MerchantId;
    Integer InventoryLocationId;
    String FirstName;
    String LastName;
    String MiddleName;
    String DisplayName;
    String Email;
    String MobileCode;
    String MobileNumber;
    String ProfileImage;
    String CoverImage;
    Integer IsCuratorUser;
    Integer StatusId;
    String StatusNameInvariant;
    String TimeZoneId;
    Integer LanguageId;
    Date LastLogin;
    Date LastModified;
    Integer[] UserSecurityGroupArray;
    Integer[] UserInventoryLocationArray;
    String Password;
    String PasswordOld;
    Merchant Merchant;
    private String UserKey;
    private String UserName;
    private Integer Count;

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer userId) {
        UserId = userId;
    }

    public Integer getUserTypeId() {
        return UserTypeId;
    }

    public void setUserTypeId(Integer userTypeId) {
        UserTypeId = userTypeId;
    }

    public Integer getMerchantId() {
        return MerchantId;
    }

    public void setMerchantId(Integer merchantId) {
        MerchantId = merchantId;
    }

    public Integer getInventoryLocationId() {
        return InventoryLocationId;
    }

    public void setInventoryLocationId(Integer inventoryLocationId) {
        InventoryLocationId = inventoryLocationId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobileCode() {
        return MobileCode;
    }

    public void setMobileCode(String mobileCode) {
        MobileCode = mobileCode;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getCoverImage() {
        return CoverImage;
    }

    public void setCoverImage(String coverImage) {
        CoverImage = coverImage;
    }

    public Integer getIsCuratorUser() {
        return IsCuratorUser;
    }

    public void setIsCuratorUser(Integer isCuratorUser) {
        IsCuratorUser = isCuratorUser;
    }

    public Integer getStatusId() {
        return StatusId;
    }

    public void setStatusId(Integer statusId) {
        StatusId = statusId;
    }

    public String getStatusNameInvariant() {
        return StatusNameInvariant;
    }

    public void setStatusNameInvariant(String statusNameInvariant) {
        StatusNameInvariant = statusNameInvariant;
    }

    public String getTimeZoneId() {
        return TimeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        TimeZoneId = timeZoneId;
    }

    public Integer getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(Integer languageId) {
        LanguageId = languageId;
    }

    public Date getLastLogin() {
        return LastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        LastLogin = lastLogin;
    }

    public Date getLastModified() {
        return LastModified;
    }

    public void setLastModified(Date lastModified) {
        LastModified = lastModified;
    }

    public Integer[] getUserSecurityGroupArray() {
        return UserSecurityGroupArray;
    }

    public void setUserSecurityGroupArray(Integer[] userSecurityGroupArray) {
        UserSecurityGroupArray = userSecurityGroupArray;
    }

    public Integer[] getUserInventoryLocationArray() {
        return UserInventoryLocationArray;
    }

    public void setUserInventoryLocationArray(Integer[] userInventoryLocationArray) {
        UserInventoryLocationArray = userInventoryLocationArray;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getPasswordOld() {
        return PasswordOld;
    }

    public void setPasswordOld(String passwordOld) {
        PasswordOld = passwordOld;
    }

    public com.mm.main.app.schema.Merchant getMerchant() {
        return Merchant;
    }

    public void setMerchant(com.mm.main.app.schema.Merchant merchant) {
        Merchant = merchant;
    }

    public User() {
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public Integer getCount() {
        return Count;
    }

    public void setCount(Integer count) {
        Count = count;
    }

    public Integer getFollowerCount() {
        return FollowerCount;
    }

    public void setFollowerCount(Integer followerCount) {
        FollowerCount = followerCount;
    }

    public Integer getFriendCount() {
        return FriendCount;
    }

    public void setFriendCount(Integer friendCount) {
        FriendCount = friendCount;
    }

    public Integer getFollowingUserCount() {
        return FollowingUserCount;
    }

    public void setFollowingUserCount(Integer followingUserCount) {
        FollowingUserCount = followingUserCount;
    }

    public Integer getFollowingMerchantCount() {
        return FollowingMerchantCount;
    }

    public void setFollowingMerchantCount(Integer followingMerchantCount) {
        FollowingMerchantCount = followingMerchantCount;
    }

    public Integer getFollowingBrandCount() {
        return FollowingBrandCount;
    }

    public void setFollowingBrandCount(Integer followingBrandCount) {
        FollowingBrandCount = followingBrandCount;
    }
}
