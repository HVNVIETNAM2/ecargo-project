package com.mm.main.app.listitem;

import com.mm.main.app.schema.Merchant;

/**
 * Created by haivu on 2/16/2016.
 */
public class BrandMerchantsListItem {
    private Merchant Merchant;
    private Boolean IsSelected;
    private Boolean IsCenter;

    public BrandMerchantsListItem(Merchant merchant) {
        Merchant = merchant;
        IsSelected = false;
        IsCenter = false;
    }

    public Boolean getIsSelected() {
        return IsSelected;
    }
    public void setIsSelected(Boolean isSelected) {
        IsSelected = isSelected;
    }

    public Merchant getMerchant() {
        return Merchant;
    }

    public void setMerchant(Merchant merchant) {
        Merchant = merchant;
    }

    public Boolean getIsCenter() {
        return IsCenter;
    }

    public void setIsCenter(Boolean isCenter) {
        IsCenter = isCenter;
    }

}
