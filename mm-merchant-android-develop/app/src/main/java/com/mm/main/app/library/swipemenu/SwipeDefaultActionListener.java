package com.mm.main.app.library.swipemenu;

/**
 * Created by thienchaud on 18-Jan-16.
 */
public interface SwipeDefaultActionListener {

    void UpdateDefaultActionUI(boolean isRightMenu);

    void CancelDefaultActionUI(boolean isRightMenu);

    void ActionDefault(boolean isRightMenu, SwipeLayout swipeLayout);
}
