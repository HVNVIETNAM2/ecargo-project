package com.mm.main.app.service;

import com.mm.main.app.schema.Badge;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by henrytung on 24/11/2015.
 */
public interface BadgeService {

    String PATH = "badge";

    @GET(PATH + "/list")
    Call<List<Badge>> list();
}
