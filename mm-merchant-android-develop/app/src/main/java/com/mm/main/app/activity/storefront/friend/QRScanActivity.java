package com.mm.main.app.activity.storefront.friend;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.mm.main.app.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class QRScanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscan);
        ButterKnife.bind(this);
    }
    public void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.tvSeeQRCode)
    public void onClickMyQRCode(){
        Intent intent = new Intent(QRScanActivity.this,UserQrCodeActivity.class);
        startActivity(intent);
    }
}
