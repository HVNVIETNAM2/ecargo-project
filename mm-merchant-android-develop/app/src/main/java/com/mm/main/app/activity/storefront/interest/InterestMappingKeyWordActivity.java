package com.mm.main.app.activity.storefront.interest;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.tnc.TermsAndConditionsActivity;
import com.mm.main.app.adapter.strorefront.interest.InterestMappingRvAdapter;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.TagListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.request.SaveTagRequest;
import com.mm.main.app.schema.Tag;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by haivu on 1/29/16.
 */
public class InterestMappingKeyWordActivity extends AppCompatActivity {

    public static final String IS_SHOWING_TNC_FLAG = "IS_SHOWING_TNC_FLAG";

    InterestMappingRvAdapter adapter;

    @Bind(R.id.userAgreementTextView)
    TextView userAgreementTextView;

    @Bind(R.id.agreedCheckbox)
    CheckBox agreedCheckbox;

    @Bind(R.id.nextStepButton)
    Button nextStepButton;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.checkboxGroupLL)
    LinearLayout checkboxGroupLL;

    ArrayList<TagListItem> tagListItems;
    boolean isNeedTNC = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.interest_mapping_keyword_activity);
        ButterKnife.bind(this);

        getTagService();
        checkNextStep();
        setupTNC();
    }

    void setupTNC() {
        isNeedTNC = getIntent().getBooleanExtra(IS_SHOWING_TNC_FLAG, true);
        if (isNeedTNC) {
            checkboxGroupLL.setVisibility(View.VISIBLE);
            //Make underline for text view.
            String htmlString = "<u>" + getResources().getString(R.string.LB_CA_TNC_LINK) + "</u>";
            userAgreementTextView.setText(Html.fromHtml(htmlString));
        } else {
            checkboxGroupLL.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.agreedCheckbox)
    public void onClickAgreedCheckbox() {
        checkNextStep();
    }

    @OnClick(R.id.userAgreementTextView)
    public void startTNCPage() {
        Intent intent = new Intent(this, TermsAndConditionsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.nextStepButton)
    public void onClickNextButton() {
        ArrayList<Tag> selectedTags = new ArrayList<Tag>();
        for (TagListItem item : tagListItems) {
            if (item.getSelected() == true) {
                selectedTags.add(item.getTag());
            }
        }

        MmProgressDialog.show(this);
        Integer userId = MmGlobal.getUserId();
        APIManager.getInstance().getTagService().saveTag(new SaveTagRequest(userId, true, selectedTags))
                .enqueue(new MmCallBack<Boolean>(InterestMappingKeyWordActivity.this) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                        startStoreFrontMain();
                    }

                });
    }

    public void startStoreFrontMain() {
        Intent intent = new Intent(this, InterestMappingCuratorsActivity.class);
        startActivity(intent);
    }

    public class MarginDecoration extends RecyclerView.ItemDecoration {
        private int margin;

        public MarginDecoration(Context context) {
            margin = UiUtil.getPixelFromDp(4);
        }

        @Override
        public void getItemOffsets(
                Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(margin, margin, margin, margin);
        }
    }

    private boolean checkSelected() {

        for (TagListItem item : tagListItems) {
            if (item.getSelected() == true)
                return true;
        }
        return false;
    }

    private void checkNextStep() {

        if ((isNeedTNC ? agreedCheckbox.isChecked() : true) && checkSelected()) {
            nextStepButton.setEnabled(true);
            nextStepButton.setAlpha(1.0f);
        } else {
            nextStepButton.setEnabled(false);
            nextStepButton.setAlpha(0.5f);
        }
    }

    public void getTagService() {
        MmProgressDialog.show(this);
        Integer tagTypeId = 1;
        APIManager.getInstance().getTagService().list(tagTypeId).enqueue(new MmCallBack<List<Tag>>(InterestMappingKeyWordActivity.this) {
            @Override
            public void onSuccess(Response<List<Tag>> response, Retrofit retrofit) {
                tagListItems = new ArrayList<>();
                List<Tag> tags = response.body();
                for (Tag item : tags) {
                    tagListItems.add(new TagListItem(item));
                }
                setupAdapter();
            }
        });
    }

    public void setupAdapter() {
        recyclerView.addItemDecoration(new MarginDecoration(this));
        recyclerView.setHasFixedSize(true);

        int sumSpan = 160;
        GridLayoutManager manager = new GridLayoutManager(this, sumSpan);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return adapter.getTagListItems().get(position).getSpanSize();
            }
        });
        recyclerView.setLayoutManager(manager);


        adapter = new InterestMappingRvAdapter(sumSpan, tagListItems, this, new InterestMappingRvAdapter.OnItemListener() {
            @Override
            public void onClick(TagListItem tagListItem, int position) {
                tagListItems.get(position).setSelected(tagListItem.getSelected());
                checkNextStep();
            }
        });

        recyclerView.setAdapter(adapter);
    }
}