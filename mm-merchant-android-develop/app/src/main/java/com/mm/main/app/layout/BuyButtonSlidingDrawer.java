package com.mm.main.app.layout;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.utils.UiComponentUtil;

import java.text.NumberFormat;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by haivu on 1/26/16.
 */
public class BuyButtonSlidingDrawer extends FrameLayout {

    //define callback interface
    public interface BuyOpenedCallBack {

        void drawerOpened(Activity context, boolean isSwipe);

    }

    public interface ScrollEventCallBack {
        void onScrollStarted();

        void onScrollEnd(boolean opened);
    }

    @Bind(R.id.tvPriceAfterSwipe)
    TextView tvPriceAfterSwipe;

    @Bind(R.id.tvSweepAfterSwipe)
    TextView tvSweepAfterSwipe;

    @Bind(R.id.tvPriceBeforeSwipe)
    TextView tvPriceBeforeSwipe;

    @Bind(R.id.slidingDrawer1)
    SlidingDrawer slidingDrawer;

    @Bind(R.id.handle)
    ImageView handle;

    @Bind(R.id.content)
    ImageView content;

    @Bind(R.id.imageView2)
    LinearLayout imageViewCartGroup;

    @Bind(R.id.backgroundSliding)
    ImageView backgroundSliding;

    @Bind(R.id.contentSliding)
    ImageView contentSliding;

    @Bind(R.id.frameLayoutSwipeButton)
    FrameLayout frameLayoutSwipeButton;

    boolean isOpened = false;

    boolean needOpenBuy = true;

    private int orgLeftHandler;

    public static int CLICK_RANGE = 5;

    private static float percentSwipeToBuy = 0.48f;


    public BuyButtonSlidingDrawer(Context context) {
        super(context);

        initView();
    }

    public BuyButtonSlidingDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public BuyButtonSlidingDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    private void initView() {
        View view = inflate(getContext(), R.layout.buy_button_sliding_drawer, null);
        addView(view);
        ButterKnife.bind(this);
    }

    public void closeSliding() {
        UiComponentUtil.changeViewBuyButtonEndScroll(tvPriceAfterSwipe, tvSweepAfterSwipe,
                handle, content, imageViewCartGroup);

        backgroundSliding.setVisibility(View.GONE);
        contentSliding.setVisibility(View.GONE);
        slidingDrawer.close();
    }

    public void bindBuyItemView(final BuyOpenedCallBack buyOpenedCallBack,
                                final Double priceRetail, final Activity parentContext,
                                final ScrollEventCallBack scrollEventCallBack) {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CHINA);
        format.setMaximumFractionDigits(0);
        tvPriceBeforeSwipe.setText(format.format(priceRetail));
        tvSweepAfterSwipe.setText("一扫即买");

        tvPriceAfterSwipe.setText(format.format(priceRetail));

        slidingDrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {
            @Override
            public void onDrawerOpened() {
                if (needOpenBuy) {

                    buyOpenedCallBack.drawerOpened(parentContext, true);
                } else {
                    slidingDrawer.close();
                }
            }
        });

        slidingDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {
            @Override
            public void onDrawerClosed() {
                if (scrollEventCallBack != null) {
                    scrollEventCallBack.onScrollEnd(false);
                }
            }
        });

        slidingDrawer.setOnDrawerScrollListener(new SlidingDrawer.OnDrawerScrollListener() {
            @Override
            public void onScrollStarted() {

                if (!isOpened) {
                    orgLeftHandler = handle.getLeft();

                    UiComponentUtil.changeViewBuyButtonStartScroll(tvPriceAfterSwipe, tvSweepAfterSwipe,
                            handle, imageViewCartGroup);
                    isOpened = true;
                    needOpenBuy = false;
                    backgroundSliding.setVisibility(View.VISIBLE);
                    contentSliding.setVisibility(View.VISIBLE);

                    if (scrollEventCallBack != null) {
                        scrollEventCallBack.onScrollStarted();
                    }
                }
            }

            @Override
            public void onScrollEnded() {

                isOpened = false;
                //int position = (handle.getLeft() + handle.getRight()) / 2;
                int position =  handle.getLeft();
                int total = frameLayoutSwipeButton.getRight() - handle.getWidth();

                // 70% but view rotated 180o => < 45%
                if (position < total * percentSwipeToBuy) {
                    needOpenBuy = true;
                } else {
                    UiComponentUtil.changeViewBuyButtonEndScroll(tvPriceAfterSwipe, tvSweepAfterSwipe,
                            handle, content, imageViewCartGroup);

                    backgroundSliding.setVisibility(View.GONE);
                    contentSliding.setVisibility(View.GONE);
                }
                if (scrollEventCallBack != null) {
                    scrollEventCallBack.onScrollEnd(needOpenBuy);
                }

                Log.i("tapswipe",position + " " + total);

                if (Math.abs(position - total) < CLICK_RANGE) {
                    buyOpenedCallBack.drawerOpened(parentContext, false);
                }
            }
        });


        handle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                slidingDrawer.setBackground(MyApplication.getContext().getResources().getDrawable(R.drawable.icon_swipe_toggle_on_full));
                handle.setImageResource(R.drawable.icon_swipe_toggle_button);
                content.setImageResource(R.drawable.icon_swipe_toggle_off);

            }
        });

        slidingDrawer.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return false;
            }
        });

    }
}
