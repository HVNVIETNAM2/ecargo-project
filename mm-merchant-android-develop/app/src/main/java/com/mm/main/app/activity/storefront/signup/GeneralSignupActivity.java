package com.mm.main.app.activity.storefront.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mm.main.app.constant.Constant;
import com.mm.main.app.manager.CheckoutFlowManager;

public class GeneralSignupActivity extends AppCompatActivity {

    public enum LoginInAppType {
        TYPE_LOGIN_REQUIRED,
        TYPE_CHECK_OUT_NORMAL,
        TYPE_CHECK_OUT_SWIPE_TO_BUY
    }

    protected LoginInAppType loginInApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getSerializableExtra(Constant.LOGIN_IN_USING_APP_KEY) != null) {
            loginInApp = (LoginInAppType) getIntent().getSerializableExtra(Constant.LOGIN_IN_USING_APP_KEY);

            switch (loginInApp) {
                case TYPE_CHECK_OUT_NORMAL:
                case TYPE_CHECK_OUT_SWIPE_TO_BUY:
                    CheckoutFlowManager.getInstance().addActivityToFlow(this);
                    break;
            }
        }
    }

    protected void startActivityWithLoginKey(Intent intent) {
        intent.putExtra(Constant.LOGIN_IN_USING_APP_KEY, loginInApp);
        startActivity(intent);
    }

    protected void startActivityWithLoginKey(Intent intent, int requestCode) {
        intent.putExtra(Constant.LOGIN_IN_USING_APP_KEY, loginInApp);
        startActivityForResult(intent, requestCode);
    }
}
