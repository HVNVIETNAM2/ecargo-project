package com.mm.main.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mm.main.app.activity.storefront.discover.DiscoverMainActivity;
import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.discover.DiscoverCategoryRVAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.layout.LinearLayoutManagerWithSmoothScroller;
import com.mm.main.app.listitem.DiscoverCategoryListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.schema.Brand;
import com.mm.main.app.schema.Category;
import com.mm.main.app.schema.CategoryBrandMerchant;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.utils.CategoryListUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class DiscoverCategoriesFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private ImageView[] imageViewIndicators;

    RecyclerView bannerRecyclerView;
    List<Category> mainCategoryList;
    DiscoverCategoryRVAdapter discoverCategoryRVAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DiscoverCategoriesFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DiscoverCategoriesFragment newInstance(int columnCount) {
        DiscoverCategoriesFragment fragment = new DiscoverCategoriesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_discover_category_list, container, false);
        ButterKnife.bind(this, view);
        // Set the adapter
        Context context = view.getContext();
        bannerRecyclerView = (RecyclerView) view.findViewById(R.id.list);

        setupBannerRv();
        return view;
    }

    private void setupBannerRv() {
        bannerRecyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(MyApplication.getContext()));

        List<DiscoverCategoryListItem> discoverCategoryListItemSimpleList = new ArrayList<>();

        discoverCategoryRVAdapter = new DiscoverCategoryRVAdapter(discoverCategoryListItemSimpleList, new OnListFragmentInteractionListener() {
            @Override
            public void onDiscoverCategoryFragmentInteraction(Category item) {
                //ToDo search in subCategory
                List<Category> categories = new ArrayList<>();
                categories.add(item);
                SearchStyle.getInstance().clearAllFilter();
                SearchStyle.getInstance().setSubCategoryid(categories);
                ((DiscoverMainActivity) getActivity()).startProductListPage(item, false);
            }

            @Override
            public void onDiscoverCategoryBrandMerchantFragmentInteraction(Category category, CategoryBrandMerchant categoryBrandMerchant) {

                List<Category> categories = new ArrayList<>();
                categories.add(category);
                SearchStyle.getInstance().clearAllFilter();
                SearchStyle.getInstance().setSubCategoryid(categories);
                if (categoryBrandMerchant.getEntity().equals("Brand")) {
                    List<Brand> brandList = new ArrayList<>();
                    Brand brand = new Brand();
                    brand.setBrandName(categoryBrandMerchant.getName());
                    brand.setBrandId(categoryBrandMerchant.getEntityId());
                    brand.setBrandNameInvariant(categoryBrandMerchant.getNameInvariant());
                    brandList.add(brand);
                    SearchStyle.getInstance().setBrandid(brandList);
                } else {
                    List<Merchant> merchantList = new ArrayList<>();
                    Merchant merchant = new Merchant();
                    merchant.setMerchantCompanyName(categoryBrandMerchant.getName());
                    merchant.setMerchantId(categoryBrandMerchant.getEntityId());
                    merchantList.add(merchant);
                    SearchStyle.getInstance().setMerchantid(merchantList);
                }
                ((DiscoverMainActivity) getActivity()).startProductListPage(category, false);

            }
        }, bannerRecyclerView);

        bannerRecyclerView.setAdapter(discoverCategoryRVAdapter);

        fetchCategory(0);
    }

    private void fetchCategory(int userId) {
        MmProgressDialog.show(getActivity());
        APIManager.getInstance().getSearchService().category()
                .enqueue(new MmCallBack<List<Category>>(getActivity()) {
                    @Override
                    public void onSuccess(Response<List<Category>> response, Retrofit retrofit) {
                        mainCategoryList = response.body();
                        mainCategoryList = CategoryListUtil.cleanupCategory(mainCategoryList);
                        renderCategoryList();
                    }
                });
    }

    private void renderCategoryList() {
        List<DiscoverCategoryListItem> discoverCategoryListItemSimpleList = new ArrayList<>();

        for (int i = 0; i < mainCategoryList.size(); i++) {
            Category levelOneCat = mainCategoryList.get(i);
            discoverCategoryListItemSimpleList.add(new DiscoverCategoryListItem(levelOneCat.getCategoryImage(), levelOneCat.getCategoryName(), levelOneCat));
        }

        discoverCategoryRVAdapter.setmValues(discoverCategoryListItemSimpleList);
        discoverCategoryRVAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onDiscoverCategoryFragmentInteraction(Category item);

        void onDiscoverCategoryBrandMerchantFragmentInteraction(Category category, CategoryBrandMerchant item);
    }

    @Override
    public void onResume(){
        super.onResume();
    }
}
