package com.mm.main.app.activity.storefront.im.model;

import android.graphics.Bitmap;

import com.mm.main.app.schema.User;

/**
 * Created by madhur on 17/01/15.
 */
public class Message {

    private String messageText;
    private String userName;
    private User user;
    private UserType userType;
    private Status messageStatus;
    private Bitmap imageMessageBody;

    public long getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(long messageTime) {
        this.messageTime = messageTime;
    }

    private long messageTime;

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public void setMessageStatus(Status messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getMessageText() {

        return messageText;
    }

    public UserType getUserType() {
        return userType;
    }

    public Status getMessageStatus() {
        return messageStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Bitmap getImageMessageBody() {
        return imageMessageBody;
    }

    public void setImageMessageBody(Bitmap imageMessageBody) {
        this.imageMessageBody = imageMessageBody;
    }
}
