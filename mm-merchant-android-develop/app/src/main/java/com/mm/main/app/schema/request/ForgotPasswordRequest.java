package com.mm.main.app.schema.request;

/**
 * Schema class for token
 */
public class ForgotPasswordRequest {

    String UserKey;

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public ForgotPasswordRequest(String userKey) {

        UserKey = userKey;
    }

    public ForgotPasswordRequest() {

    }
}
