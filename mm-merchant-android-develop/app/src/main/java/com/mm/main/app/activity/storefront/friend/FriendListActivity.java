package com.mm.main.app.activity.storefront.friend;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.profile.StoreFrontUserProfileActivity;
import com.mm.main.app.fragment.AllFriendListFragment;
import com.mm.main.app.fragment.DiscoverBrandsFragment;
import com.mm.main.app.fragment.DiscoverCategoriesFragment;
import com.mm.main.app.fragment.RequestFriendListFragment;
import com.mm.main.app.layout.SlidingTabLayout;
import com.mm.main.app.view.CustomViewPager;
import com.mm.main.app.view.MmSearchBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FriendListActivity extends AppCompatActivity implements MmSearchBar.MmSearchBarListener {

    @Bind(R.id.tabs)
    SlidingTabLayout tabs;

    @Bind(R.id.viewpager)
    CustomViewPager viewpager;

    @Bind(R.id.mm_toolbar)
    Toolbar toolbar;

    @Bind(R.id.searchView)
    MmSearchBar searchView;

    MyPageAdapter pageAdapter;
    List<Fragment> fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppTheme_NoActionBar);
        setContentView(R.layout.activity_friend_list);
        ButterKnife.bind(this);

        setupToolbar();

        setupFragments();

        searchView.setMmSearchBarListener(this);
    }

    //TODO : show add friend page.
    @OnClick(R.id.imgAddFriend)
    public void onClickAddFriend() {

        Intent i = new Intent(FriendListActivity.this, FindNewUserActivity.class);
        startActivity(i);
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    //Set up view pager
    private void setupFragments() {
        cleanFragments();
        fragments = getFragments();
        pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
        viewpager.setAdapter(pageAdapter);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.primary1);
            }
        });

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                searchView.cancelSearch();

                Fragment fragment = fragments.get(viewpager.getCurrentItem());

                if(fragment instanceof  AllFriendListFragment){
                    ((AllFriendListFragment) fragment).upDateData();
                }else{
                    ((RequestFriendListFragment) fragment).upDateData();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabs.setViewPager(viewpager);

    }

    private void cleanFragments() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        if (fragmentManager.getFragments() != null) {
            fragmentManager.getFragments().clear();
        }
        if (pageAdapter != null) {
            pageAdapter.notifyDataSetChanged();
        }
    }

    private List<Fragment> getFragments() {

        List<Fragment> fList = new ArrayList<Fragment>();
        fList.add(new AllFriendListFragment());
        fList.add(new RequestFriendListFragment());
        return fList;
    }

    @Override
    public void onEnterText(CharSequence s) {

        String search = s.toString();

        Fragment fragment = fragments.get(viewpager.getCurrentItem());

        if(fragment instanceof  AllFriendListFragment){
            ((AllFriendListFragment) fragment).searchUser(search);
        }else{
            ((RequestFriendListFragment) fragment).searchUser(search);
        }

    }

    @Override
    public void onCancelSearch() {

    }

    class MyPageAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;

        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {


            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return getResources().getString(R.string.LB_CA_ALL);
            } else {
                return getResources().getString(R.string.LB_CA_FRIENDS_REQUEST);
            }
        }

    }


}
