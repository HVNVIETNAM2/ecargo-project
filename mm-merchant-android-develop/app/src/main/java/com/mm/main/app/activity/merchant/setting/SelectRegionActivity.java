package com.mm.main.app.activity.merchant.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import com.mm.main.app.R;
import com.mm.main.app.adapter.merchant.setting.CountrySelectListAdapter;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.Country;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;
import java.util.List;
import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import retrofit.Response;
import retrofit.Retrofit;


public class SelectRegionActivity extends AppCompatActivity {

    final public static int SELECT_COUNTRY_CODE_REQUEST = 1999;
    final public static String SELECTED_COUNTRY_CODE = "SELECTED_COUNTRY_CODE";

    @Bind(R.id.list)
    ListView listView;

    @BindString(R.string.LB_COUNTRY_PICK)
    String title;

    CountrySelectListAdapter countrySelectListAdapter;
    List<Country> countryList;

    @Bind(R.id.rlSearchBar)
    View searchBar;

    @Bind(R.id.searchEditText)
    EditText searchEditText;

    @Bind(R.id.barcodeImageView)
    ImageView barcodeImageView;

    List<Country> filterList;
    Boolean isSearching = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppTheme_NoActionBar);
        setContentView(R.layout.activity_select_region);
        ButterKnife.bind(this);
        showHideButtonSearchCancel(false);
        fetchMobileCode();
        setupSearchView();
    }

    private void setupSearchView() {
        searchBar.setBackgroundResource(R.color.white);
        searchEditText.requestFocus();
        searchEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    proceedSearch(searchEditText.getText().toString());
                    return true;
                }
                return false;
            }
        });
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String text = searchEditText.getText().toString();
                proceedSearch(text);
                if (TextUtils.isEmpty(text)) {
                    showHideButtonSearchCancel(false);
                    isSearching = false;
                } else {
                    showHideButtonSearchCancel(true);
                    barcodeImageView.setImageResource(R.drawable.search_cancel);
                    isSearching = true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void proceedSearch(String searchText) {
        countrySelectListAdapter.getFilter().filter(searchText);
    }

    private void fetchMobileCode() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getGeoService().storefrontCountry()
                .enqueue(new MmCallBack<List<Country>>(this) {
                    @Override
                    public void onSuccess(Response<List<Country>> response, Retrofit retrofit) {
                        countryList = response.body();
                        if (countryList != null) {
                            setupList();
                        }
                        MmProgressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        MmProgressDialog.dismiss();
                    }
                });
    }


    private void setupList() {
        countrySelectListAdapter = new CountrySelectListAdapter(SelectRegionActivity.this, countryList);
        listView.setAdapter(countrySelectListAdapter);
    }

    @OnItemClick(R.id.list)
    public void onItemClicked(int position) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(SELECTED_COUNTRY_CODE, countrySelectListAdapter.getFilteredData().get(position));
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @OnClick(R.id.barcodeImageView)
    public void onClickBarcodeImageView() {
        if (isSearching) {
            showHideButtonSearchCancel(false);
            searchEditText.setText("");
            isSearching = false;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_name, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void finish() {
        super.finish();

        overridePendingTransition(R.anim.not_move, R.anim.left_to_right);
    }

    private void showHideButtonSearchCancel(boolean status) {
        barcodeImageView.setVisibility(status ? View.VISIBLE : View.INVISIBLE);
    }
}
