package com.mm.main.app.listitem;

/**
 * Setting schema
 */
public class BaseListItem {
    String title;
    String content;
    ItemType type;

    public enum ItemType {
        TYPE_PHOTO,
        TYPE_TEXT,
        TYPE_TEXT_ARROW,
        TYPE_RED_TEXT,
        TYPE_REGULAR_TEXT,
        TYPE_DIVIDER
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public BaseListItem() {
    }

    public BaseListItem(String title, String content, ItemType type) {

        this.title = title;
        this.content = content;
        this.type = type;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }
}
