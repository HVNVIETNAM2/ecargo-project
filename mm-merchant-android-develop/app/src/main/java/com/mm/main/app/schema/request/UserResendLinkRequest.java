package com.mm.main.app.schema.request;

/**
 * Schema class for token
 */
public class UserResendLinkRequest {

    String UserId;
    String Link;

    public UserResendLinkRequest() {
    }

    public UserResendLinkRequest(String userId, String link) {
        UserId = userId;
        Link = link;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }
}
