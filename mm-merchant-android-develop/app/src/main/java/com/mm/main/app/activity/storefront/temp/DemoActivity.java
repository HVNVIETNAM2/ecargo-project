package com.mm.main.app.activity.storefront.temp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.EditText;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.mm.main.app.activity.storefront.im.UserChatActivity;
import com.mm.main.app.activity.storefront.setting.UserProfileSettingActivity;
import com.mm.main.app.activity.storefront.signup.MobileSignupProfileActivity;
import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.interest.InterestMappingKeyWordActivity;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;

public class DemoActivity extends AppCompatActivity {

    @Bind(R.id.etHost)
    EditText etHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        ButterKnife.bind(this);

        etHost.setText(MmGlobal.getHost());
    }

    @OnClick(R.id.btSaveHost)
    public void saveHost() {
        MmGlobal.setHost(etHost.getText().toString());
        Toast.makeText(this, MmGlobal.getHost()+" saved", Toast.LENGTH_SHORT).show();
        APIManager.getInstance().refresehAPIManager();
    }

    @OnClick(R.id.demo_signup_profile)
    public void startSignupProfile() {
        startActivity(new Intent(this, MobileSignupProfileActivity.class));
    }

    @OnClick(R.id.demo_interest_tag)
    public void startInterestTag() {
        startActivity(new Intent(this, InterestMappingKeyWordActivity.class));
    }

    @OnClick(R.id.demo_user_profile_setting)
    public void startUserProfileSetting() {
        startActivity(new Intent(this, UserProfileSettingActivity.class));
    }

    @OnClick(R.id.chat_demo_java_websocket2)
    public void startChatDemoJavaWebSocket2() {
        startActivity(new Intent(this, UserChatActivity.class));
    }
}
