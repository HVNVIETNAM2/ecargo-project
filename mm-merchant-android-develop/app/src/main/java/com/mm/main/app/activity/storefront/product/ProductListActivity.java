package com.mm.main.app.activity.storefront.product;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.activity.storefront.cart.ShoppingCartActivity;
import com.mm.main.app.activity.storefront.checkout.CheckoutActivity;
import com.mm.main.app.activity.storefront.discover.DiscoverGroupActivity;
import com.mm.main.app.activity.storefront.discover.DiscoverMainActivity;
import com.mm.main.app.activity.storefront.filter.BrandFilterSelectionActivity;
import com.mm.main.app.activity.storefront.filter.FilterActivity;
import com.mm.main.app.activity.storefront.filter.FilterSelectionActivity;
import com.mm.main.app.activity.storefront.search.NoSearchResultActivity;
import com.mm.main.app.activity.storefront.search.ProductListSearchActivity;
import com.mm.main.app.activity.storefront.wishlist.WishListActivity;
import com.mm.main.app.adapter.strorefront.product.ProductSortingListAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.blurbehind.BlurBehind;
import com.mm.main.app.blurbehind.OnBlurCompleteListener;
import com.mm.main.app.fragment.ProductListFragment;
import com.mm.main.app.helper.ProductFilterHelper;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.schema.*;
import com.mm.main.app.schema.response.SearchResponse;
import com.mm.main.app.service.SearchServiceBuilder;
import com.mm.main.app.utils.*;
import com.mm.main.app.view.MmProgressDialog;
import com.parse.ParseAnalytics;
import com.umeng.analytics.MobclickAgent;

import retrofit.Response;
import retrofit.Retrofit;

import java.util.ArrayList;
import java.util.List;


public class ProductListActivity extends AppCompatActivity implements Controllable {

    public static String TAG = "Product List Activity: ";
    final private static int REQUEST_FILTER = DiscoverGroupActivity.REQUEST_PRODUCT_LIST + 1;
    final private static int REQUEST_SEARCH = REQUEST_FILTER + 1;
    final public static int REQUEST_FRAGMENT_REFRESH = REQUEST_SEARCH + 1;
    final public static int REQUEST_NO_RESULT = REQUEST_FRAGMENT_REFRESH + 1;
    final public static int REQUEST_FRAGMENT_REFRESH_FROM_NO_RESULT = REQUEST_NO_RESULT + 1;
    final public static int REQUEST_BRAND_FILTER_PAGE = REQUEST_FRAGMENT_REFRESH_FROM_NO_RESULT + 1;

    final public static String EXTRA_CATEGORY = "EXTRA_CATEGORY";
    final public static String EXTRA_FROM_SEARCH_PAGE = "EXTRA_FROM_SEARCH_PAGE";

    View icon_heart_fill;

    public enum BackOption {
        NO_REQUEST_SEARCH,
        REQUEST_SEARCH
    }

    private CharSequence mTitle;

    private DrawerLayout mDrawer;
    //    private ActionBarDrawerToggle drawerToggle;
    private Toolbar toolbar;

    private List<Category> mainCategoryList;
    private int currentMainCategory = 0;

    @Bind(R.id.tvSearchWord)
    TextView tvSearchWord;

    @Bind(R.id.ivAllSorting)
    ImageView ivAllSorting;

    @Bind(R.id.llSortingOptionPanel)
    LinearLayout llSortingOptionPanel;

    @Bind(R.id.llSortingOptionList)
    LinearLayout llSortingOptionList;

    @Bind(R.id.lvSortingOption)
    ListView lvSortingOption;

    @Bind((R.id.blurLayer))
    View blurLayer;

    @Bind((R.id.dimLayer))
    View dimLayer;

    @Bind(R.id.tvSortBrandValue)
    TextView tvSortBrandValue;

    @Bind(R.id.tvFilterValue)
    TextView tvFilterValue;

    @Bind(R.id.action_search_box)
    ImageView searchBoxHeader;

    @Bind(R.id.searchBarGroup)
    RelativeLayout searchBarGroup;

    @Bind(R.id.textSearchResult)
    TextView textSearchResult;

    @Bind(R.id.tvDescScore)
    TextView tvDescScore;

    Menu menuTop;

    ProductSortingListAdapter productSortingListAdapter;
    int selectedSort = 0;

    boolean goToCheckout = false;

    boolean isRunningSortPanel = false;

    Category currentCategory;
    boolean isOpenFromSearch;
    List<Brand> brandList;

//    @Bind(R.id.viewpager)
//    public CustomViewPager customViewPager;

//    MyPageAdapter pageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeNoActionBar);
        setContentView(R.layout.activity_product_list);
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        ButterKnife.bind(this);
        currentCategory = (Category) getIntent().getSerializableExtra(EXTRA_CATEGORY);

        isOpenFromSearch = getIntent().getBooleanExtra(EXTRA_FROM_SEARCH_PAGE, false);

        setupSearchWord();
        setupToolbar();

        if (currentCategory != null) {
            if (!isOpenFromSearch) {
                List<Category> categories = new ArrayList<>();
                categories.add(currentCategory);
                SearchStyle.getInstance().setCategoryid(categories);
                ActivityUtil.setTitle(this, currentCategory.getCategoryName());
            } else {
                initSearchBox(currentCategory.getCategoryName());
            }
        } else {
            setTitleSearchResult();
        }
//        setupFragments();
        getCategoryList(0);

    }

    private void initSearchBox(String title) {
        textSearchResult.setText(title);
        searchBarGroup.setVisibility(View.VISIBLE);
        tvSearchWord.setVisibility(View.GONE);
        ActivityUtil.setTitle(this, null);

    }

    private void setTitleSearchResult() {
        String title = "";
        if (SearchStyle.getInstance().getCategoryid() != null &&
                SearchStyle.getInstance().getCategoryid().size() == 1) {
            title = SearchStyle.getInstance().getCategoryid().get(0).getCategoryName();
        } else if (SearchStyle.getInstance().getBrandid() != null &&
                SearchStyle.getInstance().getBrandid().size() == 1) {
            title = SearchStyle.getInstance().getBrandid().get(0).getBrandName();
        } else if (SearchStyle.getInstance().getMerchantid() != null &&
                SearchStyle.getInstance().getMerchantid().size() == 1) {
            title = SearchStyle.getInstance().getMerchantid().get(0).getMerchantCompanyName();
        } else if (!TextUtils.isEmpty(SearchStyle.getInstance().getQueryString())) {
            title = SearchStyle.getInstance().getQueryString();

        }
        if (isOpenFromSearch) {
            initSearchBox(title);
            initMenu();
        } else {
            ActivityUtil.setTitle(this, title);
        }


    }

    //    @OnClick(R.id.fab)
    protected void startFilterActivity() {
        Intent intent = new Intent(this, FilterActivity.class);
        getParent().startActivityForResult(intent, REQUEST_FILTER);
    }

    @OnClick(R.id.dimLayer)
    protected void closeSortMenu() {
        enableAllSortPanel(false);
    }

    @OnClick(R.id.filter_button)
    protected void onClickFilterButton() {
        startFilterActivity();
    }

    @OnClick(R.id.all_sorting_button)
    protected void onClickAllSortingButton() {
        if (!isRunningSortPanel) {
            enableAllSortPanel(!ivAllSorting.isSelected());
        }
    }

    private void enableAllSortPanel(boolean enable) {
        if (!enable) {
            ivAllSorting.setSelected(false);
        } else {
            renderSortOptionsFilterList();
            ivAllSorting.setSelected(true);
        }
        enableSortMenu(enable);
    }

    @OnClick(R.id.brand_sorting_button)
    protected void onClickBrandSortingButton() {
        if (brandList != null) {
            startBrandFilterPage(brandList);
        }
    }

    private void enableSortMenu(boolean enable) {
        if (!enable) {
            final int originalHeight = llSortingOptionPanel.getLayoutParams().height;
            isRunningSortPanel = true;
            AnimateUtil.collapse(llSortingOptionPanel, 2, new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    llSortingOptionPanel.setVisibility(View.GONE);
                    AnimateUtil.fadeOut(dimLayer, new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            dimLayer.setVisibility(View.GONE);

                        }
                    });
                    llSortingOptionPanel.getLayoutParams().height = originalHeight;
                    isRunningSortPanel = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } else {
            dimLayer.setVisibility(View.VISIBLE);
            isRunningSortPanel = true;
            AnimateUtil.fadeIn(dimLayer, new AnimatorListenerAdapter() {

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    llSortingOptionPanel.setVisibility(View.VISIBLE);
                    llSortingOptionList.setVisibility(View.VISIBLE);
                    AnimateUtil.expand(llSortingOptionList, 2, productSortingListAdapter.getCount(), 0, null);
                    isRunningSortPanel = false;
                }
            });
        }
    }

    private void renderSortOptionsFilterList() {
        List<String> filterList = new ArrayList<>();
        filterList.add(StringUtil.getResourceString("LB_CA_SORT_OVERALL"));
        filterList.add(StringUtil.getResourceString("LB_CA_SORT_SALES_VOL"));
        filterList.add(StringUtil.getResourceString("LB_CA_SORT_DATE"));
        filterList.add(StringUtil.getResourceString("LB_CA_SORT_PRICE_ASC"));
        filterList.add(StringUtil.getResourceString("LB_CA_SORT_PRICE_DESC"));
        productSortingListAdapter = new ProductSortingListAdapter(this, filterList, selectedSort);
        lvSortingOption.setDivider(null);
        lvSortingOption.setAdapter(productSortingListAdapter);
    }

    @OnItemClick(R.id.lvSortingOption)
    public void onItemClicked(int position) {
        selectedSort = position;
        productSortingListAdapter.setSelectedItemId(position);
        productSortingListAdapter.notifyDataSetChanged();
        tvDescScore.setText((String) productSortingListAdapter.getItem(position));

        switch (position) {
            case 0:
                SearchStyle.getInstance().setSort(null);
                SearchStyle.getInstance().setOrder(SearchStyle.Order.desc);
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                SearchStyle.getInstance().setSort(SearchStyle.Sort.PriceRetail);
                SearchStyle.getInstance().setOrder(SearchStyle.Order.asc);
                break;
            case 4:
                SearchStyle.getInstance().setSort(SearchStyle.Sort.PriceRetail);
                SearchStyle.getInstance().setOrder(SearchStyle.Order.desc);
                break;
        }

        enableAllSortPanel(false);

        ProductListFragment fragment = (ProductListFragment) getVisibleFragment();
        fragment.refreshStyleList();
    }

    public void startCheckoutActivity(final Style style) {
        final Intent intent = new Intent(ProductListActivity.this, CheckoutActivity.class);
        intent.putExtra(CheckoutActivity.EXTRA_PRODUCT_DATA, style);
        intent.putExtra(CheckoutActivity.ADD_PRODUCT_TO_CART_FLAG, true);

        goToCheckout = true;
        StorefrontMainActivity.animationType = StorefrontMainActivity.AnimationType.CHECKOUT;
        startActivity(intent);
    }

    private void setupSearchWord() {
        tvSearchWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSearchPage();
            }
        });
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//        NavigationView nvDrawer = (NavigationView) findViewById(R.id.nvView);
//        setupDrawerContent(nvDrawer);
//        Menu m = nvDrawer.getMenu();
//        dlDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawerToggle = setupDrawerToggle();
//        dlDrawer.setDrawerListener(drawerToggle);
//        setTitle(m.getItem(0).getTitle());

        tvDescScore.setText(StringUtil.getResourceString("LB_CA_SORT_OVERALL"));
    }

    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getVisibleFragment();
        if (fragment != null) {
            if (requestCode == REQUEST_FILTER) {
                if (resultCode == Activity.RESULT_OK) {
                    ProductFilterHelper.setFullStyleList(null);
                    fragment.onActivityResult(REQUEST_FRAGMENT_REFRESH, resultCode, data);
                }
            } else if (requestCode == REQUEST_SEARCH) {
                if (resultCode == Activity.RESULT_OK) {
                    ProductFilterHelper.setFullStyleList(null);
                    fragment.onActivityResult(REQUEST_FRAGMENT_REFRESH, resultCode, data);

                    isOpenFromSearch = true;
                    setTitleSearchResult();
                    if (getParent() instanceof DiscoverGroupActivity) {
                        DiscoverGroupActivity parentActivity = (DiscoverGroupActivity) getParent();
                        DiscoverMainActivity mainActivity = ((DiscoverMainActivity) (parentActivity.getLocalActivityManager().getActivity(DiscoverGroupActivity.ACTIVITY_ID_DISCOVER_MAIN)));

                        boolean isFromSearch = getIntent().getBooleanExtra(EXTRA_FROM_SEARCH_PAGE, false);

                        mainActivity.setSearchPageResultOpened(isFromSearch);
                    }
                }
            } else if (requestCode == REQUEST_NO_RESULT) {
                if (resultCode == Activity.RESULT_OK) {
                    ProductFilterHelper.setFullStyleList(null);
                    fragment.onActivityResult(REQUEST_FRAGMENT_REFRESH_FROM_NO_RESULT, resultCode, data);
                }
            } else if (requestCode == REQUEST_BRAND_FILTER_PAGE) {
                if (resultCode == Activity.RESULT_OK) {
                    ProductFilterHelper.setFullStyleList(null);
                    Bundle bundle = data.getBundleExtra(BrandFilterSelectionActivity.EXTRA_RESULT);
                    List<Brand> selectedBrand = (List<Brand>) bundle.getSerializable(BrandFilterSelectionActivity.EXTRA_RESULT_DATA);
                    SearchStyle.getInstance().setBrandid(selectedBrand);
                    fragment.onActivityResult(REQUEST_FRAGMENT_REFRESH, resultCode, data);
                }
            } else {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }


    private List<Brand> getBrands(int[] selectedBrand) {
        if (selectedBrand == null || selectedBrand.length <= 0) {
            return null;
        }
        List<Brand> brands = new ArrayList<>();

        for (int i = 0; i < selectedBrand.length; i++) {
            brands.add(brandList.get(selectedBrand[i]));
        }

        return brands;
    }

//    private ActionBarDrawerToggle setupDrawerToggle() {
//        return new ActionBarDrawerToggle(this, dlDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
//    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {

        // Highlight the selected item, update the title, and close the drawer
        setTitle(menuItem.getTitle());
//        mDrawer.closeDrawers();
    }

    private void cleanFragments() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        if (fragmentManager.getFragments() != null) {
            fragmentManager.getFragments().clear();
        }
//        if (pageAdapter != null) {
//            pageAdapter.notifyDataSetChanged();
//        }
    }

    private void setupFragments() {
        cleanFragments();
//        List<Fragment> fragments = getFragments();
//        pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
//        ViewPager pager = (ViewPager) findViewById(R.id.viewpager);
//
//        pager.setAdapter(pageAdapter);


        // Assiging the Sliding Tab Layout View
//        SlidingTabLayout tabs = (SlidingTabLayout) findViewById(R.id.tabs);
//        tabs.setDistributeEvenly(false); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
//
//        // Setting Custom Size for the Scroll bar indicator of the Tab View
//        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
//            @Override
//            public int getIndicatorColor(int position) {
//                return getResources().getColor(R.color.primary1);
//            }
//        });
//
//        // Setting the ViewPager For the SlidingTabsLayout
//        tabs.setViewPager(pager);

    }

    private void addFragment() {
        final FragmentManager fragmentManager = this.getSupportFragmentManager();
        new Handler().post(new Runnable() {
            public void run() {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment productListFragment = getProductListFragment();
                fragmentTransaction.add(R.id.fragment_container, productListFragment, "productListFragment");

                // change commit() to commitAllowingStateLoss
                fragmentTransaction.commitAllowingStateLoss();

            }
        });
    }

    private List<Fragment> getFragments() {

        List<Fragment> fList = new ArrayList<Fragment>();
        Category subCategory = mainCategoryList.get(currentMainCategory);
        List<Category> subCategoryList = subCategory.getCategoryList();
        fList.add(ProductListFragment.newInstance("Fragment " + 0, 0, subCategory.getCategoryId(), true));
        for (int i = 0; i < subCategoryList.size(); i++) {
            Category currentCategory = subCategoryList.get(i);
            fList.add(ProductListFragment.newInstance("Fragment " + i + 1, 0, currentCategory.getCategoryId()));
        }

        return fList;
    }

    private Fragment getProductListFragment() {
        Category subCategory = mainCategoryList.get(currentMainCategory);
        return ProductListFragment.newInstance("Fragment " + 0, 0, subCategory.getCategoryId(), true);
    }

    private void setupNavigationDropDownList() {
        // Set up the action bar to show a dropdown list.
//        final ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayShowTitleEnabled(false);
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
//
//        ArrayList<String> itemList = new ArrayList<>();
//        for (int i = 0; i < mainCategoryList.size(); i++) {
//            itemList.add(mainCategoryList.get(i).getCategoryName());
//        }
//
//        ArrayAdapter<String> aAdpt = new ArrayAdapter<String>(this, R.layout.navigation_drop_down_list, android.R.id.text1, itemList);
//
//        aAdpt.setDropDownViewResource(R.layout.navigation_drop_down_list2);
//
//        actionBar.setListNavigationCallbacks(aAdpt, new ActionBar.OnNavigationListener() {
//            @Override
//            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
//                currentMainCategory = itemPosition;
//                setupFragments();
//                return false;
//            }
//        });
    }

    private void initMenu() {
        if (menuTop != null) {
            MenuItem item = menuTop.findItem(R.id.action_search);

            if (isOpenFromSearch) {
                item.setVisible(false);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_recycle, menu);
        menuTop = menu;
        initMenu();

        // Associate searchable configuration with the SearchView
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
//                .getActionView();
//        searchView.setSearchableInfo(searchManager
//                .getSearchableInfo(getComponentName()));
//        SearchViewUtil.setSearchHintIcon(searchView, R.drawable.btn_search);
//        SearchViewUtil.setSearchHintCloseIcon(searchView, R.drawable.btn_close);
//        SearchViewUtil.setText(searchView, getResources().getColor(R.color.mm_input_gray), 14, "");
//        SearchViewUtil.setSearchBackIcon(searchView, R.drawable.dummy_return);
//        Display display = getWindowManager().getDefaultDisplay();
//        int maxWidth = display.getWidth();  // deprecated
//        maxWidth -= TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
//
//        searchView.setMaxWidth(maxWidth);

        //for heart icon
        MenuItem item = menu.findItem(R.id.action_like);
        MenuItemCompat.setActionView(item, R.layout.badge_button_like);

        RelativeLayout badgeLayout = (RelativeLayout) MenuItemCompat.getActionView(item);
        icon_heart_fill = (View) badgeLayout.findViewById(R.id.icon_heart_fill);
        View badge = (View) badgeLayout.findViewById(R.id.btnBadge);

        if (CacheManager.getInstance().isWishlistHasItem()) {
            badge.setVisibility(View.VISIBLE);
        } else {
            badge.setVisibility(View.GONE);
        }

        item.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductListActivity.this, WishListActivity.class);
                startActivity(i);
            }
        });


        //for cart icon
        MenuItem cartItem = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(cartItem, R.layout.badge_button_cart);

        RelativeLayout badgeCartLayout = (RelativeLayout) MenuItemCompat.getActionView(cartItem);
        View cartBadge = (View) badgeCartLayout.findViewById(R.id.btnBadge);

        if (CacheManager.getInstance().isCartHasItem()) {
            cartBadge.setVisibility(View.VISIBLE);
        } else {
            cartBadge.setVisibility(View.GONE);
        }

        cartItem.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductListActivity.this, ShoppingCartActivity.class);
                startActivity(i);
            }
        });


        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (drawerToggle.onOptionsItemSelected(item)) {
//            return true;
//        }

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_search) {
            startSearchPage();
        } else if (id == R.id.action_cart) {
            Intent i = new Intent(this, ShoppingCartActivity.class);
            startActivity(i);
        } else if (id == R.id.action_like) {
            Intent i = new Intent(this, WishListActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    private void startSearchPage() {
        final Intent intent = new Intent(this, ProductListSearchActivity.class);
        BlurBehind.getInstance().execute(this, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                StorefrontMainActivity.animationType = StorefrontMainActivity.AnimationType.NONE;
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                getParent().startActivityForResult(intent, REQUEST_SEARCH);
            }
        });
    }

    public void startNoSearchResultPage() {
        final Intent intent = new Intent(this, NoSearchResultActivity.class);
        BlurBehind.getInstance().execute(this, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                StorefrontMainActivity.animationType = StorefrontMainActivity.AnimationType.NONE;
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                getParent().startActivityForResult(intent, REQUEST_NO_RESULT);
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        drawerToggle.syncState();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
//        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void updateFilterNumber() {
        int brandFilterNumber = SearchStyle.getInstance().getBrandFilterNumber();
        int totalFilterNumber = SearchStyle.getInstance().getTotalFilterNumber();

        if (brandFilterNumber >= 0) {
            tvSortBrandValue.setText(" (" + brandFilterNumber + ")");
        } else {
            tvSortBrandValue.setText("");
        }

        if (totalFilterNumber >= 0) {
            tvFilterValue.setText(" (" + totalFilterNumber + ")");
        } else {
            tvFilterValue.setText("");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        MobclickAgent.onEvent(MyApplication.getContext(), "Recycle View of Users");

        blurLayer.setVisibility(View.GONE);
        updateSearchWord();
        getSupportActionBar().show();
        fetchAggregations();
        updateFilterNumber();

        this.invalidateOptionsMenu();
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }


    private void getCategoryList(Integer userId) {
        MmProgressDialog.show(this);
        APIManager.getInstance().getCategoryService().list(userId)
                .enqueue(new MmCallBack<List<Category>>(ProductListActivity.this) {
                    @Override
                    public void onSuccess(Response<List<Category>> response, Retrofit retrofit) {
                        mainCategoryList = response.body();
                        mainCategoryList = filterCategory(mainCategoryList);
                        setupNavigationDropDownList();
                        setTitle(mainCategoryList.get(0).getCategoryName());
//                        setupFragments();
                        addFragment();
                    }
                });

    }

    private List<Category> filterCategory(List<Category> oldCategoryList) {
        List<Category> newCategoryList = new ArrayList<>();

        Category allCategory = new Category();
        allCategory.setCategoryId(-1);
        allCategory.setCategoryName(getResources().getString(R.string.LB_CA_ALL));
        newCategoryList.add(allCategory);

        for (Category category :
                oldCategoryList) {
            if (!category.getCategoryName().equals("---")) {
                newCategoryList.add(category);
            }
        }
        return newCategoryList;
    }

    @Override
    public void resume() {
        onResume();
    }


//    class MyPageAdapter extends FragmentPagerAdapter {
//
//        private List<Fragment> fragments;
//
//        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
//            super(fm);
//            this.fragments = fragments;
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return this.fragments.get(position);
//        }
//
//        @Override
//        public int getCount() {
//            return this.fragments.size();
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            if (mainCategoryList != null) {
//                if (position == 0) {
//                    return getResources().getString(R.string.LB_CA_ALL);
//                } else {
//                    return mainCategoryList.get(currentMainCategory).getCategoryList().get(position - 1).getCategoryName();
//                }
//            }
//
//            return "";
//        }
//
//    }

    private void startBrandFilterPage(List<Brand> brandList) {
        List<Brand> selectedBrand = SearchStyle.getInstance().getBrandid();
        Intent intent = new Intent(this, BrandFilterSelectionActivity.class);
        Bundle bundle = new Bundle();
        List<Integer> selected = new ArrayList<>();
        ArrayList<FilterListItem> brandArrayList = new ArrayList<>();
        for (int i = 0; i < brandList.size(); i++) {
            brandArrayList.add(new FilterListItem(brandList.get(i)));
            if (SearchStyle.getInstance().getBrandid() != null) {
                for (int j = 0; j < SearchStyle.getInstance().getBrandid().size(); j++) {
                    if (SearchStyle.getInstance().getBrandid().get(j).getBrandId() == brandList.get(i).getBrandId()) {
                        selected.add(i);
                    }
                }
            }
        }

        if (selectedBrand != null) {
            for (int i = 0; i < selectedBrand.size(); i++) {
                for (int j = 0; j < brandArrayList.size(); j++) {
                    if (selectedBrand.get(i).getBrandId().intValue() == brandList.get(j).getBrandId().intValue()) {
                        brandArrayList.get(j).setSelected(true);
                        break;
                    }
                }
            }
        }

        bundle.putSerializable(BrandFilterSelectionActivity.EXTRA_BRAND_DATA, brandArrayList);
        intent.putExtra(BrandFilterSelectionActivity.EXTRA_HEADER, getResources().getString(R.string.LB_CA_BRAND));
        intent.putExtra(FilterSelectionActivity.EXTRA_SELECTIONS, bundle);
        getParent().startActivityForResult(intent, REQUEST_BRAND_FILTER_PAGE);

    }

    private void fetchBrand(final Aggregations aggregations) {
        APIManager.getInstance().getBrandService().list()
                .enqueue(new MmCallBack<List<Brand>>(this) {
                    @Override
                    public void onSuccess(Response<List<Brand>> response, Retrofit retrofit) {
                        brandList = ProductFilterHelper.filterBrandId(response.body(),
                                aggregations.getBrandArray());

                    }
                });
    }

    private void fetchAggregations() {
        SearchServiceBuilder.searchStyleWithFilterAllBrand(new MmCallBack<SearchResponse>(this) {
            @Override
            public void onSuccess(Response<SearchResponse> response, Retrofit retrofit) {
                Aggregations aggregations = response.body().getAggregations();
                fetchBrand(aggregations);
            }
        });
    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    private void updateSearchWord() {
        String query = SearchStyle.getInstance().getQueryString();
        if (!TextUtils.isEmpty(query)) {
//            tvSearchWord.setVisibility(View.VISIBLE);
//            tvSearchWord.setText(query);
        } else {
            tvSearchWord.setVisibility(View.INVISIBLE);
            tvSearchWord.setText("");
        }
    }

    @OnClick(R.id.searchBarGroup)
    public void onSearchBarClicked() {
        startSearchPage();
    }

    @Override
    public void onBackPressed() {
        // handler back event - fix issue MM-2798
        DiscoverGroupActivity.group.back();
        return;
    }

    public void animateHeartIcon() { //in action bar
        Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zoom_in_heart);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoom_out_heart);

        icon_heart_fill.setVisibility(View.VISIBLE);
        icon_heart_fill.startAnimation(zoomin);
        zoomin.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                icon_heart_fill.startAnimation(zoomout);
                zoomout.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        icon_heart_fill.setVisibility(View.GONE);
                        ProductListActivity.this.invalidateOptionsMenu();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
