package com.mm.main.app.listitem;

/**
 * Created by ductranvt on 1/19/2016.
 */
public interface CheckoutConfirmRvItem {
    public enum ItemType {
        TYPE_SHIPPING_ADDRESS,
        TYPE_MERCHANT,
        TYPE_PRODUCT,
        TYPE_MERCHANT_TOTAL_PRICE
    }

    ItemType getType();
}
