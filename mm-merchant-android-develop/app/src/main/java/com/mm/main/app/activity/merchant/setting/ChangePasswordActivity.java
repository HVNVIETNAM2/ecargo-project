package com.mm.main.app.activity.merchant.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.mm.main.app.R;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.factory.ValidatorFactory;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.concurrent.Callable;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;


public class ChangePasswordActivity extends ActionBarActivity {

    final public static String EXTRA_USER_DETAIL = "USER_DETAIL";

    @Bind(R.id.oldPasswordEditText)
    EditText oldPasswordEditText;
    @Bind(R.id.newPasswordEditText)
    EditText newPasswordEditText;
    @Bind(R.id.confirmPasswordEditText)
    EditText confirmPasswordEditText;

    @Bind(R.id.oldPasswordLayout)
    TextInputLayout oldPasswordLayout;
    @Bind(R.id.newPasswordLayout)
    TextInputLayout newPasswordLayout;
    @Bind(R.id.confirmPasswordLayout)
    TextInputLayout confirmPasswordLayout;

    @BindString(R.string.MSG_SUC_PASSWORD_CHANGE)
    String changeSucMessage;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, getResources().getString(R.string.LB_CHANGE_PASSWORD));
        user = (User) this.getIntent().getSerializableExtra(Constant.Extra.EXTRA_USER_DETAIL);

        setupValidation();

    }

    private void setupValidation() {
        oldPasswordEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateOldPassword();
            }
        }, oldPasswordLayout));

        newPasswordEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateNewPassword();
            }
        }, newPasswordLayout));

        confirmPasswordEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateConfirmNewPassword();
            }
        }, confirmPasswordLayout));
    }


    private void render() {
    }

    @OnClick(R.id.confirm_button)
    public void changePassword() {
        if (validateOldPassword() & validateNewPassword() & validateConfirmNewPassword() & matchPasswords()) {
            user.setPassword(newPasswordEditText.getText().toString());
            user.setPasswordOld(oldPasswordEditText.getText().toString());
            MmProgressDialog.show(this);
            APIManager.getInstance().getUserService().changePassword(user)
                    .enqueue(new MmCallBack<Boolean>(ChangePasswordActivity.this) {
                        @Override
                        public void onSuccess(Response response, Retrofit retrofit) {
                            Toast.makeText(ChangePasswordActivity.this, changeSucMessage, Toast.LENGTH_SHORT).show();
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    });
        }
    }

    @OnClick(R.id.cancel_button)
    public void cancelChange() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        render();
    }

    private boolean validateOldPassword() {
        String error = null;
        String fieldName = getResources().getString(R.string.LB_OLD_PASSWORD);
        String password = oldPasswordEditText.getText().toString();

        if (!ValidationUtil.validate(ValidationUtil.PASSWORD_REGEX, password)) {
            error = getResources().getString(R.string.MSG_ERR_FIELDNAME_PATTERN)
                    .replace(Constant.TEXT_REPLACE_FIELD_NAME, fieldName);
        }
        if (password.trim().isEmpty()) {
            error = getResources().getString(R.string.MSG_ERR_RESET_OLD_PASSWORD_WRONG);
        }

        ValidationUtil.setErrorMessage(oldPasswordLayout, error);

        return error == null;
    }

    private boolean validateNewPassword() {
        String error = null;
        String fieldName = getResources().getString(R.string.LB_NEW_PASSWORD);
        String password = newPasswordEditText.getText().toString();

        if (!ValidationUtil.validate(ValidationUtil.PASSWORD_REGEX, password)) {
            error = getResources().getString(R.string.MSG_ERR_FIELDNAME_PATTERN)
                    .replace(Constant.TEXT_REPLACE_FIELD_NAME, fieldName);
        }
        if (password.trim().isEmpty()) {
            error = getResources().getString(R.string.MSG_ERR_RESET_PASSWORD_NIL);
        }

        ValidationUtil.setErrorMessage(newPasswordLayout, error);

        return error == null;
    }

    private boolean validateConfirmNewPassword() {
        String error = null;
        String fieldName = getResources().getString(R.string.LB_CONF_NEW_PASSWORD);
        String password = confirmPasswordEditText.getText().toString();

        if (!ValidationUtil.validate(ValidationUtil.PASSWORD_REGEX, password)) {
            error = getResources().getString(R.string.MSG_ERR_FIELDNAME_PATTERN)
                    .replace(Constant.TEXT_REPLACE_FIELD_NAME, fieldName);
        }
        if (password.trim().isEmpty()) {
            error = getResources().getString(R.string.MSG_ERR_RESET_PASSWORD_REENTER_NIL);
        }

        ValidationUtil.setErrorMessage(confirmPasswordLayout, error);

        return error == null;
    }

    private boolean matchPasswords() {
        if (!newPasswordEditText.getText().toString().equals(confirmPasswordEditText.getText().toString())) {
            ErrorUtil.showErrorDialog(this, null, getResources().getString(R.string.MSG_ERR_PASSWORD_REENTER_NOT_MATCH));
            return false;
        }

        return true;
    }
}
