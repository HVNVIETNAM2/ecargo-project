package com.mm.main.app.adapter.strorefront.discover;

import android.graphics.Bitmap;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.decoration.GridSpacingItemDecoration;
import com.mm.main.app.fragment.DiscoverCategoriesFragment;
import com.mm.main.app.listitem.DiscoverCategoryListItem;
import com.mm.main.app.schema.Category;
import com.mm.main.app.schema.CategoryBrandMerchant;
import com.mm.main.app.utils.AnimateUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DiscoverCategoryRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DiscoverCategoryListItem> mValues;
    private final DiscoverCategoriesFragment.OnListFragmentInteractionListener mListener;
    private DiscoverSubCategoryRVAdapter discoverBrandRVAdapter;
    private RecyclerView recyclerView;
    private boolean isClickOnItem;
    private int openingPosition = -1;

    public DiscoverCategoryRVAdapter(List<DiscoverCategoryListItem> items,
                                     DiscoverCategoriesFragment.OnListFragmentInteractionListener listener,
                                     RecyclerView recyclerView) {
        mValues = items;
        mListener = listener;
        this.recyclerView = recyclerView;

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if(isClickOnItem && openingPosition > -1) {
                        isClickOnItem = false;
                        updateOpenStatus(openingPosition);
                        recyclerView.smoothScrollToPosition(openingPosition);
                    }
                }
            }

        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.discover_category_close_item_view, parent, false);

            return new CloseViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.discover_category_open_item_view, parent, false);

            return new OpenViewHolder(view);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return mValues.get(position).isOpen() ? 1 : 0;
    }

    private void updateOpenStatus(int position) {

        for (int i = 0; i < mValues.size(); i++) {
            if (position != i) {
                boolean isOpen = mValues.get(i).isOpen();
                mValues.get(i).setOpen(false);
                if(isOpen){
                    notifyItemChanged(i);

                }
            } else {
                mValues.get(i).setOpen(true);
                notifyItemChanged(i);
            }
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final DiscoverCategoryListItem mItem = mValues.get(position);

        if (!mItem.isOpen()) {
            final CloseViewHolder closeViewHolder = (CloseViewHolder) holder;
            Picasso.with(MyApplication.getContext()).load(PathUtil.getCategoryImageUrl(mItem.getKey())).resize(UiUtil.getDisplayWidth(), UiUtil.getPixelFromDp(80)).centerCrop().into(closeViewHolder.bannerImageView);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Bitmap openBanner = Picasso.with(MyApplication.getContext()).load(PathUtil.getCategoryImageUrl(mItem.getKey())).resize(UiUtil.getDisplayWidth(), UiUtil.getPixelFromDp(140)).centerCrop().get();
                        mItem.setOpenedImage(openBanner);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            closeViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mItem.isOpen()) {
                        // close opening item
                        if(openingPosition > -1) {
                            mValues.get(openingPosition).setOpen(false);
                            notifyItemChanged(openingPosition);
                        }

                        // scroll to
                        openingPosition = position;
                        if(openingPosition > 0){
                            recyclerView.smoothScrollToPosition(position);
                        }else{
                            mValues.get(openingPosition).setOpen(true);
                            notifyItemChanged(openingPosition);
                        }


                        isClickOnItem = true;
                    } else {
                        openingPosition = -1;
                        mItem.setOpen(!mItem.isOpen());
                        notifyDataSetChanged();
                    }
                }
            });



            closeViewHolder.tvCategoryTitle.setText(mItem.getTitle());
        } else {
            OpenViewHolder openViewHolder = (OpenViewHolder) holder;
            openViewHolder.bannerImageView.setImageBitmap(mItem.getOpenedImage());

            discoverBrandRVAdapter.setItemList(mItem.getCategory().getCategoryList());
            discoverBrandRVAdapter.notifyDataSetChanged();

            List<CategoryBrandMerchant> categoryBrandMerchantList = mItem.getCategory().getCategoryBrandMerchantList();
            ImageView[] logos = new ImageView[categoryBrandMerchantList.size()];
            openViewHolder.llLogo.removeAllViews();
            for (int i = 0; i < logos.length; i++) {
                final CategoryBrandMerchant categoryBrandMerchant = categoryBrandMerchantList.get(i);
                logos[i] = new ImageView(MyApplication.getContext());
                int rightPadding = 0;
                int width = (UiUtil.getDisplayWidth() - UiUtil.getPixelFromDp(5)) / 5;
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        width,
                        UiUtil.getPixelFromDp(OpenViewHolder.logoSize)
                );
                params.setMargins(UiUtil.getPixelFromDp(1), 0, rightPadding, UiUtil.getPixelFromDp(1));
                logos[i].setLayoutParams(params);
                logos[i].setId(i);

                logos[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            mListener.onDiscoverCategoryBrandMerchantFragmentInteraction(mItem.getCategory(), categoryBrandMerchant);
                        }
                    }
                });

                String path;
                if (categoryBrandMerchant.getEntity().equals("Brand")) {
                    path = PathUtil.getBrandImageUrl(categoryBrandMerchant.getSmallLogoImage(), width);
                } else {
                    path = PathUtil.getMerchantImageUrl(categoryBrandMerchant.getSmallLogoImage(), width);
                }

                Picasso.with(MyApplication.getContext()).load(path).into(logos[i]);

                openViewHolder.llLogo.addView(logos[i]);

            }

            AnimateUtil.expand(openViewHolder.rlLowerDetail, new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    //recyclerView.smoothScrollToPosition(position);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    //recyclerView.smoothScrollToPosition(position);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });

            openViewHolder.bannerImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mItem.isOpen()) {
                        updateOpenStatus(position);
                    } else {
                        mItem.setOpen(!mItem.isOpen());
                    }
                    notifyDataSetChanged();
                }
            });

            openViewHolder.tvCategoryTitle.setText(mItem.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class CloseViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView bannerImageView;
        public DiscoverCategoryListItem mItem;
        public final TextView tvCategoryTitle;

        public CloseViewHolder(View view) {
            super(view);
            mView = view;
            bannerImageView = (ImageView) view.findViewById(R.id.bannerImageView);
            tvCategoryTitle = (TextView) view.findViewById(R.id.tvCategoryTitle);
        }
    }

    public class OpenViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView bannerImageView;
        public final LinearLayout llLogo;
        public static final int logoSize = 30;
        public RecyclerView rvSubCategory;
        public RelativeLayout rlLowerDetail;
        public final TextView tvCategoryTitle;

        public OpenViewHolder(View view) {
            super(view);
            mView = view;
            rlLowerDetail = (RelativeLayout) view.findViewById(R.id.rlLowerDetail);
            bannerImageView = (ImageView) view.findViewById(R.id.bannerImageView);
            llLogo = (LinearLayout) view.findViewById(R.id.llLogo);
            rvSubCategory = (RecyclerView) view.findViewById(R.id.rvSubCategory);
            tvCategoryTitle = (TextView) view.findViewById(R.id.tvCategoryTitle);

            setupBrandRv(rvSubCategory);


        }

        private void setupBrandRv(RecyclerView recyclerView) {
            GridLayoutManager glm = new GridLayoutManager(MyApplication.getContext(), 4);
            recyclerView.setLayoutManager(glm);
            recyclerView.setClipToPadding(false);
            recyclerView.setClipChildren(false);

            List<Category> subCategoryList = new ArrayList<>();
            discoverBrandRVAdapter = new DiscoverSubCategoryRVAdapter(subCategoryList, new DiscoverSubCategoryRVAdapter.SubCategoryOnClickListener() {
                @Override
                public void onClick(int position, Object data) {
                    if (null != mListener) {
                        // Notify the active callbacks interface (the activity, if the
                        // fragment is attached to one) that an item has been selected.
                        mListener.onDiscoverCategoryFragmentInteraction((Category) data);
                    }
                }
            });
            recyclerView.setAdapter(discoverBrandRVAdapter);
            int spacingInPixels = UiUtil.getPixelFromDp(2);
            recyclerView.addItemDecoration(new GridSpacingItemDecoration(4, spacingInPixels, true));

            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(UiUtil.getDisplayWidth(),
                    UiUtil.getPixelFromDp(100));
            p.addRule(RelativeLayout.BELOW, R.id.horizontal_scroll);
            recyclerView.setLayoutParams(p);

        }
    }

    public List<DiscoverCategoryListItem> getmValues() {
        return mValues;
    }

    public void setmValues(List<DiscoverCategoryListItem> mValues) {
        this.mValues = mValues;
    }
}
