package com.mm.main.app.listitem;

import com.mm.main.app.schema.User;

/**
 * Created by haivu on 3/4/2016.
 */
public class FriendRequestRvItem {

    private User user;
    private int typeAction; // 0 - none, 1-delete, 2- accept

    public FriendRequestRvItem() {
        typeAction = 0;
    }

    public int getTypeAction() {
        return typeAction;
    }

    public void setTypeAction(int typeAction) {
        this.typeAction = typeAction;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
