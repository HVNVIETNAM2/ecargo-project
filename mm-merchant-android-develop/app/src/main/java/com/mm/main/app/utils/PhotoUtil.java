package com.mm.main.app.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;

import com.alexbbb.uploadservice.MultipartUploadRequest;
import com.mm.main.app.activity.storefront.signup.MobileSignupProfileActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.schema.request.IdentificationSaveRequest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ductranvt on 2/22/2016.
 */
public class PhotoUtil {

    public static int MAXSIZE = 800;

    public enum PhotoType {
        PROFILE, COVER
    }

    public static Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > MAXSIZE || rotatedHeight > MAXSIZE) {
            float widthRatio = ((float) rotatedWidth) / ((float) MAXSIZE);
            float heightRatio = ((float) rotatedHeight) / ((float) MAXSIZE);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

    /*
     * if the orientation is not 0 (or -1, which means we don't know), we
     * have to do a rotation.
     */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        return srcBitmap;
    }

    public static int getOrientation(Context context, Uri photoUri) {
    /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);
        if (cursor == null) {
            return 0;
        }
        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public static void uploadPhoto(Bitmap photo, PhotoType type) {
        String path = "";
        if (type == PhotoType.COVER) {
            path = Constant.getApiURL() + "user/upload/coverimage";
        } else if (type == PhotoType.PROFILE) {
            path = Constant.getApiURL() + "user/upload/profileimage";
        }
        File f = new File(MyApplication.getContext().getCacheDir(), "iosFile.jpg");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int compressRatio = 100;
        if (photo.getWidth() > MAXSIZE) {
            compressRatio = (MAXSIZE * compressRatio / photo.getWidth());
        }

        photo.compress(Bitmap.CompressFormat.JPEG, compressRatio, bos);
        byte[] bitmapData = bos.toByteArray();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(bitmapData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        final MultipartUploadRequest request =
                new MultipartUploadRequest(MyApplication.getContext(),
                        Constant.UploadImageId.STORE_FRONT_PROFILE_PHOTO,
                        path);
        request.addFileToUpload(f.getPath(),
                "file",
                "androidImg.jpg",
                "image/jpeg");

        request.addHeader("Authorization", MmGlobal.getToken());
        request.setNotificationConfig(android.R.drawable.ic_menu_upload,
                "notification title",
                "upload in progress text",
                "upload completed successfully text",
                "upload error text",
                false);

        // set the maximum number of automatic upload retries on error
        request.setMaxRetries(2);

        try {
            //Start upload service and display the notification
            request.startUpload();

        } catch (Exception exc) {
            //You will end up here only if you pass an incomplete upload request
            Logger.e("AndroidUploadService", exc.getLocalizedMessage(), exc);
        }
    }

    private static File cacheFile(String name, Bitmap photo) {

        File f = new File(MyApplication.getContext().getCacheDir(), "iosFile.jpg");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int compressRatio = 100;
        if (photo.getWidth() > MAXSIZE) {
            compressRatio = (MAXSIZE / photo.getWidth()) * compressRatio;
        }

        photo.compress(Bitmap.CompressFormat.JPEG, compressRatio, bos);
        byte[] bitmapData = bos.toByteArray();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(bitmapData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    public static void uploadIDPhotos(IdentificationSaveRequest idInfo) {
        String path = Constant.getApiURL() + "identification/save";

        String frontName = "frontImage.jpg";
        String backName = "backImage.jpg";
        File frontFile = cacheFile(frontName, idInfo.getFrontImage());
        File backFile = cacheFile(backName, idInfo.getBackImage());

        final MultipartUploadRequest request =
                new MultipartUploadRequest(MyApplication.getContext(),
                        Constant.UploadImageId.ID_PHOTO,
                        path);
        request.addFileToUpload(frontFile.getPath(),
                "FrontImage",
                frontName,
                "image/jpeg");
        request.addFileToUpload(backFile.getPath(),
                "BackImage",
                backName,
                "image/jpeg");
        request.addParameter("UserKey", idInfo.getUserKey());
        request.addParameter("OrderKey", idInfo.getOrderKey());
        request.addParameter("FirstName", idInfo.getFirstName());
        request.addParameter("LastName", idInfo.getLastName());
        request.addParameter("IdentificationNumber", idInfo.getIdentificationNumber());

        request.addHeader("Authorization", MmGlobal.getToken());
        request.setNotificationConfig(android.R.drawable.ic_menu_upload,
                "notification title",
                "upload in progress text",
                "upload completed successfully text",
                "upload error text",
                false);

        // set the maximum number of automatic upload retries on error
        request.setMaxRetries(2);

        try {
            //Start upload service and display the notification
            request.startUpload();

        } catch (Exception exc) {
            //You will end up here only if you pass an incomplete upload request
            Logger.e("AndroidUploadService", exc.getLocalizedMessage(), exc);
        }
    }
}
