package com.mm.main.app.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.DynamicAvailableListItem;
import com.mm.main.app.schema.ColorImage;
import com.mm.main.app.schema.ProductColor;
import com.mm.main.app.schema.ProductSize;
import com.squareup.picasso.Picasso;
import com.wefika.flowlayout.FlowLayout;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by henrytung on 1/12/2015.
 */
public class UiUtil {

    public static final float productImgSizeRatio = (float) 7 / 6;

    public static final int CURATOR_ITEM_HEIGHT = 120;

    public static int getPixelFromDp(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, MyApplication.getContext().getResources().getDisplayMetrics());
    }

    public static int getPixelFromDp(float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, MyApplication.getContext().getResources().getDisplayMetrics());
    }

    public static LinearLayout.LayoutParams getLinearLayoutLayoutParams(int width, int height) {
        return new LinearLayout.LayoutParams(getPixelFromDp(width), getPixelFromDp(height));
    }

    public static RelativeLayout.LayoutParams getRelativeLayoutLayoutParams(int width, int height) {
        return new RelativeLayout.LayoutParams(getPixelFromDp(width), getPixelFromDp(height));
    }

    public static int getAspectHeightOfDescImage(int width) {
        return width * 100 * 492 / 370 / 100;
    }

    public static int getAspectWidthOfProductImage() {
        return (UiUtil.getDisplayWidth() - UiUtil.getPixelFromDp(15)) / 2;//15dp is  5dp(margin left) +  5dp (gap between two image) + 5dp (margin right)
    }

    public static int getAspectHeightOfSuggestionImage(int width) {
        //return width*100*1090/720/100;//Remove because product cell's height is now change the way to calculate as following
        return (int) (getAspectWidthOfProductImage() * productImgSizeRatio + UiUtil.getPixelFromDp(125)); // 125dp is the height of bottom view
    }

    public static Point getDisplayPoint() {
        WindowManager wm = (WindowManager) MyApplication.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static int getDisplayWidth() {
        return getDisplayPoint().x;
    }

    public static int getHeightByWidthWithRatio(int width, float ratio) {
        return (int) (width * ratio);
    }

    public static int getDisplayHeight() {
        return getDisplayPoint().y;
    }

    public static Drawable createMarkerIcon(Drawable backgroundImage, String text,
                                            int width, int height) {

        Bitmap canvasBitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        // Create a canvas, that will draw on to canvasBitmap.
        Canvas imageCanvas = new Canvas(canvasBitmap);

        // Set up the paint for use with our Canvas
        Paint imagePaint = new Paint();
        imagePaint.setTextAlign(Paint.Align.CENTER);
        imagePaint.setTextSize(16f);

        // Draw the image to our canvas
        backgroundImage.draw(imageCanvas);

        // Draw the text on top of our image
        imageCanvas.drawText(text, width / 2, height / 2, imagePaint);

        // Combine background and text to a LayerDrawable
        LayerDrawable layerDrawable = new LayerDrawable(
                new Drawable[]{backgroundImage, new BitmapDrawable(canvasBitmap)});
        return layerDrawable;
    }

    public static int getNumberColumnGrid(int paddingInDp, int itemSizeInDp) {
        int width = UiUtil.getDisplayWidth();
        int padding = UiUtil.getPixelFromDp(paddingInDp);
        int itemSize = UiUtil.getPixelFromDp(itemSizeInDp);
        int numberColumn = (int) Math.floor((width - padding) / itemSize);
        return numberColumn;
    }

    public static int calculateSizeListNumberItem(int itemWidthInDp, int leftRightPad, int minItemPadding) {
        int screenWidth = UiUtil.getDisplayWidth();
        int itemWidth = UiUtil.getPixelFromDp(itemWidthInDp);
        int leftPx = UiUtil.getPixelFromDp(leftRightPad - 8);
        int rightPx = UiUtil.getPixelFromDp(leftRightPad);
        int minItemPaddingPx = UiUtil.getPixelFromDp(minItemPadding);
        return (int) Math.floor((float) (screenWidth - leftPx - rightPx) / (itemWidth + minItemPaddingPx));
    }

    public static int calculateSizeListPadding(int numberOfItem, int itemWidthInDp, int leftRightPad) {
        int screenWidth = UiUtil.getDisplayWidth();
        int sumItemWidth = UiUtil.getPixelFromDp(itemWidthInDp) * numberOfItem;
        int leftPx = UiUtil.getPixelFromDp(leftRightPad - 8);
        int rightPx = UiUtil.getPixelFromDp(leftRightPad);
        int widthSpacing = (screenWidth - leftPx - rightPx) - sumItemWidth;
        int wSpan = (int) Math.floor(widthSpacing / (numberOfItem));
        int wSpan2 = (widthSpacing - wSpan * (numberOfItem - 1)) / 2;
        return wSpan + wSpan2 / (numberOfItem - 1);
    }

    private static int circleSize = 50;

    public static void addCircleSizeToFlowlayout(FlowLayout layout, CircleImageView[] imageView, List<DynamicAvailableListItem<ProductSize>> sizeDataList
            , CircleImageView[] imageViewDisable) {

//        imageView = new CircleImageView[sizeDataList.size()];
//        imageViewDisable = new CircleImageView[sizeDataList.size()];
        int numberOfItemInRow = UiUtil.calculateSizeListNumberItem(circleSize, 23, 8);
        int paddingItem = UiUtil.calculateSizeListPadding(numberOfItemInRow, circleSize, 23);

        int itemSize = UiUtil.getPixelFromDp(circleSize) + paddingItem;
        for (int i = 0; i < sizeDataList.size(); i++) {
            imageView[i] = new CircleImageView(MyApplication.getContext());
            LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(circleSize, circleSize);

//                    layoutParams.setMargins(0, 0, rightPadding, 0);
            imageView[i].setLayoutParams(layoutParams);
            imageView[i].setId(i);
//                    imageView[i].setImageBitmap(BitmapFactory.decodeResource(
//                            MyApplication.getContext().getResources(), R.drawable.simple_place_holder));
            imageView[i].setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.simple_place_holder_white));
            imageView[i].setBorderColor(MyApplication.getContext().getResources().getColor(R.color.secondary1));
            imageView[i].setBorderWidth(UiUtil.getPixelFromDp(1));

            RelativeLayout relativeLayout = new RelativeLayout(MyApplication.getContext());

            FlowLayout.LayoutParams layoutParams1 =
                    new FlowLayout.LayoutParams(itemSize, UiUtil.getPixelFromDp(circleSize + 10));


            if(getRowNumber((i + 1), numberOfItemInRow) ==  getRowNumber(sizeDataList.size(), numberOfItemInRow)) {
                layoutParams1 =
                        new FlowLayout.LayoutParams(itemSize, UiUtil.getPixelFromDp(circleSize));
            }

            layoutParams.setMargins(0, 0, 0, 0);
            relativeLayout.setLayoutParams(layoutParams1);
            relativeLayout.addView(imageView[i]);
            imageViewDisable[i] = new CircleImageView(MyApplication.getContext());
            imageViewDisable[i].setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.size_btn_outline));
            imageViewDisable[i].setLayoutParams(layoutParams);

            TextView textView = new TextView(MyApplication.getContext());
            FlowLayout.LayoutParams layoutParams2 =
                    new FlowLayout.LayoutParams(UiUtil.getPixelFromDp(circleSize), UiUtil.getPixelFromDp(circleSize));
            textView.setText(sizeDataList.get(i).getT().getSizeName());
            textView.setLayoutParams(layoutParams2);
            textView.setPadding(0, 0, 0, 0);
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(MyApplication.getContext().getResources().getColor(R.color.secondary1));
            relativeLayout.addView(textView);
            relativeLayout.addView(imageViewDisable[i]);

            if (sizeDataList.get(i).isAvailable()) {
                imageViewDisable[i].setVisibility(View.INVISIBLE);
            }

//            if (i < numberOfItemInRow) {
//                layout.setPadding(layout.getPaddingLeft(), padding, layout.getPaddingRight(), layout.getPaddingBottom());
//            }
            layout.addView(relativeLayout);
        }
    }

    private static int getRowNumber(int total, int numOfRow) {
        int a = total/numOfRow;
        int b = total%numOfRow;
        if(b > 0){
            a = a + 1;
        }
        return a;
    }

    public static void addCircleColorToFlowlayout(FlowLayout flowLayout, CircleImageView[] imageView, List<DynamicAvailableListItem<ProductColor>> colorDataList,
                                                  CircleImageView[] imageViewDisable, List<ColorImage> colorImageList) {

        int numberOfItemInRow = UiUtil.calculateSizeListNumberItem(circleSize, 23, 8);
        int paddingItem = UiUtil.calculateSizeListPadding(numberOfItemInRow, circleSize, 23);

        int itemSize = UiUtil.getPixelFromDp(circleSize) + paddingItem;
        for (int i = 0; i < colorDataList.size(); i++) {
            DynamicAvailableListItem<ProductColor> imageData = colorDataList.get(i);
            String imageKey = "";
            for (ColorImage colorImage : colorImageList) {
                if (colorImage.getColorKey().toUpperCase().equals(imageData.getT().getColorKey().toUpperCase()) &&
                        colorImage.getPosition() == 1) {
                    imageKey = colorImage.getImageKey();
                    break;
                }
            }

            imageView[i] = new CircleImageView(MyApplication.getContext());
            LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(circleSize, circleSize);

            //                    layoutParams.setMargins(0, 0, rightPadding, 0);
            imageView[i].setLayoutParams(layoutParams);
            imageView[i].setId(i);
            imageView[i].setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.simple_place_holder));
            imageView[i].setBorderColor(MyApplication.getContext().getResources().getColor(R.color.secondary1));
            imageView[i].setBorderWidth(UiUtil.getPixelFromDp(1));

            RelativeLayout relativeLayout = new RelativeLayout(MyApplication.getContext());
            //                    RelativeLayout.LayoutParams layoutParams1 =  UiUtil.getRelativeLayoutLayoutParams(58, 50);

            FlowLayout.LayoutParams layoutParams1 =
                    new FlowLayout.LayoutParams(itemSize, UiUtil.getPixelFromDp(circleSize + 10));

            if(getRowNumber((i + 1), numberOfItemInRow) ==  getRowNumber(colorDataList.size(), numberOfItemInRow)) {
                layoutParams1 =
                        new FlowLayout.LayoutParams(itemSize, UiUtil.getPixelFromDp(circleSize));
            }

            //int padding = UiUtil.getPixelFromDp(8);
            layoutParams.setMargins(0, 0, paddingItem, paddingItem);
            relativeLayout.setLayoutParams(layoutParams1);
            relativeLayout.addView(imageView[i]);
            imageViewDisable[i] = new CircleImageView(MyApplication.getContext());
            imageViewDisable[i].setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.size_btn_outline));
            imageViewDisable[i].setLayoutParams(layoutParams);
            relativeLayout.addView(imageViewDisable[i]);

            if (imageData.isAvailable()) {
                imageViewDisable[i].setVisibility(View.INVISIBLE);
            }

            if (!TextUtils.isEmpty(imageKey)) {
                flowLayout.addView(relativeLayout);
                Picasso.with(MyApplication.getContext()).load(PathUtil.getProductImageUrl(imageKey)).into(imageView[i]);
            } else if (!TextUtils.isEmpty(imageData.getT().getColorImage())) {
                flowLayout.addView(relativeLayout);
                Picasso.with(MyApplication.getContext()).load(PathUtil.getColorImageUrl(imageData.getT().getColorImage())).into(imageView[i]);
            }

        }
    }

    public static View findCenterView(LinearLayoutManager lm, RecyclerView recyclerView) {

        final int mCenterPivot = (int) (CircleProcessUtil.CENTER_PERCENT * recyclerView.getHeight());
        int minDistance = 0;
        View view = null;
        View returnView = null;
        boolean notFound = true;

        for (int i = lm.findFirstVisibleItemPosition(); i <= lm.findLastVisibleItemPosition() && notFound; i++) {

            view = lm.findViewByPosition(i);

            int center = lm.getOrientation() == LinearLayoutManager.HORIZONTAL ? (view.getLeft() + view.getRight()) / 2 : (view.getTop() + view.getBottom()) / 2;
            int leastDifference = Math.abs(mCenterPivot - center);

            if (leastDifference <= minDistance || i == lm.findFirstVisibleItemPosition()) {
                minDistance = leastDifference;
                returnView = view;
            } else {
                notFound = false;
            }
        }
        return returnView;
    }

    public static int findCenterItemPosition(RecyclerView recyclerView, Context context, int dataSize, int maxItem) {
        if(dataSize < 1){
            return 0;
        }

        final int centerYCircle = (int) (CircleProcessUtil.CENTER_PERCENT * recyclerView.getHeight());
        final float viewHeight = UiUtil.getPixelFromDp(CURATOR_ITEM_HEIGHT);
        int centerItem = maxItem / 2 % dataSize;
        int centerPos = centerYCircle;
        int offSetCenter = (int) ((float) centerPos / viewHeight + 0.5f);
        return dataSize - centerItem + maxItem / 2 - offSetCenter;
    }
}
