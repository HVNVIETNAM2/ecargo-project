package com.mm.main.app.activity.storefront.signup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mm.main.app.R;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.StringUtil;

import butterknife.ButterKnife;

public class StoreFrontMobileSignupActivity_bk extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_front_mobile_signup);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, StringUtil.getResourceString("LB_CA_REGISTER"));
    }
}
