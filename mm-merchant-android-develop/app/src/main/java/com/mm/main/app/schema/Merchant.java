package com.mm.main.app.schema;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by andrew on 10/11/15.
 */
public class Merchant implements Serializable {
    Integer MerchantId;
    Integer MerchantTypeId;
    String MerchantDisplayName;
    String MerchantCompanyName;
    String MerchantNameInvariant;
    String BusinessRegistrationNo;
    String MerchantSubdomain;
    String MerchantDesc;
    String MerchantDescInvariant;
    String LogoImage;
    String HeaderLogoImage;
    String SmallLogoImage;
    String LargeLogoImage;
    String ProfileBannerImage;
    String BackgroundImage;
    String GeoCountryName;
    String MerchantTypeName;
    String StatusNameInvariant;
    String District;
    String PostalCode;
    String Apartment;
    String Floor;
    String BlockNo;
    String Building;
    String StreetNo;
    String Street;
    Integer StatusId;
    Integer IsListedMerchant;
    Integer IsFeaturedMerchant;
    Integer IsRecommendedMerchant;
    Integer IsSearchableMerchant;
    Integer GeoCountryId;
    Integer GeoIdProvince;
    Integer GeoIdCity;
    Date LastStatus;
    Date LastModified;
    Integer Count;
    String MerchantName;
    Integer FollowerCount;

    public String getMerchantName() {
        return MerchantName;
    }

    public void setMerchantName(String merchantName) {
        MerchantName = merchantName;
    }


    public Merchant() {
    }

    public Integer getMerchantId() {
        return MerchantId;
    }

    public void setMerchantId(Integer merchantId) {
        MerchantId = merchantId;
    }

    public Integer getMerchantTypeId() {
        return MerchantTypeId;
    }

    public void setMerchantTypeId(Integer merchantTypeId) {
        MerchantTypeId = merchantTypeId;
    }

    public String getMerchantDisplayName() {
        return MerchantDisplayName;
    }

    public void setMerchantDisplayName(String merchantDisplayName) {
        MerchantDisplayName = merchantDisplayName;
    }

    public String getMerchantCompanyName() {
        return MerchantCompanyName;
    }

    public void setMerchantCompanyName(String merchantCompanyName) {
        MerchantCompanyName = merchantCompanyName;
    }

    public String getBusinessRegistrationNo() {
        return BusinessRegistrationNo;
    }

    public void setBusinessRegistrationNo(String businessRegistrationNo) {
        BusinessRegistrationNo = businessRegistrationNo;
    }

    public String getMerchantSubdomain() {
        return MerchantSubdomain;
    }

    public void setMerchantSubdomain(String merchantSubdomain) {
        MerchantSubdomain = merchantSubdomain;
    }

    public String getMerchantDesc() {
        return MerchantDesc;
    }

    public void setMerchantDesc(String merchantDesc) {
        MerchantDesc = merchantDesc;
    }

    public String getLogoImage() {
        return LogoImage;
    }

    public void setLogoImage(String logoImage) {
        LogoImage = logoImage;
    }

    public String getBackgroundImage() {
        return BackgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        BackgroundImage = backgroundImage;
    }

    public String getGeoCountryName() {
        return GeoCountryName;
    }

    public void setGeoCountryName(String geoCountryName) {
        GeoCountryName = geoCountryName;
    }

    public String getMerchantTypeName() {
        return MerchantTypeName;
    }

    public void setMerchantTypeName(String merchantTypeName) {
        MerchantTypeName = merchantTypeName;
    }

    public String getStatusNameInvariant() {
        return StatusNameInvariant;
    }

    public void setStatusNameInvariant(String statusNameInvariant) {
        StatusNameInvariant = statusNameInvariant;
    }

    public Integer getStatusId() {
        return StatusId;
    }

    public void setStatusId(Integer statusId) {
        StatusId = statusId;
    }

    public Date getLastStatus() {
        return LastStatus;
    }

    public void setLastStatus(Date lastStatus) {
        LastStatus = lastStatus;
    }

    public Date getLastModified() {
        return LastModified;
    }

    public void setLastModified(Date lastModified) {
        LastModified = lastModified;
    }

    public String getMerchantNameInvariant() {
        return MerchantNameInvariant;
    }

    public void setMerchantNameInvariant(String merchantNameInvariant) {
        MerchantNameInvariant = merchantNameInvariant;
    }

    public String getMerchantDescInvariant() {
        return MerchantDescInvariant;
    }

    public void setMerchantDescInvariant(String merchantDescInvariant) {
        MerchantDescInvariant = merchantDescInvariant;
    }

    public String getHeaderLogoImage() {
        return HeaderLogoImage;
    }

    public void setHeaderLogoImage(String headerLogoImage) {
        HeaderLogoImage = headerLogoImage;
    }

    public String getSmallLogoImage() {
        return SmallLogoImage;
    }

    public void setSmallLogoImage(String smallLogoImage) {
        SmallLogoImage = smallLogoImage;
    }

    public String getLargeLogoImage() {
        return LargeLogoImage;
    }

    public void setLargeLogoImage(String largeLogoImage) {
        LargeLogoImage = largeLogoImage;
    }

    public String getProfileBannerImage() {
        return ProfileBannerImage;
    }

    public void setProfileBannerImage(String profileBannerImage) {
        ProfileBannerImage = profileBannerImage;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getApartment() {
        return Apartment;
    }

    public void setApartment(String apartment) {
        Apartment = apartment;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getBlockNo() {
        return BlockNo;
    }

    public void setBlockNo(String blockNo) {
        BlockNo = blockNo;
    }

    public String getBuilding() {
        return Building;
    }

    public void setBuilding(String building) {
        Building = building;
    }

    public String getStreetNo() {
        return StreetNo;
    }

    public void setStreetNo(String streetNo) {
        StreetNo = streetNo;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public Integer getIsListedMerchant() {
        return IsListedMerchant;
    }

    public void setIsListedMerchant(Integer isListedMerchant) {
        IsListedMerchant = isListedMerchant;
    }

    public Integer getIsFeaturedMerchant() {
        return IsFeaturedMerchant;
    }

    public void setIsFeaturedMerchant(Integer isFeaturedMerchant) {
        IsFeaturedMerchant = isFeaturedMerchant;
    }

    public Integer getIsRecommendedMerchant() {
        return IsRecommendedMerchant;
    }

    public void setIsRecommendedMerchant(Integer isRecommendedMerchant) {
        IsRecommendedMerchant = isRecommendedMerchant;
    }

    public Integer getIsSearchableMerchant() {
        return IsSearchableMerchant;
    }

    public void setIsSearchableMerchant(Integer isSearchableMerchant) {
        IsSearchableMerchant = isSearchableMerchant;
    }

    public Integer getGeoCountryId() {
        return GeoCountryId;
    }

    public void setGeoCountryId(Integer geoCountryId) {
        GeoCountryId = geoCountryId;
    }

    public Integer getGeoIdProvince() {
        return GeoIdProvince;
    }

    public void setGeoIdProvince(Integer geoIdProvince) {
        GeoIdProvince = geoIdProvince;
    }

    public Integer getGeoIdCity() {
        return GeoIdCity;
    }

    public void setGeoIdCity(Integer geoIdCity) {
        GeoIdCity = geoIdCity;
    }

    public Integer getCount() {
        return Count;
    }

    public void setCount(Integer count) {
        Count = count;
    }

    public Integer getFollowerCount() {
        return FollowerCount;
    }

    public void setFollowerCount(Integer followerCount) {
        FollowerCount = followerCount;
    }
}
