package com.mm.main.app.adapter.strorefront.interest;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.mm.main.app.R;
import com.mm.main.app.listitem.TagListItem;
import com.mm.main.app.utils.UiUtil;

/**
 * Created by haivu on 1/28/16.
 */
public class InterestMappingRvAdapter extends RecyclerView.Adapter<InterestMappingRvAdapter.InterestKeywordViewHolder> {

    public interface OnItemListener {
        void onClick(TagListItem tagListItem, int position);
    }

    int sumSpan = 40;
    final int paddingCheckbox = 5;
    Activity activity;

    public ArrayList<TagListItem> getTagListItems() {
        return tagListItems;
    }

    OnItemListener listener;
    private ArrayList<TagListItem> tagListItems;

    public InterestMappingRvAdapter(int sumSpan, ArrayList<TagListItem> tagListItems, Activity activity, OnItemListener listener) {
        this.sumSpan = sumSpan;
        this.activity = activity;
        this.listener = listener;
        this.tagListItems = tagListItems;
        calculateSpan();
    }

    @Override
    public InterestKeywordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.interest_mapping_item, parent, false);
        return new InterestKeywordViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final InterestKeywordViewHolder holder, final int position) {
        final TagListItem item = tagListItems.get(position);
        final String label = item.getTag().getTagName();
        holder.textView.setText(label);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                item.setSelected(!item.getSelected());
                updateView(item.getSelected(), holder);
                listener.onClick(item, position);
            }
        });

        updateView(item.getSelected(), holder);
        //int margin = UiUtil.getPixelFromDp(paddingCheckbox);
        int margin = UiUtil.getPixelFromDp(paddingCheckbox);

        // TODO if item is first of row
        // set margin left = remainingSpan/2 * (widthOfList/sumSpan)

        if(isItemFullRow(position)){
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            );
            lp.topMargin = margin;
            holder.backgroundGroup.setLayoutParams(lp);
        }
        else if (item.getIsStartGroup()) {

            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            );

            lp.topMargin = margin;
            lp.rightMargin = margin;
            int leftMargin = (item.getSpanRemaining()) * (getScreenWidth()/sumSpan);
            lp.leftMargin = leftMargin;
            holder.backgroundGroup.setLayoutParams(lp);
        } else {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            );
            lp.topMargin = margin;
            lp.rightMargin = margin;
            holder.backgroundGroup.setLayoutParams(lp);
        }
    }

    private void updateView(boolean isCheck, InterestKeywordViewHolder holder) {

        if (isCheck) {
            holder.backgroundGroup.setBackgroundResource(R.drawable.interest_mapping_selected);
            holder.textView.setTextColor(activity.getResources().getColor(R.color.black));
            holder.selectedCheckbox.setVisibility(View.VISIBLE);
        } else {
            holder.backgroundGroup.setBackgroundResource(R.drawable.interest_mapping_unselected);
            holder.textView.setTextColor(activity.getResources().getColor(R.color.white));
            holder.selectedCheckbox.setVisibility(View.GONE);
        }
    }

    private boolean isItemFullRow(int position) {
        return tagListItems.get(position).getSpanSize() == sumSpan;
    }

    @Override
    public int getItemCount() {
        return tagListItems.size();
    }

    public static int getScreenWidth() {

        // TODO minus padding left right of recycler view
        return Resources.getSystem().getDisplayMetrics().widthPixels - UiUtil.getPixelFromDp(40);
    }

    void calculateSpan() {
        int listWidth = getScreenWidth();

        int widthPer1Span = listWidth / sumSpan;
        TextView textView = (TextView) activity.findViewById(R.id.textMeasure);
        Rect bounds = new Rect();
        Paint textPaint = textView.getPaint();
        int margin = UiUtil.getPixelFromDp(paddingCheckbox);
        // calculate potential span num
        for (TagListItem item : tagListItems) {
            textPaint.getTextBounds(item.getTag().getTagName(), 0, item.getTag().getTagName().length(), bounds);

            // TODO should add padding left & right of text view + iconRight + spacing
            int width = bounds.width() + textView.getPaddingLeft() + textView.getPaddingRight() +
                    margin + 2 * UiUtil.getPixelFromDp(8);

            int spanSize = Math.max(1, Math.min(sumSpan, Math.round((float) width / widthPer1Span + 0.5f)));
            item.setSpanSize(spanSize);
        }

        // recalculate span num
        int totalCurGroup = 0;
        int startGroup = 0;
        int endGroup = 0;
        boolean remainGroup = false;
        for (int i = 0; i < tagListItems.size(); i++) {
            TagListItem item = tagListItems.get(i);
            if (totalCurGroup + item.getSpanSize() < sumSpan) {
                totalCurGroup += item.getSpanSize();
                endGroup = i;

                if (i == tagListItems.size() - 1) {
                    remainGroup = true;
                }
                continue;
            } else {

                // balance groups
                arrangeGroup(startGroup, endGroup, totalCurGroup);

                totalCurGroup = item.getSpanSize();
                if (i == tagListItems.size() - 1 && i != endGroup) {
                    remainGroup = true;
                }
                startGroup = i;
                endGroup = i;
            }
        }

        if (remainGroup) {
            // balance groups
            arrangeGroup(startGroup, endGroup, totalCurGroup);
        }

    }

    private void arrangeGroup(int startGroup, int endGroup, int totalCurGroup) {
        int remainingSpan = sumSpan - totalCurGroup;

        TagListItem itemTmp = tagListItems.get(startGroup);
        itemTmp.setSpanRemaining(itemTmp.getSpanRemaining()  + remainingSpan/2);
        itemTmp.setSpanSize(itemTmp.getSpanSize() + remainingSpan / 2);
        if(startGroup == endGroup){
            itemTmp.setSpanSize(sumSpan);
        }

        itemTmp.setIsStartGroup(true);
    }

    public class InterestKeywordViewHolder extends RecyclerView.ViewHolder {


        public TextView textView;
        public RelativeLayout mainGroup;
        public RelativeLayout mainGroupItem;
        public RelativeLayout backgroundGroup;
        public ImageView selectedCheckbox;
        public View view;


        public InterestKeywordViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text);
            mainGroup = (RelativeLayout) itemView.findViewById(R.id.mainGroup);
            mainGroupItem = (RelativeLayout) itemView.findViewById(R.id.mainGroupItem);
            backgroundGroup = (RelativeLayout) itemView.findViewById(R.id.backgroundGroup);
            selectedCheckbox = (ImageView) itemView.findViewById(R.id.selectedCheckbox);
            view = itemView;
        }

    }

}
