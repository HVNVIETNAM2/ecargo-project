package com.mm.main.app.activity.storefront.follower;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.activity.storefront.profile.StoreFrontUserProfileActivity;
import com.mm.main.app.activity.storefront.profile.StoreFrontUserProfileDataListActivity;
import com.mm.main.app.adapter.strorefront.filter.UserFilterSelectionListAdapter;
import com.mm.main.app.adapter.strorefront.profile.FollowerListAdapter;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.ProfileLifeCycleManager;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmSearchBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by binhtruongp on 3/11/2016.
 */
public class FollowerActivity extends AppCompatActivity implements MmSearchBar.MmSearchBarListener, AdapterView.OnItemClickListener {
    public static final String USER_KEY = "USER_KEY";
    public static final String MERCHANT_ID = "MERCHANT_ID";
    public static final String FOLLOWER_TYPE_KEY = "FOLLOWER_TYPE_KEY";

    public static final int FOLLOWER_CONSUMER = 1;
    public static final int FOLLOWER_MERCHANT = 2;

    

    @Bind(R.id.searchView)
    MmSearchBar searchView;

    @Bind(R.id.mm_toolbar)
    Toolbar toolbar;

    @Bind(R.id.listView)
    ListView listView;
    FollowerListAdapter adapter;

    private String userKey = "";
    private int merchantID;
    private int followType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follower);
        ButterKnife.bind(this);
        setupToolbar();

        if (getIntent().hasExtra(USER_KEY)) {
            userKey = getIntent().getStringExtra(USER_KEY);
        }
        if (getIntent().hasExtra(MERCHANT_ID)) {
            merchantID = getIntent().getIntExtra(MERCHANT_ID, 0);
        }

        followType = getIntent().getIntExtra(FOLLOWER_TYPE_KEY, 0);

        searchView.setHint(R.string.LB_CA_SEARCH);
        searchView.setMmSearchBarListener(this);

        toolbar.setContentInsetsAbsolute(0, 0);

        requestGetFollowing(followType);

        listView.setOnItemClickListener(this);
    }


    void viewFollower() {
        if (!userKey.equals("")) {
            APIManager.getInstance().getFollowService().viewFollower(userKey).enqueue(new MmCallBack<List<User>>(this) {
                @Override
                public void onSuccess(Response<List<User>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        List<User> list = response.body();
                        ArrayList<FilterListItem<User>> listUser = new ArrayList<>();
                        for (int i = 0; i < list.size(); i++) {
                            listUser.add(new FilterListItem<>(list.get(i)));
                        }
                        adapter = new FollowerListAdapter(FollowerActivity.this, listUser);
                        listView.setAdapter(adapter);
                    }
                }
            });
        }
    }

    void viewMerchantFollowing() {
        if (!userKey.equals("")) {
            APIManager.getInstance().getFollowService().viewMerchantFollowing(userKey, merchantID).enqueue(new MmCallBack<List<User>>(this) {
                @Override
                public void onSuccess(Response<List<User>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        List<User> list = response.body();
                        ArrayList<FilterListItem<User>> listUser = new ArrayList<>();
                        for (int i = 0; i < list.size(); i++) {
                            listUser.add(new FilterListItem<>(list.get(i)));
                        }
                        adapter = new FollowerListAdapter(FollowerActivity.this, listUser);
                        listView.setAdapter(adapter);
                    }
                }
            });
        }
    }

    void requestGetFollowing(int type) {
        switch (type) {
            case FOLLOWER_CONSUMER:
                viewFollower();
                break;
            case FOLLOWER_MERCHANT:
                viewMerchantFollowing();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(!userKey.equals(MmGlobal.getUserKey()) || ProfileLifeCycleManager.getInstance().haveHistoryChain()) {
            Intent intent = new Intent();
            intent.putExtra(StoreFrontUserProfileActivity.PROFILE_TYPE_KEY, StoreFrontUserProfileActivity.ProfileType.USER_PUBLIC_PROFILE);
            intent.putExtra(StoreFrontUserProfileActivity.PUBLIC_USER_KEY, userKey);
            // TODO: put the correct friend and follow status when service is ready
            intent.putExtra(StoreFrontUserProfileActivity.BE_FRIEND_KEY, true);
            intent.putExtra(StoreFrontUserProfileActivity.BE_FOLLOWED_KEY, false);
            intent.putExtra(StorefrontMainActivity.TAB_POSITION_KEY, 4);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onEnterText(CharSequence s) {
        filterList(s);
    }

    @Override
    public void onCancelSearch() {

    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    public void filterList(CharSequence s) {
        if (listView.getAdapter() instanceof UserFilterSelectionListAdapter) {
            UserFilterSelectionListAdapter adapter = (UserFilterSelectionListAdapter) listView.getAdapter();
            adapter.getFilter().filter(s);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (listView.getAdapter() instanceof UserFilterSelectionListAdapter) {
            UserFilterSelectionListAdapter adapter = (UserFilterSelectionListAdapter) listView.getAdapter();
            User user = ((FilterListItem<User>) adapter.getItem(position)).getT();

            Intent intent = new Intent(this, StoreFrontUserProfileActivity.class);
            intent.putExtra(StoreFrontUserProfileActivity.PROFILE_TYPE_KEY, StoreFrontUserProfileActivity.ProfileType.USER_PUBLIC_PROFILE);
            intent.putExtra(StoreFrontUserProfileActivity.PUBLIC_USER_KEY, user.getUserKey());
            // TODO: put the correct friend and follow status when service is ready
            intent.putExtra(StoreFrontUserProfileActivity.BE_FRIEND_KEY, true);
            intent.putExtra(StoreFrontUserProfileActivity.BE_FOLLOWED_KEY, false);
            intent.putExtra(StorefrontMainActivity.TAB_POSITION_KEY, 4);
            setResult(RESULT_OK, intent);
            finish();

            ProfileLifeCycleManager.getInstance().openNewProfile(userKey, StoreFrontUserProfileDataListActivity.ProfileDataListType.FOLLOWER_LIST);
        }
    }
}
