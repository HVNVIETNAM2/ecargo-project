package com.mm.main.app.activity.storefront.signup;

import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.merchant.setting.SelectRegionActivity;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.layout.RequestSmsSlidingDrawer;
import com.mm.main.app.schema.*;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.request.MobileVerifySmsCheckRequest;
import com.mm.main.app.schema.request.MobileVerifySmsSendRequest;
import com.mm.main.app.schema.request.SignupRequest;
import com.mm.main.app.schema.response.MobileVerifySmsSendResponse;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.AnimateUtil;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.StringUtil;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.mm.storefront.app.wxapi.WXEntryActivity;
import retrofit.Response;
import retrofit.Retrofit;

public class StoreFrontMobileSignupActivity extends GeneralSignupActivity {

    public static final String EXTRA_USER = "user_information";
    public static final String EXTRA_USER_NAME = "username";
    public static final String EXTRA_USER_FIRST_NAME = "user_first_name";
    public static final String EXTRA_USER_LAST_NAME = "user_last_name";
    public static final String EXTRA_USER_PASS = "user_pass";
    public static final String EXTRA_USER_EMAIL = "user_email";

    String userName;
    String userFirstName;
    String userLastName;
    String password;
    String email;


    @Bind(R.id.tvError)
    TextView tvError;

    @Bind(R.id.tvCountry)
    TextView tvCountry;

    @Bind(R.id.hiddenFocus)
    EditText hiddenFocus;

    @Bind(R.id.tvPhoneNumber)
    EditText edPhoneNumber;

    @Bind(R.id.requestSmsSlidingDrawer)
    RequestSmsSlidingDrawer requestSmsSlidingDrawer;

    @Bind(R.id.edVerCode)
    EditText edVerCode;

    @Bind(R.id.rlLayoutVercode)
    RelativeLayout rlLayoutVerCode;

    @Bind(R.id.edCountryCode)
    EditText edCountryCode;

    private final int SPEED = 15;
    private int numAttempts = 5;

    List<Country> countryList;
    private Country currentCountry;
    MobileVerifySmsCheckRequest mobileVerifySmsCheckRequest;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_mobile_vercode_sent);
        ButterKnife.bind(this);

        ActivityUtil.setTitle(this, StringUtil.getResourceString("LB_CA_REGISTER"));
        mobileVerifySmsCheckRequest = new MobileVerifySmsCheckRequest();
        context = this;

        // get extra information
        Bundle bundle = (Bundle) getIntent().getBundleExtra(EXTRA_USER);
        if (bundle != null) {
            userName = bundle.getString(EXTRA_USER_NAME);
            userFirstName = bundle.getString(EXTRA_USER_FIRST_NAME);
            userLastName = bundle.getString(EXTRA_USER_LAST_NAME);
            email = bundle.getString(EXTRA_USER_EMAIL);
            password = bundle.getString(EXTRA_USER_PASS);
        }

        requestSmsSlidingDrawer.bindRequestSmsDrawer(60, getResources().getString(R.string.LB_CA_SR_REQUEST_VERCODE), new RequestSmsSlidingDrawer.SignUpCallBack() {
            @Override
            public void startCountDown() {
                edVerCode.setText(null);
                rlLayoutVerCode.setVisibility(View.VISIBLE);
                sendMobileVerifySms();
                edVerCode.requestFocus();
            }

            @Override
            public void endCountDown() {
//                rlLayoutVerCode.setVisibility(View.GONE);
            }
        });
        requestSmsSlidingDrawer.lockSliding();

        setupEditText();
        fetchMobileCode();
    }

    private void fetchMobileCode() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getGeoService().storefrontCountry()
                .enqueue(new MmCallBack<List<Country>>(this) {
                    @Override
                    public void onSuccess(Response<List<Country>> response, Retrofit retrofit) {
                        countryList = response.body();
                        if (countryList != null) {
                            currentCountry = countryList.get(0);
                            updateCountryUI();
                        }
                        MmProgressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        MmProgressDialog.dismiss();
                    }
                });
    }

    @OnClick(R.id.btnRegion)
    public void onClickRegionButton() {
        Intent intent = new Intent(this, SelectRegionActivity.class);
        startActivityForResult(intent, SelectRegionActivity.SELECT_COUNTRY_CODE_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == SelectRegionActivity.SELECT_COUNTRY_CODE_REQUEST) {
            currentCountry = (Country) data.getExtras().get(SelectRegionActivity.SELECTED_COUNTRY_CODE);
            updateCountryUI();
        }
    }

    private void updateCountryUI() {
        edCountryCode.setText(currentCountry.getMobileCode());
        tvCountry.setText(currentCountry.getGeoCountryName());
    }

    private void setupEditText() {

        hiddenFocus.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(hiddenFocus.getWindowToken(), 0);
                }
            }
        });


        edPhoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (validatePhoneEditText()) {
                        //Unlock sliding.
                        requestSmsSlidingDrawer.unlockSliding();
                    } else {
                        requestSmsSlidingDrawer.lockSliding();
                    }
                } else {
                    edPhoneNumber.setTextColor(getResources().getColor(R.color.secondary2));
                }
            }
        });

        edPhoneNumber.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if (validatePhoneEditText()) {
                        requestSmsSlidingDrawer.unlockSliding();
                    } else {
                        requestSmsSlidingDrawer.lockSliding();
                    }
                }
                return false;
            }
        });

        edPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (tvError.getVisibility() == View.VISIBLE) {
                    AnimateUtil.collapse(tvError, SPEED, null);
                    tvError.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (validatePhoneEditText(false)) {
                    requestSmsSlidingDrawer.unlockSliding();
                } else {
                    requestSmsSlidingDrawer.lockSliding();
                }
            }
        });

        edVerCode.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    checkMobileVerifySms();
                }
                return false;
            }
        });

        edVerCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edVerCode.setTextColor(getResources().getColor(R.color.secondary2));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edCountryCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s)) {
                    edCountryCode.setText("+");
                    edCountryCode.setSelection(edCountryCode.getText().length());
                }

                boolean foundCountry = false;
                for (Country country : countryList) {
                    if (s.toString().equals(country.getMobileCode())) {
                        currentCountry = country;
                        tvCountry.setText(currentCountry.getGeoCountryName());
                        foundCountry = true;
                        break;
                    }
                }

                if (foundCountry == false) {
                    currentCountry = null;
                    tvCountry.setText(getString(R.string.LB_COUNTRY_PICK));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private boolean validatePhoneEditText(boolean withError) {
        if (ValidationUtil.isEmpty(edPhoneNumber.getText().toString())) {
            if (withError) {
                ErrorUtil.showVerificationError(tvError, edPhoneNumber, getString(R.string.MSG_ERR_CA_ACCOUNT_NIL), this);
            }
            return false;
        }

        if (!ValidationUtil.isValidPhone(edPhoneNumber.getText())) {
            if (withError) {
                ErrorUtil.showVerificationError(tvError, edPhoneNumber, getString(R.string.MSG_ERR_CA_MOBILE_PATTERN), this);
            }
            return false;
        }

        return true;
    }

    private boolean validatePhoneEditText() {
        return validatePhoneEditText(true);
    }

    private void sendMobileVerifySms() {
        final MobileVerifySmsSendRequest data = new MobileVerifySmsSendRequest();
        data.setMobileCode(edCountryCode.getText().toString().trim());
        data.setMobileNumber(edPhoneNumber.getText().toString());
        APIManager.getInstance().getAuthService().mobileVerifySmsSend(data)
                .enqueue(new MmCallBack<MobileVerifySmsSendResponse>(getApplication()) {
                    @Override
                    public void onSuccess(Response<MobileVerifySmsSendResponse> response, Retrofit retrofit) {
                        mobileVerifySmsCheckRequest.setMobileNumber(data.getMobileNumber());
                        mobileVerifySmsCheckRequest.setMobileCode(data.getMobileCode());
                        mobileVerifySmsCheckRequest.setMobileVerificationId(response.body().getMobileVerificationId());
                    }

                });
    }

    @OnClick(R.id.btnCheckSms)
    public void checkMobileVerifySms() {
        if (edVerCode.getText().toString().isEmpty()) {
            setErrorVerCode();
            return;
        }
        mobileVerifySmsCheckRequest.setMobileVerificationToken(edVerCode.getText().toString());

        APIManager.getInstance().getAuthService().mobileVerifySmsCheck(mobileVerifySmsCheckRequest)
                .enqueue(new MmCallBack<Boolean>(getApplication(), MmCallBack.ERROR_520) {

                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                        final boolean success = (response.body() != null && response.body());
                        final Handler handler = new Handler();
                        final Runnable runnable = new Runnable() {
                            public void run() {
                                if (success) {
                                    // Go to sign up screen
                                    processSignUp();
                                } else {
                                    processVerifySmsFailed();
                                }

                            }
                        };
                        handler.postDelayed(runnable, 100);
                        // runnable.run();
                    }

                    @Override
                    public void onFailWithErrorCode(Response<Boolean> response, Retrofit retrofit) {
                        processVerifySmsFailed();
                    }
                });
    }

    private void processVerifySmsFailed() {
        numAttempts--;

        if (numAttempts == 0) {
            Intent intent = new Intent(context, WXEntryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
            return;
        }
        setErrorVerCode();
    }

    private void setErrorVerCode() {
        String errorStr = getString(R.string.LB_CA_VERCODE_INCORRECT_1) +
                numAttempts + getString(R.string.LB_CA_VERCODE_INCORRECT_2);
        ErrorUtil.showVerificationError(tvError, edVerCode, errorStr, this);
    }

    private void processSignUp() {
        SignupRequest request = new SignupRequest();
        request.setUserName(userName);
        request.setMobileCode(mobileVerifySmsCheckRequest.getMobileCode());
        request.setMobileNumber(mobileVerifySmsCheckRequest.getMobileNumber());
        request.setMobileVerificationId(mobileVerifySmsCheckRequest.getMobileVerificationId());
        request.setMobileVerificationToken(mobileVerifySmsCheckRequest.getMobileVerificationToken());

        Intent intent = new Intent(this, MobileSignupProfileActivity.class);
        intent.putExtra(Constant.Extra.EXTRA_SIGNUP_REQUEST, request);

        startActivityWithLoginKey(intent);
    }

}
