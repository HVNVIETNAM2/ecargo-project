package com.mm.main.app.listitem;

import com.mm.main.app.schema.CartItem;

import java.io.Serializable;

/**
 * Created by ductranvt on 1/15/2016.
 */
public class ShoppingCartProductRvItem extends CartItem implements ShoppingCartRvItem, Serializable {
    private boolean isSelected;
    private CartItem product;
    private ShoppingCartMerchantRvItem merchantRvItem;
    private int appiumIndex;

    public ShoppingCartProductRvItem(CartItem product, ShoppingCartMerchantRvItem merchant) {
        this.product = product;
        this.merchantRvItem = merchant;
    }

    public CartItem getProduct() {
        return product;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public ShoppingCartMerchantRvItem.ItemType getType() {
        return ItemType.TYPE_SHOPPING_CART_PRODUCT;
    }

    public ShoppingCartMerchantRvItem getMerchantRvItem() {
        return merchantRvItem;
    }

    @Override
    public void setAppiumIndex(int appiumIndex) {
        this.appiumIndex = appiumIndex;
    }

    @Override
    public int getAppiumIndex() {
        return appiumIndex;
    }
}
