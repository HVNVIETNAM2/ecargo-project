package com.mm.main.app.schema;

import java.util.Date;

/**
 * Created by henrytung on 26/10/15.
 */
public class InventoryLocation {
    Integer InventoryLocationId;
    Integer MerchantId;
    Integer InventoryLocationTypeId;
    String LocationExternalCode;
    String LocationName;
    String GeoCountryId;
    String GeoIdProvince;
    String GeoIdCity;
    String District;
    String PostalCode;
    String Apartment;
    String Floor;
    String BlockNo;
    String Building;
    String StreetNo;
    String Street;
    String StatusId;
    Date LastStatus;
    Date LastModified;
    String InventoryLocationTypeName;
    String StatusNameInvariant;
    String GeoCountryName;
    String GeoProvinceName;
    String GeoCityName;

    public InventoryLocation() {
    }

    public InventoryLocation(Integer inventoryLocationId, Integer merchantId, Integer inventoryLocationTypeId, String locationExternalCode, String locationName, String geoCountryId, String geoIdProvince, String geoIdCity, String district, String postalCode, String apartment, String floor, String blockNo, String building, String streetNo, String street, String statusId, Date lastStatus, Date lastModified, String inventoryLocationTypeName, String statusNameInvariant, String geoCountryName, String geoProvinceName, String geoCityName) {
        InventoryLocationId = inventoryLocationId;
        MerchantId = merchantId;
        InventoryLocationTypeId = inventoryLocationTypeId;
        LocationExternalCode = locationExternalCode;
        LocationName = locationName;
        GeoCountryId = geoCountryId;
        GeoIdProvince = geoIdProvince;
        GeoIdCity = geoIdCity;
        District = district;
        PostalCode = postalCode;
        Apartment = apartment;
        Floor = floor;
        BlockNo = blockNo;
        Building = building;
        StreetNo = streetNo;
        Street = street;
        StatusId = statusId;
        LastStatus = lastStatus;
        LastModified = lastModified;
        InventoryLocationTypeName = inventoryLocationTypeName;
        StatusNameInvariant = statusNameInvariant;
        GeoCountryName = geoCountryName;
        GeoProvinceName = geoProvinceName;
        GeoCityName = geoCityName;
    }

    public Integer getInventoryLocationId() {
        return InventoryLocationId;
    }

    public void setInventoryLocationId(Integer inventoryLocationId) {
        InventoryLocationId = inventoryLocationId;
    }

    public Integer getMerchantId() {
        return MerchantId;
    }

    public void setMerchantId(Integer merchantId) {
        MerchantId = merchantId;
    }

    public Integer getInventoryLocationTypeId() {
        return InventoryLocationTypeId;
    }

    public void setInventoryLocationTypeId(Integer inventoryLocationTypeId) {
        InventoryLocationTypeId = inventoryLocationTypeId;
    }

    public String getLocationExternalCode() {
        return LocationExternalCode;
    }

    public void setLocationExternalCode(String locationExternalCode) {
        LocationExternalCode = locationExternalCode;
    }

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }

    public String getGeoCountryId() {
        return GeoCountryId;
    }

    public void setGeoCountryId(String geoCountryId) {
        GeoCountryId = geoCountryId;
    }

    public String getGeoIdProvince() {
        return GeoIdProvince;
    }

    public void setGeoIdProvince(String geoIdProvince) {
        GeoIdProvince = geoIdProvince;
    }

    public String getGeoIdCity() {
        return GeoIdCity;
    }

    public void setGeoIdCity(String geoIdCity) {
        GeoIdCity = geoIdCity;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getApartment() {
        return Apartment;
    }

    public void setApartment(String apartment) {
        Apartment = apartment;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getBlockNo() {
        return BlockNo;
    }

    public void setBlockNo(String blockNo) {
        BlockNo = blockNo;
    }

    public String getBuilding() {
        return Building;
    }

    public void setBuilding(String building) {
        Building = building;
    }

    public String getStreetNo() {
        return StreetNo;
    }

    public void setStreetNo(String streetNo) {
        StreetNo = streetNo;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getStatusId() {
        return StatusId;
    }

    public void setStatusId(String statusId) {
        StatusId = statusId;
    }

    public Date getLastStatus() {
        return LastStatus;
    }

    public void setLastStatus(Date lastStatus) {
        LastStatus = lastStatus;
    }

    public Date getLastModified() {
        return LastModified;
    }

    public void setLastModified(Date lastModified) {
        LastModified = lastModified;
    }

    public String getInventoryLocationTypeName() {
        return InventoryLocationTypeName;
    }

    public void setInventoryLocationTypeName(String inventoryLocationTypeName) {
        InventoryLocationTypeName = inventoryLocationTypeName;
    }

    public String getStatusNameInvariant() {
        return StatusNameInvariant;
    }

    public void setStatusNameInvariant(String statusNameInvariant) {
        StatusNameInvariant = statusNameInvariant;
    }

    public String getGeoCountryName() {
        return GeoCountryName;
    }

    public void setGeoCountryName(String geoCountryName) {
        GeoCountryName = geoCountryName;
    }

    public String getGeoProvinceName() {
        return GeoProvinceName;
    }

    public void setGeoProvinceName(String geoProvinceName) {
        GeoProvinceName = geoProvinceName;
    }

    public String getGeoCityName() {
        return GeoCityName;
    }

    public void setGeoCityName(String geoCityName) {
        GeoCityName = geoCityName;
    }
}
