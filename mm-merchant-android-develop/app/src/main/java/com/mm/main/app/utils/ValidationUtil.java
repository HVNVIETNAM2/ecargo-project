package com.mm.main.app.utils;

import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by henrytung on 9/11/15.
 */
public class ValidationUtil {

    public final static String PASSWORD_REGEX = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%^&*()_+|}{:\"?><.]).{8,})";
    public final static String INPUT_LIMIT_1_50_REGEX = "^[\\w ]{1,50}$";
    public final static String INPUT_LIMIT_3_25_REGEX = "^[\\w ]{3,25}$";
    public final static String USERNAME_REGEX = "([a-zA-Z][a-zA-Z0-9]+)";

    public static boolean validate(String rules, String content) {
        boolean match = false;

        Pattern p = Pattern.compile(rules);
        Matcher m = p.matcher(content);
        if (m.matches()) {
            match = true;
        }

        return match;
    }

    public static void cleanErrorMessage(TextInputLayout textLayout) {
        setErrorMessage(textLayout, null);
    }

    public static void setErrorMessage(final TextInputLayout textLayout, final String message) {
        if (message != null) {
            if (textLayout.getChildCount() == 2)
                textLayout.getChildAt(1).setVisibility(View.VISIBLE);
            textLayout.setError(message);

        } else {
            if (textLayout.getChildCount() == 2)
                textLayout.getChildAt(1).setVisibility(View.GONE);

            textLayout.setError(null);
        }

    }

    public static boolean isEmpty(String content) {
        return (TextUtils.isEmpty(content));
    }

    public static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

    }

    public static boolean isValidPhone(CharSequence target) {
        return Patterns.PHONE.matcher(target).matches();
    }

    public static boolean isValidUsername(CharSequence username) {
        Pattern p = Pattern.compile(USERNAME_REGEX);
        Matcher m = p.matcher(username);
        return m.matches();
    }

    public static boolean isValidPassword(CharSequence password){
        Pattern p = Pattern.compile(PASSWORD_REGEX);
        Matcher m = p.matcher(password);
        return m.matches();
    }
}
