package com.mm.main.app.listitem;

import com.mm.main.app.schema.CartItem;

/**
 * Created by ductranvt on 1/26/2016.
 */
public class WishlistRvItem {
    private CartItem product;

    public WishlistRvItem(CartItem product) {
        this.product = product;
    }

    public CartItem getProduct() {
        return product;
    }

}
