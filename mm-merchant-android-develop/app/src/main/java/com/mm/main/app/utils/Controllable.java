package com.mm.main.app.utils;

/**
 * Created by henrytung on 28/12/2015.
 */
public interface Controllable {
    public void resume();
}
