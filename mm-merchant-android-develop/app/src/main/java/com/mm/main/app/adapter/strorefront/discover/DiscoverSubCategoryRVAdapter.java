package com.mm.main.app.adapter.strorefront.discover;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.schema.Category;

import java.util.List;

/**
 * Adapter for Recycle View, View Holder Pattern with getView function!
 */
public class DiscoverSubCategoryRVAdapter extends RecyclerView.Adapter<DiscoverSubCategoryRVAdapter.SubCategoryViewHolder> {

    public interface SubCategoryOnClickListener {
        void onClick(int position, Object data);
    }

    List<Category> itemList;
    SubCategoryOnClickListener subCategoryOnClickListener;
    Drawable[] drawable;


    public DiscoverSubCategoryRVAdapter(List<Category> itemList, SubCategoryOnClickListener subCategoryOnClickListener) {
        this.itemList = itemList;
        this.subCategoryOnClickListener = subCategoryOnClickListener;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public SubCategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_discover_sub_category_item, viewGroup, false);
        setSize(v);

        SubCategoryViewHolder.SubCategoryViewHolderOnClickListener subCategoryViewHolderOnClickListener = new SubCategoryViewHolder.SubCategoryViewHolderOnClickListener() {
            @Override
            public void onClick(int position) {
                proceedProductDetail(position);
            }
        };
        return new SubCategoryViewHolder(v, itemList, subCategoryViewHolderOnClickListener);
    }

    public void proceedProductDetail(int position) {
        subCategoryOnClickListener.onClick(position, itemList.get(position));
    }

    @Override
    public void onBindViewHolder(final SubCategoryViewHolder subCategoryViewHolder, final int i) {
        final Category currentCategory = itemList.get(i);
        subCategoryViewHolder.tvSubCategory.setText(currentCategory.getCategoryName());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    void setSize(View v) {
    }


    public static class SubCategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private static final String TAG = "Inside Product View Holder";
        View mainLayout;
        List<Category> itemList;
        TextView tvSubCategory;
        SubCategoryViewHolderOnClickListener subCategoryViewHolderOnClickListener;

        public interface SubCategoryViewHolderOnClickListener {
            void onClick(int position);
        }

        SubCategoryViewHolder(View itemView, List<Category> itemList, SubCategoryViewHolderOnClickListener subCategoryViewHolderOnClickListener) {
            super(itemView);
            this.itemList = itemList;
            tvSubCategory = (TextView) itemView.findViewById(R.id.tvSubCategory);
            mainLayout = itemView;

            this.subCategoryViewHolderOnClickListener = subCategoryViewHolderOnClickListener;

            itemView.setOnClickListener(this);
        }


        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            subCategoryViewHolderOnClickListener.onClick(getPosition());
        }
    }

    public List<Category> getProductList() {
        return itemList;
    }

    public void setItemList(List<Category> itemList) {
        this.itemList = itemList;
    }
}
