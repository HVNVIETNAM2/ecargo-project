package com.mm.main.app.activity.storefront.im;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.im.model.Message;
import com.mm.main.app.activity.storefront.im.model.UserType;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.log.Logger;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nhutphan on 3/14/2016.
 */
public class ChatRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final String TAG = ChatRvAdapter.class.getSimpleName();

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("HH:mm");
    private final int INTERVAL_TIMER = 2 * 60 * 1000;
    private ArrayList<Message> messages;
    private Context context;
    private ChatCallBack chatCallBack;

    public interface ChatCallBack {

    }

    public ChatRvAdapter(Context context, ArrayList<Message> messages, ChatCallBack chatCallBack){
        this.context = context;
        this.messages = messages;
        this.chatCallBack = chatCallBack;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        UserType userType = UserType.values()[viewType];
        RecyclerView.ViewHolder viewHolder = null;

        if(userType.toString().indexOf("OTHER") > 0){
            viewHolder = new ChatLeftViewHolder(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.chat_left_item, viewGroup, false), userType);
        }else{
            viewHolder = new ChatRightViewHolder(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.chat_right_item, viewGroup, false), userType);
        }

        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return messages.get(position).getUserType().ordinal();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        Message message = messages.get(position);

        if(message.getUserType().toString().indexOf("OTHER") > 0){
            ChatLeftViewHolder incomingTextView = (ChatLeftViewHolder) viewHolder;
            bindChatLeftView(position, incomingTextView, message);
        }else{
            ChatRightViewHolder outgoingTextView = (ChatRightViewHolder) viewHolder;
            bindChatRightView(position, outgoingTextView, message);
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ChatLeftViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imgProfile)
        CircleImageView imgProfile;

        @Bind(R.id.imgIsCurator)
        ImageView imgIsCurator;

        @Bind(R.id.timeTextView)
        public TextView timeTextView;

        private TextView textMessageBody;
        private ImageView imageMessageBody;

        public ChatLeftViewHolder(View itemView, UserType userType) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            LinearLayout bodyView = (LinearLayout)itemView.findViewById(R.id.llMessageBodyView);
            View messageView;

            switch (userType){
                case OTHER_CHAT_TEXT:
                    messageView =   LayoutInflater.from(context).inflate(R.layout.chat_text_item, bodyView, true);
                    textMessageBody = (TextView) messageView.findViewById(R.id.textMessageBody);
                    break;
                case OTHER_CHAT_IMAGE:
                    messageView =   LayoutInflater.from(context).inflate(R.layout.chat_image_item, bodyView, true);
                    imageMessageBody = (ImageView) messageView.findViewById(R.id.imageMessageBody);
                    break;
            }
        }
    }

    private void bindChatLeftView(int position, ChatLeftViewHolder viewHolder, Message message) {
        displayTime(position, viewHolder.timeTextView);
        displayUserProfile(position, viewHolder.imgProfile);
        viewHolder.imgIsCurator.setVisibility(View.GONE);

        switch (message.getUserType()){
            case OTHER_CHAT_TEXT:
                viewHolder.textMessageBody.setText(message.getMessageText());
                break;
            case OTHER_CHAT_IMAGE:
                viewHolder.imageMessageBody.setImageBitmap(message.getImageMessageBody());
                break;
        }
    }

    public class ChatRightViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imgProfile)
        CircleImageView imgProfile;

        @Bind(R.id.imgIsCurator)
        ImageView imgIsCurator;

        @Bind(R.id.timeTextView)
        public TextView timeTextView;

        private TextView textMessageBody;
        private ImageView imageMessageBody;

        public ChatRightViewHolder(View itemView, UserType userType) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            LinearLayout bodyView = (LinearLayout)itemView.findViewById(R.id.llMessageBodyView);
            View messageView;

            switch (userType){
                case SELF_CHAT_TEXT:
                    messageView =   LayoutInflater.from(context).inflate(R.layout.chat_text_item, bodyView, true);
                    textMessageBody = (TextView) messageView.findViewById(R.id.textMessageBody);
                    break;
                case SELF_CHAT_IMAGE:
                    messageView =   LayoutInflater.from(context).inflate(R.layout.chat_image_item, bodyView, true);
                    imageMessageBody = (ImageView) messageView.findViewById(R.id.imageMessageBody);
                    break;
            }
        }
    }

    private void bindChatRightView(int position, ChatRightViewHolder viewHolder, Message message) {
        displayTime(position, viewHolder.timeTextView);
        displayUserProfile(position, viewHolder.imgProfile);
        viewHolder.imgIsCurator.setVisibility(View.GONE);

        switch (message.getUserType()){
            case SELF_CHAT_TEXT:
                viewHolder.textMessageBody.setText(message.getMessageText());
                break;
            case SELF_CHAT_IMAGE:
                viewHolder.imageMessageBody.setImageBitmap(message.getImageMessageBody());
                break;
        }
    }

    private void displayUserProfile(int position, CircleImageView imgProfile){
        UserType currentUserType = messages.get(position).getUserType();
        UserType previousUserType = (position - 1) >= 0 ?  messages.get(position - 1).getUserType() : null;
        User user = messages.get(position).getUser();

        if(!currentUserType.equals(previousUserType)){
            String avatarUrl = PathUtil.getUserImageUrl(user != null ? user.getProfileImage() : "/demo");
            Picasso.with(MyApplication.getContext()).load(avatarUrl)
                    .placeholder(R.drawable.placeholder).into(imgProfile);
            imgProfile.setBorderWidth(UiUtil.getPixelFromDp(0));
            imgProfile.setVisibility(View.VISIBLE);
        }else {
            imgProfile.setVisibility(View.INVISIBLE);
        }
    }

    private void displayTime(int position, TextView timeTV) {
        long currentTime = messages.get(position).getMessageTime();
        long previewTime = (position - 1) >= 0 ? messages.get(position - 1).getMessageTime() : 0;
        try {
            if ((currentTime - previewTime) >= INTERVAL_TIMER ) {
                timeTV.setVisibility(View.VISIBLE);
                timeTV.setText(SIMPLE_DATE_FORMAT.format(currentTime));
            } else {
                timeTV.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Logger.e(TAG, e.getMessage());
        }
    }
}
