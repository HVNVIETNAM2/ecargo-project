package com.mm.main.app.activity.storefront.checkout;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.signup.GeneralSignupActivity;
import com.mm.main.app.adapter.strorefront.checkout.CheckoutListItemAdapter;
import com.mm.main.app.adapter.strorefront.product.ProductDetailSelectionCollection;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.DynamicAvailableListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.manager.CheckoutFlowManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.record.StylesRecord;
import com.mm.main.app.schema.Cart;
import com.mm.main.app.schema.ProductColor;
import com.mm.main.app.schema.ProductSize;
import com.mm.main.app.schema.Sku;
import com.mm.main.app.schema.Style;
import com.mm.main.app.schema.request.CartAddRequest;
import com.mm.main.app.schema.request.CartCreateRequest;
import com.mm.main.app.schema.request.CartUpdateRequest;
import com.mm.main.app.schema.response.SearchResponse;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.mm.storefront.app.wxapi.WXEntryActivity;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Response;
import retrofit.Retrofit;

public class CheckoutActivity extends AppCompatActivity implements CheckoutListItemAdapter.OnUpdateInfoListener {

    public static String SELECTED_DATA_KEY = "SELECTED_DATA_KEY";

    public static final String ADD_PRODUCT_TO_CART_FLAG = "ADD_PRODUCT_TO_CART_FLAG";
    public static final String EXTRA_PRODUCT_DATA = "EXTRA_PRODUCT_DATA";
    public static final String SWIPE_MODE = "OPENED_FROM_SWIPE_MODE";

    public static final String STYLE_CODE_DATA = "STYLE_CODE_DATA";
    public static final String QUANTITY_DATA = "QUANTITY_DATA";
    public static final String CART_ITEM_ID = "CART_ITEM_ID";
    public static final int CHECK_OUT_REQUEST = 1000;

    View cartBadge, cartIcon;
    MenuItem cartItem;

    @Bind(R.id.listView)
    ListView listView;

    @Bind(R.id.exitSpace)
    FrameLayout frameLayout;

    @Bind(R.id.checkoutPopup)
    RelativeLayout checkoutPopup;

    @Bind(R.id.imgAnimation)
    CircleImageView imgAnimation;

    @Bind(R.id.productImageView)
    ImageView productImageView;

    @Bind(R.id.brandImageView)
    ImageView brandImageView;

    @Bind(R.id.textViewSkuName)
    TextView textViewSkuName;

    @Bind(R.id.textViewPrice)
    TextView textViewPrice;

    @Bind(R.id.tvSizeTable)
    TextView tvSizeTable;

    @Bind(R.id.btn_add_to_cart)
    Button btn_add_to_cart;

    @Bind(R.id.textViewAmountLabel)
    TextView textViewAmountLabel;

    @Bind(R.id.textViewSummaryPrice)
    TextView textViewSummaryPrice;

    CheckoutListItemAdapter checkoutListItemAdapter;
    Style style;

    ProductColor selectedColor;
    ProductSize selectedSize;
    int quantity;

    private ProductDetailSelectionCollection productDetailSelectionCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        addIdForAutomationTest();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        style = (Style) getIntent().getSerializableExtra(EXTRA_PRODUCT_DATA);

        if (style != null) {
            initUI();
        } else {
            String styleId = getIntent().getStringExtra(STYLE_CODE_DATA);
            MmProgressDialog.show(this);
            APIManager.getInstance().getSearchService().searchStyleCode(styleId)
                    .enqueue(new MmCallBack<SearchResponse>(CheckoutActivity.this) {
                        @Override
                        public void onSuccess(Response<SearchResponse> response, Retrofit retrofit) {
                            MmProgressDialog.dismiss();
                            style = response.body().getPageData().get(0);
                            initUI();
                        }
                    });
        }
    }

    void initUI() {
        setupExit();
        setupSummaryView();
        initSelectedInfo();
        updateProductImageView();
        setupAnimationView();

        int quantity = getIntent().getIntExtra(QUANTITY_DATA, 1);
        updateTotalPrice(quantity);
        loadLisItem();

        // set button confirm title
        if (getIntent().getBooleanExtra(ADD_PRODUCT_TO_CART_FLAG, false)) {
            if (getIntent().getBooleanExtra(SWIPE_MODE, false)) {
                btn_add_to_cart.setText(getResources().getString(R.string.LB_CA_PDP_SWIPE2PAY_PURCHASE));
            } else {
                btn_add_to_cart.setText(getResources().getString(R.string.LB_CA_ADD2CART));
            }
        } else {
            btn_add_to_cart.setText(getResources().getString(R.string.LB_CA_CONFIRM));
        }
    }

    public void loadLisItem() {
        checkoutListItemAdapter = new CheckoutListItemAdapter(this, style, getIntent().getBooleanExtra(SWIPE_MODE, false));
        checkoutListItemAdapter.setInputQuantity(quantity);
        checkoutListItemAdapter.setProductDetailSelectionCollection(productDetailSelectionCollection);
        listView.setAdapter(checkoutListItemAdapter);
    }

    void setupAnimationView() {
        String imageUrl = style.getImageBaseColor(selectedColor);
        if (TextUtils.isEmpty(imageUrl)) {
            imgAnimation.setImageResource(R.drawable.spacer);
        } else {
            Picasso.with(MyApplication.getContext()).load(imageUrl).into(imgAnimation);
        }
        imgAnimation.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.secondary1));
        imgAnimation.setBorderWidth(UiUtil.getPixelFromDp(1));
    }

    void initSelectedInfo() {
        productDetailSelectionCollection = (ProductDetailSelectionCollection) getIntent().getSerializableExtra(CheckoutActivity.SELECTED_DATA_KEY);
        if (productDetailSelectionCollection == null) {
            productDetailSelectionCollection = new ProductDetailSelectionCollection();
        }

        if (productDetailSelectionCollection.getSelectedColor() != null) {
            selectedColor = productDetailSelectionCollection.getSelectedColor().getT();
        }

        if (productDetailSelectionCollection.getSelectedSize() != null) {
            selectedSize = productDetailSelectionCollection.getSelectedSize().getT();
        }
    }

    @OnClick(R.id.btn_add_to_cart)
    public void proceedAddToCart() {
        if (getIntent().getBooleanExtra(ADD_PRODUCT_TO_CART_FLAG, false)) {

            if (getIntent().getBooleanExtra(SWIPE_MODE, false)) {
                handleBuyNowAction();
            } else {
                handleAddCartAction();
            }

        } else {
            handleUpdateCartAction();
        }
    }

    private void showLogin() {
        CheckoutFlowManager.getInstance().clearActivitiesFlow();
        CheckoutFlowManager.getInstance().addActivityToFlow(this);
        Intent intent = new Intent(this, WXEntryActivity.class);
        intent.putExtra(Constant.LOGIN_IN_USING_APP_KEY, GeneralSignupActivity.LoginInAppType.TYPE_CHECK_OUT_SWIPE_TO_BUY);
        startActivity(intent);
    }

    private void runAnimationView(final Cart cart) {
        frameLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
        checkoutPopup.animate().translationY(checkoutPopup.getHeight()).setDuration(400).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                checkoutPopup.setVisibility(View.GONE);
            }
        });

        imgAnimation.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.checkout);
        imgAnimation.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgAnimation.setVisibility(View.GONE);

                cartItem.setVisible(true);
                cartBadge.setVisibility(View.VISIBLE);

                Animation fadeIn = AnimationUtils.loadAnimation(CheckoutActivity.this, R.anim.fadein);
                Animation zoomIn = AnimationUtils.loadAnimation(CheckoutActivity.this, R.anim.zoom_in);
                final Animation zoomOut = AnimationUtils.loadAnimation(CheckoutActivity.this, R.anim.zoom_out);

                cartBadge.startAnimation(fadeIn);
                cartBadge.startAnimation(zoomIn);

                cartBadge.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cartBadge.startAnimation(zoomOut);
                        cartBadge.setVisibility(View.GONE);
                        cartIcon.setVisibility(View.GONE);

                        Intent intent = new Intent();
                        intent.putExtra("CART", cart);
                        setResult(RESULT_OK, intent);

                        onBackPressed();
                    }
                }, 800);


            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product_detail, menu);

        //for heart icon
        MenuItem item = menu.findItem(R.id.action_like);
        item.setVisible(false);

        //for cart icon
        cartItem = menu.findItem(R.id.action_cart);
        cartItem.setVisible(false);

        MenuItemCompat.setActionView(cartItem, R.layout.badge_button_cart);

        RelativeLayout badgeCartLayout = (RelativeLayout) MenuItemCompat.getActionView(cartItem);
        cartBadge = (View) badgeCartLayout.findViewById(R.id.btnBadge);
        cartIcon = (View) badgeCartLayout.findViewById(R.id.cartIcon);

        return super.onCreateOptionsMenu(menu);
    }

    private void handleBuyNowAction() {
        Sku sku = style.getSku(selectedSize, selectedColor);

        if (sku == null) {
            MmProgressDialog.dismiss();
            ErrorUtil.showErrorDialog(CheckoutActivity.this, getResources().getString(R.string.LB_MC_COLORS_SIZE_TITLE));
            return;
        }

        if (!MmGlobal.isLogin()) {
            showLogin();
            return;
        }

        // TODO: 3/7/2016
    }

    private void handleAddCartAction() {
        Sku sku = style.getSku(selectedSize, selectedColor);

        if (sku == null) {
            MmProgressDialog.dismiss();
            ErrorUtil.showErrorDialog(CheckoutActivity.this, getResources().getString(R.string.LB_MC_COLORS_SIZE_TITLE));
            return;
        }

        APIManager.getInstance().getCartService().addCartItem(new CartAddRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKeyForCartRequest(), MmGlobal.getCartKeyForCartRequest(), sku.getSkuId(), quantity))
                .enqueue(new MmCallBack<Cart>(CheckoutActivity.this) {
                    @Override
                    public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                        MmProgressDialog.dismiss();
                        Cart cart = response.body();
                        CacheManager.getInstance().setCart(cart);
                        MmGlobal.setAnonymousShoppingCartKey(cart.getCartKey());

                        runAnimationView(cart);
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        super.onFailure(t);
                        MmStatusAlertUtil.show(CheckoutActivity.this, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                getString(R.string.LB_CA_ADD2CART_FAIL), null);
                    }
                });
    }

    private void handleUpdateCartAction() {

        int cartItemID = getIntent().getIntExtra(CART_ITEM_ID, -1);

        if (cartItemID == -1) {
            return;
        }

        Sku sku = style.getSku(selectedSize, selectedColor);

        if (sku == null) {
            ErrorUtil.showErrorDialog(CheckoutActivity.this, getResources().getString(R.string.LB_MC_COLORS_SIZE_TITLE));
            return;
        }

        APIManager.getInstance().getCartService().updateCartItem(new CartUpdateRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKeyForCartRequest(), MmGlobal.getCartKeyForCartRequest(), cartItemID, sku.getSkuId(), quantity))
                .enqueue(new MmCallBack<Cart>(CheckoutActivity.this) {
                    @Override
                    public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        super.onFailure(t);
                    }

                    @Override
                    public void onResponse(Response<Cart> response, Retrofit retrofit) {
                        Cart cart = response.body();

                        Intent intent = new Intent();
                        intent.putExtra("CART", cart);
                        setResult(RESULT_OK, intent);
                        onBackPressed();
                        CacheManager.getInstance().setCart(cart);
                    }
                });
    }


    private void setupSummaryView() {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CHINA);
        format.setMaximumFractionDigits(0);
        textViewPrice.setText(format.format(style.getPrice()));

        textViewSkuName.setText(style.getSkuName());

        Picasso.with(MyApplication.getContext()).load(PathUtil.getBrandImageUrl(style.getBrandSmallLogoImage())).into(brandImageView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        frameLayout.setBackgroundColor(getResources().getColor(R.color.transparent_black));
                    }
                });
            }

        }, 600);
    }


    @Override
    public void onBackPressed() {
        frameLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
        finish();
    }

    private void setupExit() {
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.not_move, R.anim.top_to_bottom);
    }

    private void updateTotalPrice(int quantity) {
        this.quantity = quantity;
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CHINA);
        format.setMaximumFractionDigits(0);
        textViewSummaryPrice.setText(format.format(quantity * style.getPrice()));
    }

    @Override
    public void changeQuantity(int quantity) {
        updateTotalPrice(quantity);
    }

    @Override
    public void selectColor(ProductColor productColor) {
        selectedColor = productColor;
        updateProductImageView();
        setupAnimationView();
    }

    private void updateProductImageView() {
        String imageUrl = style.getImageBaseColor(selectedColor);
        if (TextUtils.isEmpty(imageUrl)) {
            productImageView.setImageResource(R.drawable.spacer);
        } else {
            Picasso.with(MyApplication.getContext()).load(imageUrl).into(productImageView);
        }
    }

    @Override
    public void selectSize(ProductSize productSize) {
        selectedSize = productSize;
    }

    private void addIdForAutomationTest() {
        productImageView.setContentDescription(getContentDescription("UIIMG_PRODUCT"));
        brandImageView.setContentDescription(getContentDescription("UIIMG_BRAND_LOGO"));
        textViewSkuName.setContentDescription(getContentDescription("UILB_PRODUCT_NAME"));
        textViewPrice.setContentDescription(getContentDescription("UILB_RETAIL_PRICE"));
        tvSizeTable.setContentDescription(getContentDescription("UIBT_SIZEGRID"));
        textViewAmountLabel.setContentDescription(getContentDescription("UILB_EDITITEM_SUBTOTAL"));
        textViewSummaryPrice.setContentDescription(getContentDescription("UILB_SUB_TOTAL"));
        btn_add_to_cart.setContentDescription(getContentDescription("UILB_EDITITEM_CHECKOUT"));
    }

    public String getContentDescription(String key) {
        return String.format("CheckoutView-%s", key);
    }
}
