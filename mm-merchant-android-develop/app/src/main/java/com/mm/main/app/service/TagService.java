package com.mm.main.app.service;

import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.User;
import com.mm.main.app.schema.request.SaveTagRequest;
import com.mm.main.app.schema.Tag;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by henrytung on 24/11/2015.
 */
public interface TagService {

    String SERVICE_PATH = "tags";

    @GET(SERVICE_PATH + "/list")
    Call<List<Tag>> list(@Query("tagtypeid") Integer tagtypeid);

    @POST(SERVICE_PATH + "/user/save")
    Call<Boolean> saveTag(@Body SaveTagRequest saveTagRequest);

    @GET(SERVICE_PATH + "/user/merchant")
    Call<Boolean> taggedMerchant(@Query("userid") Integer userid);

    @GET(SERVICE_PATH + "/user/curator")
    Call<List<User>> curator(@Query("userkey") String userkey);

    @GET(SERVICE_PATH + "/user/merchant")
    Call<List<Merchant>> merchant(@Query("userkey") String userkey);

}
