package com.mm.main.app.adapter.strorefront.search;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.activity.storefront.search.ProductListSearchActivity;
import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.SearchListItem;
import com.mm.main.app.record.SearchHistory;
import com.mm.main.app.utils.PathUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Setting List Adapter
 */
public class SearchListItemAdapter extends BaseAdapter {

    private final Activity context;
    private List<SearchListItem> itemList;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    private Bitmap bitmap;

    public SearchListItemAdapter(Activity context, List<SearchListItem> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        SearchListItem listItem = itemList.get(position);
        LayoutInflater inflater = context.getLayoutInflater();
        if (listItem.getCompleteResponse() != null && !TextUtils.isEmpty(listItem.getCompleteResponse().getEntityImage())) {
            view = inflater.inflate(R.layout.search_image_result_item, parent, false);

            ImageView ivSearchLogo = (ImageView) view.findViewById(R.id.ivSearchLogo);
            String url;
            if (listItem.getCompleteResponse().getEntity().equals("Category")
                    || listItem.getCompleteResponse().getEntity().equals("Merchant")) {
                url = PathUtil.getCategoryImageUrl(listItem.getCompleteResponse().getEntityImage());
            } else {
                url = PathUtil.getBrandImageUrl(listItem.getCompleteResponse().getEntityImage());
            }
            Picasso.with(MyApplication.getContext()).load(url).into(ivSearchLogo);

            TextView tvName1 = (TextView) view.findViewById(R.id.tvName1);
            tvName1.setText(listItem.getCompleteResponse().getSearchTermIn());

            TextView tvName2 = (TextView) view.findViewById(R.id.tvName2);
            if (TextUtils.isEmpty(listItem.getCompleteResponse().getSearchTerm())) {
                tvName2.setVisibility(View.GONE);
            } else {
                tvName2.setText(listItem.getCompleteResponse().getSearchTerm());
            }
        } else {
            if (listItem.getType() == SearchListItem.ItemType.TYPE_HISTORY_TITLE) {
                view = inflater.inflate(R.layout.search_history_title_item, parent, false);

                ImageView removeHistory = (ImageView) view.findViewById(R.id.removeHistory);
                removeHistory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SearchHistory.getInstance().cleanHistory();
                        ProductListSearchActivity productListSearchActivity = (ProductListSearchActivity) context;
                        productListSearchActivity.prepareListItem();
                    }
                });
            } else if (listItem.getType() == SearchListItem.ItemType.TYPE_HOT_ITEM_TITLE) {
                view = inflater.inflate(R.layout.search_hot_item_title_item, parent, false);
            } else if (listItem.getType() == SearchListItem.ItemType.TYPE_NO_RESULT) {
                view = inflater.inflate(R.layout.no_search_result_item, parent, false);
            } else if (listItem.getType() == SearchListItem.ItemType.TYPE_HOT_ITEM_LIST) {
                view = inflater.inflate(R.layout.search_suggest_text_item, parent, false);
                TextView textViewTitle = (TextView) view.findViewById(R.id.title);
                textViewTitle.setText(listItem.getContent());
            } else if (listItem.getType() == SearchListItem.ItemType.TYPE_HISTORY_LIST) {
                view = inflater.inflate(R.layout.search_suggest_text_item, parent, false);
                TextView textViewTitle = (TextView) view.findViewById(R.id.title);
                textViewTitle.setText(listItem.getContent());
            }
        }
        return view;
    }

    private View getTextTypeView(int position, View view, Integer color) {
        TextView titleTextView = (TextView) view.findViewById(R.id.setting_title);
        TextView contentTextView = (TextView) view.findViewById(R.id.setting_content);
        titleTextView.setText(itemList.get(position).getTitle());
        contentTextView.setText(itemList.get(position).getContent());

        if (color != null) {
            titleTextView.setTextColor(color);
        }

        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType().ordinal();
    }

    @Override
    public int getViewTypeCount() {
        return SearchListItem.ItemType.values().length;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public SearchListItem getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<SearchListItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<SearchListItem> itemList) {
        this.itemList = itemList;
    }
}
