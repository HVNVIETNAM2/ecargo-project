package com.mm.main.app.schema;

/**
 * Created by ductranvt on 1/27/2016.
 */
public class CartUserUpdate {
    String CultureCode;
    String CartKey;
    //    Integer UserId;
    String UserKey;

    public CartUserUpdate(String cultureCode, String cartKey, String userKey) {
        CultureCode = cultureCode;
        CartKey = cartKey;
        UserKey = userKey;
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getCartKey() {
        return CartKey;
    }

    public void setCartKey(String cartKey) {
        CartKey = cartKey;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }
}
