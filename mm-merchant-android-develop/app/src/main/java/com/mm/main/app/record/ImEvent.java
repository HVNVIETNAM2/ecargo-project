package com.mm.main.app.record;

/**
 * Created by henrytung on 8/3/2016.
 */
public class ImEvent {

    public enum ActionType {
        SEND,
        RECEIVE,
        PING,
        PONG,
        ENABLE_SEND,
        DISABLE_SEND,
        UPDATE_CONV
    }

    ActionType myActionType;
    String message;

    public ActionType getMyActionType() {
        return myActionType;
    }

    public void setMyActionType(ActionType myActionType) {
        this.myActionType = myActionType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ImEvent(ActionType myActionType, String message) {
        this.myActionType = myActionType;
        this.message = message;
    }

}
