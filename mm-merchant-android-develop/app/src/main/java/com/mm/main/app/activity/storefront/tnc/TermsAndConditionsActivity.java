package com.mm.main.app.activity.storefront.tnc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.mm.main.app.R;

/**
 * Created by thienchaud on 02-Feb-16.
 */
public class TermsAndConditionsActivity extends AppCompatActivity {

    private static String TNCPath = "file:///android_asset/mm_tnc.html";

    @Bind(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        ButterKnife.bind(this);

        initView();
    }

    void initView() {
        webView.loadUrl(TNCPath);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (!webView.getUrl().equals(TNCPath)) {
            webView.loadUrl(TNCPath);
            return;
        }
        super.onBackPressed();
    }
}
