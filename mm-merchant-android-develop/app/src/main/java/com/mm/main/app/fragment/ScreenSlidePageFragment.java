package com.mm.main.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.listitem.DiscoverBannerListItem;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;


/**
 * Created by nguyennguyent1 on 2/1/2016.
 */
public class ScreenSlidePageFragment extends Fragment implements View.OnClickListener{
    ImageView imageView;
    DiscoverBannerListItem imageItem;
    OnClickSlideListener listener;
    public interface OnClickSlideListener{
        void onClick(View v, int id);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_slide_page, container, false);

        imageView = (ImageView)rootView.findViewById(R.id.imageView);

        if (imageItem.getBrandUnionMerchant().getEntity().equals("Brand")) {
            Picasso.with(MyApplication.getContext()).load(PathUtil.getBrandImageUrl(imageItem.getKey(), Constant.BRAND_IMAGE_WIDTH)).into(imageView);
        } else {
            Picasso.with(MyApplication.getContext()).load(PathUtil.getMerchantImageUrl(imageItem.getKey(), Constant.BRAND_IMAGE_WIDTH)).into(imageView);
        }

        imageView.setOnClickListener(this);
        return rootView;
    }

    public void setListener(OnClickSlideListener listener){
        this.listener = listener;
    }
    public void setImage(DiscoverBannerListItem imageItem) {
        this.imageItem = imageItem;
    }

    @Override
    public void onClick(View v) {

        if(listener != null){
            listener.onClick(v, v.getId());
        }
    }
}
