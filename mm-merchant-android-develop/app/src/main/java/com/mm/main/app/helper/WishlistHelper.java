package com.mm.main.app.helper;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.schema.CartMergeRequest;
import com.mm.main.app.schema.CartUserUpdate;
import com.mm.main.app.schema.WishList;
import com.mm.main.app.schema.request.CartCreateRequest;
import com.mm.main.app.utils.MmCallBack;

import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by ductranvt on 2/24/2016.
 */
public class WishlistHelper {
    public static void checkWishlistExist() {
        APIManager.getInstance().getWishListService()
                .viewWishListByUser(MmGlobal.getUserKey())
                .enqueue(new MmCallBack<WishList>(MyApplication.getContext()) {
                    @Override
                    public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                    }

                    @Override
                    public void onFailure(Throwable t) {
                    }

                    @Override
                    public void onResponse(Response<WishList> response, Retrofit retrofit) {
                        WishList list = response.body();

                        if (list == null) {
                            if (!MmGlobal.anonymousWishListKey().isEmpty()) {//has guest wl
                                assignAnonymousWishlistToUser();
                            }
                        } else {  //has user wl

                            if (!MmGlobal.anonymousWishListKey().isEmpty()) {//has guest wl
                                mergeAnonymousWishlistToUserWL(list.getCartKey());
                            }
                        }
                    }
                });
    }

    private static void assignAnonymousWishlistToUser() {
        APIManager.getInstance().getWishListService()
                .updateWishListUser(new CartUserUpdate(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.anonymousWishListKey(), MmGlobal.getUserKey()))
                .enqueue(new MmCallBack<WishList>(MyApplication.getContext()) {
                    @Override
                    public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                    }

                    @Override
                    public void onResponse(Response<WishList> response, Retrofit retrofit) {
                        WishList list = response.body();

                        if (list != null) {
                            MmGlobal.setAnonymousWishListKey("");
                            CacheManager.getInstance().setWishlist(list);
                        }
                        else { //fail
                            MmGlobal.setAnonymousWishListKey("");
                        }
                    }
                });
    }

    private static void mergeAnonymousWishlistToUserWL(String cartKey) {
        APIManager.getInstance().getWishListService()
                .mergeWishlist(new CartMergeRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKey(), MmGlobal.anonymousWishListKey()))
                .enqueue(new MmCallBack<WishList>(MyApplication.getContext()) {
                    @Override
                    public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                    }

                    @Override
                    public void onResponse(Response<WishList> response, Retrofit retrofit) {
                        WishList list = response.body();

                        if (list != null) {
                            //merge success we need to set the anonymous id to nil
                            MmGlobal.setAnonymousWishListKey("");
                            CacheManager.getInstance().setWishlist(list);
                        }
                        else { //fail
                            MmGlobal.setAnonymousWishListKey("");
                        }
                    }
                });
    }

    public static  void getAnonymousWishListKeyIfNeeded() {

        if (MmGlobal.anonymousWishListKey().isEmpty()) {

            APIManager.getInstance().getWishListService()
                    .createWishList(new CartCreateRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKey()))
                    .enqueue(new MmCallBack<WishList>(MyApplication.getContext()) {
                        @Override
                        public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                WishList wishList = response.body();

                                CacheManager.getInstance().setWishlist(wishList);
                            } else {
                                // retry? need to clarify with product design
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            // retry? need to clarify with product design
                        }
                    });

        } else {

            Logger.d("storefront", "Local wish list key : " + MmGlobal.anonymousWishListKey());

        }

    }
}
