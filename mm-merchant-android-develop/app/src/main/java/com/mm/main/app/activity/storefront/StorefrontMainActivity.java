package com.mm.main.app.activity.storefront;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.mm.main.app.activity.storefront.discover.DiscoverMainActivity;
import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.discover.DiscoverGroupActivity;
import com.mm.main.app.activity.storefront.friend.ImLandingActivity;
import com.mm.main.app.activity.storefront.profile.StoreFrontUserProfileActivity;
import com.mm.main.app.activity.storefront.signup.GeneralSignupActivity;
import com.mm.main.app.activity.storefront.temp.Tab1Activity;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.activity.storefront.temp.Tab3Activity;
import com.mm.main.app.manager.ProfileLifeCycleManager;
import com.mm.storefront.app.wxapi.WXEntryActivity;

public class StorefrontMainActivity extends TabActivity implements View.OnTouchListener {
    //    LocalActivityManager mlam;
    public static int STORE_FRONT_LOGIN_REQUEST_CODE = 999;
    public static int FRIEND_LOGIN_REQUEST_CODE = 998;
    public static String TAB_POSITION_KEY = "TAB_POSITION_KEY";
    public static AnimationType animationType = null;
    TabHost tabHost;
    boolean tabMovePause = false;
    private int currentTabPosition = 0;

    public enum AnimationType {
        NONE,
        CHECKOUT
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_tab_host);
        this.setTheme(R.style.AppThemeNoActionBar);

        currentTabPosition = getIntent().getIntExtra(TAB_POSITION_KEY, 0);

        initTab();
    }


    public void openTabHost(boolean show) {
//        if (show) {
//            tabHost.getTabWidget().setVisibility(View.VISIBLE);
//        } else {
//            tabHost.getTabWidget().setVisibility(View.GONE);
//        }

//        if (!tabMovePause) {
//            if (open && tabHost.getTabWidget().getVisibility() == View.VISIBLE) {
//                AnimateUtil.collapse(tabHost.getTabWidget(), new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//                        tabMovePause = true;
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        tabMovePause = false;
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });
//            } else {
//                if (tabHost.getTabWidget().getVisibility() == View.GONE) {
//                    AnimateUtil.expand(tabHost.getTabWidget(), new Animation.AnimationListener() {
//                        @Override
//                        public void onAnimationStart(Animation animation) {
//                            tabMovePause = true;
//                        }
//
//                        @Override
//                        public void onAnimationEnd(Animation animation) {
//                            tabMovePause = false;
//                        }
//
//                        @Override
//                        public void onAnimationRepeat(Animation animation) {
//
//                        }
//                    });
//                }
//
//            }
//        }
    }

    private void initTab() {
        //        getSupportActionBar().hide();

//        mlam = new LocalActivityManager(this, false);
//        mlam.dispatchCreate(savedInstanceState);

        // create the TabHost that will contain the Tabs
        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();
//        tabHost.setup(mlam);

        TabHost.TabSpec tab1 = tabHost.newTabSpec("First Tab");
        TabHost.TabSpec tab2 = tabHost.newTabSpec("Second Tab");
        TabHost.TabSpec tab3 = tabHost.newTabSpec("Third tab");
        TabHost.TabSpec tab4 = tabHost.newTabSpec("4 tab");
        TabHost.TabSpec tab5 = tabHost.newTabSpec("5 tab");

        // Set the Tab name and Activity
        // that will be opened when particular Tab will be selected
        setupTab(tab1, R.string.LB_CA_NEWSFEED, R.drawable.ic_tab_home);
        // TODO test InterestMappingCuratorsActivity
        Intent intent1 = new Intent(this, Tab1Activity.class);
//        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        tab1.setContent(intent1);

        setupTab(tab2, R.string.LB_CA_DISCOVER, R.drawable.ic_tab_discover);
        Intent intent2 = new Intent(this, DiscoverGroupActivity.class);
//        intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        tab2.setContent(intent2);

        setupTab(tab3, R.string.LB_CA_STYLEFEED, R.drawable.ic_tab_stylefeed);
        Intent intent3 = new Intent(this, Tab3Activity.class);
//        intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        tab3.setContent(intent3);

        setupTab(tab4, R.string.LB_CA_MESSENGER, R.drawable.ic_tab_im);
        Intent intent4 = new Intent(this, ImLandingActivity.class);
//        intent4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        tab4.setContent(intent4);

        setupTab(tab5, R.string.LB_CA_ME, R.drawable.ic_tab_myprofile);
        Intent intent5 = new Intent(this, StoreFrontUserProfileActivity.class);
        tab5.setContent(intent5);

        /** Add the tabs  to the TabHost to display. */
        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
        tabHost.addTab(tab3);
        tabHost.addTab(tab4);
        tabHost.addTab(tab5);

        tabHost.getTabWidget().getChildAt(0).setBackgroundColor(getResources().getColor(R.color.white));
        tabHost.getTabWidget().getChildAt(1).setBackgroundColor(getResources().getColor(R.color.white));
        tabHost.getTabWidget().getChildAt(2).setBackgroundColor(getResources().getColor(R.color.white));
        tabHost.getTabWidget().getChildAt(3).setBackgroundColor(getResources().getColor(R.color.white));
        tabHost.getTabWidget().getChildAt(4).setBackgroundColor(getResources().getColor(R.color.white));

        tabHost.getTabWidget().setDividerDrawable(null);

        for (int i = 0; i < 5; i++) {
            tabHost.getTabWidget().getChildAt(i).setOnTouchListener(this);
            tabHost.getTabWidget().getChildAt(i).setTag(i + "");
        }

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
//                Activity currentActivity = getCurrentActivity();
                Logger.d("sf main", "here!!!!!!!!!");

            }
        });

        tabHost.setCurrentTab(currentTabPosition);
    }

    private void setupTab(TabHost.TabSpec tab, int titleId, int imageId) {
        View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, null, false);
        ((TextView) tabIndicator.findViewById(R.id.tvTitle)).setText(getResources().getText(titleId));
        ((ImageView) tabIndicator.findViewById(R.id.image)).setImageDrawable(getResources().getDrawable(imageId));
        tab.setIndicator(tabIndicator);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == STORE_FRONT_LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            tabHost.setCurrentTab(4);
        }

        if(requestCode == FRIEND_LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            tabHost.setCurrentTab(3);
        }

        Logger.d("storefrontmain", "i dccam here ");
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mlam.dispatchResume();
    }

    @Override
    protected void onPause() {
        if (animationType != null) {
            switch (animationType) {
                case NONE:
                    super.overridePendingTransition(0, 0);
                    break;
                case CHECKOUT:
                    super.overridePendingTransition(R.anim.bottom_to_top, R.anim.not_move);
                    break;
            }

            animationType = null;
        }
        super.onPause();
//        mlam.dispatchPause(isFinishing());
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (MotionEvent.ACTION_DOWN == event.getAction()) {
            int previousTab = tabHost.getCurrentTab();
            int currentTab = Integer.parseInt((String) v.getTag());
            if(currentTab == 4) {
                if(!MmGlobal.isLogin()) {
                    Intent intent = new Intent(this, WXEntryActivity.class);
                    intent.putExtra(Constant.LOGIN_IN_USING_APP_KEY, GeneralSignupActivity.LoginInAppType.TYPE_LOGIN_REQUIRED);
                    intent.putExtra(Constant.LOGIN_IN_REQUEST_CODE_KEY, STORE_FRONT_LOGIN_REQUEST_CODE);
                    startActivityForResult(intent, STORE_FRONT_LOGIN_REQUEST_CODE);
                    return true;
                }
                else if(previousTab == currentTab && ProfileLifeCycleManager.getInstance().haveHistoryChain()) {
                    Intent intent = new Intent(getApplicationContext(), StorefrontMainActivity.class);
                    intent.putExtra(TAB_POSITION_KEY, currentTab);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    animationType = AnimationType.NONE;
                    startActivity(intent);
                    return true;
                }
            }

            if(currentTab == 3 && !MmGlobal.isLogin()) {
                Intent intent = new Intent(this, WXEntryActivity.class);
                intent.putExtra(Constant.LOGIN_IN_USING_APP_KEY, GeneralSignupActivity.LoginInAppType.TYPE_LOGIN_REQUIRED);
                intent.putExtra(Constant.LOGIN_IN_REQUEST_CODE_KEY, FRIEND_LOGIN_REQUEST_CODE);
                startActivityForResult(intent, FRIEND_LOGIN_REQUEST_CODE);
                return true;
            }

            if (previousTab == currentTab) {
                // TODO: Add condition "currentTab == 1" to execute the code below because this only work for discovery page.
                if(currentTab == 1) {
                    Intent backToTop = new Intent(this, DiscoverMainActivity.class);
                    DiscoverGroupActivity.group.replaceContentView(DiscoverGroupActivity.ACTIVITY_ID_DISCOVER_MAIN, backToTop);
                    return true;
                }
            }
        }
        return false;
    }

}
