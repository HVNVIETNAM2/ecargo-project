package com.mm.main.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mm.main.app.utils.Controllable;
import com.mm.main.app.activity.storefront.discover.DiscoverMainActivity;
import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.discover.DiscoverBrandMainRvAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.DiscoverBannerListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.listitem.DiscoverBrandMainListItem;
import com.mm.main.app.schema.Brand;
import com.mm.main.app.schema.BrandUnionMerchant;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class DiscoverBrandsFragment extends Fragment implements Controllable {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private ImageView[] imageViewIndicators;

    @Bind(R.id.rvDiscoverBrandMain)
    RecyclerView rvDiscoverBrandMain;

    DiscoverBrandMainRvAdapter discoverBrandMainRvAdapter;

    List<BrandUnionMerchant> brandUnionMerchantList;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DiscoverBrandsFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DiscoverBrandsFragment newInstance(int columnCount) {
        DiscoverBrandsFragment fragment = new DiscoverBrandsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_discover_list, container, false);
        ButterKnife.bind(this, view);
        // Set the adapter
        Context context = view.getContext();

        fetchDataList();

        return view;
    }

    private void setupRv() {
        rvDiscoverBrandMain.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));

        List<DiscoverBrandMainListItem> discoverBannerList = setupAdapterData();

        discoverBrandMainRvAdapter = new DiscoverBrandMainRvAdapter(getActivity(), discoverBannerList,
                brandUnionMerchantList, new DiscoverBrandMainRvAdapter.OnListFragmentInteractionListener() {
            @Override
            public void onDiscoverBrandFragmentInteraction(BrandUnionMerchant brandUnionMerchant, int actionCode) {
                if (actionCode == DiscoverBrandMainRvAdapter.ACTION_PROCEED_PRODUCT_LIST_PAGE) {
                    SearchStyle.getInstance().clearAllFilter();
                    if (brandUnionMerchant.getEntity().equals("Brand")) {
                        List<Brand> brandid = new ArrayList<>();
                        Brand brand = new Brand();
                        brand.setBrandId(brandUnionMerchant.getEntityId());
                        brand.setBrandName(brandUnionMerchant.getName());
                        brandid.add(brand);

                        SearchStyle.getInstance().clearAllFilter();
                        SearchStyle.getInstance().setBrandid(brandid);
                    } else if (brandUnionMerchant.getEntity().equals("Merchant")) {
                        List<Merchant> merchantid = new ArrayList<>();
                        Merchant merchant = new Merchant();
                        merchant.setMerchantId(brandUnionMerchant.getEntityId());
                        merchant.setMerchantCompanyName(brandUnionMerchant.getName());
                        merchantid.add(merchant);

                        SearchStyle.getInstance().clearAllFilter();
                        SearchStyle.getInstance().setMerchantid(merchantid);
                    }
                    ((DiscoverMainActivity) getActivity()).startProductListPage(null, false);
                } else if (actionCode == DiscoverBrandMainRvAdapter.ACTION_PROCEED_ALL_BRAND_PAGE) {
                    ((DiscoverMainActivity) getActivity()).startAllBrandPage();
                }
            }
        });
        discoverBrandMainRvAdapter.setFragment(this);
        rvDiscoverBrandMain.setAdapter(discoverBrandMainRvAdapter);
    }

    public List<DiscoverBrandMainListItem> setupAdapterData() {
        List<DiscoverBrandMainListItem> discoverBannerList = new ArrayList<>();
        discoverBannerList.add(new DiscoverBrandMainListItem("1", "2", DiscoverBrandMainListItem.ItemType.TYPE_BRAND_BANNER));
        discoverBannerList.add(new DiscoverBrandMainListItem("3", "4", DiscoverBrandMainListItem.ItemType.TYPE_TWO_COLUMN_BRAND));

        return discoverBannerList;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (discoverBrandMainRvAdapter != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    refreshWholeForm();
                }
            }).start();
        }
    }

    public void refreshWholeForm() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                discoverBrandMainRvAdapter.setItemsData(setupAdapterData());
                discoverBrandMainRvAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void resume() {
        onResume();
    }


    public void fetchDataList() {
         MmProgressDialog.show(getActivity());
        APIManager.getInstance().getSearchService().brandCombined()
                .enqueue(new MmCallBack<List<BrandUnionMerchant>>(getActivity()) {
                    @Override
                    public void onSuccess(Response<List<BrandUnionMerchant>> response, Retrofit retrofit) {
                        brandUnionMerchantList = response.body();
                        setupRv();
                    }
                });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DiscoverBannerListItem item);
    }
}
