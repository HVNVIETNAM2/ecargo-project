package com.mm.main.app.listitem;

/**
 * Created by ductranvt on 1/22/2016.
 */
public class ShoppingCartHeaderRvItem implements ShoppingCartRvItem{
    @Override
    public ItemType getType() {
        return ItemType.TYPE_HEADER;
    }

    @Override
    public boolean getSelected() {
        return false;
    }

    @Override
    public void setSelected(boolean isSelected) {

    }

    @Override
    public int getAppiumIndex() {
        return 0;
    }

    @Override
    public void setAppiumIndex(int appiumIndex) {

    }
}
