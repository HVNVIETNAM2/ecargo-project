package com.mm.main.app.service;

import com.mm.main.app.schema.StyleList;
import com.mm.main.app.schema.response.ProductResponse;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by andrew on 16/11/15.
 */
public interface ProductService {
    String PRODUCT_PATH =  "Product";

    @GET(PRODUCT_PATH)
    Call<ProductResponse> list();

    @GET(PRODUCT_PATH+"/style/list")
    Call<StyleList> styleList(@Query("merchantid") Integer merchantid, @Query("ParentCategoryId") Integer ParentCategoryId);

    @GET(PRODUCT_PATH+"/style/list")
    Call<StyleList> styleListAll(@Query("size") Integer size, @Query("page") Integer page);
}
