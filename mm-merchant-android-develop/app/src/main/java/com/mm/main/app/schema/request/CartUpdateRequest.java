package com.mm.main.app.schema.request;

/**
 * Created by ductranvt on 1/21/2016.
 */
public class CartUpdateRequest {
    String CultureCode;
    String CartKey;
    String UserKey;
    Integer CartItemId;
    Integer SkuId;
    Integer Qty;

    public CartUpdateRequest(String cultureCode, String userKey, String cartKey, Integer cartItemId, Integer skuId, Integer qty) {
        CultureCode = cultureCode;
        UserKey = userKey;
        CartKey = cartKey;
        CartItemId = cartItemId;
        SkuId = skuId;
        Qty = qty;
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getCartKey() {
        return CartKey;
    }

    public void setCartKey(String cartKey) {
        CartKey = cartKey;
    }

    public Integer getCartItemId() {
        return CartItemId;
    }

    public void setCartItemId(Integer cartItemId) {
        CartItemId = cartItemId;
    }

    public Integer getSkuId() {
        return SkuId;
    }

    public void setSkuId(Integer skuId) {
        SkuId = skuId;
    }

    public Integer getQty() {
        return Qty;
    }

    public void setQty(Integer qty) {
        Qty = qty;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }
}
