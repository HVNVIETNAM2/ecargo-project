package com.mm.main.app.schema.response;

/**
 * Created by thienchaud on 14-Mar-16.
 */
public class UploadIDImageResponse {
    String AppCode;
    String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getAppCode() {
        return AppCode;
    }

    public void setAppCode(String appCode) {
        AppCode = appCode;
    }
}
