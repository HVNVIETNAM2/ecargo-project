package com.mm.main.app.activity.merchant.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.merchant.setting.MobileCountrySelectListAdapter;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.MobileCode;
import com.mm.main.app.schema.MobileCodeList;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import retrofit.Response;
import retrofit.Retrofit;


public class ChangeMobileCountryActivity extends ActionBarActivity {

    final public static String EXTRA_USER_DETAIL = "USER_DETAIL";

    @Bind(R.id.list)
    ListView listView;

    @BindString(R.string.LB_COUNTRY_PICK)
    String title;
    private User user;


    MobileCountrySelectListAdapter mobileCountrySelectListAdapter;
    List<MobileCode> mobileCodes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        render();
    }

    private void render() {
        setContentView(R.layout.activity_change_mobile_country);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, title);
        user = (User) this.getIntent().getSerializableExtra(EXTRA_USER_DETAIL);
        fetchMobileCode();
    }


    private void fetchMobileCode() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getReferenceService().listMobileCode()
                .enqueue(new MmCallBack<MobileCodeList>(ChangeMobileCountryActivity.this) {
                    @Override
                    public void onSuccess(Response<MobileCodeList> response, Retrofit retrofit) {
                        mobileCodes = response.body().getMobileCodeList();
                        setupList();
                    }
                });

    }


    private void setupList() {
        mobileCountrySelectListAdapter = new MobileCountrySelectListAdapter(ChangeMobileCountryActivity.this, mobileCodes);
        listView.setAdapter(mobileCountrySelectListAdapter);

    }

    @OnItemClick(R.id.list)
    public void onItemClicked(int position) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", mobileCodes.get(position));
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        render();
    }

    @Override
    public void finish() {
        super.finish();

        overridePendingTransition(R.anim.not_move, R.anim.left_to_right);
    }
}
