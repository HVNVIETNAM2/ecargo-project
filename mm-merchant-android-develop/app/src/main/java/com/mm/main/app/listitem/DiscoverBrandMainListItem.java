package com.mm.main.app.listitem;

/**
 * Setting schema
 */
public class DiscoverBrandMainListItem {
    String title;
    String content;
    ItemType type;

    public enum ItemType {
        TYPE_BRAND_BANNER,
        TYPE_TWO_COLUMN_BRAND
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public DiscoverBrandMainListItem() {
    }

    public DiscoverBrandMainListItem(String title, String content, ItemType type) {

        this.title = title;
        this.content = content;
        this.type = type;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }
}
