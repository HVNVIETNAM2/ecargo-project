package com.mm.main.app.schema;

/**
 * Created by henrytung on 26/10/15.
 */
public class Geo {

    String GeoId;
    String GeoNameInvariant;
    String GeoName;

    public Geo(String geoId, String geoNameInvariant, String geoName) {
        GeoId = geoId;
        GeoNameInvariant = geoNameInvariant;
        GeoName = geoName;
    }

    public Geo() {
    }

    public String getGeoId() {
        return GeoId;
    }

    public void setGeoId(String geoId) {
        GeoId = geoId;
    }

    public String getGeoNameInvariant() {
        return GeoNameInvariant;
    }

    public void setGeoNameInvariant(String geoNameInvariant) {
        GeoNameInvariant = geoNameInvariant;
    }

    public String getGeoName() {
        return GeoName;
    }

    public void setGeoName(String geoName) {
        GeoName = geoName;
    }
}
