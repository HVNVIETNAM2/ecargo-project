package com.mm.main.app.adapter.strorefront.checkout;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import com.mm.main.app.activity.storefront.checkout.AddAddressActivity;
import com.mm.main.app.activity.storefront.checkout.AddressSelectionActivity;
import com.mm.main.app.activity.storefront.checkout.PaymentSelectionActivity;
import com.mm.main.app.activity.storefront.checkout.CheckoutActivity;
import com.mm.main.app.activity.storefront.product.ProductDetailPageActivity;
import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.temp.TestActivity;
import com.mm.main.app.adapter.strorefront.product.ProductDetailSelectionCollection;
import com.mm.main.app.adapter.strorefront.base.BaseRvAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.helper.ProductAvailabilityHelper;
import com.mm.main.app.listitem.BaseRvItem;
import com.mm.main.app.listitem.CheckoutListItem;
import com.mm.main.app.listitem.DynamicAvailableListItem;
import com.mm.main.app.record.StylesRecord;
import com.mm.main.app.schema.ColorImage;
import com.mm.main.app.listitem.ProductDetailListItem;
import com.mm.main.app.schema.ProductColor;
import com.mm.main.app.schema.ProductSize;
import com.mm.main.app.schema.Style;
import com.mm.main.app.utils.FormatUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;
import com.wefika.flowlayout.FlowLayout;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Setting List Adapter
 */
public class CheckoutListItemAdapter extends BaseAdapter implements View.OnFocusChangeListener {

    private final int MAX_QTY = 99;

    private final Activity context;
    private final List<CheckoutListItem> itemList;
    private Bitmap bitmap;
    private Style style;
    private boolean isSwipe;

    private int inputQuantity;

    private ImageView buttonLike;

    private boolean initSize = false;
    private boolean initColor = false;
    private boolean initComment = false;
    private boolean initRecommend = false;

    private ProductDetailSelectionCollection productDetailSelectionCollection;
    private static ProductAvailabilityHelper productAvailabilityHelper;

    CircleImageView[] imageViewsize;
    CircleImageView[] imageViewSizeDisable;
    CircleImageView[] imageColorView;
    CircleImageView[] imageViewColorDisable;

    private long mTextLostFocusTimestamp;
    EditText editTextQuantity;

    private OnUpdateInfoListener onUpdateInfoListener;

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if ((v == editTextQuantity) && !hasFocus) {
            mTextLostFocusTimestamp = System.currentTimeMillis();
        }
    }

    public interface OnUpdateInfoListener {
        void changeQuantity(int quantity);

        void selectColor(ProductColor productColor);

        void selectSize(ProductSize productSize);
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public void setInputQuantity(int inputQuantity) {
        this.inputQuantity = inputQuantity;
    }

    public CheckoutListItemAdapter(Activity context, Style style, boolean isSwipe) {
        super();
        this.context = context;
        this.style = style;
        this.isSwipe = isSwipe;
        onUpdateInfoListener = (OnUpdateInfoListener) context;

        itemList = new ArrayList<>();
        if (style.getColorList().size() != 0) {
            itemList.add(new CheckoutListItem("", "", CheckoutListItem.ItemType.TYPE_COLOR));
        }
        if (style.getSizeList().size() != 0) {
            itemList.add(new CheckoutListItem("", "", CheckoutListItem.ItemType.TYPE_SIZE));
        }
        itemList.add(new CheckoutListItem("", "", CheckoutListItem.ItemType.TYPE_QUANTITY));

        if (this.isSwipe && MmGlobal.isLogin()) {
            itemList.add(new CheckoutListItem("", "", CheckoutListItem.ItemType.TYPE_SHIPPING_ADDRESS));
            itemList.add(new CheckoutListItem("", "", CheckoutListItem.ItemType.TYPE_PAYMENT));
            itemList.add(new CheckoutListItem("", "", CheckoutListItem.ItemType.TYPE_FAPIAO));
        }

        List<DynamicAvailableListItem<ProductColor>> colorDataList = new ArrayList<>();
        for (ProductColor productColor : style.getColorList()) {
            colorDataList.add(new DynamicAvailableListItem<>(productColor));
        }

        List<DynamicAvailableListItem<ProductSize>> sizeDataList = new ArrayList<>();
        for (ProductSize productSize : style.getSizeList()) {
            sizeDataList.add(new DynamicAvailableListItem<>(productSize));
        }

        productAvailabilityHelper = new ProductAvailabilityHelper(style.getSkuList(), sizeDataList, colorDataList);

        mTextLostFocusTimestamp = -1;
    }

    public void setProductDetailSelectionCollection(ProductDetailSelectionCollection productDetailSelectionCollection) {
        this.productDetailSelectionCollection = productDetailSelectionCollection;
        autoSelect();
    }

    private void autoSelect() {
        if (style.getColorList().size() == 1) {
            productDetailSelectionCollection.setSelectedColor(new DynamicAvailableListItem<ProductColor>(style.getColorList().get(0)));
            onUpdateInfoListener.selectColor(style.getColorList().get(0));
        }

        if (style.getSizeList().size() == 1) {
            productDetailSelectionCollection.setSelectedSize(new DynamicAvailableListItem<ProductSize>(style.getSizeList().get(0)));
            onUpdateInfoListener.selectSize(style.getSizeList().get(0));
        }
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        CheckoutListItem listItem = itemList.get(position);

        switch (listItem.getType()) {
            case TYPE_SIZE:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.size_item_view, null, true);
                }
                setupSizeItemView(view);
                break;
            case TYPE_COLOR:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.color_item_view, null, true);
                }
                setupColorItemView(view);
                break;
            case TYPE_QUANTITY:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.quantity_item_view, parent, false);
                }
                setupQuantityItemView(view);
                break;

            case TYPE_SHIPPING_ADDRESS:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.checkout_address_item_view, null, true);
                }
                setupAddressView(view);
                break;
            case TYPE_PAYMENT:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.checkout_address_item_view, null, true);
                }
                setupPaymentView(view);
                break;
            case TYPE_FAPIAO:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.checkout_fapiao_item_view, null, true);
                }
                setupFapiaoView(view);
                break;
        }
        setAppiumId(view, position);
        return view;
    }

    private void setAppiumId(View view, int position) {
        CheckoutActivity mainActivity = (CheckoutActivity) context;
        CheckoutListItem listItem = itemList.get(position);

        switch (listItem.getType()) {
            case TYPE_SIZE:
                view.setContentDescription(mainActivity.getContentDescription("UIRB_COLOR_OPT"));
                break;
            case TYPE_COLOR:
                view.setContentDescription(mainActivity.getContentDescription("UIRB_SIZE_OPT"));
                break;
            case TYPE_QUANTITY:
                view.findViewById(R.id.btnIncrease).setContentDescription(mainActivity.getContentDescription("UIBT_QTY_INCR"));
                view.findViewById(R.id.btnDecrease).setContentDescription(mainActivity.getContentDescription("UIBT_QTY_DECR"));
                view.findViewById(R.id.editTextQuantity).setContentDescription(mainActivity.getContentDescription("UITB_QTY"));
                view.findViewById(R.id.txtTitleQuantity).setContentDescription(mainActivity.getContentDescription("UILB_EDITITEM_QTY"));
                break;
            case TYPE_SHIPPING_ADDRESS:
                view.setContentDescription(mainActivity.getContentDescription("UIBT_SHIP_ADDR"));
                view.findViewById(R.id.value).setContentDescription(mainActivity.getContentDescription("UILB_EDITITEM_SHIPADD"));
                break;
            case TYPE_PAYMENT:
                view.setContentDescription(mainActivity.getContentDescription("UIBT_PAYMENT_METHOD"));
                view.findViewById(R.id.value).setContentDescription(mainActivity.getContentDescription("UILB_EDITITEM_PAYMETHOD"));
                break;
            case TYPE_FAPIAO:
                view.findViewById(R.id.title).setContentDescription(mainActivity.getContentDescription("UILB_FAPIAO_TITLE"));
                view.findViewById(R.id.value).setContentDescription(mainActivity.getContentDescription("UITB_FAPIAO_TITLE"));
                break;
        }
    }

    private void setupAddressView(View view) {
        TextView value = (TextView) view.findViewById(R.id.value);

        value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i;
                //If user has previous address record{
                i = new Intent(context, AddressSelectionActivity.class);
                context.startActivity(i);
                //else
//                i = new Intent(context, AddAddressActivity.class);
//                context.startActivity(i);
            }
        });
    }

    private void setupPaymentView(View view) {
        TextView title = (TextView) view.findViewById(R.id.title);
        TextView value = (TextView) view.findViewById(R.id.value);

        title.setText(context.getResources().getString(R.string.LB_CA_EDITITEM_PAYMENT_METHOD));
        value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, PaymentSelectionActivity.class);
                context.startActivity(i);
            }
        });
    }

    private void setupFapiaoView(View view) {
        TextView title = (TextView) view.findViewById(R.id.title);
        EditText value = (EditText) view.findViewById(R.id.value);

    }

    private void setupSizeItemView(View view) {
        FlowLayout layout = (FlowLayout) view.findViewById(R.id.horizontal_scroll);
        TextView textViewColorLabel = (TextView) view.findViewById(R.id.textViewColorLabel);
        textViewColorLabel.setVisibility(View.GONE);

        if (!initSize) {
            List<DynamicAvailableListItem<ProductSize>> sizeDataList = productAvailabilityHelper.getDefaultSizeAvailability();
            imageViewsize = new CircleImageView[sizeDataList.size()];
            imageViewSizeDisable = new CircleImageView[sizeDataList.size()];
            UiUtil.addCircleSizeToFlowlayout(layout, imageViewsize, sizeDataList, imageViewSizeDisable);
            initSize = true;
        }

        if (productDetailSelectionCollection.getSelectedColor() != null) {
            productAvailabilityHelper.updateSizeAvailability(productDetailSelectionCollection.getSelectedColor());
        }

        CircleImageView circleImageView[] = imageViewsize;
        CircleImageView circleImageViewDisable[] = imageViewSizeDisable;
        final List<DynamicAvailableListItem<ProductSize>> productSizeList = productAvailabilityHelper.getSizeList();
        for (int i = 0; i < circleImageView.length; i++) {
            final DynamicAvailableListItem<ProductSize> productSize = productSizeList.get(i);

            int colorId = R.color.secondary1;

            if (productDetailSelectionCollection.getSelectedSize() != null &&
                    productSize.getT().getSizeId().intValue() == productDetailSelectionCollection.getSelectedSize().getT().getSizeId().intValue()) {
                colorId = R.color.black;
            }
            circleImageView[i].setBorderColor(MyApplication.getContext().getResources().getColor(colorId));

            circleImageView[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (productSize.isAvailable()) {
                        CircleImageView circleImageView = (CircleImageView) v;
                        if (productDetailSelectionCollection.getSelectedSize() == null ||
                                productDetailSelectionCollection.getSelectedSize().getT().getSizeId().intValue() !=
                                        productSize.getT().getSizeId().intValue()) {
                            productDetailSelectionCollection.setSelectedSize(productSize);
                            v.setSelected(true);
                            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.black));
                            productAvailabilityHelper.updateColorAvailability(productSize);
                            onUpdateInfoListener.selectSize(productSize.getT());
                        }
                        notifyDataSetChanged();
                    }
                }
            });

            if (productSize.isAvailable()) {
                circleImageViewDisable[i].setVisibility(View.INVISIBLE);
            } else {
                circleImageViewDisable[i].setVisibility(View.VISIBLE);
                if (productDetailSelectionCollection.getSelectedSize() != null &&
                        productSize.getT().getSizeId().intValue() == productDetailSelectionCollection.getSelectedSize().getT().getSizeId().intValue()) {
                    productDetailSelectionCollection.setSelectedSize(null);
                    circleImageView[i].setSelected(false);
                    circleImageView[i].setBorderColor(MyApplication.getContext().getResources().getColor(R.color.secondary1));
                }
            }
        }
    }

    private void setupColorItemView(View view) {
        FlowLayout flowLayout = (FlowLayout) view.findViewById(R.id.horizontal_scroll);
        TextView textView = (TextView) view.findViewById(R.id.textViewColorLabel);
        textView.setVisibility(View.GONE);

        List<ColorImage> colorImageList = style.getColorImageList();

        if (!initColor) {
            List<DynamicAvailableListItem<ProductColor>> colorDataList = productAvailabilityHelper.getDefaultColorAvailability();

            imageColorView = new CircleImageView[colorDataList.size()];
            imageViewColorDisable = new CircleImageView[colorDataList.size()];
            UiUtil.addCircleColorToFlowlayout(flowLayout, imageColorView, colorDataList, imageViewColorDisable, colorImageList);
            initColor = true;
        }

        if (productDetailSelectionCollection.getSelectedSize() != null) {
            productAvailabilityHelper.updateColorAvailability(productDetailSelectionCollection.getSelectedSize());
        }

        CircleImageView circleImageView[] = imageColorView;
        CircleImageView circleImageViewDisable[] = imageViewColorDisable;
        List<DynamicAvailableListItem<ProductColor>> productColorList = productAvailabilityHelper.getColorList();
        for (int i = 0; i < circleImageView.length; i++) {
            final DynamicAvailableListItem<ProductColor> productColor = productColorList.get(i);
            int colorId = R.color.secondary1;

            if (productDetailSelectionCollection.getSelectedColor() != null &&
                    productColor.getT().getColorKey().toUpperCase().equals(
                            productDetailSelectionCollection.getSelectedColor().getT().getColorKey().toUpperCase())) {
                colorId = R.color.black;
            }
            circleImageView[i].setBorderColor(MyApplication.getContext().getResources().getColor(colorId));

            circleImageView[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (productColor.isAvailable()) {
                        CircleImageView circleImageView = (CircleImageView) v;
                        if (productDetailSelectionCollection.getSelectedColor() == null ||
                                !productDetailSelectionCollection.getSelectedColor().getT().getColorKey().toUpperCase().equals(
                                        productColor.getT().getColorKey().toUpperCase())) {
                            productDetailSelectionCollection.setSelectedColor(productColor);
                            v.setSelected(true);
                            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.black));
                            productAvailabilityHelper.updateSizeAvailability(productColor);
                            onUpdateInfoListener.selectColor(productColor.getT());
                        }
                        //updateFeatureImagesList(productColor);
                        notifyDataSetChanged();
                    }
                }
            });

            if (productColor.isAvailable()) {
                circleImageViewDisable[i].setVisibility(View.INVISIBLE);
            } else {
                circleImageViewDisable[i].setVisibility(View.VISIBLE);
                if (productDetailSelectionCollection.getSelectedColor() != null &&
                        productColor.getT().getColorKey().toUpperCase().equals(
                                productDetailSelectionCollection.getSelectedColor().getT().getColorKey().toUpperCase())) {
                    productDetailSelectionCollection.setSelectedColor(null);
                    circleImageView[i].setSelected(false);
                    circleImageView[i].setBorderColor(MyApplication.getContext().getResources().getColor(R.color.secondary1));
                }
            }
        }
    }

    private void setupColorItemView1(View view) {
        FlowLayout layout = (FlowLayout) view.findViewById(R.id.horizontal_scroll);
        List<ColorImage> imageDatas = style.getColorImageList();

        if (!initColor) {
            for (int i = 0; i < imageDatas.size(); i++) {
                ColorImage imageData = imageDatas.get(i);
                CircleImageView imageView = new CircleImageView(MyApplication.getContext());
                FlowLayout.LayoutParams layoutParams =
                        new FlowLayout.LayoutParams(UiUtil.getPixelFromDp(58), UiUtil.getPixelFromDp(58));
                int padding = UiUtil.getPixelFromDp(8);
                layoutParams.setMargins(0, 0, padding, padding);
                imageView.setLayoutParams(layoutParams);

                //            ImageView imageView = new ImageView(MyApplication.getContext());
                imageView.setId(i);

                imageView.setImageBitmap(BitmapFactory.decodeResource(
                        MyApplication.getContext().getResources(), R.drawable.brand_logo_temp));
                imageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.mm_input_gray));
                imageView.setBorderWidth(UiUtil.getPixelFromDp(1));

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CircleImageView circleImageView = (CircleImageView) v;
                        if (!v.isSelected()) {
                            v.setSelected(true);
                            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.primary1));
                        } else {
                            v.setSelected(false);
                            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.mm_input_gray));
                        }
                    }
                });
                layout.addView(imageView);

                Picasso.with(MyApplication.getContext()).load(PathUtil.getProductImageUrl(imageData.getImageKey())).into(imageView);
                initColor = true;
            }
        }
    }

    private void reclaimFocus(View v, long timestamp) {
        if (timestamp == -1)
            return;
        if ((System.currentTimeMillis() - timestamp) < 250) {
            v.requestFocus();
            editTextQuantity.setSelection(editTextQuantity.getText().length());
        }
    }

    private void setupQuantityItemView(View view) {
        Button btnIncrease = (Button) view.findViewById(R.id.btnIncrease);
        Button btnDecrease = (Button) view.findViewById(R.id.btnDecrease);
        editTextQuantity = (EditText) view.findViewById(R.id.editTextQuantity);
        editTextQuantity.setOnFocusChangeListener(this);

        editTextQuantity.setText(inputQuantity + "");
        editTextQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String quantityString = s.toString();
                if (!quantityString.isEmpty()) {
                    try {
                        int quantity = Integer.parseInt(s.toString());
                        if (quantity < 1) {
                            editTextQuantity.setText("1");
                        } else if (quantity > MAX_QTY) {
                            editTextQuantity.setText(String.valueOf(MAX_QTY));
                        }
                    } catch (Exception e) {
                        editTextQuantity.setText("1");
                    }

                } else {
                    // we allow empty
                }

                if (!editTextQuantity.getText().toString().isEmpty()) {
                    int quantity = Integer.parseInt(editTextQuantity.getText().toString());
                    inputQuantity = quantity;
                    onUpdateInfoListener.changeQuantity(quantity);
                    editTextQuantity.setSelection(editTextQuantity.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        reclaimFocus(editTextQuantity, mTextLostFocusTimestamp);

        btnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextQuantity.getText().toString().isEmpty()) {
                    editTextQuantity.setText("1");
                }
                int quantity = Integer.parseInt(editTextQuantity.getText().toString());
                if (quantity < MAX_QTY) {
                    quantity++;
                } else {
                    quantity = MAX_QTY;
                }
                editTextQuantity.setText(String.valueOf(quantity));
                editTextQuantity.setSelection(editTextQuantity.getText().length());
                onUpdateInfoListener.changeQuantity(quantity);
            }
        });

        btnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextQuantity.getText().toString().isEmpty()) {
                    editTextQuantity.setText("1");
                }
                int quantity = Integer.parseInt(editTextQuantity.getText().toString());
                if (quantity > 1) {
                    quantity = quantity - 1;
                } else {
                    quantity = 1;
                }
                editTextQuantity.setText(String.valueOf(quantity));
                editTextQuantity.setSelection(editTextQuantity.getText().length());
                onUpdateInfoListener.changeQuantity(quantity);
            }
        });
    }

    private void setupBuyItemView(final View view) {

        final TextView tvPriceAfterSwipe = (TextView) view.findViewById(R.id.tvPriceAfterSwipe);
        final TextView tvPriceBeforeSwipe = (TextView) view.findViewById(R.id.tvPriceBeforeSwipe);
        final FrameLayout frameLayoutSwipeButton = (FrameLayout) view.findViewById(R.id.frameLayoutSwipeButton);
        final SlidingDrawer slidingDrawer1 = (SlidingDrawer) view.findViewById(R.id.slidingDrawer1);
        final ImageView handle = (ImageView) view.findViewById(R.id.handle);
        final ImageView content = (ImageView) view.findViewById(R.id.content);
        final ImageView imageView2 = (ImageView) view.findViewById(R.id.imageView2);

        tvPriceBeforeSwipe.setText(FormatUtil.shortFormatAmount(style.getPriceSale()));
        tvPriceAfterSwipe.setText("一扫即买 " + FormatUtil.formatAmount(style.getPriceSale()));

        slidingDrawer1.setOnDrawerScrollListener(new SlidingDrawer.OnDrawerScrollListener() {
            boolean isOpened = false;

            @Override
            public void onScrollStarted() {
                if (!isOpened) {
                    tvPriceBeforeSwipe.setVisibility(View.INVISIBLE);
                    tvPriceAfterSwipe.setVisibility(View.VISIBLE);
                    slidingDrawer1.setBackground(MyApplication.getContext().getResources().getDrawable(R.drawable.icon_swipe_toggle_on_full));
                    handle.setImageResource(R.drawable.icon_swipe_toggle_button);
                    content.setImageResource(R.drawable.icon_swipe_toggle_off);
                    imageView2.setImageResource(R.drawable.icon_swipe_toggle_frame);

                    Animation anim = new ScaleAnimation(
                            0f, 1f, // Start and end values for the X axis scaling
                            1f, 1f, // Start and end values for the Y axis scaling
                            Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                            Animation.RELATIVE_TO_SELF, 0f); // Pivot point of Y scaling
                    anim.setDuration(1000);
                    anim.setFillAfter(true);

                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            Intent intent = new Intent(MyApplication.getContext(), TestActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(ProductDetailPageActivity.EXTRA_PRODUCT_DATA, style);
                            MyApplication.getContext().startActivity(intent);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    frameLayoutSwipeButton.startAnimation(anim);
                    isOpened = true;
                }
            }

            @Override
            public void onScrollEnded() {

            }
        });


        handle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                slidingDrawer1.setBackground(MyApplication.getContext().getResources().getDrawable(R.drawable.icon_swipe_toggle_on_full));
                handle.setImageResource(R.drawable.icon_swipe_toggle_button);
                content.setImageResource(R.drawable.icon_swipe_toggle_off);
                imageView2.setImageResource(R.drawable.icon_swipe_toggle_frame);

            }
        });
    }

    private void setupImageDefaultItemView(View view) {
        ImageView imageViewDefault = (ImageView) view.findViewById(R.id.defaultImage);
        Picasso.with(MyApplication.getContext()).load(PathUtil.getProductImageUrl(style.getImageDefault())).into(imageViewDefault);

        buttonLike = (ImageView) view.findViewById(R.id.buttonLike);
        setupLikeButton();
    }

    private void setupNameItemView(View view) {
        ImageView brandImageView = (ImageView) view.findViewById(R.id.brandImageView);
        TextView textViewSkuName = (TextView) view.findViewById(R.id.textViewSkuName);
        textViewSkuName.setText(style.getSkuName());
    }

    private void setupDescItemView(View view) {
        TextView textViewDesc = (TextView) view.findViewById(R.id.textViewDesc);
        textViewDesc.setText(style.getSkuName());
    }

    private void setupParameterItemView(View view) {
        TextView textViewParameterValue = (TextView) view.findViewById(R.id.textViewParameterValue);
    }

    private void setupCommentItemView(View view) {
        TextView tvCommentNumber = (TextView) view.findViewById(R.id.tvCommentNumber);
        tvCommentNumber.setText("(112" +
                MyApplication.getContext().getResources().getString(R.string.LB_CA_COMMENT) + ")");

        LinearLayout linearLayoutCommentImage = (LinearLayout) view.findViewById(R.id.linearLayoutCommentImage);

        if (!initComment) {
            for (int i = 0; i < 2; i++) {
                CircleImageView imageView = new CircleImageView(MyApplication.getContext());
                LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(45, 70);
                int rightPadding = UiUtil.getPixelFromDp(5);
                layoutParams.setMargins(0, 0, rightPadding, 0);
                imageView.setLayoutParams(layoutParams);
                imageView.setId(i);
                imageView.setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.brand_logo_temp));
                linearLayoutCommentImage.addView(imageView);
            }
            initComment = true;
        }
    }

    private void setupRecommendItemView(View view) {
        TextView textViewRecommendNumber = (TextView) view.findViewById(R.id.textViewRecommendNumber);
        textViewRecommendNumber.setText("116" +
                MyApplication.getContext().getResources().getString(R.string.LB_CA_NUM_PPL_RCMD_ITEM));

        LinearLayout linearLayoutRecommendImage = (LinearLayout) view.findViewById(R.id.linearLayoutRecommendImage);

        if (!initRecommend) {
            for (int i = 0; i < 7; i++) {
                ImageView imageView = new ImageView(MyApplication.getContext());
                LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(30, 30);
                int rightPadding = UiUtil.getPixelFromDp(5);
                layoutParams.setMargins(0, 0, rightPadding, 0);
                imageView.setLayoutParams(layoutParams);
                imageView.setId(i);
                imageView.setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.placeholder));
                linearLayoutRecommendImage.addView(imageView);
            }
            initRecommend = true;
        }
    }

    private void setupDescriptionImageItemView(View view) {
        // 1. get a reference to recyclerView
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rvDescriptionImage);

        BaseRvItem itemsData[] = new BaseRvItem[style.getColorImageList().size()];

        for (int i = 0; i < style.getColorImageList().size(); i++) {
            ColorImage imageData = style.getColorImageList().get(i);
            itemsData[i] = new BaseRvItem(imageData.getColorKey(), imageData.getImageKey());

        }

        recyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));
        BaseRvAdapter mAdapter = new BaseRvAdapter(itemsData);
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType().ordinal();
    }

    @Override
    public int getViewTypeCount() {
        return ProductDetailListItem.ItemType.values().length;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public CheckoutListItem getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void setupLikeButton() {
        if (buttonLike != null) {
            buttonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setSelected(!v.isSelected());
                }
            });
        }
    }

}
