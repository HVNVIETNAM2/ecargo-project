package com.mm.main.app.activity.storefront.friend;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Response;
import retrofit.Retrofit;

public class UserQrCodeActivity extends AppCompatActivity {

    @Bind(R.id.profileImageView)
    CircleImageView profileImageView;

    @Bind(R.id.tvUserName)
    TextView tvUserName;

    @Bind(R.id.imgQrCode)
    ImageView imgQrCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_qr_code);
        ButterKnife.bind(this);
        getUserService();
        generateQrCode();
    }


    private void generateQrCode() {
        try {
            Bitmap bitmap = encodeAsBitmap(Constant.URL_QR_CODE + MmGlobal.getUserKey());
            imgQrCode.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.content)
    public void onClickFinsih() {
        finish();
    }

    private void getUserService() {

        MmProgressDialog.show(getApplicationContext());

        APIManager.getInstance().getUserService().viewUser(MmGlobal.getUserId())
                .enqueue(new MmCallBack<User>(UserQrCodeActivity.this) {
                    @Override
                    public void onSuccess(Response<User> response, Retrofit retrofit) {

                        if (response.body() != null) {
                            tvUserName.setText(response.body().getDisplayName());

                            Picasso.with(MyApplication.getContext()).load(PathUtil.getUserImageUrl(response.body().getProfileImage()))
                            .placeholder(R.drawable.placeholder).into(profileImageView);
                        }
                    }
                });
    }


    private Bitmap encodeAsBitmap(String str) throws WriterException {

        int wd = (int) getResources().getDimension(R.dimen.user_qr_code_code_size);
        int pinkColor = getResources().getColor(R.color.primary1);
        int whiteColor = getResources().getColor(R.color.white);

        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, wd, wd, null);
        } catch (IllegalArgumentException iae) {
            // TODO Unsupported format
            return null;
        }

        //Change color pixel
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? pinkColor : whiteColor;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, wd, 0, 0, w, h);

        return bitmap;
    }

}
