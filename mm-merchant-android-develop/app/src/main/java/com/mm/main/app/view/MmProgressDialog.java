package com.mm.main.app.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;

import com.mm.main.app.activity.storefront.discover.DiscoverMainActivity;
import com.mm.main.app.activity.storefront.product.ProductListActivity;
import com.mm.main.app.R;

/**
 * Created by andrew on 10/11/15.
 */
public class MmProgressDialog {

    public static ProgressDialog progressDialog;

    public static void show(Context context) {
        if (context instanceof ProductListActivity ||
             context instanceof DiscoverMainActivity) {
            context = ((AppCompatActivity) context).getParent();
        }
        MmProgressDialog.dismiss();
        try {
            progressDialog = new ProgressDialog(context);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.show();
            progressDialog.setContentView(R.layout.progress_dialog);
        } catch (Exception e) {}
    }

    public static ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public static void dismiss() {
        if (getProgressDialog() != null) {
            getProgressDialog().dismiss();
        }
    }
}
