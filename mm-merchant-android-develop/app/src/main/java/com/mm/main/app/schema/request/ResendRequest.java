package com.mm.main.app.schema.request;

/**
 * Created by andrew on 10/11/15.
 */
public class ResendRequest {
    String UserKey;

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public ResendRequest(String userKey) {

        UserKey = userKey;
    }

    public ResendRequest() {

    }
}
