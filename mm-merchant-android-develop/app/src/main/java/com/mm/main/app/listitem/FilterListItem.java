package com.mm.main.app.listitem;

import java.io.Serializable;

/**
 * Created by henrytung on 18/1/2016.
 */
public class FilterListItem<T> implements Serializable {

    public enum ItemType {
        TYPE_FILTER_TITLE,
        TYPE_FILTER_LIST
    }

    T t;
    boolean selected = false;
    private Integer SpanSize;
    private boolean isEndLine;
    private boolean isStartLine;

    private boolean isEndGroup;

    public FilterListItem(T t, boolean selected, ItemType type) {
        this.t = t;
        this.selected = selected;
        this.type = type;
        SpanSize = 1;
    }

    public FilterListItem() {

    }

    public boolean isEndGroup() {
        return isEndGroup;
    }

    public void setIsEndGroup(boolean isEndGroup) {
        this.isEndGroup = isEndGroup;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    ItemType type;

    public FilterListItem(T t) {
        this.t = t;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public Integer getSpanSize() {
        return SpanSize;
    }

    public void setSpanSize(Integer spanSize) {
        SpanSize = spanSize;
    }

    public boolean getIsStartLine() {
        return isStartLine;
    }

    public void setIsStartLine(boolean isStartGroup) {
        this.isStartLine = isStartGroup;
    }

    public boolean getIsEndLine() {
        return isEndLine;
    }

    public void setIsEndLine(boolean isEndGroup) {
        this.isEndLine = isEndGroup;
    }
}
