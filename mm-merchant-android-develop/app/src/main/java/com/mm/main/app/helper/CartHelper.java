package com.mm.main.app.helper;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.schema.Cart;
import com.mm.main.app.schema.CartMergeRequest;
import com.mm.main.app.schema.CartUserUpdate;
import com.mm.main.app.schema.request.CartCreateRequest;
import com.mm.main.app.utils.MmCallBack;

import retrofit.Response;
import retrofit.Retrofit;


/**
 * Created by ductranvt on 2/24/2016.
 */
public class CartHelper {
    public static void checkCartExist() {
        APIManager.getInstance().getCartService()
                .viewCartByUserKey(MmGlobal.getUserKey())
                .enqueue(new MmCallBack<Cart>(MyApplication.getContext()) {
                    @Override
                    public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                    }

                    @Override
                    public void onFailure(Throwable t) {
                    }

                    @Override
                    public void onResponse(Response<Cart> response, Retrofit retrofit) {
                        Cart cart = response.body();

                        if (cart == null) {
                            if (!MmGlobal.anonymousShoppingCartKey().isEmpty()) {//has guest cart
                                assignAnonymousCartToUser();
                            }
                        } else { //has user cart

                            if (!MmGlobal.anonymousShoppingCartKey().isEmpty()) {//has guest cart
                                mergeAnonymousCartToUserCart(cart.getCartKey());
                            }
                        }
                    }
                });
    }

    private static void assignAnonymousCartToUser() {
        APIManager.getInstance().getCartService()
                .updateCartUser(new CartUserUpdate(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.anonymousShoppingCartKey(), MmGlobal.getUserKey()))
                .enqueue(new MmCallBack<Cart>(MyApplication.getContext()) {
                    @Override
                    public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                    }

                    @Override
                    public void onResponse(Response<Cart> response, Retrofit retrofit) {
                        Cart cart = response.body();

                        if (cart != null) {
                            MmGlobal.setAnonymousShoppingCartKey("");
                            CacheManager.getInstance().setCart(cart);
                        } else { //fail
                            MmGlobal.setAnonymousShoppingCartKey("");
                        }
                    }
                });
    }

    private static void mergeAnonymousCartToUserCart(String cartKey) {
        APIManager.getInstance().getCartService()
                .mergeCart(new CartMergeRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKey(), MmGlobal.anonymousShoppingCartKey()))
                .enqueue(new MmCallBack<Cart>(MyApplication.getContext()) {
                    @Override
                    public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                    }

                    @Override
                    public void onResponse(Response<Cart> response, Retrofit retrofit) {
                        Cart cart = response.body();

                        if (cart != null) {
                            //merge success we need to set the anonymous id to nil
                            MmGlobal.setAnonymousShoppingCartKey("");
                            CacheManager.getInstance().setCart(cart);
                        } else { //fail
                            MmGlobal.setAnonymousShoppingCartKey("");
                        }
                    }
                });
    }

    public static void getAnonymousShoppingCartKeyIfNeeded() {

        if (MmGlobal.anonymousShoppingCartKey().isEmpty()) {

            APIManager.getInstance().getCartService()
                    .createCart(new CartCreateRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKey()))
                    .enqueue(new MmCallBack<Cart>(MyApplication.getContext()) {
                        @Override
                        public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                Cart cart = response.body();

                                CacheManager.getInstance().setCart(cart);
                            } else {
                                // retry? need to clarify with product design
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            // retry? need to clarify with product design
                        }
                    });

        } else {

            Logger.d("storefront", "Local cart key : " + MmGlobal.anonymousShoppingCartKey());

        }

    }
}
