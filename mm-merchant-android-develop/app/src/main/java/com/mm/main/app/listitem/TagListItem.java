package com.mm.main.app.listitem;

import com.mm.main.app.schema.Tag;

/**
 * Created by haivu on 2/1/16.
 */
public class TagListItem {

    private Tag tag;
    private Boolean IsSelected;
    private Integer SpanSize;
    private Integer SpanRemaining;
    private Boolean IsStartGroup;

    public TagListItem(Tag tag) {
        this.tag = tag;
        SpanSize = 1;
        IsSelected = false;
        SpanRemaining = 1;
        IsStartGroup = false;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public Integer getSpanSize() {
        return SpanSize;
    }

    public void setSpanSize(Integer spanSize) {
        SpanSize = spanSize;
    }

    public Boolean getSelected() {
        return IsSelected;
    }

    public void setSelected(Boolean selected) {
        IsSelected = selected;
    }

    public Integer getSpanRemaining() {
        return SpanRemaining;
    }

    public void setSpanRemaining(Integer spanRemaining) {
        SpanRemaining = spanRemaining;
    }

    public Boolean getIsStartGroup() {
        return IsStartGroup;
    }

    public void setIsStartGroup(Boolean isStartGroup) {
        IsStartGroup = isStartGroup;
    }
}
