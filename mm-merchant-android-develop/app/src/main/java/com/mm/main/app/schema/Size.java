
package com.mm.main.app.schema;

import java.io.Serializable;

public class Size implements Serializable {

    private Integer SizeId;
    private String SizeCode;
    private String SizeNameInvariant;
    private String SizeGroup;
    private Integer SizeCultureId;
    private String CultureCode;
    private String SizeName;
    private Integer SizeGroupId;
    private String SizeGroupName;
    private String SizeGroupNameInvariant;

    public String getSizeGroupName() {
        return SizeGroupName;
    }

    public void setSizeGroupName(String sizeGroupName) {
        SizeGroupName = sizeGroupName;
    }

    public String getSizeGroupNameInvariant() {
        return SizeGroupNameInvariant;
    }

    public void setSizeGroupNameInvariant(String sizeGroupNameInvariant) {
        SizeGroupNameInvariant = sizeGroupNameInvariant;
    }

    public Integer getSizeGroupId() {
        return SizeGroupId;
    }

    public void setSizeGroupId(Integer sizeGroupId) {
        SizeGroupId = sizeGroupId;
    }


    /**
     * @return The SizeId
     */
    public Integer getSizeId() {
        return SizeId;
    }

    /**
     * @param SizeId The SizeId
     */
    public void setSizeId(Integer SizeId) {
        this.SizeId = SizeId;
    }

    /**
     * @return The SizeCode
     */
    public String getSizeCode() {
        return SizeCode;
    }

    /**
     * @param SizeCode The SizeCode
     */
    public void setSizeCode(String SizeCode) {
        this.SizeCode = SizeCode;
    }

    /**
     * @return The SizeNameInvariant
     */
    public String getSizeNameInvariant() {
        return SizeNameInvariant;
    }

    /**
     * @param SizeNameInvariant The SizeNameInvariant
     */
    public void setSizeNameInvariant(String SizeNameInvariant) {
        this.SizeNameInvariant = SizeNameInvariant;
    }

    /**
     * @return The SizeGroup
     */
    public String getSizeGroup() {
        return SizeGroup;
    }

    /**
     * @param SizeGroup The SizeGroup
     */
    public void setSizeGroup(String SizeGroup) {
        this.SizeGroup = SizeGroup;
    }

    /**
     * @return The SizeCultureId
     */
    public Integer getSizeCultureId() {
        return SizeCultureId;
    }

    /**
     * @param SizeCultureId The SizeCultureId
     */
    public void setSizeCultureId(Integer SizeCultureId) {
        this.SizeCultureId = SizeCultureId;
    }

    /**
     * @return The CultureCode
     */
    public String getCultureCode() {
        return CultureCode;
    }

    /**
     * @param CultureCode The CultureCode
     */
    public void setCultureCode(String CultureCode) {
        this.CultureCode = CultureCode;
    }

    /**
     * @return The SizeName
     */
    public String getSizeName() {
        return SizeName;
    }

    /**
     * @param SizeName The SizeName
     */
    public void setSizeName(String SizeName) {
        this.SizeName = SizeName;
    }

}
