package com.mm.main.app.manager;

import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.log.Logger;
import com.mm.main.app.schema.Language;
import com.mm.main.app.schema.LanguageList;
import com.mm.main.app.service.ReferenceService;
import com.mm.main.app.utils.ObjectUtil;

import java.util.List;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by henrytung on 3/11/15.
 */
public class LanguageManager {
    private static final String TAG = LanguageManager.class.toString();
    LanguageList languageList = null;
    private static LanguageManager singleton;
    public static Locale locale = Locale.SIMPLIFIED_CHINESE;

    private LanguageManager() {
        languageList = getLanguageList();
        if (languageList != null && languageList.getLanguageList().size() > 0) {
            Logger.i(TAG, "Language list ready");
        } else {
            requestLanguageList();
        }
    }

    public static LanguageManager getInstance() {
        if (singleton == null) {
            singleton = new LanguageManager();
        }
        return singleton;
    }

    public void requestLanguageList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ReferenceService service = retrofit.create(ReferenceService.class);
        Call<LanguageList> call = service.listLanguage();
        call.enqueue(new Callback<LanguageList>() {
            @Override
            public void onResponse(Response<LanguageList> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    languageList = response.body();
                    setLanguageList(languageList);
                } else {
                    Logger.d(TAG, response.toString());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Logger.d(TAG, t.getMessage());
            }
        });
    }

    private void setLanguageList(LanguageList languageList) {
        ObjectUtil.saveComplexObject(MyApplication.getContext(), languageList, Constant.LANGUAGE_LIST);
    }

    public LanguageList getLanguageList() {
        return ObjectUtil.loadComplexObject(MyApplication.getContext(), LanguageList.class, Constant.LANGUAGE_LIST);
    }

    public static void clearLanguageList() {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().remove(Constant.LANGUAGE_LIST).commit();
        singleton = null;
    }

    public String getCurrentCultureCode() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext())
                .getString(Constant.CURRENT_CULTURE_CODE, "CHS");
    }

    public void setCurrentCultureCode(String currentCultureCode) {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit()
                .putString(Constant.CURRENT_CULTURE_CODE, currentCultureCode).commit();
        switch (currentCultureCode) {
            case "EN":
                locale = Locale.ENGLISH;
                break;
            case "CHT":
                locale = Locale.TRADITIONAL_CHINESE;
                break;
            case "CHS":
                locale = Locale.SIMPLIFIED_CHINESE;
                break;

        }

    }

    public void updateCurrentCultureCode(Integer languageId) {
        if (languageList == null || languageList.getLanguageList().size() < 0) {
            return;
        } else {
            List<Language> languages = languageList.getLanguageList();
            for (Language lang : languages) {
                if (lang.getLanguageId().equals(languageId)) {
                    setCurrentCultureCode(lang.getCultureCode());
                    break;
                }
            }
        }
    }

    public void updateDeviceLocale() {
        Resources res = MyApplication.getContext().getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = LanguageManager.locale;
        res.updateConfiguration(conf, dm);
    }
}
