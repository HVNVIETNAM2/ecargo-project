package com.mm.main.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.activity.storefront.im.UserChatActivity;
import com.mm.main.app.activity.storefront.profile.StoreFrontUserProfileActivity;
import com.mm.main.app.activity.storefront.profile.StoreFrontUserProfileDataListActivity;
import com.mm.main.app.adapter.strorefront.friend.AllFriendRvAdapter;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.ProfileLifeCycleManager;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by haivu on 3/4/2016.
 */
public class AllFriendListFragment extends Fragment {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    List<User> friends;
    AllFriendRvAdapter adapter;

    List<User> filteredFriends;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_friend_list, container, false);
        ButterKnife.bind(this, view);

        if (!MmGlobal.getUserKey().equals("")) {
            fetchDataList();
        }
        return view;
    }

    public void searchUser(String s) {
        if (friends != null && friends.size() > 0) {
            filteredFriends.clear();
            if (s.equals("")) {
                filteredFriends.addAll(friends);
            } else {
                for (User item : friends) {
                    if (item.getDisplayName().toLowerCase().contains(s.toString().toLowerCase())) {
                        filteredFriends.add(item);
                    }
                }
            }

            adapter.notifyDataSetChanged();
        }
    }

    public void upDateData() {
        getFriends();
    }


    private void fetchDataList() {
        MmProgressDialog.show(getActivity());
        getFriends();
    }

    private void getFriends() {
        friends = new ArrayList<>();
        filteredFriends = new ArrayList<>();

        APIManager.getInstance().getFriendService().listFriend(MmGlobal.getUserKey(), MmGlobal.getUserKey())
                .enqueue(new MmCallBack<List<User>>(getActivity()) {
                    @Override
                    public void onSuccess(Response<List<User>> response, Retrofit retrofit) {
                        friends = response.body();
                        filteredFriends.addAll(friends);
                        setUpAdapter();
                    }
                });
    }

    private void setUpAdapter() {

        adapter = new AllFriendRvAdapter(getActivity(), filteredFriends, new AllFriendRvAdapter.MyCallBack() {
            @Override
            public void seeProfileUser(int position) {
                //TODO go to profile page
                Intent intent = new Intent(getActivity(), StoreFrontUserProfileActivity.class);
                intent.putExtra(StoreFrontUserProfileActivity.PROFILE_TYPE_KEY, StoreFrontUserProfileActivity.ProfileType.USER_PUBLIC_PROFILE);
                intent.putExtra(StoreFrontUserProfileActivity.PUBLIC_USER_KEY, filteredFriends.get(position).getUserKey());
                // TODO: put the correct friend and follow status when service is ready
                intent.putExtra(StoreFrontUserProfileActivity.BE_FRIEND_KEY, true);
                intent.putExtra(StoreFrontUserProfileActivity.BE_FOLLOWED_KEY, false);
                intent.putExtra(StorefrontMainActivity.TAB_POSITION_KEY, 4);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
                ProfileLifeCycleManager.getInstance().openNewProfile(MmGlobal.getUserKey(), StoreFrontUserProfileDataListActivity.ProfileDataListType.FRIEND_LIST);
            }

            @Override
            public void chatWithUser(int position) {
                Intent i = new Intent(getActivity(), UserChatActivity.class);
                startActivity(i);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

    }
}
