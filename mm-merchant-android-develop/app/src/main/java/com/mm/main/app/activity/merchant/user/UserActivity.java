package com.mm.main.app.activity.merchant.user;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.merchant.setting.ChangeInventoryActivity;
import com.mm.main.app.activity.merchant.setting.SettingActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.blur.BlurBuilder;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.schema.InventoryLocation;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Response;
import retrofit.Retrofit;


public class UserActivity extends ActionBarActivity {

    final private static String TAG = UserActivity.class.toString();
    final public static String EXTRA_USER_DETAIL = "USER_DETAIL";

    final public static int CHANGE_INVENTORY_REQUEST = 2888;

    @Bind(R.id.profile_image)
    CircleImageView profileImage;
    @Bind(R.id.bannerImageView)
    ImageView bannerImageView;
    @Bind(R.id.brandImageView)
    ImageView brandImageView;
    @Bind(R.id.userNameTextView)
    TextView userNameTextView;
    @Bind(R.id.inventory_field)
    TextView inventoryTextView;
    @Bind(R.id.location_id_field)
    TextView locationIdTextView;
    @Bind(R.id.type_field)
    TextView typeTextView;
    @Bind(R.id.country_field)
    TextView countryTextView;
    @Bind(R.id.province_field)
    TextView provinceTextView;
    @Bind(R.id.city_field)
    TextView cityTextView;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeOverlay);
        render();

        LanguageManager.getInstance();
    }

    private void render() {
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        setTitle("");
    }

    public void setTitle(String title) {
        this.getSupportActionBar().setDisplayShowCustomEnabled(true);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);

        View v = LayoutInflater.from(this).inflate(R.layout.common_layout, null);

        ((TextView) v.findViewById(R.id.titleWhite)).setText(title);

        this.getSupportActionBar().setCustomView(v);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingActivity.class);
            intent.putExtra(SettingActivity.EXTRA_USER_DETAIL, user);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.button)
    protected void proceedChangeInventoryLocation() {
        Intent changeInventoryIntent = new Intent(this, ChangeInventoryActivity.class);
        changeInventoryIntent.putExtra(ChangeInventoryActivity.EXTRA_USER_DETAIL, user);
        startActivityForResult(changeInventoryIntent, CHANGE_INVENTORY_REQUEST);

        overridePendingTransition(R.anim.right_to_left, R.anim.not_move);
    }


    private void viewUser(Integer userId) {
        MmProgressDialog.show(this);
        APIManager.getInstance().getUserService().viewUser(userId)
                .enqueue(new MmCallBack<User>(UserActivity.this) {
                    @Override
                    public void onSuccess(Response<User> response, Retrofit retrofit) {
                        user = response.body();
                        LanguageManager.getInstance().updateCurrentCultureCode(user.getLanguageId());
                        renderProfileView();

                        Integer inventoryId = user.getInventoryLocationId();
                        if (inventoryId == 0) {
                            inventoryId = user.getUserInventoryLocationArray()[0];
                        }
                        fetchInventory(inventoryId);
                    }
                });
//        APIManager.getInstance().getUserService().viewUserRx(userId).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
//                .subscribe(new Action1<User>() {
//                    @Override
//                    public void call(User user) {
//                        LanguageManager.getInstance().updateCurrentCultureCode(user.getLanguageId());
//                        renderProfileView();
//
//                        Integer inventoryId = user.getInventoryLocationId();
//                        if (inventoryId == 0) {
//                            inventoryId = user.getUserInventoryLocationArray()[0];
//                        }
//                        fetchInventory(inventoryId);
//                    }
//                });

    }

    private void fetchInventory(Integer inventoryLocationId) {
        MmProgressDialog.show(this);
        APIManager.getInstance().getInventoryService().viewLocation(inventoryLocationId)
                .enqueue(new MmCallBack<InventoryLocation>(UserActivity.this) {

                    @Override
                    public void onSuccess(Response<InventoryLocation> response, Retrofit retrofit) {
                        InventoryLocation inventoryLocation = response.body();
                        renderInventoryView(inventoryLocation);
                    }

                });
    }

    private void renderBanner() {

        Picasso.with(MyApplication.getContext()).load(PathUtil.getProfileImageUrl(user.getMerchant().getBackgroundImage())).into(new com.squareup.picasso.Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                if (bitmap != null) {
                    Bitmap bm = BlurBuilder.blur(MyApplication.getContext(), bitmap);
                    bannerImageView.setImageBitmap(bm);
                    bannerImageView.setColorFilter(Color.rgb(148, 148, 148), android.graphics.PorterDuff.Mode.MULTIPLY);
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

    }

    private void renderProfileView() {
        if (user.getMerchant() != null) {
            renderBanner();
            Picasso.with(MyApplication.getContext()).load(PathUtil.getProfileImageUrl(user.getMerchant().getLogoImage())).into(brandImageView);
            setTitle(user.getMerchant().getMerchantDisplayName());
        }

        Picasso.with(MyApplication.getContext()).load(PathUtil.getProfileImageUrl(user.getProfileImage(), PathUtil.DEFAULT_PROFILE_PIC_SIZE))
                .placeholder(R.drawable.placeholder).into(profileImage);

        userNameTextView.setText(user.getDisplayName());
    }

    private void renderInventoryView(InventoryLocation inventoryLocation) {
        inventoryTextView.setText(inventoryLocation.getLocationName());
        locationIdTextView.setText(inventoryLocation.getLocationExternalCode());
        typeTextView.setText(inventoryLocation.getInventoryLocationTypeName());
        countryTextView.setText(inventoryLocation.getGeoCountryName());
        provinceTextView.setText(inventoryLocation.getGeoProvinceName());
        cityTextView.setText(inventoryLocation.getGeoCityName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        render();
        viewUser(MmGlobal.getUserId());
        Logger.i(TAG, "cc is " + LanguageManager.getInstance().getCurrentCultureCode());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CHANGE_INVENTORY_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Integer result = data.getIntExtra("result", 2);
                user.setInventoryLocationId(result);
                fetchInventory(user.getInventoryLocationId());
                MmProgressDialog.show(this);
                APIManager.getInstance().getUserService().changeInventoryLocation(user)
                        .enqueue(new MmCallBack<Boolean>(UserActivity.this) {

                            @Override
                            public void onSuccess(Response<Boolean> response, Retrofit retrofit) {

                            }
                        });
            } else if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
