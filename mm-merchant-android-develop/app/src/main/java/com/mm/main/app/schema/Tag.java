
package com.mm.main.app.schema;

public class Tag {

    private String TagName;
    private Integer Priority;
    private Integer TagTypeId;
    private String TagTypeName;
    private Integer TagId;
    private String UserTagId;
    private String UserId;

    /**
     * 
     * @return
     *     The TagName
     */
    public String getTagName() {
        return TagName;
    }

    /**
     * 
     * @param TagName
     *     The TagName
     */
    public void setTagName(String TagName) {
        this.TagName = TagName;
    }

    /**
     * 
     * @return
     *     The Priority
     */
    public Integer getPriority() {
        return Priority;
    }

    /**
     * 
     * @param Priority
     *     The Priority
     */
    public void setPriority(Integer Priority) {
        this.Priority = Priority;
    }

    /**
     * 
     * @return
     *     The TagTypeId
     */
    public Integer getTagTypeId() {
        return TagTypeId;
    }

    /**
     * 
     * @param TagTypeId
     *     The TagTypeId
     */
    public void setTagTypeId(Integer TagTypeId) {
        this.TagTypeId = TagTypeId;
    }

    /**
     * 
     * @return
     *     The TagTypeName
     */
    public String getTagTypeName() {
        return TagTypeName;
    }

    /**
     * 
     * @param TagTypeName
     *     The TagTypeName
     */
    public void setTagTypeName(String TagTypeName) {
        this.TagTypeName = TagTypeName;
    }

    public Integer getTagId() {
        return TagId;
    }

    public void setTagId(Integer tagId) {
        TagId = tagId;
    }

    public String getUserTagId() {
        return UserTagId;
    }

    public void setUserTagId(String userTagId) {
        UserTagId = userTagId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }
}

