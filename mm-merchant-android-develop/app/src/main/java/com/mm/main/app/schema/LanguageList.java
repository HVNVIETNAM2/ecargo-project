package com.mm.main.app.schema;

import java.io.Serializable;
import java.util.List;

/**
 * Created by henrytung on 27/10/15.
 */
public class LanguageList implements Serializable{

    List<Language> LanguageList;

    public LanguageList(List<Language> languageList) {
        LanguageList = languageList;
    }

    public LanguageList() {
    }

    public List<Language> getLanguageList() {
        return LanguageList;
    }

    public void setLanguageList(List<Language> languageList) {
        LanguageList = languageList;
    }
}
