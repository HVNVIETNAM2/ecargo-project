package com.mm.main.app.service;

import com.mm.main.app.schema.Badge;
import com.mm.main.app.schema.Brand;
import com.mm.main.app.schema.BrandUnionMerchant;
import com.mm.main.app.schema.Category;
import com.mm.main.app.schema.Color;
import com.mm.main.app.schema.response.CompleteResponse;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.response.SearchResponse;
import com.mm.main.app.schema.Size;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

import java.util.List;

/**
 * Created by andrew on 10/12/2015.
 */
public interface SearchService {
    String SEARCH_PATH =  "search";

    @GET(SEARCH_PATH+"/style")
    Call<SearchResponse> styleList(@Query("q") String q, @Query("pageno") Integer pageno, @Query("pagesize") Integer pagesize,
                                   @Query("s") String queryString,
                                   @Query("pricefrom") Integer pricefrom,
                                   @Query("priceto") Integer priceto,
                                   @Query("brandid") String brandid,
                                   @Query("categoryid") String categoryid,
                                   @Query("colorid") String colorid,
                                   @Query("sizeid") String sizeid,
                                   @Query("badgeid") String badgeid,
                                   @Query("sort") String sort,
                                   @Query("order") String order,
                                   @Query("merchantid") String merchantid,
                                   @Query("issale") Integer issale,
                                   @Query("isnew") Integer isnew

    );

    @GET(SEARCH_PATH + "/style")
    Call<SearchResponse> searchStyleCode(@Query("stylecode") String styleCode);

//    @GET(SEARCH_PATH + "/suggest")
//    Call<List<String>> suggest();

    @GET(SEARCH_PATH + "/complete")
    Call<List<CompleteResponse>> complete(@Query("s") String queryString);

    @GET(SEARCH_PATH + "/brand/combined")
    Call<List<BrandUnionMerchant>> brandCombined();

    @GET(SEARCH_PATH + "/brand")
    Call<List<Brand>> brand();

    @GET(SEARCH_PATH + "/merchant")
    Call<List<Merchant>> merchant();

    @GET(SEARCH_PATH + "/category")
    Call<List<Category>> category();

    @GET(SEARCH_PATH + "/color")
    Call<List<Color>> color();

    @GET(SEARCH_PATH + "/size")
    Call<List<Size>> size();

    @GET(SEARCH_PATH + "/badge")
    Call<List<Badge>> badge();

}
