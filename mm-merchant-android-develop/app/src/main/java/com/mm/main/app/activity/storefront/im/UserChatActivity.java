package com.mm.main.app.activity.storefront.im;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.im.model.Message;
import com.mm.main.app.activity.storefront.im.model.Status;
import com.mm.main.app.activity.storefront.im.model.UserType;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.ImManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.record.ImEvent;
import com.mm.main.app.schema.Conv;
import com.mm.main.app.schema.SocketMessage;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.PhotoUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.Response;
import retrofit.Retrofit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserChatActivity extends AppCompatActivity {
    private static final int CHAT_TIMER = 60;
    private static String TAG = "ChatDemoMain";

    private ArrayList<Message> messages;
    private ChatRvAdapter adapter;

    private static final int CAMERA_REQUEST_TO_CHAT = 1111;
    private static final int PHOTO_REQUEST_TO_CHAT = 2222;
    private static final int CONTACT_REQUEST_TO_CHAT = 2221;

    private Uri currentPictureUri = null;
    CountDownTimer countDownTimer = null;

    private boolean isSendMess;

    @Bind((R.id.chat_list_view))
    RecyclerView chatListView;

    @Bind(R.id.mediaChatLayout)
    RelativeLayout mediaChatLayout;

    @Bind(R.id.chatContent)
    RelativeLayout chatContent;

    @Bind(R.id.contentChatVoice)
    RelativeLayout contentChatVoice;

    @Bind(R.id.imgChatAttachment)
    ImageView imgChatAttachment;

    @Bind(R.id.chat_edit_text)
    EditText chatEditText;

    @Bind(R.id.progressbarTimer)
    ProgressBar progressbarTimer;

    @Bind(R.id.tvTimer)
    TextView tvTimer;

    @Bind(R.id.imgRecord)
    ImageView imgRecord;

    boolean isShowChatAttachment = false;
    private User selfChatUser;
    private User otherChatUser;
    private Conv conv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        conv = (Conv) getIntent().getExtras().get("ConversationObject");
        Log.i(TAG, conv.getConvKey());
        this.setTheme(R.style.AppTheme_NoActionBar);
        setContentView(R.layout.activity_chat_demo_main);
        ButterKnife.bind(this);
        setUpView();
        messages = new ArrayList<>();
        initChatUsers();
        setUpAdapter();
    }

    private void setUpAdapter() {
        adapter = new ChatRvAdapter(this, messages, null);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        chatListView.setLayoutManager(linearLayoutManager);
        chatListView.setHasFixedSize(true);
        chatListView.setAdapter(adapter);
        final View chatView = findViewById(R.id.chat_layout);
        chatView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = chatView.getRootView().getHeight() - chatView.getHeight();
                if (heightDiff > 100 && adapter.getItemCount() > 0) {
                    chatListView.smoothScrollToPosition(adapter.getItemCount() - 1);
                }
            }
        });
    }

    private void initChatUsers(){
        APIManager.getInstance().getUserService().viewUser(MmGlobal.getUserId())
                .enqueue(new MmCallBack<User>(this) {
                    @Override
                    public void onSuccess(Response<User> response, Retrofit retrofit) {
                        selfChatUser = response.body();
                    }
                });
    }

    private void sendMessage(final String messageText) {
        if (imgChatAttachment.getVisibility() == View.VISIBLE) {
            try {
                final Message message = new Message();
                message.setMessageStatus(Status.SENT);
                message.setMessageText(messageText);
                message.setUser(selfChatUser);
                UserType type = UserType.SELF_CHAT_TEXT;
                message.setUserType(type);
                message.setMessageTime(new Date().getTime());
                messages.add(message);

                ImManager.getInstance().sendMessage(messageText, conv.getConvKey());

            } catch (Exception e) {
                e.printStackTrace();
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    private void sendMessagePicture(Bitmap bitmap){
        try {
            final Message message = new Message();
            message.setMessageStatus(Status.SENT);
            message.setUser(selfChatUser);
            UserType type = UserType.SELF_CHAT_IMAGE;
            message.setUserType(type);
            message.setImageMessageBody(bitmap);
            message.setMessageTime(new Date().getTime());
            messages.add(message);

        } catch (Exception e) {
            e.printStackTrace();
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(ImEvent event) {
        switch (event.getMyActionType()) {
            case SEND:
                break;
            case RECEIVE:
                try {
                    Logger.i(TAG, "Nhut =" + event.getMessage());
                    final Message message = new Message();
                    message.setMessageStatus(Status.SENT);
                    message.setMessageText(event.getMessage());
                    message.setUser(otherChatUser);
                    UserType type = UserType.OTHER_CHAT_TEXT;

                    message.setUserType(type);
                    message.setMessageTime(new Date().getTime());
                    messages.add(message);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        imgChatAttachment.setVisibility(View.VISIBLE);
                    }
                });
                break;
            case PING:
                break;
            case PONG:
                break;
            case ENABLE_SEND:
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        imgChatAttachment.setVisibility(View.VISIBLE);
                    }
                });
                break;
            case DISABLE_SEND:
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        imgChatAttachment.setVisibility(View.INVISIBLE);
                    }
                });
                break;
        }
    }

    //Handle set up view
    private void setUpView() {

        progressbarTimer.setMax(CHAT_TIMER);
        progressbarTimer.setProgress(0);

        chatEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    isSendMess = false;
                    imgChatAttachment.setImageResource(R.drawable.chat_attachment);

                    if (isShowChatAttachment) {
                        imgChatAttachment.setRotation(45);
                    } else {
                        imgChatAttachment.setRotation(0);
                    }

                } else {
                    isSendMess = true;
                    imgChatAttachment.setImageResource(R.drawable.ic_chat_send_active);
                    imgChatAttachment.setRotation(0);
                    mediaChatLayout.setVisibility(View.GONE);
                    isShowChatAttachment = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        imgRecord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //TODO: start record

                        countDownTimer = new CountDownTimer((60 + 1) * 1000, 1 * 1000) {

                            @Override
                            public void onTick(long millisUntilFinished) {
                                long percent = 60 - (millisUntilFinished / 1000 - 1);
                                String percentStr;

                                if (percent < 10) {
                                    percentStr = "0" + percent + "\"";
                                } else {
                                    percentStr = percent + "\"";
                                }
                                tvTimer.setText(percentStr);
                                progressbarTimer.setProgress((int) percent);
                            }

                            @Override
                            public void onFinish() {

                                //TODO: send mess.

                                //TODO: update view
                                tvTimer.setText("");
                                progressbarTimer.setProgress(0);
                                contentChatVoice.setVisibility(View.GONE);
                                chatContent.setVisibility(View.VISIBLE);
                                countDownTimer.cancel();
                            }
                        };
                        countDownTimer.start();

                        return true;

                    case MotionEvent.ACTION_UP:
                        //TODO : end record and send
                        if (countDownTimer != null) {
                            countDownTimer.onFinish();
                        }
                        break;

                    case MotionEvent.ACTION_MOVE:
                        //TODO cancel record
                        break;
                }
                return false;
            }
        });

        chatListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ActivityUtil.closeKeyboard(UserChatActivity.this);
                Logger.d("CHAT" , "Touch");
                return false;
            }
        });
    }

    //Handle activity state.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PHOTO_REQUEST_TO_CHAT:
                case CAMERA_REQUEST_TO_CHAT:
                    if (data != null && data.getData() != null) {
                        currentPictureUri = data.getData();

                        Bitmap photo = null;
                        try {
                            photo = PhotoUtil.getCorrectlyOrientedImage(this, currentPictureUri);
                        } catch (IOException e) {
                            Logger.d(TAG, e.getMessage());
                        }

                        if (photo != null) {
                            sendMessagePicture(photo);
                        }
                    }

                    break;

                case CONTACT_REQUEST_TO_CHAT:
                    //TODO send contact.
                    break;

            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isShowChatAttachment) {
            mediaChatLayout.setVisibility(View.GONE);
            isShowChatAttachment = false;
            imgChatAttachment.setRotation(0);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    //Handle for attachment
    @OnClick(R.id.imgChatAttachment)
    public void onClickChatAttachment() {

        if (isSendMess) {
            sendMessage(chatEditText.getText().toString());
            chatEditText.setText("");
        } else {

            if (isShowChatAttachment) {
                //TODO Hide
                mediaChatLayout.setVisibility(View.GONE);
                imgChatAttachment.setRotation(0);

            } else {
                mediaChatLayout.setVisibility(View.VISIBLE);
                imgChatAttachment.setRotation(45);

                //slideToBottom(mediaChatLayout);

            }

            isShowChatAttachment = !isShowChatAttachment;
        }
    }

    //Open camera.
    @OnClick(R.id.imgCameraChat)
    public void onCLickImgCameraChat() {
        startCamera(CAMERA_REQUEST_TO_CHAT);
    }

    //Open photo album
    @OnClick(R.id.imgChatGallery)
    public void onClickImgChatGallery() {
        pickImage(PHOTO_REQUEST_TO_CHAT);
    }

    @OnClick(R.id.imgChatContact)
    public void onClickImgChatContact() {
        //TODO call list friend
    }

    private void startCamera(int request) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 0);

        if (Build.MODEL.toUpperCase().contains("NEXUS")) {
            String imageFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg";
            File imageFile = new File(imageFilePath);
            try {
                if (!imageFile.exists()) {
                    imageFile.getParentFile().mkdirs();
                    imageFile.createNewFile();
                }
            } catch (IOException e) {
            }
            currentPictureUri = Uri.fromFile(imageFile);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentPictureUri);
        }

        startActivityForResult(cameraIntent, request);
    }

    private void pickImage(int REQUEST_CODE) {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.LB_PHOTO_LIBRARY)), REQUEST_CODE);
    }


    //Handel for record audio.
    @OnClick(R.id.imgChatRecord)
    public void onClickImgChatRecord() {
        chatContent.setVisibility(View.GONE);
        mediaChatLayout.setVisibility(View.GONE);
        contentChatVoice.setVisibility(View.VISIBLE);
    }




}
