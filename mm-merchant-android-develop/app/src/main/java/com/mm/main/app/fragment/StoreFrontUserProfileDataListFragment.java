package com.mm.main.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.activity.storefront.merchant.StoreFrontMerchantProfileActivity;
import com.mm.main.app.activity.storefront.profile.StoreFrontUserProfileActivity;
import com.mm.main.app.adapter.strorefront.filter.UserFilterSelectionListAdapter;
import com.mm.main.app.adapter.strorefront.profile.FollowerListAdapter;
import com.mm.main.app.activity.storefront.profile.StoreFrontUserProfileDataListActivity;
import com.mm.main.app.adapter.strorefront.profile.FollowingMerchantListAdapter;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.ProfileLifeCycleManager;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.MmCallBack;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Response;
import retrofit.Retrofit;

public class StoreFrontUserProfileDataListFragment extends Fragment {
    @Bind(R.id.listView)
    ListView listView;

    private FollowingMerchantListAdapter followingMerchantListAdapter;
    private StoreFrontUserProfileDataListActivity.ProfileDataListType listType;
    private String userKey;


    public void setListType(StoreFrontUserProfileDataListActivity.ProfileDataListType listType, String userKey) {
        this.listType = listType;
        this.userKey = userKey;
        onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            listType = (StoreFrontUserProfileDataListActivity.ProfileDataListType) getArguments().getSerializable(StoreFrontUserProfileDataListActivity.LIST_TYPE_KEY);
            userKey = getArguments().getString(StoreFrontUserProfileDataListActivity.USER_KEY, "");
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        switch (listType) {
            case MERCHANT_LIST:
                requestFollowingMerchants();
                break;
            case CURATOR_LIST:
                requestFollowingCurator();
                break;
            case USER_LIST:
                requestFollowingUser();
                break;
            default:
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_store_front_user_profile_data_list, container, false);
        ButterKnife.bind(this, rootView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (listType) {
                    case MERCHANT_LIST:
                        seeMerchant(position);
                        break;
                    case CURATOR_LIST:
                        seeProfile(position, StoreFrontUserProfileDataListActivity.ProfileDataListType.CURATOR_LIST);
                        break;
                    case USER_LIST:
                        seeProfile(position, StoreFrontUserProfileDataListActivity.ProfileDataListType.USER_LIST);
                        break;
                    default:
                        break;
                }
            }
        });

        return rootView;
    }

    void seeMerchant(int position) {
        Intent intent = new Intent(getContext(), StoreFrontMerchantProfileActivity.class);
        Bundle bundle = new Bundle();
        FilterListItem<Merchant> item = (FilterListItem<Merchant>) followingMerchantListAdapter.getItem(position);
        bundle.putInt(StoreFrontMerchantProfileActivity.FOLLOWER_COUNT_KEY, item.getT().getFollowerCount());
        bundle.putInt(StoreFrontMerchantProfileActivity.MERCHANT_ID_KEY, item.getT().getMerchantId());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    void seeProfile(int position, StoreFrontUserProfileDataListActivity.ProfileDataListType dataListType) {
        UserFilterSelectionListAdapter adapter = (UserFilterSelectionListAdapter) listView.getAdapter();
        User user = ((FilterListItem<User>) adapter.getItem(position)).getT();

        Intent intent = new Intent(getContext(), StoreFrontUserProfileActivity.class);
        intent.putExtra(StoreFrontUserProfileActivity.PROFILE_TYPE_KEY, StoreFrontUserProfileActivity.ProfileType.USER_PUBLIC_PROFILE);
        intent.putExtra(StoreFrontUserProfileActivity.PUBLIC_USER_KEY, user.getUserKey());
        // TODO: put the correct friend and follow status when service is ready
        intent.putExtra(StoreFrontUserProfileActivity.BE_FRIEND_KEY, true);
        intent.putExtra(StoreFrontUserProfileActivity.BE_FOLLOWED_KEY, false);
        intent.putExtra(StorefrontMainActivity.TAB_POSITION_KEY, 4);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();

        ProfileLifeCycleManager.getInstance().openNewProfile(userKey, dataListType);
    }

    private void requestFollowingMerchants() {
        if (!userKey.equals("")) {
            APIManager.getInstance().getFollowService().viewMerchantFollowed(userKey, 0, 99)
                    .enqueue(new MmCallBack<List<Merchant>>(getContext()) {
                        @Override
                        public void onSuccess(Response<List<Merchant>> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                List<Merchant> list = response.body();
                                ArrayList<FilterListItem<Merchant>> brandArrayList = new ArrayList<>();
                                for (int i = 0; i < list.size(); i++) {
                                    brandArrayList.add(new FilterListItem<>(list.get(i)));
                                }
                                followingMerchantListAdapter = new FollowingMerchantListAdapter(getActivity(), brandArrayList);
                                listView.setAdapter(followingMerchantListAdapter);
                            }
                        }
                    });
        }
    }


    private void requestFollowingCurator() {
        if (!userKey.equals("")) {
            APIManager.getInstance().getFollowService().viewFollowingCurator(userKey).enqueue(new MmCallBack<List<User>>(getContext()) {
                @Override
                public void onSuccess(Response<List<User>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        List<User> list = response.body();
                        ArrayList<FilterListItem<User>> listUser = new ArrayList<>();
                        for (int i = 0; i < list.size(); i++) {
                            listUser.add(new FilterListItem<>(list.get(i)));
                        }
                        FollowerListAdapter followerListAdapter = new FollowerListAdapter(getActivity(), listUser);
                        listView.setAdapter(followerListAdapter);
                    }
                }
            });
        }
    }

    private void requestFollowingUser() {
        if (!userKey.equals("")) {
            APIManager.getInstance().getFollowService().viewFollowingUser(userKey).enqueue(new MmCallBack<List<User>>(getContext()) {
                @Override
                public void onSuccess(Response<List<User>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        List<User> list = response.body();
                        ArrayList<FilterListItem<User>> listUser = new ArrayList<>();
                        for (int i = 0; i < list.size(); i++) {
                            listUser.add(new FilterListItem<>(list.get(i)));
                        }
                        FollowerListAdapter followerListAdapter = new FollowerListAdapter(getActivity(), listUser);
                        listView.setAdapter(followerListAdapter);
                    }
                }
            });
        }
    }

    public void filterList(CharSequence s) {

        if (listView.getAdapter() != null) {
            if (listView.getAdapter() instanceof FollowingMerchantListAdapter) {
                FollowingMerchantListAdapter followingMerchantListAdapter = (FollowingMerchantListAdapter) listView.getAdapter();
                followingMerchantListAdapter.getFilter().filter(s);
            } else if (listView.getAdapter() instanceof FollowerListAdapter) {
                FollowerListAdapter followerListAdapter = (FollowerListAdapter) listView.getAdapter();
                followerListAdapter.getFilter().filter(s);
            }
        }

    }

    public void clearFilter() {
        listView.clearTextFilter();
    }
}
