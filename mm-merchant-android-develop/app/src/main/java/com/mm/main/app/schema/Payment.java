package com.mm.main.app.schema;

import java.io.Serializable;

/**
 * Created by thienchaud on 26-Feb-16.
 */
public class Payment implements Serializable {

    String paymentName;
    String paymentIcon;

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    public String getPaymentIcon() {
        return paymentIcon;
    }

    public void setPaymentIcon(String paymentIcon) {
        this.paymentIcon = paymentIcon;
    }
}
