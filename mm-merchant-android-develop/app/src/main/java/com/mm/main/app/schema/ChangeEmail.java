package com.mm.main.app.schema;

//import com.activeandroid.Model;
//import com.activeandroid.annotation.Column;
//import com.activeandroid.annotation.Table;

/**
 * Schema class for token
 */
//@Table(name = "ForgotPasswordRequest")
//public class ChangeEmail extends Model {
public class ChangeEmail {
//    @Column(name = "UserId", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String UserId;

//    @Column(name = "ForgotPasswordRequest")
    private String Email;

    public ChangeEmail(String userId, String email) {
        UserId = userId;
        Email = email;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
