package com.mm.main.app.activity.storefront.checkout;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.checkout.PaymentSelectionRVAdapter;
import com.mm.main.app.listitem.PaymentRvItem;
import com.mm.main.app.schema.Payment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentSelectionActivity extends AppCompatActivity implements PaymentSelectionRVAdapter.OnSelectPaymentListener {


    @Bind(R.id.recyclerPayment)
    RecyclerView recyclerPayment;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    PaymentSelectionRVAdapter paymentSelectionRVAdapter;
    List<PaymentRvItem> paymentRvItems;
    Payment selectedPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_selection);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        recyclerPayment.setHasFixedSize(true);
        recyclerPayment.setLayoutManager(new LinearLayoutManager(this));

        initTempData();
        paymentSelectionRVAdapter = new PaymentSelectionRVAdapter(this, paymentRvItems);
        paymentSelectionRVAdapter.setOnSelectPaymentListener(this);
        recyclerPayment.setAdapter(paymentSelectionRVAdapter);
    }

    // remove later
    void initTempData() {
        paymentRvItems = new ArrayList<PaymentRvItem>();
        for (int i = 0; i < 2; i++) {
            Payment payment = new Payment();
            switch (i) {
                case 0:
                    payment.setPaymentName("支付宝");
                    break;
                case 1:
                    payment.setPaymentName("货到付款");
                    break;
            }
            PaymentRvItem item = new PaymentRvItem(payment, false);
            paymentRvItems.add(item);
        }
    }

    @OnClick(R.id.btnConfirm)
    public void confirm() {

    }

    @Override
    public void onSelectPayment(Payment payment) {
        selectedPayment = payment;
    }
}
