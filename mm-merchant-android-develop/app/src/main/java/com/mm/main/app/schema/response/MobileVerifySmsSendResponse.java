package com.mm.main.app.schema.response;

/**
 * Created by henrytung on 2/2/2016.
 */
public class MobileVerifySmsSendResponse {

    String MobileVerificationId;
    String MobileVerificationToken;

    public String getMobileVerificationToken() {
        return MobileVerificationToken;
    }

    public void setMobileVerificationToken(String mobileVerificationToken) {
        MobileVerificationToken = mobileVerificationToken;
    }

    public String getMobileVerificationId() {
        return MobileVerificationId;
    }

    public void setMobileVerificationId(String mobileVerificationId) {
        MobileVerificationId = mobileVerificationId;
    }
}
