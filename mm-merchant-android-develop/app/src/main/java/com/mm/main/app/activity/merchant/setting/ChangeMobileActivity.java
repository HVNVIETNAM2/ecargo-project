package com.mm.main.app.activity.merchant.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.MobileCode;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;


public class ChangeMobileActivity extends ActionBarActivity {

    final public static String EXTRA_USER_DETAIL = "USER_DETAIL";

    final public static int CHANGE_COUNTRY_CODE_REQUEST = 1888;
    @Bind(R.id.currentMobileLabel)
    TextView currentMobileLabel;
    @Bind(R.id.areaCodeEditText)
    EditText areaCodeEditText;
    @Bind(R.id.numberEditText)
    EditText numberEditText;
    @Bind(R.id.change_mobile_country)
    View changeMobileCountryView;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        render();
    }

    private void render() {
        setContentView(R.layout.activity_change_mobile);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, getResources().getString(R.string.LB_MOBILE));
        user = (User) this.getIntent().getSerializableExtra(EXTRA_USER_DETAIL);
        renderCountryName(getResources().getString(R.string.LB_COUNTRY_PICK));
        currentMobileLabel.getText().toString().replace("<current mobile number>", "");
        currentMobileLabel.setText(currentMobileLabel.getText().toString() + user.getMobileCode() + " " + user.getMobileNumber());
        numberEditText.setHint(R.string.LB_NEW_MOBILE);
    }

    private void renderCountryName(String title) {
        TextView titleTextView = (TextView) (changeMobileCountryView.findViewById(R.id.setting_title));
        TextView contentTextView = (TextView) (changeMobileCountryView.findViewById(R.id.setting_content));

        titleTextView.setText(title);
        contentTextView.setText("");
    }

    @OnClick(R.id.change_mobile_country)
    public void onClicked() {
        Intent changeMobileCountryIntent = new Intent(this, ChangeMobileCountryActivity.class);
        changeMobileCountryIntent.putExtra(ChangeMobileCountryActivity.EXTRA_USER_DETAIL, user);
        startActivityForResult(changeMobileCountryIntent, CHANGE_COUNTRY_CODE_REQUEST);
    }

    @OnClick(R.id.confirm_button)
    public void changeMobile() {

        user.setMobileCode(areaCodeEditText.getText().toString());
        user.setMobileNumber(numberEditText.getText().toString());

        if (validate(user.getMobileCode(), user.getMobileNumber())) {
            MmProgressDialog.show(this);
            APIManager.getInstance().getUserService().changeMobile(user).
                    enqueue(new MmCallBack<Boolean>(ChangeMobileActivity.this) {
                        @Override
                        public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                            MmGlobal.setUserName(user.getMobileCode() + user.getMobileNumber());
                            Intent intent = new Intent(ChangeMobileActivity.this, ChangeMobileReactivateActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    });
        }
    }

    @OnClick(R.id.cancel_button)
    public void cancelChange() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == CHANGE_COUNTRY_CODE_REQUEST) {
            MobileCode mobilecode = (MobileCode) data.getExtras().get("result");
            renderCountryName(mobilecode.getMobileCodeName());
            areaCodeEditText.setText(mobilecode.getMobileCodeNameInvariant());
        }
    }

    private boolean validate(String areaCode, String number) {
        if (ValidationUtil.isEmpty(number)) {
            ErrorUtil.showErrorDialog(this, null, getResources().getString(R.string.MSG_ERR_MOBILE_NIL));
            return false;
        }

        if (!ValidationUtil.isValidPhone(areaCode + number)) {
            ErrorUtil.showErrorDialog(this, null, getResources().getString(R.string.MSG_ERR_MOBILE_PATTERN));
            return false;
        }

        return true;
    }


}
