package com.mm.main.app.adapter.strorefront.product;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mm.main.app.R;

import java.util.List;

/**
 * Created by henrytung on 4/11/15.
 */
public class ProductSortingListAdapter extends BaseAdapter {

    private final Activity context;
    private List<String> originalData;
    private List<String> filteredData;
    private Integer selectedItemId;
    private Drawable rightIcon;


    public ProductSortingListAdapter(Activity context, List<String> itemList, Integer selectedItemId) {
        this.context = context;
        this.originalData = itemList;
        this.filteredData = itemList;
        this.selectedItemId = selectedItemId;
        rightIcon = context.getResources().getDrawable(R.drawable.icon_tick);
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String currentItem = filteredData.get(position);
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.sorting_option_selection_list_item, null, true);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.label);
        textView.setText(currentItem);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);

        ListView lv = (ListView) parent;
        if (selectedItemId == position) {
            imageView.setImageDrawable(rightIcon);
        } else {
            imageView.setImageDrawable(null);
        }

        return convertView;
    }

    public Integer getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(Integer selectedItemId) {
        this.selectedItemId = selectedItemId;
    }

}
