package com.mm.main.app.activity.storefront.temp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.base.BaseRvAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.BaseRvItem;
import com.mm.main.app.schema.ColorImage;
import com.mm.main.app.schema.Style;

public class TestActivity extends AppCompatActivity {

    public static final String EXTRA_PRODUCT_DATA = "EXTRA_PRODUCT_DATA";
    private Style style;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        setupRV();
    }

    private void setupRV() {
        style = (Style)getIntent().getSerializableExtra(EXTRA_PRODUCT_DATA);
        setupDescriptionImageItemView();
    }

    private void setupDescriptionImageItemView() {
        // 1. get a reference to recyclerView
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvDescriptionImage);

        BaseRvItem itemsData[] = new BaseRvItem[style.getColorImageList().size()];

        for (int i = 0; i < style.getColorImageList().size(); i++) {
            ColorImage imageData = style.getColorImageList().get(i);
            itemsData[i] = new BaseRvItem(imageData.getColorKey(), imageData.getImageKey());

        }

        recyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));
        BaseRvAdapter mAdapter = new BaseRvAdapter(itemsData);
        recyclerView.setAdapter(mAdapter);

    }
}
