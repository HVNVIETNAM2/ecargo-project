package com.mm.main.app.record;

import com.mm.main.app.schema.Badge;
import com.mm.main.app.schema.Brand;
import com.mm.main.app.schema.Category;
import com.mm.main.app.schema.Color;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.Size;
import com.mm.main.app.utils.ObjectUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by henrytung on 15/12/2015.
 */
public class SearchStyle implements Serializable{

    private static SearchStyle mySearchStyle = null;

    private static SearchStyle mySearchStyleTmp = null;

    public static SearchStyle getInstance() {
        if (mySearchStyle == null) {
            mySearchStyle = new SearchStyle();
        }

        return mySearchStyle;
    }

    public static void applySearchInstance() {

        mySearchStyle = (SearchStyle) ObjectUtil.cloneObject(mySearchStyleTmp);
    }

    public static void cloneInstance() {
        mySearchStyleTmp = new SearchStyle();
        mySearchStyleTmp = (SearchStyle) ObjectUtil.cloneObject(mySearchStyle);
    }



    public static SearchStyle getClonedInstance() {
        if (mySearchStyleTmp == null) {
            mySearchStyleTmp = new SearchStyle();
        }
        return mySearchStyleTmp;
    }

    public static SearchStyle resetClonedInstance() {

        mySearchStyleTmp = new SearchStyle();

        return mySearchStyleTmp;
    }

    public static enum Order{
        asc,
        desc
    }

    public static enum Sort{
        PriceRetail
    }

    private SearchStyle(){}

    String queryString;
    Integer pricefrom = 0;
    Integer priceTo = null;
    List<Brand> brandid;
    List<Category> categoryid;
    List<Category> subCategoryid;
    List<Color> colorid;
    List<Size> sizeid;
    List<Badge> badgeid;
    List<Merchant> merchantid;
    Sort sort;
    Order order = Order.asc;
    Integer issale = null;
    Integer isnew = null;

    public int getTotalFilterNumber() {
        int amount = 0;
//        if (!TextUtils.isEmpty(queryString)) {
//            amount++;
//        }

        if (pricefrom != 0 || priceTo != null) {
            amount++;
        }

        if (issale != null) {
            amount++;
        }

        if (isnew != null) {
            amount++;
        }

        int otherAmount = getBrandFilterNumber()+getCategoryFilterNumber()+getColorFilterNumber()
                +getSizeFilterNumber()+getBadgeFilterNumber()+getMerchantFilterNumber();

        amount += otherAmount;

        return amount;
    }

    public int getBrandFilterNumber() {
        if (brandid != null) {
            return brandid.size();
        } else {
            return 0;
        }
    }

    private int getCategoryFilterNumber() {
        if (categoryid != null) {
            return categoryid.size();
        } else {
            return 0;
        }
    }

    private int getColorFilterNumber() {
        if (colorid!= null) {
            return colorid.size();
        } else {
            return 0;
        }
    }

    private int getSizeFilterNumber() {
        if (sizeid != null) {
            return sizeid.size();
        } else {
            return 0;
        }
    }

    private int getBadgeFilterNumber() {
        if (badgeid != null) {
            return badgeid.size();
        } else {
            return 0;
        }
    }

    private int getMerchantFilterNumber() {
        if (merchantid != null) {
            return merchantid.size();
        } else {
            return 0;
        }
    }

    public void clearAllFilter() {
        queryString = null;
        pricefrom = 0;
        priceTo = null;
        brandid = null;
        categoryid = null;
        colorid = null;
        sizeid = null;
        badgeid = null;
        subCategoryid = null;
        merchantid = null;
        issale = null;
        isnew = null;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public Integer getPricefrom() {
        return pricefrom;
    }

    public void setPricefrom(Integer pricefrom) {
        this.pricefrom = pricefrom;
    }

    public Integer getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(Integer priceTo) {
        this.priceTo = priceTo;
    }

    public List<Brand> getBrandid() {
        return brandid;
    }

    public void setBrandid(List<Brand> brandid) {
        this.brandid = brandid;
    }

    public List<Category> getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(List<Category> categoryid) {
        this.categoryid = categoryid;
    }

    public List<Category> getSubCategoryid() {
        return subCategoryid;
    }

    public void setSubCategoryid(List<Category> subCategoryid) {
        this.subCategoryid = subCategoryid;
    }

    public List<Color> getColorid() {
        return colorid;
    }

    public void setColorid(List<Color> colorid) {
        this.colorid = colorid;
    }

    public List<Size> getSizeid() {
        return sizeid;
    }

    public void setSizeid(List<Size> sizeid) {
        this.sizeid = sizeid;
    }

    public List<Badge> getBadgeid() {
        return badgeid;
    }

    public void setBadgeid(List<Badge> badgeid) {
        this.badgeid = badgeid;
    }

    public void setMerchantid(List<Merchant> merchantid) {
        this.merchantid = merchantid;
    }

    public List<Merchant> getMerchantid() {
        return merchantid;
    }

    public String getSort() {
        if (sort != null) {
           return sort.toString();
        }
        return null;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public String getOrder() {
        if (order != null) {
            return order.toString();
        }
        return null;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Integer getIssale() {
        return issale;
    }

    public void setIssale(Integer issale) {
        this.issale = issale;
    }

    public Integer getIsnew() {
        return isnew;
    }

    public void setIsnew(Integer isnew) {
        this.isnew = isnew;
    }
}
