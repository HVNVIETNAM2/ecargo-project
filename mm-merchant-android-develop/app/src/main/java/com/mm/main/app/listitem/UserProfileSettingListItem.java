package com.mm.main.app.listitem;

import com.mm.main.app.schema.User;

/**
 * Setting schema
 */
public class UserProfileSettingListItem {
    String title;
    String content;
    ItemType type;
    User user;

    public enum ItemType {
        TYPE_USER_INFO,
        TYPE_TEXT_ARROW,
        TYPE_DIVIDER
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public UserProfileSettingListItem(String title, String content, ItemType type) {

        this.title = title;
        this.content = content;
        this.type = type;
    }

    public UserProfileSettingListItem(String title, User user, ItemType type) {

        this.title = title;
        this.user = user;
        this.type = type;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
