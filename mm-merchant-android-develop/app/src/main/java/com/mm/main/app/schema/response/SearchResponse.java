
package com.mm.main.app.schema.response;

import com.mm.main.app.schema.Aggregations;
import com.mm.main.app.schema.Style;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchResponse implements Serializable {

    private Integer Hits;
    private Integer HitsTotal;
    private Integer PageCurrent;
    private List<Style> PageData = new ArrayList<>();
    private com.mm.main.app.schema.Aggregations Aggregations;

    /**
     * 
     * @return
     *     The Hits
     */
    public Integer getHits() {
        return Hits;
    }

    /**
     * 
     * @param Hits
     *     The Hits
     */
    public void setHits(Integer Hits) {
        this.Hits = Hits;
    }

    /**
     * 
     * @return
     *     The HitsTotal
     */
    public Integer getHitsTotal() {
        return HitsTotal;
    }

    /**
     * 
     * @param HitsTotal
     *     The HitsTotal
     */
    public void setHitsTotal(Integer HitsTotal) {
        this.HitsTotal = HitsTotal;
    }

    /**
     * 
     * @return
     *     The PageCurrent
     */
    public Integer getPageCurrent() {
        return PageCurrent;
    }

    /**
     * 
     * @param PageCurrent
     *     The PageCurrent
     */
    public void setPageCurrent(Integer PageCurrent) {
        this.PageCurrent = PageCurrent;
    }

    public List<Style> getPageData() {
        return PageData;
    }

    public void setPageData(List<Style> pageData) {
        PageData = pageData;
    }

    public Aggregations getAggregations() {
        return Aggregations;
    }

    public void setAggregations(Aggregations aggregations) {
        this.Aggregations = aggregations;
    }
}
