package com.mm.main.app.utils;

import android.app.Activity;
import android.content.Context;

import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.LifeCycleManager;
import com.mm.main.app.view.MmProgressDialog;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by andrew on 6/11/15.
 */
public abstract class MmCallBack<T> implements Callback<T> {

    public static final Integer ERROR_520 = 520;
    public static final Integer ERROR_500 = 500;

    /**
     * Successful HTTP response.
     *
     * @param response
     * @param retrofit
     */
    Context context;
    boolean dismissProgressOnResponse = true;
    Integer notHandlerErrorCode = null;

    public MmCallBack(Context context) {
        this.context = context;
    }

    public MmCallBack(Context context, boolean dismissProgressOnResponse) {
        this.context = context;
        this.dismissProgressOnResponse = dismissProgressOnResponse;
    }

    public MmCallBack(Context context, int notHandlerErrorCode) {
        this.context = context;
        this.notHandlerErrorCode = notHandlerErrorCode;
    }

    @Override
    public void onResponse(Response<T> response, Retrofit retrofit) {
        if(dismissProgressOnResponse){
            MmProgressDialog.dismiss();
        }

        if (response.isSuccess()) {
            onSuccess(response, retrofit);
        } else {
            // dismiss in case callback not set to auto dismiss progress and failed
            if(!dismissProgressOnResponse){
                MmProgressDialog.dismiss();
            }

            if(notHandlerErrorCode != null && notHandlerErrorCode.equals(response.code())) {
                onFailWithErrorCode(response, retrofit);
            }
            else {
                ErrorUtil.alert(response, context);
            }

            if (response.code() == 401) {
                LifeCycleManager.relogin((Activity) context);
            }
        }
    }

    abstract public void onSuccess(Response<T> response, Retrofit retrofit);
    public void onFailWithErrorCode(Response<T> response, Retrofit retrofit){

    }

    /**
     * Invoked when a network or unexpected exception occurred during the HTTP request.
     *
     * @param t
     */
    @Override
    public void onFailure(Throwable t) {
        MmProgressDialog.dismiss();
        Logger.i("Error", t.toString());
        ErrorUtil.alert(context);
    }
}
