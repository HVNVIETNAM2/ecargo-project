package com.mm.main.app.adapter.merchant.setting;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.schema.Language;

import java.util.List;

/**
 * Created by henrytung on 4/11/15.
 */
public class LanguageSelectListAdapter extends BaseAdapter {

    private final Activity context;
    private final List<Language> itemList;
    private Integer selectedItemId;
    private Drawable rightIcon;

    public LanguageSelectListAdapter(Activity context, List<Language> itemList, Integer selectedItemId) {
        this.context = context;
        this.itemList = itemList;
        this.selectedItemId = selectedItemId;
        rightIcon = context.getResources().getDrawable(R.drawable.ico_tick);
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Language language = itemList.get(position);
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.simple_selection_list_item, null, true);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.label);
        textView.setText(language.getLanguageName());

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        if (language.getLanguageId().equals(selectedItemId)) {
            imageView.setImageDrawable(rightIcon);
        } else {
            imageView.setImageDrawable(null);
        }

        return convertView;
    }

    public Integer getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(Integer selectedItemId) {
        this.selectedItemId = selectedItemId;
    }
}
