package com.mm.main.app.utils;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.main.app.R;

/**
 * Created by yendangn on 1/25/2016.
 */
public class UiComponentUtil {


    public static void changeViewBuyButtonEndScroll(TextView tvPriceAfterSwipe, TextView tvSweepAfterSwipe,
                                                    ImageView handle, ImageView content, LinearLayout imageViewCart) {
        tvPriceAfterSwipe.setVisibility(View.INVISIBLE);
        tvSweepAfterSwipe.setVisibility(View.INVISIBLE);
        handle.setImageResource(R.drawable.transparent_box);
        content.setImageResource(R.drawable.transparent_box);
        imageViewCart.setVisibility(View.VISIBLE);
    }

    public static void changeViewBuyButtonStartScroll(TextView tvPriceAfterSwipe, TextView tvSweepAfterSwipe,
                                                      ImageView handle, LinearLayout imageViewCart) {
        tvPriceAfterSwipe.setVisibility(View.VISIBLE);
        tvSweepAfterSwipe.setVisibility(View.VISIBLE);
        handle.setImageResource(R.drawable.icon_swipe_toggle_button);
        imageViewCart.setVisibility(View.INVISIBLE);
    }

}
