package com.mm.main.app.adapter.strorefront.cart;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.checkout.CheckoutActivity;
import com.mm.main.app.activity.storefront.product.ProductDetailPageActivity;
import com.mm.main.app.activity.storefront.cart.ShoppingCartActivity;
import com.mm.main.app.adapter.strorefront.product.ProductDetailSelectionCollection;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.blurbehind.BlurBehind;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.library.swipemenu.SwipeDefaultActionListener;
import com.mm.main.app.library.swipemenu.SwipeLayout;
import com.mm.main.app.library.swipemenu.adapters.RecyclerSwipeAdapter;
import com.mm.main.app.listitem.DynamicAvailableListItem;
import com.mm.main.app.listitem.ShoppingCartMerchantRvItem;
import com.mm.main.app.listitem.ShoppingCartProductRvItem;
import com.mm.main.app.listitem.ShoppingCartRvItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.record.StylesRecord;
import com.mm.main.app.schema.Cart;
import com.mm.main.app.schema.CartItem;
import com.mm.main.app.schema.request.CartItemRemoveRequest;
import com.mm.main.app.schema.ProductColor;
import com.mm.main.app.schema.ProductSize;
import com.mm.main.app.schema.WishList;
import com.mm.main.app.schema.request.WishListItemAddRequest;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class ShoppingCartRVAdapter extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    List<ShoppingCartRvItem> items;
    Activity activity;

    public ShoppingCartRVAdapter(Activity activity, List<ShoppingCartRvItem> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().ordinal();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ShoppingCartRvItem.ItemType itemType = ShoppingCartRvItem.ItemType.values()[viewType];

        RecyclerView.ViewHolder viewHolder = null;

        switch (itemType) {
            case TYPE_SHOPPING_CART_MERCHANT:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_cart_brand_item_view, parent, false);
                viewHolder = new ShoppingCartMerchantViewHolder(view);
                break;

            case TYPE_SHOPPING_CART_PRODUCT:
                View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_cart_item_view, parent, false);
                viewHolder = new ShoppingCartProductViewHolder(view1);
                break;

            case TYPE_HEADER:
                View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_cart_header_view, parent, false);
                viewHolder = new ShoppingCartHeaderViewHolder(view2);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        final ShoppingCartRvItem item = items.get(position);

        switch (item.getType()) {
            case TYPE_SHOPPING_CART_MERCHANT:
                ShoppingCartMerchantViewHolder merchantViewHolder = (ShoppingCartMerchantViewHolder) viewHolder;

                merchantViewHolder.textViewDesc.setText(((ShoppingCartMerchantRvItem) item).getMerchant().getMerchantName());

                if (((ShoppingCartMerchantRvItem) item).getMerchant().getMerchantImage() != null) {
                    Picasso.with(MyApplication.getContext()).load(PathUtil.getMerchantImageUrl(((ShoppingCartMerchantRvItem) item).getMerchant().getMerchantImage())).into(merchantViewHolder.imageView);
                }

                List<ShoppingCartProductRvItem> cartItems = ((ShoppingCartMerchantRvItem) item).getProductItems();
                item.setSelected(true);
                for (ShoppingCartProductRvItem cartItem : cartItems) {
                    if (cartItem.getSelected() == false) {
                        item.setSelected(false);
                    }
                }

                merchantViewHolder.merChantCheckBox.setChecked(item.getSelected());
                merchantViewHolder.merChantCheckBox.setTag(position);
                merchantViewHolder.merChantCheckBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CheckBox checkBox = (CheckBox) v;
                        int pos = (int) v.getTag();
                        processSelectItem(pos, checkBox.isChecked());
                    }
                });



                break;

            case TYPE_SHOPPING_CART_PRODUCT:
                final ShoppingCartProductViewHolder productViewHolder = (ShoppingCartProductViewHolder) viewHolder;

                productViewHolder.productName.setText(((ShoppingCartProductRvItem) item).getProduct().getSkuName());
                productViewHolder.productColor.setText(((ShoppingCartProductRvItem) item).getProduct().getSkuColor());
                productViewHolder.productSize.setText(((ShoppingCartProductRvItem) item).getProduct().getSizeName());
                productViewHolder.productQuantity.setText(((ShoppingCartProductRvItem) item).getProduct().getQty() + "");

                NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CHINA);
                format.setMaximumFractionDigits(0);
                double retailPrice = ((ShoppingCartProductRvItem) item).getProduct().getPriceRetail();
                double salePrice = ((ShoppingCartProductRvItem) item).getProduct().getPriceSale();

                if (salePrice > 0) { //exits sale price
                    productViewHolder.productPrice.setText(format.format(salePrice));
                    productViewHolder.productPrice.setTextColor(activity.getResources().getColor(R.color.primary1));
                    productViewHolder.originalPrice.setVisibility(View.VISIBLE);
                    productViewHolder.originalPrice.setText(format.format(retailPrice));
                    productViewHolder.originalPrice.setPaintFlags(productViewHolder.originalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                } else {
                    productViewHolder.productPrice.setText(format.format(retailPrice));
                    productViewHolder.productPrice.setTextColor(activity.getResources().getColor(R.color.secondary2));
                    productViewHolder.originalPrice.setVisibility(View.GONE);
                }

                Picasso.with(MyApplication.getContext()).load(PathUtil.getProductImageUrl(((ShoppingCartProductRvItem) item).getProduct().getProductImage())).into(productViewHolder.productImage);
                //productViewHolder.productImage.setTag(position);
                productViewHolder.productImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CartItem cartItem = ((ShoppingCartProductRvItem) item).getProduct();
                        Intent intent = new Intent(activity, ProductDetailPageActivity.class);
                        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(ProductDetailPageActivity.STYLE_CODE_DATA, cartItem.getStyleCode());
                        activity.startActivity(intent);
                    }
                });

                Picasso.with(MyApplication.getContext()).load(PathUtil.getBrandImageUrl(((ShoppingCartProductRvItem) item).getProduct().getBrandImage())).into(productViewHolder.brandImage);

                if (isItemSoldOut((ShoppingCartProductRvItem) item)) {
                    productViewHolder.soldOut.setVisibility(View.VISIBLE);
                } else {
                    productViewHolder.soldOut.setVisibility(View.GONE);
                }


                productViewHolder.checkBox.setChecked(item.getSelected());
                productViewHolder.checkBox.setTag(position);
                productViewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CheckBox checkBox = (CheckBox) v;
                        int pos = (int) v.getTag();
                        processSelectItem(pos, checkBox.isChecked());
                    }
                });

                productViewHolder.linearWishlist.setTag(position);
                productViewHolder.linearWishlist.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemManger.removeShownLayouts(productViewHolder.swipeLayout);
                        addItemToWishList(((ShoppingCartProductRvItem) item).getProduct());
                    }
                });

                productViewHolder.linearDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteAction(((ShoppingCartProductRvItem) item).getProduct().getCartItemId());
                    }
                });

                productViewHolder.linearEdit.setTag(position);
                productViewHolder.linearEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemManger.removeShownLayouts(productViewHolder.swipeLayout);
                        CartItem cartItem = ((ShoppingCartProductRvItem) item).getProduct();
                        updateCartItem(cartItem);
                    }
                });


                productViewHolder.editButton.setTag(position);
                productViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mItemManger.removeShownLayouts(productViewHolder.swipeLayout);
                        CartItem cartItem = ((ShoppingCartProductRvItem) item).getProduct();
                        updateCartItem(cartItem);
                    }
                });

                SwipeLayout swipeLayout = productViewHolder.swipeLayout;
                swipeLayout.setDefaultViewRightID(R.id.linearDelete);
                swipeLayout.setDefaultViewLeftID(R.id.linearWishlist);
                swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
                swipeLayout.addDrag(SwipeLayout.DragEdge.Right, swipeLayout.findViewById(R.id.rightMenu));
                swipeLayout.addDrag(SwipeLayout.DragEdge.Left, swipeLayout.findViewById(R.id.leftMenu));
                swipeLayout.setTag(position);
                swipeLayout.setmDefaultActionListener(swipeDefaultActionListener);

                mItemManger.bind(viewHolder.itemView, position);


                break;

            case TYPE_HEADER:
                ShoppingCartHeaderViewHolder headerViewHolder = (ShoppingCartHeaderViewHolder) viewHolder;
                
                float ratio = (float) 4 / 15;
                int screenWidth = UiUtil.getDisplayWidth();
                headerViewHolder.img_free_delivery.getLayoutParams().width = screenWidth;
                headerViewHolder.img_free_delivery.getLayoutParams().height = UiUtil.getHeightByWidthWithRatio(screenWidth, ratio);
                headerViewHolder.img_free_delivery.requestLayout();
                
                break;

        }

        setAppiumId(viewHolder, position);
    }

    private boolean isItemSoldOut(ShoppingCartProductRvItem item) {
        //TODO
        try {
            if (Integer.parseInt(item.getQtyAts()) < item.getQtySafetyThreshold()) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return false;
    }

    private SwipeDefaultActionListener swipeDefaultActionListener = new SwipeDefaultActionListener() {
        @Override
        public void UpdateDefaultActionUI(boolean isRightMenu) {
        }

        @Override
        public void CancelDefaultActionUI(boolean isRightMenu) {
        }

        @Override
        public void ActionDefault(boolean isRightMenu, SwipeLayout swipeLayout) {
            int pos = (int) swipeLayout.getTag();
            mItemManger.removeShownLayouts(swipeLayout);

            if (isRightMenu) {
                deleteAction(((ShoppingCartProductRvItem) items.get(pos)).getProduct().getCartItemId());
            } else {
                addItemToWishList(((ShoppingCartProductRvItem) items.get(pos)).getProduct());
            }
        }
    };

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeLayout;
    }

    void processSelectItem(int pos, boolean isSelected) {
        ShoppingCartRvItem item = items.get(pos); //could be 1 of 2 types
        item.setSelected(isSelected);

        switch (item.getType()) {
            case TYPE_SHOPPING_CART_MERCHANT:
                List<ShoppingCartProductRvItem> cartItems = ((ShoppingCartMerchantRvItem) item).getProductItems();

                for (ShoppingCartProductRvItem cartItem : cartItems) {
                    cartItem.setSelected(isSelected);
                }

                break;

            case TYPE_SHOPPING_CART_PRODUCT:
                ShoppingCartMerchantRvItem merChantItem = ((ShoppingCartProductRvItem) item).getMerchantRvItem();

                if (!((ShoppingCartProductRvItem) item).getSelected()) {
                    merChantItem.setSelected(false);
                } else { //check other childs
                    merChantItem.setSelected(true);
                    for (ShoppingCartProductRvItem p : merChantItem.getProductItems()) {
                        if (!p.getSelected()) {
                            merChantItem.setSelected(false);
                            break;
                        }
                    }
                }

                break;
        }

        notifyDataSetChanged();
        ((ShoppingCartActivity) activity).updateBottomBar();
    }



    private void deleteAction(final Integer cartItemID) {
        ErrorUtil.showConfirmDialog(activity, activity.getString(R.string.MSG_CA_CONFIRM_REMOVE), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((ShoppingCartActivity) activity).saveSelectedItems();
                dialog.dismiss();
                deleteCartItem(cartItemID);
            }
        });
    }

    private void deleteCartItem(Integer cartItemID) {
        APIManager.getInstance().getCartService().removeCartItem(new CartItemRemoveRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKeyForCartRequest(), MmGlobal.getCartKeyForCartRequest(), cartItemID))
                .enqueue(new MmCallBack<Cart>(activity) {
                    @Override
                    public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        super.onFailure(t);
                        MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR,
                                activity.getString(R.string.LB_CA_CART_ITEM_FAILED), null);
                    }

                    @Override
                    public void onResponse(Response<Cart> response, Retrofit retrofit) {
                        Cart cart = response.body();
                        if (cart != null) {
                            MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                    activity.getString(R.string.LB_CA_DEL_CART_ITEM_SUCCESS), null);

                            ((ShoppingCartActivity) activity).loadDataForCart(cart);
                            notifyDataSetChanged();
                            ((ShoppingCartActivity) activity).updateBottomBar();
                            CacheManager.getInstance().setCart(cart);
                            activity.invalidateOptionsMenu();
                        } else {
                            MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR,
                                    activity.getString(R.string.LB_CA_CART_ITEM_FAILED), null);
                        }
                    }
                });
    }

    private void addItemToWishList(final CartItem cartItem) {

        ((ShoppingCartActivity) activity).saveSelectedItems();

//        APIManager.getInstance().getWishListService().addWishListItem(new WishListItemAddRequest(LanguageManager.getInstance().getCurrentCultureCode(), cartItem.getStyleCode(), MmGlobal.getUserKeyForCartRequest(), MmGlobal.getWishlistKeyForRequest(), cartItem.getSkuId(), cartItem.getColorKey(), cartItem.getMerchantId()))
        APIManager.getInstance().getCartService().addCartItemToWishlist(new WishListItemAddRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKeyForCartRequest(), MmGlobal.getCartKeyForCartRequest(), MmGlobal.getWishlistKeyForRequest(), cartItem.getCartItemId()))
                .enqueue(new MmCallBack<Cart>(activity) {
                    @Override
                    public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        super.onFailure(t);
                        MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR,
                                activity.getString(R.string.LB_CA_MOVE2WISHLIST_FAILED), null);
                    }

                    @Override
                    public void onResponse(Response<Cart> response, Retrofit retrofit) {
                        Cart cart = response.body();
                        if (cart != null) {
                            MmGlobal.setAnonymousWishListKey(cart.getWishlistKey());
                            cacheWishList(cart.getWishlistKey());
                            MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                    activity.getString(R.string.LB_CA_MOVE2WISHLIST_SUCCESS), null);
                            //reload cart bc item was removed
                            ((ShoppingCartActivity) activity).loadDataForCart(cart);
                            notifyDataSetChanged();
                            ((ShoppingCartActivity) activity).updateBottomBar();
                            CacheManager.getInstance().setCart(cart);
                            activity.invalidateOptionsMenu();
                        } else {
                            MmStatusAlertUtil.show(activity, MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR,
                                    activity.getString(R.string.LB_CA_MOVE2WISHLIST_FAILED), null);
                        }
                    }
                });
    }

    private void cacheWishList(String wishlistKey) {
        APIManager.getInstance().getWishListService().viewWishList(wishlistKey)
                .enqueue(new MmCallBack<WishList>(activity) {
                    @Override
                    public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        super.onFailure(t);
                    }

                    @Override
                    public void onResponse(Response<WishList> response, Retrofit retrofit) {
                        WishList list = response.body();
                        CacheManager.getInstance().setWishlist(list);
                    }
                });
    }

    private void updateCartItem(final CartItem cartItem) {
//        BlurBehind.getInstance().execute(activity, new OnBlurCompleteListener() {
//            @Override
//            public void onBlurComplete() {
//        ((ShoppingCartActivity) activity).blurLayer.setVisibility(View.VISIBLE);
//        ((ShoppingCartActivity) activity).getSupportActionBar().hide();
//        BlurBehind.getInstance().withAlpha(80).withFilterColor(activity.getResources().getColor(android.R.color.black))
//                .setBackground(activity, ((ShoppingCartActivity) activity).blurLayer);

        ((ShoppingCartActivity) activity).saveSelectedItems();


        final Intent intent = new Intent(activity, CheckoutActivity.class);
        intent.putExtra(CheckoutActivity.STYLE_CODE_DATA, cartItem.getStyleCode());
        intent.putExtra(CheckoutActivity.ADD_PRODUCT_TO_CART_FLAG, false);
        intent.putExtra(CheckoutActivity.QUANTITY_DATA, cartItem.getQty());
        intent.putExtra(CheckoutActivity.CART_ITEM_ID, cartItem.getCartItemId());
        intent.putExtra(CheckoutActivity.SELECTED_DATA_KEY, StylesRecord.getSelectionFromShoppingCart(cartItem));

        showCheckoutScreen(intent);
    }

    public void showCheckoutScreen(final Intent intent) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                activity.startActivityForResult(intent, CheckoutActivity.CHECK_OUT_REQUEST);
                activity.overridePendingTransition(R.anim.bottom_to_top, R.anim.not_move);
            }
        });
    }

    private void setAppiumId(RecyclerView.ViewHolder viewHolder, int position) {
        ShoppingCartActivity mainActivity = (ShoppingCartActivity) activity;
        ShoppingCartRvItem item = items.get(position);
        String indexString = "";

        switch (item.getType()) {
            case TYPE_SHOPPING_CART_MERCHANT:
                ShoppingCartMerchantViewHolder merchant = (ShoppingCartMerchantViewHolder) viewHolder;
                indexString = "-" + String.valueOf(item.getAppiumIndex());
                merchant.merChantCheckBox.setContentDescription(mainActivity.getContentDescription("UICB_CHECK_MERCHANT_ALL" + indexString));
                merchant.imageView.setContentDescription(mainActivity.getContentDescription("UIIMG_MERCHANT_LOGO" + indexString));
                merchant.textViewDesc.setContentDescription(mainActivity.getContentDescription("UILB_MERCHANT_NAME" + indexString));
                break;
            case TYPE_SHOPPING_CART_PRODUCT:
                ShoppingCartProductViewHolder productView = (ShoppingCartProductViewHolder) viewHolder;
                indexString = "-" + String.valueOf(item.getAppiumIndex());
                productView.checkBox.setContentDescription(mainActivity.getContentDescription("UICB_CHECK_PRODUCT" + indexString));
                productView.productImage.setContentDescription(mainActivity.getContentDescription("UIIMG_PRODUCT" + indexString));
                productView.brandImage.setContentDescription(mainActivity.getContentDescription("UIIMG_BRAND_LOGO" + indexString));
                productView.productName.setContentDescription(mainActivity.getContentDescription("UILB_PRODUCT_NAME" + indexString));
                productView.soldOut.setContentDescription(mainActivity.getContentDescription("UICB_CHECK_MERCHANT_ALL" + indexString));
                productView.titleColor.setContentDescription(mainActivity.getContentDescription("UILB_PI_COLOR" + indexString));
                productView.productColor.setContentDescription(mainActivity.getContentDescription("UILB_PI_COLOR_VAL" + indexString));
                productView.txtTitleSize.setContentDescription(mainActivity.getContentDescription("UILB_PI_SIZE" + indexString));
                productView.productSize.setContentDescription(mainActivity.getContentDescription("UILB_PI_SIZE_VAL" + indexString));
                productView.txtTitleQuantity.setContentDescription(mainActivity.getContentDescription("UILB_PI_QTY" + indexString));
                productView.productQuantity.setContentDescription(mainActivity.getContentDescription("UILB_PI_QTY_VAL" + indexString));
                productView.productPrice.setContentDescription(mainActivity.getContentDescription("UILB_SALES_PRICE" + indexString));
                productView.originalPrice.setContentDescription(mainActivity.getContentDescription("UILB_RETAIL_PRICE_SH" + indexString));
                productView.editButton.setContentDescription(mainActivity.getContentDescription("UIBT_EDIT" + indexString));
                productView.linearEdit.setContentDescription(mainActivity.getContentDescription("UIBT_EDIT_2" + indexString));
                productView.linearDelete.setContentDescription(mainActivity.getContentDescription("UIBT_DELETE" + indexString));
                productView.linearWishlist.setContentDescription(mainActivity.getContentDescription("UIBT_MOVE2WISHLIST" + indexString));
                break;
            case TYPE_HEADER:
                ShoppingCartHeaderViewHolder headerView = (ShoppingCartHeaderViewHolder) viewHolder;
                headerView.txtCoupon.setContentDescription(mainActivity.getContentDescription("UILB_USER_COUPON"));
                headerView.img_free_delivery.setContentDescription(mainActivity.getContentDescription("UIIMG_CART_BANNER"));
                headerView.edVoucherCode.setContentDescription(mainActivity.getContentDescription("UITB_COUPON_CODE"));
                break;
        }
    }

    public static class ShoppingCartProductViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.swipeLayout)
        SwipeLayout swipeLayout;

        @Bind(R.id.shopping_cart_product_name)
        TextView productName;

        @Bind(R.id.shopping_cart_checkbox)
        public CheckBox checkBox;

        @Bind(R.id.textview_color)
        TextView titleColor;

        @Bind(R.id.shopping_cart_color)
        TextView productColor;

        @Bind(R.id.txtTitleSize)
        TextView txtTitleSize;

        @Bind(R.id.shopping_cart_size)
        TextView productSize;

        @Bind(R.id.txtTitleQuantity)
        TextView txtTitleQuantity;

        @Bind(R.id.shopping_cart_number)
        TextView productQuantity;

        @Bind(R.id.shopping_cart_image)
        ImageView productImage;

        @Bind(R.id.product_brand_image)
        ImageView brandImage;

        @Bind(R.id.shopping_cart_price)
        TextView productPrice;

        @Bind(R.id.original_price)
        TextView originalPrice;

        @Bind(R.id.sold_out_text)
        TextView soldOut;

        @Bind(R.id.linearDelete)
        LinearLayout linearDelete;

        @Bind(R.id.linearEdit)
        LinearLayout linearEdit;

        @Bind(R.id.linearWishlist)
        LinearLayout linearWishlist;

        @Bind(R.id.shopping_cart_button_edit)
        Button editButton;

        public ShoppingCartProductViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            ButterKnife.bind(this, itemLayoutView);
        }
    }

    public static class ShoppingCartMerchantViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.shopping_cart_checkbox_brand)
        public CheckBox merChantCheckBox;

        @Bind(R.id.shopping_cart_image_brand)
        public ImageView imageView;

        @Bind(R.id.textViewDesc)
        public TextView textViewDesc;

        public ShoppingCartMerchantViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            ButterKnife.bind(this, itemLayoutView);
        }
    }

    public static class ShoppingCartHeaderViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.voucher_code)
        public EditText edVoucherCode;

        @Bind(R.id.img_free_delivery)
        public ImageView img_free_delivery;

        @Bind(R.id.txtCoupon)
        public TextView txtCoupon;

        public ShoppingCartHeaderViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            ButterKnife.bind(this, itemLayoutView);
        }
    }

}




