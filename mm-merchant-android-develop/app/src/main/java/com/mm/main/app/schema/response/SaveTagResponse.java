
package com.mm.main.app.schema.response;

import com.mm.main.app.schema.Tag;

import java.util.ArrayList;
import java.util.List;

public class SaveTagResponse {

    private Integer UserId;
    private Boolean replace;
    private List<Tag> TagIds = new ArrayList<Tag>();

    /**
     * 
     * @return
     *     The UserId
     */
    public Integer getUserId() {
        return UserId;
    }

    /**
     * 
     * @param UserId
     *     The UserId
     */
    public void setUserId(Integer UserId) {
        this.UserId = UserId;
    }

    /**
     * 
     * @return
     *     The replace
     */
    public Boolean getReplace() {
        return replace;
    }

    /**
     * 
     * @param replace
     *     The replace
     */
    public void setReplace(Boolean replace) {
        this.replace = replace;
    }


    public List<Tag> getTagIds() {
        return TagIds;
    }

    public void setTagIds(List<Tag> tagIds) {
        TagIds = tagIds;
    }
}
