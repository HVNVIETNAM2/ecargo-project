package com.mm.main.app.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;

import com.mm.main.app.uicomponent.FixedSpeedScroller;

import java.lang.reflect.Field;

/**
 * Created by henrytung on 11/12/2015.
 */
public class CustomViewPager extends ViewPager {

    TouchEventCallBack touchEventCallBack;
    //define callback interface
    public interface TouchEventCallBack {

        void touchedCallback(MotionEvent ev);

    }

    private boolean enabled;

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;
    }

    public void setTouchEventCallBack(TouchEventCallBack touchEventCallBack){
        this.touchEventCallBack = touchEventCallBack;
        this.enabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(touchEventCallBack != null){
            touchEventCallBack.touchedCallback(event);
        }

        if (this.enabled) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Override the Scroller instance with our own class so we can change the
     * duration
     */
    public void setVelocityScroller(Context context, int duration) {
        try {
            Field mScroller;
            Interpolator sInterpolator = new AccelerateInterpolator();
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(context, sInterpolator, duration);
            mScroller.set(this, scroller);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }
    }
}