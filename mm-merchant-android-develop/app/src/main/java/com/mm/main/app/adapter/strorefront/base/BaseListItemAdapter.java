package com.mm.main.app.adapter.strorefront.base;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.BaseListItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Setting List Adapter
 */
public class BaseListItemAdapter extends BaseAdapter {

    private final Activity context;
    private final List<BaseListItem> itemList;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    private Bitmap bitmap;

    public BaseListItemAdapter(Activity context, List<BaseListItem> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        BaseListItem baseListItem = itemList.get(position);

        if (baseListItem.getType() == BaseListItem.ItemType.TYPE_PHOTO) {
            if (view == null) {
                LayoutInflater inflater = context.getLayoutInflater();
                view = inflater.inflate(R.layout.setting_photo_item, null, true);
            }
            TextView textView = (TextView) view.findViewById(R.id.photo_title);
            ImageView imageView = (ImageView) view.findViewById(R.id.setting_image);
            textView.setText(itemList.get(position).getTitle());
            if (getBitmap() == null) {
                Picasso.with(MyApplication.getContext()).load(getItem(position).getContent())
                        .placeholder(R.drawable.placeholder).into(imageView);

            } else {
                imageView.setImageBitmap(bitmap);
            }
        } else if (baseListItem.getType() == BaseListItem.ItemType.TYPE_TEXT) {
            if (view == null) {
                LayoutInflater inflater = context.getLayoutInflater();
                view = inflater.inflate(R.layout.setting_text_item, null, true);
            }
            view = getTextTypeView(position, view, null);
        } else if (baseListItem.getType() == BaseListItem.ItemType.TYPE_TEXT_ARROW) {
            if (view == null) {
                LayoutInflater inflater = context.getLayoutInflater();
                view = inflater.inflate(R.layout.setting_text_item_arrow, null, true);
            }
            view = getTextTypeView(position, view, null);
        } else if (baseListItem.getType() == BaseListItem.ItemType.TYPE_REGULAR_TEXT) {
            if (view == null) {
                LayoutInflater inflater = context.getLayoutInflater();
                view = inflater.inflate(R.layout.setting_regular_text_item, null, true);
            }
            view = getTextTypeView(position, view, null);
        } else if (baseListItem.getType() == BaseListItem.ItemType.TYPE_RED_TEXT) {
            if (view == null) {
                LayoutInflater inflater = context.getLayoutInflater();
                view = inflater.inflate(R.layout.setting_text_item, null, true);
            }
            view = getTextTypeView(position, view, context.getResources().getColor(R.color.primary1));
        } else if (baseListItem.getType() == BaseListItem.ItemType.TYPE_DIVIDER) {
            if (view == null) {
                LayoutInflater inflater = context.getLayoutInflater();
                view = inflater.inflate(R.layout.divider_item, null, true);
            }
        }
        return view;
    }

    private View getTextTypeView(int position, View view, Integer color) {
        TextView titleTextView = (TextView) view.findViewById(R.id.setting_title);
        TextView contentTextView = (TextView) view.findViewById(R.id.setting_content);
        titleTextView.setText(itemList.get(position).getTitle());
        contentTextView.setText(itemList.get(position).getContent());

        if (color != null) {
            titleTextView.setTextColor(color);
        }

        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType().ordinal();
    }

    @Override
    public int getViewTypeCount() {
        return BaseListItem.ItemType.values().length;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public BaseListItem getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
