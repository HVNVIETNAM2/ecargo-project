package com.mm.main.app.schema;

/**
 * Created by ductranvt on 2/24/2016.
 */
public class CartMergeRequest {
    String CultureCode;
//    String CartKey;
    String UserKey;
    String MergeCartKey;

    public CartMergeRequest(String cultureCode, String userKey, String mergeCartKey) {
        CultureCode = cultureCode;
        UserKey = userKey;
        MergeCartKey = mergeCartKey;
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public String getMergeCartKey() {
        return MergeCartKey;
    }

    public void setMergeCartKey(String mergeCartKey) {
        MergeCartKey = mergeCartKey;
    }
}