package com.mm.main.app.utils;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.animation.ScaleInAnimationAdapter;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by haivu on 2/16/2016.
 */
public class CenterLockListener extends RecyclerView.OnScrollListener {

    public interface RecyclerViewCallBack {
        void updateView(View newView, View oldView, int oldCenterPosition, int newCenterPosition);
    }

    private Context context;
    private RecyclerViewCallBack callBack;
    private boolean mAutoSet;
    private RecyclerView recyclerView;
    private View centerView;
    private float lastSize = 0.6f;
    private ScaleInAnimationAdapter scaleAdapter;
    CircleImageView circleProcess;
    public CenterLockListener(Context context,CircleImageView circleProcess, RecyclerView recyclerView,ScaleInAnimationAdapter scaleAdapter ,RecyclerViewCallBack callBack) {
        this.context = context;
        this.callBack = callBack;
        this.recyclerView = recyclerView;
        this.scaleAdapter = scaleAdapter;
        this.circleProcess = circleProcess;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        final int mCenterPivot = (int) (CircleProcessUtil.CENTER_PERCENT * recyclerView.getHeight());
        LinearLayoutManager lm = (LinearLayoutManager) recyclerView.getLayoutManager();

        if (!mAutoSet) {

            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                //ScrollStoppped
                View view = UiUtil.findCenterView(lm,recyclerView);//get the view nearest to center
                int viewCenter = lm.getOrientation() == LinearLayoutManager.HORIZONTAL ? (view.getLeft() + view.getRight()) / 2 : (view.getTop() + view.getBottom()) / 2;
                //compute scroll from center
                int scrollNeeded = viewCenter - mCenterPivot; // Add or subtract any offsets you need here

                if (lm.getOrientation() == LinearLayoutManager.HORIZONTAL) {

                    recyclerView.smoothScrollBy(scrollNeeded, 0);
                } else {
                    recyclerView.smoothScrollBy(0, scrollNeeded);

                }

                //TODO call center animation


                mAutoSet = true;


                circleProcess = (CircleImageView) view.findViewById(R.id.centerAnimateView);
                if(circleProcess != null){
                    circleProcess.setScaleX(0.0f);
                    circleProcess.setScaleY(0.0f);
                    circleProcess.animate().scaleXBy(1f)
                            .scaleYBy(1f)
                            .setDuration(300).alpha(0f).alphaBy(0.5f)
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    circleProcess.setAlpha(0f);
                                    circleProcess.setScaleX(0.0f);
                                    circleProcess.setScaleY(0.0f);
                                }
                            }).start();
                }
            }
        }
        if (newState == RecyclerView.SCROLL_STATE_DRAGGING || newState == RecyclerView.SCROLL_STATE_SETTLING) {

            mAutoSet = false;
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager lm = (LinearLayoutManager) recyclerView.getLayoutManager();
        final float viewHeight = UiUtil.getPixelFromDp(UiUtil.CURATOR_ITEM_HEIGHT);

        int h = (int) (CircleProcessUtil.CENTER_PERCENT * recyclerView.getHeight());
        final View view = UiUtil.findCenterView(lm, recyclerView);
        if (view != null) {

            float toSize = 1f;
            float currentY = view.getY() + (view.getHeight() / 2);

            int oldCenterPosition = recyclerView.getChildLayoutPosition(centerView);

            if (Math.abs(h - currentY) <= viewHeight) {
                toSize = 1f - (Math.abs((h - currentY) / viewHeight) * 0.4f);
            } else {
                toSize = 0.60f;
            }

            View oldView = centerView;
            if (centerView != view) {
                scaleAdapter.nearCenterAnimation(centerView, lastSize, 0.6f);
                if (lastSize != toSize) {
                    Log.d("Animation", "Tosize" + toSize);
                    Handler handler = new Handler();
                    final float fToSize = toSize;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            scaleAdapter.nearCenterAnimation(view, 0.6f, fToSize);
                        }
                    });

                }
                centerView = view;
            } else {
                if (lastSize != toSize) {
                    scaleAdapter.nearCenterAnimation(centerView, lastSize, toSize);
                }
            }
            int newCenterPosition = recyclerView.getChildLayoutPosition(centerView);
            lastSize = toSize;
            callBack.updateView(centerView, oldView, oldCenterPosition, newCenterPosition);
        }
    }
}
