package com.mm.main.app.helper;

import com.mm.main.app.listitem.DynamicAvailableListItem;
import com.mm.main.app.schema.ProductColor;
import com.mm.main.app.schema.ProductSize;
import com.mm.main.app.schema.Sku;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by henrytung on 20/1/2016.
 */
public class ProductAvailabilityHelper {

    List<Sku> skuList;
    List<DynamicAvailableListItem<ProductSize>> sizeList;
    List<DynamicAvailableListItem<ProductColor>> colorList;
    HashMap<String, Sku> productAvailabilityMap;
    HashMap<String, List<Sku>> sizeAvailabilityMap;
    HashMap<String, List<Sku>> colorAvailabilityMap;

    public ProductAvailabilityHelper(List<Sku> skuList,
                                     List<DynamicAvailableListItem<ProductSize>> sizeList,
                                     List<DynamicAvailableListItem<ProductColor>> colorList) {
        this.skuList = skuList;
        this.sizeList = sizeList;
        this.colorList = colorList;

        initMap();
    }

    private void initMap() {
        productAvailabilityMap = new HashMap<>();
        sizeAvailabilityMap = new HashMap<>();
        colorAvailabilityMap = new HashMap<>();
        for (Sku sku: skuList) {
            productAvailabilityMap.put(getAvailabilityKey(sku), sku);
            initArrayToMap(sizeAvailabilityMap, sku, String.valueOf(sku.getSizeId()));
            initArrayToMap(colorAvailabilityMap, sku, sku.getColorKey());
        }
    }

    private void initArrayToMap(HashMap<String, List<Sku>> map, Sku sku, String key) {
        if (map.containsKey(key)) {
            map.get(key).add(sku);
        } else {
            List<Sku> skuList = new ArrayList<>();
            skuList.add(sku);
            map.put(key, skuList);
        }

    }

    private String getAvailabilityKey(Sku sku) {
        return getAvailabilityKey(sku.getSizeId(), sku.getColorKey());
    }

    private String getAvailabilityKey(int sizeId, String colorKey) {
        return sizeId + "_" + colorKey;
    }

    public List<DynamicAvailableListItem<ProductSize>> getDefaultSizeAvailability() {
        for (DynamicAvailableListItem<ProductSize> productSize:
                sizeList) {
            productSize.setAvailable(sizeAvailabilityMap.containsKey(String.valueOf(productSize.getT().getSizeId())));
        }

        return sizeList;
    }

    public List<DynamicAvailableListItem<ProductColor>> getDefaultColorAvailability() {
        for (DynamicAvailableListItem<ProductColor> productColor:
                colorList) {
            productColor.setAvailable(colorAvailabilityMap.containsKey(String.valueOf(productColor.getT().getColorKey())));
        }

        return colorList;
    }

    public List<DynamicAvailableListItem<ProductSize>> updateSizeAvailability(DynamicAvailableListItem<ProductColor> productColor) {
        for (DynamicAvailableListItem<ProductSize> productSize:
             sizeList) {
            productSize.setAvailable(productAvailabilityMap.containsKey(
                    getAvailabilityKey(productSize.getT().getSizeId(), productColor.getT().getColorKey())));
        }

        return sizeList;
    }

    public List<DynamicAvailableListItem<ProductColor>> updateColorAvailability(DynamicAvailableListItem<ProductSize> productSize) {
        for (DynamicAvailableListItem<ProductColor> productColor:
                colorList) {
            productColor.setAvailable(productAvailabilityMap.containsKey(
                    getAvailabilityKey(productSize.getT().getSizeId(), productColor.getT().getColorKey())));
        }

        return colorList;
    }

    public List<DynamicAvailableListItem<ProductSize>> getSizeList() {
        return sizeList;
    }

    public List<DynamicAvailableListItem<ProductColor>> getColorList() {
        return colorList;
    }
}
