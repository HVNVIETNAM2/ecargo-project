package com.mm.main.app.adapter.strorefront.discover;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.decoration.GridSpacingItemDecoration;

import com.mm.main.app.fragment.DiscoverBrandsFragment;
import com.mm.main.app.fragment.ScreenSlidePageFragment;
import com.mm.main.app.listitem.DiscoverBannerListItem;
import com.mm.main.app.listitem.DiscoverBrandListItem;
import com.mm.main.app.listitem.DiscoverBrandMainListItem;
import com.mm.main.app.log.Logger;
import com.mm.main.app.schema.BrandUnionMerchant;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.view.CustomViewPager;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by henrytung on 2/12/2015.
 */
public class DiscoverBrandMainRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final float imgSizeRatio = (float)3/5;
    private final Activity context;
    private DiscoverBrandsFragment fragment;
    private List<DiscoverBrandMainListItem> itemsData;
    private List<BrandUnionMerchant> brandUnionMerchantList;

    public final static int ACTION_PROCEED_PRODUCT_LIST_PAGE = 0;
    public final static int ACTION_PROCEED_ALL_BRAND_PAGE = ACTION_PROCEED_PRODUCT_LIST_PAGE + 1;

    public Timer autoScrollTimer;
    public CustomViewPager vPager;
    private ScreenSlidePagerAdapter mPagerAdapter;
    public int currentPage;

    final DiscoverBrandMainRvAdapter.OnListFragmentInteractionListener onListFragmentInteractionListener;

    public DiscoverBrandMainRvAdapter(Activity context, List<DiscoverBrandMainListItem> itemsData, List<BrandUnionMerchant> brandUnionMerchantList, DiscoverBrandMainRvAdapter.OnListFragmentInteractionListener onListFragmentInteractionListener) {
        this.itemsData = itemsData;
        this.context = context;
        this.brandUnionMerchantList = brandUnionMerchantList;
        this.onListFragmentInteractionListener = onListFragmentInteractionListener;

    }

    public void setFragment(DiscoverBrandsFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public int getItemViewType(int position) {
        return itemsData.get(position).getType().ordinal();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        DiscoverBrandMainListItem.ItemType itemType = DiscoverBrandMainListItem.ItemType.values()[viewType];

        RecyclerView.ViewHolder viewHolder = null;

        switch (itemType) {
            case TYPE_BRAND_BANNER:
                viewHolder = new ViewHolderBanner(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.discover_brand_banner_item_view, viewGroup, false));
                break;
            case TYPE_TWO_COLUMN_BRAND:
                viewHolder = new ViewHolderBrandList(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.discover_brand_2_column_item_view, viewGroup, false));
                break;
        }

        return viewHolder;
    }

    private void bindBannerItemView(final ViewHolderBanner viewHolderImageDefault) {
        if (autoScrollTimer != null && vPager != null) {
            vPager.setVelocityScroller(context, 200);
            autoScrollTimer.cancel();
            autoScrollTimer = null;
        }

        autoScrollTimer = new Timer();
        autoScrollTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        int newPos = 0;
                        int currentPos = currentPage;
                        if (currentPos < viewHolderImageDefault.imageViewIndicators.length - 1) {
                            newPos = currentPos + 1;
                        }

                        if(vPager != null){
                            vPager.setVelocityScroller(context, 1000);
                            vPager.setCurrentItem(newPos, true);
                        }
                    }
                });
            }
        }, Constant.AUTO_SCROLL_TIMER, Constant.AUTO_SCROLL_TIMER);

    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public ImageView imageViewIndicators[];

        public void onPageSelected(int position) {
            Logger.d("Current", "page selected " + position);
            currentPage = position;
            if(imageViewIndicators != null){
                updateDefaultImageIndicator(imageViewIndicators, position);
            }
        }
    }

    private int getImageIndicatorPosition(ImageView[] imageViewIndicators) {
        for (int i = 0; i < imageViewIndicators.length; i++) {
            if (imageViewIndicators[i].isSelected()) {
                return i;
            }
        }
        return 0;
    }

    private void updateDefaultImageIndicator(ImageView[] imageViewIndicators, int newPos) {
        for (int i = 0; i < imageViewIndicators.length; i++) {
            if (i == newPos) {
                imageViewIndicators[i].setImageDrawable(context.getResources().getDrawable(R.drawable.scroll_color_ball_red));
                imageViewIndicators[i].setSelected(true);
            } else {
                imageViewIndicators[i].setImageDrawable(context.getResources().getDrawable(R.drawable.scroll_color_ball_white));
                imageViewIndicators[i].setSelected(false);
            }
        }
    }

    private void bindBrand2ColumnItemView(final ViewHolderBrandList viewHolderBrandList) {
        viewHolderBrandList.btnAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onListFragmentInteractionListener.onDiscoverBrandFragmentInteraction(null, ACTION_PROCEED_ALL_BRAND_PAGE);
            }
        });

        viewHolderBrandList.btnRecommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolderBrandList.btnRecommend.setSelected(!viewHolderBrandList.btnRecommend.isSelected());
                viewHolderBrandList.btnAll.setSelected(!viewHolderBrandList.btnRecommend.isSelected());
                setRecommendToBrandList(viewHolderBrandList.discoverBrandRVAdapter, viewHolderBrandList.recyclerView);
            }
        });

        viewHolderBrandList.btnRecommend.setSelected(true);
    }

    private void setRecommendToBrandList(DiscoverBrandRVAdapter discoverBrandRVAdapter, RecyclerView recyclerView) {
        List<DiscoverBrandListItem> discoverBrandListItemList = new ArrayList<>();
        for (int i = 0; i < brandUnionMerchantList.size(); i++) {
            BrandUnionMerchant brandUnionMerchant = brandUnionMerchantList.get(i);
            //ToDo remove comment out when ALL page is ready
//            if (brandUnionMerchant.getIsRecommended() == 1) {
            if (!TextUtils.isEmpty(brandUnionMerchant.getLargeLogoImage())) {
                discoverBrandListItemList.add(new DiscoverBrandListItem(brandUnionMerchant.getLargeLogoImage(), brandUnionMerchant));
            }
//            }
        }

        discoverBrandRVAdapter.setItemList(discoverBrandListItemList);
        discoverBrandRVAdapter.notifyDataSetChanged();

        int screenWidth = UiUtil.getDisplayWidth();
        int height = ((discoverBrandListItemList.size() + 1) / 2) * (screenWidth / 2);

        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(screenWidth, height);
        p.addRule(RelativeLayout.BELOW, R.id.llSecondLevelControl);
        recyclerView.setLayoutParams(p);

    }

    private void setAllToBrandList(DiscoverBrandRVAdapter discoverBrandRVAdapter, RecyclerView recyclerView) {
        List<DiscoverBrandListItem> discoverBrandListItemList = new ArrayList<>();
        for (int i = 0; i < brandUnionMerchantList.size(); i++) {
            BrandUnionMerchant brandUnionMerchant = brandUnionMerchantList.get(i);
            discoverBrandListItemList.add(new DiscoverBrandListItem(brandUnionMerchant.getLargeLogoImage(), brandUnionMerchant));
        }

        discoverBrandRVAdapter.setItemList(discoverBrandListItemList);
        discoverBrandRVAdapter.notifyDataSetChanged();

        int screenWidth = UiUtil.getDisplayWidth();
        int height = ((discoverBrandListItemList.size() + 1) / 2) * (screenWidth / 2);

        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(screenWidth, height);
        p.addRule(RelativeLayout.BELOW, R.id.llSecondLevelControl);
        recyclerView.setLayoutParams(p);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        DiscoverBrandMainListItem listItem = itemsData.get(position);

        switch (listItem.getType()) {
            case TYPE_BRAND_BANNER:
                ViewHolderBanner viewHolderImageDefault = (ViewHolderBanner) viewHolder;
                bindBannerItemView(viewHolderImageDefault);
                break;
            case TYPE_TWO_COLUMN_BRAND:
                ViewHolderBrandList viewHolderDescriptionImage = (ViewHolderBrandList) viewHolder;
                bindBrand2ColumnItemView(viewHolderDescriptionImage);
                break;
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private List<DiscoverBannerListItem> mValues;
        public void setmValues(List<DiscoverBannerListItem> mValues) {
            this.mValues = mValues;
        }

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
            fragment.setImage(mValues.get(position));
            final int itemPos = position;
            fragment.setListener(new ScreenSlidePageFragment.OnClickSlideListener() {
                @Override
                public void onClick(View v, int id) {
                    onListFragmentInteractionListener.onDiscoverBrandFragmentInteraction((mValues.get(itemPos) ).getBrandUnionMerchant(), ACTION_PROCEED_PRODUCT_LIST_PAGE);
                }
            });
            return fragment;
        }

        @Override
        public int getCount() {
            return mValues.size();
        }

    }


    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolderBanner extends RecyclerView.ViewHolder {
        public LinearLayout llIndicator;
        public ImageView imageViewIndicators[];

        public ViewHolderBanner(View itemLayoutView) {
            super(itemLayoutView);

            // Instantiate a ViewPager and a PagerAdapter.

            llIndicator = (LinearLayout) itemLayoutView.findViewById(R.id.llIndicator);

            final List<DiscoverBannerListItem> discoverBannerListItemList = new ArrayList<>();
            if (brandUnionMerchantList != null) {
                for (int i = 0; i < brandUnionMerchantList.size(); i++) {
                    String imageKey = brandUnionMerchantList.get(i).getProfileBannerImage();
                    if (!TextUtils.isEmpty(imageKey) && brandUnionMerchantList.get(i).getIsFeatured() == 1) {
                        discoverBannerListItemList.add(new DiscoverBannerListItem(imageKey, brandUnionMerchantList.get(i)));
                    }
                }
            }

            imageViewIndicators = new ImageView[discoverBannerListItemList.size()];
            for (int i = 0; i < imageViewIndicators.length; i++) {
//                ImageData imageData = imageDatas.get(i);
                imageViewIndicators[i] = new ImageView(MyApplication.getContext());
                LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(5, 5);
                int rightPadding = UiUtil.getPixelFromDp(10);
                layoutParams.setMargins(0, 0, rightPadding, 0);
                imageViewIndicators[i].setLayoutParams(layoutParams);

                imageViewIndicators[i].setId(i);

                if (i == 0) {
                    imageViewIndicators[i].setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.scroll_color_ball_red));
                } else {
                    imageViewIndicators[i].setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.scroll_color_ball_white));
                }

                llIndicator.addView(imageViewIndicators[i]);
            }



            vPager = (CustomViewPager) itemLayoutView.findViewById(R.id.vPager);

            vPager.setLayoutParams(new RelativeLayout.LayoutParams(UiUtil.getDisplayWidth() * discoverBannerListItemList.size(),
                    UiUtil.getHeightByWidthWithRatio(UiUtil.getDisplayWidth(), imgSizeRatio)));

            mPagerAdapter = new ScreenSlidePagerAdapter(fragment.getFragmentManager());
            mPagerAdapter.setmValues(discoverBannerListItemList);
            vPager.setAdapter(mPagerAdapter);
            PageListener pageListener = new PageListener();
            pageListener.imageViewIndicators = imageViewIndicators;
            vPager.addOnPageChangeListener(pageListener);

            final ViewHolderBanner viewHolderBanner = this;
            vPager.setTouchEventCallBack(new CustomViewPager.TouchEventCallBack() {
                @Override
                public void touchedCallback(MotionEvent ev) {
                    switch (ev.getAction()){
                        case MotionEvent.ACTION_DOWN:
                            if(autoScrollTimer != null){
                                autoScrollTimer.cancel();
                            }
                            // kill timer
                            break;
                        case MotionEvent.ACTION_UP:
                            // start timer
                            bindBannerItemView(viewHolderBanner);
                            break;

                    }
                }
            });

        }
    }

    public class ViewHolderBrandList extends RecyclerView.ViewHolder {
        public RecyclerView recyclerView;
        public Button btnRecommend;
        public Button btnAll;
        public DiscoverBrandRVAdapter discoverBrandRVAdapter;

        public ViewHolderBrandList(View itemLayoutView) {
            super(itemLayoutView);

            recyclerView = (RecyclerView) itemLayoutView.findViewById(R.id.rvBrands);
            btnRecommend = (Button) itemLayoutView.findViewById(R.id.btnRecommend);
            btnAll = (Button) itemLayoutView.findViewById(R.id.btnAll);

            final GridLayoutManager glm = new GridLayoutManager(MyApplication.getContext(), 2);
            recyclerView.setLayoutManager(glm);
            recyclerView.setClipToPadding(false);
            recyclerView.setClipChildren(false);

            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                int mLastFirstVisibleItem = 0;

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
//                    if (scroll_up) {
////                    ((StorefrontMainActivity) (getActivity().getParent().getParent())).openTabHost(false);
//                        StoreFrontUtil.setMasterTabVisibility(getActivity(), false);
//                    } else {
////                    ((StorefrontMainActivity) (getActivity().getParent().getParent())).openTabHost(true);
//                        StoreFrontUtil.setMasterTabVisibility(getActivity(), true);
//                    }
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
//                    if (dy > 70) {
//                        scroll_up = true;
//
//                    } else if (dy < -5) {
//                        scroll_up = false;
//                    }
                }
            });

            List<DiscoverBrandListItem> discoverBrandListItemList = new ArrayList<>();
            discoverBrandRVAdapter = new DiscoverBrandRVAdapter(discoverBrandListItemList, new DiscoverBrandRVAdapter.BrandListOnClickListener() {
                @Override
                public void onClick(int position, Object data) {
//                    ((DiscoverMainActivity)context).startProductListPage();
                    DiscoverBrandListItem discoverBrandListItem = (DiscoverBrandListItem) data;
                    onListFragmentInteractionListener.onDiscoverBrandFragmentInteraction(discoverBrandListItem.getBrandUnionMerchant(), ACTION_PROCEED_PRODUCT_LIST_PAGE);
                }
            });
            recyclerView.setAdapter(discoverBrandRVAdapter);

            setRecommendToBrandList(discoverBrandRVAdapter, recyclerView);

            int spacingInPixels = MyApplication.getContext().getResources().getDimensionPixelSize(R.dimen.grid_spacing);
            recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacingInPixels, true));

            discoverBrandRVAdapter.notifyDataSetChanged();

        }

    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public List<DiscoverBrandMainListItem> getItemsData() {
        return itemsData;
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onDiscoverBrandFragmentInteraction(BrandUnionMerchant brandUnionMerchant, int actionCode);
    }

    public void setItemsData(List<DiscoverBrandMainListItem> itemsData) {
        this.itemsData = itemsData;
    }
}
