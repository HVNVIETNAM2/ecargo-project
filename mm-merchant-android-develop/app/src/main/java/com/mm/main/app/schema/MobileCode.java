package com.mm.main.app.schema;

import java.io.Serializable;

/**
 * Created by henrytung on 9/11/15.
 */
public class MobileCode implements Serializable {

    String MobileCodeId;
    String MobileCodeNameInvariant;
    String MobileCodeCultureId;
    String CultureCode;
    String MobileCodeName;

    public MobileCode() {
    }

    public MobileCode(String mobileCodeId, String mobileCodeNameInvariant, String mobileCodeCultureId, String cultureCode, String mobileCodeName) {
        MobileCodeId = mobileCodeId;
        MobileCodeNameInvariant = mobileCodeNameInvariant;
        MobileCodeCultureId = mobileCodeCultureId;
        CultureCode = cultureCode;
        MobileCodeName = mobileCodeName;
    }

    public String getMobileCodeId() {
        return MobileCodeId;
    }

    public void setMobileCodeId(String mobileCodeId) {
        MobileCodeId = mobileCodeId;
    }

    public String getMobileCodeNameInvariant() {
        return MobileCodeNameInvariant;
    }

    public void setMobileCodeNameInvariant(String mobileCodeNameInvariant) {
        MobileCodeNameInvariant = mobileCodeNameInvariant;
    }

    public String getMobileCodeCultureId() {
        return MobileCodeCultureId;
    }

    public void setMobileCodeCultureId(String mobileCodeCultureId) {
        MobileCodeCultureId = mobileCodeCultureId;
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getMobileCodeName() {
        return MobileCodeName;
    }

    public void setMobileCodeName(String mobileCodeName) {
        MobileCodeName = mobileCodeName;
    }
}
