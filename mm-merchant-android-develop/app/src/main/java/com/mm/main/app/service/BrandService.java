package com.mm.main.app.service;

import com.mm.main.app.schema.Brand;
import com.mm.main.app.schema.BrandUnionMerchant;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by henrytung on 24/11/2015.
 */
public interface BrandService {

    String CATEGORY_PATH = "brand";

    @GET(CATEGORY_PATH + "/list")
    Call<List<Brand>> list();

    @GET(CATEGORY_PATH + "/followed")
    Call<List<Brand>> viewFollowed(@Query("UserKey") String userKey);

    @GET(CATEGORY_PATH + "/following")
    Call<List<Brand>> viewFollowing(@Query("UserKey") String userKey);

    @GET(CATEGORY_PATH + "/list/combined")
    Call<List<BrandUnionMerchant>> listCombined();
}
