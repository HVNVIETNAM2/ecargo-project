package com.mm.main.app.listitem;

import java.io.Serializable;

/**
 * Created by henrytung on 18/1/2016.
 */
public class DynamicAvailableListItem<T> implements Serializable{
    T t;
    boolean selected = false;
    boolean available = true;

    public DynamicAvailableListItem(T t) {
        this.t = t;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
