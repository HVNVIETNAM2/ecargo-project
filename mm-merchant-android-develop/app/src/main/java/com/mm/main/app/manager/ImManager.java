package com.mm.main.app.manager;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import com.google.gson.*;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.record.ImEvent;
import com.mm.main.app.schema.Conv;
import com.mm.main.app.schema.SocketMessage;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.framing.Framedata;
import org.java_websocket.framing.FramedataImpl1;
import org.java_websocket.handshake.ServerHandshake;

import java.lang.reflect.Type;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import de.greenrobot.event.*;

/**
 * Created by henrytung on 8/3/2016.
 */
public class ImManager {

    private static ImManager myImManager = null;

//    private final String host = "52.76.113.145";
//   private final String host = "192.168.214.151";
//    private final String port = "7500";
//    private final String port = "9000";

    //TODO test
    private final String scheme = "ws://";
    private final String host = "52.76.113.145";
    private final String port = "7600";

    private WebSocketClient mWebSocketClient;
    private URI uri;
    private long lastPong = 0;
    private boolean delayCheck = false;
    public List<Conv> convList = new ArrayList<>();

    public static ImManager getInstance() {
        if (myImManager == null) {
            myImManager = new ImManager();
        }

        return myImManager;
    }

    private ImManager () {

        final String wsuri = scheme + host + ":" + port;

        try {
            uri = new URI(wsuri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = generateWebSocketClient();
        mWebSocketClient.connect();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                sendPing(mWebSocketClient.getConnection());
                Logger.d("send ping: " , System.currentTimeMillis()+"");
            }
        }, 1000, 1000);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                long currentTime = System.currentTimeMillis();
                Logger.d("check pong: " , (currentTime - lastPong) +"");
                if (currentTime - lastPong > 3000 && !delayCheck) {
                    EventBus.getDefault().post(new ImEvent(ImEvent.ActionType.DISABLE_SEND, ""));
                    reconnect();
                } else {
                    delayCheck = false;
                }
            }
        }, 2000, 1000);

    }

    WebSocketClient generateWebSocketClient() {
        return new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i("Websocket", "Opened");
                delayCheck = true;
//                mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
                sendAnnounce();
                EventBus.getDefault().post(new ImEvent(ImEvent.ActionType.ENABLE_SEND, ""));
            }

            @Override
            public void onMessage(String s) {
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'").create();

                SocketMessage socketMessage = gson.fromJson(s, SocketMessage.class);
                switch (socketMessage.getType()) {
                    case "Init":
                        convList = socketMessage.getConvList();
                        EventBus.getDefault().post(new ImEvent(ImEvent.ActionType.UPDATE_CONV, ""));

                        break;
                    case "Conv":
                        Conv convKey = new Conv();
                        convKey.setConvKey(socketMessage.getConvKey());
                        convList.add(convKey);
                        break;
                    default:
                        final String messageStr = socketMessage.getBody();
                        EventBus.getDefault().post(new ImEvent(ImEvent.ActionType.RECEIVE, messageStr));
                }

            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Logger.i("Websocket", "Closed " + s);
                try {
//                    Toast.makeText(JavaWebSocketEchoActivity.this, "Closed " + s, Toast.LENGTH_LONG).show();
                } catch (Exception ee) {}
            }

            @Override
            public void onError(Exception e) {
                Logger.i("Websocket", "Error " + e.getMessage());
                try {
//                    Toast.makeText(JavaWebSocketEchoActivity.this, "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
                } catch (Exception ee) {}
            }

            @Override
            public void onWebsocketPong(org.java_websocket.WebSocket conn, Framedata f ) {
                Logger.i("Websocket", "I can get the pong");
                lastPong = System.currentTimeMillis();
                Logger.d("last pong: " , lastPong +"");
            }
        };
    }

    void sendPing(org.java_websocket.WebSocket conn) {
        FramedataImpl1 frame = new FramedataImpl1(Framedata.Opcode.PING);
        frame.setFin(true);
        conn.sendFrame(frame);
    }

    void reconnect() {
        try {
            mWebSocketClient.close();
            mWebSocketClient = generateWebSocketClient();
            mWebSocketClient.connect();
        } catch (Exception e) {
            EventBus.getDefault().post(new ImEvent(ImEvent.ActionType.DISABLE_SEND, ""));
        }
    }

    private void writeString(String messageText) {
        mWebSocketClient.send(messageText);
    }

    private void writeData(byte[] data){
        mWebSocketClient.send(data);
    }

    public void writeMessage(SocketMessage socketMessage){
        Gson gson = new Gson();
        String socketMessageString = gson.toJson(socketMessage);
        mWebSocketClient.send(socketMessageString);
    }


    public void sendAnnounce(){
        SocketMessage socketMessage = new SocketMessage();
        socketMessage.setType("Announce");
        socketMessage.setUserKey(MmGlobal.getUserKey());
        writeMessage(socketMessage);
    }

    public void sendMessage(String body, String convKey){
        SocketMessage socketMessage = new SocketMessage();
        socketMessage.setType("Msg");
        if (!convList.isEmpty()) {
            socketMessage.setConvKey(convKey);
            socketMessage.setBody(body);
            writeMessage(socketMessage);
        }
    }

    public void sendConvStart(List<String> userList){
        SocketMessage socketMessage = new SocketMessage();
        socketMessage.setType("ConvStart");
        socketMessage.setUserList(userList);
        writeMessage(socketMessage);
    }

    public void sendMsgList(){
        SocketMessage socketMessage = new SocketMessage();
        socketMessage.setType("MsgList");
        if (!convList.isEmpty()) {
            socketMessage.setConvKey(convList.get(convList.size()- 1).getConvKey());
            writeMessage(socketMessage);
        }
    }

}
