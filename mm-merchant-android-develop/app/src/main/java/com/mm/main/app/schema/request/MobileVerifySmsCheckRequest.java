package com.mm.main.app.schema.request;

/**
 * Created by henrytung on 2/2/2016.
 */
public class MobileVerifySmsCheckRequest {

    String MobileNumber;
    String MobileCode;
    String MobileVerificationId;
    String MobileVerificationToken;

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getMobileCode() {
        return MobileCode;
    }

    public void setMobileCode(String mobileCode) {
        MobileCode = mobileCode;
    }

    public String getMobileVerificationId() {
        return MobileVerificationId;
    }

    public void setMobileVerificationId(String mobileVerificationId) {
        MobileVerificationId = mobileVerificationId;
    }

    public String getMobileVerificationToken() {
        return MobileVerificationToken;
    }

    public void setMobileVerificationToken(String mobileVerificationToken) {
        MobileVerificationToken = mobileVerificationToken;
    }
}
