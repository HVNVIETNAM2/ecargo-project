package com.mm.main.app.schema.request;

import java.io.Serializable;

/**
 * Created by henrytung on 2/2/2016.
 */
public class SignupRequest implements Serializable{

    String MobileNumber;
    String MobileCode;
    String MobileVerificationId;
    String MobileVerificationToken;
    String UserName;
    String DisplayName;
    String Password;

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getMobileCode() {
        return MobileCode;
    }

    public void setMobileCode(String mobileCode) {
        MobileCode = mobileCode;
    }

    public String getMobileVerificationId() {
        return MobileVerificationId;
    }

    public void setMobileVerificationId(String mobileVerificationId) {
        MobileVerificationId = mobileVerificationId;
    }

    public String getMobileVerificationToken() {
        return MobileVerificationToken;
    }

    public void setMobileVerificationToken(String mobileVerificationToken) {
        MobileVerificationToken = mobileVerificationToken;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }
}
