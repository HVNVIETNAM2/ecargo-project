package com.mm.main.app.schema;

public class UserTaggedMerchant {

    private Integer MerchantId;
    private String MerchantName;
    private String MerchantNameInvariant;
    private String SmallLogoImage;
    private String LargeLogoImage;
    private Integer Count;

    /**
     * @return The MerchantId
     */
    public Integer getMerchantId() {
        return MerchantId;
    }

    /**
     * @param MerchantId The MerchantId
     */
    public void setMerchantId(Integer MerchantId) {
        this.MerchantId = MerchantId;
    }

    /**
     * @return The MerchantName
     */
    public String getMerchantName() {
        return MerchantName;
    }

    /**
     * @param MerchantName The MerchantName
     */
    public void setMerchantName(String MerchantName) {
        this.MerchantName = MerchantName;
    }

    /**
     * @return The MerchantNameInvariant
     */
    public String getMerchantNameInvariant() {
        return MerchantNameInvariant;
    }

    /**
     * @param MerchantNameInvariant The MerchantNameInvariant
     */
    public void setMerchantNameInvariant(String MerchantNameInvariant) {
        this.MerchantNameInvariant = MerchantNameInvariant;
    }

    public String getSmallLogoImage() {
        return SmallLogoImage;
    }

    public void setSmallLogoImage(String smallLogoImage) {
        SmallLogoImage = smallLogoImage;
    }

    public String getLargeLogoImage() {
        return LargeLogoImage;
    }

    public void setLargeLogoImage(String largeLogoImage) {
        LargeLogoImage = largeLogoImage;
    }

    /**
     * @return The Count
     */
    public Integer getCount() {
        return Count;
    }

    /**
     * @param Count The Count
     */
    public void setCount(Integer Count) {
        this.Count = Count;
    }

}
