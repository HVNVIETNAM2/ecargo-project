package com.mm.main.app.activity.storefront.product;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.cart.ShoppingCartActivity;
import com.mm.main.app.activity.storefront.wishlist.WishListActivity;
import com.mm.main.app.activity.storefront.checkout.CheckoutActivity;
import com.mm.main.app.adapter.strorefront.product.ProductDetailRvAdapter;
import com.mm.main.app.adapter.strorefront.product.ProductDetailSelectionCollection;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.layout.BuyButtonSlidingDrawer;
import com.mm.main.app.listitem.ProductDetailListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.record.StylesRecord;
import com.mm.main.app.schema.Cart;
import com.mm.main.app.schema.request.CartAddRequest;
import com.mm.main.app.schema.request.CartCreateRequest;
import com.mm.main.app.schema.ProductColor;
import com.mm.main.app.schema.ProductSize;
import com.mm.main.app.schema.response.SearchResponse;
import com.mm.main.app.schema.Sku;
import com.mm.main.app.schema.Style;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Response;
import retrofit.Retrofit;

public class ProductDetailPageActivity extends AppCompatActivity {

    public static final String EXTRA_PRODUCT_DATA = "EXTRA_PRODUCT_DATA";
    public static final String EXTRA_LIKED = "EXTRA_LIKED";
    public static final String STYLE_CODE_DATA = "STYLE_CODE_DATA";

    private Style style;

    @Bind(R.id.listView)
    RecyclerView recyclerView;

    @Bind(R.id.blurLayer)
    public View blurLayer;

    @Bind(R.id.titleImage)
    ImageView titleImage;

    @Bind(R.id.imageViewIm)
    ImageView imageViewIm;

    @Bind(R.id.buy_button)
    BuyButtonSlidingDrawer buyButtonSlidingDrawer;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    boolean liked;
    boolean needToRefreshList;

    View icon_heart_fill;

    private List<ProductDetailListItem> productDetailListItemList;
    private ProductDetailRvAdapter productDetailListItemAdapter;

    ProductColor selectedColor = null;
    ProductSize selectedSize = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail_page);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        ActivityUtil.setTitle(this, style.getBrandName());
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        initData();

    }

    private void initData() {
        style = (Style) getIntent().getSerializableExtra(EXTRA_PRODUCT_DATA);
        liked = getIntent().getBooleanExtra(EXTRA_LIKED, false);

        if (style != null) {
            initUI();
        } else {
            String styleId = getIntent().getStringExtra(STYLE_CODE_DATA);
            MmProgressDialog.show(this);
            APIManager.getInstance().getSearchService().searchStyleCode(styleId)
                    .enqueue(new MmCallBack<SearchResponse>(ProductDetailPageActivity.this) {
                        @Override
                        public void onSuccess(Response<SearchResponse> response, Retrofit retrofit) {
                            MmProgressDialog.dismiss();
                            style = response.body().getPageData().get(0);
                            initUI();
                        }
                    });
        }
    }

    private void initUI() {
        initTitleImage();
        renderListView();
        bindBuyItemView();
    }

    private void initTitleImage() {
        Picasso.with(MyApplication.getContext()).
                load(PathUtil.getBrandImageUrl(style.getBrandHeaderLogoImage())).into(titleImage);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (productDetailListItemAdapter != null && needToRefreshList) {
            productDetailListItemAdapter.notifyDataSetChanged();
        }
        blurLayer.setVisibility(View.GONE);
        getSupportActionBar().show();

        this.invalidateOptionsMenu();
    }

    public void renderListView() {
        productDetailListItemList = new ArrayList<>();
        productDetailListItemList.add(new ProductDetailListItem("image default", Boolean.toString(liked), ProductDetailListItem.ItemType.TYPE_IMAGE_DEFAULT));
        productDetailListItemList.add(new ProductDetailListItem("name", "", ProductDetailListItem.ItemType.TYPE_NAME));
        productDetailListItemList.add(new ProductDetailListItem("color", "", ProductDetailListItem.ItemType.TYPE_COLOR));
        productDetailListItemList.add(new ProductDetailListItem("size", "", ProductDetailListItem.ItemType.TYPE_SIZE));
        productDetailListItemList.add(new ProductDetailListItem("Comment", "", ProductDetailListItem.ItemType.TYPE_COMMENT));
        productDetailListItemList.add(new ProductDetailListItem("Recommend", "", ProductDetailListItem.ItemType.TYPE_RECOMMEND));
        productDetailListItemList.add(new ProductDetailListItem("Des", "", ProductDetailListItem.ItemType.TYPE_DESC));
        productDetailListItemList.add(new ProductDetailListItem("Desc Image", "", ProductDetailListItem.ItemType.TYPE_DESC_IMAGE));
        productDetailListItemList.add(new ProductDetailListItem("outfit", "", ProductDetailListItem.ItemType.TYPE_OUTFIT));
        productDetailListItemList.add(new ProductDetailListItem("Suggestion", "", ProductDetailListItem.ItemType.TYPE_SUGGESTION));
        productDetailListItemList.add(new ProductDetailListItem("score", "", ProductDetailListItem.ItemType.TYPE_SCORE));
        productDetailListItemAdapter = new ProductDetailRvAdapter(this, productDetailListItemList, style);

        recyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));

        recyclerView.setAdapter(productDetailListItemAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product_detail, menu);

        //for heart icon
        MenuItem item = menu.findItem(R.id.action_like);
        MenuItemCompat.setActionView(item, R.layout.badge_button_like);

        RelativeLayout badgeLayout = (RelativeLayout) MenuItemCompat.getActionView(item);
        icon_heart_fill = (View) badgeLayout.findViewById(R.id.icon_heart_fill);
        View badge = (View) badgeLayout.findViewById(R.id.btnBadge);

        if (CacheManager.getInstance().isWishlistHasItem()) {
            badge.setVisibility(View.VISIBLE);
        } else {
            badge.setVisibility(View.GONE);
        }

        item.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductDetailPageActivity.this, WishListActivity.class);
                startActivity(i);
            }
        });


        //for cart icon
        MenuItem cartItem = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(cartItem, R.layout.badge_button_cart);

        RelativeLayout badgeCartLayout = (RelativeLayout) MenuItemCompat.getActionView(cartItem);
        View cartBadge = (View) badgeCartLayout.findViewById(R.id.btnBadge);

        if (CacheManager.getInstance().isCartHasItem()) {
            cartBadge.setVisibility(View.VISIBLE);
        } else {
            cartBadge.setVisibility(View.GONE);
        }

        cartItem.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductDetailPageActivity.this, ShoppingCartActivity.class);
                startActivity(i);
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cart) {
            Intent i = new Intent(this, ShoppingCartActivity.class);
            startActivity(i);
        } else if (id == R.id.action_like) {
            Intent i = new Intent(this, WishListActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        ProductDetailListItem defaultImageItemData = productDetailListItemAdapter.getItemsData().get(0);
        Intent intent = getIntent();
        intent.putExtra(Constant.Extra.EXTRA_LIKED, Boolean.parseBoolean(defaultImageItemData.getContent()));
        setResult(RESULT_OK, intent);
        finish();
    }

    private void bindBuyItemView() {

        BuyButtonSlidingDrawer.ScrollEventCallBack scrollEventCallBack = new BuyButtonSlidingDrawer.ScrollEventCallBack() {
            @Override
            public void onScrollStarted() {
                imageViewIm.setVisibility(View.GONE);
            }

            @Override
            public void onScrollEnd(boolean opened) {
                if (!opened) {
                    imageViewIm.setVisibility(View.VISIBLE);
                }
            }
        };


        buyButtonSlidingDrawer.bindBuyItemView(new BuyButtonSlidingDrawer.BuyOpenedCallBack() {
            @Override
            public void drawerOpened(Activity context, boolean isSwipe) {
                proceedCheckout(context, buyButtonSlidingDrawer, isSwipe);
            }
        }, style.getPrice(), this, scrollEventCallBack);

    }

    public void showCheckoutScreen(final Activity context, final BuyButtonSlidingDrawer buyButton, final boolean isSwipe) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(context, CheckoutActivity.class);
                intent.putExtra(CheckoutActivity.EXTRA_PRODUCT_DATA, style);
                intent.putExtra(CheckoutActivity.ADD_PRODUCT_TO_CART_FLAG, true);
                intent.putExtra(CheckoutActivity.SWIPE_MODE, isSwipe);
                intent.putExtra(CheckoutActivity.SELECTED_DATA_KEY, productDetailListItemAdapter.getProductDetailSelectionCollection());
                context.startActivityForResult(intent, CheckoutActivity.CHECK_OUT_REQUEST);

                context.overridePendingTransition(R.anim.bottom_to_top, R.anim.not_move);

                if (buyButton != null) {
                    buyButton.closeSliding();
                    imageViewIm.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void proceedCheckout(final Activity context, final BuyButtonSlidingDrawer buyButton, boolean isSwipe) {
        if (!directCheckout(isSwipe)) {

            showCheckoutScreen(context, buyButton, isSwipe);

        } else {
            if (buyButton != null) {
                buyButton.closeSliding();
                imageViewIm.setVisibility(View.VISIBLE);
            }
        }
    }

    public void animateHeartIcon() { //in action bar
        Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zoom_in_heart);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoom_out_heart);

        icon_heart_fill.setVisibility(View.VISIBLE);
        icon_heart_fill.startAnimation(zoomin);
        zoomin.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                icon_heart_fill.startAnimation(zoomout);
                zoomout.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        icon_heart_fill.setVisibility(View.GONE);
                        ProductDetailPageActivity.this.invalidateOptionsMenu();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private boolean directCheckout(boolean isSwipe) {
        selectedColor = null;
        selectedSize = null;

        ProductDetailSelectionCollection productDetailSelectionCollection = productDetailListItemAdapter.getProductDetailSelectionCollection();

        if (productDetailSelectionCollection.getSelectedColor() != null) {
            selectedColor = productDetailSelectionCollection.getSelectedColor().getT();
        }

        if (productDetailSelectionCollection.getSelectedSize() != null) {
            selectedSize = productDetailSelectionCollection.getSelectedSize().getT();
        }

        if (style.getColorList().size() != 0 && style.getSizeList().size() != 0 && selectedColor != null && selectedSize != null && !isSwipe) {
            handleAddCartAction();
            return true;
        }

        if (style.getColorList().size() == 0 && selectedSize != null && !isSwipe) {
            handleAddCartAction();
            return true;
        }

        if (style.getSizeList().size() == 0 && selectedColor != null && !isSwipe) {
            handleAddCartAction();
            return true;
        }

        return false;
    }

    private void handleAddCartAction() {
        Sku sku = style.getSku(selectedSize, selectedColor);

        if (sku == null) {
            MmProgressDialog.dismiss();
            ErrorUtil.showErrorDialog(ProductDetailPageActivity.this, getResources().getString(R.string.LB_MC_COLORS_SIZE_TITLE));
            return;
        }

        APIManager.getInstance().getCartService().addCartItem(new CartAddRequest(LanguageManager.getInstance().getCurrentCultureCode(), MmGlobal.getUserKeyForCartRequest(), MmGlobal.getCartKeyForCartRequest(), sku.getSkuId(), 1))
                .enqueue(new MmCallBack<Cart>(ProductDetailPageActivity.this) {
                    @Override
                    public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                        MmProgressDialog.dismiss();
                        Cart cart = response.body();

                        CacheManager.getInstance().setCart(cart);
                        MmGlobal.setAnonymousShoppingCartKey(cart.getCartKey());

                        MmStatusAlertUtil.show(ProductDetailPageActivity.this, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                getString(R.string.LB_CA_ADD2CART_SUCCESS), null);
                        ProductDetailPageActivity.this.invalidateOptionsMenu();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        super.onFailure(t);
                        MmStatusAlertUtil.show(ProductDetailPageActivity.this, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                getString(R.string.LB_CA_ADD2CART_FAIL), null);
                    }
                });
    }

    public void setScrollable(boolean enable) {
        recyclerView.requestDisallowInterceptTouchEvent(!enable);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CheckoutActivity.CHECK_OUT_REQUEST && resultCode == RESULT_OK){
            needToRefreshList = false;
        }
        else{
            needToRefreshList = true;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
