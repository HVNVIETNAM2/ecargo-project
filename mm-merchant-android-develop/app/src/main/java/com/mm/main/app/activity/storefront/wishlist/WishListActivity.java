package com.mm.main.app.activity.storefront.wishlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.cart.ShoppingCartActivity;
import com.mm.main.app.adapter.strorefront.wishlist.WishListRVAdapter;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.WishlistRvItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.schema.CartItem;
import com.mm.main.app.schema.WishList;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class WishListActivity extends AppCompatActivity {

    @Bind(R.id.txtTitle)
    TextView txtTitle;

    @Bind(R.id.wish_list)
    RecyclerView mRecyclerView;

    Toolbar toolbar;
    WishListRVAdapter mAdapter;
    List<WishlistRvItem> items;

    @Bind(R.id.blurLayer)
    public View blurLayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        ButterKnife.bind(this);
        addIdForAutomationTest();
        setupToolbar();
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        items = new ArrayList<>();
    }

    private void updateData() {

        if (MmGlobal.isLogin()) {
            APIManager.getInstance().getWishListService().viewWishListByUser(MmGlobal.getUserKey())
                    .enqueue(new MmCallBack<WishList>(WishListActivity.this) {
                        @Override
                        public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            super.onFailure(t);
                        }

                        @Override
                        public void onResponse(Response<WishList> response, Retrofit retrofit) {
                            WishList list = response.body();
                            try {
                                loadDataForList(list);
                                //RecyclerView.Adapter
                                mAdapter = new WishListRVAdapter(WishListActivity.this, items, "0");
                                //Apply this adapter to the RecyclerView
                                mRecyclerView.setAdapter(mAdapter);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
        } else {

            final String cartKey = MmGlobal.anonymousWishListKey();

            if (cartKey.isEmpty()) {
                //TODO
            } else {
                MmProgressDialog.show(WishListActivity.this);
                APIManager.getInstance().getWishListService().viewWishList(cartKey)
                        .enqueue(new MmCallBack<WishList>(WishListActivity.this) {
                            @Override
                            public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                                MmProgressDialog.dismiss();
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                super.onFailure(t);
                                MmProgressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(Response<WishList> response, Retrofit retrofit) {
                                WishList list = response.body();
                                try {
                                    loadDataForList(list);
                                    //RecyclerView.Adapter
                                    mAdapter = new WishListRVAdapter(WishListActivity.this, items, list.getCartKey());
                                    //Apply this adapter to the RecyclerView
                                    mRecyclerView.setAdapter(mAdapter);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                MmProgressDialog.dismiss();
                            }
                        });
            }
        }

    }

    public void loadDataForList(WishList wishlist) {
        items.clear();

        List<CartItem> products = wishlist.getCartItems();

        for (CartItem product : products) {
            WishlistRvItem productItem = new WishlistRvItem(product);
            items.add(productItem);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_discover, menu);

        //for heart icon
        MenuItem item = menu.findItem(R.id.action_like);
        item.setVisible(false);

        //for cart icon
        MenuItem cartItem = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(cartItem, R.layout.badge_button_cart);

        RelativeLayout badgeCartLayout = (RelativeLayout) MenuItemCompat.getActionView(cartItem);
        View cartBadge = (View) badgeCartLayout.findViewById(R.id.btnBadge);

        if (CacheManager.getInstance().isCartHasItem()) {
            cartBadge.setVisibility(View.VISIBLE);
        } else {
            cartBadge.setVisibility(View.GONE);
        }

        cartItem.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WishListActivity.this, ShoppingCartActivity.class);
                startActivity(i);
            }
        });

        cartItem.getActionView().setContentDescription("UIBT_CART");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cart) {
            Intent i = new Intent(this, ShoppingCartActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        blurLayer.setVisibility(View.GONE);
        getSupportActionBar().show();

        this.invalidateOptionsMenu();
        updateData();
    }

    private void addIdForAutomationTest() {
        txtTitle.setContentDescription(getContentDescription("UILB_WISHLIST"));
    }

    public String getContentDescription(String key) {
        return String.format("WishList-%s", key);
    }
}
