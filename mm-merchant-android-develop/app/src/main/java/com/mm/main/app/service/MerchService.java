package com.mm.main.app.service;

import com.mm.main.app.schema.Merchant;
import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by andrew on 10/11/15.
 */
public interface MerchService {
    String MERCH_PATH = "merch";

    @GET(MERCH_PATH + "/view")
    Call<Merchant> view();

}
