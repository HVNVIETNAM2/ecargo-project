package com.mm.main.app.listitem;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class CheckoutConfirmMerchantTotalPriceRvItem implements CheckoutConfirmRvItem {

    double totalPrice = 0;
    CheckoutConfirmMerchantRvItem merchant;

    public CheckoutConfirmMerchantTotalPriceRvItem(CheckoutConfirmMerchantRvItem merchant){
        this.merchant = merchant;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public CheckoutConfirmMerchantRvItem getMerchant() {
        return merchant;
    }

    public void setMerchant(CheckoutConfirmMerchantRvItem merchant) {
        this.merchant = merchant;
    }

    @Override
    public ItemType getType() {
        return ItemType.TYPE_MERCHANT_TOTAL_PRICE;
    }
}
