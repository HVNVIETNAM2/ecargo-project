package com.mm.main.app.adapter.strorefront.animation;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.mm.main.app.helper.ViewHelper;

import java.util.WeakHashMap;


/**
 * Copyright (C) 2015 Wasabeef
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public abstract class AnimationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private RecyclerView.Adapter<RecyclerView.ViewHolder> mAdapter;
  private int mDuration = 500;
  private Interpolator mInterpolator = new LinearInterpolator();
  private int mLastPosition = -1;
  private int count = 0;

  private WeakHashMap<RecyclerView.ViewHolder, Integer> mHolders = new WeakHashMap<>();

  private boolean isFirstOnly = true;

  public AnimationAdapter(RecyclerView.Adapter<RecyclerView.ViewHolder> adapter) {
    mAdapter = adapter;
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return mAdapter.onCreateViewHolder(parent, viewType);
  }

  @Override public void registerAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
    super.registerAdapterDataObserver(observer);
    mAdapter.registerAdapterDataObserver(observer);
  }

  @Override public void unregisterAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
    super.unregisterAdapterDataObserver(observer);
    mAdapter.unregisterAdapterDataObserver(observer);
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    mAdapter.onBindViewHolder(holder, position);

    if (!isFirstOnly || position > mLastPosition || !Integer.valueOf(position).equals(mHolders.get(holder))) {
        for (Animator anim : getAnimators(holder.itemView)) {
          anim.setDuration(mDuration).start();
          anim.setInterpolator(mInterpolator);
        }
      mHolders.put(holder, position);
    } else {
      ViewHelper.clear(holder.itemView);
    }
  }

    public void centerAnimation(View itemView) {
        for (Animator anim : getCenterAnimators(itemView)) {
            anim.setDuration(mDuration).start();
            anim.setInterpolator(mInterpolator);
        }
    }

    public void reverseCenterAnimation(View itemView) {
        for (Animator anim : getReverseCenterAnimators(itemView)) {
            anim.setDuration(mDuration).start();
            anim.setInterpolator(mInterpolator);
        }
    }

    public void nearCenterAnimation(View itemView, float from, float to) {
        for (Animator anim : getNearCenterAnimators(itemView, from, to)) {
            anim.setDuration(mDuration).start();
            anim.setInterpolator(mInterpolator);
        }
    }

  @Override public int getItemCount() {
    return mAdapter.getItemCount();
  }

  public void setDuration(int duration) {
    mDuration = duration;
  }

  public void setInterpolator(Interpolator interpolator) {
    mInterpolator = interpolator;
  }

  public void setStartPosition(int start) {
    mLastPosition = start;
  }

  protected abstract Animator[] getAnimators(View view);

  protected abstract Animator[] getCenterAnimators(View view);

  public void setFirstOnly(boolean firstOnly) {
    isFirstOnly = firstOnly;
  }

  @Override public int getItemViewType(int position) {
    return mAdapter.getItemViewType(position);
  }

  public RecyclerView.Adapter<RecyclerView.ViewHolder> getWrappedAdapter() {
    return mAdapter;
  }
  
  @Override
  public long getItemId(int position) {
        return mAdapter.getItemId(position);
  }

    protected Animator[] getReverseCenterAnimators(View view) {
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", 1f, 0.6f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", 1f, 0.6f);
        return new ObjectAnimator[] { scaleX, scaleY };
    }

    protected Animator[] getNearCenterAnimators(View view, float from, float to) {
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", from, to);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", from, to);
        return new ObjectAnimator[] { scaleX, scaleY };
    }

}
