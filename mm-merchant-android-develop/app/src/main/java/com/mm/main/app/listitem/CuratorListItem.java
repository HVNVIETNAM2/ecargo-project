package com.mm.main.app.listitem;

import com.mm.main.app.schema.User;
/**
 * Created by yendangn on 2/15/2016.
 */
public class CuratorListItem {
    private User Curator;
    private Boolean IsSelected;
    private Boolean IsCenter;

    public CuratorListItem(){

    }

    public CuratorListItem(User curator){
        this.Curator = curator;
        this.IsSelected = false;
        this.IsCenter = false;
    }

    public User getCurator() {
        return Curator;
    }

    public void setCurator(User curator) {
        this.Curator = curator;
    }

    public Boolean getIsSelected() {
        return IsSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        IsSelected = isSelected;
    }

    public Boolean getIsCenter() {
        return IsCenter;
    }

    public void setIsCenter(Boolean isCenter) {
        IsCenter = isCenter;
    }

}
