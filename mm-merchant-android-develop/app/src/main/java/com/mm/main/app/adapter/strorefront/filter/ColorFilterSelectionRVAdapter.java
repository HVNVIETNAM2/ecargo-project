package com.mm.main.app.adapter.strorefront.filter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Color;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nguyennguyent1 on 1/21/2016.
 */
public class ColorFilterSelectionRVAdapter extends RecyclerView.Adapter<ColorFilterSelectionRVAdapter.ColorFilterViewHolder> implements Filterable {
    private List<FilterListItem<Color>> filteredData;
    private final Activity context;
    private List<FilterListItem<Color>> originalData;
    private Integer selectedItemId;
    private ItemFilter mFilter = new ItemFilter();
    private int borderWidthSelected;
    private int borderNotSelected;

    public ColorFilterSelectionRVAdapter(Activity context, List<FilterListItem<Color>> itemList, Integer selectedItemId) {
        this.context = context;
        this.originalData = itemList;
        this.filteredData = itemList;
        this.selectedItemId = selectedItemId;
        this.borderNotSelected = UiUtil.getPixelFromDp(1);
        this.borderWidthSelected = UiUtil.getPixelFromDp(2);
    }

    @Override
    public ColorFilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.color_selection_list_item, parent, false);
        ColorFilterViewHolder colorFilterViewHolder = new ColorFilterViewHolder(view);
        return colorFilterViewHolder;
    }


    @Override
    public void onBindViewHolder(final ColorFilterViewHolder colorFilterViewHolder, final int position) {
        final FilterListItem<Color> currentItem = filteredData.get(position);

        colorFilterViewHolder.textView.setText(currentItem.getT().getColorName());
        if (!TextUtils.isEmpty(currentItem.getT().getColorImage())) {
            Picasso.with(MyApplication.getContext()).load(PathUtil.getColorImageUrl(currentItem.getT().getColorImage())).into(colorFilterViewHolder.imageViewSimple);
        } else {
            colorFilterViewHolder.imageViewSimple.setImageResource(R.drawable.color_circle);
        }
        setBorderColor(currentItem.isSelected(), colorFilterViewHolder.imageViewSimple);
        colorFilterViewHolder.imageViewSimple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentItem.setSelected(!currentItem.isSelected());
                setBorderColor(currentItem.isSelected(), colorFilterViewHolder.imageViewSimple);
            }
        });
    }

    private void setBorderColor(boolean isSelected, CircleImageView circleImageView){
        if(isSelected){
            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.black));
            circleImageView.setBorderWidth(borderWidthSelected);
        }
        else{
            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.secondary1));
            circleImageView.setBorderWidth(borderNotSelected);
        }
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }
    @Override
    public int getItemViewType(int position) {
        return 0;
    }
    @Override
    public long getItemId(int position){
        return position;
    }

    public Object getItem(int position){
        return filteredData.get(position);
    }


    public class ColorFilterViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView imageViewSimple;
        TextView textView;

        public ColorFilterViewHolder(View itemView) {
            super(itemView);
            imageViewSimple = (CircleImageView) itemView.findViewById(R.id.imageSimple);
            textView = (TextView) itemView.findViewById(R.id.label);
        }
    }

    public Integer getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(Integer selectedItemId) {
        if (selectedItemId >= 0) {
            filteredData.get(selectedItemId).setSelected(!filteredData.get(selectedItemId).isSelected());
        }
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString;
            filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<FilterListItem<Color>> list = originalData;

            int count = list.size();
            final ArrayList<FilterListItem<Color>> nlist = new ArrayList<>(count);

            String filterableString ;

            if (!TextUtils.isEmpty(constraint)) {
                for (int i = 0; i < count; i++) {
                    filterableString = "" + list.get(i).getT().getColorName();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        FilterListItem<Color> mYourCustomData = list.get(i);
                        nlist.add(mYourCustomData);
                    }
                }
            } else {
                for (int i = 0; i < count; i++) {
                    FilterListItem<Color> mYourCustomData = list.get(i);
                    nlist.add(mYourCustomData);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<FilterListItem<Color>>) results.values;
            notifyDataSetChanged();
        }
    }
    public List<FilterListItem<Color>> getOriginalData() {
        return originalData;
    }

    public void setOriginalData(List<FilterListItem<Color>> originalData) {
        this.originalData = originalData;
    }
}
