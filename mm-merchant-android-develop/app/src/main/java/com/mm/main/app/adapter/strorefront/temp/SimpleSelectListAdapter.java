package com.mm.main.app.adapter.strorefront.temp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;

import java.util.List;

/**
 * Created by henrytung on 4/11/15.
 */
public class SimpleSelectListAdapter extends BaseAdapter {

    private final Activity context;
    private final List<String> itemList;

    public SimpleSelectListAdapter(Activity context, List<String> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.simple_selection_list_item, null, true);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.label);
        textView.setText(itemList.get(position));

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        if (position == 1) {
           imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ico_tick));
        } else {
            imageView.setImageDrawable(null);
        }

        return convertView;
    }
}
