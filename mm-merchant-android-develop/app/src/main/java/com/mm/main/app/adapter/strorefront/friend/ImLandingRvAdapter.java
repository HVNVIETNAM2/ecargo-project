package com.mm.main.app.adapter.strorefront.friend;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.library.swipemenu.SwipeDefaultActionListener;
import com.mm.main.app.library.swipemenu.SwipeLayout;
import com.mm.main.app.library.swipemenu.adapters.RecyclerSwipeAdapter;
import com.mm.main.app.schema.Conv;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by haivu on 3/3/2016.
 */
public class ImLandingRvAdapter extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    public interface LandingCallBack {
        void delete(int position);
        void goChatRoom(int position);
    }

    private Context context;
    private ArrayList<Conv> convs;
    private ArrayList<User> users;
    private LandingCallBack callBack;

    public ImLandingRvAdapter(Context context, ArrayList<Conv> convs, ArrayList<User> users, LandingCallBack callBack) {
        this.context = context;
        this.convs = convs;
        this.users = users;
        this.callBack = callBack;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.im_landing_item, parent, false);
        viewHolder = new ImLandingItemViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        Conv conv = convs.get(position);
        User user = users.get(position);
        int width = (UiUtil.getDisplayWidth() - UiUtil.getPixelFromDp(15)) / 2;

        final ImLandingItemViewHolder landingItemViewHolder = (ImLandingItemViewHolder) viewHolder;

        landingItemViewHolder.tvUserName.setText(user.getDisplayName());
        landingItemViewHolder.tvChatContent.setText(conv.getConvKey());
        landingItemViewHolder.tvTime.setText("");
        Picasso.with(MyApplication.getContext()).load(PathUtil.getUserImageUrl(user.getProfileImage())).
                resize(width, width)
                .centerCrop().into(landingItemViewHolder.profileImageViewUser);
        landingItemViewHolder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.goChatRoom(position);
            }
        });

        landingItemViewHolder.linearDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItem(position,landingItemViewHolder);
            }
        });

        SwipeLayout swipeLayout = landingItemViewHolder.swipeLayout;
        swipeLayout.setDefaultViewRightID(R.id.linearDelete);
        swipeLayout.addDrag(SwipeLayout.DragEdge.Right, swipeLayout.findViewById(R.id.rightMenu));
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        swipeLayout.setmDefaultActionListener(new SwipeDefaultActionListener() {
            @Override
            public void UpdateDefaultActionUI(boolean isRightMenu) {

            }

            @Override
            public void CancelDefaultActionUI(boolean isRightMenu) {

            }

            @Override
            public void ActionDefault(boolean isRightMenu, SwipeLayout swipeLayout) {
                deleteItem(position,landingItemViewHolder);
            }
        });

        mItemManger.bind(viewHolder.itemView, position);
    }

    private void deleteItem(int position, ImLandingItemViewHolder holder) {
        mItemManger.removeShownLayouts(holder.swipeLayout);
        callBack.delete(position);
    }

    @Override
    public int getItemCount() {
        return convs.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeLayout;
    }

    public static class ImLandingItemViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.swipeLayout)
        SwipeLayout swipeLayout;

        @Bind(R.id.imgProfile)
        ImageView imgProfile;

        @Bind(R.id.profileImageViewUser)
        CircleImageView profileImageViewUser;

        @Bind(R.id.tvUserName)
        TextView tvUserName;

        @Bind(R.id.tvTime)
        TextView tvTime;

        @Bind(R.id.tvChatContent)
        TextView tvChatContent;

        @Bind(R.id.linearDelete)
        LinearLayout linearDelete;

        @Bind(R.id.groupProfileUser)
        RelativeLayout groupProfileUser;

        @Bind(R.id.content)
        RelativeLayout content;


        public ImLandingItemViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
            swipeLayout.setDefaultActionRange(0.75f);
        }
    }
}
