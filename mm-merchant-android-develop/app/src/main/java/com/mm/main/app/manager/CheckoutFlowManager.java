package com.mm.main.app.manager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.mm.main.app.activity.storefront.cart.ShoppingCartActivity;
import com.mm.main.app.activity.storefront.checkout.AddAddressActivity;
import com.mm.main.app.activity.storefront.checkout.CheckoutActivity;
import com.mm.main.app.listitem.ShoppingCartMerchantRvItem;

import java.util.ArrayList;

/**
 * Created by thienchaud on 22-Feb-16.
 */
public class CheckoutFlowManager {

    private static final int DELAY_FINISH_ACTIVITY = 500;

    private static CheckoutFlowManager instance;

    private ArrayList<ShoppingCartMerchantRvItem> selectedShoppingCartData;
    private ArrayList<Activity> activitiesFlow;

    public static CheckoutFlowManager getInstance() {
        if (instance == null) {
            instance = new CheckoutFlowManager();
        }
        return instance;
    }

    public CheckoutFlowManager() {
    }

    public ArrayList<ShoppingCartMerchantRvItem> getSelectedShoppingCartData() {
        return selectedShoppingCartData;
    }

    public void setSelectedShoppingCartData(ArrayList<ShoppingCartMerchantRvItem> selectedShoppingCartData) {
        this.selectedShoppingCartData = selectedShoppingCartData;
    }

    public ArrayList<Activity> getActivitiesFlow() {
        return activitiesFlow;
    }

    public void setActivitiesFlow(ArrayList<Activity> activitiesFlow) {
        this.activitiesFlow = activitiesFlow;
    }

    public void clearActivitiesFlow() {
        activitiesFlow = new ArrayList<Activity>();
    }

    public void addActivityToFlow(Activity activity) {
        if (activitiesFlow == null) {
            activitiesFlow = new ArrayList<Activity>();
        }
        activitiesFlow.add(activity);
    }

    public void actionCompleteLoginRequest() {
        ShoppingCartActivity shoppingCartActivity = (ShoppingCartActivity)activitiesFlow.get(0);
        shoppingCartActivity.showConfirmCheckout();

        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = activitiesFlow.size() - 1; i > 0; i--) {
                    activitiesFlow.get(i).finish();
                }
                clearActivitiesFlow();
            }
        };
        handler.postDelayed(runnable, DELAY_FINISH_ACTIVITY);
    }

    public void actionCompleteSignUpRequest() {
        ShoppingCartActivity shoppingCartActivity = (ShoppingCartActivity)activitiesFlow.get(0);
        shoppingCartActivity.showAddAddress();

        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = activitiesFlow.size() - 1; i > 0; i--) {
                    if (!(activitiesFlow.get(i) instanceof AddAddressActivity)) {
                        activitiesFlow.get(i).finish();
                    }
                }
            }
        };
        handler.postDelayed(runnable, DELAY_FINISH_ACTIVITY);
    }

    public void actionCompleteLoginAtSwipeCheckout() {
        CheckoutActivity checkout = (CheckoutActivity)activitiesFlow.get(0);
        checkout.loadLisItem();

        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = activitiesFlow.size() - 1; i > 0; i--) {
                    activitiesFlow.get(i).finish();
                }
                clearActivitiesFlow();
            }
        };
        handler.postDelayed(runnable, DELAY_FINISH_ACTIVITY);
    }
}
