package com.mm.main.app.schema.request;

/**
 * Created by thienchaud on 25-Jan-16.
 */
public class CartAddRequest {

    String CultureCode;
    String CartKey;
    String UserKey;
    Integer SkuId;
    Integer Qty;

    public CartAddRequest(String cultureCode, String userKey, String cartKey, Integer skuId, Integer qty) {
        CultureCode = cultureCode;
        UserKey = userKey;
        CartKey = cartKey;
        SkuId = skuId;
        Qty = qty;
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getCartKey() {
        return CartKey;
    }

    public void setCartKey(String cartKey) {
        CartKey = cartKey;
    }

    public Integer getSkuId() {
        return SkuId;
    }

    public void setSkuId(Integer skuId) {
        SkuId = skuId;
    }

    public Integer getQty() {
        return Qty;
    }

    public void setQty(Integer qty) {
        Qty = qty;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }
}
