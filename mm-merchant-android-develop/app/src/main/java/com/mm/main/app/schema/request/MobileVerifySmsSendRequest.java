package com.mm.main.app.schema.request;

/**
 * Created by henrytung on 2/2/2016.
 */
public class MobileVerifySmsSendRequest {

    String MobileNumber;
    String MobileCode;

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getMobileCode() {
        return MobileCode;
    }

    public void setMobileCode(String mobileCode) {
        MobileCode = mobileCode;
    }
}
