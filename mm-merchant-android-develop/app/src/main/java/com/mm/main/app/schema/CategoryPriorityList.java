
package com.mm.main.app.schema;

import java.io.Serializable;

public class CategoryPriorityList implements Serializable {

    private Integer CategoryId;
    private String CategoryName;
    private String CategoryNameInvariant;
    private Integer Level;
    private Integer ParentCategoryId;
    private Integer Priority;
    String CategoryCode;
    Integer IsMerchCanSelect;
    String CategoryImage;
    Integer SkuCount;
    Integer StatusId;

    public String getCategoryCode() {
        return CategoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        CategoryCode = categoryCode;
    }

    public Integer getIsMerchCanSelect() {
        return IsMerchCanSelect;
    }

    public void setIsMerchCanSelect(Integer isMerchCanSelect) {
        IsMerchCanSelect = isMerchCanSelect;
    }

    public String getCategoryImage() {
        return CategoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        CategoryImage = categoryImage;
    }

    public Integer getSkuCount() {
        return SkuCount;
    }

    public void setSkuCount(Integer skuCount) {
        SkuCount = skuCount;
    }

    public Integer getStatusId() {
        return StatusId;
    }

    public void setStatusId(Integer statusId) {
        StatusId = statusId;
    }

    /**
     * @return The CategoryId
     */
    public Integer getCategoryId() {
        return CategoryId;
    }

    /**
     * @param CategoryId The CategoryId
     */
    public void setCategoryId(Integer CategoryId) {
        this.CategoryId = CategoryId;
    }

    /**
     * @return The CategoryName
     */
    public String getCategoryName() {
        return CategoryName;
    }

    /**
     * @param CategoryName The CategoryName
     */
    public void setCategoryName(String CategoryName) {
        this.CategoryName = CategoryName;
    }

    /**
     * @return The CategoryNameInvariant
     */
    public String getCategoryNameInvariant() {
        return CategoryNameInvariant;
    }

    /**
     * @param CategoryNameInvariant The CategoryNameInvariant
     */
    public void setCategoryNameInvariant(String CategoryNameInvariant) {
        this.CategoryNameInvariant = CategoryNameInvariant;
    }

    /**
     * @return The Level
     */
    public Integer getLevel() {
        return Level;
    }

    /**
     * @param Level The Level
     */
    public void setLevel(Integer Level) {
        this.Level = Level;
    }

    /**
     * @return The ParentCategoryId
     */
    public Integer getParentCategoryId() {
        return ParentCategoryId;
    }

    /**
     * @param ParentCategoryId The ParentCategoryId
     */
    public void setParentCategoryId(Integer ParentCategoryId) {
        this.ParentCategoryId = ParentCategoryId;
    }

    /**
     * @return The Priority
     */
    public Integer getPriority() {
        return Priority;
    }

    /**
     * @param Priority The Priority
     */
    public void setPriority(Integer Priority) {
        this.Priority = Priority;
    }

}
