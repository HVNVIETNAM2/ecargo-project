package com.mm.main.app.activity.storefront.filter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.filter.CategoryFilterSelectionRVAdapter;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Category;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.view.MmSearchBar;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryFilterSelectionActivity extends AppCompatActivity implements MmSearchBar.MmSearchBarListener {

    public static final String EXTRA_HEADER = "extra_header";
    public static final String EXTRA_SELECTIONS = "selection";

    public static final String EXTRA_RESULT = "result";
    public static final String EXTRA_RESULT_DATA = "resultData";
    public static final String EXTRA_FIRST_SELECTIONS = "first_selection";

    ArrayList<FilterListItem> filterList;

    @Bind(R.id.textViewHeader)
    TextView textViewHeader;

    @Bind(R.id.recyclerViewFilter)
    RecyclerView recyclerViewFilter;

    CategoryFilterSelectionRVAdapter recyclerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeNoActionBar);
        setContentView(R.layout.activity_filter_category_selection);
        ButterKnife.bind(this);
        setupToolbar();

        Bundle bundle = (Bundle)getIntent().getBundleExtra(EXTRA_SELECTIONS);

        boolean isFirstTime = getIntent().getBooleanExtra(EXTRA_FIRST_SELECTIONS, false);
        renderFilterList((ArrayList<FilterListItem>) bundle.getSerializable(FilterActivity.EXTRA_TEXT_DATA));

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        textViewHeader.setText(getIntent().getStringExtra(EXTRA_HEADER));
        // TODO setupSearchView();

        if(isFirstTime) {
            setSelection();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);
    }

    private void setSelection() {

        for (int i = 0; i < filterList.size(); i++) {
            filterList.get(i).setSelected(false);
        }
        recyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void finish() {
        super.finish();
    }

    private void renderFilterList(ArrayList<FilterListItem> filters) {
        filterList = createCategoryListItem(filters);
        recyclerAdapter = new CategoryFilterSelectionRVAdapter(this, filterList, -1);
        recyclerViewFilter.setAdapter(recyclerAdapter);

        GridLayoutManager manager = new GridLayoutManager(this, 40);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return recyclerAdapter.getItemSpan(position);
            }
        });

        recyclerViewFilter.setHasFixedSize(true);
        recyclerViewFilter.setLayoutManager(manager);

        recyclerAdapter.notifyDataSetChanged();

    }

    @OnClick(R.id.buttonOk)
    protected void proceedOk() {
        ActivityUtil.closeKeyboard(this);
        Intent intent = this.getIntent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_RESULT_DATA, createResultList());
        intent.putExtra(EXTRA_RESULT, bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    private ArrayList<Category> createResultList() {
        ArrayList<FilterListItem> filterListItemList = recyclerAdapter.getOriginalData();
        ArrayList<Category> resultList = new ArrayList<>();
        for (int i = 0; i < filterListItemList.size(); i++) {
            FilterListItem item = filterListItemList.get(i);

            if (item.getT() instanceof Category &&  ((Category)item.getT()).getIsSelected()) {
                ((Category) item.getT()).setIsSelected(true);
                resultList.add((Category)item.getT());
            }
        }

        return resultList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_brand_filter_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_ok) {
            proceedOk();
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reset) {
            resetSelection();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEnterText(CharSequence s) {
        recyclerAdapter.getFilter().filter(s);
    }

    @Override
    public void onCancelSearch() {

    }

    private ArrayList<FilterListItem> createCategoryListItem(ArrayList<FilterListItem> fullCategoryList) {
        // add header item
        ArrayList<FilterListItem> listCategoryFilter = new ArrayList<>();

        for (FilterListItem item: fullCategoryList) {
            Category categoryItem = ((Category) item.getT());
            listCategoryFilter.add(new FilterListItem(categoryItem.getCategoryName(), false, FilterListItem.ItemType.TYPE_FILTER_TITLE));
            int count = categoryItem.getCategoryList().size();
            for (int i = 0; i < count; i++) {
                Category subCategory = categoryItem.getCategoryList().get(i);
                FilterListItem subCategoryItem = new FilterListItem(subCategory);
                subCategoryItem.setType(FilterListItem.ItemType.TYPE_FILTER_LIST);
                if (subCategory.getIsSelected()) {
                    subCategoryItem.setSelected(true);
                } else {
                    subCategoryItem.setSelected(false);
                }

                listCategoryFilter.add(subCategoryItem);
            }
        }
        return listCategoryFilter;
    }

    private void resetSelection() {
        for (int i = 0; i < filterList.size(); i++) {
            if(filterList.get(i).getT() instanceof Category){
                ((Category)filterList.get(i).getT()).setIsSelected(false);
            }
        }
        recyclerAdapter.notifyDataSetChanged();
    }

}
