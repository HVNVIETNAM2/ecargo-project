
package com.mm.main.app.schema;


import java.io.Serializable;
import java.util.Date;

public class Sku implements Serializable{

    private Integer SkuId;
    private String StyleCode;
    private String Upc;
    private String Bar;
    private Integer BrandId;
    private Integer BadgeId;
    private Integer SeasonId;
    private Integer SizeId;
    private Integer ColorId;
    private Integer GeoCountryId;
    private Integer LaunchYear;
    private String ManufacturerName;
    private Double PriceRetail;
    private Double PriceSale;
    private String SaleFrom;
    private String SaleTo;
    private String AvailableFrom;
    private String AvailableTo;
    private String WeightKg;
    private String HeightCm;
    private String WidthCm;
    private String LengthCm;
    private Integer StatusId;
    private Integer MerchantId;
    private String ColorKey;
    private Object SkuNameInvariant;
    private Object SkuDescInvariant;
    private Object SkuFeatureInvariant;
    private Object SkuMaterialInvariant;
    private Object SkuColorInvariant;
    private String LastModified;
    private String BrandName;
    private String SizeName;
    private String ColorName;
    private String SkuName;
    private String SkuColor;
    private String SkuDesc;
    private String SkuMaterial;
    private String SkuFeature;
    private Integer PrimaryCategoryId;
    private String StatusName;
    Integer IsNew;
    Integer IsSale;
    Date LastCreated;
    Integer QtySafetyThreshold;
    String ColorCode;
    String ColorImage;
    Integer LocationCount;
    Integer QtyAts;
    Integer InventoryStatusId;


    public Integer getIsNew() {
        return IsNew;
    }

    public void setIsNew(Integer isNew) {
        IsNew = isNew;
    }

    public Integer getIsSale() {
        return IsSale;
    }

    public void setIsSale(Integer isSale) {
        IsSale = isSale;
    }

    public Date getLastCreated() {
        return LastCreated;
    }

    public void setLastCreated(Date lastCreated) {
        LastCreated = lastCreated;
    }

    public Integer getQtySafetyThreshold() {
        return QtySafetyThreshold;
    }

    public void setQtySafetyThreshold(Integer qtySafetyThreshold) {
        QtySafetyThreshold = qtySafetyThreshold;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public String getColorImage() {
        return ColorImage;
    }

    public void setColorImage(String colorImage) {
        ColorImage = colorImage;
    }

    public Integer getLocationCount() {
        return LocationCount;
    }

    public void setLocationCount(Integer locationCount) {
        LocationCount = locationCount;
    }

    public Integer getQtyAts() {
        return QtyAts;
    }

    public void setQtyAts(Integer qtyAts) {
        QtyAts = qtyAts;
    }

    public Integer getInventoryStatusId() {
        return InventoryStatusId;
    }

    public void setInventoryStatusId(Integer inventoryStatusId) {
        InventoryStatusId = inventoryStatusId;
    }

    /**
     * 
     * @return
     *     The SkuId
     */
    public Integer getSkuId() {
        return SkuId;
    }

    /**
     * 
     * @param SkuId
     *     The SkuId
     */
    public void setSkuId(Integer SkuId) {
        this.SkuId = SkuId;
    }

    /**
     * 
     * @return
     *     The StyleCode
     */
    public String getStyleCode() {
        return StyleCode;
    }

    /**
     * 
     * @param StyleCode
     *     The StyleCode
     */
    public void setStyleCode(String StyleCode) {
        this.StyleCode = StyleCode;
    }

    /**
     * 
     * @return
     *     The Upc
     */
    public String getUpc() {
        return Upc;
    }

    /**
     * 
     * @param Upc
     *     The Upc
     */
    public void setUpc(String Upc) {
        this.Upc = Upc;
    }

    /**
     * 
     * @return
     *     The Bar
     */
    public String getBar() {
        return Bar;
    }

    /**
     * 
     * @param Bar
     *     The Bar
     */
    public void setBar(String Bar) {
        this.Bar = Bar;
    }

    /**
     * 
     * @return
     *     The BrandId
     */
    public Integer getBrandId() {
        return BrandId;
    }

    /**
     * 
     * @param BrandId
     *     The BrandId
     */
    public void setBrandId(Integer BrandId) {
        this.BrandId = BrandId;
    }

    /**
     * 
     * @return
     *     The BadgeId
     */
    public Integer getBadgeId() {
        return BadgeId;
    }

    /**
     * 
     * @param BadgeId
     *     The BadgeId
     */
    public void setBadgeId(Integer BadgeId) {
        this.BadgeId = BadgeId;
    }

    /**
     * 
     * @return
     *     The SeasonId
     */
    public Integer getSeasonId() {
        return SeasonId;
    }

    /**
     * 
     * @param SeasonId
     *     The SeasonId
     */
    public void setSeasonId(Integer SeasonId) {
        this.SeasonId = SeasonId;
    }

    /**
     * 
     * @return
     *     The SizeId
     */
    public Integer getSizeId() {
        return SizeId;
    }

    /**
     * 
     * @param SizeId
     *     The SizeId
     */
    public void setSizeId(Integer SizeId) {
        this.SizeId = SizeId;
    }

    /**
     * 
     * @return
     *     The ColorId
     */
    public Integer getColorId() {
        return ColorId;
    }

    /**
     * 
     * @param ColorId
     *     The ColorId
     */
    public void setColorId(Integer ColorId) {
        this.ColorId = ColorId;
    }

    /**
     * 
     * @return
     *     The GeoCountryId
     */
    public Integer getGeoCountryId() {
        return GeoCountryId;
    }

    /**
     * 
     * @param GeoCountryId
     *     The GeoCountryId
     */
    public void setGeoCountryId(Integer GeoCountryId) {
        this.GeoCountryId = GeoCountryId;
    }

    /**
     * 
     * @return
     *     The LaunchYear
     */
    public Integer getLaunchYear() {
        return LaunchYear;
    }

    /**
     * 
     * @param LaunchYear
     *     The LaunchYear
     */
    public void setLaunchYear(Integer LaunchYear) {
        this.LaunchYear = LaunchYear;
    }

    /**
     * 
     * @return
     *     The ManufacturerName
     */
    public String getManufacturerName() {
        return ManufacturerName;
    }

    /**
     * 
     * @param ManufacturerName
     *     The ManufacturerName
     */
    public void setManufacturerName(String ManufacturerName) {
        this.ManufacturerName = ManufacturerName;
    }

    public Double getPriceRetail() {
        return PriceRetail;
    }

    public void setPriceRetail(Double priceRetail) {
        PriceRetail = priceRetail;
    }

    public Double getPriceSale() {
        return PriceSale;
    }

    public void setPriceSale(Double priceSale) {
        PriceSale = priceSale;
    }

    /**
     * 
     * @return
     *     The SaleFrom
     */
    public String getSaleFrom() {
        return SaleFrom;
    }

    /**
     * 
     * @param SaleFrom
     *     The SaleFrom
     */
    public void setSaleFrom(String SaleFrom) {
        this.SaleFrom = SaleFrom;
    }

    /**
     * 
     * @return
     *     The SaleTo
     */
    public String getSaleTo() {
        return SaleTo;
    }

    /**
     * 
     * @param SaleTo
     *     The SaleTo
     */
    public void setSaleTo(String SaleTo) {
        this.SaleTo = SaleTo;
    }

    /**
     * 
     * @return
     *     The AvailableFrom
     */
    public String getAvailableFrom() {
        return AvailableFrom;
    }

    /**
     * 
     * @param AvailableFrom
     *     The AvailableFrom
     */
    public void setAvailableFrom(String AvailableFrom) {
        this.AvailableFrom = AvailableFrom;
    }

    /**
     * 
     * @return
     *     The AvailableTo
     */
    public String getAvailableTo() {
        return AvailableTo;
    }

    /**
     * 
     * @param AvailableTo
     *     The AvailableTo
     */
    public void setAvailableTo(String AvailableTo) {
        this.AvailableTo = AvailableTo;
    }

    /**
     * 
     * @return
     *     The WeightKg
     */
    public Integer getWeightKg() {
        return  Integer.parseInt(WeightKg);
    }

    /**
     * 
     * @param WeightKg
     *     The WeightKg
     */
    public void setWeightKg(Integer WeightKg) {
        this.WeightKg = Integer.toString(WeightKg);
    }

    /**
     * 
     * @return
     *     The HeightCm
     */
    public Integer getHeightCm() {
        return Integer.parseInt(HeightCm);
    }

    /**
     * 
     * @param HeightCm
     *     The HeightCm
     */
    public void setHeightCm(Integer HeightCm) {
        this.HeightCm = Integer.toString(HeightCm);
    }

    /**
     * 
     * @return
     *     The WidthCm
     */
    public Integer getWidthCm() {
        return Integer.parseInt(WidthCm);
    }

    /**
     * 
     * @param WidthCm
     *     The WidthCm
     */
    public void setWidthCm(Integer WidthCm) {
        this.WidthCm = Integer.toString(WidthCm);
    }

    /**
     * 
     * @return
     *     The LengthCm
     */
    public Integer getLengthCm() {
        return Integer.parseInt(LengthCm);
    }

    /**
     * 
     * @param LengthCm
     *     The LengthCm
     */
    public void setLengthCm(Integer LengthCm) {
        this.LengthCm = Integer.toString(LengthCm);
    }

    /**
     * 
     * @return
     *     The StatusId
     */
    public Integer getStatusId() {
        return StatusId;
    }

    /**
     * 
     * @param StatusId
     *     The StatusId
     */
    public void setStatusId(Integer StatusId) {
        this.StatusId = StatusId;
    }

    /**
     * 
     * @return
     *     The MerchantId
     */
    public Integer getMerchantId() {
        return MerchantId;
    }

    /**
     * 
     * @param MerchantId
     *     The MerchantId
     */
    public void setMerchantId(Integer MerchantId) {
        this.MerchantId = MerchantId;
    }

    public String getColorKey() {
        return ColorKey;
    }

    public void setColorKey(String colorKey) {
        ColorKey = colorKey;
    }

    /**
     * 
     * @return
     *     The SkuNameInvariant
     */
    public Object getSkuNameInvariant() {
        return SkuNameInvariant;
    }

    /**
     * 
     * @param SkuNameInvariant
     *     The SkuNameInvariant
     */
    public void setSkuNameInvariant(Object SkuNameInvariant) {
        this.SkuNameInvariant = SkuNameInvariant;
    }

    /**
     * 
     * @return
     *     The SkuDescInvariant
     */
    public Object getSkuDescInvariant() {
        return SkuDescInvariant;
    }

    /**
     * 
     * @param SkuDescInvariant
     *     The SkuDescInvariant
     */
    public void setSkuDescInvariant(Object SkuDescInvariant) {
        this.SkuDescInvariant = SkuDescInvariant;
    }

    /**
     * 
     * @return
     *     The SkuFeatureInvariant
     */
    public Object getSkuFeatureInvariant() {
        return SkuFeatureInvariant;
    }

    /**
     * 
     * @param SkuFeatureInvariant
     *     The SkuFeatureInvariant
     */
    public void setSkuFeatureInvariant(Object SkuFeatureInvariant) {
        this.SkuFeatureInvariant = SkuFeatureInvariant;
    }

    /**
     * 
     * @return
     *     The SkuMaterialInvariant
     */
    public Object getSkuMaterialInvariant() {
        return SkuMaterialInvariant;
    }

    /**
     * 
     * @param SkuMaterialInvariant
     *     The SkuMaterialInvariant
     */
    public void setSkuMaterialInvariant(Object SkuMaterialInvariant) {
        this.SkuMaterialInvariant = SkuMaterialInvariant;
    }

    /**
     * 
     * @return
     *     The SkuColorInvariant
     */
    public Object getSkuColorInvariant() {
        return SkuColorInvariant;
    }

    /**
     * 
     * @param SkuColorInvariant
     *     The SkuColorInvariant
     */
    public void setSkuColorInvariant(Object SkuColorInvariant) {
        this.SkuColorInvariant = SkuColorInvariant;
    }

    /**
     * 
     * @return
     *     The LastModified
     */
    public String getLastModified() {
        return LastModified;
    }

    /**
     * 
     * @param LastModified
     *     The LastModified
     */
    public void setLastModified(String LastModified) {
        this.LastModified = LastModified;
    }

    /**
     * 
     * @return
     *     The BrandName
     */
    public String getBrandName() {
        return BrandName;
    }

    /**
     * 
     * @param BrandName
     *     The BrandName
     */
    public void setBrandName(String BrandName) {
        this.BrandName = BrandName;
    }

    /**
     * 
     * @return
     *     The SizeName
     */
    public String getSizeName() {
        return SizeName;
    }

    /**
     * 
     * @param SizeName
     *     The SizeName
     */
    public void setSizeName(String SizeName) {
        this.SizeName = SizeName;
    }

    /**
     * 
     * @return
     *     The ColorName
     */
    public String getColorName() {
        return ColorName;
    }

    /**
     * 
     * @param ColorName
     *     The ColorName
     */
    public void setColorName(String ColorName) {
        this.ColorName = ColorName;
    }

    /**
     * 
     * @return
     *     The SkuName
     */
    public String getSkuName() {
        return SkuName;
    }

    /**
     * 
     * @param SkuName
     *     The SkuName
     */
    public void setSkuName(String SkuName) {
        this.SkuName = SkuName;
    }

    /**
     * 
     * @return
     *     The SkuColor
     */
    public String getSkuColor() {
        return SkuColor;
    }

    /**
     * 
     * @param SkuColor
     *     The SkuColor
     */
    public void setSkuColor(String SkuColor) {
        this.SkuColor = SkuColor;
    }

    /**
     * 
     * @return
     *     The SkuDesc
     */
    public String getSkuDesc() {
        return SkuDesc;
    }

    /**
     * 
     * @param SkuDesc
     *     The SkuDesc
     */
    public void setSkuDesc(String SkuDesc) {
        this.SkuDesc = SkuDesc;
    }

    /**
     * 
     * @return
     *     The SkuMaterial
     */
    public String getSkuMaterial() {
        return SkuMaterial;
    }

    /**
     * 
     * @param SkuMaterial
     *     The SkuMaterial
     */
    public void setSkuMaterial(String SkuMaterial) {
        this.SkuMaterial = SkuMaterial;
    }

    /**
     * 
     * @return
     *     The SkuFeature
     */
    public String getSkuFeature() {
        return SkuFeature;
    }

    /**
     * 
     * @param SkuFeature
     *     The SkuFeature
     */
    public void setSkuFeature(String SkuFeature) {
        this.SkuFeature = SkuFeature;
    }

    /**
     * 
     * @return
     *     The PrimaryCategoryId
     */
    public Integer getPrimaryCategoryId() {
        return PrimaryCategoryId;
    }

    /**
     * 
     * @param PrimaryCategoryId
     *     The PrimaryCategoryId
     */
    public void setPrimaryCategoryId(Integer PrimaryCategoryId) {
        this.PrimaryCategoryId = PrimaryCategoryId;
    }

    /**
     * 
     * @return
     *     The StatusName
     */
    public String getStatusName() {
        return StatusName;
    }

    /**
     * 
     * @param StatusName
     *     The StatusName
     */
    public void setStatusName(String StatusName) {
        this.StatusName = StatusName;
    }

}
