package com.mm.main.app.activity.storefront.filter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appyvet.rangebar.IRangeBarFormatter;
import com.appyvet.rangebar.RangeBar;
import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.base.BaseListItemAdapter;
import com.mm.main.app.adapter.strorefront.filter.FilterBadgeRVAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.decoration.GridSpacingItemDecoration;
import com.mm.main.app.helper.ProductFilterHelper;
import com.mm.main.app.listitem.BaseListItem;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.schema.Badge;
import com.mm.main.app.schema.Brand;
import com.mm.main.app.schema.Category;
import com.mm.main.app.schema.Color;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.response.SearchResponse;
import com.mm.main.app.schema.Size;
import com.mm.main.app.schema.Style;
import com.mm.main.app.service.SearchServiceBuilder;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.StringUtil;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import retrofit.Response;
import retrofit.Retrofit;

public class FilterActivity extends AppCompatActivity {

    public static final int REQUEST_BRAND_SELECTION = 1234;
    public static final int REQUEST_CATEGORY_SELECTION = REQUEST_BRAND_SELECTION + 1;
    public static final int REQUEST_COLOR_SELECTION = REQUEST_CATEGORY_SELECTION + 1;
    public static final int REQUEST_SIZE_SELECTION = REQUEST_COLOR_SELECTION + 1;
    public static final int REQUEST_MERCHANT_SELECTION = REQUEST_SIZE_SELECTION + 1;

    private static int FILTER_BRAND = 0;
    private static int FILTER_CATEGORY = 1;
    private static int FILTER_COLOR = 2;
    private static int FILTER_SIZE = 3;
    private static int FILTER_MERCHANT = 4;

    public static final String EXTRA_COLOR_DATA = "colorData";
    public static final String EXTRA_TEXT_DATA = "textData";

    @Bind(R.id.listViewFilter)
    ListView listViewFilter;

    @Bind(R.id.seekBar)
    RangeBar seekBar;

    @Bind(R.id.textViewPriceRangeValue)
    TextView textViewPriceRangeValue;

    @Bind(R.id.textViewProductNumber)
    TextView textViewProductNumber;

    @Bind(R.id.parentView)
    RelativeLayout parentView;

    @BindString(R.string.LB_CA_FILTER)
    String LB_CA_FILTER;
    @BindString(R.string.LB_CA_RESET)
    String LB_CA_RESET;
    @BindString(R.string.LB_CA_BRAND)
    String LB_CA_BRAND;
    @BindString(R.string.LB_CA_MERCHANT)
    String LB_CA_MERCHANT;
    @BindString(R.string.LB_CA_CATEGORY)
    String LB_CA_CATEGORY;
    @BindString(R.string.LB_CA_COLOUR)
    String LB_CA_COLOUR;
    @BindString(R.string.LB_CA_SIZE)
    String LB_CA_SIZE;
    @BindString(R.string.LB_CA_NUM_PRODUCTS_1)
    String LB_CA_NUM_PRODUCTS_1;
    @BindString(R.string.LB_CA_NUM_PRODUCTS_2)
    String LB_CA_NUM_PRODUCTS_2;

    @Bind(R.id.rvBadge)
    RecyclerView recyclerView;

    @Bind(R.id.rvStaticBadge)
    RecyclerView recyclerViewStatic;

    ArrayList filterList;
    BaseListItemAdapter filterListItemAdapter;

    FilterBadgeRVAdapter filterBadgeRVAdapter;
    FilterBadgeRVAdapter filterStaticBadgeRVAdapter;

    List<Category> fullCategoryList;
    List<Brand> fullBrandList;
    List<Color> fullColorList;
    List<Size> fullSizeList;
    List<Badge> fullBadgeList;
    List<Merchant> fullMerchantList;

    List<Category> selectedCategory;
    List<Brand> selectedBrand;
    List<Color> selectedColor;
    List<Size> selectedSize;
    List<Badge> selectedBadge;
    List<Merchant> selectedMerchant;

    Integer selectedIsSale;
    Integer selectedIsNew;
    boolean isFirstFilterCategory = true;

    int maxPrice = 10000;
    int factor = 1;
    final int barMax = 1000;
    String rangeMin = "0";
    String rangeMax = "10000";
    private int userId;
    int itemBadgeHeight = 42;
    int badgeMargin = 20;

    private boolean initPrice = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        SearchStyle.cloneInstance();

        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, LB_CA_FILTER);
        renderFilterListView();
        setupSeekBar();

        fetchFilterData();
    }

    private void updateSelectedStaticBadge(List<FilterListItem<Badge>> filterListItems) {
        selectedIsSale = filterListItems.get(1).isSelected() ? 1 : null;
        selectedIsNew = filterListItems.get(0).isSelected() ? 1 : null;

        submitSearch();
    }

    private void updateSelectedBadgeList(List<FilterListItem<Badge>> filterListItems) {
        List<Badge> badgeTemp = new ArrayList<>();

        for (int i = 0; i < filterListItems.size(); i++) {
            if (filterListItems.get(i).isSelected()) {
                badgeTemp.add(filterListItems.get(i).getT());
            }
        }
        selectedBadge = badgeTemp;

        submitSearch();
    }

    private void initStaticBadge() {
        GridLayoutManager glm = new GridLayoutManager(MyApplication.getContext(), 3);
        recyclerViewStatic.setLayoutManager(glm);
        recyclerViewStatic.setClipToPadding(false);
        recyclerViewStatic.setClipChildren(false);

        List<FilterListItem<Badge>> badgeFilterList = new ArrayList<>();
        badgeFilterList.add(new FilterListItem<>(new Badge(StringUtil.getResourceString("LB_CA_NEW_PRODUCT_SHORT"))));
        badgeFilterList.add(new FilterListItem<>(new Badge(StringUtil.getResourceString("LB_CA_DISCOUNT"))));

        if (SearchStyle.getClonedInstance().getIsnew() != null) {
            if (SearchStyle.getClonedInstance().getIsnew() == 1) {
                badgeFilterList.get(0).setSelected(true);
            }
            selectedIsNew = SearchStyle.getClonedInstance().getIsnew();
        }

        if (SearchStyle.getClonedInstance().getIssale() != null) {
            if (SearchStyle.getClonedInstance().getIssale() == 1) {
                badgeFilterList.get(1).setSelected(true);
            }
            selectedIsSale = SearchStyle.getClonedInstance().getIssale();
        }

        filterStaticBadgeRVAdapter = new FilterBadgeRVAdapter(badgeFilterList, new FilterBadgeRVAdapter.BadgeOnClickListener() {
            @Override
            public void onClick(int position, Object data) {
                updateSelectedStaticBadge((List<FilterListItem<Badge>>) data);
            }
        });
        recyclerViewStatic.setAdapter(filterStaticBadgeRVAdapter);
        recyclerViewStatic.addItemDecoration(new GridSpacingItemDecoration(4, 0, true));

        int factor = (int) Math.ceil(badgeFilterList.size() / 3.0);
        if (factor < 1) {
            factor = 1;
        }

        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(UiUtil.getDisplayWidth(),
                UiUtil.getPixelFromDp(itemBadgeHeight * factor));
        p.setMargins(0, UiUtil.getPixelFromDp(badgeMargin), 0, 0);
        recyclerViewStatic.setLayoutParams(p);
    }

    private void initBadge() {
        selectedBadge = SearchStyle.getClonedInstance().getBadgeid();
        GridLayoutManager glm = new GridLayoutManager(MyApplication.getContext(), 3);
        recyclerView.setLayoutManager(glm);
        recyclerView.setClipToPadding(false);
        recyclerView.setClipChildren(false);

        List<FilterListItem<Badge>> badgeFilterList = new ArrayList<>();
        for (Badge badge : fullBadgeList) {
            badgeFilterList.add(new FilterListItem<>(badge));
        }

        if (selectedBadge != null) {
            for (int i = 0; i < selectedBadge.size(); i++) {
                for (int j = 0; j < badgeFilterList.size(); j++) {
                    if (selectedBadge.get(i).getBadgeId().intValue() == fullBadgeList.get(j).getBadgeId().intValue()) {
                        badgeFilterList.get(j).setSelected(true);
                        break;
                    }
                }
            }
        }

        filterBadgeRVAdapter = new FilterBadgeRVAdapter(badgeFilterList, new FilterBadgeRVAdapter.BadgeOnClickListener() {
            @Override
            public void onClick(int position, Object data) {
                updateSelectedBadgeList((List<FilterListItem<Badge>>) data);
            }
        });
        recyclerView.setAdapter(filterBadgeRVAdapter);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(4, 0, true));

        int factor = (int) Math.ceil(badgeFilterList.size() / 3.0);
        if (factor < 1) {
            factor = 1;
        }

        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(UiUtil.getDisplayWidth(),
                UiUtil.getPixelFromDp(itemBadgeHeight * factor));
        p.setMargins(0, UiUtil.getPixelFromDp(badgeMargin), 0, 0);
        recyclerView.setLayoutParams(p);
    }

    private void fetchFilterData() {
        fetchAggregations();
    }

    private void fetchAggregations() {
        MmProgressDialog.show(this);
        SearchServiceBuilder.searchStyleFilteredByEntry(new MmCallBack<SearchResponse>(this, false) {
            @Override
            public void onSuccess(Response<SearchResponse> response, Retrofit retrofit) {
                ProductFilterHelper.setAggregations(response.body().getAggregations());
                fetchCategory(0, true);
            }
        });
    }

    private void init() {
        initBrand();
        initCategory();
        initColor();
        initSize();
        initStaticBadge();
        initBadge();
        initMerchant();

        filterListItemAdapter.notifyDataSetChanged();

        submitSearch();
    }

    private void initPrice() {
        Integer priceFrom = SearchStyle.getClonedInstance().getPricefrom();
        Integer priceTo = SearchStyle.getClonedInstance().getPriceTo();
        if (priceFrom == null) {
            priceFrom = maxPrice;
        }
        if (priceTo == null) {
            priceTo = maxPrice;
        }
        seekBar.setRangePinsByValue(priceFrom / factor, priceTo / factor);
    }

    private void initBrand() {
        List<Brand> storedBrands = SearchStyle.getClonedInstance().getBrandid();
        if (storedBrands != null && storedBrands.size() > 0 && fullBrandList != null) {
            selectedBrand = storedBrands;

            BaseListItem baseListItem0 = (BaseListItem) filterList.get(FILTER_BRAND);
            if (selectedBrand.size() > 0) {
                baseListItem0.setContent(getBrandResultStr(fullBrandList, selectedBrand));
            }
        }
    }

    private void initMerchant() {
        List<Merchant> storedMerchants = SearchStyle.getClonedInstance().getMerchantid();
        if (storedMerchants != null && storedMerchants.size() > 0 && fullMerchantList != null) {
            selectedMerchant = storedMerchants;

            BaseListItem baseListItem0 = (BaseListItem) filterList.get(FILTER_MERCHANT);
            if (selectedMerchant.size() > 0) {
                baseListItem0.setContent(getMerchantResultStr(fullMerchantList, selectedMerchant));
            }
        }
    }

    private void initCategory() {
        List<Category> storedCategories = SearchStyle.getClonedInstance().getCategoryid();
        if (storedCategories != null && storedCategories.size() > 0 && fullCategoryList != null) {
            selectedCategory = storedCategories;

            BaseListItem baseListItem0 = (BaseListItem) filterList.get(FILTER_CATEGORY);
            if (selectedCategory.size() > 0) {
                baseListItem0.setContent(getCategoryResultStr(fullCategoryList, selectedCategory));
            }
        }
    }

    private void initColor() {
        List<Color> storedColors = SearchStyle.getClonedInstance().getColorid();
        if (storedColors != null && storedColors.size() > 0 && fullColorList != null) {
            selectedColor = storedColors;

            BaseListItem baseListItem0 = (BaseListItem) filterList.get(FILTER_COLOR);
            if (selectedColor.size() > 0) {
                baseListItem0.setContent(getColorResultStr(fullColorList, selectedColor));
            }
        }
    }

    private void initSize() {
        List<Size> storedSizes = SearchStyle.getClonedInstance().getSizeid();
        if (storedSizes != null && storedSizes.size() > 0 && fullSizeList != null) {
            selectedSize = storedSizes;

            BaseListItem baseListItem0 = (BaseListItem) filterList.get(FILTER_SIZE);
            if (selectedSize.size() > 0) {
                baseListItem0.setContent(getSizeResultStr(fullSizeList, selectedSize));
            }
        }
    }

    private void setupSeekBar() {
        factor = maxPrice / barMax;
        seekBar.setTickEnd((float) (maxPrice / factor));
        seekBar.setTickInterval((float) (maxPrice / factor / 4.0));

        textViewPriceRangeValue.setText("¥" + rangeMin + " - " + "¥" + rangeMax + "+");
        seekBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex,
                                              String leftPinValue, String rightPinValue) {

                rangeMin = String.valueOf(Integer.valueOf(leftPinValue) * factor);
                Integer upperValue = Integer.valueOf(rightPinValue) * factor;
                rangeMax = String.valueOf(upperValue);

                String plusEnd = "";
                if (upperValue == maxPrice) {
                    plusEnd = "+";
                }

                textViewPriceRangeValue.setText("¥" + rangeMin + " - " + "¥" + rangeMax + plusEnd);

                if (!initPrice) {
                    submitSearch();
                } else {
                    initPrice = false;
                }
            }
        });

        seekBar.setFormatter(new IRangeBarFormatter() {
            @Override
            public String format(String s) {
                // Transform the String s here then return s
                return String.valueOf(Integer.valueOf(s) * factor);
            }
        });

        initPrice();

    }

    public void renderFilterListView() {
        filterList = new ArrayList<>();
        filterList.add(new BaseListItem(LB_CA_BRAND, "", BaseListItem.ItemType.TYPE_TEXT_ARROW));
        filterList.add(new BaseListItem(LB_CA_CATEGORY, "", BaseListItem.ItemType.TYPE_TEXT_ARROW));
        filterList.add(new BaseListItem(LB_CA_COLOUR, "", BaseListItem.ItemType.TYPE_TEXT_ARROW));
        filterList.add(new BaseListItem(LB_CA_SIZE, "", BaseListItem.ItemType.TYPE_TEXT_ARROW));
        filterList.add(new BaseListItem(LB_CA_MERCHANT, "", BaseListItem.ItemType.TYPE_TEXT_ARROW));
        filterListItemAdapter = new BaseListItemAdapter(this, filterList);
        listViewFilter.setAdapter(filterListItemAdapter);
        filterListItemAdapter.notifyDataSetChanged();

        LinearLayout.LayoutParams p = (LinearLayout.LayoutParams) listViewFilter.getLayoutParams();

        int totalHeight = 0;
        int count = filterList.size();

        for (int i = 0; i < count; i++) {
            listViewFilter.measure(0, 0);
            totalHeight += listViewFilter.getMeasuredHeight();
        }
        p.height = totalHeight + (listViewFilter.getDividerHeight() * (count - 1));

        listViewFilter.setLayoutParams(p);
        listViewFilter.requestLayout();
    }

    private String getBrandResultStr(List<Brand> fullList, List<Brand> selectedList) {
        String result = "";
        if (fullList != null && selectedList != null) {
            for (int i = 0; i < selectedList.size(); i++) {
                for (int j = 0; j < fullList.size(); j++) {
                    if (selectedList.get(i).getBrandId().intValue() == fullList.get(j).getBrandId().intValue()) {
                        result += fullList.get(j).getBrandName() + " ";
                        break;
                    }
                }
            }
        }

        return result;
    }

    private String getMerchantResultStr(List<Merchant> fullList, List<Merchant> selectedList) {
        String result = "";
        if (fullList != null && selectedList != null) {
            for (int i = 0; i < selectedList.size(); i++) {
                for (int j = 0; j < fullList.size(); j++) {
                    if (selectedList.get(i).getMerchantId().intValue() == fullList.get(j).getMerchantId().intValue()) {
                        result += fullList.get(j).getMerchantCompanyName() + " ";
                        break;
                    }
                }
            }
        }

        return result;
    }

    private String getColorResultStr(List<Color> fullList, List<Color> selectedList) {
        String result = "";
        if (fullList != null && selectedList != null) {
            for (int i = 0; i < selectedList.size(); i++) {
                for (int j = 0; j < fullList.size(); j++) {
                    if (selectedList.get(i).getColorId().intValue() == fullList.get(j).getColorId().intValue()) {
                        result += fullList.get(j).getColorName() + " ";
                    }
                }
            }
        }

        return result;
    }

    private String getSizeResultStr(List<Size> fullList, List<Size> selectedList) {
        String result = "";
        if (fullList != null && selectedList != null) {
            for (int i = 0; i < selectedList.size(); i++) {
                for (int j = 0; j < fullList.size(); j++) {
                    if (selectedList.get(i).getSizeId().intValue() == fullList.get(j).getSizeId().intValue()) {
                        result += fullList.get(j).getSizeName() + " ";
                    }
                }
            }
        }

        return result;
    }

    private String getCategoryResultStr(List<Category> fullList, List<Category> selectedList) {
        String result = "";

        for (int j = 0; j < fullList.size(); j++) {
            if (fullList.get(j).getCategoryList() != null) {
                for (int k = 0; k < fullList.get(j).getCategoryList().size(); k++) {
                    Category categorySelected = fullList.get(j).getCategoryList().get(k);
                    categorySelected.setIsSelected(false);
                }
            }
        }

        if (fullList != null && selectedList != null) {
            for (int i = 0; i < selectedList.size(); i++) {
                for (int j = 0; j < fullList.size(); j++) {
                    if (selectedList.get(i).getCategoryId().intValue() == fullList.get(j).getCategoryId().intValue()) {
                        result += fullList.get(j).getCategoryName() + " ";
                    }
                    if (fullList.get(j).getCategoryList() != null) {
                        for (int k = 0; k < fullList.get(j).getCategoryList().size(); k++) {
                            Category categorySelected = fullList.get(j).getCategoryList().get(k);
                            if (selectedList.get(i).getCategoryId().intValue() == categorySelected.getCategoryId().intValue()) {
                                result += categorySelected.getCategoryName() + " ";
                                categorySelected.setIsSelected(true);
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    private String getBadgeResultStr(List<Badge> fullList, List<Badge> selectedList) {
        String result = "";
        if (fullList != null && selectedList != null) {
            for (int i = 0; i < selectedList.size(); i++) {
                for (int j = 0; j < fullList.size(); j++) {
                    if (selectedList.get(i).getBadgeId().intValue() == fullList.get(j).getBadgeId().intValue()) {
                        result += fullList.get(j).getBadgeName() + " ";
                    }
                }
            }
        }

        return result;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_BRAND_SELECTION:
                    BaseListItem baseListItem0 = (BaseListItem) filterList.get(FILTER_BRAND);
                    Bundle bundle = data.getBundleExtra(BrandFilterSelectionActivity.EXTRA_RESULT);
                    selectedBrand = (List<Brand>) bundle.getSerializable(BrandFilterSelectionActivity.EXTRA_RESULT_DATA);
                    baseListItem0.setContent(getBrandResultStr(fullBrandList, selectedBrand));
                    break;
                case REQUEST_CATEGORY_SELECTION:
                    BaseListItem baseListItem1 = (BaseListItem) filterList.get(FILTER_CATEGORY);
                    Bundle bundle1 = data.getBundleExtra(CategoryFilterSelectionActivity.EXTRA_RESULT);
                    selectedCategory = (List<Category>) bundle1.getSerializable(CategoryFilterSelectionActivity.EXTRA_RESULT_DATA);
                    baseListItem1.setContent(getCategoryResultStr(fullCategoryList, selectedCategory));
                    break;
                case REQUEST_COLOR_SELECTION:
                    BaseListItem baseListItem2 = (BaseListItem) filterList.get(FILTER_COLOR);
                    Bundle bundle2 = data.getBundleExtra(ColorFilterSelectionActivity.EXTRA_RESULT);
                    selectedColor = (List<Color>) bundle2.getSerializable(ColorFilterSelectionActivity.EXTRA_RESULT_DATA);
                    baseListItem2.setContent(getColorResultStr(fullColorList, selectedColor));
                    break;
                case REQUEST_SIZE_SELECTION:
                    BaseListItem baseListItem3 = (BaseListItem) filterList.get(FILTER_SIZE);
                    Bundle bundle3 = data.getBundleExtra(FilterSelectionActivity.EXTRA_RESULT);
                    selectedSize = (List<Size>) bundle3.getSerializable(SizeFilterSelectionActivity.EXTRA_RESULT_DATA);
                    baseListItem3.setContent(getSizeResultStr(fullSizeList, selectedSize));
                    break;
                case REQUEST_MERCHANT_SELECTION:
                    BaseListItem baseListItem4 = (BaseListItem) filterList.get(FILTER_MERCHANT);
                    Bundle bundle4 = data.getBundleExtra(MerchantFilterSelectionActivity.EXTRA_RESULT);
                    selectedMerchant = (List<Merchant>) bundle4.getSerializable(MerchantFilterSelectionActivity.EXTRA_RESULT_DATA);
                    baseListItem4.setContent(getMerchantResultStr(fullMerchantList, selectedMerchant));
                    break;
            }
            submitSearch();
        }

        filterListItemAdapter.notifyDataSetChanged();
    }

    private void setupProductNumber(int number) {
        textViewProductNumber.setText(LB_CA_NUM_PRODUCTS_1 + number + LB_CA_NUM_PRODUCTS_2);
    }

    @OnClick(R.id.buttonOk)
    public void okButtonClick() {
        Intent resultIntent = new Intent();
        setResult(RESULT_OK, resultIntent);
        // TODO update filter
        SearchStyle.applySearchInstance();
        finish();
    }

    @OnItemClick(R.id.listViewFilter)
    public void onItemClicked(int position) {
        Intent intent = null;
        int requestCode = REQUEST_BRAND_SELECTION;
        Bundle bundle = new Bundle();
        boolean createSuccess = false;

        switch (position) {
            case 0:
                requestCode = REQUEST_BRAND_SELECTION;
                intent = new Intent(this, BrandFilterSelectionActivity.class);
                ArrayList<FilterListItem> brandArrayList = new ArrayList<>();
                for (int i = 0; i < fullBrandList.size(); i++) {
                    brandArrayList.add(new FilterListItem(fullBrandList.get(i)));
                }

                if (selectedBrand != null) {
                    for (int i = 0; i < selectedBrand.size(); i++) {
                        for (int j = 0; j < brandArrayList.size(); j++) {
                            if (selectedBrand.get(i).getBrandId().intValue() == fullBrandList.get(j).getBrandId().intValue()) {
                                brandArrayList.get(j).setSelected(true);
                                break;
                            }
                        }
                    }
                }
                bundle.putSerializable(BrandFilterSelectionActivity.EXTRA_BRAND_DATA, brandArrayList);
                intent.putExtra(FilterSelectionActivity.EXTRA_SELECTIONS, bundle);
                createSuccess = true;
                break;
            case 1:
                requestCode = REQUEST_CATEGORY_SELECTION;
                intent = new Intent(this, CategoryFilterSelectionActivity.class);

                for (int j = 0; j < fullCategoryList.size(); j++) {
                    if (fullCategoryList.get(j).getCategoryList() != null) {
                        for (int k = 0; k < fullCategoryList.get(j).getCategoryList().size(); k++) {
                            Category categorySelected = fullCategoryList.get(j).getCategoryList().get(k);
                            categorySelected.setIsSelected(false);
                        }
                    }
                }

                if (fullCategoryList != null && selectedCategory != null) {
                    for (int i = 0; i < selectedCategory.size(); i++) {
                        for (int j = 0; j < fullCategoryList.size(); j++) {
                            if (fullCategoryList.get(j).getCategoryList() != null) {
                                for (int k = 0; k < fullCategoryList.get(j).getCategoryList().size(); k++) {
                                    Category categorySelected = fullCategoryList.get(j).getCategoryList().get(k);
                                    if (selectedCategory.get(i).getCategoryId().intValue() == categorySelected.getCategoryId().intValue()) {
                                        categorySelected.setIsSelected(true);
                                    }
                                }
                            }
                        }
                    }
                }

                ArrayList<FilterListItem> categoryArrayList = new ArrayList<>();
                for (int i = 0; i < fullCategoryList.size(); i++) {
                    categoryArrayList.add(new FilterListItem(fullCategoryList.get(i)));
                }

                bundle.putSerializable(EXTRA_TEXT_DATA, categoryArrayList);
                intent.putExtra(CategoryFilterSelectionActivity.EXTRA_FIRST_SELECTIONS, isFirstFilterCategory);
                isFirstFilterCategory = false;

                intent.putExtra(FilterSelectionActivity.EXTRA_SELECTIONS, bundle);
                createSuccess = true;
                break;
            case 2:
                requestCode = REQUEST_COLOR_SELECTION;
                intent = new Intent(this, ColorFilterSelectionActivity.class);
                ArrayList<FilterListItem> colorArrayList = new ArrayList<>();
                for (int i = 0; i < fullColorList.size(); i++) {
                    colorArrayList.add(new FilterListItem(fullColorList.get(i)));
                }

                if (selectedColor != null) {
                    for (int i = 0; i < selectedColor.size(); i++) {
                        for (int j = 0; j < colorArrayList.size(); j++) {
                            if (selectedColor.get(i).getColorId().intValue() == fullColorList.get(j).getColorId().intValue()) {
                                colorArrayList.get(j).setSelected(true);
                                break;
                            }
                        }
                    }
                }

                bundle.putSerializable(EXTRA_COLOR_DATA, colorArrayList);
                intent.putExtra(ColorFilterSelectionActivity.EXTRA_SELECTIONS, bundle);
                createSuccess = true;
                break;
            case 3:
                // in case size list not get by request
                if (fullSizeList == null) {
                    return;
                }
                requestCode = REQUEST_SIZE_SELECTION;
                intent = new Intent(this, SizeFilterSelectionActivity.class);
                ArrayList<FilterListItem> sizeArrayList = new ArrayList<>();

                for (int i = 0; i < fullSizeList.size(); i++) {
                    sizeArrayList.add(new FilterListItem(fullSizeList.get(i)));
                }

                if (selectedSize != null) {
                    for (int i = 0; i < selectedSize.size(); i++) {
                        for (int j = 0; j < sizeArrayList.size(); j++) {
                            if (selectedSize.get(i).getSizeId().intValue() == fullSizeList.get(j).getSizeId().intValue()) {
                                sizeArrayList.get(j).setSelected(true);
                                break;
                            }
                        }
                    }
                }

                bundle.putSerializable(EXTRA_TEXT_DATA, sizeArrayList);
                intent.putExtra(SizeFilterSelectionActivity.EXTRA_SELECTIONS, bundle);
                createSuccess = true;

                break;
            case 4:
                requestCode = REQUEST_MERCHANT_SELECTION;
                intent = new Intent(this, MerchantFilterSelectionActivity.class);
                ArrayList<FilterListItem> merchantArrayList = new ArrayList<>();
                for (int i = 0; i < fullMerchantList.size(); i++) {
                    merchantArrayList.add(new FilterListItem(fullMerchantList.get(i)));
                }

                if (selectedMerchant != null) {
                    for (int i = 0; i < selectedMerchant.size(); i++) {
                        for (int j = 0; j < merchantArrayList.size(); j++) {
                            if (selectedMerchant.get(i).getMerchantId().intValue() == fullMerchantList.get(j).getMerchantId().intValue()) {
                                merchantArrayList.get(j).setSelected(true);
                                break;
                            }
                        }
                    }
                }
                bundle.putSerializable(MerchantFilterSelectionActivity.EXTRA_MERCHANT_DATA, merchantArrayList);
                intent.putExtra(FilterSelectionActivity.EXTRA_SELECTIONS, bundle);
                createSuccess = true;
                break;
            default:
                break;
        }

        if (createSuccess && intent != null) {
            BaseListItem baseListItem = (BaseListItem) filterList.get(position);
            intent.putExtra(FilterSelectionActivity.EXTRA_HEADER, baseListItem.getTitle());
            startActivityForResult(intent, requestCode);
        }
    }

    private void fetchBadge(final boolean isInit) {

        APIManager.getInstance().getSearchService().badge()
                .enqueue(new MmCallBack<List<Badge>>(FilterActivity.this, false) {
                    @Override
                    public void onSuccess(Response<List<Badge>> response, Retrofit retrofit) {
                        fullBadgeList = ProductFilterHelper.filterBadgeId(response.body(),
                                ProductFilterHelper.getAggregations().getBadgeArray());

                        fetchMerchant(true);
                    }
                });
    }

    private void fetchMerchant(final boolean isInit) {

        APIManager.getInstance().getSearchService().merchant()
                .enqueue(new MmCallBack<List<Merchant>>(FilterActivity.this) {
                    @Override
                    public void onSuccess(Response<List<Merchant>> response, Retrofit retrofit) {
                        fullMerchantList = ProductFilterHelper.filterMerchantId(response.body(),
                                ProductFilterHelper.getAggregations().getMerchantArray());
                        if (!isInit) {
                            onItemClicked(FILTER_MERCHANT);
                        }

                        init();
                        parentView.setVisibility(View.VISIBLE);
                    }

                });
    }

    private void fetchCategory(int userId, final boolean isInit) {
        this.userId = userId;

        APIManager.getInstance().getSearchService().category()
                .enqueue(new MmCallBack<List<Category>>(FilterActivity.this, false) {
                    @Override
                    public void onSuccess(Response<List<Category>> response, Retrofit retrofit) {
                        fullCategoryList = ProductFilterHelper.filterCategoryId(response.body(),
                                ProductFilterHelper.getAggregations().getCategoryArray());
                        if (!isInit) {
                            onItemClicked(FILTER_CATEGORY);
                        }

                        fetchBrand(true);
                    }
                });
    }

    private void fetchBrand(final boolean isInit) {

        APIManager.getInstance().getSearchService().brand()
                .enqueue(new MmCallBack<List<Brand>>(FilterActivity.this, false) {
                    @Override
                    public void onSuccess(Response<List<Brand>> response, Retrofit retrofit) {
                        fullBrandList = ProductFilterHelper.filterBrandId(response.body(),
                                ProductFilterHelper.getAggregations().getBrandArray());
                        if (!isInit) {
                            onItemClicked(FILTER_BRAND);
                        }

                        fetchColor(true);
                    }
                });
    }

    private void fetchColor(final boolean isInit) {

        APIManager.getInstance().getSearchService().color()
                .enqueue(new MmCallBack<List<Color>>(FilterActivity.this, false) {
                    @Override
                    public void onSuccess(Response<List<Color>> response, Retrofit retrofit) {
                        fullColorList = ProductFilterHelper.filterColorId(response.body(),
                                ProductFilterHelper.getAggregations().getColorArray());
                        if (!isInit) {
                            onItemClicked(FILTER_COLOR);
                        }

                        fetchSize(true);
                    }
                });
    }

    private void fetchSize(final boolean isInit) {

        APIManager.getInstance().getSearchService().size()
                .enqueue(new MmCallBack<List<Size>>(FilterActivity.this, false) {
                    @Override
                    public void onSuccess(Response<List<Size>> response, Retrofit retrofit) {
                        fullSizeList = ProductFilterHelper.filterSizeId(response.body(),
                                ProductFilterHelper.getAggregations().getSizeArray());
                        if (!isInit) {
                            onItemClicked(FILTER_SIZE);
                        }

                        fetchBadge(true);
                    }
                });
    }

    private void submitSearch() {
        SearchServiceBuilder.searchStyle(new MmCallBack<SearchResponse>(this) {
                                             @Override
                                             public void onSuccess(Response<SearchResponse> response, Retrofit retrofit) {
                                                 List<Style> suggestProductList = response.body().getPageData();
//                ProductFilterHelper.setFullStyleList(suggestProductList);
//                renderStyleList(viewHolderSuggestion);
                                                 setupProductNumber(suggestProductList.size());

                                             }
                                         }, getQueryString(), getPriceFrom(), getPriceTo(), getBrands(), getCategories(),
                getColors(), getSizes(), selectedBadge, getMerchants(), selectedIsSale, selectedIsNew, false);

        // Save Temp intance of search
        SearchStyle tempStyle = SearchStyle.getClonedInstance();
        tempStyle.setQueryString(getQueryString());
        tempStyle.setPricefrom(getPriceFrom());
        tempStyle.setPriceTo(getPriceTo());
        tempStyle.setBrandid(getBrands());
        tempStyle.setCategoryid(getCategories());
        tempStyle.setColorid(getColors());
        tempStyle.setSizeid(getSizes());
        tempStyle.setBadgeid(selectedBadge);
        tempStyle.setMerchantid(getMerchants());
        tempStyle.setIssale(selectedIsSale);
        tempStyle.setIsnew(selectedIsNew);
    }


    private String getQueryString() {
        return SearchStyle.getClonedInstance().getQueryString();
    }

    private Integer autoAdjustRange(String value) {
        Integer intValue = Integer.valueOf(value);
        if (intValue == maxPrice) {
            return null;
        }

        return intValue;
    }

    private Integer getPriceFrom() {
        return autoAdjustRange(rangeMin);
    }

    private Integer getPriceTo() {
        return autoAdjustRange(rangeMax);
    }

    private List<Merchant> getMerchants() {
        return selectedMerchant;
    }

    private List<Brand> getBrands() {
        return selectedBrand;
    }

    private List<Category> getCategories() {
        return selectedCategory;
    }

    private List<Color> getColors() {
        return selectedColor;
    }

    private List<Size> getSizes() {
        return selectedSize;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reset) {
            resetAllSelection();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void resetAllSelection() {
        SearchStyle.resetClonedInstance();
        BaseListItem baseListItem0 = (BaseListItem) filterList.get(FILTER_BRAND);
        selectedBrand = null;
        baseListItem0.setContent("");
        BaseListItem baseListItem1 = (BaseListItem) filterList.get(FILTER_CATEGORY);
        selectedCategory = null;
        baseListItem1.setContent("");
        BaseListItem baseListItem2 = (BaseListItem) filterList.get(FILTER_COLOR);
        selectedColor = null;
        baseListItem2.setContent("");
        BaseListItem baseListItem3 = (BaseListItem) filterList.get(FILTER_SIZE);
        selectedSize = null;
        baseListItem3.setContent("");
        BaseListItem baseListItem4 = (BaseListItem) filterList.get(FILTER_MERCHANT);
        selectedMerchant = null;
        baseListItem4.setContent("");

        filterStaticBadgeRVAdapter.reset();
        filterBadgeRVAdapter.reset();
        selectedBadge = null;

        textViewPriceRangeValue.setText("¥" + rangeMin + " - " + "¥" + rangeMax + "+");
        seekBar.setRangePinsByValue(0, maxPrice / factor);

        submitSearch();

        filterListItemAdapter.notifyDataSetChanged();
    }
}

