package com.mm.main.app.service;

import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.User;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by tuuphungd on 3/14/2016.
 */
public interface ViewService {
    String VIEW_PATH = "view";

    @GET(VIEW_PATH + "/user")
    Call<User> viewUser(@Query("UserKey") String userKey);

    @GET(VIEW_PATH + "/merchant")
    Call<Merchant> viewMerchant(@Query("MerchantId") Integer merchanId);
}
