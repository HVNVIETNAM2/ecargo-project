
package com.mm.main.app.schema.response;

public class CompleteResponse {

    private String Entity;
    private Integer EntityId;
    private String EntityImage;
    private String SearchTerm;
    private String SearchTermIn;

    /**
     * 
     * @return
     *     The Entity
     */
    public String getEntity() {
        return Entity;
    }

    /**
     * 
     * @param Entity
     *     The Entity
     */
    public void setEntity(String Entity) {
        this.Entity = Entity;
    }

    /**
     * 
     * @return
     *     The EntityId
     */
    public Integer getEntityId() {
        return EntityId;
    }

    /**
     * 
     * @param EntityId
     *     The EntityId
     */
    public void setEntityId(Integer EntityId) {
        this.EntityId = EntityId;
    }

    /**
     * 
     * @return
     *     The EntityImage
     */
    public String getEntityImage() {
        return EntityImage;
    }

    /**
     * 
     * @param EntityImage
     *     The EntityImage
     */
    public void setEntityImage(String EntityImage) {
        this.EntityImage = EntityImage;
    }

    /**
     * 
     * @return
     *     The SearchTerm
     */
    public String getSearchTerm() {
        return SearchTerm;
    }

    /**
     * 
     * @param SearchTerm
     *     The SearchTerm
     */
    public void setSearchTerm(String SearchTerm) {
        this.SearchTerm = SearchTerm;
    }

    /**
     * 
     * @return
     *     The SearchTermIn
     */
    public String getSearchTermIn() {
        return SearchTermIn;
    }

    /**
     * 
     * @param SearchTermIn
     *     The SearchTermIn
     */
    public void setSearchTermIn(String SearchTermIn) {
        this.SearchTermIn = SearchTermIn;
    }

}
