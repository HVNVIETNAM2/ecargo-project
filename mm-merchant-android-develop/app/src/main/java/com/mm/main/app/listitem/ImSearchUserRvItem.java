package com.mm.main.app.listitem;

import com.mm.main.app.schema.User;

public class ImSearchUserRvItem {

    public enum ItemType {
        TYPE_NOT_BEFRIENDED,
        TYPE_BEFRIENDED,
        TYPE_FOLLOW,
        TYPE_FOLLOWED,
        TYPE_REQ_CANCEL
    }

    private User user;
    private ItemType typeFriendAction;

    private ItemType typeFollowAction;

    public ItemType getTypeFriendAction() {
        return typeFriendAction;
    }

    public void setTypeFriendAction(ItemType typeAction) {
        this.typeFriendAction = typeAction;
    }

    public ItemType getTypeFollowAction() {
        return typeFollowAction;
    }

    public void setTypeFollowAction(ItemType typeFollowAction) {
        this.typeFollowAction = typeFollowAction;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
