package com.mm.main.app.activity.storefront.checkout;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.signup.GeneralSignupActivity;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CheckoutFlowManager;
import com.mm.main.app.schema.Address;
import com.mm.main.app.schema.Country;
import com.mm.main.app.schema.Geo;
import com.mm.main.app.schema.request.AddressSaveRequest;
import com.mm.main.app.utils.AnimateUtil;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

public class AddAddressActivity extends GeneralSignupActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.etReceiver)
    EditText etReceiver;
    @Bind(R.id.etPhone)
    EditText etPhone;
    @Bind(R.id.etZipCode)
    EditText etZipCode;
    @Bind(R.id.txtCountry)
    TextView txtCountry;
    @Bind(R.id.etPhoneCode)
    EditText etPhoneCode;
    @Bind(R.id.txtCity)
    TextView txtCity;
    @Bind(R.id.linearCity)
    LinearLayout linearCity;
    @Bind(R.id.etAddress)
    EditText etAddress;
    @Bind(R.id.tvError)
    TextView tvError;

    List<Country> countries;
    List<Geo> provinces;
    List<Geo> cities;
    Country selectedCountry;
    Geo selectedProvince;
    Geo selectedCity;

    NumberPicker pickerProvince;
    NumberPicker pickerCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);

        initUI();
        loadCountries();
    }

    private void initUI() {
        etPhoneCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s)) {
                    etPhoneCode.setText("+");
                    etPhoneCode.setSelection(etPhoneCode.getText().length());
                }

                boolean foundCountry = false;
                for (Country country : countries) {
                    if (s.toString().equals(country.getMobileCode())) {
                        selectedCountry = country;
                        txtCountry.setText(selectedCountry.getGeoCountryName());
                        foundCountry = true;
                        loadProvinces();
                        break;
                    }
                }

                if (foundCountry == false) {
                    selectedCountry = null;
                    selectedProvince = null;
                    selectedCity = null;
                    txtCountry.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etReceiver.addTextChangedListener(handleErrorWatcher);
        etPhone.addTextChangedListener(handleErrorWatcher);
    }

    TextWatcher handleErrorWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            AnimateUtil.collapse(tvError);
            etPhone.setTextColor(ContextCompat.getColor(AddAddressActivity.this, R.color.secondary2));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void loadCountries() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getGeoService().storefrontCountry()
                .enqueue(new MmCallBack<List<Country>>(this) {
                    @Override
                    public void onSuccess(Response<List<Country>> response, Retrofit retrofit) {
                        countries = response.body();
                        if (countries != null && countries.size() > 0) {
                            selectedCountry = countries.get(0);
                            txtCountry.setText(selectedCountry.getGeoCountryName());
                            etPhoneCode.setText(selectedCountry.getMobileCode());
                            loadProvinces();
                        }
                    }
                });
    }

    private void loadProvinces() {
        APIManager.getInstance().getGeoService().listProvince(selectedCountry.getGeoCountryId())
                .enqueue(new MmCallBack<List<Geo>>(this) {
                    @Override
                    public void onSuccess(Response<List<Geo>> response, Retrofit retrofit) {
                        provinces = response.body();
                        if (provinces != null && provinces.size() > 0) {
                            selectedProvince = provinces.get(0);
                            loadCities();
                        }
                    }
                });
    }

    private void loadCities() {
        APIManager.getInstance().getGeoService().listCity(selectedProvince.getGeoId())
                .enqueue(new MmCallBack<List<Geo>>(this) {
                    @Override
                    public void onSuccess(Response<List<Geo>> response, Retrofit retrofit) {
                        cities = response.body();
                        selectedCity = cities.get(0);
                        loadUICityPicker();
                    }
                });
    }

    private boolean validate() {

        if (TextUtils.isEmpty(etReceiver.getText().toString().trim())) {
            ErrorUtil.showVerificationError(tvError, null, getString(R.string.MSG_ERR_CA_RECEIVER_NIL), this);
            return false;
        }

        if (selectedCountry == null) {
            ErrorUtil.showVerificationError(tvError, null, getString(R.string.LB_COUNTRY_PICK), this);
            return false;
        }

        if (TextUtils.isEmpty(etPhone.getText().toString().trim())) {
            ErrorUtil.showVerificationError(tvError, null, getString(R.string.MSG_ERR_MOBILE_NIL), this);
            return false;
        }

        if (!ValidationUtil.isValidPhone(etPhone.getText())) {
            ErrorUtil.showVerificationError(tvError, etPhone, getString(R.string.MSG_ERR_CA_MOBILE_PATTERN), this);
            return false;
        }

        if (selectedProvince == null || TextUtils.isEmpty(txtCity.getText().toString().trim())) {
            ErrorUtil.showVerificationError(tvError, null, getString(R.string.MSG_ERR_LOCATION_PROVINCE), this);
            return false;
        }

        if (selectedCity == null || TextUtils.isEmpty(txtCity.getText().toString().trim())) {
            ErrorUtil.showVerificationError(tvError, null, getString(R.string.MSG_ERR_LOCATION_CITY), this);
            return false;
        }

        if (TextUtils.isEmpty(etAddress.getText().toString().trim())) {
            ErrorUtil.showVerificationError(tvError, null, getString(R.string.MSG_ERR_DETAIL_ADDRESS_NIL), this);
            return false;
        }

        return true;
    }

    @OnClick(R.id.linearCountry)
    public void selectCountry() {
        showCountryAlert();
    }

    @OnClick(R.id.linearCity)
    public void selectCity() {
        showCityAlert();
    }

    @OnClick(R.id.btnAddAddress)
    public void saveNewAddress() {
        if (validate()) {
            MmProgressDialog.show(this);
            APIManager.getInstance().getAddressService().saveAddress(new AddressSaveRequest(MmGlobal.getUserKey(), etReceiver.getText().toString().trim(), selectedCountry.getGeoCountryId(), selectedProvince.getGeoId(), selectedCity.getGeoId(), selectedCountry.getMobileCode(), etPhone.getText().toString().trim(), true, etAddress.getText().toString().trim(), etZipCode.getText().toString().trim()))
                    .enqueue(new MmCallBack<Address>(this) {
                        @Override
                        public void onSuccess(Response<Address> response, Retrofit retrofit) {
                            Address address = response.body();
                            if (address != null) {
                                handleAfterSaving(address);
                            }
                        }
                    });
        }
    }

    private void handleAfterSaving(Address address) {
        if (loginInApp == LoginInAppType.TYPE_CHECK_OUT_NORMAL) {
            CheckoutFlowManager.getInstance().actionCompleteLoginRequest();
        } else {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constant.RESULT, address);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    public void showCityAlert() {
        if (selectedCountry == null) {
            return;
        }

        String provincesName[] = new String[provinces.size()];
        for (int i = 0; i < provinces.size(); i++) {
            provincesName[i] = provinces.get(i).getGeoName();
        }

        String citiesName[] = new String[cities.size()];
        for (int i = 0; i < cities.size(); i++) {
            citiesName[i] = cities.get(i).getGeoName();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialog = inflater.inflate(R.layout.select_city_alert, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        pickerProvince = (NumberPicker) dialog.findViewById(R.id.pickerProvince);
        pickerProvince.setMinValue(0);
        pickerProvince.setMaxValue(provincesName.length - 1);
        pickerProvince.setWrapSelectorWheel(false);
        pickerProvince.setDisplayedValues(provincesName);
        pickerProvince.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        setDividerColor(pickerProvince, ContextCompat.getColor(this, R.color.background_gray));
        pickerProvince.setOnValueChangedListener(provinceChangeListener);

        pickerCity = (NumberPicker) dialog.findViewById(R.id.pickerCity);
        pickerCity.setMinValue(0);
        pickerCity.setMaxValue(citiesName.length - 1);
        pickerCity.setWrapSelectorWheel(false);
        pickerCity.setDisplayedValues(citiesName);
        pickerCity.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        setDividerColor(pickerCity, ContextCompat.getColor(this, R.color.background_gray));

        Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProvince = provinces.get(pickerProvince.getValue());
                selectedCity = cities.get(pickerCity.getValue());
                txtCity.setText(selectedProvince.getGeoName() + ", " + selectedCity.getGeoName());
                alertDialog.dismiss();
                AnimateUtil.collapse(tvError);
            }
        });

        alertDialog.show();
    }

    private void loadUICityPicker() {

        if (pickerCity == null) {
            return;
        }

        String citiesName[] = new String[cities.size()];
        for (int i = 0; i < cities.size(); i++) {
            citiesName[i] = cities.get(i).getGeoName();
        }

        pickerCity.setDisplayedValues(null);
        pickerCity.setMinValue(0);
        pickerCity.setMaxValue(citiesName.length - 1);
        pickerCity.setWrapSelectorWheel(false);
        pickerCity.setDisplayedValues(citiesName);
        pickerCity.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        pickerCity.setValue(0);
        setDividerColor(pickerCity, ContextCompat.getColor(this, R.color.background_gray));
    }

    NumberPicker.OnValueChangeListener provinceChangeListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            selectedProvince = provinces.get(newVal);
            loadCities();
        }
    };

    public void showCountryAlert() {
        String countryName[] = new String[countries.size()];
        for (int i = 0; i < countries.size(); i++) {
            countryName[i] = countries.get(i).getNameAndCode();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialog = inflater.inflate(R.layout.select_single_alert, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        final NumberPicker pickerCountry = (NumberPicker) dialog.findViewById(R.id.picker);
        pickerCountry.setMinValue(0);
        pickerCountry.setMaxValue(countryName.length - 1);
        pickerCountry.setWrapSelectorWheel(false);
        pickerCountry.setDisplayedValues(countryName);
        pickerCountry.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        setDividerColor(pickerCountry, ContextCompat.getColor(this, R.color.background_gray));

        Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedCountry = countries.get(pickerCountry.getValue());
                txtCountry.setText(selectedCountry.getGeoCountryName());
                etPhoneCode.setText(selectedCountry.getMobileCode());
                selectedProvince = null;
                selectedCity = null;
                txtCity.setText("");
                loadProvinces();
                alertDialog.dismiss();
                AnimateUtil.collapse(tvError);
            }
        });

        alertDialog.show();
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
}
