package com.mm.main.app.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.mm.main.app.activity.storefront.product.ProductDetailPageActivity;
import com.mm.main.app.activity.storefront.product.ProductListActivity;
import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.product.ProductRVAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.decoration.GridSpacingItemDecoration;
import com.mm.main.app.helper.ProductFilterHelper;
import com.mm.main.app.log.Logger;
import com.mm.main.app.schema.response.SearchResponse;
import com.mm.main.app.schema.Style;
import com.mm.main.app.service.SearchServiceBuilder;
import com.mm.main.app.uicomponent.PinterestView;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by chiyungtung on 18/11/2015.
 */
public class ProductListFragment extends Fragment {

    public static final String TAG = ProductListFragment.class.toString();

    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    public static final String EXTRA_MERCHANT_ID = "EXTRA_MERCHANT_ID";
    public static final String EXTRA_CATEGORY_ID = "EXTRA_CATEGORY_ID";

    public static final int REQUEST_PRODUCT_DETAIL = 1993;

    public static final int SPAN_COUNT = 2;
    //    private List<Product> productList;
    ProductRVAdapter productRVAdapter;
    private List<Style> productList;
    private PinterestView pinterestView;
    private ProductRVAdapter.ProductListOnClickListener productListOnClickListener;

    boolean primaryCategory;
    int categoryId;

    RecyclerView rv;

    public static final ProductListFragment newInstance(String message, int merchantId, int categoryId, boolean primaryCategory) {

        ProductListFragment f = new ProductListFragment();
        Bundle bdl = new Bundle(1);
        bdl.putString(EXTRA_MESSAGE, message);
        bdl.putInt(EXTRA_MERCHANT_ID, merchantId);
        bdl.putInt(EXTRA_CATEGORY_ID, categoryId);
        f.primaryCategory = primaryCategory;
        f.setArguments(bdl);

        return f;
    }

    public static final ProductListFragment newInstance(String message, int merchantId, int categoryId) {
        return newInstance(message, merchantId, categoryId, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (productRVAdapter != null) {
            productRVAdapter.notifyDataSetChanged();
        }
    }

    public void refreshStyleList() {
        ProductFilterHelper.setFullStyleList(null);
        renderStyleList(true);
    }

    private void renderStyleList(final boolean showNoResult) {
        if (ProductFilterHelper.getFullStyleList() == null) {
            MmProgressDialog.show(getActivity());
            SearchServiceBuilder.searchStyleWithFilter(new MmCallBack<SearchResponse>(getActivity()) {
                @Override
                public void onSuccess(Response<SearchResponse> response, Retrofit retrofit) {
                    productList = response.body().getPageData();
                    if (productList.size() == 0) {
                        if (showNoResult) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    ProductListActivity productListActivity = (ProductListActivity) getActivity();
                                    productListActivity.startNoSearchResultPage();
                                }
                            }).start();
                        }
                    }
                    ProductFilterHelper.setFullStyleList(productList);
                    renderStyleList(categoryId);
                    productRVAdapter.notifyDataSetChanged();
                }
            });
        } else {
            renderStyleList(categoryId);
            productRVAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        String message = getArguments().getString(EXTRA_MESSAGE);
        int merchantId = getArguments().getInt(EXTRA_MERCHANT_ID);
        categoryId = getArguments().getInt(EXTRA_CATEGORY_ID);
        View v = inflater.inflate(R.layout.fragment_product_list, container, false);

        GridLayoutManager glm = new GridLayoutManager(getContext(), SPAN_COUNT);
        rv = (RecyclerView) v.findViewById(R.id.rv);
        rv.setLayoutManager(glm);
        rv.setClipToPadding(false);
        rv.setClipChildren(false);

        productList = new ArrayList<>();
        productRVAdapter = new ProductRVAdapter(productList, productListOnClickListener);
        productRVAdapter.setContext(getContext());
        rv.setAdapter(productRVAdapter);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_spacing);
        rv.addItemDecoration(new GridSpacingItemDecoration(2, spacingInPixels, true));

        pinterestView = (PinterestView) v.findViewById(R.id.item_layout);

        pinterestView.setOnViewVisibleListener(new PinterestView.OnViewVisibleListener() {
            @Override
            public void onVisible() {
//                ((ProductListActivity) getActivity()).customViewPager.setPagingEnabled(false);
            }

            @Override
            public void onDisable() {
//                ((ProductListActivity) getActivity()).customViewPager.setPagingEnabled(true);
            }
        });

        productListOnClickListener = new ProductRVAdapter.ProductListOnClickListener() {
            @Override
            public void onClick(int position, Object data) {
                Intent intent = new Intent(getContext(), ProductDetailPageActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(ProductDetailPageActivity.EXTRA_PRODUCT_DATA, productList.get(position));
                intent.putExtra(ProductDetailPageActivity.EXTRA_LIKED, (boolean) data);
                getActivity().startActivityForResult(intent, REQUEST_PRODUCT_DETAIL);
//        MyApplication.getContext().startActivity(intent);
            }
        };

        pinterestView.addShowView(40, PinterestView.createChildView(R.drawable.pinterest_circle, "", MyApplication.getContext())
                , PinterestView.createChildView(R.drawable.icon_fan_contact, "联系", MyApplication.getContext())
                , PinterestView.createChildView(R.drawable.icon_fan_share, "分享", MyApplication.getContext())
                , PinterestView.createChildView(R.drawable.icon_fan_shopping_cart, "购物车", MyApplication.getContext()));

        renderStyleList(true);

        rv.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Logger.d(TAG, "I am here");

            }
        }));

        return v;
    }

    private void renderStyleList(int categoryId) {
        if (categoryId >= 0) {
            if (primaryCategory) {
                productList = ProductFilterHelper.filterStyleByPrimaryCategoryId(ProductFilterHelper.getFullStyleList(), categoryId);
            } else {
                productList = ProductFilterHelper.filterStyleByCategoryId(ProductFilterHelper.getFullStyleList(), categoryId);
            }
        }  else {
            productList = ProductFilterHelper.getFullStyleList();
        }
        productRVAdapter.setProductList(productList);
        productRVAdapter.notifyDataSetChanged();
    }

    public void proceedProductDetail() {
//        startActivityForResult();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //in fragment class callback
        switch (requestCode) {
            case REQUEST_PRODUCT_DETAIL:
                if (resultCode == Activity.RESULT_OK) {
                    boolean liked = data.getBooleanExtra(Constant.Extra.EXTRA_LIKED, false);
                    Style style = (Style) data.getSerializableExtra(ProductDetailPageActivity.EXTRA_PRODUCT_DATA);
                    updateListData(style, liked);
                }
                break;
            case ProductListActivity.REQUEST_FRAGMENT_REFRESH:
                renderStyleList(true);
                break;
            case ProductListActivity.REQUEST_FRAGMENT_REFRESH_FROM_NO_RESULT:
                renderStyleList(false);
                break;

        }
    }

    public void updateListData(Style style, boolean liked) {
        List<Style> styleList = productRVAdapter.getProductList();
        for (int i = 0; i < styleList.size(); i++) {
            if (styleList.get(i).getStyleCode().equals(style.getStyleCode())) {
                productRVAdapter.likes[i] = liked;
                productRVAdapter.notifyDataSetChanged();
            }

        }
    }

    interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    private void proceedCheckout(final AppCompatActivity context, final Style style) {
        final ProductListActivity productListActivity = (ProductListActivity) context;
        productListActivity.startCheckoutActivity(style);
    }

    private void proceedShare(final AppCompatActivity context, final Style style) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);

//        # change the type of data you need to share,
//        # for image use "image/*"
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, style.getSkuName());
        startActivity(Intent.createChooser(intent, "Happy Share"));
    }

    private void proceedCallSupport() {
        String url = "tel:+85290433672";
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getActivity().startActivity(intent);
    }

    private class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
        private OnItemClickListener mListener;

        GestureDetector mGestureDetector;

        public RecyclerItemClickListener(Context context, OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(final RecyclerView view, MotionEvent e) {
                View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
            }
            if (pinterestView.getVisibility() == View.GONE && childView != null) {
                final long cId = rv.getChildLayoutPosition(childView);

                pinterestView.setPinClickListener(new PinterestView.PinMenuClickListener() {
                    @Override
                    public void onMenuItemClick(int childAt) {
//                        Toast.makeText(getActivity(), cId + " onMenuItemClick " + childAt, Toast.LENGTH_SHORT).show();
                        if (childAt == 3) {
                            proceedCheckout((AppCompatActivity) getActivity(), productList.get((int) cId));
                        } else if (childAt == 1) {
                            proceedCallSupport();
                        } else if (childAt == 2) {
                            proceedShare((AppCompatActivity) getActivity(), productList.get((int) cId));
                        }
                    }

                    @Override
                    public void onPreViewClick() {
//                        Toast.makeText(getActivity(), cId + " onPreViewClick", Toast.LENGTH_SHORT).show();
                    }
                });

            }
            pinterestView.dispatchTouchEvent(e);
            return pinterestView.getVisibility() == View.VISIBLE;
        }

        @Override
        public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
            pinterestView.dispatchTouchEvent(motionEvent);
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


}
