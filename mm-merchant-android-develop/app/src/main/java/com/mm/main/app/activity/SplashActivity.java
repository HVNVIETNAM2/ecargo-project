package com.mm.main.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Button;

import com.mm.main.app.R;
import com.mm.main.app.activity.merchant.login.LoginActivity;
import com.mm.main.app.activity.merchant.register.RegistrationActivity;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.view.StretchedVideoView;
import com.umeng.analytics.MobclickAgent;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SplashActivity extends Activity {
    @Bind(R.id.registerButton)
    Button registerButton;
    @Bind(R.id.loginButton)
    Button loginButton;

    StretchedVideoView videoHolder = null;
    final int duration = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        try {
            videoHolder = (StretchedVideoView) findViewById(R.id.myvideoview);
            Uri video = Uri.parse(PathUtil.getLocalVideo(getPackageName(), R.raw.android_1));
            videoHolder.setVideoURI(video);
            videoHolder.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                }
            });

            videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            videoHolder.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    return false;
                }
            });

            videoHolder.start();
        } catch (Exception ex) {
            //login();
        }
        registerButton.setAlpha(0f);
        loginButton.setAlpha(0f);
        registerButton.animate().alpha(1f).setDuration(duration).start();
        loginButton.animate().alpha(1f).setDuration(duration).start();

    }


    @OnClick(R.id.loginButton)
    protected void login() {
        if (isFinishing())
            return;
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @OnClick(R.id.registerButton)
    protected void register() {
        if (isFinishing())
            return;
        startActivity(new Intent(this, RegistrationActivity.class));
        finish();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        videoHolder.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        videoHolder.stopPlayback();
    }

}
