package com.mm.main.app.manager;

import com.mm.main.app.activity.storefront.profile.StoreFrontUserProfileDataListActivity;

import java.util.Stack;

/**
 * Created by tuuphungd on 3/16/2016.
 */
public class ProfileLifeCycleManager {
    private static ProfileLifeCycleManager singleton;
    private Stack<String> userKeyHistoryChain;
    private Stack<StoreFrontUserProfileDataListActivity.ProfileDataListType> listTypeHistoryChain;

    private ProfileLifeCycleManager() {
        userKeyHistoryChain = new Stack<>();
        listTypeHistoryChain = new Stack<>();
    }

    public static ProfileLifeCycleManager getInstance() {
        if(singleton == null) {
            singleton = new ProfileLifeCycleManager();
        }
        return singleton;
    }

    public void restartLifeCycle() {
        userKeyHistoryChain.clear();
        listTypeHistoryChain.clear();
    }

    public void openNewProfile(String currentUserId, StoreFrontUserProfileDataListActivity.ProfileDataListType currentPageType) {
        userKeyHistoryChain.push(currentUserId);
        listTypeHistoryChain.push(currentPageType);
    }

    public String backFromDataList() {
        if(userKeyHistoryChain.size()>0) {
            return userKeyHistoryChain.pop();
        }
        return null;
    }

    public StoreFrontUserProfileDataListActivity.ProfileDataListType backFromProfile() {
        if(listTypeHistoryChain.size()>0) {
            return listTypeHistoryChain.pop();
        }
        return null;
    }

    public boolean haveHistoryChain() {
        return userKeyHistoryChain.size()>0;
    }
}
