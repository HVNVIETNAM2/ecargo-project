package com.mm.main.app.activity.storefront.im.model;

/**
 * Created by madhur on 17/01/15.
 */
public enum UserType {
    OTHER_CHAT_TEXT,
    OTHER_CHAT_AUDIO,
    OTHER_CHAT_VIDEO,
    OTHER_CHAT_IMAGE,
    OTHER_CHAT_PRODUCT,
    OTHER_CHAT_CONTACT,
    SELF_CHAT_TEXT,
    SELF_CHAT_AUDIO,
    SELF_CHAT_VIDEO,
    SELF_CHAT_IMAGE,
    SELF_CHAT_PRODUCT,
    SELF_CHAT_CONTACT
};
