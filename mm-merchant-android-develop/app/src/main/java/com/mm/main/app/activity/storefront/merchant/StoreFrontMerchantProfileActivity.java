package com.mm.main.app.activity.storefront.merchant;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.cart.ShoppingCartActivity;
import com.mm.main.app.activity.storefront.follower.FollowerActivity;
import com.mm.main.app.activity.storefront.search.ProductListSearchActivity;
import com.mm.main.app.activity.storefront.wishlist.WishListActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.schema.Follow;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.Style;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.utils.PathUtil;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by binhtruongp on 3/9/2016.
 */
public class StoreFrontMerchantProfileActivity extends AppCompatActivity {
    public static String FOLLOWER_COUNT_KEY = "";
    public static String MERCHANT_ID_KEY = "";

    private boolean mIsToolbarCollapsed = false;
    private boolean beFollowed = true;
    int merchantID;
    @Bind(R.id.ivCompanyLogo)
    ImageView ivCompanyLogo;

    @Bind(R.id.ivMainBanner)
    ImageView ivMainBanner;

    @Bind(R.id.ivBrandImageView)
    ImageView ivBrandImageView;

    @Bind(R.id.tvFollowerLabel)
    TextView tvFollowerLabel;

    @Bind(R.id.tvMerchantName)
    TextView tvMerchantName;

    @Bind(R.id.tvProductScore)
    TextView tvProductScore;

    @Bind(R.id.tvCustomerServiceScore)
    TextView tvCustomerServiceScore;

    @Bind(R.id.tvShipmentRating)
    TextView tvShipmentRating;

    @Bind(R.id.tvMerchantProfileDes)
    TextView tvMerchantProfileDes;

    @Bind(R.id.btnSearch)
    ImageButton btnSearch;


    @OnClick(R.id.btnShare)
    void processShare() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, tvMerchantName.getText());
        startActivity(Intent.createChooser(intent, "Happy Share"));
    }

    @Bind(R.id.btnFollow)
    Button btnFollow;

    @OnClick(R.id.btnFollow)
    void handleFollowRequest() {
        Follow followRequest = new Follow();
        followRequest.setUserKey(MmGlobal.getUserKey());
        followRequest.setToMerchantId(String.valueOf(merchantID));
        if (beFollowed) {
            APIManager.getInstance().getFollowService().deleteFollowMerchant(followRequest).enqueue(new MmCallBack<Boolean>(this) {
                @Override
                public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                    if (response.isSuccess() && response.body().equals(true)) {
                        beFollowed = false;
                        btnFollow.setText(R.string.LB_CA_FOLLOW);
                        btnFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_add, 0, 0, 0);
                    }
                }
            });
        } else {
            APIManager.getInstance().getFollowService().saveFollowMerchant(followRequest).enqueue(new MmCallBack<Boolean>(this) {
                @Override
                public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                    if (response.isSuccess() && response.body().equals(true)) {
                        beFollowed = true;
                        MmStatusAlertUtil.show(StoreFrontMerchantProfileActivity.this, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                getString(R.string.MSG_SUC_FOLLOWED), null);
                        btnFollow.setText(R.string.LB_CA_FOLLOWED);
                        btnFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ok, 0, 0, 0);
                    }
                }
            });
        }
    }

    @Bind(R.id.btnCS)
    Button btnCS;

    @OnClick(R.id.btnCS)
    void sendIM() {

    }


    @OnClick(R.id.rlFollower)
    void startFollowerActivity() {
        Intent intent = new Intent(this, FollowerActivity.class);
        intent.putExtra(FollowerActivity.USER_KEY, MmGlobal.getUserKey());
        intent.putExtra(FollowerActivity.FOLLOWER_TYPE_KEY, FollowerActivity.FOLLOWER_MERCHANT);
        intent.putExtra(FollowerActivity.MERCHANT_ID, merchantID);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_public_profile);
        ButterKnife.bind(this);
        setupToolbar();
        merchantID = getIntent().getIntExtra(MERCHANT_ID_KEY, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMerchantDetail(merchantID);
    }

    private void getMerchantDetail(int merchantID) {
        APIManager.getInstance().getViewService().viewMerchant(merchantID).enqueue(new MmCallBack<Merchant>(this) {
            @Override
            public void onSuccess(Response<Merchant> response, Retrofit retrofit) {
                response.toString();
                if (response.isSuccess()) {

                    Merchant merchant = response.body();
                    String url = PathUtil.getMerchantImageUrl(merchant.getProfileBannerImage());
                    Picasso.with(MyApplication.getContext()).load(url).into(ivMainBanner);

                    String headerUrl = PathUtil.getMerchantImageUrl(merchant.getHeaderLogoImage());
                    Picasso.with(MyApplication.getContext()).load(headerUrl).into(ivCompanyLogo);
                    Picasso.with(MyApplication.getContext()).load(headerUrl).into(ivBrandImageView);

                    tvMerchantName.setText(merchant.getMerchantName());
                    tvMerchantProfileDes.setText(merchant.getMerchantDesc());
                    tvFollowerLabel.setText(String.format("%s %s", getString(R.string.LB_CA_FOLLOWER), merchant.getFollowerCount()));
                    tvProductScore.setText(String.format("%s %s", getString(R.string.LB_CA_PROD_DESC), " 5.0"));
                    tvShipmentRating.setText(String.format("%s %s", getString(R.string.LB_CA_SHIPMENT_RATING), " 5.0"));
                    tvCustomerServiceScore.setText(String.format("%s %s", getString(R.string.LB_CA_CUST_SERVICE), " 5.0"));
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_profile, menu);

        //for heart icon
        MenuItem item = menu.findItem(R.id.action_like);
        MenuItemCompat.setActionView(item, R.layout.badge_button_like);

        RelativeLayout badgeLayout = (RelativeLayout) MenuItemCompat.getActionView(item);
        View badge = badgeLayout.findViewById(R.id.btnBadge);
        ImageView heartIcon = (ImageView) badgeLayout.findViewById(R.id.icon_heart_stroke);

        if (mIsToolbarCollapsed) {
            heartIcon.setImageResource(R.drawable.wishlist_grey);
        } else {
            heartIcon.setImageResource(R.drawable.wishlist_wht);
        }

        if (CacheManager.getInstance().isWishlistHasItem()) {
            badge.setVisibility(View.VISIBLE);
        } else {
            badge.setVisibility(View.GONE);
        }

        item.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StoreFrontMerchantProfileActivity.this, WishListActivity.class);
                startActivity(i);
            }
        });

        //for cart icon
        MenuItem cartItem = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(cartItem, R.layout.badge_button_cart);

        RelativeLayout badgeCartLayout = (RelativeLayout) MenuItemCompat.getActionView(cartItem);
        View cartBadge = badgeCartLayout.findViewById(R.id.btnBadge);
        ImageView cartIcon = (ImageView) badgeCartLayout.findViewById(R.id.cartIcon);

        if (mIsToolbarCollapsed) {
            cartIcon.setImageResource(R.drawable.cart_grey);
        } else {
            cartIcon.setImageResource(R.drawable.cart_wht);
        }

        if (CacheManager.getInstance().isCartHasItem()) {
            cartBadge.setVisibility(View.VISIBLE);
        } else {
            cartBadge.setVisibility(View.GONE);
        }

        cartItem.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StoreFrontMerchantProfileActivity.this, ShoppingCartActivity.class);
                startActivity(i);
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        AppBarLayout appBar = (AppBarLayout) findViewById(R.id.appbar);
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int scrollRange = appBarLayout.getTotalScrollRange();
                int toolbarHeight = getSupportActionBar().getHeight();
                if (scrollRange + verticalOffset <= toolbarHeight) {
                    if (!mIsToolbarCollapsed) {
                        mIsToolbarCollapsed = true;
                        ivCompanyLogo.setVisibility(View.VISIBLE);
                        btnSearch.setImageResource(R.drawable.btn_search);
                        invalidateOptionsMenu();
                    }
                } else if (mIsToolbarCollapsed) {
                    ivCompanyLogo.setVisibility(View.INVISIBLE);
                    mIsToolbarCollapsed = false;
                    btnSearch.setImageResource(R.drawable.search_wht);
                    invalidateOptionsMenu();
                }
            }
        });

        toolbar.setContentInsetsAbsolute(0, 0);
    }

    @OnClick(R.id.btnSearch)
    public void startSearch() {
        Intent intent = new Intent(this, ProductListSearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rlDescription)
    void startMerchantDescription() {
        Intent intent = new Intent(this, StoreFrontMerchantDescriptionActivity.class);
        int merchantID = getIntent().getIntExtra(MERCHANT_ID_KEY, 0);
        Bundle bundle = new Bundle();
        bundle.putInt(MERCHANT_ID_KEY, merchantID);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
