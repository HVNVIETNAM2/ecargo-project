package com.mm.main.app.schema.response;

import com.mm.main.app.schema.Product;

import java.util.List;

/**
 * Created by andrew on 16/11/15.
 */
public class ProductResponse {
    List<Product> results;

    public ProductResponse() {
    }

    public List<Product> getResults() {
        return results;
    }

    public void setResults(List<Product> results) {
        this.results = results;
    }
}
