package com.mm.main.app.listitem;

import com.mm.main.app.schema.Cart.CartMerchant;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class ShoppingCartMerchantRvItem implements ShoppingCartRvItem, Serializable {
    private boolean isSelected;
    private CartMerchant merchant;
    List<ShoppingCartProductRvItem> productItems;
    private int appiumIndex;

    public ShoppingCartMerchantRvItem(CartMerchant merchant) {
        this.merchant = merchant;
    }

    public CartMerchant getMerchant() {
        return merchant;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public ItemType getType() {
        return ItemType.TYPE_SHOPPING_CART_MERCHANT;
    }

    public List<ShoppingCartProductRvItem> getProductItems() {
        return productItems;
    }

    public void setProductItems(List<ShoppingCartProductRvItem> productItems) {
        this.productItems = productItems;
    }

    @Override
    public void setAppiumIndex(int appiumIndex) {
        this.appiumIndex = appiumIndex;
    }

    @Override
    public int getAppiumIndex() {
        return appiumIndex;
    }
}
