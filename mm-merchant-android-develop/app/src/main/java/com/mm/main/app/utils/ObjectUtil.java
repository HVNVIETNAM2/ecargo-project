package com.mm.main.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

/**
 * Created by henrytung on 28/10/15.
 */
public class ObjectUtil {
    public static String serializeObject(Object o) {
        Gson gson = new Gson();
        String serializedObject = gson.toJson(o);
        return serializedObject;
    }

    public static Object unserializeObject(String s, Object o){
        Gson gson = new Gson();
        Object object = gson.fromJson(s, o.getClass());
        return object;
    }

    public static Object cloneObject(Object o){
        String s = serializeObject(o);
        Object object = unserializeObject(s, o);
        return object;
    }

    public static <T> void saveComplexObject(Context context, T object, String key){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, new Gson().toJson(object));
        editor.commit();
    }

    public static <T> T loadComplexObject (Context context, Class<T> objectClass, String key){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String sobj = preferences.getString(key, "");
        if(sobj.equals(""))return null;
        else return new Gson().fromJson(sobj, objectClass);
    }

    public static void removeObject(Context context, String key){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().remove(key).commit();
    }
}
