package com.mm.main.app.log;

import android.util.Log;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.ConfigManager;

/**
 * Created by henrytung on 12/2/2016.
 */
public class Logger {

    private static boolean enableLog = ConfigManager.getInstance().getConfig().isEnableLog();

    public static void d(String tag, String message) {
        if (enableLog) {
            Log.d(tag, message);
        }
    }

    public static void e(String tag, String message) {
        if (enableLog) {
            Log.e(tag, message);
        }
    }

    public static void i(String tag, String message) {
        if (enableLog) {
            Log.i(tag, message);
        }
    }

    public static void v(String tag, String message) {
        if (enableLog) {
            Log.v(tag, message);
        }
    }

    public static void w(String tag, String message) {
        if (enableLog) {
            Log.w(tag, message);
        }
    }

    public static void d(String tag, String message, String... parameters) {
        if (enableLog) {
            Log.d(tag, generateMessage(message, parameters));
        }
    }

    public static void e(String tag, String message, String... parameters) {
        if (enableLog) {
            Log.e(tag, generateMessage(message, parameters));
        }
    }

    public static void i(String tag, String message, String... parameters) {
        if (enableLog) {
            Log.i(tag, generateMessage(message, parameters));
        }
    }

    public static void v(String tag, String message, String... parameters) {
        if (enableLog) {
            Log.v(tag, generateMessage(message, parameters));
        }
    }

    public static void w(String tag, String message, String... parameters) {
        if (enableLog) {
            Log.w(tag, generateMessage(message, parameters));
        }
    }

    public static void e(String tag, String message, Throwable throwable) {
        if (enableLog) {
            Log.e(tag, message, throwable);
        }
    }


    private static String generateMessage(String message, String... parameters) {
        String result = message + " - ";
        for (String parameter : parameters) {
            result += parameter;
            result += " ";
        }

        return result;
    }


}
