package com.mm.main.app.listitem;

import com.mm.main.app.schema.Address;

/**
 * Created by thienchaud on 19-Feb-16.
 */
public class AddressRvItem {

    Address address;
    boolean isSelected;

    public AddressRvItem(Address address, boolean isSelected) {
        this.address = address;
        this.isSelected = isSelected;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
