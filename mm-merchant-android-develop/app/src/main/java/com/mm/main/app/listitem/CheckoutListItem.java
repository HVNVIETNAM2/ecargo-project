package com.mm.main.app.listitem;

/**
 * Setting schema
 */
public class CheckoutListItem {

    String title;
    String content;
    ItemType type;

    public enum ItemType {
        TYPE_SIZE,
        TYPE_COLOR,
        TYPE_QUANTITY,
        TYPE_SHIPPING_ADDRESS,
        TYPE_PAYMENT,
        TYPE_FAPIAO
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public CheckoutListItem() {
    }

    public CheckoutListItem(String title, String content, ItemType type) {
        this.title = title;
        this.content = content;
        this.type = type;
    }
}
