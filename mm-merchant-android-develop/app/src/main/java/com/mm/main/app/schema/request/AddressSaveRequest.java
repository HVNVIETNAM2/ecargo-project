package com.mm.main.app.schema.request;

/**
 * Created by thienchaud on 07-Mar-16.
 */
public class AddressSaveRequest {

    String UserKey;
    String RecipientName;
    String GeoCountryId;
    String GeoProvinceId;
    String GeoCityId;
    String PhoneCode;
    String PhoneNumber;
    Boolean IsDefault;
    String Address;
    String PostalCode;

    public AddressSaveRequest(String userKey, String recipientName, String geoCountryId, String geoProvinceId, String geoCityId, String phoneCode, String phoneNumber, Boolean isDefault, String address, String postalCode) {
        UserKey = userKey;
        RecipientName = recipientName;
        GeoCountryId = geoCountryId;
        GeoProvinceId = geoProvinceId;
        GeoCityId = geoCityId;
        PhoneCode = phoneCode;
        PhoneNumber = phoneNumber;
        IsDefault = isDefault;
        Address = address;
        PostalCode = postalCode;
    }
}
