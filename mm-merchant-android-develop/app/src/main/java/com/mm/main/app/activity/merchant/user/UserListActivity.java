package com.mm.main.app.activity.merchant.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.mm.main.app.activity.storefront.product.ProductListActivity;
import com.mm.main.app.R;
import com.mm.main.app.adapter.merchant.user.UserListAdapter;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.User;
import com.mm.main.app.view.MmProgressDialog;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class UserListActivity extends ActionBarActivity {

    public static final String TAG = "User List Activity: ";

    private ListView userListView;
    private List<User> userList;
    private UserListAdapter userListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list_activitiy);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        userList = new ArrayList<>();
        userListView = (ListView) findViewById(R.id.userListView);
        MmProgressDialog.show(this);
        APIManager.getInstance().getUserService().list()
                .enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Response<List<User>> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            userList = response.body();
                            userListAdapter = new UserListAdapter(UserListActivity.this, userList);
                            userListView.setAdapter(userListAdapter);
                            userListAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(getApplicationContext(), "You are not authenticated",
                                    Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Toast.makeText(getApplicationContext(), "You are not connected to the network",
                                Toast.LENGTH_LONG).show();
                    }
                });

        userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(UserListActivity.this, ProductListActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_list_activitiy, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
