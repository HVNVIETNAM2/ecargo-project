package com.mm.main.app.activity.storefront.friend;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.mm.main.app.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FindNewUserActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_new_user);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.rlSearchUser)
    public void searchUserOnClick(){
        Intent intent = new Intent(FindNewUserActivity.this, ImSearchUserActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rlScanQR)
    public void scanQROnClick(){
        Intent intent = new Intent(FindNewUserActivity.this, QRScanActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rlSMS)
    public void smsOnClick(){
        //TODO : goto sms
    }

    @OnClick(R.id.tvSeeQRCode)
    public void seeQRCodeOnClick(){
        Intent intent = new Intent(FindNewUserActivity.this,UserQrCodeActivity.class);
        startActivity(intent);
    }
}
