package com.mm.main.app.activity.merchant.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mm.main.app.R;
import com.mm.main.app.factory.ValidatorFactory;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.concurrent.Callable;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

public class ChangeEmailActivity extends ActionBarActivity {

    final public static String EXTRA_USER_DETAIL = "USER_DETAIL";

    @Bind(R.id.currentEmailLabel)
    TextView currentEmailLabel;
    @Bind(R.id.newEmailEditText)
    EditText newEmailEditText;
    @Bind(R.id.confirmEmailEditText)
    EditText confirmEmailEditText;

    @Bind(R.id.newEmailLayout)
    TextInputLayout newEmailLayout;
    @Bind(R.id.confirmEmailLayout)
    TextInputLayout confirmEmailLayout;

    @BindString(R.string.MSG_SUC_EMAIL_CHANGE)
    String changeSucMessage;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_email);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, getResources().getString(R.string.LB_CHANGE_EMAIL));
        user = (User) this.getIntent().getSerializableExtra(EXTRA_USER_DETAIL);
        currentEmailLabel.setText(getResources().getString(R.string.LB_CURRENT_EMAIL) + user.getEmail());

        setupValidation();
    }

    private void setupValidation() {
        newEmailEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateNewEmail();
            }
        }, newEmailLayout));

        confirmEmailEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateConfirmEmail();
            }
        }, confirmEmailLayout));
    }


    @OnClick(R.id.confirm_button)
    public void changeEmail() {
        if (validateNewEmail() & validateConfirmEmail() & matchEmails()) {
            user.setEmail(newEmailEditText.getText().toString());
            MmProgressDialog.show(this);
            APIManager.getInstance().getUserService().changeEmail(user)
                    .enqueue(new MmCallBack<Boolean>(ChangeEmailActivity.this) {
                        @Override
                        public void onSuccess(Response response, Retrofit retrofit) {
                            Toast.makeText(ChangeEmailActivity.this, changeSucMessage, Toast.LENGTH_SHORT).show();
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    });
        }
    }

    @OnClick(R.id.cancel_button)
    public void cancelChange() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private boolean matchEmails() {
        if (!newEmailEditText.getText().toString().equals(confirmEmailEditText.getText().toString())) {
            ErrorUtil.showErrorDialog(this, null, getResources().getString(R.string.MSG_ERR_EMAIL_NOT_MATCH));
            return false;
        }

        return true;
    }

    private boolean validateNewEmail() {
        String error = null;

        String fieldName = getResources().getString(R.string.LB_NEW_EMAIL);
        String value = newEmailEditText.getText().toString();

        if (!ValidationUtil.isValidEmail(value)) {
            error = getResources().getString(R.string.MSG_ERR_EMAIL_PATTERN);
        }
        if (ValidationUtil.isEmpty(value)) {
            error = getResources().getString(R.string.MSG_ERR_EMAIL_NIL);
        }

        ValidationUtil.setErrorMessage(newEmailLayout, error);

        return error == null;
    }

    private boolean validateConfirmEmail() {
        String error = null;
        String fieldName = getResources().getString(R.string.LB_CONF_NEW_EMAIL);
        String value = confirmEmailEditText.getText().toString();

        if (!ValidationUtil.isValidEmail(value)) {
            error = getResources().getString(R.string.MSG_ERR_EMAIL_PATTERN);
        }
        if (ValidationUtil.isEmpty(value)) {
            error = getResources().getString(R.string.MSG_ERR_EMAIL_REENTER_NIL);
        }

        ValidationUtil.setErrorMessage(confirmEmailLayout, error);

        return error == null;
    }

}
