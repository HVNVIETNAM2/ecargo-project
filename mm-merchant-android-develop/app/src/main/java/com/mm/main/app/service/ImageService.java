package com.mm.main.app.service;

import com.mm.main.app.schema.response.UploadImageResponse;
import com.squareup.okhttp.RequestBody;

import retrofit.Call;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by andrew on 30/9/15.
 */
public interface ImageService {

    String IMAGE_PATH = "image";

    @Multipart
    @POST(IMAGE_PATH + "/upload")
    Call<UploadImageResponse> uploadImage(@Part("data") RequestBody file, @Part("name") String name, @Part("fileName") String fileName, @Part("mimeType") String mimeType);

    @Multipart
    @POST(IMAGE_PATH + "/upload")
    Call<UploadImageResponse> uploadImage(@Part("data") RequestBody file);
}
