package com.mm.main.app.activity.storefront.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.ShoppingCartMerchantRvItem;
import com.mm.main.app.listitem.ShoppingCartProductRvItem;
import com.mm.main.app.adapter.strorefront.checkout.CheckoutConfirmRVAdapter;
import com.mm.main.app.listitem.CheckoutConfirmMerchantRvItem;
import com.mm.main.app.listitem.CheckoutConfirmMerchantTotalPriceRvItem;
import com.mm.main.app.listitem.CheckoutConfirmProductRvItem;
import com.mm.main.app.listitem.CheckoutConfirmRvItem;
import com.mm.main.app.listitem.CheckoutConfirmShippingAddressRvItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CheckoutFlowManager;
import com.mm.main.app.schema.Address;
import com.mm.main.app.schema.CartItem;
import com.mm.main.app.schema.SimpleCartItem;
import com.mm.main.app.schema.request.OrderCreateRequest;
import com.mm.main.app.schema.response.Order;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class CheckoutConfirmActivity extends AppCompatActivity {

    @Bind(R.id.checkout_list)
    RecyclerView mRecyclerView;


    @Bind(R.id.textViewTotalAmount)
    TextView textViewTotalAmount;

    CheckoutConfirmRVAdapter mAdapter;
    List<CheckoutConfirmRvItem> items;

    ArrayList<ShoppingCartMerchantRvItem> merchantList;

    List<SimpleCartItem> Skus = new ArrayList<SimpleCartItem>();

    Address address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_confirm);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        items = new ArrayList<CheckoutConfirmRvItem>();

        merchantList = CheckoutFlowManager.getInstance().getSelectedShoppingCartData();

        loadDataForCart(merchantList);

        loadAddress();

    }

    private void loadAddress() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getAddressService().loadDefaultAddress(MmGlobal.getUserKey())
                .enqueue(new MmCallBack<Address>(CheckoutConfirmActivity.this, 500) {

                    public void setupWithAddress(Address address) {
                        //RecyclerView.Adapter
                        mAdapter = new CheckoutConfirmRVAdapter(CheckoutConfirmActivity.this, items, MmGlobal.anonymousShoppingCartKey());
                        mAdapter.setAddress(address);

                        //Apply this adapter to the RecyclerView
                        mRecyclerView.setAdapter(mAdapter);
                    }

                    @Override
                    public void onSuccess(Response<Address> response, Retrofit retrofit) {
                        address = (Address) response.body();
                        setupWithAddress(address);
                    }

                    @Override
                    public void onFailWithErrorCode(Response<Address> response, Retrofit retrofit) {
                        super.onFailWithErrorCode(response, retrofit);
                        setupWithAddress(null);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
            switch (requestCode) {
                case AddressSelectionActivity.ADD_ADDRESS_CODE_REQUEST:
                    address = (Address) data.getSerializableExtra(Constant.RESULT);
                    mAdapter.setAddress(address);
                    mAdapter.notifyDataSetChanged();
                    break;
            }
    }


    public void loadDataForCart(ArrayList<ShoppingCartMerchantRvItem> merchantList) {
        items.clear();

        items.add(new CheckoutConfirmShippingAddressRvItem());

        double checkout_total_price = 0;
        for (ShoppingCartMerchantRvItem merchant : merchantList) {

            double merchant_total_price = 0;
            CheckoutConfirmMerchantRvItem merchantItem = new CheckoutConfirmMerchantRvItem(merchant.getMerchant());
            items.add(merchantItem);

            List<ShoppingCartProductRvItem> products = merchant.getProductItems();
            List<CheckoutConfirmProductRvItem> productListRows = new ArrayList<CheckoutConfirmProductRvItem>();

            for (ShoppingCartProductRvItem product : products) {
                if (product.getSelected()) {
                    CheckoutConfirmProductRvItem productItem = new CheckoutConfirmProductRvItem(product.getProduct(), merchantItem);
                    items.add(productItem);

                    productListRows.add(productItem);

                    Skus.add(new SimpleCartItem(product.getProduct().getSkuId(), product.getProduct().getQty()));

                    //for price
                    merchant_total_price += product.getProduct().getPrice() * product.getProduct().getQty();
                }
            }

            //set the reference
            merchantItem.setProductItems(productListRows);

            items.add(new CheckoutConfirmMerchantTotalPriceRvItem(merchantItem));

            //show price
            checkout_total_price += merchant_total_price;
        }
        if (checkout_total_price == 0) {
            textViewTotalAmount.setText("0.0");
        } else {
            textViewTotalAmount.setText(NumberFormat.getNumberInstance(Locale.CHINA).format(checkout_total_price));
        }
    }

    @OnClick(R.id.btn_submit_order)
    public void btnSubmitClick() {
        if (address == null) {
            ErrorUtil.showErrorDialog(CheckoutConfirmActivity.this, "Please add addr");
            return;
        }
        //create order
        APIManager.getInstance().getOrderService().createOrder(new OrderCreateRequest(MmGlobal.getUserKey(), address.getUserAddressKey(), Skus))
                .enqueue(new MmCallBack<Order>(CheckoutConfirmActivity.this) {
                    @Override
                    public void onSuccess(Response<Order> response, Retrofit retrofit) {
                        Order order = (Order) response.body();

                        if (order != null) {
                            if (order.getIsCrossBorder() && !order.getIsUserIdentificationExists()) {
                                Intent intent = new Intent(CheckoutConfirmActivity.this, IDCollectionActivity.class);
                                intent.putExtra(IDCollectionActivity.ORDER_ID_KEY, order.getOrderKey());
                                startActivity(intent);
                            } else if (order.getIsUserIdentificationExists() || !order.getIsCrossBorder()) {
                                Intent intent = new Intent(CheckoutConfirmActivity.this, PaymentSelectionActivity.class);
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        super.onFailure(t);
                    }
                });
    }

}
