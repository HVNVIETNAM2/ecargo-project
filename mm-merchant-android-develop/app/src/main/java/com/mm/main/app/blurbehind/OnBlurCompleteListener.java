package com.mm.main.app.blurbehind;

/**
 * Created by gergun on 13/05/15.
 */
public interface OnBlurCompleteListener {

    public void onBlurComplete();
}
