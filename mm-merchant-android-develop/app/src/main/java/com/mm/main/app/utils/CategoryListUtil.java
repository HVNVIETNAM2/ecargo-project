package com.mm.main.app.utils;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.schema.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by henrytung on 21/12/2015.
 */
public class CategoryListUtil {

    public static List<Category> filterCategory(List<Category> oldCategoryList) {
        List<Category> newCategoryList = new ArrayList<>();

        Category allCategory = new Category();
        allCategory.setCategoryId(-1);
        allCategory.setCategoryName(MyApplication.getContext().getResources().getString(R.string.LB_CA_ALL));
        newCategoryList.add(allCategory);

        for (Category category :
                oldCategoryList) {
            if (!category.getCategoryName().equals("---")) {
                newCategoryList.add(category);
            }
        }
        return newCategoryList;
    }

    public static List<Category> cleanupCategory(List<Category> oldCategoryList) {
        List<Category> newCategoryList = new ArrayList<>();
        for (Category category :
                oldCategoryList) {
            if (!category.getCategoryName().equals("---")) {
                newCategoryList.add(category);
            }
        }
        return newCategoryList;
    }
}
