package com.mm.main.app.adapter.strorefront.product;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.activity.storefront.product.ProductDetailPageActivity;
import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.product.ProductListActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.manager.WishlistActionUtil;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.schema.Color;
import com.mm.main.app.schema.ColorImage;
import com.mm.main.app.schema.ProductColor;
import com.mm.main.app.schema.Style;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.StringUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Adapter for Recycle View, View Holder Pattern with getView function!
 */
public class ProductRVAdapter extends RecyclerView.Adapter<ProductRVAdapter.ProductViewHolder> {

    public interface ProductListOnClickListener {
        void onClick(int position, Object data);
    }

    List<Style> productList;
    ProductListOnClickListener productListOnClickListener;

    Context context;

    public boolean[] likes;

    public ProductRVAdapter(List<Style> productList, ProductListOnClickListener productListOnClickListener) {
        this.productList = productList;
        likes = new boolean[productList.size()];
        for (int i = 0; i < productList.size(); i++) {
            likes[i] = false;
        }

        this.productListOnClickListener = productListOnClickListener;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_item, viewGroup, false);
        setSize(v);

        ProductViewHolder.ProductViewHolderOnClickListener productOnClickListener = new ProductViewHolder.ProductViewHolderOnClickListener() {
            @Override
            public void onClick(int position) {
                proceedProductDetail(position);
            }
        };
        return new ProductViewHolder(v, productList, productOnClickListener);
    }

    public void proceedProductDetail(int position) {
        if (productListOnClickListener == null) {
            Intent intent = new Intent(MyApplication.getContext(), ProductDetailPageActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(ProductDetailPageActivity.EXTRA_PRODUCT_DATA, productList.get(position));
            MyApplication.getContext().startActivity(intent);
        } else {
            productListOnClickListener.onClick(position, false);
        }
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder productViewHolder, final int i) {
        final Style currentStyle = productList.get(i);

        productViewHolder.firstName.setText(currentStyle.getSkuName());

        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CHINA);
        format.setMaximumFractionDigits(0);
        if (currentStyle.getPriceSale() > 0) {
            productViewHolder.salesDisplayPrice.setVisibility(View.VISIBLE);
            productViewHolder.lastName.setVisibility(View.GONE);
            productViewHolder.salesPrice.setText(format.format(currentStyle.getPriceSale()));
            productViewHolder.originalPrice.setText(format.format(currentStyle.getPriceRetail()));
            StringUtil.setStrikeThroughText(productViewHolder.originalPrice);
        } else {
            productViewHolder.salesDisplayPrice.setVisibility(View.GONE);
            productViewHolder.lastName.setVisibility(View.VISIBLE);
            productViewHolder.lastName.setText(format.format(currentStyle.getPriceRetail()));
        }

        renderProductImage(productViewHolder, currentStyle);

        if (productViewHolder.btnLike != null) {
            productViewHolder.btnLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    WishlistActionUtil.actionWishlist(v, currentStyle, null, context, new WishlistActionUtil.WishListActionListener() {
                        @Override
                        public void completedAction(boolean isSuccess) {
                            if (ProductRVAdapter.this instanceof ProductRVOneLineAdapter) {
                                ProductRVOneLineAdapter oneLineAdapter = (ProductRVOneLineAdapter) ProductRVAdapter.this;
                                if (oneLineAdapter.detailRvAdapter != null) {
                                    oneLineAdapter.detailRvAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    });
                }
            });
        }
        productViewHolder.btnLike.setSelected(currentStyle.isWished());

        Picasso.with(MyApplication.getContext())
                .load(PathUtil.getBrandImageUrl(currentStyle.getBrandHeaderLogoImage()))
                .into(productViewHolder.brandPhoto);
    }

    public void renderProductImage(final ProductViewHolder productViewHolder, final Style currentStyle) {

        String imageKey = "";

        if (SearchStyle.getInstance().getColorid() != null && SearchStyle.getInstance().getColorid().size() == 1) {
            ProductColor styleColor = getColorFromId(currentStyle.getColorList(), SearchStyle.getInstance().getColorid().get(0));
            if (styleColor != null) {
                List<ColorImage> filteredColorImageList = getColorImageList(currentStyle.getColorImageList(), styleColor);
                if (filteredColorImageList != null && filteredColorImageList.size() > 0) {
                    imageKey = filteredColorImageList.get(0).getImageKey();
                } else {
                    imageKey = currentStyle.getImageDefault();
                }
            } else {
                imageKey = currentStyle.getImageDefault();
            }
        } else {
            imageKey = currentStyle.getImageDefault();
        }

        Picasso.with(MyApplication.getContext())
                .load(PathUtil.getProductImageUrl(imageKey, UiUtil.getPixelFromDp(180)))
                .into(productViewHolder.photo);
    }

    private List<ColorImage> getColorImageList(List<ColorImage> styleColorImageList, ProductColor productColor) {
        List<ColorImage> filteredColorImageList = new ArrayList<>();
        for (ColorImage styleColorImage : styleColorImageList) {
            if (styleColorImage.getColorKey().toUpperCase().equals(productColor.getColorKey().toUpperCase())) {
                filteredColorImageList.add(styleColorImage);
            }
        }

        return filteredColorImageList;
    }

    private ProductColor getColorFromId(List<ProductColor> styleColorList, Color filterColor) {
        for (ProductColor styleColor : styleColorList) {
            if (styleColor.getColorId().intValue() == filterColor.getColorId().intValue()) {
                return styleColor;
            }
        }
        return null;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    void setSize(View v) {

    }


    public static class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private static final String TAG = "Inside Product View Holder";
        CardView cv;
        TextView firstName;
        TextView lastName;
        TextView salesPrice;
        TextView originalPrice;
        ImageView photo;
        ImageView brandPhoto;
        ImageView btnLike;
        List<Style> productList;
        LinearLayout salesDisplayPrice;
        ProductViewHolderOnClickListener productViewHolderOnClickListener;

        public interface ProductViewHolderOnClickListener {
            void onClick(int position);
        }

        ProductViewHolder(View itemView, List<Style> productList, ProductViewHolderOnClickListener productViewHolderOnClickListener) {
            super(itemView);
            this.productList = productList;
            cv = (CardView) itemView.findViewById(R.id.cv);
            firstName = (TextView) itemView.findViewById(R.id.user_first_name);
            lastName = (TextView) itemView.findViewById(R.id.user_last_name);
            salesPrice = (TextView) itemView.findViewById(R.id.salesPrice);
            originalPrice = (TextView) itemView.findViewById(R.id.originalPrice);
            salesDisplayPrice = (LinearLayout) itemView.findViewById(R.id.salesDisplayPrice);
            photo = (ImageView) itemView.findViewById(R.id.user_photo);
            brandPhoto = (ImageView) itemView.findViewById(R.id.brand_photo);
            btnLike = (ImageView) itemView.findViewById(R.id.buttonLike);
            this.productViewHolderOnClickListener = productViewHolderOnClickListener;
            int imageWith = UiUtil.getAspectWidthOfProductImage();
            photo.setLayoutParams(new RelativeLayout.LayoutParams(imageWith, UiUtil.getHeightByWidthWithRatio(imageWith, UiUtil.productImgSizeRatio)));
            itemView.setOnClickListener(this);
        }


        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            productViewHolderOnClickListener.onClick(getPosition());
        }
    }

    public List<Style> getProductList() {
        return productList;
    }

    public void setProductList(List<Style> productList) {
        this.productList = productList;
    }
}
