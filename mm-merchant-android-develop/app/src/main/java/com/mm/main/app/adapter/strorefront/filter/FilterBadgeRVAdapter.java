package com.mm.main.app.adapter.strorefront.filter;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Badge;
import com.mm.main.app.utils.UiUtil;

import java.util.List;

/**
 * Adapter for Recycle View, View Holder Pattern with getView function!
 */
public class FilterBadgeRVAdapter extends RecyclerView.Adapter<FilterBadgeRVAdapter.BadgeViewHolder> {

    public interface BadgeOnClickListener {
        void onClick(int position, Object data);
    }

    List<FilterListItem<Badge>> itemList;
    BadgeOnClickListener badgeOnClickListener;
    Drawable[] drawable;


    public FilterBadgeRVAdapter(List<FilterListItem<Badge>> itemList, BadgeOnClickListener badgeOnClickListener) {
        this.itemList = itemList;
        this.badgeOnClickListener = badgeOnClickListener;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public BadgeViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.filter_badge_item, viewGroup, false);
        setSize(v);

        BadgeViewHolder.BadgeItemOnClickListener badgeItemOnClickListener = new BadgeViewHolder.BadgeItemOnClickListener() {
            @Override
            public void onClick(int position) {
                itemList.get(position).setSelected(!itemList.get(position).isSelected());
                notifyDataSetChanged();
                proceedProductDetail(position);
            }
        };
        return new BadgeViewHolder(v, itemList, badgeItemOnClickListener);
    }

    public void proceedProductDetail(int position) {
        badgeOnClickListener.onClick(position, itemList);
    }

    @Override
    public void onBindViewHolder(final BadgeViewHolder badgeViewHolder, final int i) {
        badgeViewHolder.tvBadge.setText(itemList.get(i).getT().getBadgeName());
        if (itemList.get(i).isSelected()) {
            badgeViewHolder.tvBadge.setBackground(
                    MyApplication.getContext().getResources().getDrawable(R.drawable.filter_selection_background));
        } else {
            badgeViewHolder.tvBadge.setBackground(MyApplication.getContext().getResources().getDrawable(R.drawable.filter_unselection_background));
        }

        badgeViewHolder.tvBadge.setPadding(UiUtil.getPixelFromDp(9), 0, UiUtil.getPixelFromDp(9), 0);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    void setSize(View v) {
    }


    public static class BadgeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private static final String TAG = "Inside Product View Holder";
        View mainLayout;
        List<FilterListItem<Badge>> itemList;
        TextView tvBadge;
        BadgeItemOnClickListener badgeItemOnClickListener;

        public interface BadgeItemOnClickListener {
            void onClick(int position);
        }

        BadgeViewHolder(View itemView, List<FilterListItem<Badge>> itemList,
                        BadgeItemOnClickListener badgeItemOnClickListener) {
            super(itemView);
            this.itemList = itemList;
            tvBadge = (TextView) itemView.findViewById(R.id.tvBadge);
            mainLayout = itemView;

            this.badgeItemOnClickListener = badgeItemOnClickListener;

            itemView.setOnClickListener(this);
        }


        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            int selected = getPosition();
            badgeItemOnClickListener.onClick(selected);
        }
    }

    public List<FilterListItem<Badge>> getItemList() {
        return itemList;
    }

    public void setItemList(List<FilterListItem<Badge>> itemList) {
        this.itemList = itemList;
    }

    public void reset() {
        for (FilterListItem<Badge> item : itemList) {
            item.setSelected(false);
        }
        notifyDataSetChanged();
    }
}
