package com.mm.main.app.factory;

import android.support.design.widget.TextInputLayout;
import android.view.View;

import com.mm.main.app.log.Logger;
import com.mm.main.app.utils.ValidationUtil;

import java.util.concurrent.Callable;

/**
 * Created by henrytung on 12/11/15.
 */
public class ValidatorFactory {

    final public static String TAG = ValidatorFactory.class.toString();

    public static View.OnFocusChangeListener createOnFocusValidateListener(final Callable<Boolean> validateFunction, final TextInputLayout textInputLayout) {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    try {
                        validateFunction.call();
                    } catch (Exception e) {
                        Logger.d(TAG, e.getMessage());
                    }
                } else {
                    ValidationUtil.cleanErrorMessage(textInputLayout);
                }
            }
        };
    }
}
