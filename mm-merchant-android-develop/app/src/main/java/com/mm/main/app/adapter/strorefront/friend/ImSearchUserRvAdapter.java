package com.mm.main.app.adapter.strorefront.friend;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.ImSearchUserRvItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.Follow;
import com.mm.main.app.schema.FriendRequest;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.squareup.picasso.Picasso;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Response;
import retrofit.Retrofit;

public class ImSearchUserRvAdapter extends RecyclerView.Adapter<ImSearchUserRvAdapter.ImSearchUserItemViewHolder> {

    private final int CURATOR_USER = 1;

    public interface SearchUserCallback {
        void seeProfile(int position);
    }

    SearchUserCallback callback;
    Context context;
    List<ImSearchUserRvItem> data;

    public ImSearchUserRvAdapter(Context context, List<ImSearchUserRvItem> data, SearchUserCallback callback) {
        this.context = context;
        this.data = data;
        this.callback = callback;
    }

    @Override
    public ImSearchUserItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ImSearchUserItemViewHolder viewHolder = null;

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.im_search_user_item, parent, false);
        viewHolder = new ImSearchUserItemViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ImSearchUserItemViewHolder holder, final int position) {

        final ImSearchUserRvItem item = data.get(position);
        updateView(holder, item);

        holder.tvUserName.setText(item.getUser().getDisplayName());
        final User user = item.getUser();

        if (user.getIsCuratorUser() != null && user.getIsCuratorUser() == CURATOR_USER) {
            holder.imgIsCurator.setVisibility(View.VISIBLE);
            holder.imgProfile.setBorderWidth(UiUtil.getPixelFromDp(1));

        } else {
            holder.imgIsCurator.setVisibility(View.GONE);
            holder.imgProfile.setBorderWidth(UiUtil.getPixelFromDp(0));
        }

        String url = PathUtil.getUserImageUrl(user.getProfileImage());
        Picasso.with(MyApplication.getContext()).load(url)
                .placeholder(R.drawable.placeholder).into(holder.imgProfile);

        holder.btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getTypeFollowAction() == ImSearchUserRvItem.ItemType.TYPE_FOLLOW) {
                    sendFollowRequest(user.getUserKey(), holder.btnFollow, item);
                } else if (item.getTypeFollowAction() == ImSearchUserRvItem.ItemType.TYPE_FOLLOWED) {
                    cancelFollowRequest(user.getUserKey(), holder.btnFollow, item);
                }
            }
        });

        holder.btnAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getTypeFriendAction() == ImSearchUserRvItem.ItemType.TYPE_NOT_BEFRIENDED) {
                    sendFriendRequest(user.getUserKey(), holder.btnAddFriend, item);
                } else if (item.getTypeFriendAction() == ImSearchUserRvItem.ItemType.TYPE_BEFRIENDED) {
                    String confirmationContent = context.getString(R.string.LB_CA_REMOVE_FRD_CONF).replace("{0",user.getDisplayName());
                    ErrorUtil.showTwoOptionsDialog(context, "", confirmationContent, context.getString(R.string.LB_YES), context.getString(R.string.LB_NO), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            cancelFriendRequest(user.getUserKey(), holder.btnAddFriend, item);
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
            }
        });

        holder.rlProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.seeProfile(position);
            }
        });
    }

    private void sendFriendRequest(String toUserKey, final TextView btnStatus, final ImSearchUserRvItem type) {
        MmProgressDialog.show(context);
        FriendRequest friendRequest = new FriendRequest();
        friendRequest.setUserKey(MmGlobal.getUserKey());
        friendRequest.setToUserKey(toUserKey);

        APIManager.getInstance().getFriendService().request(friendRequest)
                .enqueue(new MmCallBack<Boolean>(context) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                        MmProgressDialog.dismiss();
                        if(response.isSuccess() && response.body().equals(true)) {
                            MmStatusAlertUtil.show(context, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                    context.getString(R.string.MSG_SUC_FRIEND_REQ_SENT), null);
                            type.setTypeFriendAction(ImSearchUserRvItem.ItemType.TYPE_BEFRIENDED);
                            updateFriendStatus(btnStatus, type.getTypeFriendAction());
                        }
                    }
                });
    }

    private void cancelFriendRequest(String toUserKey, final TextView btnStatus, final ImSearchUserRvItem type) {
        MmProgressDialog.show(context);
        FriendRequest friendRequest = new FriendRequest();
        friendRequest.setUserKey(MmGlobal.getUserKey());
        friendRequest.setToUserKey(toUserKey);

        APIManager.getInstance().getFriendService().deleteRequest(friendRequest)
                .enqueue(new MmCallBack<Boolean>(context) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                        MmProgressDialog.dismiss();
                        if(response.isSuccess() && response.body().equals(true)) {
                            type.setTypeFriendAction(ImSearchUserRvItem.ItemType.TYPE_NOT_BEFRIENDED);
                            updateFriendStatus(btnStatus, type.getTypeFriendAction());
                        }
                    }
                });
    }

    private void sendFollowRequest(String toUserKey, final TextView btnStatus, final ImSearchUserRvItem type) {
        MmProgressDialog.show(context);
        Follow followRequest = new Follow();
        followRequest.setUserKey(MmGlobal.getUserKey());
        followRequest.setToUserKey(toUserKey);

        APIManager.getInstance().getFollowService().saveFollowCurator(followRequest)
                .enqueue(new MmCallBack<Boolean>(context) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                        MmProgressDialog.dismiss();
                        if(response.isSuccess() && response.body().equals(true)) {
                            MmStatusAlertUtil.show(context, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                    context.getString(R.string.MSG_SUC_FOLLOWED), null);
                            type.setTypeFollowAction(ImSearchUserRvItem.ItemType.TYPE_FOLLOWED);
                            updateFollowStatus(btnStatus, type.getTypeFollowAction());
                        }
                    }
                });
    }

    private void cancelFollowRequest(String toUserKey, final TextView btnStatus, final ImSearchUserRvItem type) {
        MmProgressDialog.show(context);
        Follow followRequest = new Follow();
        followRequest.setUserKey(MmGlobal.getUserKey());
        followRequest.setToUserKey(toUserKey);

        APIManager.getInstance().getFollowService().deleteFollow(followRequest)
                .enqueue(new MmCallBack<Boolean>(context) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                        MmProgressDialog.dismiss();
                        if(response.isSuccess() && response.body().equals(true)) {
                            MmStatusAlertUtil.show(context, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                    context.getString(R.string.MSG_SUC_UNFOLLOWED), null);
                            type.setTypeFollowAction(ImSearchUserRvItem.ItemType.TYPE_FOLLOW);
                            updateFollowStatus(btnStatus, type.getTypeFollowAction());
                        }
                    }
                });
    }

    private void updateView(ImSearchUserItemViewHolder holder, ImSearchUserRvItem item) {
        updateFriendStatus(holder.btnAddFriend, item.getTypeFriendAction());
        updateFollowStatus(holder.btnFollow, item.getTypeFollowAction());
    }

    private void updateFriendStatus(TextView btnStatus, ImSearchUserRvItem.ItemType type){
        if(type == ImSearchUserRvItem.ItemType.TYPE_NOT_BEFRIENDED){
            btnStatus.setText(context.getString(R.string.LB_CA_ADD_FRIEND));
            btnStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_add, 0, 0, 0);
        }else if (type == ImSearchUserRvItem.ItemType.TYPE_BEFRIENDED){
            btnStatus.setText(context.getString(R.string.LB_CA_BEFRIENDED));
            btnStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ok, 0, 0, 0);
        }else if(type == ImSearchUserRvItem.ItemType.TYPE_REQ_CANCEL){
            btnStatus.setText(context.getString(R.string.LB_CA_FRD_REQ_CANCEL));
            btnStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    private void updateFollowStatus(TextView btnStatus, ImSearchUserRvItem.ItemType type){
        if(type == ImSearchUserRvItem.ItemType.TYPE_FOLLOWED){
            btnStatus.setText(context.getString(R.string.LB_CA_FOLLOWED));
            btnStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ok, 0, 0, 0);
        }else if(type == ImSearchUserRvItem.ItemType.TYPE_FOLLOW) {
            btnStatus.setText(context.getString(R.string.LB_CA_FOLLOW));
            btnStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_add, 0, 0, 0);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ImSearchUserItemViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imgProfile)
        CircleImageView imgProfile;

        @Bind(R.id.imgIsCurator)
        ImageView imgIsCurator;

        @Bind(R.id.tvUserName)
        TextView tvUserName;

        @Bind(R.id.btnAddFriend)
        TextView btnAddFriend;

        @Bind(R.id.btnFollow)
        TextView btnFollow;

        @Bind(R.id.rlProfile)
        RelativeLayout rlProfile;

        public ImSearchUserItemViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
        }
    }
}
