package com.mm.main.app.schema;

import java.io.Serializable;

/**
 * Created by henrytung on 26/11/2015.
 */
public class ImageData implements Serializable{

    String ColorKey;
    String ImageKey;
    Integer StyleImageId;
    Integer Position;

    public ImageData() {
    }

    public ImageData(String colorKey, String imageKey) {
        ColorKey = colorKey;
        ImageKey = imageKey;
    }

    public String getColorKey() {
        return ColorKey;
    }

    public void setColorKey(String colorKey) {
        ColorKey = colorKey;
    }

    public String getImageKey() {
        return ImageKey;
    }

    public void setImageKey(String imageKey) {
        ImageKey = imageKey;
    }

    public Integer getStyleImageId() {
        return StyleImageId;
    }

    public void setStyleImageId(Integer styleImageId) {
        StyleImageId = styleImageId;
    }

    public Integer getPosition() {
        return Position;
    }

    public void setPosition(Integer position) {
        Position = position;
    }
}
