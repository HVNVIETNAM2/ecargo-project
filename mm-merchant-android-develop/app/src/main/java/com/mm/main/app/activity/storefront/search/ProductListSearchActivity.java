package com.mm.main.app.activity.storefront.search;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.mm.main.app.R;
import com.mm.main.app.utils.Controllable;
import com.mm.main.app.adapter.strorefront.search.SearchListItemAdapter;
import com.mm.main.app.blurbehind.BlurBehind;
import com.mm.main.app.listitem.SearchListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.record.SearchHistory;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.schema.Brand;
import com.mm.main.app.schema.Category;
import com.mm.main.app.schema.response.CompleteResponse;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import retrofit.Response;
import retrofit.Retrofit;

public class ProductListSearchActivity extends AppCompatActivity implements Controllable {


    enum EntityType {
        BRAND(0),
        MERCHANT(1),
        CATEGORY(2),
        OTHER(3);

        private final int id;
        EntityType(int id) { this.id = id; }
        public int getValue() { return id; }

        public static EntityType getEntityType(String type) {
            EntityType entityType = EntityType.OTHER;

            try {
                entityType = EntityType.valueOf(type.toUpperCase());
            } catch (Exception e) {}

            return entityType;
        }
    }

    @Bind(R.id.mainListView)
    ListView listView;

    @Bind(R.id.searchEditText)
    EditText searchEditText;

    @Bind(R.id.barcodeImageView)
    ImageView barcodeImageView;

    Boolean isSearching;

    Boolean suggestListInUse = true;
    List<CompleteResponse> hotList = new ArrayList<>();
    List<CompleteResponse> suggestList = new ArrayList<>();
    List<CompleteResponse> completeList = new ArrayList<>();

    private List<SearchListItem> searchListItems = new ArrayList<>();
    SearchListItemAdapter searchListItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeNoActionBar);
        setContentView(R.layout.activity_product_list_search);
        ButterKnife.bind(this);
        fetchSuggest();
        setupBackground();
        setupToolbar();
        setupSearchEditText();
        isSearching = false;
        renderMainListView();
    }

    private void renderMainListView() {
        searchListItemAdapter = new SearchListItemAdapter(this, searchListItems);
        listView.setAdapter(searchListItemAdapter);
        prepareListItem();
    }

    public void prepareListItem() {
        searchListItems = new ArrayList<>();
//        if (!suggestListInUse && hotList.size() == 0) {
//            searchListItems.add(new SearchListItem("hotItem title", "", null, SearchListItem.ItemType.TYPE_NO_RESULT));
//            hotList = suggestList;
//        }

        if (suggestListInUse) {
            int size = getSizeHistory();
            if (size > 0) {
            searchListItems.add(new SearchListItem("history title", "", null, SearchListItem.ItemType.TYPE_HISTORY_TITLE));
            addHistoryList();
        }
        }
        searchListItems.add(new SearchListItem("hotItem title", "", null, SearchListItem.ItemType.TYPE_HOT_ITEM_TITLE));
        addHotItemList();

        searchListItemAdapter.setItemList(searchListItems);

        searchListItemAdapter.notifyDataSetChanged();
    }

    @OnItemClick(R.id.mainListView)
    void onItemClick(int position) {
        SearchListItem currentSearchListItem = searchListItems.get(position);
        if (currentSearchListItem.getType() == SearchListItem.ItemType.TYPE_HISTORY_LIST ||
                currentSearchListItem.getType() == SearchListItem.ItemType.TYPE_HOT_ITEM_LIST) {
            if (currentSearchListItem.getCompleteResponse() != null &&
                    currentSearchListItem.getCompleteResponse().getEntity().equals("Brand")) {
                List<Brand> brands = new ArrayList<>();
                Brand brand = new Brand();
                brand.setBrandId(currentSearchListItem.getCompleteResponse().getEntityId());
                brand.setBrandName(currentSearchListItem.getCompleteResponse().getSearchTerm());
                SearchHistory.getInstance().addSearchWord(currentSearchListItem.getCompleteResponse().getSearchTerm());
                brands.add(brand);

                SearchStyle.getInstance().clearAllFilter();
                SearchStyle.getInstance().setBrandid(brands);
                setResult("");
            } else if (currentSearchListItem.getCompleteResponse() != null &&
                    currentSearchListItem.getCompleteResponse().getEntity().equals("Category")) {
                List<Category> categories = new ArrayList<>();
                Category category = new Category();
                category.setCategoryId(currentSearchListItem.getCompleteResponse().getEntityId());
                category.setCategoryName(currentSearchListItem.getCompleteResponse().getSearchTerm());
                SearchHistory.getInstance().addSearchWord(currentSearchListItem.getCompleteResponse().getSearchTerm());
                categories.add(category);

                SearchStyle.getInstance().clearAllFilter();
                SearchStyle.getInstance().setCategoryid(categories);
                setResult("");
            } else if (currentSearchListItem.getCompleteResponse() != null &&
                    currentSearchListItem.getCompleteResponse().getEntity().equals("Merchant")) {
                List<Merchant> merchants = new ArrayList<>();
                Merchant merchant = new Merchant();
                merchant.setMerchantId(currentSearchListItem.getCompleteResponse().getEntityId());
                merchant.setMerchantCompanyName(currentSearchListItem.getCompleteResponse().getSearchTerm());
                SearchHistory.getInstance().addSearchWord(currentSearchListItem.getCompleteResponse().getSearchTerm());
                merchants.add(merchant);

                SearchStyle.getInstance().clearAllFilter();
                SearchStyle.getInstance().setMerchantid(merchants);
                setResult("");
            } else {
                proceedSearch(searchListItems.get(position).getContent());
            }
        }
    }

    @OnClick(R.id.cancelButton)
    public void onClickCancelButton() {
        finish();
    }

    public void proceedSearch(String query) {
        SearchStyle.getInstance().clearAllFilter();
        SearchStyle.getInstance().setQueryString(query);
        setResult(query);
    }


    @OnClick(R.id.barcodeImageView)
    public void onClickBarcodeImageView() {
        if (isSearching) {
            searchEditText.setText("");
            isSearching = false;
        }
    }

    private void addHistoryList() {
        List<String> historyList = SearchHistory.getInstance().getSearchedWords();

        int size = historyList.size();
        if (size <= 5) {
            for (int i = 0; i < historyList.size(); i++) {
                searchListItems.add(new SearchListItem("history item", historyList.get(size - i - 1), null, SearchListItem.ItemType.TYPE_HISTORY_LIST));
            }
        } else {
            for (int i = 0; i < 5; i++) {
                searchListItems.add(new SearchListItem("history item", historyList.get(size - i - 1), null, SearchListItem.ItemType.TYPE_HISTORY_LIST));
            }
        }

    }

    private int getSizeHistory() {
        List<String> historyList = SearchHistory.getInstance().getSearchedWords();
        return historyList.size();
    }

    private void addHotItemList() {
        for (int i = 0; i < hotList.size(); i++) {
            searchListItems.add(new SearchListItem("hot item", hotList.get(i).getSearchTerm(), hotList.get(i), SearchListItem.ItemType.TYPE_HOT_ITEM_LIST));
        }
    }

    public void setupBackground() {
        BlurBehind.getInstance()
                .withAlpha(85)
                .withFilterColor(getResources().getColor(R.color.mm_input_gray)) //or Color.RED
                .setBackground(this);
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.

        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

    }

    private void setupSearchEditText() {
        searchEditText.requestFocus();

        searchEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    proceedSearch(searchEditText.getText().toString());
                    return true;
                }
                return false;
            }
        });
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String text = searchEditText.getText().toString();
                if (TextUtils.isEmpty(text)) {
                    suggestListInUse = true;
                    isSearching = false;
                    barcodeImageView.setImageResource(R.drawable.ic_scan);
                    hotList = suggestList;
                    prepareListItem();
                } else {
                    suggestListInUse = false;
                    barcodeImageView.setImageResource(R.drawable.search_cancel);
                    isSearching = true;
                    fetchComplete(text);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setResult(String query) {
        if (!TextUtils.isEmpty(query)) {
            SearchHistory.getInstance().addSearchWord(query);
        }
        setResult(Activity.RESULT_OK);
        finish();
    }

    private void fetchSuggest() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getSearchService().complete(null)
                .enqueue(new MmCallBack<List<CompleteResponse>>(this) {
                    @Override
                    public void onSuccess(Response<List<CompleteResponse>> response, Retrofit retrofit) {
                        suggestList = response.body();
                        hotList = suggestList;
                        prepareListItem();
                    }
                });
    }

    private void fetchComplete(String queryString) {
        APIManager.getInstance().getSearchService().complete(queryString)
                .enqueue(new MmCallBack<List<CompleteResponse>>(this) {
                    @Override
                    public void onSuccess(Response<List<CompleteResponse>> response, Retrofit retrofit) {
                        completeList = response.body();
                        synchronized (suggestListInUse) {
                            if (!suggestListInUse) {
                                hotList = sort(completeList);
                                prepareListItem();
                            }
                        }
                    }
                });
    }

    private List<CompleteResponse> sort(List<CompleteResponse> oldList) {
        Collections.sort(oldList, new Comparator<CompleteResponse>() {
            public int compare(CompleteResponse o1, CompleteResponse o2) {
                EntityType entityType1 = EntityType.getEntityType(o1.getEntity());
                EntityType entityType2 = EntityType.getEntityType(o2.getEntity());

                return entityType1.getValue() - entityType2.getValue();
            }
        });

        return oldList;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void resume() {

    }
}
