package com.mm.main.app.activity.storefront.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.fragment.StoreFrontUserProfileDataListFragment;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.layout.SlidingTabLayout;
import com.mm.main.app.manager.ProfileLifeCycleManager;
import com.mm.main.app.view.MmSearchBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class StoreFrontUserProfileDataListActivity extends AppCompatActivity implements MmSearchBar.MmSearchBarListener {
    public static final String USER_KEY = "USER_KEY";
    public static final String LIST_TYPE_KEY = "LIST_TYPE_KEY";

    List<Fragment> fragments;
    ProfilePagerAdapter pageAdapter;

    @Bind(R.id.tabs)
    SlidingTabLayout tabs;

    @Bind(R.id.searchView)
    MmSearchBar searchView;

    @Bind(R.id.mm_toolbar)
    Toolbar toolbar;

    @Bind(R.id.viewpager)
    ViewPager viewPager;

    private String userKey = "";
    private ProfileDataListType profileDataType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_front_user_profile_data_list);
        ButterKnife.bind(this);

        userKey = getIntent().getStringExtra(USER_KEY);
        profileDataType = (ProfileDataListType) getIntent().getSerializableExtra(LIST_TYPE_KEY);

        setupToolbar();
        setupFragments();

        searchView.setHint(R.string.LB_CA_SEARCH);
        searchView.setMmSearchBarListener(this);

        toolbar.setContentInsetsAbsolute(0, 0);
    }

    @Override
    public void onBackPressed() {
        if (!userKey.equals(MmGlobal.getUserKey()) || ProfileLifeCycleManager.getInstance().haveHistoryChain()) {
            Intent intent = new Intent();
            intent.putExtra(StoreFrontUserProfileActivity.PROFILE_TYPE_KEY, StoreFrontUserProfileActivity.ProfileType.USER_PUBLIC_PROFILE);
            intent.putExtra(StoreFrontUserProfileActivity.PUBLIC_USER_KEY, userKey);
            // TODO: put the correct friend and follow status when service is ready
            intent.putExtra(StoreFrontUserProfileActivity.BE_FRIEND_KEY, true);
            intent.putExtra(StoreFrontUserProfileActivity.BE_FOLLOWED_KEY, false);
            intent.putExtra(StorefrontMainActivity.TAB_POSITION_KEY, 4);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<>();
        fList.add(createFragment(ProfileDataListType.MERCHANT_LIST));
        fList.add(createFragment(ProfileDataListType.CURATOR_LIST));
        fList.add(createFragment(ProfileDataListType.USER_LIST));
        return fList;
    }

    private Fragment createFragment(ProfileDataListType type) {
        StoreFrontUserProfileDataListFragment fragment = new StoreFrontUserProfileDataListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(USER_KEY, userKey);
        bundle.putSerializable(LIST_TYPE_KEY, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    private void cleanFragments() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        if (fragmentManager.getFragments() != null) {
            fragmentManager.getFragments().clear();
        }
        if (pageAdapter != null) {
            pageAdapter.notifyDataSetChanged();
        }
    }

    private void setupFragments() {
        cleanFragments();
        fragments = getFragments();
        pageAdapter = new ProfilePagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(pageAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                searchView.cancelSearch();
                StoreFrontUserProfileDataListFragment fragment = (StoreFrontUserProfileDataListFragment) fragments.get(position);
                switch (position) {
                    case 0:
                        fragment.setListType(ProfileDataListType.MERCHANT_LIST, userKey);
                        break;
                    case 1:
                        fragment.setListType(ProfileDataListType.CURATOR_LIST, userKey);
                        break;
                    case 2:
                        fragment.setListType(ProfileDataListType.USER_LIST, userKey);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Assiging the Sliding Tab Layout View
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Size for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.primary1);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(viewPager);

        switch (profileDataType) {
            case MERCHANT_LIST:
                viewPager.setCurrentItem(0);
                break;
            case CURATOR_LIST:
                viewPager.setCurrentItem(1);
                break;
            case USER_LIST:
                viewPager.setCurrentItem(2);
                break;
        }
    }

    @Override
    public void onEnterText(CharSequence s) {
        StoreFrontUserProfileDataListFragment listFragment = (StoreFrontUserProfileDataListFragment) fragments.get(viewPager.getCurrentItem());
        listFragment.filterList(s);

    }

    @Override
    public void onCancelSearch() {
        StoreFrontUserProfileDataListFragment listFragment = (StoreFrontUserProfileDataListFragment) fragments.get(viewPager.getCurrentItem());
        listFragment.clearFilter();
    }

    class ProfilePagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;

        public ProfilePagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            switch (position) {
                case 0:
                    title = getResources().getString(R.string.LB_CA_BRAND);
                    break;
                case 1:
                    title = getResources().getString(R.string.LB_CA_CURATOR);
                    break;
                case 2:
                    title = getResources().getString(R.string.LB_CA_USER);
                    break;
            }
            return title;
        }
    }

    public enum ProfileDataListType {
        FRIEND_LIST,
        MERCHANT_LIST,
        CURATOR_LIST,
        USER_LIST,
        WISH_LIST,
        FOLLOWER_LIST
    }
}
