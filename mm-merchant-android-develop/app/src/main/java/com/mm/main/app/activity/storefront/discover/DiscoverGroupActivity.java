package com.mm.main.app.activity.storefront.discover;

import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.mm.main.app.utils.Controllable;
import com.mm.main.app.activity.storefront.product.ProductListActivity;
import com.mm.main.app.log.Logger;
import com.mm.main.app.view.ActivityView;

import java.util.ArrayList;

public class DiscoverGroupActivity extends ActivityGroup implements Controllable {

    final public static int REQUEST_DISCOVER_MAIN = 1000;
    final public static int REQUEST_PRODUCT_LIST = 2000;

    final public static String ACTIVITY_ID_DISCOVER_MAIN = "discoverMain";
    final public static String ACTIVITY_ID_PRODUCT_LIST = "productList";

    // Keep this in a static variable to make it accessible for all the nesten activities, lets them manipulate the view
    public static DiscoverGroupActivity group;

    // Need to keep track of the history if you want the back-button to work properly, don't use this if your activities requires a lot of memory.
//    private ArrayList<ActivityView> history;
    private ArrayList<String> history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.history = new ArrayList();
        group = this;

        // Start the root activity withing the group and get its view
        View view = getLocalActivityManager().startActivity(ACTIVITY_ID_DISCOVER_MAIN, new
                Intent(this, DiscoverMainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                .getDecorView();

        // Replace the view of this ActivityGroup
        replaceView(new ActivityView(this, view), ACTIVITY_ID_DISCOVER_MAIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.d("storefrontmain", "i am here");
        int localRequestCode = (requestCode / 1000) * 1000;

        switch (localRequestCode) {

            case REQUEST_PRODUCT_LIST:
                try {
                    ProductListActivity productListActivity = (ProductListActivity) getLocalActivityManager().getCurrentActivity();
                    productListActivity.onActivityResult(requestCode, resultCode, data);
                    break;
                } catch (Exception e) {
                }
            case REQUEST_DISCOVER_MAIN:
                try {
                    ((DiscoverMainActivity) getLocalActivityManager().getActivity(ACTIVITY_ID_DISCOVER_MAIN)).onActivityResult(requestCode, resultCode, data);
                    break;
                } catch (Exception e) {
                }

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void replaceView(ActivityView v, String activityId) {
        // Adds the old one to history
        history.add(activityId);
        // Changes this Groups View to the new View.
        setContentView(v.getView());
    }

    public void back() {

        if (history.size() > 0) {
            history.remove(history.size() - 1);

            if (history.size() > 0) {
                LocalActivityManager manager = getLocalActivityManager();
                String lastId = history.get(history.size()-1);
                Intent lastIntent = manager.getActivity(lastId).getIntent();
                Window newWindow = manager.startActivity(lastId, lastIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                setContentView(newWindow.getDecorView());
            } else {
                finish();
            }

        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        DiscoverGroupActivity.group.back();
        return;
    }


    @Override
    public void resume() {
        ((DiscoverMainActivity) (getLocalActivityManager().getActivity(ACTIVITY_ID_DISCOVER_MAIN))).resume();
    }

    public void replaceContentView(String activityIdOrTag, Intent activityIntent) {
        View v = getLocalActivityManager().startActivity(activityIdOrTag, activityIntent)
                .getDecorView();
        this.setContentView(v);
    }
}
