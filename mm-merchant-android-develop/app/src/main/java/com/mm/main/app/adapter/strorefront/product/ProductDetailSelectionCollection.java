package com.mm.main.app.adapter.strorefront.product;

import com.mm.main.app.listitem.DynamicAvailableListItem;
import com.mm.main.app.schema.ProductColor;
import com.mm.main.app.schema.ProductSize;

import java.io.Serializable;

/**
 * Created by henrytung on 7/12/2015.
 */
public class ProductDetailSelectionCollection implements Serializable {

    private DynamicAvailableListItem<ProductColor> selectedColor = null;
    private DynamicAvailableListItem<ProductSize> selectedSize = null;
    private int viewingDefaultImage = -1;
    private boolean liked = false;

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public int getViewingDefaultImage() {
        return viewingDefaultImage;
    }

    public void setViewingDefaultImage(int viewingDefaultImage) {
        this.viewingDefaultImage = viewingDefaultImage;
    }

    public DynamicAvailableListItem<ProductColor> getSelectedColor() {
        return selectedColor;
    }

    public void setSelectedColor(DynamicAvailableListItem<ProductColor> selectedColor) {
        this.selectedColor = selectedColor;
    }

    public DynamicAvailableListItem<ProductSize> getSelectedSize() {
        return selectedSize;
    }

    public void setSelectedSize(DynamicAvailableListItem<ProductSize> selectedSize) {
        this.selectedSize = selectedSize;
    }
}
