package com.mm.main.app.schema;

import java.io.Serializable;

/**
 * Created by henrytung on 27/10/15.
 */
public class Language implements Serializable{

    String CultureCode;
    String LanguageCultureId;
    Integer LanguageId;
    String LanguageName;
    String LanguageNameInvariant;

    public Language(String cultureCode, String languageCultureId, Integer languageId, String languageName, String languageNameInvariant) {
        CultureCode = cultureCode;
        LanguageCultureId = languageCultureId;
        LanguageId = languageId;
        LanguageName = languageName;
        LanguageNameInvariant = languageNameInvariant;
    }

    public Language() {
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getLanguageCultureId() {
        return LanguageCultureId;
    }

    public void setLanguageCultureId(String languageCultureId) {
        LanguageCultureId = languageCultureId;
    }

    public Integer getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(Integer languageId) {
        LanguageId = languageId;
    }

    public String getLanguageName() {
        return LanguageName;
    }

    public void setLanguageName(String languageName) {
        LanguageName = languageName;
    }

    public String getLanguageNameInvariant() {
        return LanguageNameInvariant;
    }

    public void setLanguageNameInvariant(String languageNameInvariant) {
        LanguageNameInvariant = languageNameInvariant;
    }
}
