package com.mm.main.app.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.mm.main.app.R;
import com.mm.main.app.log.Logger;
import com.mm.main.app.schema.AppError;

import java.io.IOException;

import retrofit.Response;

/**
 * Created by andrew on 6/11/15.
 */
public class ErrorUtil {

    public static final String TAG = ErrorUtil.class.toString();
    private static final int VERIFICATION_ERROR_SHOW_SPEED = 15;
    private static final int VERIFICATION_ERROR_SHOW_TIME = 5000;

    //Alert server return App Error
    public static void alert(Response<?> response, Context context) {
        Gson gson = new Gson();
        AppError myError = null;
        try {
            myError = gson.fromJson(response.errorBody().string(), AppError.class);
            String translatedErrorString = myError.getAppCode();
            alert(translatedErrorString, context);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JsonParseException je) {
            Logger.d(TAG, "May need to update the AppError schema");
        }
    }

    // Alert Network Error
    public static void alert(Context context) {

        String translatedErrorString = "MSG_ERRNETWORK_FAIL";
        alert(translatedErrorString, context);
    }

    //Generic method for alerting
    public static void alert(String translatedErrorString, Context context) {
        translatedErrorString = StringUtil.getResourceString(translatedErrorString);

        showErrorDialog(context, null, translatedErrorString);

    }

    //Helper function for showing dialog
    public static void showErrorDialog(Context context, String content) {
        showErrorDialog(context, null, content);
    }

    //Helper function for showing dialog
    public static void showErrorDialog(Context context, String title, String content) {

        final AlertDialog dialog = showDialog(context);

        if (dialog != null) {
            TextView titleTextView = (TextView) (dialog.findViewById(R.id.errorTitle));
            TextView contentTextView = (TextView) (dialog.findViewById(R.id.errorContent));

            if (title != null) {
                titleTextView.setText(title);
            }
            if (content != null) {
                contentTextView.setText(content);
            }


            Button buttonOk = (Button) (dialog.findViewById(R.id.buttonOk));
            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }

    }

    private static AlertDialog showDialog(Context context) {
        if (context == null) {
            return null;
        }

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            builder.setView(inflater.inflate(R.layout.dialog_common_error, null));
            final AlertDialog dialog = builder.create();

            dialog.show();

            return dialog;
        } catch (WindowManager.BadTokenException e) {
            return showDialog(((Activity) context).getParent());
        } catch (Exception e) {
            return null;
        }
    }

    public static void showConfirmDialog(Context context, String content, DialogInterface.OnClickListener confirmAction) {
        showTwoOptionsDialog(context, "", content, context.getString(R.string.LB_CA_CONFIRM), context.getString(R.string.LB_CA_CANCEL), confirmAction, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    public static void showTwoOptionsDialog(Context context, String title, String content, String posText, String negText, DialogInterface.OnClickListener posAction, DialogInterface.OnClickListener negAction) {

        if (context == null) {
            return;
        }
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setTitle(title);
            alertDialog.setMessage(content);
            alertDialog.setPositiveButton(posText, posAction);
            alertDialog.setNegativeButton(negText, negAction);
            alertDialog.show();

            return;
        } catch (WindowManager.BadTokenException e) {
            showDialog(((Activity) context).getParent());
            return;
        } catch (Exception e) {
            return;
        }
    }

    public static void showVerificationError(final TextView txtError, EditText editTextInput, String errorContent, Activity activity) {
        txtError.setText(errorContent);
        if (editTextInput != null) {
            editTextInput.setTextColor(activity.getResources().getColor(R.color.primary1));
        }

        if (txtError.getVisibility() != View.VISIBLE) {
            AnimateUtil.expand(txtError, VERIFICATION_ERROR_SHOW_SPEED, 1, UiUtil.getPixelFromDp(35), null);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (txtError.getVisibility() == View.VISIBLE) {
                        AnimateUtil.collapse(txtError, VERIFICATION_ERROR_SHOW_SPEED, null);
                        txtError.setVisibility(View.INVISIBLE);
                    }
                }
            }, VERIFICATION_ERROR_SHOW_TIME);
        }
    }

}
