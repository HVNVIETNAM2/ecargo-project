package com.mm.main.app.schema;

/**
 * Created by andrew on 9/11/15.
 */
public class Problem {
    public Problem() {
    }

    public Problem(Integer userId, String subject, String message) {
        UserId = userId;
        Subject = subject;
        Message = message;
    }

    public Integer getUserId() {

        return UserId;
    }

    public void setUserId(Integer userId) {
        UserId = userId;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    Integer UserId;
    String Subject;
    String Message;

}
