package com.mm.main.app.adapter.strorefront.filter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by binhtruongp on 3/11/2016.
 */
public class UserFilterSelectionListAdapter extends BaseAdapter implements Filterable {
    protected final Activity context;
    protected List<FilterListItem<User>> originalData;
    protected List<FilterListItem<User>> filteredData;
    private ItemFilter mFilter = new ItemFilter();

    public UserFilterSelectionListAdapter(Activity context, List<FilterListItem<User>> originalData) {
        this.context = context;
        this.originalData = originalData;
        this.filteredData = originalData;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }


    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString;
            filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<FilterListItem<User>> list = originalData;

            int count = list.size();
            final ArrayList<FilterListItem<User>> nlist = new ArrayList<>(count);

            String filterableString;

            if (!TextUtils.isEmpty(constraint)) {
                for (int i = 0; i < count; i++) {
                    filterableString = getFilterableString(list.get(i).getT());
                    if (filterableString.toLowerCase().contains(filterString)) {
                        FilterListItem<User> mYourCustomData = list.get(i);
                        nlist.add(mYourCustomData);
                    }
                }
            } else {
                for (int i = 0; i < count; i++) {
                    FilterListItem<User> mYourCustomData = list.get(i);
                    nlist.add(mYourCustomData);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        protected String getFilterableString(User user) {
            return user.getDisplayName();
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<FilterListItem<User>>) results.values;
            notifyDataSetChanged();
        }

    }
}
