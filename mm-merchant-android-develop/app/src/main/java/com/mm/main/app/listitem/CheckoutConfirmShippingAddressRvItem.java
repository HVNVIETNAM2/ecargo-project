package com.mm.main.app.listitem;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class CheckoutConfirmShippingAddressRvItem implements CheckoutConfirmRvItem {

    String address1;
    String address2;
    String phone;

    public CheckoutConfirmShippingAddressRvItem(){

    }

    public CheckoutConfirmShippingAddressRvItem(String address1, String address2, String phone) {
        this.address1 = address1;
        this.address2 = address2;
        this.phone = phone;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public ItemType getType() {
        return ItemType.TYPE_SHIPPING_ADDRESS;
    }
}
