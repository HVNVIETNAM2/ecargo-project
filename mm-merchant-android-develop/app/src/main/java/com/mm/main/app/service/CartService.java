package com.mm.main.app.service;

import com.mm.main.app.schema.Cart;
import com.mm.main.app.schema.CartMergeRequest;
import com.mm.main.app.schema.CartUserUpdate;
import com.mm.main.app.schema.request.CartAddRequest;
import com.mm.main.app.schema.request.CartCreateRequest;
import com.mm.main.app.schema.request.CartItemRemoveRequest;
import com.mm.main.app.schema.request.CartUpdateRequest;
import com.mm.main.app.schema.request.WishListItemAddRequest;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by ductranvt on 1/15/2016.
 */
public interface CartService {
    String CART_PATH = "cart";

    @GET(CART_PATH + "/view")
    Call<Cart> viewCartByCartKey(@Query("cartkey") String cartKey);

    @GET(CART_PATH + "/view/user")
    Call<Cart> viewCartByUserKey(@Query("userkey") String userKey);

    @POST(CART_PATH + "/create")
    Call<Cart> createCart(@Body CartCreateRequest cartCreateRequest);

    @POST(CART_PATH + "/item/add")
    Call<Cart> addCartItem(@Body CartAddRequest cartAddRequest);

    @POST(CART_PATH + "/item/move/wishlist")
    Call<Cart> addCartItemToWishlist(@Body WishListItemAddRequest cartAddRequest);

    @POST(CART_PATH + "/item/update")
    Call<Cart> updateCartItem(@Body CartUpdateRequest cartUpdateRequest);

    @POST(CART_PATH + "/item/remove")
    Call<Cart> removeCartItem(@Body CartItemRemoveRequest cartRemoveRequest);

    @POST(CART_PATH + "/user/update")
    Call<Cart> updateCartUser(@Body CartUserUpdate cartUserUpdateRequest);

    @POST(CART_PATH + "/merge")
    Call<Cart> mergeCart(@Body CartMergeRequest cartMergeRequest);

}
