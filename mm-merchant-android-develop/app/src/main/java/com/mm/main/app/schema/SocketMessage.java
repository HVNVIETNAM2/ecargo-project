package com.mm.main.app.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrew on 17/3/2016.
 */
public class SocketMessage implements Serializable{
    String Type;
    String UserKey;
    List<String> UserList = new ArrayList<>();
    List<Conv> ConvList = new ArrayList<>();
    String ConvKey;
    String Body;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public List<String> getUserList() {
        return UserList;
    }

    public void setUserList(List<String> userList) {
        UserList = userList;
    }

    public List<Conv> getConvList() {
        return ConvList;
    }

    public void setConvList(List<Conv> convList) {
        ConvList = convList;
    }

    public String getConvKey() {
        return ConvKey;
    }

    public void setConvKey(String convKey) {
        ConvKey = convKey;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }
}
