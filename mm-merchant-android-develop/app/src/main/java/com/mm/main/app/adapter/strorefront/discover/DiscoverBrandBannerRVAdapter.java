package com.mm.main.app.adapter.strorefront.discover;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.DiscoverBannerListItem;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DiscoverBrandBannerRVAdapter extends RecyclerView.Adapter<DiscoverBrandBannerRVAdapter.ViewHolder> {

    public interface BrandListOnClickListener {
        void onClick(int position, Object data);
    }

    private final List<DiscoverBannerListItem> mValues;
    private final BrandListOnClickListener mListener;

    public DiscoverBrandBannerRVAdapter(List<DiscoverBannerListItem> items, BrandListOnClickListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.discover_banner_ad_item_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);

        if (holder.mItem.getBrandUnionMerchant().getEntity().equals("Brand")) {
            Picasso.with(MyApplication.getContext()).load(PathUtil.getBrandImageUrl(holder.mItem.getKey(), UiUtil.getDisplayWidth())).into(holder.bannerImageView);
        } else {
            Picasso.with(MyApplication.getContext()).load(PathUtil.getMerchantImageUrl(holder.mItem.getKey(), UiUtil.getDisplayWidth())).into(holder.bannerImageView);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onClick(position, holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView bannerImageView;
        public DiscoverBannerListItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            bannerImageView = (ImageView) view.findViewById(R.id.bannerImageView);
        }
    }
}
