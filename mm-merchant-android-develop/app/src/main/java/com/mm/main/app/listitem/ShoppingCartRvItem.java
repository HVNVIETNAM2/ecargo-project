package com.mm.main.app.listitem;

/**
 * Created by ductranvt on 1/19/2016.
 */
public interface ShoppingCartRvItem {
    public enum ItemType {
        TYPE_SHOPPING_CART_MERCHANT,
        TYPE_SHOPPING_CART_PRODUCT,
        TYPE_HEADER
    }

    ItemType getType();

    boolean getSelected();

    void setSelected(boolean isSelected);

    int getAppiumIndex();

    void setAppiumIndex(int appiumIndex);
}
