package com.mm.main.app.adapter.strorefront.product;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.product.ProductDetailPageActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.decoration.GridSpacingItemDecoration;
import com.mm.main.app.helper.ProductAvailabilityHelper;
import com.mm.main.app.helper.ProductFilterHelper;
import com.mm.main.app.layout.BuyButtonSlidingDrawer;
import com.mm.main.app.listitem.DynamicAvailableListItem;
import com.mm.main.app.listitem.ProductDetailListItem;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.WishlistActionUtil;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.schema.Color;
import com.mm.main.app.schema.ColorImage;
import com.mm.main.app.schema.ImageData;
import com.mm.main.app.schema.ProductColor;
import com.mm.main.app.schema.ProductSize;
import com.mm.main.app.schema.Style;
import com.mm.main.app.schema.response.SearchResponse;
import com.mm.main.app.service.SearchServiceBuilder;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.view.CustomViewPager;
import com.squareup.picasso.Picasso;
import com.wefika.flowlayout.FlowLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by henrytung on 2/12/2015.
 */
public class ProductDetailRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity context;
    private List<ProductDetailListItem> itemsData;
    private static Style style;
    private static boolean initColor = false;
    private static boolean initComment = false;
    private static boolean initRecommend = false;
    private static boolean initSize = false;
    private static boolean initFeatureImage = false;

    private List<Style> suggestProductList;
    private static List<ImageData> displayFeatureProductList;
    private static boolean updateFeatureImageList = false;
    private ProductDetailSelectionCollection productDetailSelectionCollection;
    private static ProductAvailabilityHelper productAvailabilityHelper;

    public Timer autoScrollTimer;
    private CustomViewPager vPager;
    private ProductImagePagerAdapter mPagerAdapter;
    private PageListener featureImagesPageListener;
    private int currentPage;
    private final float imgSizeRatio = 1.0f;

    CircleImageView[] imageViewColors;
    CircleImageView[] imageViewColorsDisable;
    CircleImageView[] imageViewSizes;
    CircleImageView[] imageViewSizesDisable;


    public ProductDetailRvAdapter(Activity context, List<ProductDetailListItem> itemsData, Style style) {
        this.itemsData = itemsData;
        this.context = context;
        this.style = style;

        initColor = false;
        initComment = false;
        initRecommend = false;
        initSize = false;
        initFeatureImage = false;

        List<DynamicAvailableListItem<ProductColor>> colorDataList = new ArrayList<>();
        for (ProductColor productColor : style.getColorList()) {
            colorDataList.add(new DynamicAvailableListItem<>(productColor));
        }

        List<DynamicAvailableListItem<ProductSize>> sizeDataList = new ArrayList<>();
        for (ProductSize productSize : style.getSizeList()) {
            sizeDataList.add(new DynamicAvailableListItem<>(productSize));
        }

        productAvailabilityHelper = new ProductAvailabilityHelper(style.getSkuList(), sizeDataList, colorDataList);

        productDetailSelectionCollection = new ProductDetailSelectionCollection();
    }

    public ProductDetailSelectionCollection getProductDetailSelectionCollection() {
        return productDetailSelectionCollection;
    }

    @Override
    public int getItemViewType(int position) {
        return itemsData.get(position).getType().ordinal();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ProductDetailListItem.ItemType itemType = ProductDetailListItem.ItemType.values()[viewType];

        RecyclerView.ViewHolder viewHolder = null;

        switch (itemType) {
            case TYPE_IMAGE_DEFAULT:
                viewHolder = new ViewHolderImageDefault(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.image_default_item_view, viewGroup, false));
                break;
            case TYPE_NAME:
                viewHolder = new ViewHolderName(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.name_item_view, viewGroup, false));
                break;
            case TYPE_SIZE:
                viewHolder = new ViewHolderSize(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.size_item_view, viewGroup, false));
                break;
            case TYPE_COLOR:
                viewHolder = new ViewHolderColor(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.color_item_view, viewGroup, false));
                break;
            case TYPE_DESC:
                viewHolder = new ViewHolderDesc(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.desc_item_view, viewGroup, false));
                break;
            case TYPE_PARAMETER:
                viewHolder = new ViewHolderParameter(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.parameter_item_view, viewGroup, false));
                break;
            case TYPE_COMMENT:
                viewHolder = new ViewHolderComment(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.comment_item_view, viewGroup, false));
                break;
            case TYPE_RECOMMEND:
                viewHolder = new ViewHolderRecommend(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recommend_item_view, viewGroup, false));
                break;
            case TYPE_DESC_IMAGE:
                viewHolder = new ViewHolderDescriptionImage(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.description_image_item_view, viewGroup, false));
                break;
            case TYPE_OUTFIT:
                viewHolder = new ViewHolderOutfit(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.outfit_item_view, viewGroup, false));
                break;
            case TYPE_SUGGESTION:
                viewHolder = new ViewHolderSuggestion(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.suggest_item_view, viewGroup, false));
                break;
            case TYPE_SCORE:
                viewHolder = new ViewHolderScore(LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.score_item_view, viewGroup, false));
                break;
        }

        return viewHolder;
    }

    private void proceedCheckout(final BuyButtonSlidingDrawer buyButtonSlidingDrawer) {
        ProductDetailPageActivity productDetailPageActivity = (ProductDetailPageActivity) context;
        productDetailPageActivity.proceedCheckout(context, buyButtonSlidingDrawer, false);

    }

    private void bindImageDefaultItemView(final ViewHolderImageDefault viewHolderImageDefault) {

        if (!initFeatureImage && SearchStyle.getInstance().getColorid() != null &&
                SearchStyle.getInstance().getColorid().size() == 1) {
            DynamicAvailableListItem<ProductColor> daltProductColor = new DynamicAvailableListItem<>(
                    getColorFromId(style.getColorList(), SearchStyle.getInstance().getColorid().get(0)));
            updateFeatureImagesList(daltProductColor);
            initFeatureImage = true;
        }

        if (autoScrollTimer != null && vPager != null) {
            vPager.setVelocityScroller(context, 200);
            autoScrollTimer.cancel();
            autoScrollTimer = null;
        }

        if (updateFeatureImageList) {
            int offsetAmount = displayFeatureProductList.size() * ProductImagePagerAdapter.LOOPS_COUNT;
            viewHolderImageDefault.imageViewIndicators = renderFeatureImages(viewHolderImageDefault.llIndicator, viewHolderImageDefault.imageViewIndicators);
            vPager.setCurrentItem(offsetAmount / 2, false);
            featureImagesPageListener.imageIndicators = viewHolderImageDefault.imageViewIndicators;
            updateFeatureImageList = false;
        }

        autoScrollTimer = new Timer();
        autoScrollTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        int newPos = 0;
                        int currentPos = currentPage;
                        int offsetAmount = displayFeatureProductList.size() * ProductImagePagerAdapter.LOOPS_COUNT;
                        if (currentPos < offsetAmount - 1) {
                            newPos = currentPos + 1;
                        }

                        if (vPager != null) {
                            vPager.setVelocityScroller(context, 1000);
                            vPager.setCurrentItem(newPos, true);
                        }
                    }
                });
            }
        }, Constant.AUTO_SCROLL_TIMER, Constant.AUTO_SCROLL_TIMER);

        if (viewHolderImageDefault.buttonLike != null) {
            viewHolderImageDefault.buttonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String colorKey = null;
                    if (productDetailSelectionCollection.getSelectedColor() != null) {
                        colorKey = productDetailSelectionCollection.getSelectedColor().getT().getColorKey();
                    }
                    WishlistActionUtil.actionWishlist(v, style, colorKey, context, new WishlistActionUtil.WishListActionListener() {
                        @Override
                        public void completedAction(boolean isSuccess) {
                            ProductDetailRvAdapter.this.notifyDataSetChanged();
                        }
                    });
                }
            });
        }
        viewHolderImageDefault.buttonLike.setSelected(style.isWished());
    }

    private ProductColor getColorFromId(List<ProductColor> styleColorList, Color filterColor) {
        for (ProductColor styleColor : styleColorList) {
            if (styleColor.getColorId().intValue() == filterColor.getColorId().intValue()) {
                return styleColor;
            }
        }
        return null;
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public ImageView imageIndicators[];

        public void onPageSelected(int position) {
            Logger.d("Current", "page selected " + position);
            currentPage = position;
            if (imageIndicators != null) {
                int realPos = position % imageIndicators.length;
                updateDefaultImageIndicator(imageIndicators, realPos);
            }
        }
    }

    private void updateDefaultImageIndicator(ImageView[] imageViewIndicators, int newPos) {
        for (int i = 0; i < imageViewIndicators.length; i++) {
            if (i == newPos) {
                imageViewIndicators[i].setImageDrawable(context.getResources().getDrawable(R.drawable.scroll_color_ball_red));
            } else {
                imageViewIndicators[i].setImageDrawable(context.getResources().getDrawable(R.drawable.scroll_color_ball_white));
            }
        }
    }

    private void bindNameItemView(ViewHolderName viewHolderName) {
        viewHolderName.textViewSkuName.setText(style.getSkuName());

        Picasso.with(MyApplication.getContext())
                .load(PathUtil.getBrandImageUrl(style.getBrandHeaderLogoImage()))
                .into(viewHolderName.brandImageView);
    }

    private void bindSizeItemView(ViewHolderSize viewHolderSize) {
        CircleImageView circleImageView[] = imageViewSizes;
        CircleImageView circleImageViewDisable[] = imageViewSizesDisable;
        final List<DynamicAvailableListItem<ProductSize>> productSizeList = productAvailabilityHelper.getSizeList();
        for (int i = 0; i < circleImageView.length; i++) {
            final DynamicAvailableListItem<ProductSize> productSize = productSizeList.get(i);

            int colorId = R.color.secondary1;

            if (productDetailSelectionCollection.getSelectedSize() != null &&
                    productSize.getT().getSizeId().intValue() == productDetailSelectionCollection.getSelectedSize().getT().getSizeId().intValue()) {
                colorId = R.color.black;
            }
            circleImageView[i].setBorderColor(MyApplication.getContext().getResources().getColor(colorId));

            circleImageView[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (productSize.isAvailable()) {
                        CircleImageView circleImageView = (CircleImageView) v;
                        if (productDetailSelectionCollection.getSelectedSize() == null ||
                                productDetailSelectionCollection.getSelectedSize().getT().getSizeId().intValue() !=
                                        productSize.getT().getSizeId().intValue()) {
                            productDetailSelectionCollection.setSelectedSize(productSize);
                            circleImageView.setSelected(true);
                            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.black));
                            productAvailabilityHelper.updateColorAvailability(productSize);
                        }
                        notifyDataSetChanged();
                    }
                }
            });

            if (productSize.isAvailable()) {
                circleImageViewDisable[i].setVisibility(View.INVISIBLE);
            } else {
                circleImageViewDisable[i].setVisibility(View.VISIBLE);
                if (productDetailSelectionCollection.getSelectedSize() != null &&
                        productSize.getT().getSizeId().intValue() == productDetailSelectionCollection.getSelectedSize().getT().getSizeId().intValue()) {
                    productDetailSelectionCollection.setSelectedSize(null);
                    circleImageView[i].setSelected(false);
                    circleImageView[i].setBorderColor(MyApplication.getContext().getResources().getColor(R.color.secondary1));
                }
            }
        }
    }

    private void updateFeatureImagesList(DynamicAvailableListItem<ProductColor> productColor) {
        List<ImageData> imageDataList = getDisplayColorImage(productColor);
        if (imageDataList.size() > 0) {
            updateFeatureImageList = true;
            displayFeatureProductList.clear();
            displayFeatureProductList.addAll(imageDataList);
            displayFeatureProductList.addAll(style.getFeaturedImageList());
        } else {
            updateFeatureImageList = true;
            displayFeatureProductList.clear();
            displayFeatureProductList.addAll(style.getFeaturedImageList());
        }

        if (vPager != null && vPager.getAdapter() != null) {
            vPager.getAdapter().notifyDataSetChanged();
        }
    }

    private List<ImageData> getDisplayColorImage(DynamicAvailableListItem<ProductColor> productColor) {
        List<ImageData> colorImageList = new ArrayList<>();

        for (ColorImage colorImage : style.getColorImageList()) {
            if (productColor.getT().getColorKey().toUpperCase().equals(colorImage.getColorKey().toUpperCase())) {
                colorImageList.add(new ImageData(colorImage.getColorKey(), colorImage.getImageKey()));
            }
        }

        return colorImageList;
    }

    private void bindColorItemView(ViewHolderColor viewHolderColor) {
        CircleImageView circleImageView[] = imageViewColors;
        CircleImageView circleImageViewDisable[] = imageViewColorsDisable;
        List<DynamicAvailableListItem<ProductColor>> productColorList = productAvailabilityHelper.getColorList();
        for (int i = 0; i < circleImageView.length; i++) {
            final DynamicAvailableListItem<ProductColor> productColor = productColorList.get(i);
            int colorId = R.color.secondary1;

            if (productDetailSelectionCollection.getSelectedColor() != null &&
                    productColor.getT().getColorKey().toUpperCase().equals(
                            productDetailSelectionCollection.getSelectedColor().getT().getColorKey().toUpperCase())) {
                colorId = R.color.black;
            }
            circleImageView[i].setBorderColor(MyApplication.getContext().getResources().getColor(colorId));

            circleImageView[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (productColor.isAvailable()) {
                        CircleImageView circleImageView = (CircleImageView) v;
                        if (productDetailSelectionCollection.getSelectedColor() == null ||
                                !productDetailSelectionCollection.getSelectedColor().getT().getColorKey().toUpperCase().equals(
                                        productColor.getT().getColorKey().toUpperCase())) {
                            productDetailSelectionCollection.setSelectedColor(productColor);
                            circleImageView.setSelected(true);
                            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.black));
                            productAvailabilityHelper.updateSizeAvailability(productColor);
                        }
                    }
                    updateFeatureImagesList(productColor);
                    notifyDataSetChanged();
                }
            });

            if (productColor.isAvailable()) {
                circleImageViewDisable[i].setVisibility(View.INVISIBLE);
            } else {
                circleImageViewDisable[i].setVisibility(View.VISIBLE);
                if (productDetailSelectionCollection.getSelectedColor() != null &&
                        productColor.getT().getColorKey().toUpperCase().equals(
                                productDetailSelectionCollection.getSelectedColor().getT().getColorKey().toUpperCase())) {
                    productDetailSelectionCollection.setSelectedColor(null);
                    circleImageView[i].setSelected(false);
                    circleImageView[i].setBorderColor(MyApplication.getContext().getResources().getColor(R.color.secondary1));
                }
            }
        }
    }

    private void bindDescItemView(ViewHolderDesc viewHolderDesc) {
        viewHolderDesc.textViewDesc.setText(style.getSkuDesc());
    }

    private void bindParameterItemView(ViewHolderParameter viewHolderParameter) {
    }

    private void bindCommentItemView(ViewHolderComment viewHolderComment) {
        viewHolderComment.tvCommentNumber.setText("(112" +
                MyApplication.getContext().getResources().getString(R.string.LB_CA_COMMENT) + ")");
    }

    private void bindRecommendItemView(ViewHolderRecommend viewHolderRecommend) {
        viewHolderRecommend.textViewRecommendNumber.setText("116" + " " +
                MyApplication.getContext().getResources().getString(R.string.LB_CA_NUM_PPL_RCMD_ITEM));
    }

    private void bindDescriptionImageItemView(ViewHolderDescriptionImage viewHolderDescriptionImage) {
    }

    private void bindOutfitItemView(final ViewHolderOutfit viewHolderOutfit) {
        BuyButtonSlidingDrawer.ScrollEventCallBack scrollEventCallBack = new BuyButtonSlidingDrawer.ScrollEventCallBack() {
            @Override
            public void onScrollStarted() {
                if (context instanceof ProductDetailPageActivity) {
                    ((ProductDetailPageActivity) context).setScrollable(false);
                }
            }

            @Override
            public void onScrollEnd(boolean opened) {
                ((ProductDetailPageActivity) context).setScrollable(true);
            }
        };

        viewHolderOutfit.buyButtonSlidingDrawer.bindBuyItemView(
                new BuyButtonSlidingDrawer.BuyOpenedCallBack() {
                    @Override
                    public void drawerOpened(Activity context, boolean isSwipe) {
                        proceedCheckout(viewHolderOutfit.buyButtonSlidingDrawer);
                    }
                }, style.getPrice(), null, scrollEventCallBack);

        viewHolderOutfit.buttonLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String colorKey = null;
                if (productDetailSelectionCollection.getSelectedColor() != null) {
                    colorKey = productDetailSelectionCollection.getSelectedColor().getT().getColorKey();
                }
                WishlistActionUtil.actionWishlist(v, style, colorKey, context, new WishlistActionUtil.WishListActionListener() {
                    @Override
                    public void completedAction(boolean isSuccess) {
                        ProductDetailRvAdapter.this.notifyDataSetChanged();
                    }
                });
            }
        });
        viewHolderOutfit.buttonLike.setSelected(style.isWished());
    }

    private void bindSuggestionItemView(final ViewHolderSuggestion viewHolderSuggestion) {

        final ProductRVAdapter.ProductListOnClickListener productListOnClickListener = new ProductRVAdapter.ProductListOnClickListener() {
            @Override
            public void onClick(int position, Object data) {
                style = suggestProductList.get(position);
                notifyDataSetChanged();
            }
        };

        viewHolderSuggestion.recyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));

        if (ProductFilterHelper.getFullStyleList() == null) {
            SearchServiceBuilder.searchStyle(new MmCallBack<SearchResponse>(context) {
                @Override
                public void onSuccess(Response<SearchResponse> response, Retrofit retrofit) {
                    suggestProductList = response.body().getPageData();
                    ProductFilterHelper.setFullStyleList(suggestProductList);
                    renderStyleList(viewHolderSuggestion);
                }
            });
        } else {
            suggestProductList = ProductFilterHelper.getFullStyleList();
            renderStyleList(viewHolderSuggestion);
        }
    }

    private void renderStyleList(ViewHolderSuggestion viewHolderSuggestion) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MyApplication.getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        viewHolderSuggestion.recyclerView.setLayoutManager(linearLayoutManager);

        int screenWidth = UiUtil.getDisplayWidth();
        viewHolderSuggestion.recyclerView.setLayoutParams(new RelativeLayout.LayoutParams(screenWidth, UiUtil.getAspectHeightOfSuggestionImage(screenWidth / 2)));


        ProductRVOneLineAdapter productRVAdapter = new ProductRVOneLineAdapter(suggestProductList, null);
        productRVAdapter.setContext(context);
        viewHolderSuggestion.recyclerView.setAdapter(productRVAdapter);
        productRVAdapter.setDetailRvAdapter(this);
        productRVAdapter.notifyDataSetChanged();
    }

    private void bindScoreItemView(ViewHolderScore viewHolderScore) {
        viewHolderScore.tvDescScore.setText(MyApplication.getContext().getResources().getText(R.string.LB_CA_PROD_DESC) + " 5.0");
        viewHolderScore.tvServiceScore.setText(MyApplication.getContext().getResources().getText(R.string.LB_CA_CUST_SERVICE) + " 5.0");
        viewHolderScore.tvShipmentScore.setText(MyApplication.getContext().getResources().getText(R.string.LB_CA_SHIPMENT_RATING) + " 5.0");
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ProductDetailListItem listItem = itemsData.get(position);

        switch (listItem.getType()) {
            case TYPE_IMAGE_DEFAULT:
                ViewHolderImageDefault viewHolderImageDefault = (ViewHolderImageDefault) viewHolder;
                bindImageDefaultItemView(viewHolderImageDefault);
                break;
            case TYPE_NAME:
                ViewHolderName viewHolderName = (ViewHolderName) viewHolder;
                bindNameItemView(viewHolderName);
                break;
            case TYPE_SIZE:
                ViewHolderSize viewHolderSize = (ViewHolderSize) viewHolder;
                bindSizeItemView(viewHolderSize);
                break;
            case TYPE_COLOR:
                ViewHolderColor viewHolderColor = (ViewHolderColor) viewHolder;
                bindColorItemView(viewHolderColor);
                break;
            case TYPE_DESC:
                ViewHolderDesc viewHolderDesc = (ViewHolderDesc) viewHolder;
                bindDescItemView(viewHolderDesc);
                break;
            case TYPE_PARAMETER:
                ViewHolderParameter viewHolderParameter = (ViewHolderParameter) viewHolder;
                bindParameterItemView(viewHolderParameter);
                break;
            case TYPE_COMMENT:
                ViewHolderComment viewHolderComment = (ViewHolderComment) viewHolder;
                bindCommentItemView(viewHolderComment);
                break;
            case TYPE_RECOMMEND:
                ViewHolderRecommend viewHolderRecommend = (ViewHolderRecommend) viewHolder;
                bindRecommendItemView(viewHolderRecommend);
                break;
            case TYPE_DESC_IMAGE:
                ViewHolderDescriptionImage viewHolderDescriptionImage = (ViewHolderDescriptionImage) viewHolder;
                bindDescriptionImageItemView(viewHolderDescriptionImage);
                break;
            case TYPE_OUTFIT:
                ViewHolderOutfit viewHolderOutfit = (ViewHolderOutfit) viewHolder;
                bindOutfitItemView(viewHolderOutfit);
                break;
            case TYPE_SUGGESTION:
                ViewHolderSuggestion viewHolderSuggestion = (ViewHolderSuggestion) viewHolder;
                bindSuggestionItemView(viewHolderSuggestion);
                break;
            case TYPE_SCORE:
                ViewHolderScore viewHolderScore = (ViewHolderScore) viewHolder;
                bindScoreItemView(viewHolderScore);
                break;
        }
    }

    private ImageView[] renderFeatureImages(LinearLayout llIndicator, ImageView imageViewIndicators[]) {

        llIndicator.removeAllViews();
        imageViewIndicators = new ImageView[displayFeatureProductList.size()];
        for (int i = 0; i < imageViewIndicators.length; i++) {
            imageViewIndicators[i] = new ImageView(MyApplication.getContext());
            LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(5, 5);
            int rightPadding = UiUtil.getPixelFromDp(10);
            layoutParams.setMargins(0, 0, rightPadding, 0);
            imageViewIndicators[i].setLayoutParams(layoutParams);

            imageViewIndicators[i].setId(i);

            if (i == 0) {
                imageViewIndicators[i].setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.scroll_color_ball_red));
            } else {
                imageViewIndicators[i].setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.scroll_color_ball_white));
            }
//                imageViews[i].setBorderColor(MyApplication.getContext().getResources().getColor(R.color.mm_input_gray));
//                imageViews[i].setBorderWidth(UiUtil.getPixelFromDp(1));

            llIndicator.addView(imageViewIndicators[i]);
        }

        return imageViewIndicators;
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolderImageDefault extends RecyclerView.ViewHolder implements CustomViewPager.TouchEventCallBack {
        public ImageView buttonLike;
        public LinearLayout llIndicator;
        public ImageView imageViewIndicators[];
        private ViewHolderImageDefault viewHolderImageDefault;

        public ViewHolderImageDefault(View itemLayoutView) {
            super(itemLayoutView);
            this.viewHolderImageDefault = this;
            displayFeatureProductList = new ArrayList<>();
            displayFeatureProductList.addAll(style.getFeaturedImageList());
            int sizeOfItems = displayFeatureProductList.size();

            buttonLike = (ImageView) itemLayoutView.findViewById(R.id.buttonLike);
            vPager = (CustomViewPager) itemLayoutView.findViewById(R.id.rvFeatureImages);
            vPager.setLayoutParams(new RelativeLayout.LayoutParams(UiUtil.getDisplayWidth() * sizeOfItems,
                    UiUtil.getHeightByWidthWithRatio(UiUtil.getDisplayWidth(), imgSizeRatio)));
            llIndicator = (LinearLayout) itemLayoutView.findViewById(R.id.llIndicator);

            mPagerAdapter = new ProductImagePagerAdapter();
            mPagerAdapter.setContext(context);
            mPagerAdapter.setmValues(displayFeatureProductList);
            vPager.setOffscreenPageLimit(1);
            vPager.setAdapter(mPagerAdapter);
            int offsetAmount = sizeOfItems * ProductImagePagerAdapter.LOOPS_COUNT;
            currentPage = offsetAmount / 2;
            vPager.setCurrentItem(currentPage, false);

            imageViewIndicators = renderFeatureImages(llIndicator, imageViewIndicators);

            featureImagesPageListener = new PageListener();
            featureImagesPageListener.imageIndicators = imageViewIndicators;
            vPager.addOnPageChangeListener(featureImagesPageListener);
            vPager.setTouchEventCallBack(this);
        }

        @Override
        public void touchedCallback(MotionEvent ev) {
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (autoScrollTimer != null) {
                        autoScrollTimer.cancel();
                    }
                    // kill timer
                    break;
                case MotionEvent.ACTION_UP:
                    // start timer
                    bindImageDefaultItemView(viewHolderImageDefault);
                    break;

            }
        }
    }

    public static class ViewHolderName extends RecyclerView.ViewHolder {
        public ImageView brandImageView;
        public TextView textViewSkuName;

        public ViewHolderName(View itemLayoutView) {
            super(itemLayoutView);
            brandImageView = (ImageView) itemLayoutView.findViewById(R.id.brandImageView);
            textViewSkuName = (TextView) itemLayoutView.findViewById(R.id.textViewSkuName);
        }
    }

    public class ViewHolderSize extends RecyclerView.ViewHolder {
        public FlowLayout layout;

        public ViewHolderSize(View itemLayoutView) {
            super(itemLayoutView);
            layout = (FlowLayout) itemLayoutView.findViewById(R.id.horizontal_scroll);
            List<DynamicAvailableListItem<ProductSize>> sizeDataList = productAvailabilityHelper.getDefaultSizeAvailability();
            if (!initSize) {
                imageViewSizes = new CircleImageView[sizeDataList.size()];
                imageViewSizesDisable = new CircleImageView[sizeDataList.size()];
                UiUtil.addCircleSizeToFlowlayout(layout, imageViewSizes, sizeDataList, imageViewSizesDisable);
                initSize = true;
            }
        }
    }

    public class ViewHolderColor extends RecyclerView.ViewHolder {
        public FlowLayout flowLayout;


        public ViewHolderColor(View itemLayoutView) {
            super(itemLayoutView);

            flowLayout = (FlowLayout) itemLayoutView.findViewById(R.id.horizontal_scroll);
            List<DynamicAvailableListItem<ProductColor>> colorDataList = productAvailabilityHelper.getDefaultColorAvailability();

            List<ColorImage> colorImageList = style.getColorImageList();

            if (!initColor) {
                imageViewColors = new CircleImageView[colorDataList.size()];
                imageViewColorsDisable = new CircleImageView[colorDataList.size()];
                UiUtil.addCircleColorToFlowlayout(flowLayout, imageViewColors, colorDataList, imageViewColorsDisable, colorImageList);
            }
            initColor = true;

        }
    }

    public static class ViewHolderDesc extends RecyclerView.ViewHolder {
        public TextView textViewDesc;

        public ViewHolderDesc(View itemLayoutView) {
            super(itemLayoutView);
            textViewDesc = (TextView) itemLayoutView.findViewById(R.id.textViewDesc);
        }
    }

    public static class ViewHolderParameter extends RecyclerView.ViewHolder {
        public TextView textViewParameterValue;

        public ViewHolderParameter(View itemLayoutView) {
            super(itemLayoutView);
            textViewParameterValue = (TextView) itemLayoutView.findViewById(R.id.textViewParameterValue);
        }
    }

    public static class ViewHolderComment extends RecyclerView.ViewHolder {
        public TextView tvCommentNumber;
        public LinearLayout linearLayoutCommentImage;

        public ViewHolderComment(View itemLayoutView) {
            super(itemLayoutView);
            tvCommentNumber = (TextView) itemLayoutView.findViewById(R.id.tvCommentNumber);
            tvCommentNumber.setText("(112" +
                    MyApplication.getContext().getResources().getString(R.string.LB_CA_COMMENT) + ")");

            linearLayoutCommentImage = (LinearLayout) itemLayoutView.findViewById(R.id.linearLayoutCommentImage);
            Drawable[] drawables = new Drawable[2];
            drawables[0] = MyApplication.getContext().getResources().getDrawable(R.drawable.comment_demo1);
            drawables[1] = MyApplication.getContext().getResources().getDrawable(R.drawable.comment_demo2);

            if (!initComment) {
                for (int i = 0; i < 2; i++) {
                    ImageView imageView = new ImageView(MyApplication.getContext());
                    LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(45, 70);
                    int rightPadding = UiUtil.getPixelFromDp(1);
                    layoutParams.setMargins(0, 0, rightPadding, 0);
                    imageView.setLayoutParams(layoutParams);
                    imageView.setId(i);
//                    imageViewColors.setImageBitmap(BitmapFactory.decodeResource(
//                            MyApplication.getContext().getResources(), R.drawable.brand_logo_temp));
                    imageView.setImageDrawable(drawables[i]);
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    linearLayoutCommentImage.addView(imageView);
                }
                initComment = true;
            }
        }
    }

    public static class ViewHolderRecommend extends RecyclerView.ViewHolder {
        public TextView textViewRecommendNumber;
        public LinearLayout linearLayoutRecommendImage;

        public ViewHolderRecommend(View itemLayoutView) {
            super(itemLayoutView);
            textViewRecommendNumber = (TextView) itemLayoutView.findViewById(R.id.textViewRecommendNumber);
            textViewRecommendNumber.setText("116" + " " +
                    MyApplication.getContext().getResources().getString(R.string.LB_CA_NUM_PPL_RCMD_ITEM));

            linearLayoutRecommendImage = (LinearLayout) itemLayoutView.findViewById(R.id.linearLayoutRecommendImage);
            Drawable[] drawables = new Drawable[7];
            drawables[0] = MyApplication.getContext().getResources().getDrawable(R.drawable.user01);
            drawables[1] = MyApplication.getContext().getResources().getDrawable(R.drawable.user02);
            drawables[2] = MyApplication.getContext().getResources().getDrawable(R.drawable.user03);
            drawables[3] = MyApplication.getContext().getResources().getDrawable(R.drawable.user04);
            drawables[4] = MyApplication.getContext().getResources().getDrawable(R.drawable.user05);
            drawables[5] = MyApplication.getContext().getResources().getDrawable(R.drawable.user06);
            drawables[6] = MyApplication.getContext().getResources().getDrawable(R.drawable.user07);


            if (!initRecommend) {
                for (int i = 0; i < 7; i++) {
                    CircleImageView imageView = new CircleImageView(MyApplication.getContext());
                    LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(30, 30);
                    int rightPadding = UiUtil.getPixelFromDp(5);
                    layoutParams.setMargins(0, 0, rightPadding, 0);
                    imageView.setLayoutParams(layoutParams);
                    imageView.setId(i);
                    imageView.setImageDrawable(drawables[i]);
                    imageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.secondary2));
                    imageView.setBorderWidth(1);
                    linearLayoutRecommendImage.addView(imageView);
                }
                initRecommend = true;
            }
        }
    }

    public class ViewHolderDescriptionImage extends RecyclerView.ViewHolder {
        public RecyclerView recyclerView;

        public ViewHolderDescriptionImage(View itemLayoutView) {
            super(itemLayoutView);
            recyclerView = (RecyclerView) itemLayoutView.findViewById(R.id.rvDescriptionImage);

            //ToDo replace with real list after data set ready
//            List<ImageData> imageDataList = style.getDescriptionImageList();
            List<ImageData> imageDataList = style.getFeaturedImageList();

            recyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));

            ProductImageRVAdapter mAdapter = new ProductImageRVAdapter(imageDataList, null);
            mAdapter.setContext(context);
            recyclerView.setAdapter(mAdapter);

            int spacingInPixels = MyApplication.getContext().getResources().getDimensionPixelSize(R.dimen.grid_spacing);
            recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

            int screenWidth = UiUtil.getDisplayWidth();
            recyclerView.setLayoutParams(new RelativeLayout.LayoutParams(screenWidth,
                    UiUtil.getAspectHeightOfDescImage(screenWidth) * imageDataList.size()));
        }

        public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
            private int space;

            public SpacesItemDecoration(int space) {
                this.space = space;
            }

            @Override
            public void getItemOffsets(Rect outRect, View view,
                                       RecyclerView parent, RecyclerView.State state) {
                outRect.bottom = space;

            }
        }
    }

    public static class ViewHolderOutfit extends RecyclerView.ViewHolder {
        BuyButtonSlidingDrawer buyButtonSlidingDrawer;
        final public FrameLayout frameLayoutSwipeButton;
        public ImageView buttonLike;


        public ViewHolderOutfit(View itemLayoutView) {
            super(itemLayoutView);

            buyButtonSlidingDrawer = (BuyButtonSlidingDrawer) itemLayoutView.findViewById(R.id.buy_item_view_included);
            frameLayoutSwipeButton = (FrameLayout) itemLayoutView.findViewById(R.id.frameLayoutSwipeButton);
            buttonLike = (ImageView) itemLayoutView.findViewById(R.id.buttonLike);
        }
    }

    public static class ViewHolderSuggestion extends RecyclerView.ViewHolder {
        public RecyclerView recyclerView;

        public ViewHolderSuggestion(View itemLayoutView) {
            super(itemLayoutView);
            recyclerView = (RecyclerView) itemLayoutView.findViewById(R.id.rvSuggestionImage);
            int spacingInPixels = itemLayoutView.getResources().getDimensionPixelSize(R.dimen.grid_spacing);
            recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, spacingInPixels, true));
        }
    }

    public static class ViewHolderScore extends RecyclerView.ViewHolder {
        public TextView tvDescScore;
        public TextView tvServiceScore;
        public TextView tvShipmentScore;

        public ViewHolderScore(View itemLayoutView) {
            super(itemLayoutView);

            tvDescScore = (TextView) itemLayoutView.findViewById(R.id.tvDescScore);
            tvServiceScore = (TextView) itemLayoutView.findViewById(R.id.tvServiceScore);
            tvShipmentScore = (TextView) itemLayoutView.findViewById(R.id.tvShipmentScore);
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public List<ProductDetailListItem> getItemsData() {
        return itemsData;
    }
}
