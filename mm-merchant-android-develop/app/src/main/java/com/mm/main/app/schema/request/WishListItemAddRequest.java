package com.mm.main.app.schema.request;

import android.content.Intent;

/**
 * Created by ductranvt on 1/26/2016.
 */
public class WishListItemAddRequest {
    String CultureCode;
    String StyleCode;
    String UserKey;
    String CartKey;
    String WishlistKey;
    Integer SkuId;
    String ColorKey;
    Integer MerchantId;
    Integer CartItemId;

    public WishListItemAddRequest(String cultureCode, String styleCode, String userKey, String cartKey, Integer skuId, String colorKey, Integer merchantId) {
        CultureCode = cultureCode;
        StyleCode = styleCode;
        UserKey = userKey;
        CartKey = cartKey;
        SkuId = skuId;
        ColorKey = colorKey;
        MerchantId = merchantId;
    }

    public WishListItemAddRequest(String cultureCode, String userKey, String cartKey, String wishlistKey, Integer cartItemID) {
        CultureCode = cultureCode;
        UserKey = userKey;
        CartKey = cartKey;
        WishlistKey = wishlistKey;
        CartItemId = cartItemID;
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getStyleCode() {
        return StyleCode;
    }

    public void setStyleCode(String styleCode) {
        StyleCode = styleCode;
    }

    public String getCartKey() {
        return CartKey;
    }

    public void setCartKey(String cartKey) {
        CartKey = cartKey;
    }

    public Integer getSkuId() {
        return SkuId;
    }

    public void setSkuId(Integer skuId) {
        SkuId = skuId;
    }

    public String getColorKey() {
        return ColorKey;
    }

    public void setColorKey(String colorKey) {
        ColorKey = colorKey;
    }

    public Integer getMerchantId() {
        return MerchantId;
    }

    public void setMerchantId(Integer merchantId) {
        MerchantId = merchantId;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }
}
