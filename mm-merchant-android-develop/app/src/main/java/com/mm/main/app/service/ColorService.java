package com.mm.main.app.service;

import com.mm.main.app.schema.Color;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by henrytung on 24/11/2015.
 */
public interface ColorService {

    String SERIVCE_PATH = "color";

    @GET(SERIVCE_PATH + "/list")
    Call<List<Color>> list();
}
