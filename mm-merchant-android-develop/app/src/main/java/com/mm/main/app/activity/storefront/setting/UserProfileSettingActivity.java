package com.mm.main.app.activity.storefront.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.setting.UserProfileSettingRvAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.decoration.DividerItemDecoration;
import com.mm.main.app.event.UpdatePhotoEvent;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.UserProfileSettingListItem;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.LifeCycleManager;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.PhotoUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.mm.storefront.app.wxapi.WXEntryActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by nhutphan on 2/26/2016.
 */
public class UserProfileSettingActivity extends AppCompatActivity {

    @Bind(R.id.listView)
    RecyclerView recyclerView;

    @BindString(R.string.LB_CA_SHIPPING_ADDR)
    String setting_item_shipping_address;

    @BindString(R.string.LB_CA_MOBILE)
    String setting_item_mobile;

    @BindString(R.string.LB_CA_PASSWORD)
    String setting_item_password;

    @BindString(R.string.LB_CA_PROF_LINK_ACC)
    String setting_item_link_account;

    @BindString(R.string.LB_CA_ALIPAY_ACC)
    String setting_item_alipay_acc;

    @BindString(R.string.LB_CA_PROF_REVIEWS)
    String setting_item_reviews;

    @BindString(R.string.LB_CA_PROF_CS)
    String setting_item_customer_service;

    @BindString(R.string.setting_item_divider)
    String setting_item_divider;

    private List<UserProfileSettingListItem> settingList;
    private UserProfileSettingRvAdapter settingAdapter;
    private User user;
    private static final int CAMERA_REQUEST = 9872;
    private static final int SELECT_FILE_REQUEST = CAMERA_REQUEST + 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_setting);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewUser(MmGlobal.getUserId());
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_REQUEST:
                case SELECT_FILE_REQUEST:
                    Uri selectedImageUri = data.getData();
                    try {
                        Bitmap photo = PhotoUtil.getCorrectlyOrientedImage(this, selectedImageUri);
                        if(photo != null){
                            settingAdapter.setUserPhoto(photo);
                            settingAdapter.notifyDataSetChanged();
                            PhotoUtil.uploadPhoto(photo, PhotoUtil.PhotoType.PROFILE);
                        }
                    } catch (Exception e) {
                        Logger.e(getClass().getSimpleName().toString(), e.getMessage());
                    }
                    break;
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btnLogoutProfile)
    protected void signOut() {
        LifeCycleManager.storeFrontLogout(this, WXEntryActivity.class);
    }

    private void viewUser(Integer userId) {
        MmProgressDialog.show(this);
        APIManager.getInstance().getUserService().viewUser(userId)
                .enqueue(new MmCallBack<User>(UserProfileSettingActivity.this) {
                    @Override
                    public void onSuccess(Response<User> response, Retrofit retrofit) {
                        user = response.body();
                        renderListView();
                    }

                });
    }

    private void renderListView() {
        settingList = new ArrayList<>();
        settingList.add(new UserProfileSettingListItem(setting_item_divider, "", UserProfileSettingListItem.ItemType.TYPE_DIVIDER));
        settingList.add(new UserProfileSettingListItem("user info", user, UserProfileSettingListItem.ItemType.TYPE_USER_INFO));
        settingList.add(new UserProfileSettingListItem(setting_item_divider, "", UserProfileSettingListItem.ItemType.TYPE_DIVIDER));
        settingList.add(new UserProfileSettingListItem(setting_item_shipping_address, "", UserProfileSettingListItem.ItemType.TYPE_TEXT_ARROW));
        settingList.add(new UserProfileSettingListItem(setting_item_divider, "", UserProfileSettingListItem.ItemType.TYPE_DIVIDER));
        settingList.add(new UserProfileSettingListItem(setting_item_mobile, user.getMobileNumber(), UserProfileSettingListItem.ItemType.TYPE_TEXT_ARROW));
        settingList.add(new UserProfileSettingListItem(setting_item_password, user.getPassword(), UserProfileSettingListItem.ItemType.TYPE_TEXT_ARROW));
        settingList.add(new UserProfileSettingListItem(setting_item_link_account, "", UserProfileSettingListItem.ItemType.TYPE_TEXT_ARROW));
        settingList.add(new UserProfileSettingListItem(setting_item_alipay_acc, "", UserProfileSettingListItem.ItemType.TYPE_TEXT_ARROW));
        settingList.add(new UserProfileSettingListItem(setting_item_divider, "", UserProfileSettingListItem.ItemType.TYPE_DIVIDER));
        settingList.add(new UserProfileSettingListItem(setting_item_reviews, "", UserProfileSettingListItem.ItemType.TYPE_TEXT_ARROW));
        settingList.add(new UserProfileSettingListItem(setting_item_customer_service, "", UserProfileSettingListItem.ItemType.TYPE_TEXT_ARROW));

        settingAdapter = new UserProfileSettingRvAdapter(this, settingList);
        recyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(UserProfileSettingActivity.this));
        recyclerView.setAdapter(settingAdapter);
    }

    public void onClickUserPhoto(){
        final CharSequence[] items = {getString(R.string.LB_TAKE_PHOTO), getString(R.string.LB_PHOTO_LIBRARY)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        startCamera();
                        break;
                    case 1:
                        pickImage();
                        break;
                    default:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }

    private void startCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    private void pickImage() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.LB_PHOTO_LIBRARY)), SELECT_FILE_REQUEST);
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(UpdatePhotoEvent event) {
        Logger.i(this.getClass().getSimpleName(), "Success");
    }
}
