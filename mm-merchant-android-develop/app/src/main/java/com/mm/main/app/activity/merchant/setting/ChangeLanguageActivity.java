package com.mm.main.app.activity.merchant.setting;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.merchant.setting.LanguageSelectListAdapter;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.schema.Language;
import com.mm.main.app.schema.response.SaveUserResponse;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import retrofit.Response;
import retrofit.Retrofit;


public class ChangeLanguageActivity extends ActionBarActivity {

    final public static String EXTRA_USER_DETAIL = "USER_DETAIL";

    @Bind(R.id.list)
    ListView listView;

    @BindString(R.string.LB_LANGUAGE)
    String title;
    private User user;

    LanguageSelectListAdapter languageSelectListAdapter;
    List<Language> languages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_language);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, title);
        user = (User) this.getIntent().getSerializableExtra(EXTRA_USER_DETAIL);
        LanguageManager languageManager = LanguageManager.getInstance();
        languages = languageManager.getLanguageList().getLanguageList();
        languageSelectListAdapter = new LanguageSelectListAdapter(this, languages, user.getLanguageId());
        listView.setAdapter(languageSelectListAdapter);
    }

    @OnItemClick(R.id.list)
    public void onItemClicked(int position) {
        languageSelectListAdapter.setSelectedItemId(languages.get(position).getLanguageId());
        languageSelectListAdapter.notifyDataSetChanged();
        listView.invalidate();
        user.setLanguageId(languages.get(position).getLanguageId());
        MmProgressDialog.show(this);
        APIManager.getInstance().getUserService().save(user)
                .enqueue(new MmCallBack<SaveUserResponse>(ChangeLanguageActivity.this) {
                    @Override
                    public void onSuccess(Response<SaveUserResponse> response, Retrofit retrofit) {
                        LanguageManager.getInstance().updateCurrentCultureCode(user.getLanguageId());
                        finish();
                    }
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void finish() {
        super.finish();

        overridePendingTransition(R.anim.not_move, R.anim.left_to_right);
    }
}
