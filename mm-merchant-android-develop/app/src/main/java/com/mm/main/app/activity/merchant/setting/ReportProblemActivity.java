package com.mm.main.app.activity.merchant.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.Problem;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

//import com.mm.merchant.app.facade.SettingServiceFacade;


public class ReportProblemActivity extends ActionBarActivity {

    final public static String EXTRA_USER_DETAIL = "USER_DETAIL";

    @Bind(R.id.subjectEditText)
    TextView subjectEditText;
    @Bind(R.id.contentEditText)
    EditText contentEditText;
//    @Bind(R.id.pinterestView)
//    PinterestView pinterestView;

    @Bind((R.id.inputSubjectLayout))
    TextInputLayout inputSubjectLayout;

    @BindString(R.string.LB_REPORT_PROBLEM)
    String title;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        render();
    }

//    private void initPinterestView() {
//        pinterestView.addShowView(40,PinterestView.createChildView(R.drawable.googleplus, "google", getBaseContext())
//                ,PinterestView.createChildView(R.drawable.linkedin, "linkedin", getBaseContext())
//                ,PinterestView.createChildView(R.drawable.twitter, "twitter", getBaseContext())
//                ,PinterestView.createChildView(R.drawable.pinterest, "pinterest", getBaseContext()));
//        /**
//         * add pinterestview menu and Pre click view click
//         */
//        pinterestView.setPinClickListener(new PinterestView.PinMenuClickListener() {
//
//            @Override
//            public void onMenuItemClick(int childAt) {
//                Toast.makeText(ReportProblemActivity.this, "onMenuItemClick" + childAt, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onPreViewClick() {
//                Toast.makeText(ReportProblemActivity.this, "onPreViewClick", Toast.LENGTH_SHORT).show();
//            }
//        });
//        /**
//         * dispatch pre click view onTouchEvent to PinterestView
//         */
//        findViewById(R.id.contentEditText).setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                pinterestView.dispatchTouchEvent(event);
//                return true;
//            }
//        });
//    }

    private void render() {
        setContentView(R.layout.activity_report_a_problem);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(ReportProblemActivity.this, title);
        user = (User) this.getIntent().getSerializableExtra(EXTRA_USER_DETAIL);
//        initPinterestView();
    }


    @OnClick(R.id.send_button)
    public void reportProblem() {
        Problem problem = new Problem(MmGlobal.getUserId(), subjectEditText.getText().toString(), contentEditText.getText().toString());
        MmProgressDialog.show(this);
        APIManager.getInstance().getUserService().reportProblem(problem)
                .enqueue(new MmCallBack(ReportProblemActivity.this) {
                    @Override
                    public void onSuccess(Response response, Retrofit retrofit) {
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
    }
}
