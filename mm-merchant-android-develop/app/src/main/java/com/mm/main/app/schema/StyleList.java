
package com.mm.main.app.schema;

import java.util.ArrayList;
import java.util.List;

public class StyleList {

    private List<Style> Data = new ArrayList<Style>();
    private Integer PageCount;
    private Integer PageCurrent;
    private Integer PageSize;
    private Integer RowTotal;

    /**
     * 
     * @return
     *     The Data
     */
    public List<Style> getData() {
        return Data;
    }

    /**
     * 
     * @param Data
     *     The Data
     */
    public void setData(List<Style> Data) {
        this.Data = Data;
    }

    /**
     * 
     * @return
     *     The PageCount
     */
    public Integer getPageCount() {
        return PageCount;
    }

    /**
     * 
     * @param PageCount
     *     The PageCount
     */
    public void setPageCount(Integer PageCount) {
        this.PageCount = PageCount;
    }

    /**
     * 
     * @return
     *     The PageCurrent
     */
    public Integer getPageCurrent() {
        return PageCurrent;
    }

    /**
     * 
     * @param PageCurrent
     *     The PageCurrent
     */
    public void setPageCurrent(Integer PageCurrent) {
        this.PageCurrent = PageCurrent;
    }

    /**
     * 
     * @return
     *     The PageSize
     */
    public Integer getPageSize() {
        return PageSize;
    }

    /**
     * 
     * @param PageSize
     *     The PageSize
     */
    public void setPageSize(Integer PageSize) {
        this.PageSize = PageSize;
    }

    /**
     * 
     * @return
     *     The RowTotal
     */
    public Integer getRowTotal() {
        return RowTotal;
    }

    /**
     * 
     * @param RowTotal
     *     The RowTotal
     */
    public void setRowTotal(Integer RowTotal) {
        this.RowTotal = RowTotal;
    }

}
