package com.mm.main.app.schema;

/**
 * Created by andrew on 6/11/15.
 */
public class AppError {
    public String getAppCode() {
        return AppCode;
    }

    public void setAppCode(String appCode) {
        AppCode = appCode;
    }

    public String getException() {
        return Exception;
    }

    public void setException(String exception) {
        Exception = exception;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStack() {
        return Stack;
    }

    public void setStack(String stack) {
        Stack = stack;
    }

    public int getLoginAttempts() {
        return LoginAttempts;
    }

    public void setLoginAttempts(int loginAttempts) {
        LoginAttempts = loginAttempts;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public boolean isMobile() {
        return IsMobile;
    }

    public void setMobile(boolean mobile) {
        IsMobile = mobile;
    }

    String AppCode;
    String Exception;
    String Message;
    String Stack;
    int LoginAttempts;
    String Username;
    boolean IsMobile;

    AppError(){

    }
}
