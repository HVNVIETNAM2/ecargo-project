package com.mm.main.app.adapter.strorefront.product;

import android.view.View;
import android.widget.RelativeLayout;

import com.mm.main.app.schema.Style;
import com.mm.main.app.utils.UiUtil;

import java.util.List;

/**
 * Adapter for Recycle View, View Holder Pattern with getView function!
 */
public class ProductRVOneLineAdapter extends ProductRVAdapter {

    ProductDetailRvAdapter detailRvAdapter;

    public void setDetailRvAdapter(ProductDetailRvAdapter detailRvAdapter) {
        this.detailRvAdapter = detailRvAdapter;
    }

    public ProductDetailRvAdapter getDetailRvAdapter() {
        return detailRvAdapter;
    }

    public ProductRVOneLineAdapter(List<Style> productList) {
        super(productList, null);
    }

    public ProductRVOneLineAdapter(List<Style> productList, ProductListOnClickListener productListOnClickListener) {
        super(productList, productListOnClickListener);
    }

    void setSize(View v) {
        int width = UiUtil.getAspectWidthOfProductImage();
        v.setLayoutParams(new RelativeLayout.LayoutParams(width, UiUtil.getAspectHeightOfSuggestionImage(width)));
    }
    //Disable scrolling, just display 2 first items.
    @Override
    public int getItemCount() {
        return productList.size() > 2 ? 2 : productList.size();
    }
}
