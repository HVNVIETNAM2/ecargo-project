package com.mm.main.app.service;

import com.mm.main.app.schema.Address;
import com.mm.main.app.schema.request.AddressSaveRequest;
import com.mm.main.app.schema.request.DefaultAddressSaveRequest;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by thienchau on 3/7/2016.
 */
public interface AddressService {
    String ADDRESS_PATH = "address";

    @GET(ADDRESS_PATH + "/list")
    Call<List<Address>> listAddressByUserKey(@Query("UserKey") String userKey);

    @POST(ADDRESS_PATH + "/save")
    Call<Address> saveAddress(@Body AddressSaveRequest addressSaveRequest);

    @POST(ADDRESS_PATH + "/default/save")
    Call<String> saveDefaultAddress(@Body DefaultAddressSaveRequest addressSaveRequest);

    @GET(ADDRESS_PATH + "/default/view")
    Call<Address> loadDefaultAddress(@Query("UserKey") String userKey);
}
