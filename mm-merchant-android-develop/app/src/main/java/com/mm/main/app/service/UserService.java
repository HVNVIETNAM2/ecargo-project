package com.mm.main.app.service;

import com.mm.main.app.schema.*;

import com.mm.main.app.schema.request.UserChangeLanguageRequest;
import com.mm.main.app.schema.request.UserResendLinkRequest;
import com.mm.main.app.schema.response.DeleteCoverResponce;
import com.mm.main.app.schema.response.DeleteImageResponce;
import com.mm.main.app.schema.response.SaveUserResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;

import java.util.List;

/**
 * Created by andrew on 30/9/15.
 */
public interface UserService {
    String USER_PATH = "user";

    @GET(USER_PATH + "/list/mm")
    Call<List<User>> list();

    @POST(USER_PATH + "/upload/coverimage")
    Call<DeleteCoverResponce> deleteCover();

    @POST(USER_PATH + "/upload/profileimage")
    Call<DeleteImageResponce> deleteProfileImage();

    @GET(USER_PATH + "/view")
    Call<User> viewUser(@Query("id") Integer id);

    @GET(USER_PATH + "/view")
    Observable<User> viewUserRx(@Query("id") Integer id);

    @GET(USER_PATH + "/reference")
    Call<User> userReference(@Query("id") Integer id);

    @POST(USER_PATH + "/save")
    Call<SaveUserResponse> save(@Body User user);

    @POST(USER_PATH + "/inventorylocation/change")
    Call<Boolean> changeInventoryLocation(@Body User user);

    @POST(USER_PATH + "/status/change")
    Call<User> changeStatus(@Body UserStatus userStatus);

    @POST(USER_PATH + "/email/change/immediate")
    Call<Boolean> changeEmail(@Body User user);

    @POST(USER_PATH + "/resend/link")
    Call<Boolean> resendLink(@Body UserResendLinkRequest userResendLinkRequest);

    @POST(USER_PATH + "/change/language")
    Call<Token> changeLanguage(@Body UserChangeLanguageRequest userChangeLanguageRequest);

    @POST(USER_PATH + "/password/change/immediate")
    Call<Boolean> changePassword(@Body User user);

    @POST(USER_PATH + "/mobile/change")
    Call<Boolean> changeMobile(@Body User user);

    @POST(USER_PATH + "/problem")
    Call<Boolean> reportProblem(@Body Problem problem);
}
