package com.mm.main.app.manager;

import com.mm.main.app.factory.RetrofitFactory;
import com.mm.main.app.service.*;

import retrofit.Retrofit;

/**
 * Created by andrew on 9/11/15.
 */
public class APIManager {

    private static Retrofit retrofit;
    private static Retrofit retrofitParse;
    private static APIManager singleton;
    private static UserService userService;
    private static ViewService viewService;
    private static AuthService authService;
    private static GeoService geoService;
    private static ImageService imageService;
    private static InventoryService inventoryService;
    private static MerchService merchService;
    private static ReferenceService referenceService;
    private static ProductService productService;
    private static CategoryService categoryService;
    private static OrderService orderService;
    private static BrandService brandService;
    private static ColorService colorService;
    private static SizeService sizeService;
    private static SearchService searchService;
    private static BadgeService badgeService;
    private static CartService cartService;
    private static WishListService wishlistService;
    private static TagService tagService;
    private static FollowService followService;
    private static AddressService addressService;
    private static FriendService friendService;

    private APIManager() {
        retrofit = RetrofitFactory.create();
        retrofitParse = RetrofitFactory.createParse();
    }

    public void refresehAPIManager() {
        retrofit = RetrofitFactory.create();
        retrofitParse = RetrofitFactory.createParse();
    }

    public static APIManager getInstance() {
        if (singleton == null) {
            singleton = new APIManager();
        }
        return singleton;
    }

    private <T> T getService(Object service, Class<T> className) {
        if (service == null) {
            service = retrofit.create(className);
        }

        return (T) service;
    }

    private <T> T getParseService(Object service, Class<T> className) {
        if (service == null) {
            service = retrofitParse.create(className);
        }

        return (T) service;
    }


    public UserService getUserService() {
        return getService(userService, UserService.class);
    }

    public ViewService getViewService() { return getService(viewService, ViewService.class); }

    public CartService getCartService() {
        return getService(cartService, CartService.class);
    }

    public WishListService getWishListService() {
        return getService(wishlistService, WishListService.class);
    }

    public BadgeService getBadgeService() {
        return getService(badgeService, BadgeService.class);
    }

    public CategoryService getCategoryService() {
        return getService(categoryService, CategoryService.class);
    }

    public OrderService getOrderService() {
        return getService(orderService, OrderService.class);
    }

    public BrandService getBrandService() {
        return getService(brandService, BrandService.class);
    }

    public ColorService getColorService() {
        return getService(colorService, ColorService.class);
    }

    public SizeService getSizeService() {
        return getService(sizeService, SizeService.class);
    }

    public AuthService getAuthService() {
        return getService(authService, AuthService.class);
    }

    public InventoryService getInventoryService() {
        return getService(inventoryService, InventoryService.class);
    }

    public ImageService getImageService() {
        return getService(imageService, ImageService.class);
    }

    public ReferenceService getReferenceService() {
        return getService(referenceService, ReferenceService.class);
    }

    public GeoService getGeoService() {
        return getService(geoService, GeoService.class);
    }

    public MerchService getMerchService() {
        return getService(merchService, MerchService.class);
    }

    public ProductService getProductService() {
//        return getParseService(productService,ProductService.class);
        return getService(productService, ProductService.class);
    }

    public SearchService getSearchService() {
        return getService(searchService, SearchService.class);
    }

    public TagService getTagService() {
        return getService(tagService, TagService.class);
    }

    public FollowService getFollowService() {
        return getService(followService, FollowService.class);
    }

    public FriendService getFriendService() {
        return getService(friendService, FriendService.class);
    }

    public AddressService getAddressService() {
        return getService(addressService, AddressService.class);
    }
}
