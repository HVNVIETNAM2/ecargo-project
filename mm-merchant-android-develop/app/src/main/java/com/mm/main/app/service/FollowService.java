package com.mm.main.app.service;

import com.mm.main.app.schema.Brand;
import com.mm.main.app.schema.Follow;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.schema.User;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by haivu on 2/18/2016.
 */
public interface FollowService {
    String FOLLOW_PATH = "follow";

    @POST(FOLLOW_PATH + "/merchant/save")
    Call<Boolean> saveFollowMerchant(@Body Follow follow);

    @POST(FOLLOW_PATH + "/merchant/delete")
    Call<Boolean> deleteFollowMerchant(@Body Follow follow);

    @GET(FOLLOW_PATH + "/merchant/followed")
    Call<List<Merchant>> viewMerchantFollowed(@Query("UserKey") String userKey, @Query("start") int start, @Query("limit") int limit);

    @GET(FOLLOW_PATH + "/merchant/following")
    Call<List<User>> viewMerchantFollowing(@Query("UserKey") String userKey, @Query("MerchantId") Integer merchantId);

    @POST(FOLLOW_PATH + "/user/save")
    Call<Boolean> saveFollowCurator(@Body Follow follows);

    @POST(FOLLOW_PATH + "/user/delete")
    Call<Boolean> deleteFollow(@Body Follow follows);

    @GET(FOLLOW_PATH + "/user/following")
    Call<List<User>> viewFollower(@Query("UserKey") String userKey);

    @GET(FOLLOW_PATH + "/user/following?IsCuratorUser=1")
    Call<List<User>> viewFollowingCurator(@Query("UserKey") String userKey);

    @GET(FOLLOW_PATH + "/user/following?IsCuratorUser=0")
    Call<List<User>> viewFollowingUser(@Query("UserKey") String userKey);


}
