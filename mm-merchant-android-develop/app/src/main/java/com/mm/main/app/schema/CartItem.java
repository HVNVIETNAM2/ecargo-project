package com.mm.main.app.schema;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ductranvt on 1/15/2016.
 */
public class CartItem implements Serializable{
    Integer SkuId;
    Integer CartId;
    Integer CartItemId;
    Integer Qty;
    String StyleCode;
    String SkuCode;
    String Bar;
    Integer BrandId;
    String BrandName;
    String BrandNameInvariant;
    String BrandImage;
    Integer BadgeId;
    Integer SeasonId;
    Integer SizeId;
    Integer ColorId;
    Integer GeoCountryId;
    Integer LaunchYear;
    Double PriceRetail;
    Double PriceSale;
    String SaleFrom;
    String SaleTo;
    String AvailableFrom;
    String AvailableTo;
    Integer QtySafetyThreshold;
    Integer WeightKg;
    Integer HeightCm;
    Integer WidthCm;
    Integer LengthCm;
    Integer MerchantId;
    Integer StatusId;
    Integer PrimaryCategoryId;
    String SizeName;
    String ColorKey;
    String ColorName;
    String SkuColor;
    String SkuName;
    Integer LocationCount;
    String QtyAts;
    Integer InventoryStatusId;
    String ProductImage;
    Style Style;

    public Integer getCartId() {
        return CartId;
    }

    public void setCartId(Integer cartId) {
        CartId = cartId;
    }

    public Integer getSkuId() {
        return SkuId;
    }

    public void setSkuId(Integer skuId) {
        SkuId = skuId;
    }

    public Integer getCartItemId() {
        return CartItemId;
    }

    public void setCartItemId(Integer cartItemId) {
        CartItemId = cartItemId;
    }

    public Integer getQty() {
        return Qty;
    }

    public void setQty(Integer qty) {
        Qty = qty;
    }

    public String getStyleCode() {
        return StyleCode;
    }

    public void setStyleCode(String styleCode) {
        StyleCode = styleCode;
    }

    public String getSkuCode() {
        return SkuCode;
    }

    public void setSkuCode(String skuCode) {
        SkuCode = skuCode;
    }

    public String getBar() {
        return Bar;
    }

    public void setBar(String bar) {
        Bar = bar;
    }

    public Integer getBrandId() {
        return BrandId;
    }

    public void setBrandId(Integer brandId) {
        BrandId = brandId;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public String getBrandNameInvariant() {
        return BrandNameInvariant;
    }

    public void setBrandNameInvariant(String brandNameInvariant) {
        BrandNameInvariant = brandNameInvariant;
    }

    public String getBrandImage() {
        return BrandImage;
    }

    public void setBrandImage(String brandImage) {
        BrandImage = brandImage;
    }

    public Integer getBadgeId() {
        return BadgeId;
    }

    public void setBadgeId(Integer badgeId) {
        BadgeId = badgeId;
    }

    public Integer getSeasonId() {
        return SeasonId;
    }

    public void setSeasonId(Integer seasonId) {
        SeasonId = seasonId;
    }

    public Integer getSizeId() {
        return SizeId;
    }

    public void setSizeId(Integer sizeId) {
        SizeId = sizeId;
    }

    public Integer getColorId() {
        return ColorId;
    }

    public void setColorId(Integer colorId) {
        ColorId = colorId;
    }

    public Integer getGeoCountryId() {
        return GeoCountryId;
    }

    public void setGeoCountryId(Integer geoCountryId) {
        GeoCountryId = geoCountryId;
    }

    public Integer getLaunchYear() {
        return LaunchYear;
    }

    public void setLaunchYear(Integer launchYear) {
        LaunchYear = launchYear;
    }

    public Double getPriceRetail() {
        return PriceRetail;
    }

    public void setPriceRetail(Double priceRetail) {
        PriceRetail = priceRetail;
    }

    public Double getPriceSale() {
        return PriceSale;
    }

    public void setPriceSale(Double priceSale) {
        PriceSale = priceSale;
    }

    public String getSaleFrom() {
        return SaleFrom;
    }

    public void setSaleFrom(String saleFrom) {
        SaleFrom = saleFrom;
    }

    public String getSaleTo() {
        return SaleTo;
    }

    public void setSaleTo(String saleTo) {
        SaleTo = saleTo;
    }

    public String getAvailableFrom() {
        return AvailableFrom;
    }

    public void setAvailableFrom(String availableFrom) {
        AvailableFrom = availableFrom;
    }

    public String getAvailableTo() {
        return AvailableTo;
    }

    public void setAvailableTo(String availableTo) {
        AvailableTo = availableTo;
    }

    public Integer getQtySafetyThreshold() {
        return QtySafetyThreshold;
    }

    public void setQtySafetyThreshold(Integer qtySafetyThreshold) {
        QtySafetyThreshold = qtySafetyThreshold;
    }

    public Integer getWeightKg() {
        return WeightKg;
    }

    public void setWeightKg(Integer weightKg) {
        WeightKg = weightKg;
    }

    public Integer getHeightCm() {
        return HeightCm;
    }

    public void setHeightCm(Integer heightCm) {
        HeightCm = heightCm;
    }

    public Integer getWidthCm() {
        return WidthCm;
    }

    public void setWidthCm(Integer widthCm) {
        WidthCm = widthCm;
    }

    public Integer getLengthCm() {
        return LengthCm;
    }

    public void setLengthCm(Integer lengthCm) {
        LengthCm = lengthCm;
    }

    public Integer getMerchantId() {
        return MerchantId;
    }

    public void setMerchantId(Integer merchantId) {
        MerchantId = merchantId;
    }

    public Integer getStatusId() {
        return StatusId;
    }

    public void setStatusId(Integer statusId) {
        StatusId = statusId;
    }

    public Integer getPrimaryCategoryId() {
        return PrimaryCategoryId;
    }

    public void setPrimaryCategoryId(Integer primaryCategoryId) {
        PrimaryCategoryId = primaryCategoryId;
    }

    public String getSizeName() {
        return SizeName;
    }

    public void setSizeName(String sizeName) {
        SizeName = sizeName;
    }

    public String getColorKey() {
        return ColorKey;
    }

    public void setColorKey(String colorKey) {
        ColorKey = colorKey;
    }

    public String getColorName() {
        return ColorName;
    }

    public void setColorName(String colorName) {
        ColorName = colorName;
    }

    public String getSkuColor() {
        return SkuColor;
    }

    public void setSkuColor(String skuColor) {
        SkuColor = skuColor;
    }

    public String getSkuName() {
        return SkuName;
    }

    public void setSkuName(String skuName) {
        SkuName = skuName;
    }

    public Integer getLocationCount() {
        return LocationCount;
    }

    public void setLocationCount(Integer locationCount) {
        LocationCount = locationCount;
    }

    public String getQtyAts() {
        return QtyAts;
    }

    public void setQtyAts(String qtyAts) {
        QtyAts = qtyAts;
    }

    public Integer getInventoryStatusId() {
        return InventoryStatusId;
    }

    public void setInventoryStatusId(Integer inventoryStatusId) {
        InventoryStatusId = inventoryStatusId;
    }

    public String getProductImage() {
        return ProductImage;
    }

    public void setProductImage(String productImage) {
        ProductImage = productImage;
    }

    public double getPrice() {
        if (PriceSale != 0) {
            return PriceSale;
        } else {
            return PriceRetail;
        }
    }

    public com.mm.main.app.schema.Style getStyle() {
        return Style;
    }

    public void setStyle(com.mm.main.app.schema.Style style) {
        Style = style;
    }

    public ProductColor getSelectedColor() {
        if (ColorKey != null) {
            return Style.findColorByColorKey(ColorKey);
        }
        return null;
    }

    public ProductSize getSelectedSize() {
        Sku sku = Style.findSkuBySkuId(SkuId);
        if (sku != null) {
            return Style.findSizeBySizeId(sku.getSizeId());
        }
        return null;
    }
}
