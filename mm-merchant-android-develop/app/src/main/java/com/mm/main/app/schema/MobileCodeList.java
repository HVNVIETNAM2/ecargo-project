package com.mm.main.app.schema;

import java.util.List;

/**
 * Created by andrew on 9/11/15.
 */
public class MobileCodeList {
    public List<MobileCode> getMobileCodeList() {
        return MobileCodeList;
    }

    public void setMobileCodeList(List<MobileCode> mobileCodeList) {
        MobileCodeList = mobileCodeList;
    }

    List<MobileCode> MobileCodeList;

    public MobileCodeList() {
    }
}
