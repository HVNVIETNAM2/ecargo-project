package com.mm.main.app.schema.request;

/**
 * Created by andrew on 9/11/15.
 */
public class ReactivateRequest {
    String UserKey;
    String ActivationToken;

    public ReactivateRequest() {
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public String getActivationToken() {
        return ActivationToken;
    }

    public void setActivationToken(String activationToken) {
        ActivationToken = activationToken;
    }

    public ReactivateRequest(String userKey, String activationToken) {

        UserKey = userKey;
        ActivationToken = activationToken;
    }
}
