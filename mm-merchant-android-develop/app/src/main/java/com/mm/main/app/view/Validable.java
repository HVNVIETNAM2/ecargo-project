package com.mm.main.app.view;

/**
 * Created by andrew on 10/11/15.
 */
public interface Validable {
    void validate();
}
