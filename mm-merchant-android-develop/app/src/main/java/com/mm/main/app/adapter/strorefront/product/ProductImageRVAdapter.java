package com.mm.main.app.adapter.strorefront.product;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mm.main.app.activity.storefront.product.PhotosViewerActivity;
import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.schema.ImageData;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

/**
 * Adapter for Recycle View, View Holder Pattern with getView function!
 */
public class ProductImageRVAdapter extends RecyclerView.Adapter<ProductImageRVAdapter.ImageViewHolder> {

    public interface ProductListOnClickListener {
        void onClick(int position, Object data);
    }

    List<ImageData> imageDatas;
    ProductListOnClickListener productListOnClickListener;
    Activity context;

    public boolean[] likes;

    public  ProductImageRVAdapter(List<ImageData> imageDatas, ProductListOnClickListener productListOnClickListener) {
        this.imageDatas = imageDatas;
        this.productListOnClickListener = productListOnClickListener;
    }

    public void setContext(Activity context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return imageDatas.size();
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_default, viewGroup, false);
        setSize(v);

        ImageViewHolder.ProductViewHolderOnClickListener productOnClickListener = new ImageViewHolder.ProductViewHolderOnClickListener() {
            @Override
            public void onClick(int position) {
//                proceedProductDetail(position);
            }
        };
        ImageViewHolder imageViewHolder = new ImageViewHolder(v, imageDatas, productOnClickListener);
        imageViewHolder.setContext(context);
        return imageViewHolder;
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder imageViewHolder, final int i) {
        imageViewHolder.setContext(context);
        imageViewHolder.itemView.setTag(i);
        Picasso.with(MyApplication.getContext()).load(PathUtil.getProductImageUrl(imageDatas.get(i).getImageKey())).into(imageViewHolder.defaultImage);

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    void setSize(View v) {

    }


    public static class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private static final String TAG = "Inside Product View Holder";
        List<ImageData> imageDatas;
        ImageView defaultImage;
        Activity context;
        ProductViewHolderOnClickListener productViewHolderOnClickListener;

        public void setContext(Activity context) {
            this.context = context;
        }

        public interface ProductViewHolderOnClickListener {
            void onClick(int position);
        }

        ImageViewHolder(View itemView, List<ImageData> imageDatas, ProductViewHolderOnClickListener productViewHolderOnClickListener) {
            super(itemView);
            this.imageDatas = imageDatas;
            defaultImage = (ImageView) itemView.findViewById(R.id.defaultImage);

            int screenWidth = UiUtil.getDisplayWidth();
            defaultImage.getLayoutParams().width = screenWidth;
            defaultImage.requestLayout();

            itemView.setOnClickListener(this);
        }


        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
//            productViewHolderOnClickListener.onClick(getPosition());

            Intent intent = new Intent(context, PhotosViewerActivity.class);
            intent.putExtra(PhotosViewerActivity.IMAGE_DATA_KEY, (Serializable) imageDatas);
            intent.putExtra(PhotosViewerActivity.CURRENT_POS_KEY, (int) v.getTag());
            context.startActivity(intent);
            context.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        }
    }

    public List<ImageData> getProductList() {
        return imageDatas;
    }
}
