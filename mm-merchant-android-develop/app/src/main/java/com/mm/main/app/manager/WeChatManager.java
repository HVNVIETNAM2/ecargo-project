package com.mm.main.app.manager;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * Created by andrew on 19/2/2016.
 */
public class WeChatManager {
    private static WeChatManager singleton;

    public static IWXAPI getApi() {
        return api;
    }

    public static void setApi(IWXAPI api) {
        WeChatManager.api = api;
    }

    private static IWXAPI api;

    private WeChatManager() {
        api = WXAPIFactory.createWXAPI(MyApplication.getContext(), Constant.APP_ID, false);
        api.registerApp(Constant.APP_ID);
    }

    public static WeChatManager getInstance(){
        if (singleton == null) {
            singleton = new WeChatManager();
        }
        return singleton;
    }
}
