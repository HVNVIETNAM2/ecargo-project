package com.mm.main.app.service;

import com.mm.main.app.schema.InventoryLocation;
import com.mm.main.app.schema.request.InventoryStatusChangeRequest;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by andrew on 30/9/15.
 */
public interface InventoryService {
    String INVENTROY_PATH = "inventory";

    @GET(INVENTROY_PATH + "/location/list")
    Call<List<InventoryLocation>> listLocation(@Query("merchantid") Integer merchantId);

    @GET(INVENTROY_PATH + "/location/list/user")
    Call<List<InventoryLocation>> listLocationByUser(@Query("userid") Integer userId);

    @POST(INVENTROY_PATH + "/location/save")
    Call<InventoryLocation> saveLocation(@Body InventoryLocation inventoryLocation);

    @GET(INVENTROY_PATH + "/location/view")
    Call<InventoryLocation> viewLocation(@Query("id") Integer inventoryId);

    @POST(INVENTROY_PATH + "/status/change")
    Call<InventoryLocation> changeLocationStatus(@Body InventoryStatusChangeRequest inventoryStatusChangeRequest);
}
