package com.mm.main.app.activity.storefront.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.checkout.AddAddressActivity;
import com.mm.main.app.activity.storefront.checkout.CheckoutActivity;
import com.mm.main.app.activity.storefront.checkout.CheckoutConfirmActivity;
import com.mm.main.app.activity.storefront.signup.GeneralSignupActivity;
import com.mm.main.app.adapter.strorefront.cart.ShoppingCartRVAdapter;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.ShoppingCartHeaderRvItem;
import com.mm.main.app.listitem.ShoppingCartMerchantRvItem;
import com.mm.main.app.listitem.ShoppingCartProductRvItem;
import com.mm.main.app.listitem.ShoppingCartRvItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CheckoutFlowManager;
import com.mm.main.app.schema.Cart;
import com.mm.main.app.schema.CartItem;
import com.mm.main.app.utils.MmCallBack;
import com.mm.storefront.app.wxapi.WXEntryActivity;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class ShoppingCartActivity extends AppCompatActivity {

    @Bind(R.id.txtTitle)
    TextView txtTitle;

    @Bind(R.id.textViewSelectAll)
    TextView textViewSelectAll;

    @Bind(R.id.textViewTotalAmountLabel)
    TextView textViewTotalAmountLabel;

    @Bind(R.id.shopping_cart_list)
    RecyclerView mRecyclerView;

    @Bind(R.id.checkbox_all_item_cart)
    CheckBox checkbox_all_item_cart;

    @Bind(R.id.totalItems)
    TextView totalItems;

    @Bind(R.id.textViewTotalAmount)
    TextView textViewTotalAmount;

    @Bind(R.id.blurLayer)
    public View blurLayer;

    @Bind(R.id.btn_confirm_cart)
    Button confirmCheckoutBtn;


    ShoppingCartRVAdapter mAdapter;
    List<ShoppingCartRvItem> items;

    private Hashtable<String, Boolean> selectedHashMerchant;
    private Hashtable<String, Boolean> selectedHashProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);

        ButterKnife.bind(this);
        addIdForAutomationTest();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(mActivity,LinearLayoutManager.VERTICAL));

        items = new ArrayList<ShoppingCartRvItem>();
        selectedHashMerchant = new Hashtable<>();
        selectedHashProduct = new Hashtable<>();

        MmCallBack callback = new MmCallBack<Cart>(ShoppingCartActivity.this) {
            @Override
            public void onSuccess(Response<Cart> response, Retrofit retrofit) {
            }

            @Override
            public void onFailure(Throwable t) {
                super.onFailure(t);
            }

            @Override
            public void onResponse(Response<Cart> response, Retrofit retrofit) {
                Cart cart = response.body();

                try {
                    loadDataForCart(cart);

                    //RecyclerView.Adapter
                    mAdapter = new ShoppingCartRVAdapter(ShoppingCartActivity.this, items);

                    //Apply this adapter to the RecyclerView
                    mRecyclerView.setAdapter(mAdapter);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        if (MmGlobal.isLogin()) {

            APIManager.getInstance()
                    .getCartService()
                    .viewCartByUserKey(MmGlobal.getUserKey())
                    .enqueue(callback);

        } else {

            final String cartKey = MmGlobal.anonymousShoppingCartKey();

            if (cartKey.isEmpty()) {
                //no request

                loadDataForCart(null);

                //RecyclerView.Adapter
                mAdapter = new ShoppingCartRVAdapter(ShoppingCartActivity.this, items);

                //Apply this adapter to the RecyclerView
                mRecyclerView.setAdapter(mAdapter);

            } else {

                APIManager.getInstance()
                        .getCartService()
                        .viewCartByCartKey(cartKey)
                        .enqueue(callback);

            }
        }

    }

    public void saveSelectedItems() {
        selectedHashMerchant.clear();
        selectedHashProduct.clear();
        for (ShoppingCartRvItem item : items) {
            if (item instanceof ShoppingCartMerchantRvItem) {
                ShoppingCartMerchantRvItem shoppingCartMerchantRvItem = (ShoppingCartMerchantRvItem) item;
                if (shoppingCartMerchantRvItem.getSelected()) {
                    selectedHashMerchant.put(shoppingCartMerchantRvItem.getMerchant().getMerchantId().toString(), true);
                }
            }
            if (item instanceof ShoppingCartProductRvItem) {
                ShoppingCartProductRvItem shoppingCartProductRvItem = (ShoppingCartProductRvItem) item;
                if (shoppingCartProductRvItem.getSelected()) {
                    selectedHashProduct.put(shoppingCartProductRvItem.getProduct().getCartItemId().toString(), true);
                }
            }
        }
    }

    private void restoreSelectedItems() {
        for (ShoppingCartRvItem item : items) {
            if (item instanceof ShoppingCartMerchantRvItem) {
                ShoppingCartMerchantRvItem shoppingCartMerchantRvItem = (ShoppingCartMerchantRvItem) item;
                if (selectedHashMerchant.get(shoppingCartMerchantRvItem.getMerchant().getMerchantId().toString()) != null) {
                    shoppingCartMerchantRvItem.setSelected(true);
                }
            }
            if (item instanceof ShoppingCartProductRvItem) {
                ShoppingCartProductRvItem shoppingCartProductRvItem = (ShoppingCartProductRvItem) item;
                if (selectedHashProduct.get(shoppingCartProductRvItem.getProduct().getCartItemId().toString()) != null) {
                    shoppingCartProductRvItem.setSelected(true);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
            updateBottomBar();
        }
        blurLayer.setVisibility(View.GONE);
        getSupportActionBar().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CheckoutActivity.CHECK_OUT_REQUEST:
                    Cart cart = (Cart) data.getSerializableExtra("CART");
                    loadDataForCart(cart);
                    mAdapter.notifyDataSetChanged();
                    updateBottomBar();
                    this.invalidateOptionsMenu();
                    break;
            }
        }
    }

    public void loadDataForCart(Cart cart) {
        int productIndex = 0;
        int merchanIndex = 0;

        items.clear();

        items.add(new ShoppingCartHeaderRvItem());

        if (cart != null) {

            for (Cart.CartMerchant merchant : cart.getMerchantList()) {

                ShoppingCartMerchantRvItem merchantItem = new ShoppingCartMerchantRvItem(merchant);
                merchantItem.setAppiumIndex(++merchanIndex);
                items.add(merchantItem);

                List<CartItem> products = merchant.getItemList();
                List<ShoppingCartProductRvItem> productListRows = new ArrayList<ShoppingCartProductRvItem>();

                for (CartItem product : products) {
                    ShoppingCartProductRvItem productItem = new ShoppingCartProductRvItem(product, merchantItem);
                    productItem.setAppiumIndex(++productIndex);
                    items.add(productItem);

                    productListRows.add(productItem);
                }

                //set the reference
                merchantItem.setProductItems(productListRows);
            }

            restoreSelectedItems();

        }
    }

    @OnClick(R.id.rlSelectItem)
    public void allItemChecked() {

        checkbox_all_item_cart.setChecked(!checkbox_all_item_cart.isChecked());
        updateSelectItem(checkbox_all_item_cart.isChecked());
    }

    public void updateSelectItem(boolean isSelect) {
        for (ShoppingCartRvItem item : items) {
            item.setSelected(isSelect);
        }
        showConfirmButton();
        mAdapter.notifyDataSetChanged();
        calculateTotalPrice();
    }

    public void checkAllSelected(boolean isSelectedAll) {
        checkbox_all_item_cart.setChecked(isSelectedAll);
    }

    public void calculateTotalPrice() {
        double totalPrice = 0;
        int totalQty = 0;
        for (ShoppingCartRvItem cartItem : items) {
            if (cartItem instanceof ShoppingCartProductRvItem) {
                if (cartItem.getSelected()) {
                    int qty = ((ShoppingCartProductRvItem) cartItem).getProduct().getQty();
                    totalQty += qty;
                    totalPrice += ((ShoppingCartProductRvItem) cartItem).getProduct().getPrice() * qty;
                }
            }
        }

        if (totalPrice == 0) {
            textViewTotalAmount.setText("0.0");
        } else {
            textViewTotalAmount.setText(NumberFormat.getNumberInstance(Locale.CHINA).format(totalPrice));
        }
        totalItems.setText("(" + totalQty + ")");
    }

    public void showConfirmButton() {
        //for check any selected
        boolean isAnyItemSelected = false;
        for (ShoppingCartRvItem cartItem : items) {
            if (cartItem instanceof ShoppingCartProductRvItem) {
                if (cartItem.getSelected()) {
                    isAnyItemSelected = true;
                    break;
                }
            }
        }
        if (isAnyItemSelected) {
            confirmCheckoutBtn.setEnabled(true);
        } else {
            confirmCheckoutBtn.setEnabled(false);
        }
    }

    public void updateBottomBar() {
        boolean isSelectedAll = true;
        if (items.size() == 1) {
            isSelectedAll = false;
        }
        for (ShoppingCartRvItem cartItem : items) {
            if (cartItem instanceof ShoppingCartProductRvItem) {
                if (!cartItem.getSelected()) {
                    isSelectedAll = false;
                    break;
                }
            }
        }

        checkAllSelected(isSelectedAll);

        calculateTotalPrice();

        showConfirmButton();
    }

    @OnClick(R.id.btn_confirm_cart)
    public void confirmCheckout() {
        CheckoutFlowManager.getInstance().setSelectedShoppingCartData(selectedData());
        if (MmGlobal.isLogin()) {
            showConfirmCheckout();
        } else {
            showLogin();
        }
    }

    private void showLogin() {
        CheckoutFlowManager.getInstance().clearActivitiesFlow();
        CheckoutFlowManager.getInstance().addActivityToFlow(this);
        Intent intent = new Intent(this, WXEntryActivity.class);
        intent.putExtra(Constant.LOGIN_IN_USING_APP_KEY, GeneralSignupActivity.LoginInAppType.TYPE_CHECK_OUT_NORMAL);
        startActivity(intent);
    }

    public void showConfirmCheckout() {
        Intent confirmPage = new Intent(this, CheckoutConfirmActivity.class);
        startActivity(confirmPage);
    }

    public void showAddAddress() {
        Intent intent = new Intent(this, AddAddressActivity.class);
        intent.putExtra(Constant.LOGIN_IN_USING_APP_KEY, GeneralSignupActivity.LoginInAppType.TYPE_CHECK_OUT_NORMAL);
        startActivity(intent);
    }

    private ArrayList<ShoppingCartMerchantRvItem> selectedData() {
        ArrayList<ShoppingCartMerchantRvItem> selectedMerchantList = new ArrayList<ShoppingCartMerchantRvItem>();

        for (ShoppingCartRvItem merchant : items) {
            if (merchant instanceof ShoppingCartMerchantRvItem) {
                List<ShoppingCartProductRvItem> listProduct = ((ShoppingCartMerchantRvItem) merchant).getProductItems();

                if (!merchant.getSelected()) {

                    boolean isMerchantInCheckOutList = false;

                    for (ShoppingCartProductRvItem product : listProduct) {
                        if (product.getSelected()) {
                            isMerchantInCheckOutList = true;
                            break;
                        }
                    }
                    //end of loop

                    if (isMerchantInCheckOutList) {
                        selectedMerchantList.add((ShoppingCartMerchantRvItem) merchant);
                    }

                } else {
                    selectedMerchantList.add((ShoppingCartMerchantRvItem) merchant);
                }
            }
        }

        return selectedMerchantList;
    }

    private void addIdForAutomationTest() {
        txtTitle.setContentDescription(getContentDescription("UILB_CART"));
        textViewSelectAll.setContentDescription(getContentDescription("UILB_SELECT_ALL_PI"));
        checkbox_all_item_cart.setContentDescription(getContentDescription("UICB_CHECK_ALL"));
        textViewTotalAmountLabel.setContentDescription(getContentDescription("UILB_SELECTED_PI_NO"));
        totalItems.setContentDescription(getContentDescription("UILB_SELECTED_PI_NO_VAL"));
        textViewTotalAmount.setContentDescription(getContentDescription("UILB_TOTAL_PRICE"));
        confirmCheckoutBtn.setContentDescription(getContentDescription("UIBT_CONFIRM_PURCHASE"));
    }

    public String getContentDescription(String key) {
        return String.format("ShoppingCart-%s", key);
    }

}
