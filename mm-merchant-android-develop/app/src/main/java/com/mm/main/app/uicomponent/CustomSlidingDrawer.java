package com.mm.main.app.uicomponent;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SlidingDrawer;

/**
 * Created by haivu on 2/1/2016.
 */
public class CustomSlidingDrawer extends SlidingDrawer {

    public  interface OnChangeListener {
         void onChange(float x);
    }

    public  interface OnTouchUpListener {
        void onTouchUp();
    }

    private OnChangeListener changeListener;

    private OnTouchUpListener touchUpListener;

    public CustomSlidingDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public void setOnChangeListener(OnChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    public void setTouchUpListener(OnTouchUpListener touchUpListener) {
        this.touchUpListener = touchUpListener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        if(event.getAction() == MotionEvent.ACTION_MOVE){
            changeListener.onChange(event.getX());
        }

        super.onTouchEvent(event);
        return true;
    }
}
