
package com.mm.main.app.schema;

public class BrandUnionMerchant {

    private Integer EntityId;
    private String Entity;
    private String Name;
    private String Description;
    private Integer IsListed;
    private Integer IsFeatured;
    private Integer IsRecommended;
    private String HeaderLogoImage;
    private String SmallLogoImage;
    private String LargeLogoImage;
    private String ProfileBannerImage;
    /**
     * 
     * @return
     *     The EntityId
     */
    public Integer getEntityId() {
        return EntityId;
    }

    /**
     * 
     * @param EntityId
     *     The EntityId
     */
    public void setEntityId(Integer EntityId) {
        this.EntityId = EntityId;
    }

    /**
     * 
     * @return
     *     The Entity
     */
    public String getEntity() {
        return Entity;
    }

    /**
     * 
     * @param Entity
     *     The Entity
     */
    public void setEntity(String Entity) {
        this.Entity = Entity;
    }

    /**
     * 
     * @return
     *     The Name
     */
    public String getName() {
        return Name;
    }

    /**
     * 
     * @param Name
     *     The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * 
     * @return
     *     The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * 
     * @param Description
     *     The Description
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * 
     * @return
     *     The IsListed
     */
    public Integer getIsListed() {
        return IsListed;
    }

    /**
     * 
     * @param IsListed
     *     The IsListed
     */
    public void setIsListed(Integer IsListed) {
        this.IsListed = IsListed;
    }

    /**
     * 
     * @return
     *     The IsFeatured
     */
    public Integer getIsFeatured() {
        return IsFeatured;
    }

    /**
     * 
     * @param IsFeatured
     *     The IsFeatured
     */
    public void setIsFeatured(Integer IsFeatured) {
        this.IsFeatured = IsFeatured;
    }

    /**
     * 
     * @return
     *     The IsRecommended
     */
    public Integer getIsRecommended() {
        return IsRecommended;
    }

    /**
     * 
     * @param IsRecommended
     *     The IsRecommended
     */
    public void setIsRecommended(Integer IsRecommended) {
        this.IsRecommended = IsRecommended;
    }

    /**
     * 
     * @return
     *     The HeaderLogoImage
     */
    public String getHeaderLogoImage() {
        return HeaderLogoImage;
    }

    /**
     * 
     * @param HeaderLogoImage
     *     The HeaderLogoImage
     */
    public void setHeaderLogoImage(String HeaderLogoImage) {
        this.HeaderLogoImage = HeaderLogoImage;
    }

    /**
     * 
     * @return
     *     The SmallLogoImage
     */
    public String getSmallLogoImage() {
        return SmallLogoImage;
    }

    /**
     * 
     * @param SmallLogoImage
     *     The SmallLogoImage
     */
    public void setSmallLogoImage(String SmallLogoImage) {
        this.SmallLogoImage = SmallLogoImage;
    }

    /**
     * 
     * @return
     *     The LargeLogoImage
     */
    public String getLargeLogoImage() {
        return LargeLogoImage;
    }

    /**
     * 
     * @param LargeLogoImage
     *     The LargeLogoImage
     */
    public void setLargeLogoImage(String LargeLogoImage) {
        this.LargeLogoImage = LargeLogoImage;
    }

    /**
     * 
     * @return
     *     The ProfileBannerImage
     */
    public String getProfileBannerImage() {
        return ProfileBannerImage;
    }

    /**
     * 
     * @param ProfileBannerImage
     *     The ProfileBannerImage
     */
    public void setProfileBannerImage(String ProfileBannerImage) {
        this.ProfileBannerImage = ProfileBannerImage;
    }

}
