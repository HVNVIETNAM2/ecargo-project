package com.mm.main.app.global;

import android.preference.PreferenceManager;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;

/**
 * Created by andrew on 9/11/15.
 */
public class MmGlobal {
    public static Integer getUserId() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getInt(Constant.USER_ID, 0);
    }

    public static void setUserId(Integer userId) {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().
                putInt(Constant.USER_ID, userId).commit();
    }

    public static String getUserKey() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getString(Constant.USER_KEY, "0");
    }

    public static void setUserKey(String userKey) {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().
                putString(Constant.USER_KEY, userKey).commit();
    }

    public static String anonymousShoppingCartKey() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getString(Constant.SHOPPING_CART_KEY, "");
    }

    public static void setAnonymousShoppingCartKey(String cartKey) {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().
                putString(Constant.SHOPPING_CART_KEY, cartKey).commit();
    }

    public static String anonymousWishListKey() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getString(Constant.WISHLIST_KEY, "");
    }

    public static void setAnonymousWishListKey(String cartKey) {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().
                putString(Constant.WISHLIST_KEY, cartKey).commit();
    }

    public static void setToken(String tokenString) {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().
                putString(Constant.TOKEN, tokenString).commit();
    }

    public static String getToken() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getString(Constant.TOKEN, "");

    }

    public static void setRemember(Boolean remember) {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().putBoolean(Constant.REMEMBER, remember).commit();
    }

    public static Boolean getRemember() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getBoolean(Constant.REMEMBER, false);
    }

    public static String getUserName() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getString(Constant.USER_NAME, "");
    }

    public static void setUserName(String userName) {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().
                putString(Constant.USER_NAME, userName).commit();
    }

    public static Boolean isLogin() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getBoolean(Constant.LOGIN_KEY, false);
    }

    public static void setLogin(Boolean login) {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().
                putBoolean(Constant.LOGIN_KEY, login).commit();
    }

    public static String getUserKeyForCartRequest() {
        if (!isLogin()) {
            return null;
        }
        return getUserKey();
    }

    public static String getCartKeyForCartRequest() {
        if (getUserKeyForCartRequest() != null) {
            return null;
        }
        if (anonymousShoppingCartKey().isEmpty()) {
            return "0";
        }
        return anonymousShoppingCartKey();
    }

    public static String getWishlistKeyForRequest() {
        if (getUserKeyForCartRequest() != null) {
            return null;
        }
        if (anonymousWishListKey().isEmpty()) {
            return null;
        }
        return anonymousWishListKey();
    }

    public static String getHost() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getString(Constant.HOST_KEY, Constant.HOST);
    }

    public static void setHost(String userKey) {
        if (userKey == null || userKey.equals("")) {
            userKey = null;
        }

        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().
                putString(Constant.HOST_KEY, userKey).commit();
    }

    public static void setConfig(String configData) {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).edit().putString(Constant.CONFIG_KEY, configData).commit();
    }

    public static String getConfig() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext()).getString(Constant.CONFIG_KEY, "");
    }
}
