package com.mm.main.app.schema;

/**
 * Created by haivu on 2/18/2016.
 */
public class Follow {
    private String UserKey;
    private String ToMerchantId;
    private String ToUserKeys;

    public String getToUserKey() {
        return ToUserKey;
    }

    public void setToUserKey(String toUserKey) {
        ToUserKey = toUserKey;
    }

    private String ToUserKey;


    public String getToUserKeys() {
        return ToUserKeys;
    }

    public void setToUserKeys(String toUserKeys) {
        ToUserKeys = toUserKeys;
    }

    public String getToMerchantId() {
        return ToMerchantId;
    }

    public void setToMerchantId(String toMerchantId) {
        ToMerchantId = toMerchantId;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

}
