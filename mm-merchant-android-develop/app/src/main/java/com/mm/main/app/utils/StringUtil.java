package com.mm.main.app.utils;

import android.graphics.Paint;
import android.widget.TextView;

import com.mm.main.app.application.MyApplication;

/**
 * Created by henrytung on 6/1/2016.
 */
public class StringUtil {

    public static String getResourceString(String key) {
        String resultString = key;
        if (MyApplication.getContext().getResources().getIdentifier(key, "string", MyApplication.getContext().getPackageName()) != 0) {
            resultString = MyApplication.getContext().getResources().getString(MyApplication.getContext().getResources().getIdentifier(key, "string", MyApplication.getContext().getPackageName()));
        }

        return resultString;
    }

    public static void setStrikeThroughText (TextView tv) {
        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
}
