package com.mm.main.app.factory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.LanguageManager;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
* Retrofit Factory to create a Retrofit object pointing to the API_URL and with OAuth 2.0 token set in the header
 */
public class RetrofitFactory {
    public static Retrofit create() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(5, TimeUnit.MINUTES);
        client.setReadTimeout(5, TimeUnit.MINUTES);
        client.interceptors().add(new Interceptor() {
            @Override
            public com.squareup.okhttp.Response intercept(Interceptor.Chain chain) throws IOException {
                Request request = chain.request();
                Request newRequest;

                HttpUrl url = chain.request().httpUrl()
                        .newBuilder()
                        .addQueryParameter("cc", LanguageManager.getInstance().getCurrentCultureCode())
                        .build();

                newRequest = request.newBuilder()
                        .addHeader("Authorization", MmGlobal.getToken())
                        .url(url).build();

                return chain.proceed(newRequest);
            }
        });
        return new Retrofit.Builder()
                .baseUrl(Constant.getApiURL())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    public static Retrofit createParse() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();
        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new Interceptor() {
            @Override
            public com.squareup.okhttp.Response intercept(Interceptor.Chain chain) throws IOException {
                Request request = chain.request();
                Request newRequest;

                HttpUrl url = chain.request().httpUrl()
                        .newBuilder()
                        .addQueryParameter("cc", LanguageManager.getInstance().getCurrentCultureCode())
                        .build();

                newRequest = request.newBuilder()
                        .addHeader("X-Parse-Application-Id", "tSF6abJ4ZwUM6pjpVhDF4poWYmxbPQQg7DbYh8GS")
                        .addHeader("X-Parse-REST-API-Key", "jib3yX9B3E4WV6iViDL8kbBQWKQWEkB66juY5Fr1")
                        .url(url).build();

                return chain.proceed(newRequest);
            }
        });
        return new Retrofit.Builder()
                .baseUrl(Constant.PARSE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }
}
