package com.mm.main.app.adapter.strorefront.checkout;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.listitem.AddressRvItem;
import com.mm.main.app.schema.Address;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thienchaud on 19-Feb-16.
 */
public class AddressSelectionRVAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    List<AddressRvItem> items;
    Activity activity;
    OnSelectAddressListener onSelectAddressListener;

    public AddressSelectionRVAdapter(Activity activity, List<AddressRvItem> items) {
        this.activity = activity;
        this.items = items;
    }

    public void setOnSelectAddressListener(OnSelectAddressListener onSelectAddressListener) {
        this.onSelectAddressListener = onSelectAddressListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_selection_item_view, parent, false);
        RecyclerView.ViewHolder viewHolder = new AddressSelectionViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        AddressRvItem addressRvItem = items.get(position);
        AddressSelectionViewHolder addressViewHolder = (AddressSelectionViewHolder) viewHolder;

        addressViewHolder.parentView.setTag(position);
        addressViewHolder.parentView.setOnClickListener(this);
        addressViewHolder.checkbox.setChecked(addressRvItem.isSelected());
        addressViewHolder.checkbox.setTag(position);
        addressViewHolder.checkbox.setOnClickListener(this);
        addressViewHolder.txtReceiver.setText(addressRvItem.getAddress().getRecipientName());
        addressViewHolder.txtPhone.setText(addressRvItem.getAddress().getFullPhoneNumber());
        addressViewHolder.txtStreet.setText(addressRvItem.getAddress().getFullAddress());
    }

    @Override
    public void onClick(View v) {
        int pos = (int) v.getTag();
        processSelectItem(pos, items.get(pos).isSelected());
    }

    void processSelectItem(int pos, boolean isSelected) {
        if (!isSelected) {
            for (int i = 0; i < items.size(); i++) {
                if (i == pos) {
                    items.get(i).setIsSelected(true);
                } else {
                    items.get(i).setIsSelected(false);
                }
            }
        }
        notifyDataSetChanged();
        if (onSelectAddressListener != null) {
            onSelectAddressListener.onSelectAddress(items.get(pos).getAddress());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class AddressSelectionViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.parentView)
        public LinearLayout parentView;

        @Bind(R.id.checkbox)
        public CheckBox checkbox;

        @Bind(R.id.txtReceiver)
        public TextView txtReceiver;

        @Bind(R.id.txtPhone)
        public TextView txtPhone;

        @Bind(R.id.txtStreet)
        public TextView txtStreet;

        public AddressSelectionViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
        }
    }

    public interface OnSelectAddressListener {
        public void onSelectAddress(Address address);
    }
}
