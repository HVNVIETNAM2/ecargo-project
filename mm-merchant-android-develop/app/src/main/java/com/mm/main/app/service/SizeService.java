package com.mm.main.app.service;

import com.mm.main.app.schema.Size;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by henrytung on 24/11/2015.
 */
public interface SizeService {

    String SERIVCE_PATH = "size";

    @GET(SERIVCE_PATH + "/list")
    Call<List<Size>> list();
}
