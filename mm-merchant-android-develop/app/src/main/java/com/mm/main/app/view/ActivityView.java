package com.mm.main.app.view;

import android.view.View;

import com.mm.main.app.utils.Controllable;

/**
 * Created by henrytung on 28/12/2015.
 */
public class ActivityView {
    Controllable activity;
    View view;

    public ActivityView(Controllable activity, View view) {
        this.activity = activity;
        this.view = view;
    }

    public Controllable getActivity() {
        return activity;
    }

    public void setActivity(Controllable activity) {
        this.activity = activity;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }
}
