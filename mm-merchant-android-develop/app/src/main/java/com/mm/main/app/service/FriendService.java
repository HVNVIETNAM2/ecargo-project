package com.mm.main.app.service;

import com.mm.main.app.schema.Follow;
import com.mm.main.app.schema.FriendRequest;
import com.mm.main.app.schema.User;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by binhtruongp on 2/26/2016.
 */
public interface FriendService {
    String FRIEND_PATH = "friend";

    @GET(FRIEND_PATH + "/list")
    Call<List<User>> listFriend(@Query("UserKey") String userKey, @Query("userkey") String userkey);

    @POST(FRIEND_PATH + "/delete")
    Call<Boolean> deleteRequest(@Body FriendRequest friendRequest);

    @GET(FRIEND_PATH + "/request/list")
    Call<List<User>> listRequest(@Query("UserKey") String userKey, @Query("userkey") String userkey);

    @POST(FRIEND_PATH + "/request")
    Call<Boolean> request(@Body FriendRequest friendRequest);

    @POST(FRIEND_PATH + "/request/accept")
    Call<Boolean> acceptRequest(@Body FriendRequest friendRequest);

    @GET(FRIEND_PATH + "/find")
    Call<List<User>> searchFriends(@Query("UserKey") String userKey, @Query("s") String displayName);
}
