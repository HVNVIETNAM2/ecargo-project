package com.mm.main.app.listitem;

import com.mm.main.app.schema.Cart;
import java.util.List;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class CheckoutConfirmMerchantRvItem implements CheckoutConfirmRvItem {
    private Cart.CartMerchant merchant;
    List<CheckoutConfirmProductRvItem> productItems;

    public CheckoutConfirmMerchantRvItem(Cart.CartMerchant merchant) {
        this.merchant = merchant;
    }

    public Cart.CartMerchant getMerchant() {
        return merchant;
    }

    @Override
    public ItemType getType() {
        return ItemType.TYPE_MERCHANT;
    }

    public List<CheckoutConfirmProductRvItem> getProductItems() {
        return productItems;
    }

    public void setProductItems(List<CheckoutConfirmProductRvItem> productItems) {
        this.productItems = productItems;
    }
}
