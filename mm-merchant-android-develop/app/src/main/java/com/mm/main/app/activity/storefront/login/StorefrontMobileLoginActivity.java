package com.mm.main.app.activity.storefront.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mm.main.app.R;
import com.mm.main.app.activity.merchant.setting.SelectRegionActivity;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.activity.storefront.signup.GeneralSignupActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CheckoutFlowManager;
import com.mm.main.app.manager.LifeCycleManager;
import com.mm.main.app.schema.AppError;
import com.mm.main.app.schema.Country;
import com.mm.main.app.schema.response.LoginResponse;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.AnimateUtil;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class StorefrontMobileLoginActivity extends GeneralSignupActivity {
    final String TAG = "MobileLoginActivity";

    @Bind(R.id.btnLogin)
    Button btnLogin;

//    @Bind(R.id.etUsername)
//    EditText etUsername;

    @Bind(R.id.etPassword)
    EditText etPassword;

    @Bind(R.id.edCountryCode)
    EditText edCountryCode;

    @Bind(R.id.tvCountry)
    TextView tvCountry;

    @Bind(R.id.tvPhoneNumber)
    EditText tvPhoneNumber;

    @Bind(R.id.tvError)
    TextView tvError;

    @Bind(R.id.etUsername)
    EditText etUsername;

    @Bind(R.id.rlLoginInputCode)
    RelativeLayout rlLoginInputCode;

    @Bind(R.id.rlLoginInput)
    RelativeLayout rlLoginInput;

    String userName;
    String password;
    private Country currentCountry;
    Handler handlerShowError = new Handler();
    Runnable closeErrorCallback;
    private final int SPEED = 15;
    private final int TIME_CLOSE_ERROR = 5;
    private final int MAX_LOGIN_RETRIES = 5;
    private int loginCounter = 1;
    private int mLoginRequestCode = -1;
    private boolean isChangeView;

    List<Country> countryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storefront_mobile_login);
        ButterKnife.bind(this);

        mLoginRequestCode = getIntent().getIntExtra(Constant.LOGIN_IN_REQUEST_CODE_KEY, -1);

        rlLoginInputCode.setVisibility(View.GONE);
        isChangeView = false;
        setUpCountryCodeEditText();
        fetchMobileCode();
        CheckoutFlowManager.getInstance().addActivityToFlow(this);
    }

    private void changeView() {
        if (!isChangeView) {
            rlLoginInputCode.setVisibility(View.VISIBLE);

            etUsername.setVisibility(View.GONE);
            etPassword.setText("");

            rlLoginInput.setBackgroundResource(R.drawable.login_input_group_1);
            rlLoginInput.getLayoutParams().height = UiUtil.getPixelFromDp(46);
            rlLoginInput.requestLayout();

            isChangeView = true;
        }
    }

    @OnClick(R.id.exitSpace)
    public void setupExit() {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.not_move, R.anim.top_to_bottom);
    }

    @OnClick(R.id.btnLogin)
    public void login() {

        ActivityUtil.closeKeyboard(this);

        if (!isChangeView) {
            if (validateUserName() && validatePassword()) {
                userName = etUsername.getText().toString();
                password = etPassword.getText().toString();
                loginService();
            }
        } else {
            if (validatePhone() && validatePassword()) {
                userName = edCountryCode.getText().toString() + "-" + tvPhoneNumber.getText().toString().trim();
                password = etPassword.getText().toString().trim();
                loginService();
            }
        }
    }

    private void startStoreFrontMain() {
        if (loginInApp == LoginInAppType.TYPE_CHECK_OUT_NORMAL) {
            if(mLoginRequestCode == StorefrontMainActivity.STORE_FRONT_LOGIN_REQUEST_CODE ||
                    mLoginRequestCode == StorefrontMainActivity.FRIEND_LOGIN_REQUEST_CODE) {
                setResult(RESULT_OK);
                finish();
            } else {
                CheckoutFlowManager.getInstance().actionCompleteLoginRequest();
            }
        }
        else if (loginInApp == LoginInAppType.TYPE_CHECK_OUT_SWIPE_TO_BUY) {
            CheckoutFlowManager.getInstance().actionCompleteLoginAtSwipeCheckout();
        }
        else {
            LifeCycleManager.cleanStart(StorefrontMobileLoginActivity.this, StorefrontMainActivity.class, null);
        }
    }
	
	private void startForgotPassword() {
        Intent intent = new Intent(StorefrontMobileLoginActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    private void loginService() {
        btnLogin.setEnabled(false);
        MmProgressDialog.show(this);
        APIManager.getInstance().getAuthService().login(userName, password)
                .enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                        MmProgressDialog.dismiss();
                        if (response.isSuccess()) {
                            LifeCycleManager.login(response, userName, true);
                            startStoreFrontMain();
                        } else if (loginCounter >= MAX_LOGIN_RETRIES) {
                            startForgotPassword();
                        } else {
                            loginCounter++;
                            btnLogin.setEnabled(true);
                            Gson gson = new Gson();
                            AppError myError = null;
                            String translatedErrorString = "";
                            try {
                                myError = gson.fromJson(response.errorBody().string(), AppError.class);
                                translatedErrorString = myError.getAppCode();
                                if (MyApplication.getContext().getResources().getIdentifier(myError.getAppCode(), "string", MyApplication.getContext().getPackageName()) != 0) {
                                    translatedErrorString = MyApplication.getContext().getResources().getString(MyApplication.getContext().getResources().getIdentifier(myError.getAppCode(), "string", MyApplication.getContext().getPackageName()));
                                }

                                if (myError.getAppCode().equals("MSG_ERR_USER_AUTHENTICATION_INVALID")) {
                                    translatedErrorString = translatedErrorString.replace(Constant.TEXT_REPLACE_VALUE_0, String.valueOf(3 - myError.getLoginAttempts()));
                                }
                            } catch (Exception e) {
                                Logger.d(TAG, e.getMessage());

                            }

                            // TODO : Change MSG_ERR_CA_INVALID_CREDENTIALS_1 to MSG_ERR_CA_INVALID_CREDENTIALS
//                            showError(getResources().getString(R.string.MSG_ERR_CA_INVALID_CREDENTIALS_1));


                            if (myError != null && myError.isMobile()) {
                                showError(getResources().getString(R.string.MSG_ERR_CA_INVALID_CREDENTIALS));
                                changeView();
                            } else {
                                showError(getResources().getString(R.string.MSG_ERR_USER_AUTHENTICATION_FAIL));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        MmProgressDialog.dismiss();
                        btnLogin.setEnabled(true);
                        ErrorUtil.alert(StorefrontMobileLoginActivity.this);
                        changeView();
                        t.printStackTrace();
                    }
                });
    }

    private boolean validatePhone() {
        String error = null;

        String phoneNumber = tvPhoneNumber.getText().toString();

        if (ValidationUtil.isEmpty(phoneNumber)) {
            error = getResources().getString(R.string.MSG_ERR_CA_ACCOUNT_NIL);
        }

        if (error != null) {
            showError(error);
        } else {
            if (!ValidationUtil.isValidPhone(phoneNumber)) {
                error = getResources().getString(R.string.MSG_ERR_CA_ACCOUNT_PATTERN);
            }

            if (error != null) {
                showError(error);
            }
        }

        return (error == null);
    }

    private boolean validateUserName() {
        String error = null;

        String userName = etUsername.getText().toString();

        if (ValidationUtil.isEmpty(userName)) {
            error = getResources().getString(R.string.MSG_ERR_CA_ACCOUNT_NIL);
        }

        if (error != null) {
            showError(error);
        }

        return (error == null);
    }


    private boolean validatePassword() {
        String error = null;
        String password = etPassword.getText().toString();

        if (password.trim().isEmpty()) {
            error = getResources().getString(R.string.MSG_ERR_CA_PW_NIL);
        }
        if (error != null) {
            showError(error);
        }
        return error == null;
    }

    @OnClick(R.id.btnRegion)
    public void onClickRegionButton() {
        Intent intent = new Intent(this, SelectRegionActivity.class);
        startActivityForResult(intent, SelectRegionActivity.SELECT_COUNTRY_CODE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == SelectRegionActivity.SELECT_COUNTRY_CODE_REQUEST) {
            currentCountry = (Country) data.getExtras().get(SelectRegionActivity.SELECTED_COUNTRY_CODE);
            updateCountryUI();
        }
    }

    private void updateCountryUI() {
        edCountryCode.setText(currentCountry.getMobileCode());
        tvCountry.setText(currentCountry.getGeoCountryName());
    }

    private void showError(String errorStr) {

        tvError.setText(errorStr);
        if (tvError.getVisibility() != View.VISIBLE) {
            AnimateUtil.expand(tvError, SPEED, 1, UiUtil.getPixelFromDp(35), null);

            if (closeErrorCallback != null) {
                handlerShowError.removeCallbacks(closeErrorCallback);
            }

            closeErrorCallback = new Runnable() {
                @Override
                public void run() {
                    closeError();
                }
            };
            handlerShowError.postDelayed(closeErrorCallback, TIME_CLOSE_ERROR * 1000);
        }
    }

    private void closeError() {
        if (tvError.getVisibility() == View.VISIBLE) {
            AnimateUtil.collapse(tvError, SPEED, null);
            tvError.setVisibility(View.INVISIBLE);
        }
    }

    public void setUpCountryCodeEditText() {

        edCountryCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s)) {
                    edCountryCode.setText("+");
                    edCountryCode.setSelection(edCountryCode.getText().length());
                }

                boolean foundCountry = false;
                for (Country country : countryList) {
                    if (s.toString().equals(country.getMobileCode())) {
                        currentCountry = country;
                        tvCountry.setText(currentCountry.getGeoCountryName());
                        foundCountry = true;
                        break;
                    }
                }

                if (foundCountry == false) {
                    currentCountry = null;
                    tvCountry.setText(getString(R.string.LB_COUNTRY_PICK));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    private void fetchMobileCode() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getGeoService().storefrontCountry()
                .enqueue(new MmCallBack<List<Country>>(this) {
                    @Override
                    public void onSuccess(Response<List<Country>> response, Retrofit retrofit) {
                        countryList = response.body();
                        if (countryList != null) {
                            currentCountry = countryList.get(0);
                            updateCountryUI();
                        }
                        MmProgressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        MmProgressDialog.dismiss();
                    }
                });
    }

    @OnClick(R.id.tvForgotPassword)
    public void onClickForgotPassword() {
        startForgotPassword();
    }

}
