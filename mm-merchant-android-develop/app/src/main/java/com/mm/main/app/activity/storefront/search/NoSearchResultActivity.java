package com.mm.main.app.activity.storefront.search;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;

import com.mm.main.app.R;
import com.mm.main.app.utils.Controllable;
import com.mm.main.app.adapter.strorefront.search.SearchListItemAdapter;
import com.mm.main.app.blurbehind.BlurBehind;
import com.mm.main.app.listitem.SearchListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.record.SearchHistory;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.schema.response.CompleteResponse;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import retrofit.Response;
import retrofit.Retrofit;

public class NoSearchResultActivity extends AppCompatActivity implements Controllable {

    @Bind(R.id.searchView)
    SearchView searchView;

    @Bind(R.id.mainListView)
    ListView listView;

    @Bind(R.id.btnSearch)
    ImageView btnSearch;

    Boolean suggestListInUse = true;
    List<CompleteResponse> hotList = new ArrayList<>();
    List<CompleteResponse> suggestList = new ArrayList<>();
    List<CompleteResponse> completeList = new ArrayList<>();

    private List<SearchListItem> searchListItems = new ArrayList<>();
    SearchListItemAdapter searchListItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeNoActionBar);
        setContentView(R.layout.activity_no_search_result);
        ButterKnife.bind(this);
        fetchSuggest();
        setupBackground();
        setupToolbar();

        renderMainListView();
    }

    private void renderMainListView() {
        searchListItemAdapter = new SearchListItemAdapter(this, searchListItems);
        listView.setAdapter(searchListItemAdapter);

        prepareListItem();

        searchListItemAdapter.notifyDataSetChanged();
    }

    public void prepareListItem() {
        searchListItems = new ArrayList<>();
        searchListItems.add(new SearchListItem("no search result", "", null, SearchListItem.ItemType.TYPE_NO_RESULT));
        hotList = suggestList;

//        searchListItems.add(new SearchListItem("hotItem title", "", null, SearchListItem.ItemType.TYPE_HOT_ITEM_TITLE));
//        addHotItemList();

        searchListItemAdapter.setItemList(searchListItems);

        searchListItemAdapter.notifyDataSetChanged();
    }

    @OnItemClick(R.id.mainListView)
    void onItemClick(int position) {
        if (searchListItems.get(position).getType() == SearchListItem.ItemType.TYPE_HISTORY_LIST ||
                searchListItems.get(position).getType() == SearchListItem.ItemType.TYPE_HOT_ITEM_LIST) {
            proceedSearch(searchListItems.get(position).getContent());
        }
    }

    @OnClick(R.id.btnSearch)
    public void proceedSearch() {
        String query = searchView.getQuery().toString();
        proceedSearch(query);
    }

    public void proceedSearch(String query) {
        SearchStyle.getInstance().clearAllFilter();
        SearchStyle.getInstance().setQueryString(query);
        setResult(query);
    }

    private void addHotItemList() {
        for (int i = 0; i < hotList.size(); i++) {
            searchListItems.add(new SearchListItem("hot item", hotList.get(i).getSearchTerm(), hotList.get(i), SearchListItem.ItemType.TYPE_HOT_ITEM_LIST));
        }
    }

    public void setupBackground() {
        BlurBehind.getInstance()
                .withAlpha(85)
                .withFilterColor(getResources().getColor(R.color.mm_input_gray)) //or Color.RED
                .setBackground(this);
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.

        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        searchView.setIconified(false);
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                SearchStyle.getInstance().setQueryString(null);
                setResult(null);
                return false;
            }
        });

        String query = SearchStyle.getInstance().getQueryString();
        if (!TextUtils.isEmpty(query)) {
            searchView.setQuery(query, false);
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                proceedSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    btnSearch.setVisibility(View.INVISIBLE);
                    suggestListInUse = true;
                    hotList = suggestList;
                    prepareListItem();
                } else {
                    btnSearch.setVisibility(View.VISIBLE);
                    suggestListInUse = false;
                    fetchComplete(newText);
                }

                return false;
            }
        });

    }

    private void setResult(String query) {
        if (!TextUtils.isEmpty(query)) {
            SearchHistory.getInstance().addSearchWord(query);
        }
        setResult(Activity.RESULT_OK);
        finish();
    }

    private void fetchSuggest() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getSearchService().complete(null)
                .enqueue(new MmCallBack<List<CompleteResponse>>(this) {
                    @Override
                    public void onSuccess(Response<List<CompleteResponse>> response, Retrofit retrofit) {
                        suggestList = response.body();
                        hotList = suggestList;
                        prepareListItem();
                    }
                });
    }

    private void fetchComplete(String queryString) {
        APIManager.getInstance().getSearchService().complete(queryString)
                .enqueue(new MmCallBack<List<CompleteResponse>>(this) {
                    @Override
                    public void onSuccess(Response<List<CompleteResponse>> response, Retrofit retrofit) {
                        completeList = response.body();
                        synchronized (suggestListInUse) {
                            if (!suggestListInUse) {
                                hotList = sort(completeList);
                                prepareListItem();
                            }
                        }
                    }
                });
    }

    private List<CompleteResponse> sort(List<CompleteResponse> oldList) {
        Collections.sort(oldList, new Comparator<CompleteResponse>() {
            public int compare(CompleteResponse o1, CompleteResponse o2) {
                return o1.getEntity().compareToIgnoreCase(o2.getEntity());
            }
        });

        return oldList;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {
        SearchStyle.getInstance().setQueryString(null);
        setResult(null);
    }

    @Override
    public void resume() {

    }
}
