package com.mm.main.app.activity.merchant.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.mm.main.app.R;
import com.mm.main.app.factory.ValidatorFactory;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.response.SaveUserResponse;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.concurrent.Callable;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;


public class ChangeNameActivity extends ActionBarActivity {

    final public static String EXTRA_USER_DETAIL = "USER_DETAIL";

    @Bind(R.id.firstNameEditText)
    EditText firstNameEditText;
    @Bind(R.id.middleNameEditText)
    EditText middleNameEditText;
    @Bind(R.id.lastNameEditText)
    EditText lastNameEditText;
    @Bind(R.id.displayNameEditText)
    EditText displayNameEditText;

    @Bind(R.id.firstNameLayout)
    TextInputLayout firstNameLayout;
    @Bind(R.id.middleNameLayout)
    TextInputLayout middleNameLayout;
    @Bind(R.id.lastNameLayout)
    TextInputLayout lastNameLayout;
    @Bind(R.id.displayNameLayout)
    TextInputLayout displayNameLayout;

    @BindString(R.string.MSG_SUC_DISP_NAME_CHANGE)
    String changeSucMessage;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_name);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, getResources().getString(R.string.LB_NAME));
        user = (User) this.getIntent().getSerializableExtra(EXTRA_USER_DETAIL);

        setupValidation();
    }

    private void setupValidation() {
        firstNameEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateFirstName();
            }
        }, firstNameLayout));
        middleNameEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateMiddleName();
            }
        }, middleNameLayout));
        lastNameEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateLastName();
            }
        }, lastNameLayout));
        displayNameEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateDisplayName();
            }
        }, displayNameLayout));
    }

    private void render() {
        firstNameEditText.setText(user.getFirstName());
        middleNameEditText.setText(user.getMiddleName());
        lastNameEditText.setText(user.getLastName());
        displayNameEditText.setText(user.getDisplayName());
    }

    @OnClick(R.id.confirm_change_name_button)
    public void changeName() {
        if (validateFirstName() & validateMiddleName() & validateLastName() & validateDisplayName()) {
            user.setFirstName(firstNameEditText.getText().toString());
            user.setMiddleName(middleNameEditText.getText().toString());
            user.setLastName(lastNameEditText.getText().toString());
            user.setDisplayName(displayNameEditText.getText().toString());
            MmProgressDialog.show(this);
            APIManager.getInstance().getUserService().save(user)
                    .enqueue(new MmCallBack<SaveUserResponse>(ChangeNameActivity.this) {

                        @Override
                        public void onSuccess(Response<SaveUserResponse> response, Retrofit retrofit) {
                            Toast.makeText(ChangeNameActivity.this, changeSucMessage, Toast.LENGTH_SHORT).show();
                            user = response.body().getUser();
                            Intent intent = ChangeNameActivity.this.getIntent();
                            intent.putExtra(SettingActivity.EXTRA_USER_DETAIL, user);
                            ChangeNameActivity.this.setResult(RESULT_OK, intent);
                            finish();
                        }
                    });
        }

    }

    @OnClick(R.id.cancel_change_name_button)
    public void cancelChange() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_name, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        render();
    }

    private boolean validateFirstName() {
        String error = null;

        String fieldName = getResources().getString(R.string.LB_FIRSTNAME);
        String value = firstNameEditText.getText().toString();

        if (!ValidationUtil.validate(ValidationUtil.INPUT_LIMIT_1_50_REGEX, value)) {
            error = getResources().getString(R.string.MSG_ERR_MERCHANT_FIRSTNAME);
        }


        ValidationUtil.setErrorMessage(firstNameLayout, error);

        return error == null;
    }

    private boolean validateMiddleName() {
        String error = null;

        String fieldName = getResources().getString(R.string.LB_MIDDLE_NAME);
        String value = middleNameEditText.getText().toString();

        if (!ValidationUtil.validate(ValidationUtil.INPUT_LIMIT_1_50_REGEX, value)) {
            error = getResources().getString(R.string.MSG_ERR_MERCHANT_MIDDLENAME);
        }

        ValidationUtil.setErrorMessage(middleNameLayout, error);

        return error == null;
    }

    private boolean validateLastName() {
        String error = null;

        String fieldName = getResources().getString(R.string.LB_LASTNAME);
        String value = lastNameEditText.getText().toString();

        if (!ValidationUtil.validate(ValidationUtil.INPUT_LIMIT_1_50_REGEX, value)) {
            error = getResources().getString(R.string.MSG_ERR_MERCHANT_LASTNAME);
        }

        ValidationUtil.setErrorMessage(lastNameLayout, error);

        return error == null;
    }

    private boolean validateDisplayName() {
        String error = null;

        String fieldName = getResources().getString(R.string.LB_DISP_NAME);
        String value = displayNameEditText.getText().toString();

        if (!ValidationUtil.validate(ValidationUtil.INPUT_LIMIT_3_25_REGEX, value)) {
//            error = getResources().getString(R.string.MSG_ERR_MERCHANT_DISP_NAME);
        }

        ValidationUtil.setErrorMessage(displayNameLayout, error);

        return error == null;
    }

}
