package com.mm.main.app.utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by henrytung on 2/12/2015.
 */
public class FormatUtil {

    public static String formatNumber(int number) {
        NumberFormat format = NumberFormat.getNumberInstance(Locale.CHINA);
        return format.format(number);
    }

    public static String formatAmount(double amount) {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CHINA);
        return format.format(amount);
    }

    public static String shortFormatAmount(double amount) {
        String amountStr = formatAmount(amount).replace(" ", "");
        if (amountStr.endsWith(".00")) {
            amountStr = amountStr.substring(0, amountStr.length() - 3);
        }

        return amountStr;
    }
}
