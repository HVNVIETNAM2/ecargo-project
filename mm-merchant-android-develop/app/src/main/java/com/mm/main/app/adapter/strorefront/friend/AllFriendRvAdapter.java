package com.mm.main.app.adapter.strorefront.friend;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.library.swipemenu.SwipeLayout;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.security.auth.callback.Callback;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by haivu on 3/4/2016.
 */
public class AllFriendRvAdapter extends RecyclerView.Adapter<AllFriendRvAdapter.AllFriendItemViewHolder> {

    public interface MyCallBack {
        void seeProfileUser(int position);

        void chatWithUser(int position);
    }

    MyCallBack callback;
    Context context;
    List<User> data;

    public AllFriendRvAdapter(Context context, List<User> data, MyCallBack callback) {
        this.context = context;
        this.data = data;
        this.callback = callback;
    }

    @Override
    public AllFriendRvAdapter.AllFriendItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AllFriendItemViewHolder viewHolder = null;

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_friend_list_item, parent, false);
        viewHolder = new AllFriendItemViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AllFriendRvAdapter.AllFriendItemViewHolder holder, final int position) {

        User user = data.get(position);

        holder.tvUserName.setText(user.getDisplayName());

        if (user.getIsCuratorUser()!= null && user.getIsCuratorUser()==1) {
            holder.imgProfile.setBorderColor(context.getResources().getColor(R.color.mm_red));
            holder.imgIsCurator.setVisibility(View.VISIBLE);
        } else {
            holder.imgProfile.setBorderColor(context.getResources().getColor(R.color.white));
            holder.imgIsCurator.setVisibility(View.GONE);
        }

        String url = PathUtil.getUserImageUrl(user.getProfileImage());
        Picasso.with(MyApplication.getContext()).load(url).placeholder(R.drawable.placeholder).into(holder.imgProfile);

        holder.imgChatIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.chatWithUser(position);
            }
        });

        holder.rlProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.seeProfileUser(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class AllFriendItemViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imgProfile)
        CircleImageView imgProfile;

        @Bind(R.id.tvUserName)
        TextView tvUserName;

        @Bind(R.id.imgIsCurator)
        ImageView imgIsCurator;

        @Bind(R.id.imgChatIcon)
        ImageView imgChatIcon;

        @Bind(R.id.rlProfile)
        RelativeLayout rlProfile;


        public AllFriendItemViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
        }
    }
}
