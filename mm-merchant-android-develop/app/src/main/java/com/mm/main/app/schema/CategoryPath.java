
package com.mm.main.app.schema;

import java.io.Serializable;

public class CategoryPath implements Serializable{

    private Integer CategoryId;
    private String CategoryName;
    private Integer Level;
    private Integer ParentCategoryId;
    private String CategoryNameInvariant;

    /**
     * 
     * @return
     *     The CategoryId
     */
    public Integer getCategoryId() {
        return CategoryId;
    }

    /**
     * 
     * @param CategoryId
     *     The CategoryId
     */
    public void setCategoryId(Integer CategoryId) {
        this.CategoryId = CategoryId;
    }

    /**
     * 
     * @return
     *     The CategoryName
     */
    public String getCategoryName() {
        return CategoryName;
    }

    /**
     * 
     * @param CategoryName
     *     The CategoryName
     */
    public void setCategoryName(String CategoryName) {
        this.CategoryName = CategoryName;
    }

    /**
     * 
     * @return
     *     The Level
     */
    public Integer getLevel() {
        return Level;
    }

    /**
     * 
     * @param Level
     *     The Level
     */
    public void setLevel(Integer Level) {
        this.Level = Level;
    }

    /**
     * 
     * @return
     *     The ParentCategoryId
     */
    public Integer getParentCategoryId() {
        return ParentCategoryId;
    }

    /**
     * 
     * @param ParentCategoryId
     *     The ParentCategoryId
     */
    public void setParentCategoryId(Integer ParentCategoryId) {
        this.ParentCategoryId = ParentCategoryId;
    }

    public String getCategoryNameInvariant() {
        return CategoryNameInvariant;
    }

    public void setCategoryNameInvariant(String categoryNameInvariant) {
        CategoryNameInvariant = categoryNameInvariant;
    }
}
