package com.mm.main.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by andrew on 10/11/15.
 */
public abstract class MmEditText extends EditText implements Validable{
    public MmEditText(Context context) {
        super(context);
    }

    public MmEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MmEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
