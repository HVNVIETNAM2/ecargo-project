package com.mm.main.app.adapter.merchant.setting;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.schema.MobileCode;

import java.util.List;

/**
 * Created by henrytung on 4/11/15.
 */
public class MobileCountrySelectListAdapter extends BaseAdapter {

    private final Activity context;
    private final List<MobileCode> itemList;
    private Drawable rightIcon;

    public MobileCountrySelectListAdapter(Activity context, List<MobileCode> itemList) {
        this.context = context;
        this.itemList = itemList;
        rightIcon = context.getResources().getDrawable(R.drawable.ico_tick);
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MobileCode currentItem = itemList.get(position);
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.mobile_conutry_selection_list_item, null, true);
        }
        TextView labelTextView = (TextView) convertView.findViewById(R.id.label);
        labelTextView.setText(currentItem.getMobileCodeName());

        TextView valueTextView = (TextView) convertView.findViewById(R.id.value);
        valueTextView.setText(currentItem.getMobileCodeNameInvariant());

        return convertView;
    }

}
