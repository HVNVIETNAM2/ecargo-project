package com.mm.main.app.service;

import com.mm.main.app.schema.ForgotPassword;
import com.mm.main.app.schema.request.SignupRequest;
import com.mm.main.app.schema.request.ActivateRequest;
import com.mm.main.app.schema.request.ForgotPasswordRequest;
import com.mm.main.app.schema.response.LoginResponse;
import com.mm.main.app.schema.request.MobileVerifySmsCheckRequest;
import com.mm.main.app.schema.request.MobileVerifySmsSendRequest;
import com.mm.main.app.schema.response.MobileVerifySmsSendResponse;
import com.mm.main.app.schema.request.ReactivateRequest;
import com.mm.main.app.schema.request.ResendRequest;
import com.mm.main.app.schema.request.ValidateRequest;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by andrew on 30/9/15.
 */
public interface AuthService {

    String AUTH_PATH = "auth";

    @FormUrlEncoded
    @POST(AUTH_PATH + "/login")
    Call<LoginResponse> login(@Field("Username") String userName, @Field("Password") String password);

    @FormUrlEncoded
    @POST(AUTH_PATH + "/login/wechat")
    Call<LoginResponse> loginWeChat(@Field("AuthorizationCode") String authorizationCode);

    @POST(AUTH_PATH + "/password/forgot")
    Call<Boolean> forgotPassword(@Body ForgotPasswordRequest forgotPasswordRequest);

    @POST(AUTH_PATH + "/code/reactivate")
    Call<Boolean> reactivate(@Body ReactivateRequest reactivateRequest);

    @POST(AUTH_PATH + "/code/validate")
    Call<Boolean> validate(@Body ValidateRequest validateRequest);

    @POST(AUTH_PATH + "/code/activate")
    Call<LoginResponse> activate(@Body ActivateRequest activateRequest);

    @POST(AUTH_PATH + "/code/resend")
    Call<Boolean> resend(@Body ResendRequest resendRequest);

    @POST(AUTH_PATH + "/mobileverification/send")
    Call<MobileVerifySmsSendResponse> mobileVerifySmsSend(@Body MobileVerifySmsSendRequest mobileVerifySmsSendRequest);

    @POST(AUTH_PATH + "/mobileverification/check")
    Call<Boolean> mobileVerifySmsCheck(@Body MobileVerifySmsCheckRequest mobileVerifySmsCheckRequest);

    @POST(AUTH_PATH + "/signup")
    Call<LoginResponse> signup(@Body SignupRequest signupRequest);

    @POST(AUTH_PATH + "/passwordreset")
    Call<Boolean> resetPassword(@Body ForgotPassword forgotPassword);


}
