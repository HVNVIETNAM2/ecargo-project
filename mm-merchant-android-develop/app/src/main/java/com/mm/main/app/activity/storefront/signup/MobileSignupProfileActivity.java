package com.mm.main.app.activity.storefront.signup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.alexbbb.uploadservice.MultipartUploadRequest;
import com.mm.main.app.R;
import com.mm.main.app.manager.CheckoutFlowManager;
import com.mm.main.app.utils.PhotoUtil;
import com.mm.main.app.activity.storefront.interest.InterestMappingKeyWordActivity;
import com.mm.main.app.activity.storefront.tnc.TermsAndConditionsActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.event.UpdatePhotoEvent;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.LifeCycleManager;
import com.mm.main.app.schema.response.LoginResponse;
import com.mm.main.app.schema.request.SignupRequest;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.AnimateUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.StringUtil;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Response;
import retrofit.Retrofit;

public class MobileSignupProfileActivity extends GeneralSignupActivity {

    public enum ValidateType {
        VALIDATE_USERNAME_TYPE,
        VALIDATE_DISPLAY_NAME_TYPE,
        VALIDATE_PASSWORD_TYPE,
        VALIDATE_CONFIRM_PASSWORD_TYPE,
    }

    final String TAG = "MobileSignupProfile";
    final int REQUEST_CAMERA = 9872;
    final int REQUEST_SELECT_FILE = REQUEST_CAMERA + 1;
    public static final int THUMBNAIL_SIZE = 1000;

    @Bind(R.id.civProfilePhoto)
    CircleImageView civProfilePhoto;

    @Bind(R.id.etUserName)
    EditText etUserName;

    @Bind(R.id.etDisplayName)
    EditText etDisplayName;

    @Bind(R.id.etPassword)
    EditText etPassword;

    @Bind(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Bind(R.id.agreedCheckbox)
    CheckBox agreedCheckbox;

    @Bind(R.id.tvError)
    TextView tvError;

    @Bind(R.id.btnRegisterProfile)
    Button buttonRegister;

    @Bind(R.id.hiddenFocus)
    EditText edHiddenFocus;

    Bitmap photo;

    SignupRequest signupRequest;

    private final int SPEED = 15;
    private final int TIME_CLOSE_ERROR = 5;
    private final int NUM_FIELD_VALIDATE = 5;
    private int numValid = 0;
    Handler handlerShowError = new Handler();
    Runnable closeErrorCallback;
    boolean isOpened;
    boolean isEditConfirmPasswordFocus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_signup_profile);
        ButterKnife.bind(this);
        signupRequest = (SignupRequest) getIntent().getSerializableExtra(Constant.Extra.EXTRA_SIGNUP_REQUEST);
        ActivityUtil.setTitle(this, StringUtil.getResourceString("LB_CA_REGISTER"));
        setupEditText();
        setListenerToRootView();
        checkEnableRegisterButton();
    }

    private void setupEditText() {

        edHiddenFocus.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edHiddenFocus.getWindowToken(), 0);
                }
            }
        });
        setOnFocusChangeEditText(etUserName, ValidateType.VALIDATE_USERNAME_TYPE);
        setOnFocusChangeEditText(etDisplayName, ValidateType.VALIDATE_DISPLAY_NAME_TYPE);
        setOnFocusChangeEditText(etPassword, ValidateType.VALIDATE_PASSWORD_TYPE);
        setOnFocusChangeEditText(etConfirmPassword, ValidateType.VALIDATE_CONFIRM_PASSWORD_TYPE);

        setOnTextChangeEditText(etUserName);
        setOnTextChangeEditText(etDisplayName);
        setOnTextChangeEditText(etPassword);
        setOnTextChangeEditText(etConfirmPassword);
    }

    private void setOnFocusChangeEditText(final EditText editText, final ValidateType validateType) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    isEditConfirmPasswordFocus = false;
                    validateRegister(editText, validateType, isEditConfirmPasswordFocus);
                } else {
                    isEditConfirmPasswordFocus = true;
                    editText.setTextColor(getResources().getColor(R.color.secondary2));
                    if (!validateRegister(editText, validateType, isEditConfirmPasswordFocus)) {
                        numValid++;
                    }
                }
            }
        });
    }

    private void setOnTextChangeEditText(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showHideEnabledButtonRegister(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setListenerToRootView() {
        final View activityRootView = getWindow().getDecorView().findViewById(android.R.id.content);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > 100 && !isOpened) { // 99% of the time the height diff will be due to a keyboard.
                    isOpened = true;
                } else if (isOpened == true) {
                    if (isEditConfirmPasswordFocus && validateRegister(etConfirmPassword, ValidateType.VALIDATE_CONFIRM_PASSWORD_TYPE, isEditConfirmPasswordFocus)) {
                        showHideEnabledButtonRegister(true);
                    }
                    isOpened = false;
                }
            }
        });
    }


    @OnClick(R.id.civProfilePhoto)
    public void selectImage() {
        final CharSequence[] items = {getString(R.string.LB_TAKE_PHOTO), getString(R.string.LB_PHOTO_LIBRARY)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        startCamera();
                        break;
                    case 1:
                        pickImage();
                        break;
                    default:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }

    public void startCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    public void pickImage() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, getString(R.string.LB_PHOTO_LIBRARY)),
                REQUEST_SELECT_FILE);
    }


    @OnClick(R.id.userAgreementTextView)
    public void startTNCPage() {
        Intent intent = new Intent(this, TermsAndConditionsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnRegisterProfile)
    public void proceedRegister() {
        signupRequest.setDisplayName(etDisplayName.getText().toString());
        signupRequest.setUserName(etUserName.getText().toString());
        signupRequest.setPassword(etPassword.getText().toString());

        requestSignup();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                case REQUEST_SELECT_FILE:
                    Uri selectedImageUri = data.getData();
                    try {
                        photo = PhotoUtil.getCorrectlyOrientedImage(this, selectedImageUri);
                        civProfilePhoto.setImageBitmap(photo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }


    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(UpdatePhotoEvent event) {
        if (loginInApp == LoginInAppType.TYPE_CHECK_OUT_NORMAL) {
            CheckoutFlowManager.getInstance().actionCompleteSignUpRequest();
        }
        else if (loginInApp == LoginInAppType.TYPE_CHECK_OUT_SWIPE_TO_BUY) {
            CheckoutFlowManager.getInstance().actionCompleteLoginAtSwipeCheckout();
        }
        else {
            Bundle bundle = new Bundle();
            bundle.putBoolean(InterestMappingKeyWordActivity.IS_SHOWING_TNC_FLAG, false);
            LifeCycleManager.cleanStart(MobileSignupProfileActivity.this, InterestMappingKeyWordActivity.class, bundle);
        }
    }

    public void requestSignup() {
        signupRequest.setPassword(etPassword.getText().toString());
        MmProgressDialog.show(this);
        APIManager.getInstance().getAuthService()
                .signup(signupRequest)
                .enqueue(new MmCallBack<LoginResponse>(MobileSignupProfileActivity.this) {
                    @Override
                    public void onSuccess(Response<LoginResponse> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            // LoginResponse loginResponse = response.body();
                            LifeCycleManager.login(response, signupRequest.getUserName(), true);
                            requestUploadImage();
                            MmProgressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        MmProgressDialog.dismiss();
                        super.onFailure(t);
                    }
                });
    }

    public void requestUploadImage() {
        if (photo != null) {
            PhotoUtil.uploadPhoto(photo, PhotoUtil.PhotoType.PROFILE);
        } else {
            onEvent(null);
        }
    }

    private boolean validateRegister(EditText editText, ValidateType validateType, boolean isFocus) {
        String textToValidate = editText.getText().toString();
        String errorString = "";
        switch (validateType) {
            case VALIDATE_USERNAME_TYPE:
                if (ValidationUtil.isEmpty(textToValidate)) {
                    errorString = getStringFromResource(R.string.MSG_ERR_CA_USERNAME_NIL);
                    break;
                }
                if (!ValidationUtil.isValidUsername(textToValidate)) {
                    errorString = getStringFromResource(R.string.MSG_ERR_CA_USERNAME_PATTERN);
                    break;
                }
                break;

            case VALIDATE_DISPLAY_NAME_TYPE:
                if (ValidationUtil.isEmpty(textToValidate)) {
                    errorString = getStringFromResource(R.string.MSG_ERR_CA_USERNAME_NIL);
                    break;
                }
                break;
            case VALIDATE_PASSWORD_TYPE:
                if (ValidationUtil.isEmpty(textToValidate)) {
                    errorString = getStringFromResource(R.string.MSG_ERR_CA_PW_NIL);
                    break;
                }
                if (!ValidationUtil.isValidPassword(textToValidate)) {
                    errorString = getStringFromResource(R.string.MSG_ERR_CA_PW_PATTERN);
                    break;
                }
                break;
            case VALIDATE_CONFIRM_PASSWORD_TYPE:
                if (ValidationUtil.isEmpty(textToValidate)) {
                    errorString = getStringFromResource(R.string.MSG_ERR_CA_PW_NIL);
                    break;
                }
                if (!textToValidate.equals(etPassword.getText().toString())) {
                    errorString = getStringFromResource(R.string.MSG_ERR_CA_CFM_PW_NOT_MATCH);
                    break;
                }
                break;
        }

        if (isFocus) {
            return errorString.equals("");
        }

        if (!errorString.equals("")) {
            showError(errorString, editText);
            numValid--;
            checkEnableRegisterButton();
            return false;
        } else {
            checkEnableRegisterButton();
        }
        return true;
    }

    @OnClick(R.id.agreedCheckbox)
    public void onCheckAgreedCheckbox() {
        if (agreedCheckbox.isChecked()) {
            if (etConfirmPassword.isFocused()) {
                etConfirmPassword.clearFocus();
            }
            numValid++;
        } else {
            numValid--;
        }
        checkEnableRegisterButton();
    }

    private void checkEnableRegisterButton() {
        showHideEnabledButtonRegister(numValid == NUM_FIELD_VALIDATE);
    }

    private void showError(String errorStr, EditText editText) {

        tvError.setText(errorStr);
        if (tvError.getVisibility() != View.VISIBLE) {
            AnimateUtil.expand(tvError, SPEED, 1, UiUtil.getPixelFromDp(35), null);
            editText.setTextColor(getResources().getColor(R.color.primary1));

            if (closeErrorCallback != null) {
                handlerShowError.removeCallbacks(closeErrorCallback);
            }

            closeErrorCallback = new Runnable() {
                @Override
                public void run() {
                    closeError();
                }
            };
            handlerShowError.postDelayed(closeErrorCallback, TIME_CLOSE_ERROR * 1000);
        }
    }

    private void closeError() {
        if (tvError.getVisibility() == View.VISIBLE) {
            AnimateUtil.collapse(tvError, SPEED, null);
            tvError.setVisibility(View.INVISIBLE);
        }
    }

    private String getStringFromResource(int id) {
        return getResources().getString(id);
    }

    private void showHideEnabledButtonRegister(boolean status) {
        buttonRegister.setEnabled(status ? true : false);
    }

}
