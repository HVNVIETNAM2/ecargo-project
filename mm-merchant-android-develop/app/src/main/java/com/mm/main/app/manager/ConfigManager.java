package com.mm.main.app.manager;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.schema.ConfigModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by nhutphan on 2/29/2016.
 */
public class ConfigManager {
    private static final String TAG = ConfigManager.class.toString();

    private static ConfigManager singleton;
    private Context context = null;

    public static ConfigManager getInstance() {
        if (singleton == null) {
            singleton = new ConfigManager();
        }
        return singleton;
    }

    private ConfigManager() {
        loadConfig();
    }

    public void loadConfig() {
        context = MyApplication.getContext();
        Gson gson = new Gson();
        InputStream inputStream;

        try {
            inputStream = context.getAssets().open(Constant.CONFIG_FILE_URL);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            ConfigModel configModel = gson.fromJson(reader, ConfigModel.class);
            String json = gson.toJson(configModel);
            MmGlobal.setConfig(json);
            inputStream.close();
        } catch (IOException e) {
            Log.e(TAG, "Exception parsing JSON", e);
        }
    }

    public ConfigModel getConfig() {
        Gson gson = new Gson();
        String configJson = MmGlobal.getConfig();
        if (configJson.isEmpty()) return null;

        ConfigModel configModel = gson.fromJson(configJson, ConfigModel.class);
        return configModel;
    }
}
