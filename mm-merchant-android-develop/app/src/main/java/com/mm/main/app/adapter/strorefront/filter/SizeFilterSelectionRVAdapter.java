package com.mm.main.app.adapter.strorefront.filter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Size;
import com.mm.main.app.utils.UiUtil;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nguyennguyent1 on 1/22/2016.
 */
public class SizeFilterSelectionRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private final Activity context;
    private ArrayList<FilterListItem> originalData;
    private ArrayList<FilterListItem> filteredData;
    private Integer selectedItemId;
    private ItemFilter mFilter = new ItemFilter();
    private int borderWidthSelected;
    private int borderNotSelected;

    public SizeFilterSelectionRVAdapter(Activity context, ArrayList<FilterListItem> itemList, Integer selectedItemId) {
        this.context = context;
        this.originalData = itemList;
        this.filteredData = itemList;
        this.selectedItemId = selectedItemId;
        this.borderNotSelected = UiUtil.getPixelFromDp(1);
        this.borderWidthSelected = UiUtil.getPixelFromDp(2);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        FilterListItem.ItemType itemType = FilterListItem.ItemType.values()[viewType];
        RecyclerView.ViewHolder viewHolder = null;

        if (itemType == FilterListItem.ItemType.TYPE_FILTER_TITLE) {
            viewHolder = new SizeFilterTitleViewHolder(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.size_filter_header_item, viewGroup, false));
        } else {
            viewHolder = new SizeFilterViewHolder(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.size_selection_list_item, viewGroup, false));
        }

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        final FilterListItem currentItem = filteredData.get(position);

        switch (currentItem.getType()) {
            case TYPE_FILTER_LIST:
                final SizeFilterViewHolder sizeFilterViewHolder = (SizeFilterViewHolder) viewHolder;

                sizeFilterViewHolder.textView.setText(((Size)currentItem.getT()).getSizeNameInvariant());
                setBorderColor(currentItem.isSelected(), sizeFilterViewHolder.imageViewSimple);
                sizeFilterViewHolder.imageViewSimple.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentItem.setSelected(!currentItem.isSelected());
                        setBorderColor(currentItem.isSelected(), sizeFilterViewHolder.imageViewSimple);
                    }
                });
                break;

            case TYPE_FILTER_TITLE:
                SizeFilterTitleViewHolder viewHolderName = (SizeFilterTitleViewHolder) viewHolder;
                viewHolderName.textView.setText(currentItem.getT().toString());
                if(position == 0){
                    viewHolderName.headerLine.setVisibility(View.GONE);
                }
                break;
        }


    }

    private void setBorderColor(boolean isSelected, CircleImageView circleImageView) {
        if (isSelected) {
            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.black));
            circleImageView.setBorderWidth(borderWidthSelected);
        } else {
            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.secondary1));
            circleImageView.setBorderWidth(borderNotSelected);
        }
    }

    public class SizeFilterViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imageViewSimple;
        TextView textView;

        public SizeFilterViewHolder(View itemView) {
            super(itemView);
            imageViewSimple = (CircleImageView) itemView.findViewById(R.id.imageSimple);
            textView = (TextView) itemView.findViewById(R.id.label);
        }
    }

    public class SizeFilterTitleViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        View headerLine;
        public SizeFilterTitleViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.sizeFilterHeaderTextView);
            headerLine = itemView.findViewById(R.id.headerLine);
        }
    }


    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return filteredData.get(position).getType().ordinal();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString;
            filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<FilterListItem> list = originalData;

            int count = list.size();
            final ArrayList<FilterListItem> nlist = new ArrayList<>(count);

            String filterableString;

            if (!TextUtils.isEmpty(constraint)) {
                for (int i = 0; i < count; i++) {
                    if (list.get(i).getType() == FilterListItem.ItemType.TYPE_FILTER_LIST) {
                        filterableString = "" + ((Size)list.get(i).getT()).getSizeNameInvariant();
                        if (filterableString.toLowerCase().contains(filterString)) {
                            nlist.add(list.get(i));
                        }
                    } else {
                        nlist.add(list.get(i));
                    }
                }

            } else {
                nlist.addAll(list);
            }


            // clear non item group header
            for (int i = 0; i < nlist.size() - 1; i++) {
                if(nlist.get(i).getType().equals(FilterListItem.ItemType.TYPE_FILTER_TITLE)
                        && nlist.get(i + 1).getType().equals(FilterListItem.ItemType.TYPE_FILTER_TITLE)) {
                    nlist.remove(i);
                    i--;
                    continue;
                }
            }

            if(nlist.get(nlist.size() - 1).getType() == FilterListItem.ItemType.TYPE_FILTER_TITLE){
                nlist.remove(nlist.size() - 1);
            }

            results.values = nlist;
            results.count = nlist.size();
            return results;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<FilterListItem>) results.values;
            notifyDataSetChanged();
        }

    }

    public ArrayList<FilterListItem> getOriginalData() {
        return originalData;
    }

    public Integer getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(Integer selectedItemId) {
        if (selectedItemId >= 0) {
            filteredData.get(selectedItemId).setSelected(!filteredData.get(selectedItemId).isSelected());
        }
    }

}
