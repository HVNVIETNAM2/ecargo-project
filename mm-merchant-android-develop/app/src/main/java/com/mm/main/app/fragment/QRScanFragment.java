package com.mm.main.app.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.google.zxing.Result;
import com.mm.main.app.R;
import com.mm.main.app.log.Logger;
import com.mm.main.app.uicomponent.CustomScannerView;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.utils.UiUtil;

import me.dm7.barcodescanner.core.DisplayUtils;
import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;

/**
 * Created by nhutphan on 3/8/2016.
 */
public class QRScanFragment extends Fragment implements CustomScannerView.ResultHandler, MmStatusAlertUtil.OnStatusAlertDismissListener, Animation.AnimationListener {
    private CustomScannerView mScannerView;
    private View lineView;
    private Animation animTopToBottom;
    private static final int MIN_FRAME_WIDTH = 240;
    private static final int MIN_FRAME_HEIGHT = 240;

    private static final float LANDSCAPE_WIDTH_RATIO = 5f/8;
    private static final float LANDSCAPE_HEIGHT_RATIO = 5f/8;
    private static final int LANDSCAPE_MAX_FRAME_WIDTH = (int) (1920 * LANDSCAPE_WIDTH_RATIO); // = 5/8 * 1920
    private static final int LANDSCAPE_MAX_FRAME_HEIGHT = (int) (1080 * LANDSCAPE_HEIGHT_RATIO); // = 5/8 * 1080

    private static final float PORTRAIT_WIDTH_RATIO = 7f/8;
    private static final float PORTRAIT_HEIGHT_RATIO = 3f/8;
    private static final int PORTRAIT_MAX_FRAME_WIDTH = (int) (1080 * PORTRAIT_WIDTH_RATIO); // = 7/8 * 1080
    private static final int PORTRAIT_MAX_FRAME_HEIGHT = (int) (1920 * PORTRAIT_HEIGHT_RATIO); // = 3/8 * 1920

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mScannerView = new CustomScannerView(getActivity()) {
            @Override
            protected IViewFinder createViewFinderView(Context context) {
                return new CustomViewFinderView(context);
            }
        };

        mScannerView.addView(createLinesView());
        return mScannerView;
    }

    private View createLinesView() {
        Point viewResolution = new Point(UiUtil.getDisplayWidth(), UiUtil.getDisplayHeight());
        int width;
        int height;
        int orientation = DisplayUtils.getScreenOrientation(getContext());

        if(orientation != Configuration.ORIENTATION_PORTRAIT) {
            width = findDesiredDimensionInRange(LANDSCAPE_WIDTH_RATIO, viewResolution.x, MIN_FRAME_WIDTH, LANDSCAPE_MAX_FRAME_WIDTH);
            height = findDesiredDimensionInRange(LANDSCAPE_HEIGHT_RATIO, viewResolution.y, MIN_FRAME_HEIGHT, LANDSCAPE_MAX_FRAME_HEIGHT);
        } else {
            width = findDesiredDimensionInRange(PORTRAIT_WIDTH_RATIO, viewResolution.x, MIN_FRAME_WIDTH, PORTRAIT_MAX_FRAME_WIDTH);
            height = findDesiredDimensionInRange(PORTRAIT_HEIGHT_RATIO, viewResolution.y, MIN_FRAME_HEIGHT, PORTRAIT_MAX_FRAME_HEIGHT);
        }

        int leftOffset = (viewResolution.x - width) / 2;
        int topOffset = (viewResolution.y - height) / 2;

        LinearLayout rootView = new LinearLayout(getActivity());
        LinearLayout.LayoutParams rootviewLP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        rootView.setLayoutParams(rootviewLP);

        LinearLayout view = new LinearLayout(getActivity());
        LinearLayout.LayoutParams viewLP = new LinearLayout.LayoutParams(width, height );
        viewLP.setMargins(leftOffset, topOffset - UiUtil.getPixelFromDp(12.5f), 0, 0);
        view.setLayoutParams(viewLP);

        animTopToBottom = AnimationUtils.loadAnimation(getActivity(), R.anim.scan_aim_top_to_bottom);
        animTopToBottom.setAnimationListener(this);

        lineView = new View(getActivity());
        LinearLayout.LayoutParams lineLP = new LinearLayout.LayoutParams(width, height / 3);
        lineLP.setMargins(0, height, 0, 0);
        lineView.setLayoutParams(lineLP);
        lineView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.primary1));
        lineView.setAlpha(0.5f);
        lineView.startAnimation(animTopToBottom);

        view.addView(lineView);
        rootView.addView(view);

        return rootView;
    }

    private static int findDesiredDimensionInRange(float ratio, int resolution, int hardMin, int hardMax) {
        int dim = (int) (ratio * resolution);
        if (dim < hardMin) {
            return hardMin;
        }
        if (dim > hardMax) {
            return hardMax;
        }
        return dim;
    }

    @Override
    public void onResume() {
        super.onResume();
        startScanner();
    }

    @Override
    public void handleResult(Result rawResult) {
//        String warningMsg = getActivity().getString(R.string.MSG_ERR_IM_QR_SCAN);
//        MmStatusAlertUtil.showLongMsg(getActivity(), MmStatusAlertUtil.StatusAlertType.StatusAlertType_ERROR, rawResult.toString(), this);
        ErrorUtil.showErrorDialog(getActivity(), rawResult.toString());
    }

    @Override
    public void onPause() {
        super.onPause();
        stopScanner();
    }

    @Override
    public void onDismiss() {
        stopScanner();
        startScanner();
    }

    private void startScanner() {
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    private void stopScanner() {
        mScannerView.stopCamera();
        mScannerView.setResultHandler(null);
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.scan_aim_top_to_bottom);
        anim.setAnimationListener(this);
        lineView.startAnimation(animTopToBottom);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    private class CustomViewFinderView extends ViewFinderView {
        private final String TAG = CustomViewFinderView.class.getSimpleName();
        private final int SCAN_TEXT_SIZE_SP = 14;
        private Paint paintScanNote = new Paint();
        private Context context;

        public CustomViewFinderView(Context context) {
            super(context);
            this.context = context;
            init();
        }

        public CustomViewFinderView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        private void init() {
            paintScanNote.setColor(Color.WHITE);
            paintScanNote.setAntiAlias(true);
            float textPixelSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                    SCAN_TEXT_SIZE_SP, getResources().getDisplayMetrics());
            paintScanNote.setTextSize(textPixelSize);
            setBorderColor(ContextCompat.getColor(context, R.color.primary1));
        }

        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            drawScanNote(canvas);
        }

        @Override
        public void drawLaser(Canvas canvas) {
            Logger.i(TAG, "ignored the default laser line");
        }

        private void drawScanNote(Canvas canvas) {
            Logger.i(TAG, "Draw scan note.");
            Rect mFramingRect = this.getFramingRect();
            String scanNote = context.getString(R.string.LB_CA_IM_SCAN_NOTE);
            float x = 0;
            float y = 0;
            Rect bounds = new Rect();
            paintScanNote.getTextBounds(scanNote, 0, scanNote.length(), bounds);
            if (mFramingRect != null) {
                x = mFramingRect.left + (mFramingRect.width() - bounds.width()) / 2;
                y = mFramingRect.top - paintScanNote.getTextSize() - 20;
            }

            canvas.drawText(scanNote, x, y, paintScanNote);
        }
    }
}