package com.mm.main.app.adapter.strorefront.filter;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by henrytung on 4/11/15.
 */
public class ColorFilterSelectionListAdapter extends BaseAdapter implements Filterable{

    private final Activity context;
    private List<FilterListItem<Color>> originalData;
    private List<FilterListItem<Color>> filteredData;
    private Integer selectedItemId;
    private Drawable rightIcon;

    private ItemFilter mFilter = new ItemFilter();

    public ColorFilterSelectionListAdapter(Activity context, List<FilterListItem<Color>> itemList, Integer selectedItemId) {
        this.context = context;
        this.originalData = itemList;
        this.filteredData = itemList;
        this.selectedItemId = selectedItemId;
        rightIcon = context.getResources().getDrawable(R.drawable.icon_tick);
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FilterListItem<Color> currentItem = filteredData.get(position);
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.color_selection_list_item, null, true);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.label);
        textView.setText(currentItem.getT().getColorName());

        ImageView imageViewSimple = (ImageView) convertView.findViewById(R.id.imageSimple);
        Drawable drawable = context.getResources().getDrawable(R.drawable.color_circle);
        String colorHex = currentItem.getT().getHexCode();
        if (!TextUtils.isEmpty(colorHex)) {
            drawable.setColorFilter(android.graphics.Color.parseColor(colorHex), PorterDuff.Mode.SRC_ATOP);
            imageViewSimple.setImageDrawable(drawable);
        } else {
            imageViewSimple.setImageResource(R.drawable.color_circle);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);

        if (currentItem.isSelected()) {
            imageView.setImageDrawable(rightIcon);
        } else {
            imageView.setImageDrawable(null);
        }

        return convertView;
    }

    public Integer getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(Integer selectedItemId) {
        filteredData.get(selectedItemId).setSelected(!filteredData.get(selectedItemId).isSelected());
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString;
            filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<FilterListItem<Color>> list = originalData;

            int count = list.size();
            final ArrayList<FilterListItem<Color>> nlist = new ArrayList<>(count);

            String filterableString ;

            if (!TextUtils.isEmpty(constraint)) {
                for (int i = 0; i < count; i++) {
                    filterableString = "" + list.get(i).getT().getColorName();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        FilterListItem<Color> mYourCustomData = list.get(i);
                        nlist.add(mYourCustomData);
                    }
                }
            } else {
                for (int i = 0; i < count; i++) {
                    FilterListItem<Color> mYourCustomData = list.get(i);
                    nlist.add(mYourCustomData);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<FilterListItem<Color>>) results.values;
            notifyDataSetChanged();
        }

    }

    public List<FilterListItem<Color>> getOriginalData() {
        return originalData;
    }

    public void setOriginalData(List<FilterListItem<Color>> originalData) {
        this.originalData = originalData;
    }
}
