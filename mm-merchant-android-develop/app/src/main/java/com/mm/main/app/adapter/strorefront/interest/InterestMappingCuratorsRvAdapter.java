package com.mm.main.app.adapter.strorefront.interest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.CuratorListItem;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.CircleProcessUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by haivu on 2/15/2016.
 */
public class InterestMappingCuratorsRvAdapter extends RecyclerView.Adapter<InterestMappingCuratorsRvAdapter.CuratorViewHolder> {

    public interface OnItemListener {
        void onClick(boolean isSelected, int position);
    }

    public static final int MAX_NUMBER_ITEM = 9999;
    private Context context;
    private ArrayList<CuratorListItem> data;
    private OnItemListener listener;
    int centerItem;
    RecyclerView recyclerView;
    boolean isInit = true;
    int oldCenter;

    public InterestMappingCuratorsRvAdapter(Context context, ArrayList<CuratorListItem> data, RecyclerView recyclerView, OnItemListener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
        if (data.size() > 0) {
            centerItem = UiUtil.findCenterItemPosition(recyclerView, context, data.size(), MAX_NUMBER_ITEM);
        }
        this.recyclerView = recyclerView;
    }

    @Override
    public CuratorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.curators_item_view, parent, false);
        return new CuratorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CuratorViewHolder holder, final int position) {
        if (data.size() < 1) {
            return;
        }

        if (position == centerItem && isInit) {
            recyclerView.smoothScrollBy(0, 100);
            isInit = false;
        }

        final int realPos = position % data.size();
        final CuratorListItem item = data.get(realPos);

        String url = PathUtil.getUserImageUrl(item.getCurator().getProfileImage());
        Picasso.with(MyApplication.getContext()).load(url).into(holder.profileImageView);

        holder.profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getIsCenter()) {
                    item.setIsSelected(!item.getIsSelected());
                    upDateViewSelected(item.getIsSelected(), holder);

                    listener.onClick(item.getIsSelected(), realPos);
                }
            }
        });

        upDateViewSelectedOnBind(item.getIsSelected(), holder);
        upDateViewCenter(item, holder);
    }

    private void upDateViewSelectedOnBind(boolean isSelected, CuratorViewHolder holder) {

        Log.d("upDateViewSelected", "upDateViewSelectedOnBind" + isSelected);
        holder.selectedCheckbox.setScaleX(1f);
        holder.selectedCheckbox.setScaleY(1f);
        if (isSelected) {
            holder.selectedCheckbox.setVisibility(View.VISIBLE);
            holder.circleProcess.setPercentNoAnim(100);
        } else {
            holder.selectedCheckbox.setVisibility(View.INVISIBLE);
            holder.circleProcess.setPercentNoAnim(0);
        }
    }

    private void upDateViewSelected(boolean isSelected, CuratorViewHolder holder) {
        // Log.d("upDateViewSelected", "" + isSelected);
        final View cbx = holder.selectedCheckbox;
        if (isSelected) {
            cbx.setScaleX(0f);
            cbx.setScaleY(0f);
            cbx.setVisibility(View.VISIBLE);
            holder.circleProcess.setPercentAnim(100, 45);
            cbx.animate().scaleXBy(1.4f)
                    .scaleYBy(1.4f).withEndAction(new Runnable() {
                @Override
                public void run() {
                    cbx.animate().scaleXBy(-0.4f)
                            .scaleYBy(-0.4f)
                            .setDuration(180).start();
                }
            })
                    .setDuration(600).start();


        } else {

            holder.circleProcess.setPercentAnim(0, 45);

            holder.selectedCheckbox.animate().scaleX(1f).scaleXBy(-1f)
                    .scaleY(1f).scaleYBy(-1f)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            cbx.setVisibility(View.INVISIBLE);
                        }
                    })
                    .setDuration(600).start();
        }

    }

    public void upDateViewCenter(CuratorListItem item, CuratorViewHolder holder) {
        User curator = item.getCurator();
        holder.curatorNameTextView.setText(curator.getLastName() + curator.getFirstName());
        holder.curatorDisplayNameTextView.setText(curator.getDisplayName());

//        if (curator.getCount() > Constant.MIN_FOLLOWER_COUNT) {
//            holder.curatorFollowTextView.setVisibility(View.VISIBLE);
//            holder.curatorFollowTextView.setText(context.getString(R.string.LB_CA_CURATORS_FOLLOWER_NO) + " " +
//                    FormatUtil.formatNumber(curator.getCount() * Constant.DEMO_FACTOR_FOR_FOLLOWER));
//        } else {
//            holder.curatorFollowTextView.setVisibility(View.GONE);
//        }
    }

    @Override
    public int getItemCount() {
        return MAX_NUMBER_ITEM;
    }

    public void updateCenterView(int position, View view, boolean isCenter) {
        if (data.size() < 1) {
            return;
        }

        CuratorListItem itemData = data.get(position % data.size());
        itemData.setIsCenter(isCenter);

        if (isCenter) {
            view.findViewById(R.id.curatorInfoLL).setVisibility(View.VISIBLE);
            view.findViewById(R.id.profileImageView).setAlpha(1.0f);
        } else {
            view.findViewById(R.id.curatorInfoLL).setVisibility(View.INVISIBLE);
            view.findViewById(R.id.profileImageView).setAlpha(0.5f);
        }

        if (itemData.getIsSelected()) {
            view.findViewById(R.id.selectedCheckbox).setScaleX(1f);
            view.findViewById(R.id.selectedCheckbox).setScaleY(1f);
            view.findViewById(R.id.selectedCheckbox).setVisibility(View.VISIBLE);
            CircleProcessUtil circleProcessUtil = (CircleProcessUtil) view.findViewById(R.id.circleProcess);
            circleProcessUtil.setPercentNoAnim(100);
        } else {
            view.findViewById(R.id.selectedCheckbox).setVisibility(View.INVISIBLE);
            CircleProcessUtil circleProcessUtil = (CircleProcessUtil) view.findViewById(R.id.circleProcess);
            circleProcessUtil.setPercentNoAnim(0);
        }
    }

    public void updateViewTheSame(boolean isSelected, View view) {
        if (isSelected) {
            CheckBox cbx = (CheckBox) view.findViewById(R.id.selectedCheckbox);
            cbx.setVisibility(View.VISIBLE);
            cbx.setScaleX(1f);
            cbx.setScaleY(1f);

            CircleProcessUtil circleProcessUtil = (CircleProcessUtil) view.findViewById(R.id.circleProcess);
            circleProcessUtil.setPercentNoAnim(100);
        } else {
            view.findViewById(R.id.selectedCheckbox).setVisibility(View.INVISIBLE);
            CircleProcessUtil circleProcessUtil = (CircleProcessUtil) view.findViewById(R.id.circleProcess);
            circleProcessUtil.setPercentNoAnim(0);
        }
    }

    public class CuratorViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.profileImageView)
        public ImageView profileImageView;

        @Bind(R.id.curatorNameTextView)
        public TextView curatorNameTextView;

        @Bind(R.id.curatorDisplayNameTextView)
        public TextView curatorDisplayNameTextView;

        @Bind(R.id.curatorFollowTextView)
        public TextView curatorFollowTextView;

        @Bind(R.id.selectedCheckbox)
        public CheckBox selectedCheckbox;

        @Bind(R.id.curatorInfoLL)
        LinearLayout curatorInfoLL;

        @Bind(R.id.circleProcess)
        CircleProcessUtil circleProcess;

        public View view;

        public CuratorViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            circleProcess.setWidth(2);
            circleProcess.setColor(R.color.primary1);
            circleProcess.setWidthPercent(1);
        }
    }
}
