package com.mm.main.app.adapter.strorefront.discover;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.DiscoverBrandListItem;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Adapter for Recycle View, View Holder Pattern with getView function!
 */
public class DiscoverBrandRVAdapter extends RecyclerView.Adapter<DiscoverBrandRVAdapter.BrandImageViewHolder> {

    public interface BrandListOnClickListener {
        void onClick(int position, Object data);
    }

    List<DiscoverBrandListItem> itemList;
    BrandListOnClickListener brandListOnClickListener;

    public DiscoverBrandRVAdapter(List<DiscoverBrandListItem> itemList, BrandListOnClickListener productListOnClickListener) {
        this.itemList = itemList;

        this.brandListOnClickListener = productListOnClickListener;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public BrandImageViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_discover_brands_item, viewGroup, false);
        setSize(v);

        BrandImageViewHolder.ProductViewHolderOnClickListener productOnClickListener = new BrandImageViewHolder.ProductViewHolderOnClickListener() {
            @Override
            public void onClick(int position) {
                proceedProductDetail(position);
            }
        };
        return new BrandImageViewHolder(v, itemList, productOnClickListener);
    }

    public void proceedProductDetail(int position) {
        brandListOnClickListener.onClick(position, itemList.get(position));
    }

    @Override
    public void onBindViewHolder(final BrandImageViewHolder productViewHolder, final int i) {
        final DiscoverBrandListItem currentDiscoverBrandListItem = itemList.get(i);
        int width = (UiUtil.getDisplayWidth() - UiUtil.getPixelFromDp(15)) / 2;

        String key = PathUtil.getBrandImageUrl(currentDiscoverBrandListItem.getKey());
        if (currentDiscoverBrandListItem.getBrandUnionMerchant().getEntity().toLowerCase().equals("merchant")) {
            key = PathUtil.getMerchantImageUrl(currentDiscoverBrandListItem.getKey());
        }

        Picasso.with(MyApplication.getContext()).load(key).
                resize(width, width)
                .centerCrop().into(productViewHolder.ivBrand);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    void setSize(View v) {
    }

    public static class BrandImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private static final String TAG = "Inside Product View Holder";
        ImageView ivBrand;
        List<DiscoverBrandListItem> itemList;
        View mainLayout;
        ProductViewHolderOnClickListener productViewHolderOnClickListener;

        public interface ProductViewHolderOnClickListener {
            void onClick(int position);
        }

        BrandImageViewHolder(View itemView, List<DiscoverBrandListItem> itemList, ProductViewHolderOnClickListener productViewHolderOnClickListener) {
            super(itemView);
            this.itemList = itemList;
            ivBrand = (ImageView) itemView.findViewById(R.id.ivBrand);
            mainLayout = itemView;

            this.productViewHolderOnClickListener = productViewHolderOnClickListener;

            itemView.setOnClickListener(this);
        }


        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            productViewHolderOnClickListener.onClick(getPosition());
        }
    }

    public List<DiscoverBrandListItem> getProductList() {
        return itemList;
    }

    public void setItemList(List<DiscoverBrandListItem> itemList) {
        this.itemList = itemList;
    }
}
