package com.mm.main.app.schema;

import java.io.Serializable;

/**
 * Created by henrytung on 26/11/2015.
 */
public class ColorImage implements Serializable{

    String ColorKey;
    String ImageKey;
    String StyleImageId;
    int Position;

    public ColorImage() {
    }

    public String getColorKey() {
        return ColorKey;
    }

    public void setColorKey(String colorKey) {
        ColorKey = colorKey;
    }

    public String getImageKey() {
        return ImageKey;
    }

    public void setImageKey(String imageKey) {
        ImageKey = imageKey;
    }

    public String getStyleImageId() {
        return StyleImageId;
    }

    public void setStyleImageId(String styleImageId) {
        StyleImageId = styleImageId;
    }

    public int getPosition() {
        return Position;
    }

    public void setPosition(int position) {
        Position = position;
    }
}
