package com.mm.main.app.activity.storefront.profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.follower.FollowerActivity;
import com.mm.main.app.activity.storefront.friend.FriendListActivity;
import com.mm.main.app.activity.storefront.search.ProductListSearchActivity;
import com.mm.main.app.activity.storefront.setting.UserProfileSettingActivity;
import com.mm.main.app.activity.storefront.temp.DemoActivity;

import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mm.main.app.activity.storefront.cart.ShoppingCartActivity;
import com.mm.main.app.activity.storefront.wishlist.WishListActivity;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.event.UpdatePhotoEvent;
import com.mm.main.app.fragment.StoreFrontUserProfileDataListFragment;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.manager.ConfigManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Response;
import retrofit.Retrofit;

import com.mm.main.app.manager.ProfileLifeCycleManager;
import com.mm.main.app.schema.Follow;
import com.mm.main.app.schema.FriendRequest;
import com.mm.main.app.schema.ImageData;
import com.mm.main.app.schema.User;
import com.mm.main.app.schema.response.DeleteCoverResponce;
import com.mm.main.app.schema.response.DeleteImageResponce;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.PhotoUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

public class StoreFrontUserProfileActivity extends AppCompatActivity {

    public enum ProfileType {
        USER_PRIVATE_PROFILE,
        USER_PUBLIC_PROFILE,
        MERCHANT_PROFILE
    }

    @Bind(R.id.tvUserId)
    TextView tvUserId;

    @Bind(R.id.tvDisplayName)
    TextView tvDisplayName;

    @Bind(R.id.tvUserKey)
    TextView tvUserKey;

    @Bind(R.id.tvToken)
    TextView tvToken;

    @Bind(R.id.tvFollowingUser)
    TextView tvFollowingUser;

    @Bind(R.id.tvFollower)
    TextView tvFollower;

    @Bind(R.id.tvFollowingMerchant)
    TextView tvFollowingMerchant;

    @Bind(R.id.tvFollowingCurator)
    TextView tvFollowingCurator;

    @Bind(R.id.tvProductWishlist)
    TextView tvProductWishlist;

    @Bind(R.id.tvProductWishlistLabel)
    TextView tvProductWishlistLabel;

    @Bind(R.id.tvToolbarTitle)
    TextView tvToolbarTitle;

    @Bind(R.id.ivProfilePic)
    CircleImageView ivProfilePic;

    @Bind(R.id.ivCoverPic)
    ImageView ivCoverPic;

    @Bind(R.id.ivCuratorDiamond)
    ImageView ivCuratorDiamond;

    @Bind(R.id.btnProfileSettings)
    ImageView btnProfileSettings;

    @Bind(R.id.btnProfileSearch)
    ImageView btnProfileSearch;

    @Bind(R.id.btnAvatarEdit)
    Button btnAvatarEdit;

    @Bind(R.id.btnCoverEdit)
    Button btnCoverEdit;

    @Bind(R.id.btnFollow)
    Button btnFollow;

    @Bind(R.id.btnFriend)
    Button btnFriend;

    private User mProfileUser;
    private long openDemoCount = 1;
    private final long MAX_DEMO_COUNTER = 3;
    private boolean mIsToolbarCollapsed = false;
    private ProfileType profileType;
    private boolean beFriend = false;
    private boolean beFollowed = false;
    private boolean isProfileLoaded = false;
    private String publicUserKey = "";
    private Uri currentPictureUri = null;

    private static final int CAMERA_REQUEST_TO_PROFILE = 9882;
    private static final int SELECT_FILE_REQUEST_TO_PROFILE = CAMERA_REQUEST_TO_PROFILE + 1;
    private static final int SELECT_FILE_REQUEST_TO_COVER = SELECT_FILE_REQUEST_TO_PROFILE + 1;
    private static final int CAMERA_REQUEST_TO_COVER = SELECT_FILE_REQUEST_TO_COVER + 1;
    private static final int PROFILE_LIFE_CYCLE_REQUEST = 9900;

    public static final String PROFILE_TYPE_KEY = "PROFILE_TYPE_KEY";
    public static final String BE_FRIEND_KEY = "BE_FRIEND_KEY";
    public static final String BE_FOLLOWED_KEY = "BE_FOLLOWED_KEY";
    public static final String PUBLIC_USER_KEY = "PUBLIC_USER_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_front_user_profile);
        ButterKnife.bind(this);
        setupToolbar();

        getProfileExtras(getIntent());
    }

    @Override
    public void onBackPressed() {
        if(isProfileLoaded) {
            StoreFrontUserProfileDataListActivity.ProfileDataListType type = ProfileLifeCycleManager.getInstance().backFromProfile();
            if(type!=null) {
                if(type == StoreFrontUserProfileDataListActivity.ProfileDataListType.FRIEND_LIST) {
                    Intent showListFriend = new Intent(this, FriendListActivity.class);
                    startActivityForResult(showListFriend, PROFILE_LIFE_CYCLE_REQUEST);
                } else if (type == StoreFrontUserProfileDataListActivity.ProfileDataListType.FOLLOWER_LIST) {
                    String userKey = ProfileLifeCycleManager.getInstance().backFromDataList();
                    Intent intent = new Intent(this, FollowerActivity.class);
                    intent.putExtra(FollowerActivity.USER_KEY, userKey);
                    intent.putExtra(FollowerActivity.FOLLOWER_TYPE_KEY, FollowerActivity.FOLLOWER_CONSUMER);
                    startActivityForResult(intent, PROFILE_LIFE_CYCLE_REQUEST);
                } else {
                    String userKey = ProfileLifeCycleManager.getInstance().backFromDataList();
                    openDataListActivity(userKey, type);
                }
            } else {
                super.onBackPressed();
            }
        }
    }

    private void updateLayout(ProfileType profileType) {
        switch (profileType) {
            case MERCHANT_PROFILE:
                break;
            case USER_PUBLIC_PROFILE:
                tvProductWishlistLabel.setText(R.string.LB_CA_WISHLIST_PROD);
                btnCoverEdit.setVisibility(View.GONE);
                btnAvatarEdit.setVisibility(View.GONE);
                btnProfileSettings.setVisibility(View.GONE);

                btnFriend.setVisibility(View.VISIBLE);
                btnFollow.setVisibility(View.VISIBLE);
                btnProfileSearch.setVisibility(View.VISIBLE);

                updateFriendButtonStatus();
                updateFollowButtonStatus();

                tvUserId.setText("");
                tvUserKey.setText("");
                tvToken.setText("");
                break;
            case USER_PRIVATE_PROFILE:
                tvProductWishlistLabel.setText(R.string.LB_CA_FRIEND_LIST);

                btnCoverEdit.setVisibility(View.VISIBLE);
                btnAvatarEdit.setVisibility(View.VISIBLE);
                btnProfileSettings.setVisibility(View.VISIBLE);

                btnFriend.setVisibility(View.GONE);
                btnFollow.setVisibility(View.GONE);
                btnProfileSearch.setVisibility(View.GONE);

                tvUserId.setText(String.valueOf(MmGlobal.getUserId()));
                tvUserKey.setText(MmGlobal.getUserKey());
                tvToken.setText(MmGlobal.getToken());
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_profile, menu);

        //for heart icon
        MenuItem item = menu.findItem(R.id.action_like);
        MenuItemCompat.setActionView(item, R.layout.badge_button_like);

        RelativeLayout badgeLayout = (RelativeLayout) MenuItemCompat.getActionView(item);
        View badge = badgeLayout.findViewById(R.id.btnBadge);
        ImageView heartIcon = (ImageView) badgeLayout.findViewById(R.id.icon_heart_stroke);

        if (mIsToolbarCollapsed) {
            heartIcon.setImageResource(R.drawable.wishlist_grey);
        } else {
            heartIcon.setImageResource(R.drawable.wishlist_wht);
        }

        if (CacheManager.getInstance().isWishlistHasItem()) {
            badge.setVisibility(View.VISIBLE);
        } else {
            badge.setVisibility(View.GONE);
        }

        item.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StoreFrontUserProfileActivity.this, WishListActivity.class);
                startActivity(i);
            }
        });

        //for cart icon
        MenuItem cartItem = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(cartItem, R.layout.badge_button_cart);

        RelativeLayout badgeCartLayout = (RelativeLayout) MenuItemCompat.getActionView(cartItem);
        View cartBadge = badgeCartLayout.findViewById(R.id.btnBadge);
        ImageView cartIcon = (ImageView) badgeCartLayout.findViewById(R.id.cartIcon);

        if (mIsToolbarCollapsed) {
            cartIcon.setImageResource(R.drawable.cart_grey);
        } else {
            cartIcon.setImageResource(R.drawable.cart_wht);
        }

        if (CacheManager.getInstance().isCartHasItem()) {
            cartBadge.setVisibility(View.VISIBLE);
        } else {
            cartBadge.setVisibility(View.GONE);
        }

        cartItem.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StoreFrontUserProfileActivity.this, ShoppingCartActivity.class);
                startActivity(i);
            }
        });

        if (mIsToolbarCollapsed) {
            btnProfileSettings.setImageResource(R.drawable.setting_btn);
            btnProfileSearch.setImageResource(R.drawable.search_grey);
        } else {
            btnProfileSettings.setImageResource(R.drawable.setting_btn_wht);
            btnProfileSearch.setImageResource(R.drawable.search_wht);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUEST_TO_PROFILE || requestCode == CAMERA_REQUEST_TO_COVER
                || requestCode == SELECT_FILE_REQUEST_TO_PROFILE || requestCode == SELECT_FILE_REQUEST_TO_COVER) {
            handleImageResult(requestCode, resultCode, data);
        } else if(requestCode == PROFILE_LIFE_CYCLE_REQUEST) {
            handleLifeCycleResult(resultCode, data);
        }
    }

    private void handleImageResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri selectedImageUri;
            if (data != null && data.getData() != null) {
                selectedImageUri = data.getData();
            } else {
                selectedImageUri = currentPictureUri;
            }
            Bitmap photo = null;
            try {
                photo = PhotoUtil.getCorrectlyOrientedImage(this, selectedImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (requestCode) {
                case CAMERA_REQUEST_TO_PROFILE:
                case SELECT_FILE_REQUEST_TO_PROFILE:
                    if (photo != null) {
                        ivProfilePic.setImageBitmap(photo);
                        PhotoUtil.uploadPhoto(photo, PhotoUtil.PhotoType.PROFILE);
                    }
                    break;
                case CAMERA_REQUEST_TO_COVER:
                case SELECT_FILE_REQUEST_TO_COVER:
                    if (photo != null) {
                        ivCoverPic.setImageBitmap(photo);
                        PhotoUtil.uploadPhoto(photo, PhotoUtil.PhotoType.COVER);
                    }
                    break;

            }
        }
    }

    private void handleLifeCycleResult(int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            getProfileExtras(data);
        } else {
            profileType = ProfileType.USER_PRIVATE_PROFILE;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        isProfileLoaded = false;
        tvDisplayName.setText("");
        tvFollowingUser.setText("0");
        tvFollowingCurator.setText("0");
        tvProductWishlist.setText("0");
        tvFollowingMerchant.setText("0");
        tvFollower.setText("0");
        ivProfilePic.setBorderColor(getResources().getColor(R.color.white));
        ivCuratorDiamond.setVisibility(View.GONE);
        ivProfilePic.setImageResource(R.drawable.default_profile_icon);
        ivCoverPic.setImageResource(R.drawable.default_cover);

        getUser();
        updateLayout(profileType);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onEvent(UpdatePhotoEvent event) {
        if (event != null) {
            if (event.profileImage != null) {
                mProfileUser.setProfileImage(event.profileImage);
            }
            if (event.coverImage != null) {
                mProfileUser.setCoverImage(event.coverImage);
            }
        }
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        AppBarLayout appBar = (AppBarLayout) findViewById(R.id.appbar);
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int scrollRange = appBarLayout.getTotalScrollRange();
                int toolbarHeight = getSupportActionBar().getHeight();
                if (scrollRange + verticalOffset <= toolbarHeight) {
                    if (!mIsToolbarCollapsed && mProfileUser != null) {
                        tvToolbarTitle.setText(mProfileUser.getDisplayName());
                        mIsToolbarCollapsed = true;
                        invalidateOptionsMenu();
                    }
                } else if (mIsToolbarCollapsed) {
                    tvToolbarTitle.setText("");
                    mIsToolbarCollapsed = false;
                    invalidateOptionsMenu();
                }
            }
        });
    }

    private void pickImage(int REQUEST_CODE) {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.LB_PHOTO_LIBRARY)), REQUEST_CODE);
    }

    private void getUser() {
        switch (profileType) {
            case USER_PRIVATE_PROFILE:
                getPrivateProfile();
                ProfileLifeCycleManager.getInstance().restartLifeCycle();
                break;
            case USER_PUBLIC_PROFILE:
                getPublicProfile();
                break;
            case MERCHANT_PROFILE:
                break;
        }
    }

    private void getPublicProfile() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getViewService().viewUser(publicUserKey).enqueue(new MmCallBack<User>(this, true) {
            @Override
            public void onSuccess(Response<User> response, Retrofit retrofit) {
                mProfileUser = response.body();
                if(mProfileUser.getUserKey().equals(publicUserKey) && !isProfileLoaded) {
                    tvDisplayName.setText(mProfileUser.getDisplayName());
                    tvFollowingMerchant.setText(String.valueOf(mProfileUser.getFollowingMerchantCount()));
                    tvFollower.setText(String.valueOf(mProfileUser.getFollowerCount()));
                    tvFollowingUser.setText(String.valueOf(mProfileUser.getFollowingUserCount()));
                    tvFollowingCurator.setText(String.valueOf(mProfileUser.getFollowingCuratorCount()));
                    // TODO: change get wish list count
                    tvProductWishlist.setText("0");

                    if (mProfileUser.getIsCuratorUser() == 1) {
                        ivProfilePic.setBorderColor(getResources().getColor(R.color.mm_red));
                        ivCuratorDiamond.setVisibility(View.VISIBLE);
                    } else {
                        ivProfilePic.setBorderColor(getResources().getColor(R.color.white));
                        ivCuratorDiamond.setVisibility(View.GONE);
                    }

                    if (!TextUtils.isEmpty(mProfileUser.getProfileImage())) {
                        String avatarUrl = PathUtil.getUserImageUrl(mProfileUser.getProfileImage());
                        Picasso.with(MyApplication.getContext()).load(avatarUrl).placeholder(R.drawable.default_profile_icon).into(ivProfilePic);
                    }

                    if (!TextUtils.isEmpty(mProfileUser.getCoverImage())) {
                        String coverUrl = PathUtil.getUserImageUrl(mProfileUser.getCoverImage());
                        Picasso.with(MyApplication.getContext()).load(coverUrl).placeholder(R.drawable.default_cover).into(ivCoverPic);
                    }
                }
            }

            @Override
            public void onResponse(Response<User> response, Retrofit retrofit) {
                super.onResponse(response, retrofit);
                isProfileLoaded = true;
            }
        });
    }

    private void getPrivateProfile() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getUserService().viewUser(MmGlobal.getUserId()).enqueue(new MmCallBack<User>(this, true) {
            @Override
            public void onSuccess(Response<User> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    mProfileUser = response.body();
                    if(mProfileUser.getUserId().equals(MmGlobal.getUserId()) && !isProfileLoaded) {
                        tvDisplayName.setText(mProfileUser.getDisplayName());
                        tvFollowingMerchant.setText(String.valueOf(mProfileUser.getFollowingMerchantCount()));
                        tvFollower.setText(String.valueOf(mProfileUser.getFollowerCount()));
                        tvFollowingUser.setText(String.valueOf(mProfileUser.getFollowingUserCount()));
                        tvFollowingCurator.setText(String.valueOf(mProfileUser.getFollowingCuratorCount()));
                        tvProductWishlist.setText(String.valueOf(mProfileUser.getFriendCount()));

                        if (mProfileUser.getIsCuratorUser() == 1) {
                            ivProfilePic.setBorderColor(getResources().getColor(R.color.mm_red));
                            ivCuratorDiamond.setVisibility(View.VISIBLE);
                        } else {
                            ivProfilePic.setBorderColor(getResources().getColor(R.color.white));
                            ivCuratorDiamond.setVisibility(View.GONE);
                        }

                        if (!TextUtils.isEmpty(mProfileUser.getProfileImage())) {
                            String avatarUrl = PathUtil.getUserImageUrl(mProfileUser.getProfileImage());
                            Picasso.with(MyApplication.getContext()).load(avatarUrl).placeholder(R.drawable.default_profile_icon).into(ivProfilePic);
                        }

                        if (!TextUtils.isEmpty(mProfileUser.getCoverImage())) {
                            String coverUrl = PathUtil.getUserImageUrl(mProfileUser.getCoverImage());
                            Picasso.with(MyApplication.getContext()).load(coverUrl).placeholder(R.drawable.default_cover).into(ivCoverPic);
                        }
                    }
                }
            }

            @Override
            public void onResponse(Response<User> response, Retrofit retrofit) {
                super.onResponse(response, retrofit);
                isProfileLoaded = true;
            }
        });
    }

    @OnClick(R.id.btnProfileSettings)
    public void startUserProfileSetting() {
        startActivity(new Intent(this, UserProfileSettingActivity.class));
    }

    @OnClick(R.id.btnProfileSearch)
    public void startSearch() {
        Intent intent = new Intent(this, ProductListSearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnAvatarEdit)
    public void onClickUserPhoto() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (mProfileUser != null && !TextUtils.isEmpty(mProfileUser.getProfileImage())) {
            final CharSequence[] items = {getString(R.string.LB_CA_VIEW_PROF_PIC),
                    getString(R.string.LB_CA_DEL_CURR_PROF_PIC),
                    getString(R.string.LB_CA_PROF_PIC_TAKE_PHOTO),
                    getString(R.string.LB_CA_PROF_PIC_CHOOSE_LIBRARY)};
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            displayFullscreen(PhotoUtil.PhotoType.PROFILE);
                            break;
                        case 1:
                            deleteProfilePhoto();
                            break;
                        case 2:
                            startCamera(CAMERA_REQUEST_TO_PROFILE);
                            break;
                        case 3:
                            pickImage(SELECT_FILE_REQUEST_TO_PROFILE);
                            break;
                    }
                    dialog.dismiss();
                }
            });
        } else {
            final CharSequence[] items = {getString(R.string.LB_CA_PROF_PIC_TAKE_PHOTO), getString(R.string.LB_CA_PROF_PIC_CHOOSE_LIBRARY)};
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            startCamera(CAMERA_REQUEST_TO_PROFILE);
                            break;
                        case 1:
                            pickImage(SELECT_FILE_REQUEST_TO_PROFILE);
                            break;
                    }
                    dialog.dismiss();
                }
            });
        }
        builder.show();
    }

    @OnClick(R.id.rlFriends)
    protected void displayFriends() {
        if (profileType == ProfileType.USER_PRIVATE_PROFILE) {
            if (mProfileUser != null && mProfileUser.getFriendCount() > 0) {
                Intent showListFriend = new Intent(this, FriendListActivity.class);
                startActivityForResult(showListFriend, PROFILE_LIFE_CYCLE_REQUEST);
            }
        } else {
            // TODO: go to wish list
        }
    }

    @OnClick(R.id.rlFollowingUser)
    protected void displayFollowingUser() {
        
        if (mProfileUser != null && mProfileUser.getFollowingUserCount() > 0) {
            openDataListActivity(mProfileUser.getUserKey(), StoreFrontUserProfileDataListActivity.ProfileDataListType.USER_LIST);
        }
    }

    @OnClick(R.id.rlFollowingCurator)
    protected void displayFollowingCurator() {
        if (mProfileUser != null && mProfileUser.getFollowingCuratorCount() > 0) {
            openDataListActivity(mProfileUser.getUserKey(), StoreFrontUserProfileDataListActivity.ProfileDataListType.CURATOR_LIST);
        }
    }

    @OnClick(R.id.rlFollowingMerchant)
    protected void displayFollowingMerchant() {
        if (mProfileUser != null && mProfileUser.getFollowingMerchantCount() > 0) {
            openDataListActivity(mProfileUser.getUserKey(), StoreFrontUserProfileDataListActivity.ProfileDataListType.MERCHANT_LIST);
        }
    }

    @OnClick(R.id.lnFollower)
    protected void startFollowerActivity() {
        if (mProfileUser != null && mProfileUser.getFollowerCount() > 0) {
            Intent intent = new Intent(this, FollowerActivity.class);
            intent.putExtra(FollowerActivity.USER_KEY, mProfileUser.getUserKey());
            intent.putExtra(FollowerActivity.FOLLOWER_TYPE_KEY, FollowerActivity.FOLLOWER_CONSUMER);
            startActivityForResult(intent, PROFILE_LIFE_CYCLE_REQUEST);
        }
    }

    @OnClick(R.id.btnFollow)
    protected void handleFollowRequest() {
        Follow followRequest = new Follow();
        followRequest.setUserKey(MmGlobal.getUserKey());
        followRequest.setToUserKey(mProfileUser.getUserKey());

        if (beFollowed) {
            APIManager.getInstance().getFollowService().deleteFollow(followRequest).enqueue(new MmCallBack<Boolean>(this) {
                @Override
                public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                    if (response.isSuccess() && response.body().equals(true)) {
                        beFollowed = false;
                        updateFollowButtonStatus();
                        MmStatusAlertUtil.show(StoreFrontUserProfileActivity.this, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                getString(R.string.MSG_SUC_UNFOLLOWED), null);
                    }
                }
            });
        } else {
            APIManager.getInstance().getFollowService().saveFollowCurator(followRequest).enqueue(new MmCallBack<Boolean>(this) {
                @Override
                public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                    if (response.isSuccess() && response.body().equals(true)) {
                        beFollowed = true;
                        updateFollowButtonStatus();
                        MmStatusAlertUtil.show(StoreFrontUserProfileActivity.this, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                                getString(R.string.MSG_SUC_FOLLOWED), null);
                    }
                }
            });
        }
    }

    @OnClick(R.id.btnFriend)
    protected void handleFriendRequest() {
        final FriendRequest friendRequest = new FriendRequest();
        friendRequest.setUserKey(MmGlobal.getUserKey());
        friendRequest.setToUserKey(mProfileUser.getUserKey());

        if (beFriend) {
            String confirmationContent = getString(R.string.LB_CA_REMOVE_FRD_CONF).replace("{0}", " " + mProfileUser.getDisplayName() + " ");
            ErrorUtil.showTwoOptionsDialog(this, "", confirmationContent, getString(R.string.LB_YES), getString(R.string.LB_NO), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    APIManager.getInstance().getFriendService().deleteRequest(friendRequest).enqueue(new MmCallBack<Boolean>(StoreFrontUserProfileActivity.this) {
                        @Override
                        public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                            beFriend = false;
                            updateFriendButtonStatus();
                        }
                    });
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        } else {
            APIManager.getInstance().getFriendService().request(friendRequest).enqueue(new MmCallBack<Boolean>(this) {
                @Override
                public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                    beFriend = true;
                    updateFriendButtonStatus();
                    MmStatusAlertUtil.show(StoreFrontUserProfileActivity.this, MmStatusAlertUtil.StatusAlertType.StatusAlertType_OK,
                            getString(R.string.MSG_SUC_FRIEND_REQ_SENT), null);
                }
            });
        }
    }

    @OnClick(R.id.ivProfilePic)
    protected void viewProfileImage() {
        if (!TextUtils.isEmpty(mProfileUser.getProfileImage())) {
            displayFullscreen(PhotoUtil.PhotoType.PROFILE);
        }
    }

    @OnClick(R.id.ivCoverPic)
    protected void viewCoverImage() {
        if (!TextUtils.isEmpty(mProfileUser.getCoverImage())) {
            displayFullscreen(PhotoUtil.PhotoType.COVER);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (openDemoCount >= MAX_DEMO_COUNTER)) {
            if (ConfigManager.getInstance().getConfig().isEnableDebugMode()) {
                openDemoCount = 1; //reset the counter
                openDemo();
                return true;
            }
        }
        openDemoCount++;
        return super.onKeyDown(keyCode, event);
    }

    public void openDemo() {
        Intent intent = new Intent(this, DemoActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.bottom_to_top, R.anim.not_move);
    }

    @OnClick(R.id.btnCoverEdit)
    public void onClickCoverPhoto() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (mProfileUser != null && !TextUtils.isEmpty(mProfileUser.getCoverImage())) {
            final CharSequence[] items = {getString(R.string.LB_CA_VIEW_COVER_PIC),
                    getString(R.string.LB_CA_DEL_CURR_PROF_PIC),
                    getString(R.string.LB_CA_PROF_PIC_TAKE_PHOTO),
                    getString(R.string.LB_CA_PROF_PIC_CHOOSE_LIBRARY)};
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            displayFullscreen(PhotoUtil.PhotoType.COVER);
                            break;
                        case 1:
                            deleteCover();
                            break;
                        case 2:
                            startCamera(CAMERA_REQUEST_TO_COVER);
                            break;
                        case 3:
                            pickImage(SELECT_FILE_REQUEST_TO_COVER);
                            break;
                        default:
                            dialog.dismiss();
                            break;
                    }
                }
            });
        } else {
            final CharSequence[] items = {getString(R.string.LB_CA_PROF_PIC_TAKE_PHOTO), getString(R.string.LB_CA_PROF_PIC_CHOOSE_LIBRARY)};
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            startCamera(CAMERA_REQUEST_TO_COVER);
                            break;
                        case 1:
                            pickImage(SELECT_FILE_REQUEST_TO_COVER);
                            break;
                    }
                    dialog.dismiss();
                }
            });
        }

        builder.show();
    }

    private void deleteCover() {
        APIManager.getInstance().getUserService().deleteCover().enqueue(new MmCallBack<DeleteCoverResponce>(this) {
            @Override
            public void onSuccess(Response<DeleteCoverResponce> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Picasso.with(MyApplication.getContext()).load(R.drawable.default_cover).into(ivCoverPic);
                    mProfileUser.setCoverImage("");
                }
            }
        });
    }

    private void startCamera(int request) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);

        if (Build.MODEL.toUpperCase().contains("NEXUS")) {
            String imageFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg";
            File imageFile = new File(imageFilePath);
            try {
                if (!imageFile.exists()) {
                    imageFile.getParentFile().mkdirs();
                    imageFile.createNewFile();
                }
            } catch (IOException e) {
            }
            currentPictureUri = Uri.fromFile(imageFile);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentPictureUri);
        }

        startActivityForResult(cameraIntent, request);
    }

    private void displayFullscreen(PhotoUtil.PhotoType type) {
        Intent intent = new Intent(StoreFrontUserProfileActivity.this, ProfilePhotoViewerActivity.class);

        if (type == PhotoUtil.PhotoType.PROFILE) {
            intent.putExtra(ProfilePhotoViewerActivity.IMAGE_DATA_KEY, new ImageData("", mProfileUser.getProfileImage()));
        } else {
            intent.putExtra(ProfilePhotoViewerActivity.IMAGE_DATA_KEY, new ImageData("", mProfileUser.getCoverImage()));
        }
        startActivity(intent);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    public void deleteProfilePhoto() {
        APIManager.getInstance().getUserService().deleteProfileImage().enqueue(new MmCallBack<DeleteImageResponce>(this) {
            @Override
            public void onSuccess(Response<DeleteImageResponce> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    mProfileUser.setProfileImage("");
                    Picasso.with(MyApplication.getContext()).load(R.drawable.default_profile_icon).into(ivProfilePic);
                }
            }
        });
    }

    private void updateFollowButtonStatus() {
        if (beFollowed) {
            btnFollow.setText(R.string.LB_CA_FOLLOWED);
            btnFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ok, 0, 0, 0);
        } else {
            btnFollow.setText(R.string.LB_CA_FOLLOW);
            btnFollow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_add, 0, 0, 0);
        }
    }

    private void updateFriendButtonStatus() {
        if (beFriend) {
            btnFriend.setText(R.string.LB_CA_BEFRIENDED);
            btnFriend.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_ok, 0, 0, 0);
        } else {
            btnFriend.setText(R.string.LB_CA_ADD_FRIEND);
            btnFriend.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_add, 0, 0, 0);
        }
    }

    private void openDataListActivity(String userKey, StoreFrontUserProfileDataListActivity.ProfileDataListType dataListType) {
        Intent intent = new Intent(this, StoreFrontUserProfileDataListActivity.class);
        intent.putExtra(StoreFrontUserProfileDataListActivity.USER_KEY, userKey);
        intent.putExtra(StoreFrontUserProfileDataListActivity.LIST_TYPE_KEY, dataListType);
        startActivityForResult(intent, PROFILE_LIFE_CYCLE_REQUEST);
    }

    private void getProfileExtras(Intent intent) {
        if(intent.hasExtra(PUBLIC_USER_KEY) && intent.hasExtra(PROFILE_TYPE_KEY)) {
            profileType = (ProfileType) intent.getSerializableExtra(PROFILE_TYPE_KEY);
            publicUserKey = intent.getStringExtra(PUBLIC_USER_KEY);
            beFriend = intent.getBooleanExtra(BE_FRIEND_KEY, false);
            beFollowed = intent.getBooleanExtra(BE_FOLLOWED_KEY, false);
        } else {
            profileType = ProfileType.USER_PRIVATE_PROFILE;
        }
    }
}
