package com.mm.main.app.schema;

import java.io.Serializable;

/**
 * Created by henrytung on 4/2/2016.
 */
public class CategoryBrandMerchant implements Serializable {

    Integer CategoryBrandMerchantId;
    Integer CategoryId;
    Integer EntityId;
    Integer Priority;
    String Entity;
    String Name;
    String NameInvariant;
    String HeaderLogoImage;
    String SmallLogoImage;
    String LargeLogoImage;
    String ProfileBannerImage;

    public Integer getCategoryBrandMerchantId() {
        return CategoryBrandMerchantId;
    }

    public void setCategoryBrandMerchantId(Integer categoryBrandMerchantId) {
        CategoryBrandMerchantId = categoryBrandMerchantId;
    }

    public Integer getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(Integer categoryId) {
        CategoryId = categoryId;
    }

    public Integer getEntityId() {
        return EntityId;
    }

    public void setEntityId(Integer entityId) {
        EntityId = entityId;
    }

    public Integer getPriority() {
        return Priority;
    }

    public void setPriority(Integer priority) {
        Priority = priority;
    }

    public String getEntity() {
        return Entity;
    }

    public void setEntity(String entity) {
        Entity = entity;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNameInvariant() {
        return NameInvariant;
    }

    public void setNameInvariant(String nameInvariant) {
        NameInvariant = nameInvariant;
    }

    public String getHeaderLogoImage() {
        return HeaderLogoImage;
    }

    public void setHeaderLogoImage(String headerLogoImage) {
        HeaderLogoImage = headerLogoImage;
    }

    public String getSmallLogoImage() {
        return SmallLogoImage;
    }

    public void setSmallLogoImage(String smallLogoImage) {
        SmallLogoImage = smallLogoImage;
    }

    public String getLargeLogoImage() {
        return LargeLogoImage;
    }

    public void setLargeLogoImage(String largeLogoImage) {
        LargeLogoImage = largeLogoImage;
    }

    public String getProfileBannerImage() {
        return ProfileBannerImage;
    }

    public void setProfileBannerImage(String profileBannerImage) {
        ProfileBannerImage = profileBannerImage;
    }
}
