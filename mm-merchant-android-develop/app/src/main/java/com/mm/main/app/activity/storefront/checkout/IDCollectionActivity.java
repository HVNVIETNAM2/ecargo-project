package com.mm.main.app.activity.storefront.checkout;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.tnc.TermsAndConditionsActivity;
import com.mm.main.app.event.UpdatePhotoEvent;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.schema.request.IdentificationSaveRequest;
import com.mm.main.app.utils.AnimateUtil;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.PhotoUtil;

import java.io.File;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class IDCollectionActivity extends AppCompatActivity {

    final int REQUEST_CAMERA = 9872;
    final int REQUEST_SELECT_FILE = REQUEST_CAMERA + 1;
    public final static String ORDER_ID_KEY = "ORDER_ID_KEY";

    @Bind(R.id.etFirstname)
    EditText etFirstname;

    @Bind(R.id.etLastname)
    EditText etLastname;

    @Bind(R.id.etIDNumber)
    EditText etIDNumber;

    @Bind(R.id.tvError)
    TextView tvError;

    @Bind(R.id.btnOK)
    Button btnOK;

    @Bind(R.id.hiddenFocus)
    EditText edHiddenFocus;

    @Bind(R.id.id_holder_1)
    ImageView id_holder_1;

    @Bind(R.id.id_holder_2)
    ImageView id_holder_2;

    ImageView currentImageView;

    Bitmap photo1 = null;
    Bitmap photo2 = null;

    private Uri currentPictureUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_id_card_collection);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ButterKnife.bind(this);
        setupEditText();
    }

    private void setupEditText() {

        edHiddenFocus.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edHiddenFocus.getWindowToken(), 0);
                }
            }
        });

        etLastname.addTextChangedListener(handleErrorWatcher);
        etFirstname.addTextChangedListener(handleErrorWatcher);
        etIDNumber.addTextChangedListener(handleErrorWatcher);

    }

    TextWatcher handleErrorWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            AnimateUtil.collapse(tvError);
            int color = ContextCompat.getColor(IDCollectionActivity.this, R.color.secondary2);
            etFirstname.setTextColor(color);
            etLastname.setTextColor(color);
            etIDNumber.setTextColor(color);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @OnClick(R.id.id_holder_1)
    public void onHolder1Click() {
        selectImage();
        currentImageView = id_holder_1;
        AnimateUtil.collapse(tvError);
    }

    @OnClick(R.id.id_holder_2)
    public void onHolder2Click() {
        selectImage();
        currentImageView = id_holder_2;
        AnimateUtil.collapse(tvError);
    }

    public void selectImage() {
        final CharSequence[] items = {getString(R.string.LB_TAKE_PHOTO), getString(R.string.LB_PHOTO_LIBRARY)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        startCamera();
                        break;
                    case 1:
                        pickImage();
                        break;
                    default:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }

    public void startCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);

        if (Build.MODEL.toUpperCase().contains("NEXUS")) {
            String imageFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg";
            File imageFile = new File(imageFilePath);
            try {
                if (!imageFile.exists()) {
                    imageFile.getParentFile().mkdirs();
                    imageFile.createNewFile();
                }
            } catch (IOException e) {
            }
            currentPictureUri = Uri.fromFile(imageFile);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentPictureUri);
        }

        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    public void pickImage() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, getString(R.string.LB_PHOTO_LIBRARY)),
                REQUEST_SELECT_FILE);
    }

    @OnClick(R.id.btnOK)
    public void proceed() {
        if (validate()) {
            saveInfo();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                case REQUEST_SELECT_FILE:

                    Uri selectedImageUri;
                    if (data != null && data.getData() != null) {
                        selectedImageUri = data.getData();
                    } else {
                        selectedImageUri = currentPictureUri;
                    }

                    try {
                        Bitmap photo = PhotoUtil.getCorrectlyOrientedImage(this, selectedImageUri);

                        currentImageView.setImageBitmap(photo);

                        if (currentImageView == id_holder_1) {
                            photo1 = photo;
                        } else {
                            photo2 = photo;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    private boolean validate() {

        if (photo1 == null) {
            ErrorUtil.showVerificationError(tvError, null, getString(R.string.MSG_ERR_ID_FRONT_IMAGE_NIL), this);
            return false;
        }

        if (photo2 == null) {
            ErrorUtil.showVerificationError(tvError, null,  getString(R.string.MSG_ERR_ID_BACK_IMAGE_NIL), this);
            return false;
        }

        if (TextUtils.isEmpty(etFirstname.getText().toString().trim())) {
            ErrorUtil.showVerificationError(tvError, null, getString(R.string.MSG_ERR_MERCHANT_FIRSTNAME_NIL), this);
            return false;
        }

        if (TextUtils.isEmpty(etLastname.getText().toString().trim())) {
            ErrorUtil.showVerificationError(tvError, null, getString(R.string.MSG_ERR_LASTNAME_NIL), this);
            return false;
        }

        if (TextUtils.isEmpty(etIDNumber.getText().toString().trim())) {
            ErrorUtil.showVerificationError(tvError, null, getString(R.string.MSG_ERR_ID_NUMBER_NIL), this);
            return false;
        }

        return true;
    }

    private void saveInfo() {
        IdentificationSaveRequest info = new IdentificationSaveRequest(MmGlobal.getUserKey(), getIntent().getStringExtra(ORDER_ID_KEY), etFirstname.getText().toString().trim(), etLastname.getText().toString().trim(), etIDNumber.getText().toString().trim(), ((BitmapDrawable)id_holder_1.getDrawable()).getBitmap(), ((BitmapDrawable)id_holder_2.getDrawable()).getBitmap());

        PhotoUtil.uploadIDPhotos(info);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onEvent(UpdatePhotoEvent event) {
        if (event.isSuccess) {
            startActivity(new Intent(this, PaymentSelectionActivity.class));
        } else {
            ErrorUtil.showErrorDialog(this, getString(R.string.MSG_ERR_ADMIN_GENERAL_ERR));
        }
    }
}
