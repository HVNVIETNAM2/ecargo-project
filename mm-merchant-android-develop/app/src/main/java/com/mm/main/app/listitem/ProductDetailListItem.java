package com.mm.main.app.listitem;

/**
 * Setting schema
 */
public class ProductDetailListItem {
    String title;
    String content;
    ItemType type;

    public enum ItemType {
        TYPE_IMAGE_DEFAULT,
        TYPE_NAME,
        TYPE_BUY,
        TYPE_SIZE,
        TYPE_COLOR,
        TYPE_DESC,
        TYPE_PARAMETER,
        TYPE_COMMENT,
        TYPE_RECOMMEND,
        TYPE_DESC_IMAGE,
        TYPE_OUTFIT,
        TYPE_SUGGESTION,
        TYPE_SCORE
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public ProductDetailListItem() {
    }

    public ProductDetailListItem(String title, String content, ItemType type) {

        this.title = title;
        this.content = content;
        this.type = type;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }
}
