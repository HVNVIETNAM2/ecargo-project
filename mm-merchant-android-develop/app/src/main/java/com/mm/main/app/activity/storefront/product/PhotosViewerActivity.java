package com.mm.main.app.activity.storefront.product;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.fragment.PhotoPagerFragment;
import com.mm.main.app.library.swipeback.SwipeBackLayout;
import com.mm.main.app.schema.ImageData;
import com.mm.main.app.utils.UiUtil;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class PhotosViewerActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, SwipeBackLayout.SwipeBackListener {
    public static String IMAGE_DATA_KEY = "IMAGE_DATA_KEY";
    public static String CURRENT_POS_KEY = "CURRENT_POS_KEY";

    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.llIndicator)
    LinearLayout llIndicator;
    @Bind(R.id.swipe_back)
    SwipeBackLayout swipeBack;
    @Bind(R.id.viewBackground)
    View viewBackground;

    private ImageView[] imageViewIndicators;
    protected ArrayList<ImageData> photoList;
    protected int currentPos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_viewer);
        ButterKnife.bind(this);

        PagerAdapter pagerAdapter = initPhotoAdapter();
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(this);
        pager.setCurrentItem(currentPos);
        swipeBack.setOnSwipeBackListener(this);
    }

    protected PagerAdapter initPhotoAdapter() {
        photoList = (ArrayList<ImageData>)getIntent().getSerializableExtra(IMAGE_DATA_KEY);
        currentPos = getIntent().getIntExtra(CURRENT_POS_KEY, 0);
        currentPos = (3000 / photoList.size()) * photoList.size() + currentPos;

        renderIndicator();
        return new ScreenSlidePagerAdapter(getSupportFragmentManager());
    }

    private void renderIndicator() {

        imageViewIndicators = new ImageView[photoList.size()];
        for (int i = 0; i < photoList.size(); i++) {
            imageViewIndicators[i] = new ImageView(MyApplication.getContext());
            LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(5, 5);
            int rightPadding = UiUtil.getPixelFromDp(10);
            layoutParams.setMargins(0, 0, rightPadding, 0);
            imageViewIndicators[i].setLayoutParams(layoutParams);

            imageViewIndicators[i].setId(i);

            if (i == 0) {
                imageViewIndicators[i].setImageDrawable(ContextCompat.getDrawable(MyApplication.getContext(), R.drawable.scroll_color_ball_red));
            } else {
                imageViewIndicators[i].setImageDrawable(ContextCompat.getDrawable(MyApplication.getContext(), R.drawable.scroll_color_ball_gray));
            }

            llIndicator.addView(imageViewIndicators[i]);
        }
    }

    private void setIndicator(int pos) {
        for (int i = 0; i < imageViewIndicators.length; i++) {
            if (i == pos) {
                imageViewIndicators[i].setImageDrawable(ContextCompat.getDrawable(MyApplication.getContext(), R.drawable.scroll_color_ball_red));
            } else {
                imageViewIndicators[i].setImageDrawable(ContextCompat.getDrawable(MyApplication.getContext(), R.drawable.scroll_color_ball_gray));
            }
        }
    }

    public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private int imageType;

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            this(fm, PhotoPagerFragment.IMAGE_TYPE_PRODUCT);
        }

        public ScreenSlidePagerAdapter(FragmentManager fm, int imageType) {
            super(fm);
            this.imageType = imageType;
        }

        @Override
        public Fragment getItem(int position) {
            PhotoPagerFragment fragment = new PhotoPagerFragment();
            fragment.setImageType(imageType);
            fragment.setImageData(photoList.get(position % photoList.size()));
            return fragment;
        }

        @Override
        public int getCount() {
            switch (imageType) {
                case PhotoPagerFragment.IMAGE_TYPE_PROFILE:
                    return 1;
                case PhotoPagerFragment.IMAGE_TYPE_PRODUCT:
                    return Integer.MAX_VALUE;
                default:
                    return Integer.MAX_VALUE;
            }
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        position = position % photoList.size();
        setIndicator(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onViewPositionChanged(float fractionAnchor, float fractionScreen) {
        float alpha = Math.max(1.0f - (fractionScreen * 2.0f), 0.2f);
        viewBackground.setAlpha(alpha);
    }

    public void setPagerEnabled(boolean enabled) {

    }
}