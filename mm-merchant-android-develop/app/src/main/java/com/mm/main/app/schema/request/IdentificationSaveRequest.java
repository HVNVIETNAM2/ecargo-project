package com.mm.main.app.schema.request;

import android.graphics.Bitmap;

/**
 * Created by thienchaud on 11-Mar-16.
 */
public class IdentificationSaveRequest {

    String UserKey;
    String OrderKey;
    String FirstName;
    String LastName;
    String IdentificationNumber;
    Bitmap frontImage;
    Bitmap backImage;

    public IdentificationSaveRequest(String userKey, String orderKey, String firstName, String lastName, String identificationNumber, Bitmap frontImage, Bitmap backImage) {
        UserKey = userKey;
        OrderKey = orderKey;
        FirstName = firstName;
        LastName = lastName;
        IdentificationNumber = identificationNumber;
        this.frontImage = frontImage;
        this.backImage = backImage;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public String getOrderKey() {
        return OrderKey;
    }

    public void setOrderKey(String orderKey) {
        OrderKey = orderKey;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getIdentificationNumber() {
        return IdentificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        IdentificationNumber = identificationNumber;
    }

    public Bitmap getFrontImage() {
        return frontImage;
    }

    public void setFrontImage(Bitmap frontImage) {
        this.frontImage = frontImage;
    }

    public Bitmap getBackImage() {
        return backImage;
    }

    public void setBackImage(Bitmap backImage) {
        this.backImage = backImage;
    }
}
