package com.mm.main.app.adapter.strorefront.filter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.utils.PathUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by henrytung on 4/11/15.
 */
public class MerchantFilterSelectionListAdapter extends BaseAdapter implements Filterable {

    protected final Activity context;
    protected List<FilterListItem<Merchant>> originalData;
    protected List<FilterListItem<Merchant>> filteredData;
    private Integer selectedItemId;
    private Drawable rightIcon;

    protected ItemFilter mFilter;

    public MerchantFilterSelectionListAdapter(Activity context, List<FilterListItem<Merchant>> itemList, Integer selectedItemId) {
        this.context = context;
        this.originalData = itemList;
        this.filteredData = itemList;
        this.selectedItemId = selectedItemId;
        rightIcon = context.getResources().getDrawable(R.drawable.icon_tick);
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FilterListItem<Merchant> currentItem = filteredData.get(position);
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.brand_filter_selection_item, null, true);
        }

        ImageView ivBrandLogo = (ImageView) convertView.findViewById(R.id.ivBrandLogo);
        String url = PathUtil.getMerchantImageUrl(currentItem.getT().getHeaderLogoImage());
        Picasso.with(MyApplication.getContext()).load(url).into(ivBrandLogo);

        TextView textView = (TextView) convertView.findViewById(R.id.tvName1);
        if (!TextUtils.isEmpty(currentItem.getT().getMerchantDisplayName())) {
            textView.setText(currentItem.getT().getMerchantDisplayName());
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE
            );
        }

        TextView textView2 = (TextView) convertView.findViewById(R.id.tvName2);
        textView2.setText(currentItem.getT().getMerchantCompanyName());

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);

//        ListView lv = (ListView) parent;
        if (currentItem.isSelected()) {
            imageView.setImageDrawable(rightIcon);
        } else {
            imageView.setImageDrawable(null);
        }

        return convertView;
    }

    public Integer getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(Integer selectedItemId) {
        if (selectedItemId >= 0) {
            filteredData.get(selectedItemId).setSelected(!filteredData.get(selectedItemId).isSelected());
        }
    }

    @Override
    public Filter getFilter() {
        if(mFilter==null) {
            mFilter = new ItemFilter();
        }
        return mFilter;
    }

    protected class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString;
            filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<FilterListItem<Merchant>> list = originalData;

            int count = list.size();
            final ArrayList<FilterListItem<Merchant>> nlist = new ArrayList<>(count);

            String filterableString ;

            if (!TextUtils.isEmpty(constraint)) {
                for (int i = 0; i < count; i++) {
                    filterableString = getFilterableString(list.get(i).getT());
                    if (filterableString.toLowerCase().contains(filterString)) {
                        FilterListItem<Merchant> mYourCustomData = list.get(i);
                        nlist.add(mYourCustomData);
                    }
                }
            } else {
                for (int i = 0; i < count; i++) {
                    FilterListItem<Merchant> mYourCustomData = list.get(i);
                    nlist.add(mYourCustomData);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        protected String getFilterableString(Merchant item) {
            String filterableString = item.getMerchantDisplayName() + " " + item.getMerchantCompanyName();
            return  filterableString;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<FilterListItem<Merchant>>) results.values;
            notifyDataSetChanged();
        }

    }

    public List<FilterListItem<Merchant>> getOriginalData() {
        return originalData;
    }

    public void setOriginalData(List<FilterListItem<Merchant>> originalData) {
        this.originalData = originalData;
    }

    public List<FilterListItem<Merchant>> getFilteredData() {
        return filteredData;
    }

    public void setFilteredData(List<FilterListItem<Merchant>> filteredData) {
        this.filteredData = filteredData;
    }
}
