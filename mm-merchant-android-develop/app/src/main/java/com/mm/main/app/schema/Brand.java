
package com.mm.main.app.schema;

import java.io.Serializable;

public class Brand implements Serializable{

    private Integer BrandId;
    private String BrandNameInvariant;
    private String BrandCode;
    private String BrandSubdomain;
    private Integer IsListedBrand;
    private Integer IsFeaturedBrand;
    private Integer IsRecommendedBrand;
    private Integer IsSearchableBrand;
    private String HeaderLogoImage;
    private String SmallLogoImage;
    private String LargeLogoImage;
    private String ProfileBannerImage;
    private String BrandDescInvariant;
    private Integer BrandCultureId;
    private Integer StatusId;
    private Integer StyleCount;
    private Integer MerchantCount;
    private String CultureCode;
    private String BrandName;

    /**
     * 
     * @return
     *     The BrandId
     */
    public Integer getBrandId() {
        return BrandId;
    }

    /**
     * 
     * @param BrandId
     *     The BrandId
     */
    public void setBrandId(Integer BrandId) {
        this.BrandId = BrandId;
    }

    /**
     * 
     * @return
     *     The BrandNameInvariant
     */
    public String getBrandNameInvariant() {
        return BrandNameInvariant;
    }

    /**
     * 
     * @param BrandNameInvariant
     *     The BrandNameInvariant
     */
    public void setBrandNameInvariant(String BrandNameInvariant) {
        this.BrandNameInvariant = BrandNameInvariant;
    }

    /**
     * 
     * @return
     *     The BrandCode
     */
    public String getBrandCode() {
        return BrandCode;
    }

    /**
     * 
     * @param BrandCode
     *     The BrandCode
     */
    public void setBrandCode(String BrandCode) {
        this.BrandCode = BrandCode;
    }

    /**
     * 
     * @return
     *     The BrandCultureId
     */
    public Integer getBrandCultureId() {
        return BrandCultureId;
    }

    /**
     * 
     * @param BrandCultureId
     *     The BrandCultureId
     */
    public void setBrandCultureId(Integer BrandCultureId) {
        this.BrandCultureId = BrandCultureId;
    }

    /**
     * 
     * @return
     *     The CultureCode
     */
    public String getCultureCode() {
        return CultureCode;
    }

    /**
     * 
     * @param CultureCode
     *     The CultureCode
     */
    public void setCultureCode(String CultureCode) {
        this.CultureCode = CultureCode;
    }

    /**
     * 
     * @return
     *     The BrandName
     */
    public String getBrandName() {
        return BrandName;
    }

    /**
     * 
     * @param BrandName
     *     The BrandName
     */
    public void setBrandName(String BrandName) {
        this.BrandName = BrandName;
    }

    public String getBrandSubdomain() {
        return BrandSubdomain;
    }

    public void setBrandSubdomain(String brandSubdomain) {
        BrandSubdomain = brandSubdomain;
    }

    public Integer getIsListedBrand() {
        return IsListedBrand;
    }

    public void setIsListedBrand(Integer isListedBrand) {
        IsListedBrand = isListedBrand;
    }

    public Integer getIsFeaturedBrand() {
        return IsFeaturedBrand;
    }

    public void setIsFeaturedBrand(Integer isFeaturedBrand) {
        IsFeaturedBrand = isFeaturedBrand;
    }

    public Integer getIsRecommendedBrand() {
        return IsRecommendedBrand;
    }

    public void setIsRecommendedBrand(Integer isRecommendedBrand) {
        IsRecommendedBrand = isRecommendedBrand;
    }

    public Integer getIsSearchableBrand() {
        return IsSearchableBrand;
    }

    public void setIsSearchableBrand(Integer isSearchableBrand) {
        IsSearchableBrand = isSearchableBrand;
    }

    public String getHeaderLogoImage() {
        return HeaderLogoImage;
    }

    public void setHeaderLogoImage(String headerLogoImage) {
        HeaderLogoImage = headerLogoImage;
    }

    public String getSmallLogoImage() {
        return SmallLogoImage;
    }

    public void setSmallLogoImage(String smallLogoImage) {
        SmallLogoImage = smallLogoImage;
    }

    public String getLargeLogoImage() {
        return LargeLogoImage;
    }

    public void setLargeLogoImage(String largeLogoImage) {
        LargeLogoImage = largeLogoImage;
    }

    public String getProfileBannerImage() {
        return ProfileBannerImage;
    }

    public void setProfileBannerImage(String profileBannerImage) {
        ProfileBannerImage = profileBannerImage;
    }

    public String getBrandDescInvariant() {
        return BrandDescInvariant;
    }

    public void setBrandDescInvariant(String brandDescInvariant) {
        BrandDescInvariant = brandDescInvariant;
    }

    public Integer getStatusId() {
        return StatusId;
    }

    public void setStatusId(Integer statusId) {
        StatusId = statusId;
    }

    public Integer getStyleCount() {
        return StyleCount;
    }

    public void setStyleCount(Integer styleCount) {
        StyleCount = styleCount;
    }

    public Integer getMerchantCount() {
        return MerchantCount;
    }

    public void setMerchantCount(Integer merchantCount) {
        MerchantCount = merchantCount;
    }
}
