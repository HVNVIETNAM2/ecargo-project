package com.mm.main.app.activity.storefront.friend;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.friend.ImSearchUserRvAdapter;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.ImSearchUserRvItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.FriendRequest;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.MmStatusAlertUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.mm.main.app.view.MmSearchBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

public class ImSearchUserActivity extends AppCompatActivity {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerFriends;

    @Bind(R.id.searchView)
    MmSearchBar searchView;

    List<ImSearchUserRvItem> listData;
    ImSearchUserRvAdapter adapter;
    List<ImSearchUserRvItem> currentData;
    private boolean isSearching = false;
    private String SEARCH_ALL_USERS = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_im_search_user);
        ButterKnife.bind(this);

        searchView.setHint(R.string.MSG_ERR_MERCHANT_DISPLAYNAME_NIL);

        if (!MmGlobal.getUserKey().isEmpty()) {
            fetchDataList();
            setUpSearchView();
        }
        setUpAdapter();
    }

    private void setUpSearchView() {
        searchView.setMmSearchBarListener(new MmSearchBar.MmSearchBarListener() {
            @Override
            public void onEnterText(CharSequence s) {
                if (listData != null && listData.size() > 0) {
                    currentData.clear();
                    if (s.equals(SEARCH_ALL_USERS)) {
                        currentData.addAll(listData);
                    } else {
                        for (ImSearchUserRvItem item : listData) {
                            User user = item.getUser();
                            if (user != null && user.getDisplayName().toLowerCase().contains(s.toString().toLowerCase())) {
                                currentData.add(item);
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }

                    if(currentData.size() == 0){
                        searchFriend(s.toString());
                    }
                }
            }

            @Override
            public void onCancelSearch() {
                if (listData != null && listData.size() > 0) {
                    currentData.clear();
                    currentData.addAll(listData);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void fetchDataList() {
        listData = new ArrayList<>();
        currentData = new ArrayList<>();
        searchFriend(SEARCH_ALL_USERS);
    }

    private void setUpAdapter() {

        adapter = new ImSearchUserRvAdapter(this, currentData, new ImSearchUserRvAdapter.SearchUserCallback() {
            @Override
            public void seeProfile(int position) {

            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerFriends.setLayoutManager(linearLayoutManager);
        recyclerFriends.setHasFixedSize(true);
        recyclerFriends.setAdapter(adapter);
    }

    @OnClick(R.id.btnBack)
    public void cancelSearchUser() {
        finish();
    }

    private void resetData(){
        if(listData != null && currentData != null){
            listData.clear();
            currentData.clear();
        }
    }

    private void searchFriend(String displayName){
        if(isSearching) return;
        MmProgressDialog.show(this);
        isSearching = true;
        APIManager.getInstance().getFriendService().searchFriends(MmGlobal.getUserKey(), displayName)
        .enqueue(new MmCallBack<List<User>>(ImSearchUserActivity.this) {
            @Override
            public void onSuccess(Response<List<User>> response, Retrofit retrofit) {
                List<ImSearchUserRvItem> tempList = new ArrayList<>();
                currentData.clear();

                for (User user : response.body()) {
                    ImSearchUserRvItem searchUserRvItem = new ImSearchUserRvItem();
                    searchUserRvItem.setUser(user);
                    searchUserRvItem.setTypeFriendAction(ImSearchUserRvItem.ItemType.TYPE_NOT_BEFRIENDED);
                    searchUserRvItem.setTypeFollowAction(ImSearchUserRvItem.ItemType.TYPE_FOLLOW);
                    tempList.add(searchUserRvItem);
                    listData.add(searchUserRvItem);
                }
                currentData.addAll(tempList);
                adapter.notifyDataSetChanged();
                isSearching = false;
                MmProgressDialog.dismiss();
            }
        });
    }
}
