
package com.mm.main.app.schema;


import java.io.Serializable;

public class ProductColor implements Serializable {

    private Integer ColorId;
    private String ColorKey;
    private String ColorName;
    private String ColorImage;
    private String ColorNameInvariant;
    private String ColorCultureId;
    private String CultureCode;
    private String ColorCode;
    private String SkuColor;

    /**
     * 
     * @return
     *     The ColorId
     */
    public Integer getColorId() {
        return ColorId;
    }

    /**
     * 
     * @param ColorId
     *     The ColorId
     */
    public void setColorId(Integer ColorId) {
        this.ColorId = ColorId;
    }

    public String getColorKey() {
        return ColorKey;
    }

    public void setColorKey(String colorKey) {
        ColorKey = colorKey;
    }

    /**
     * 
     * @return
     *     The ColorName
     */
    public String getColorName() {
        return ColorName;
    }

    /**
     * 
     * @param ColorName
     *     The ColorName
     */
    public void setColorName(String ColorName) {
        this.ColorName = ColorName;
    }

    /**
     * 
     * @return
     *     The SkuColor
     */
    public String getSkuColor() {
        return SkuColor;
    }

    /**
     * 
     * @param SkuColor
     *     The SkuColor
     */
    public void setSkuColor(String SkuColor) {
        this.SkuColor = SkuColor;
    }

    public String getColorNameInvariant() {
        return ColorNameInvariant;
    }

    public void setColorNameInvariant(String colorNameInvariant) {
        ColorNameInvariant = colorNameInvariant;
    }

    public String getColorCultureId() {
        return ColorCultureId;
    }

    public void setColorCultureId(String colorCultureId) {
        ColorCultureId = colorCultureId;
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public String getColorImage() {
        return ColorImage;
    }

    public void setColorImage(String colorImage) {
        ColorImage = colorImage;
    }
}
