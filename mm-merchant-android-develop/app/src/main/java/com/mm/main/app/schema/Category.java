
package com.mm.main.app.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Category implements Serializable{

    private Integer CategoryId;
    private List<Category> CategoryList = new ArrayList<Category>();
    private String CategoryName;
    private Integer ParentCategoryId;
    private String CategoryImage;
    private Integer IsMerchCanSelect;
    private Integer Priority;
    private Integer SkuCount;
    private Integer StatusId;
    private List<CategoryBrandMerchant> CategoryBrandMerchantList;
    private String CategoryNameInvariant;
    private boolean isSelected;




    public boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }


    public Category(String categoryName) {
        CategoryName = categoryName;
    }

    public Category() {
    }

    /**
     * 
     * @return
     *     The CategoryId
     */
    public Integer getCategoryId() {
        return CategoryId;
    }

    /**
     * 
     * @param CategoryId
     *     The CategoryId
     */
    public void setCategoryId(Integer CategoryId) {
        this.CategoryId = CategoryId;
    }

    /**
     * 
     * @return
     *     The CategoryList
     */
    public List<Category> getCategoryList() {
        return CategoryList;
    }

    /**
     * 
     * @param CategoryList
     *     The CategoryList
     */
    public void setCategoryList(List<Category> CategoryList) {
        this.CategoryList = CategoryList;
    }

    /**
     * 
     * @return
     *     The CategoryName
     */
    public String getCategoryName() {
        return CategoryName;
    }

    /**
     * 
     * @param CategoryName
     *     The CategoryName
     */
    public void setCategoryName(String CategoryName) {
        this.CategoryName = CategoryName;
    }

    /**
     * 
     * @return
     *     The ParentCategoryId
     */
    public Integer getParentCategoryId() {
        return ParentCategoryId;
    }

    /**
     * 
     * @param ParentCategoryId
     *     The ParentCategoryId
     */
    public void setParentCategoryId(Integer ParentCategoryId) {
        this.ParentCategoryId = ParentCategoryId;
    }

    public String getCategoryNameInvariant() {
        return CategoryNameInvariant;
    }

    public void setCategoryNameInvariant(String categoryNameInvariant) {
        CategoryNameInvariant = categoryNameInvariant;
    }

    public String getCategoryImage() {
        return CategoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        CategoryImage = categoryImage;
    }

    public Integer getIsMerchCanSelect() {
        return IsMerchCanSelect;
    }

    public void setIsMerchCanSelect(Integer isMerchCanSelect) {
        IsMerchCanSelect = isMerchCanSelect;
    }

    public Integer getPriority() {
        return Priority;
    }

    public void setPriority(Integer priority) {
        Priority = priority;
    }

    public Integer getSkuCount() {
        return SkuCount;
    }

    public void setSkuCount(Integer skuCount) {
        SkuCount = skuCount;
    }

    public Integer getStatusId() {
        return StatusId;
    }

    public void setStatusId(Integer statusId) {
        StatusId = statusId;
    }

    public List<CategoryBrandMerchant> getCategoryBrandMerchantList() {
        return CategoryBrandMerchantList;
    }

    public void setCategoryBrandMerchantList(List<CategoryBrandMerchant> categoryBrandMerchantList) {
        CategoryBrandMerchantList = categoryBrandMerchantList;
    }
}
