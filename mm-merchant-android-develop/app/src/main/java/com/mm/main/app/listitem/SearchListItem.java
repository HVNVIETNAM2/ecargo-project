package com.mm.main.app.listitem;

import com.mm.main.app.schema.response.CompleteResponse;

/**
 * Setting schema
 */
public class SearchListItem {
    String title;
    String content;
    CompleteResponse completeResponse;
    ItemType type;

    public enum ItemType {
        TYPE_HISTORY_TITLE,
        TYPE_HISTORY_LIST,
        TYPE_HOT_ITEM_TITLE,
        TYPE_HOT_ITEM_LIST,
        TYPE_NO_RESULT
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public SearchListItem() {
    }

    public SearchListItem(String title, String content, CompleteResponse completeResponse, ItemType type) {
        this.title = title;
        this.content = content;
        this.completeResponse = completeResponse;
        this.type = type;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public CompleteResponse getCompleteResponse() {
        return completeResponse;
    }

    public void setCompleteResponse(CompleteResponse completeResponse) {
        this.completeResponse = completeResponse;
    }
}
