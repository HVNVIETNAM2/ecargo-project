package com.mm.main.app.schema;

import java.io.Serializable;

/**
 * Created by henrytung on 26/10/15.
 */
public class Country implements Serializable {

    String GeoCountryId;
    String CountryCode;
    String GeoCountryNameInvariant;
    String MobileCode;
    Integer IsStoreFrontCountry;
    String GeoCountryCultureId;
    String CultureCode;
    String GeoCountryName;


    public Country(String countryCode, String cultureCode, String geoCountryCultureId, String geoCountryId, String geoCountryName, String geoCountryNameInvariant) {
        CountryCode = countryCode;
        CultureCode = cultureCode;
        GeoCountryCultureId = geoCountryCultureId;
        GeoCountryId = geoCountryId;
        GeoCountryName = geoCountryName;
        GeoCountryNameInvariant = geoCountryNameInvariant;
    }

    public Country() {
    }

    public String getMobileCode() {
        return MobileCode;
    }

    public void setMobileCode(String mobileCode) {
        MobileCode = mobileCode;
    }

    public Integer getIsStoreFrontCountry() {
        return IsStoreFrontCountry;
    }

    public void setIsStoreFrontCountry(Integer isStoreFrontCountry) {
        IsStoreFrontCountry = isStoreFrontCountry;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getGeoCountryCultureId() {
        return GeoCountryCultureId;
    }

    public void setGeoCountryCultureId(String geoCountryCultureId) {
        GeoCountryCultureId = geoCountryCultureId;
    }

    public String getGeoCountryId() {
        return GeoCountryId;
    }

    public void setGeoCountryId(String geoCountryId) {
        GeoCountryId = geoCountryId;
    }

    public String getGeoCountryName() {
        return GeoCountryName;
    }

    public void setGeoCountryName(String geoCountryName) {
        GeoCountryName = geoCountryName;
    }

    public String getGeoCountryNameInvariant() {
        return GeoCountryNameInvariant;
    }

    public void setGeoCountryNameInvariant(String geoCountryNameInvariant) {
        GeoCountryNameInvariant = geoCountryNameInvariant;
    }

    public String getNameAndCode() {
        return GeoCountryName + " (" + MobileCode + ")";
    }
}
