package com.mm.main.app.schema;

import java.io.Serializable;

/**
 * Created by nhutphan on 2/29/2016.
 */
public class ConfigModel implements Serializable {
    private boolean enableLog;
    private boolean enableDebugMode;

    public boolean isEnableLog() {
        return enableLog;
    }

    public void setEnableLog(boolean enableLog) {
        this.enableLog = enableLog;
    }

    public boolean isEnableDebugMode() {
        return enableDebugMode;
    }

    public void setEnableDebugMode(boolean enableDebugMode) {
        this.enableDebugMode = enableDebugMode;
    }
}
