package com.mm.main.app.activity.merchant.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.merchant.setting.InventorySelectListAdapter;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.InventoryLocation;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.view.MmProgressDialog;

import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import retrofit.Response;
import retrofit.Retrofit;

//import com.mm.merchant.app.facade.SettingServiceFacade;


public class ChangeInventoryActivity extends ActionBarActivity {

    final public static String EXTRA_USER_DETAIL = "USER_DETAIL";

    @Bind(R.id.list)
    ListView listView;

    @BindString(R.string.LB_INVENTORY_LOCATION)
    String title;
    private User user;


    InventorySelectListAdapter inventorySelectListAdapter;
    List<InventoryLocation> inventoryLocations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_inventory);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, title);
        user = (User) this.getIntent().getSerializableExtra(EXTRA_USER_DETAIL);

        listLocationByUser(MmGlobal.getUserId());

    }

    private void listLocationByUser(Integer userId) {
        MmProgressDialog.show(this);
        APIManager.getInstance().getInventoryService().listLocationByUser(userId)
                .enqueue(new MmCallBack<List<InventoryLocation>>(ChangeInventoryActivity.this) {
                    @Override
                    public void onSuccess(Response<List<InventoryLocation>> response, Retrofit retrofit) {
                        inventoryLocations = response.body();
                        inventorySelectListAdapter = new InventorySelectListAdapter(ChangeInventoryActivity.this, inventoryLocations, user.getInventoryLocationId());
                        listView.setAdapter(inventorySelectListAdapter);
                    }
                });
    }

    @OnItemClick(R.id.list)
    public void onItemClicked(int position) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", inventoryLocations.get(position).getInventoryLocationId());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void finish() {
        super.finish();

        overridePendingTransition(R.anim.not_move, R.anim.left_to_right);
    }
}
