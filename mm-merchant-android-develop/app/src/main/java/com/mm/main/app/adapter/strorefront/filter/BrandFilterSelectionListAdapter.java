package com.mm.main.app.adapter.strorefront.filter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Brand;
import com.mm.main.app.utils.PathUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by henrytung on 4/11/15.
 */
public class BrandFilterSelectionListAdapter extends BaseAdapter implements Filterable {

    protected final Activity context;
    protected List<FilterListItem<Brand>> originalData;
    protected List<FilterListItem<Brand>> filteredData;
    private Integer selectedItemId;
    private Drawable rightIcon;

    private ItemFilter mFilter = new ItemFilter();

    public BrandFilterSelectionListAdapter(Activity context, List<FilterListItem<Brand>> itemList, Integer selectedItemId) {
        this.context = context;
        this.originalData = itemList;
        this.filteredData = itemList;
        this.selectedItemId = selectedItemId;
        rightIcon = context.getResources().getDrawable(R.drawable.icon_tick);
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FilterListItem<Brand> currentItem = filteredData.get(position);
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.brand_filter_selection_item, null, true);
        }

        ImageView ivBrandLogo = (ImageView) convertView.findViewById(R.id.ivBrandLogo);
        String url = PathUtil.getBrandImageUrl(currentItem.getT().getHeaderLogoImage());
        Picasso.with(MyApplication.getContext()).load(url).into(ivBrandLogo);

        TextView textView = (TextView) convertView.findViewById(R.id.tvName1);
        textView.setText(currentItem.getT().getBrandNameInvariant());

        TextView textView2 = (TextView) convertView.findViewById(R.id.tvName2);
        textView2.setText(currentItem.getT().getBrandName());

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);

//        ListView lv = (ListView) parent;
        if (currentItem.isSelected()) {
            imageView.setImageDrawable(rightIcon);
        } else {
            imageView.setImageDrawable(null);
        }

        return convertView;
    }

    public Integer getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(Integer selectedItemId) {
        if (selectedItemId >= 0) {
            filteredData.get(selectedItemId).setSelected(!filteredData.get(selectedItemId).isSelected());
        }
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString;
            filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<FilterListItem<Brand>> list = originalData;

            int count = list.size();
            final ArrayList<FilterListItem<Brand>> nlist = new ArrayList<>(count);

            String filterableString ;

            if (!TextUtils.isEmpty(constraint)) {
                for (int i = 0; i < count; i++) {
                    filterableString = list.get(i).getT().getBrandName() + " " + list.get(i).getT().getBrandNameInvariant();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        FilterListItem<Brand> mYourCustomData = list.get(i);
                        nlist.add(mYourCustomData);
                    }
                }
            } else {
                for (int i = 0; i < count; i++) {
                    FilterListItem<Brand> mYourCustomData = list.get(i);
                    nlist.add(mYourCustomData);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<FilterListItem<Brand>>) results.values;
            notifyDataSetChanged();
        }

    }

    public List<FilterListItem<Brand>> getOriginalData() {
        return originalData;
    }

    public void setOriginalData(List<FilterListItem<Brand>> originalData) {
        this.originalData = originalData;
    }

    public List<FilterListItem<Brand>> getFilteredData() {
        return filteredData;
    }

    public void setFilteredData(List<FilterListItem<Brand>> filteredData) {
        this.filteredData = filteredData;
    }
}
