package com.mm.main.app.adapter.strorefront.filter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;

import java.util.List;

/**
 * Created by henrytung on 4/11/15.
 */
public class FilterListAdapter extends BaseAdapter {

    private final Activity context;
    private final List<String> itemList;
    private Integer selectedItemId;
    private Drawable rightIcon;

    public FilterListAdapter(Activity context, List<String> itemList, Integer selectedItemId) {
        this.context = context;
        this.itemList = itemList;
        this.selectedItemId = selectedItemId;
        rightIcon = context.getResources().getDrawable(R.drawable.ico_arrow_close);
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String currentItem = itemList.get(position);
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.simple_selection_list_item, null, true);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.label);
        textView.setText(currentItem);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        imageView.setImageDrawable(rightIcon);

        return convertView;
    }

    public Integer getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(Integer selectedItemId) {
        this.selectedItemId = selectedItemId;
    }
}
