
package com.mm.main.app.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ImageList implements Serializable{

    private List<ImageData> ColorImageList = new ArrayList<>();
    private List<ImageData> DescriptionImageList = new ArrayList<>();
    private List<ImageData> FeaturedImageList = new ArrayList<>();

    /**
     * 
     * @return
     *     The ColorImageList
     */
    public List<ImageData> getColorImageList() {
        return ColorImageList;
    }

    /**
     * 
     * @param ColorImageList
     *     The ColorImageList
     */
    public void setColorImageList(List<ImageData> ColorImageList) {
        this.ColorImageList = ColorImageList;
    }

    /**
     * 
     * @return
     *     The DescriptionImageList
     */
    public List<ImageData> getDescriptionImageList() {
        return DescriptionImageList;
    }

    /**
     * 
     * @param DescriptionImageList
     *     The DescriptionImageList
     */
    public void setDescriptionImageList(List<ImageData> DescriptionImageList) {
        this.DescriptionImageList = DescriptionImageList;
    }

    /**
     * 
     * @return
     *     The FeaturedImageList
     */
    public List<ImageData> getFeaturedImageList() {
        return FeaturedImageList;
    }

    /**
     * 
     * @param FeaturedImageList
     *     The FeaturedImageList
     */
    public void setFeaturedImageList(List<ImageData> FeaturedImageList) {
        this.FeaturedImageList = FeaturedImageList;
    }

}
