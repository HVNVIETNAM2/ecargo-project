package com.mm.main.app.adapter.strorefront.product;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import com.mm.main.app.activity.storefront.product.ProductDetailPageActivity;
import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.temp.TestActivity;
import com.mm.main.app.adapter.strorefront.base.BaseRvAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.BaseRvItem;
import com.mm.main.app.listitem.ProductDetailListItem;
import com.mm.main.app.schema.ColorImage;
import com.mm.main.app.schema.Style;
import com.mm.main.app.utils.FormatUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Setting List Adapter
 */
public class ProductDetailListItemAdapter extends BaseAdapter {

    private final Activity context;
    private final List<ProductDetailListItem> itemList;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    private Bitmap bitmap;

    private Style style;

    private ImageView buttonLike;

    private boolean initColor = false;
    private boolean initComment = false;
    private boolean initRecommend = false;

    public ProductDetailListItemAdapter(Activity context, List<ProductDetailListItem> itemList, Style style) {
        super();
        this.context = context;
        this.itemList = itemList;
        this.style = style;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ProductDetailListItem listItem = itemList.get(position);

        switch (listItem.getType()) {
            case TYPE_IMAGE_DEFAULT:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.image_default_item_view, null, true);
                }

                setupImageDefaultItemView(view);
                break;
            case TYPE_NAME:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.name_item_view, null, true);
                }
                setupNameItemView(view);
                break;
            case TYPE_BUY:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.buy_item_view, null, true);
                    setupBuyItemView(view);
                }
                break;
            case TYPE_SIZE:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.size_item_view, null, true);
                }
                setupSizeItemView(view);
                break;
            case TYPE_COLOR:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.color_item_view, null, true);
                }
                setupColorItemView(view);
                break;
            case TYPE_DESC:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.desc_item_view, null, true);
                }
                setupDescItemView(view);
                break;
            case TYPE_PARAMETER:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.parameter_item_view, null, true);
                }
                setupParameterItemView(view);
                break;
            case TYPE_COMMENT:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.comment_item_view, null, true);
                }
                setupCommentItemView(view);
                break;
            case TYPE_RECOMMEND:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.recommend_item_view, null, true);
                }
                setupRecommendItemView(view);
                break;
            case TYPE_DESC_IMAGE:
                if (view == null) {
                    LayoutInflater inflater = context.getLayoutInflater();
                    view = inflater.inflate(R.layout.description_image_item_view, null, true);
                }
                setupDescriptionImageItemView(view);
                break;
            case TYPE_OUTFIT:
                break;
            case TYPE_SUGGESTION:
                break;
            case TYPE_SCORE:
                break;
        }

//        if (baseListItem.getType() == BaseListItem.ItemType.TYPE_PHOTO) {
//            if (view == null) {
//                LayoutInflater inflater = context.getLayoutInflater();
//                view = inflater.inflate(R.layout.setting_photo_item, null, true);
//            }
//            TextView textView = (TextView) view.findViewById(R.id.photo_title);
//            ImageView imageViewColors = (ImageView) view.findViewById(R.id.setting_image);
//            textView.setText(itemList.get(position).getTitle());
//            if (getBitmap() == null) {
//                Picasso.with(MyApplication.getContext()).load(getItem(position).getContent())
//                        .placeholder(R.drawable.placeholder).into(imageViewColors);
//
//            } else {
//                imageViewColors.setImageBitmap(bitmap);
//            }
//        } else if (baseListItem.getType() == BaseListItem.ItemType.TYPE_TEXT) {
//            if (view == null) {
//                LayoutInflater inflater = context.getLayoutInflater();
//                view = inflater.inflate(R.layout.setting_text_item, null, true);
//            }
//            view = getTextTypeView(position, view, null);
//        } else if (baseListItem.getType() == BaseListItem.ItemType.TYPE_TEXT_ARROW_DOWN) {
//            if (view == null) {
//                LayoutInflater inflater = context.getLayoutInflater();
//                view = inflater.inflate(R.layout.setting_text_item_arrow, null, true);
//            }
//            view = getTextTypeView(position, view, null);
//        } else if (baseListItem.getType() == BaseListItem.ItemType.TYPE_REGULAR_TEXT) {
//            if (view == null) {
//                LayoutInflater inflater = context.getLayoutInflater();
//                view = inflater.inflate(R.layout.setting_regular_text_item, null, true);
//            }
//            view = getTextTypeView(position, view, null);
//        } else if (baseListItem.getType() == BaseListItem.ItemType.TYPE_RED_TEXT) {
//            if (view == null) {
//                LayoutInflater inflater = context.getLayoutInflater();
//                view = inflater.inflate(R.layout.setting_text_item, null, true);
//            }
//            view = getTextTypeView(position, view, context.getResources().getColor(R.color.primary1));
//        } else if (baseListItem.getType() == BaseListItem.ItemType.TYPE_DIVIDER) {
//            if (view == null) {
//                LayoutInflater inflater = context.getLayoutInflater();
//                view = inflater.inflate(R.layout.divider_item, null, true);
//            }
//        }
        return view;
    }

    private void setupBuyItemView(final View view) {

        final TextView tvPriceAfterSwipe = (TextView) view.findViewById(R.id.tvPriceAfterSwipe);
        final TextView tvPriceBeforeSwipe = (TextView) view.findViewById(R.id.tvPriceBeforeSwipe);
        final FrameLayout frameLayoutSwipeButton = (FrameLayout) view.findViewById(R.id.frameLayoutSwipeButton);
        final SlidingDrawer slidingDrawer1 = (SlidingDrawer) view.findViewById(R.id.slidingDrawer1);
        final ImageView handle = (ImageView) view.findViewById(R.id.handle);
        final ImageView content = (ImageView) view.findViewById(R.id.content);
        final ImageView imageView2 = (ImageView) view.findViewById(R.id.imageView2);

        tvPriceBeforeSwipe.setText(FormatUtil.shortFormatAmount(style.getPriceSale()));
        tvPriceAfterSwipe.setText("一扫即买 " + FormatUtil.formatAmount(style.getPriceSale()));

        slidingDrawer1.setOnDrawerScrollListener(new SlidingDrawer.OnDrawerScrollListener() {
            boolean isOpened = false;

            @Override
            public void onScrollStarted() {
                if (!isOpened) {
                    tvPriceBeforeSwipe.setVisibility(View.INVISIBLE);
                    tvPriceAfterSwipe.setVisibility(View.VISIBLE);
                    slidingDrawer1.setBackground(MyApplication.getContext().getResources().getDrawable(R.drawable.icon_swipe_toggle_on_full));
                    handle.setImageResource(R.drawable.icon_swipe_toggle_button);
                    content.setImageResource(R.drawable.icon_swipe_toggle_off);
                    imageView2.setImageResource(R.drawable.icon_swipe_toggle_frame);

                    Animation anim = new ScaleAnimation(
                            0f, 1f, // Start and end values for the X axis scaling
                            1f, 1f, // Start and end values for the Y axis scaling
                            Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                            Animation.RELATIVE_TO_SELF, 0f); // Pivot point of Y scaling
                    anim.setDuration(1000);
                    anim.setFillAfter(true);

                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            Intent intent = new Intent(MyApplication.getContext(), TestActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(ProductDetailPageActivity.EXTRA_PRODUCT_DATA, style);
                            MyApplication.getContext().startActivity(intent);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    frameLayoutSwipeButton.startAnimation(anim);
                    isOpened = true;
                }
            }

            @Override
            public void onScrollEnded() {

            }
        });


        handle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                slidingDrawer1.setBackground(MyApplication.getContext().getResources().getDrawable(R.drawable.icon_swipe_toggle_on_full));
                handle.setImageResource(R.drawable.icon_swipe_toggle_button);
                content.setImageResource(R.drawable.icon_swipe_toggle_off);
                imageView2.setImageResource(R.drawable.icon_swipe_toggle_frame);

            }
        });

    }


    private void setupImageDefaultItemView(View view) {
        ImageView imageViewDefault = (ImageView) view.findViewById(R.id.defaultImage);
        Picasso.with(MyApplication.getContext()).load(PathUtil.getProductImageUrl(style.getImageDefault())).into(imageViewDefault);

        buttonLike = (ImageView) view.findViewById(R.id.buttonLike);
        setupLikeButton();
    }

    private void setupNameItemView(View view) {
        ImageView brandImageView = (ImageView) view.findViewById(R.id.brandImageView);
        TextView textViewSkuName = (TextView) view.findViewById(R.id.textViewSkuName);
        textViewSkuName.setText(style.getSkuName());
    }

    private void setupSizeItemView(View view) {
//        TextView textViewSizeValue = (TextView) view.findViewById(R.id.textViewSizeValue);
    }

    private void setupColorItemView(View view) {
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.horizontal_scroll);
        List<ColorImage> imageDatas = style.getColorImageList();

        if (!initColor) {
            for (int i = 0; i < imageDatas.size(); i++) {
                ColorImage imageData = imageDatas.get(i);
                CircleImageView imageView = new CircleImageView(MyApplication.getContext());
                LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(50, 50);
                int rightPadding = UiUtil.getPixelFromDp(19);
                layoutParams.setMargins(0, 0, rightPadding, 0);
                imageView.setLayoutParams(layoutParams);

                //            ImageView imageViewColors = new ImageView(MyApplication.getContext());
                imageView.setId(i);

//                imageViewColors.setImageBitmap(BitmapFactory.decodeResource(
//                        MyApplication.getContext().getResources(), R.drawable.brand_logo_temp));
                imageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.mm_input_gray));
                imageView.setBorderWidth(UiUtil.getPixelFromDp(1));

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CircleImageView circleImageView = (CircleImageView) v;
                        if (!v.isSelected()) {
                            v.setSelected(true);
                            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.primary1));
                        } else {
                            v.setSelected(false);
                            circleImageView.setBorderColor(MyApplication.getContext().getResources().getColor(R.color.mm_input_gray));
                        }
                    }
                });
                layout.addView(imageView);

                Picasso.with(MyApplication.getContext()).load(PathUtil.getProductImageUrl(imageData.getImageKey())).into(imageView);
                initColor = true;
            }
        }
    }

    private void setupDescItemView(View view) {
        TextView textViewDesc = (TextView) view.findViewById(R.id.textViewDesc);
        textViewDesc.setText(style.getSkuName());
    }

    private void setupParameterItemView(View view) {
        TextView textViewParameterValue = (TextView) view.findViewById(R.id.textViewParameterValue);
    }

    private void setupCommentItemView(View view) {
        TextView tvCommentNumber = (TextView) view.findViewById(R.id.tvCommentNumber);
        tvCommentNumber.setText("(112"+
        MyApplication.getContext().getResources().getString(R.string.LB_CA_COMMENT)+")");

        LinearLayout linearLayoutCommentImage = (LinearLayout) view.findViewById(R.id.linearLayoutCommentImage);

        if (!initComment) {
            for (int i = 0; i < 2; i++) {
                CircleImageView imageView = new CircleImageView(MyApplication.getContext());
                LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(45, 70);
                int rightPadding = UiUtil.getPixelFromDp(5);
                layoutParams.setMargins(0, 0, rightPadding, 0);
                imageView.setLayoutParams(layoutParams);
                imageView.setId(i);
//                imageViewColors.setImageBitmap(BitmapFactory.decodeResource(
//                        MyApplication.getContext().getResources(), R.drawable.brand_logo_temp));
                imageView.setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.brand_logo_temp));
                linearLayoutCommentImage.addView(imageView);
            }
            initComment = true;
        }
    }

    private void setupRecommendItemView(View view) {
        TextView textViewRecommendNumber = (TextView) view.findViewById(R.id.textViewRecommendNumber);
        textViewRecommendNumber.setText("116"+
                MyApplication.getContext().getResources().getString(R.string.LB_CA_NUM_PPL_RCMD_ITEM));

        LinearLayout linearLayoutRecommendImage = (LinearLayout) view.findViewById(R.id.linearLayoutRecommendImage);

        if (!initRecommend) {
            for (int i = 0; i < 7; i++) {
                ImageView imageView = new ImageView(MyApplication.getContext());
                LinearLayout.LayoutParams layoutParams = UiUtil.getLinearLayoutLayoutParams(30, 30);
                int rightPadding = UiUtil.getPixelFromDp(5);
                layoutParams.setMargins(0, 0, rightPadding, 0);
                imageView.setLayoutParams(layoutParams);
                imageView.setId(i);
//                imageViewColors.setImageBitmap(BitmapFactory.decodeResource(
//                        MyApplication.getContext().getResources(), R.drawable.placeholder));
                imageView.setImageDrawable(MyApplication.getContext().getResources().getDrawable(R.drawable.placeholder));
                linearLayoutRecommendImage.addView(imageView);
            }
            initRecommend = true;
        }
    }

    private void setupDescriptionImageItemView(View view) {
        // 1. get a reference to recyclerView
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rvDescriptionImage);

        BaseRvItem itemsData[] = new BaseRvItem[style.getColorImageList().size()];

        for (int i = 0; i < style.getColorImageList().size(); i++) {
            ColorImage imageData = style.getColorImageList().get(i);
            itemsData[i] = new BaseRvItem(imageData.getColorKey(), imageData.getImageKey());

        }

        recyclerView.setLayoutManager(new LinearLayoutManager(MyApplication.getContext()));
        BaseRvAdapter mAdapter = new BaseRvAdapter(itemsData);
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType().ordinal();
    }

    @Override
    public int getViewTypeCount() {
        return ProductDetailListItem.ItemType.values().length;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public ProductDetailListItem getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void setupLikeButton() {
        if (buttonLike != null) {
            buttonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setSelected(!v.isSelected());
                }
            });
        }
    }

}
