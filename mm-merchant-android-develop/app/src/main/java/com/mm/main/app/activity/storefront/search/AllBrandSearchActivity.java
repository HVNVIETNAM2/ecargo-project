package com.mm.main.app.activity.storefront.search;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.filter.BrandFilterSelectionListAdapter;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.record.SearchStyle;
import com.mm.main.app.schema.Brand;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.StringUtil;
import com.mm.main.app.view.MmSearchBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import retrofit.Response;
import retrofit.Retrofit;

public class AllBrandSearchActivity extends AppCompatActivity implements MmSearchBar.MmSearchBarListener {

    @Bind(R.id.listViewFilter)
    ListView listViewFilter;

    @Bind(R.id.searchView)
    MmSearchBar searchView;
    List<FilterListItem<Brand>> filterList;

    @Bind(R.id.textViewHeader)
    TextView textViewHeader;

    BrandFilterSelectionListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeNoActionBar);
        setContentView(R.layout.activity_all_brand_search);
        ButterKnife.bind(this);
        searchView.setMmSearchBarListener(this);
        setupToolbar();

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        textViewHeader.setText(StringUtil.getResourceString("LB_CA_ALL_BRAND"));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        fetchBrand();
    }

    private void fetchBrand() {
        APIManager.getInstance().getSearchService().brand()
                .enqueue(new MmCallBack<List<Brand>>(this) {
                    @Override
                    public void onSuccess(Response<List<Brand>> response, Retrofit retrofit) {
                        List<Brand> brands = response.body();
                        filterList = new ArrayList<>();

                        for (int i = 0; i < brands.size(); i++) {
                            if (!brands.get(i).getBrandName().equals("---")) {
                                filterList.add(new FilterListItem<>(brands.get(i)));
                            }
                        }

                        renderFilterList(filterList);
                    }
                });
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);
    }

    private void renderFilterList(List<FilterListItem<Brand>> filters) {
        filterList = filters;
        listAdapter = new BrandFilterSelectionListAdapter(this, filterList, -1);
        listViewFilter.setAdapter(listAdapter);
        listViewFilter.setTextFilterEnabled(false);
        listViewFilter.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }

    @OnItemClick(R.id.listViewFilter)
    public void onItemClicked(int position) {
//        listViewFilter.setItemChecked(position, listViewFilter.isItemChecked(position));
//        listAdapter.setSelectedItemId(position);
        SearchStyle.getInstance().clearAllFilter();
        List<Brand> brandid = new ArrayList<>();
        brandid.add(listAdapter.getFilteredData().get(position).getT());
        SearchStyle.getInstance().setBrandid(brandid);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onEnterText(CharSequence s) {
        if (TextUtils.isEmpty(s)) {
            listViewFilter.clearTextFilter();
        } else {
            listViewFilter.setFilterText(s.toString());
        }
        listAdapter.getFilter().filter(s);
    }

    @Override
    public void onCancelSearch() {
        listViewFilter.clearTextFilter();
    }
}
