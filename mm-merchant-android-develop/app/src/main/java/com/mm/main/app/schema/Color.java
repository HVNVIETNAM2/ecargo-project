
package com.mm.main.app.schema;

import java.io.Serializable;

public class Color implements Serializable{

    private Integer ColorId;
    private String ColorNameInvariant;
    private String ColorCode;
    private String HexCode;
    private Integer ColorCultureId;
    private String CultureCode;
    private String ColorName;
    private String ColorImage;

    /**
     *
     * @return
     *     The ColorId
     */
    public Integer getColorId() {
        return ColorId;
    }

    /**
     *
     * @param ColorId
     *     The ColorId
     */
    public void setColorId(Integer ColorId) {
        this.ColorId = ColorId;
    }

    /**
     *
     * @return
     *     The ColorNameInvariant
     */
    public String getColorNameInvariant() {
        return ColorNameInvariant;
    }

    /**
     *
     * @param ColorNameInvariant
     *     The ColorNameInvariant
     */
    public void setColorNameInvariant(String ColorNameInvariant) {
        this.ColorNameInvariant = ColorNameInvariant;
    }

    /**
     *
     * @return
     *     The ColorCode
     */
    public String getColorCode() {
        return ColorCode;
    }

    /**
     *
     * @param ColorCode
     *     The ColorCode
     */
    public void setColorCode(String ColorCode) {
        this.ColorCode = ColorCode;
    }

    /**
     *
     * @return
     *     The HexCode
     */
    public String getHexCode() {
        return HexCode;
    }

    /**
     *
     * @param HexCode
     *     The HexCode
     */
    public void setHexCode(String HexCode) {
        this.HexCode = HexCode;
    }

    /**
     *
     * @return
     *     The ColorCultureId
     */
    public Integer getColorCultureId() {
        return ColorCultureId;
    }

    /**
     *
     * @param ColorCultureId
     *     The ColorCultureId
     */
    public void setColorCultureId(Integer ColorCultureId) {
        this.ColorCultureId = ColorCultureId;
    }

    /**
     *
     * @return
     *     The CultureCode
     */
    public String getCultureCode() {
        return CultureCode;
    }

    /**
     *
     * @param CultureCode
     *     The CultureCode
     */
    public void setCultureCode(String CultureCode) {
        this.CultureCode = CultureCode;
    }

    /**
     *
     * @return
     *     The ColorName
     */
    public String getColorName() {
        return ColorName;
    }

    /**
     *
     * @param ColorName
     *     The ColorName
     */
    public void setColorName(String ColorName) {
        this.ColorName = ColorName;
    }

    /**
     *
     * @return
     *     The ColorImage
     */
    public String getColorImage() {
        return ColorImage;
    }

    /**
     *
     * @param ColorImage
     *     The ColorImage
     */
    public void setColorImage(String ColorImage) {
        this.ColorImage = ColorImage;
    }


}
