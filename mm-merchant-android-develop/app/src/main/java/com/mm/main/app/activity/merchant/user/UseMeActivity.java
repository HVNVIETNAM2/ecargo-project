package com.mm.main.app.activity.merchant.user;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mm.main.app.R;
import com.mm.main.app.blurbehind.BlurBehind;

public class UseMeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_use_me);

        BlurBehind.getInstance().setBackground(this);

    }
}
