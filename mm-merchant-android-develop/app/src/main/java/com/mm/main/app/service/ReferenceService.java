package com.mm.main.app.service;

import com.mm.main.app.schema.LanguageList;
import com.mm.main.app.schema.MobileCodeList;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by andrew on 30/9/15.
 */
public interface ReferenceService {

    String REF_PATH = "reference";

    @GET(REF_PATH + "/changelanguage")
    Call<LanguageList> listLanguage();

    @GET(REF_PATH + "/general")
    Call<MobileCodeList> listMobileCode();

}
