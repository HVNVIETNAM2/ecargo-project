package com.mm.main.app.manager;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.schema.Cart;
import com.mm.main.app.schema.CartItem;
import com.mm.main.app.schema.Style;
import com.mm.main.app.schema.WishList;
import com.mm.main.app.utils.MmCallBack;

import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by thienchaud on 29-Jan-16.
 * This is for saving ProductCart and Wishlist
 */
public class CacheManager {

    private static CacheManager singleton;

    private Cart cart;
    private WishList wishlist;

    public static CacheManager getInstance() {
        if (singleton == null) {
            singleton = new CacheManager();
        }
        return singleton;
    }

    private CacheManager() {
        cart = new Cart();
        wishlist = new WishList();
    }

    public void updateAllCache() {
        updateCartCache();
        updateWishlistCache();
    }

    public void updateCartCache() {
        if (MmGlobal.isLogin()) {
            APIManager.getInstance().getCartService().viewCartByUserKey(MmGlobal.getUserKey())
                    .enqueue(new MmCallBack<Cart>(MyApplication.getContext()) {
                        @Override
                        public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            super.onFailure(t);
                        }

                        @Override
                        public void onResponse(Response<Cart> response, Retrofit retrofit) {
                            if (response.body() != null) {
                                cart = response.body();
                            }
                        }
                    });
        } else {
            String cartKey = MmGlobal.anonymousShoppingCartKey();
            if (cartKey.isEmpty()) {
                return;
            }
            APIManager.getInstance().getCartService().viewCartByCartKey(cartKey)
                    .enqueue(new MmCallBack<Cart>(MyApplication.getContext()) {
                        @Override
                        public void onSuccess(Response<Cart> response, Retrofit retrofit) {
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            super.onFailure(t);
                        }

                        @Override
                        public void onResponse(Response<Cart> response, Retrofit retrofit) {
                            if (response.body() != null) {
                                cart = response.body();
                            }
                        }
                    });
        }
    }

    public void updateWishlistCache() {
        if (MmGlobal.isLogin()) {
            APIManager.getInstance().getWishListService().viewWishListByUser(MmGlobal.getUserKey())
                    .enqueue(new MmCallBack<WishList>(MyApplication.getContext()) {
                        @Override
                        public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            super.onFailure(t);
                        }

                        @Override
                        public void onResponse(Response<WishList> response, Retrofit retrofit) {
                            if (response.body() != null) {
                                wishlist = response.body();
                            }
                        }
                    });
        } else {
            String wishlistKey = MmGlobal.anonymousWishListKey();
            if (wishlistKey.isEmpty()) {
                return;
            }
            APIManager.getInstance().getWishListService().viewWishList(wishlistKey)
                    .enqueue(new MmCallBack<WishList>(MyApplication.getContext()) {
                        @Override
                        public void onSuccess(Response<WishList> response, Retrofit retrofit) {
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            super.onFailure(t);
                        }

                        @Override
                        public void onResponse(Response<WishList> response, Retrofit retrofit) {
                            if (response.body() != null) {
                                wishlist = response.body();
                            }
                        }
                    });
        }
    }

    public CartItem wishlistItemForStyle(Style style) {
        if (wishlist == null) {
            return null;
        }
        for (int i = 0; i < wishlist.getCartItems().size(); i++) {
            if (style.getStyleCode().equals(wishlist.getCartItems().get(i).getStyleCode())) {
                return wishlist.getCartItems().get(i);
            }
        }
        return null;
    }

    public boolean isInShoppingCart(Style style) {
        return false;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public WishList getWishlist() {
        return wishlist;
    }

    public void setWishlist(WishList wishlist) {
        this.wishlist = wishlist;
    }

    public boolean isCartHasItem() {
        return (cart.getMerchantList().size() > 0);
    }

    public boolean isWishlistHasItem() {
        return (wishlist.getCartItems().size() > 0);
    }
}
