package com.mm.main.app.listitem;

/**
 * Created by henrytung on 2/12/2015.
 */
public class BaseRvItem {
    private String title;
    private String value;

    public BaseRvItem(String title, String value) {
        this.title = title;
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
