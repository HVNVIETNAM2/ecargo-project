package com.mm.main.app.adapter.strorefront.filter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Size;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by henrytung on 4/11/15.
 */
public class FilterSelectionListAdapter extends BaseAdapter implements Filterable {

    private final Activity context;
    private List<FilterListItem<Size>> originalData;
    private List<FilterListItem<Size>> filteredData;
    private Integer selectedItemId;
    private Drawable rightIcon;

    private ItemFilter mFilter = new ItemFilter();

    public FilterSelectionListAdapter(Activity context, List<FilterListItem<Size>> itemList, Integer selectedItemId) {
        this.context = context;
        this.originalData = itemList;
        this.filteredData = itemList;
        this.selectedItemId = selectedItemId;
        rightIcon = context.getResources().getDrawable(R.drawable.icon_tick);
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FilterListItem<Size> currentItem = filteredData.get(position);
        if (convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.simple_selection_list_item, null, true);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.label);
        textView.setText(currentItem.getT().getSizeName());

        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);

//        ListView lv = (ListView) parent;
        if (currentItem.isSelected()) {
            imageView.setImageDrawable(rightIcon);
        } else {
            imageView.setImageDrawable(null);
        }

        return convertView;
    }

    public Integer getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(Integer selectedItemId) {
        if (selectedItemId >= 0) {
            filteredData.get(selectedItemId).setSelected(!filteredData.get(selectedItemId).isSelected());
        }
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString;
            filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<FilterListItem<Size>> list = originalData;

            int count = list.size();
            final ArrayList<FilterListItem<Size>> nlist = new ArrayList<>(count);

            String filterableString ;

            if (!TextUtils.isEmpty(constraint)) {
                for (int i = 0; i < count; i++) {
                    filterableString = "" + list.get(i).getT();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        FilterListItem<Size> mYourCustomData = list.get(i);
                        nlist.add(mYourCustomData);
                    }
                }
            } else {
                for (int i = 0; i < count; i++) {
                    FilterListItem<Size> mYourCustomData = list.get(i);
                    nlist.add(mYourCustomData);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<FilterListItem<Size>>) results.values;
            notifyDataSetChanged();
        }

    }

    public List<FilterListItem<Size>> getOriginalData() {
        return originalData;
    }

    public void setOriginalData(List<FilterListItem<Size>> originalData) {
        this.originalData = originalData;
    }
}
