package com.mm.main.app.utils;

import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by henrytung on 17/11/2015.
 */
public class SearchViewUtil {

    public static void setSearchHintIcon(SearchView searchView, int resourceId) {
        int searchImgId = android.support.v7.appcompat.R.id.search_button; // I used the explicit layout ID of searchview's ImageView
        setIconIntoImageView(searchView, searchImgId, resourceId);
    }

    public static void setSearchVoiceIcon(SearchView searchView, int resourceId) {
        int searchImgId = android.support.v7.appcompat.R.id.search_voice_btn; // I used the explicit layout ID of searchview's ImageView
        setIconIntoImageView(searchView, searchImgId, resourceId);
    }

    public static void setSearchGoIcon(SearchView searchView, int resourceId) {
        int searchImgId = android.support.v7.appcompat.R.id.search_go_btn; // I used the explicit layout ID of searchview's ImageView
        setIconIntoImageView(searchView, searchImgId, resourceId);
    }

    public static void setSearchBackIcon(SearchView searchView, int resourceId) {
        int searchImgId = android.support.v7.appcompat.R.id.search_mag_icon; // I used the explicit layout ID of searchview's ImageView
        setIconIntoImageView(searchView, searchImgId, resourceId);
    }

    public static void setSearchHintCloseIcon(SearchView searchView, int resourceId) {
        int searchImgId = android.support.v7.appcompat.R.id.search_close_btn; // I used the explicit layout ID of searchview's ImageView
        setIconIntoImageView(searchView, searchImgId, resourceId);
    }



    private static void setIconIntoImageView(View view, int searchImgId, int resourceId) {
        ImageView v = (ImageView) view.findViewById(searchImgId);
        v.setImageResource(resourceId);
    }

    public static void setText(SearchView searchView, int color, int size, String hint) {
        int searchImgId = android.support.v7.appcompat.R.id.search_src_text;
        SearchView.SearchAutoComplete theTextArea = (SearchView.SearchAutoComplete)searchView.findViewById(searchImgId);
        theTextArea.setTextColor(color);
        theTextArea.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        if (!TextUtils.isEmpty(hint)) {
            theTextArea.setHint(hint);
        }
    }

}
