package com.mm.main.app.listitem;


import com.mm.main.app.schema.CartItem;

/**
 * Created by ductranvt on 1/15/2016.
 */
public class CheckoutConfirmProductRvItem extends CartItem implements CheckoutConfirmRvItem {
    private CartItem product;
    private CheckoutConfirmMerchantRvItem merchantRvItem;

    public CheckoutConfirmProductRvItem(CartItem product, CheckoutConfirmMerchantRvItem merchant) {
        this.product = product;
        this.merchantRvItem = merchant;
    }

    public CartItem getProduct() {
        return product;
    }

    public ItemType getType() {
        return ItemType.TYPE_PRODUCT;
    }

    public CheckoutConfirmRvItem getMerchantRvItem() {
        return merchantRvItem;
    }
}
