package com.mm.main.app.activity.merchant.login;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.gson.Gson;
import com.mm.main.app.R;
import com.mm.main.app.activity.merchant.setting.ResetPasswordActivity;
import com.mm.main.app.activity.merchant.user.UserActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.factory.ValidatorFactory;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.LifeCycleManager;
import com.mm.main.app.schema.AppError;
import com.mm.main.app.schema.request.ForgotPasswordRequest;
import com.mm.main.app.schema.response.LoginResponse;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.umeng.analytics.MobclickAgent;

import java.util.concurrent.Callable;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class LoginActivity extends ActionBarActivity {
    public static final String TAG = "Login Activity: ";
    @Bind(R.id.loginActionButton)
    Button loginActionButton;
    @Bind(R.id.userNameEditText)
    EditText userNameEditText;
    @Bind(R.id.passwordEditText)
    EditText passwordEditText;
    @Bind(R.id.rememberCheckBox)
    CheckBox rememberCheckBox;

    @Bind(R.id.inputUserNameLayout)
    TextInputLayout inputUserNameLayout;
    @Bind(R.id.inputPasswordLayout)
    TextInputLayout inputPasswordLayout;

    @BindString(R.string.LB_LOGIN)
    String title;
    private String userName, password;

    final private static int FORGOT_PASSWORD_REQUEST = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, title);
        loginActionButton.setTextColor(Color.WHITE);
        userNameEditText.setText(MmGlobal.getUserName());

        setupValidation();
    }

    public void setupValidation() {
        userNameEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateUserName();
            }
        }, inputUserNameLayout));

        passwordEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validatePassword();
            }
        }, inputPasswordLayout));
    }

    @OnClick(R.id.loginActionButton)
    public void login() {

        if (validateUserName() & validatePassword()) {

            loginActionButton.setEnabled(false);
            userName = userNameEditText.getText().toString().trim();
            password = passwordEditText.getText().toString().trim();
            MmProgressDialog.show(this);
            APIManager.getInstance().getAuthService().login(userName, password)
                    .enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                            MmProgressDialog.dismiss();
                            if (response.isSuccess()) {
                                LifeCycleManager.login(response, userName, rememberCheckBox.isChecked());
                                viewUser();
                            } else {
                                loginActionButton.setEnabled(true);
                                Gson gson = new Gson();
                                AppError myError = null;
                                String translatedErrorString = "";
                                try {
                                    myError = gson.fromJson(response.errorBody().string(), AppError.class);
                                    translatedErrorString = myError.getAppCode();
                                    if (MyApplication.getContext().getResources().getIdentifier(myError.getAppCode(), "string", MyApplication.getContext().getPackageName()) != 0) {
                                        translatedErrorString = MyApplication.getContext().getResources().getString(MyApplication.getContext().getResources().getIdentifier(myError.getAppCode(), "string", MyApplication.getContext().getPackageName()));
                                    }

                                    if (myError.getAppCode().equals("MSG_ERR_USER_AUTHENTICATION_INVALID")) {
                                        translatedErrorString = translatedErrorString.replace(Constant.TEXT_REPLACE_VALUE_0, String.valueOf(3 - myError.getLoginAttempts()));
                                    }
                                } catch (Exception e) {
                                    Logger.d(TAG, e.getMessage());

                                }

                                ErrorUtil.showErrorDialog(LoginActivity.this, null, translatedErrorString);
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            MmProgressDialog.dismiss();
                            loginActionButton.setEnabled(true);
                            ErrorUtil.alert(LoginActivity.this);
                            t.printStackTrace();
                        }
                    });
        }
    }

    @OnClick(R.id.forgotPasswordTextView)
    public void proceedForgotPassword() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.dialog_forget_password, null));
        final AlertDialog dialog = builder.create();

        dialog.show();

        final EditText userName = (EditText) (dialog.findViewById(R.id.userNameEditText));
        Button buttonCancel = (Button) (dialog.findViewById(R.id.buttonCancel));
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button buttonOk = (Button) (dialog.findViewById(R.id.buttonOk));
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                MmProgressDialog.show(LoginActivity.this);
                APIManager.getInstance().getAuthService()
                        .forgotPassword(new ForgotPasswordRequest(userName.getText().toString()))
                        .enqueue(new MmCallBack<Boolean>(LoginActivity.this) {
                            @Override
                            public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                                Intent intent = new Intent(LoginActivity.this, ResetPasswordActivity.class);
                                intent.putExtra(ResetPasswordActivity.EXTRA_INPUT_USERNAME, userName.getText().toString());
                                startActivityForResult(intent, FORGOT_PASSWORD_REQUEST);
                            }
                        });

            }
        });
    }

    private void viewUser() {
        Intent intent = new Intent(LoginActivity.this, UserActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    private boolean validateUserName() {
        String error = null;
        String fieldName = getResources().getString(R.string.LB_EMAIL_OR_MOBILE);
        String userName = userNameEditText.getText().toString();

        if (!ValidationUtil.isValidEmail(userName) && !ValidationUtil.isValidPhone(userName)) {
            error = getResources().getString(R.string.MSG_ERR_FIELDNAME_PATTERN)
                    .replace(Constant.TEXT_REPLACE_FIELD_NAME, fieldName);
        }

        if (ValidationUtil.isEmpty(userName)) {
            error = getResources().getString(R.string.MSG_ERR_EMAIL_OR_MOBILE_NIL);
        }

        ValidationUtil.setErrorMessage(inputUserNameLayout, error);

        return (error == null);
    }


    private boolean validatePassword() {
        String error = null;
        String fieldName = getResources().getString(R.string.LB_PASSWORD);
        String password = passwordEditText.getText().toString();

        if (!ValidationUtil.validate(ValidationUtil.PASSWORD_REGEX, password)) {
            error = getResources().getString(R.string.MSG_ERR_FIELDNAME_PATTERN)
                    .replace(Constant.TEXT_REPLACE_FIELD_NAME, fieldName);
        }
        if (password.trim().isEmpty()) {
            error = getResources().getString(R.string.MSG_ERR_PASSWORD_NIL);
        }

        ValidationUtil.setErrorMessage(inputPasswordLayout, error);

        return error == null;
    }

}
