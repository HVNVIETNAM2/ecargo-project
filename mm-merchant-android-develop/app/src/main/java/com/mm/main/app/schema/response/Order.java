package com.mm.main.app.schema.response;

/**
 * Created by ductranvt on 3/11/2016.
 */
public class Order {
    String OrderKey;
    Boolean IsCrossBorder = false;
    Boolean IsUserIdentificationExists = false;

    public String getOrderKey() {
        return OrderKey;
    }

    public Boolean getIsCrossBorder() {
        return IsCrossBorder;
    }

    public Boolean getIsUserIdentificationExists() {
        return IsUserIdentificationExists;
    }
}
