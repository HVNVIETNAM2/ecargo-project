package com.mm.main.app.adapter.strorefront.base;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.BaseRvItem;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

/**
 * Created by henrytung on 2/12/2015.
 */
public class BaseRvAdapter extends RecyclerView.Adapter<BaseRvAdapter.ViewHolder> {

    private BaseRvItem[] itemsData;

    public BaseRvAdapter(BaseRvItem[] itemsData) {
        this.itemsData = itemsData;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BaseRvAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                       int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_item, viewGroup, false);

        // create CloseViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        Picasso.with(MyApplication.getContext()).load(PathUtil.getProductImageUrl(itemsData[position].getValue())).into(viewHolder.imgView);
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgView;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            imgView = (ImageView) itemLayoutView.findViewById(R.id.imageView);

            int screenWidth = UiUtil.getDisplayWidth();
            imgView.setLayoutParams(new RelativeLayout.LayoutParams(screenWidth, UiUtil.getAspectHeightOfDescImage(screenWidth)));
        }
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.length;
    }
}
