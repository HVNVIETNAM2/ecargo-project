package com.mm.main.app.schema.request;

/**
 * Created by andrew on 10/11/15.
 */
public class ValidateRequest {
    public String getActivationToken() {
        return ActivationToken;
    }

    public void setActivationToken(String activationToken) {
        ActivationToken = activationToken;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public ValidateRequest(String activationToken, String userKey) {

        ActivationToken = activationToken;
        UserKey = userKey;
    }

    public ValidateRequest() {

    }

    String ActivationToken;
    String UserKey;

}
