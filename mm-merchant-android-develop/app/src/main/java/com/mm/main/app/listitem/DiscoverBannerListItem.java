package com.mm.main.app.listitem;

import com.mm.main.app.schema.BrandUnionMerchant;

/**
 * Created by henrytung on 17/12/2015.
 */
public class DiscoverBannerListItem {
    String key;
    BrandUnionMerchant brandUnionMerchant;

    public DiscoverBannerListItem(String key, BrandUnionMerchant brandUnionMerchant) {
        this.key = key;
        this.brandUnionMerchant = brandUnionMerchant;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public BrandUnionMerchant getBrandUnionMerchant() {
        return brandUnionMerchant;
    }

    public void setBrandUnionMerchant(BrandUnionMerchant brandUnionMerchant) {
        this.brandUnionMerchant = brandUnionMerchant;
    }
}
