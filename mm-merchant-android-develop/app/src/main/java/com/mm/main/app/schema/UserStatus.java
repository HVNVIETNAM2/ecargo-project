package com.mm.main.app.schema;

/**
 * User class for both Retrofit server exchange schema, and ORM model for Active Android
 */

public class UserStatus {

     String UserId;

     String Status;

    public UserStatus() {
    }

    public UserStatus(String userId, String status) {
        UserId = userId;
        Status = status;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
