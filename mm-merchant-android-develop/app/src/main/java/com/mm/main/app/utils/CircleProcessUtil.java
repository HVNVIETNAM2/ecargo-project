package com.mm.main.app.utils;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.mm.main.app.R;

/**
 * Created by haivu on 2/5/2016.
 */
public class CircleProcessUtil extends View {

    private int WIDTH = 20;
    private final int TIME_ANIMATOR = 600;
    public static float WIDTH_PERCENT = 0.75f;
    public static final float CENTER_PERCENT = 0.50f;
    private int color = -1;
    private int startAngle = -90;

    private int percent;

    public CircleProcessUtil(Context context) {
        super(context);
    }

    public CircleProcessUtil(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircleProcessUtil(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setWidth(int width) {
        WIDTH = width;
    }

    public void setWidthPercent(int widthPercent) {
        WIDTH_PERCENT = widthPercent;
    }

    public void setColor(int color){
        this.color = color;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = getWidth();
        int y = getHeight();
        int widthDp = UiUtil.getPixelFromDp(WIDTH);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(widthDp);
        //paint.setColor(getResources().getColor(R.color.curators_circle_bg));
        paint.setColor(getResources().getColor(R.color.transparent));

        int angle = (int) (((float) percent / 100) * 360);
        RectF rect = new RectF( Math.max(widthDp, x * (1 - WIDTH_PERCENT) / 2),
                Math.max(widthDp,(y * CENTER_PERCENT) - (x * WIDTH_PERCENT) / 2),
                Math.min(x * (1 - WIDTH_PERCENT) / 2 + x * WIDTH_PERCENT , x -widthDp),
                Math.min(x - widthDp, y * CENTER_PERCENT + (x * WIDTH_PERCENT) / 2));

        RectF rectOut = new RectF(
                Math.max(widthDp, x * (1 - WIDTH_PERCENT) / 2) - widthDp,
                Math.max(widthDp, (y * CENTER_PERCENT) - (x * WIDTH_PERCENT) / 2) - widthDp,
                Math.min(x - widthDp, x * (1 - WIDTH_PERCENT) / 2 + x * WIDTH_PERCENT) + widthDp,
                Math.min(x - widthDp, (y * CENTER_PERCENT) + (x * WIDTH_PERCENT) / 2) + widthDp);

        canvas.drawArc(rect, 0, 360, false, paint);

        Paint paint2 = new Paint();
        paint2.setStyle(Paint.Style.STROKE);
        paint2.setStrokeWidth(widthDp);   //paint2.setStrokeJoin(Paint.Join.ROUND);
        paint2.setStrokeCap(Paint.Cap.ROUND);
        paint2.setAntiAlias(true);
        if(color == -1) {
            paint2.setColor(getResources().getColor(R.color.curators_circle_percent));
        }else{
            paint2.setColor(getResources().getColor(color));
        }

        Path path = new Path();
        path.arcTo(rect, startAngle, (float) angle);

        path.arcTo(rectOut, (float) angle + startAngle, -(float) angle);


        canvas.drawArc(rect, startAngle, (float) angle, false, paint2);

    }

    public void setPercent(int percent) {
        setAnimation(percent);
    }

    public void setPercentNoAnim(int percent) {

        this.percent = percent;
        invalidate();
    }

    public void setPercentAnim(int percentIn, int offset) {
        startAngle = -90 + offset;

       setAnimation(percentIn);

    }

    private void setAnimation(int value) {
        ValueAnimator animator = ValueAnimator.ofInt(percent, value);
        animator.setDuration(TIME_ANIMATOR);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                percent = (int) animation.getAnimatedValue();
                invalidate();
            }
        });
        animator.start();
    }

}
