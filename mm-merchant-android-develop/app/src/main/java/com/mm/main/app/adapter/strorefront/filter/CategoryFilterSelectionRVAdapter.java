package com.mm.main.app.adapter.strorefront.filter;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Category;
import com.mm.main.app.utils.UiUtil;

import java.util.ArrayList;


/**
 * Created by nguyennguyent1 on 2/15/2016.
 */
public class CategoryFilterSelectionRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private final Activity context;
    private ArrayList<FilterListItem> originalData;
    private ArrayList<FilterListItem> filteredData;
    private Integer selectedItemId;
    int sumSpan = 40;

    private ItemFilter mFilter = new ItemFilter();

    public CategoryFilterSelectionRVAdapter(Activity context, ArrayList<FilterListItem> itemList, Integer selectedItemId) {
        this.context = context;
        this.originalData = itemList;
        this.filteredData = itemList;
        this.selectedItemId = selectedItemId;
        calculateSpan();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        FilterListItem.ItemType itemType = FilterListItem.ItemType.values()[viewType];
        RecyclerView.ViewHolder viewHolder = null;

        if (itemType == FilterListItem.ItemType.TYPE_FILTER_TITLE) {
            viewHolder = new CategoryFilterTitleViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.category_filter_header_item, parent, false));
        } else {
            viewHolder = new CategoryFilterViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.category_simple_selection_list_item, parent, false));
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        final FilterListItem currentItem = filteredData.get(position);

        switch (currentItem.getType()) {
            case TYPE_FILTER_LIST:
                final CategoryFilterViewHolder categoryFilterViewHolder = (CategoryFilterViewHolder) viewHolder;

                ViewGroup.MarginLayoutParams lpt =(ViewGroup.MarginLayoutParams)categoryFilterViewHolder.backgroundGroup.getLayoutParams();

                categoryFilterViewHolder.textView.setText(((Category) currentItem.getT()).getCategoryName());

                int left = UiUtil.getPixelFromDp(5), right = UiUtil.getPixelFromDp(5);
                if(currentItem.getIsStartLine()){
                    left = UiUtil.getPixelFromDp(20);
                }

                if(currentItem.getIsEndLine()){
                    right = UiUtil.getPixelFromDp(20);
                }

                lpt.setMargins(left, 0, right, 0);

                categoryFilterViewHolder.backgroundGroup.setLayoutParams(lpt);
                setBorderColor(((Category)currentItem.getT()).getIsSelected(), categoryFilterViewHolder);

                categoryFilterViewHolder.labelGroup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((Category)currentItem.getT()).setIsSelected(!((Category) currentItem.getT()).getIsSelected());
                        setBorderColor(((Category) currentItem.getT()).getIsSelected(), categoryFilterViewHolder);
                    }
                });
                break;

            case TYPE_FILTER_TITLE:
                CategoryFilterTitleViewHolder categoryFilterTitleViewHolder = (CategoryFilterTitleViewHolder) viewHolder;
                categoryFilterTitleViewHolder.textView.setText(currentItem.getT().toString());
                categoryFilterTitleViewHolder.textView.setPadding(0, 0, 0, 0);
                break;
        }
    }

    private void setBorderColor(boolean isSelected, CategoryFilterViewHolder viewHolder) {
        if (isSelected) {
            viewHolder.labelGroup.setBackground(
                    MyApplication.getContext().getResources().getDrawable(R.drawable.filter_selection_background));
        } else {
            viewHolder.labelGroup.setBackground(MyApplication.getContext().getResources().getDrawable(R.drawable.filter_unselection_background));
        }
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    public int getItemSpan(int position) {
        return filteredData.get(position).getSpanSize();
    }

    @Override
    public int getItemViewType(int position) {
        return filteredData.get(position).getType().ordinal();
    }
    @Override
    public long getItemId(int position){
        return position;
    }

    public Object getItem(int position){
        return filteredData.get(position);
    }

    public static int getScreenWidth() {

        // TODO minus padding left right of recycler view
        return Resources.getSystem().getDisplayMetrics().widthPixels - UiUtil.getPixelFromDp(40);
    }

    void calculateSpan() {
        int listWidth = getScreenWidth();

        int widthPer1Span = listWidth / sumSpan; // TODO - 40dp
        TextView textView = (TextView) context.findViewById(R.id.textFilterMeasure);
        Rect bounds = new Rect();
        Paint textPaint = textView.getPaint();
        // calculate potential span num
        int index = 0;
        for (FilterListItem item : filteredData) {

            if(item.getT() instanceof Category) {
                Category category = (Category)item.getT();
                // add item
                textPaint.getTextBounds(category.getCategoryName(), 0, category.getCategoryName().length(), bounds);

                // TODO should add padding left & right of text view + iconRight + spacing
                int width = bounds.width() +  textView.getPaddingLeft() + textView.getPaddingRight() +
                        2 * UiUtil.getPixelFromDp(8);

                int spanSize = Math.max(1, Math.min(sumSpan, Math.round((float) width / widthPer1Span + 0.5f)));
                item.setSpanSize(spanSize);
            }
            else{
                item.setSpanSize(sumSpan);
                if (index != 0) {
                    filteredData.get(index - 1).setIsEndGroup(true);
                }
            }

            index = index + 1;
        }
        filteredData.get(filteredData.size() - 1).setIsEndGroup(true);

        // recalculate span num
        int totalCurGroup = 0;
        int startGroup = 0;
        int endGroup = 0;
        boolean remainGroup = false;
        for (int i = 0; i < filteredData.size(); i++) {
            FilterListItem item = filteredData.get(i);

            if(i == 0){
                totalCurGroup = item.getSpanSize();
                continue;
            }

            if (totalCurGroup + item.getSpanSize() < sumSpan) {
                totalCurGroup += item.getSpanSize();
                endGroup = i;

                if (i == filteredData.size() - 1) {
                    remainGroup = true;
                }
                continue;
            } else {

                // balance groups
                arrangeGroup(startGroup, endGroup, totalCurGroup);

                totalCurGroup = item.getSpanSize();
                if (i == filteredData.size() - 1 && i != endGroup) {
                    remainGroup = true;
                }
                startGroup = i;
                endGroup = i;
            }
        }

        if (remainGroup) {
            // balance groups
            arrangeGroup(startGroup, endGroup, totalCurGroup);
        }
    }

    private void arrangeGroup(int startGroup, int endGroup, int totalCurGroup) {
        int remainingSpan = sumSpan - totalCurGroup;
        int additionalSpan = remainingSpan / (endGroup - startGroup + 1);
        int lastRemaining = remainingSpan - additionalSpan * (endGroup - startGroup + 1);
        // update additional span and get smallest to add last remaining
        int smallest = sumSpan, smallestId = 0;
        filteredData.get(startGroup).setIsStartLine(true);
        filteredData.get(endGroup).setIsEndLine(true);
        for (int j = startGroup; j <= endGroup; j++) {

            FilterListItem<Category> itemTmp = filteredData.get(j);
            if (itemTmp.getSpanSize() < smallest) {
                smallest = itemTmp.getSpanSize();
                smallestId = j;
            }
        }

        // Log.d("Remaining", "" + lastRemaining);
        if(endGroup - startGroup > 0) {
            FilterListItem<Category> itemSmallest = filteredData.get(endGroup);
            itemSmallest.setSpanSize(itemSmallest.getSpanSize() + lastRemaining);
        }
    }

    public class CategoryFilterViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout labelGroup;
        TextView textView;
        LinearLayout backgroundGroup;

        public CategoryFilterViewHolder(View itemView) {
            super(itemView);
            labelGroup = (LinearLayout) itemView.findViewById(R.id.labelGroup);
            textView = (TextView) itemView.findViewById(R.id.label);
            backgroundGroup = (LinearLayout) itemView.findViewById(R.id.backgroundGroup);
        }
    }

    public class CategoryFilterTitleViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        View headerLine;
        public CategoryFilterTitleViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.sizeFilterHeaderTextView);
            headerLine = itemView.findViewById(R.id.headerLine);
        }
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString;
            filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<FilterListItem> list = originalData;

            int count = list.size();
            final ArrayList<FilterListItem> nlist = new ArrayList<>(count);

            String filterableString;

            if (!TextUtils.isEmpty(constraint)) {
                for (int i = 0; i < count; i++) {
                    if (list.get(i).getType() == FilterListItem.ItemType.TYPE_FILTER_LIST) {
                        filterableString = "" + ((Category)list.get(i).getT()).getCategoryName();
                        if (filterableString.toLowerCase().contains(filterString)) {
                            nlist.add(list.get(i));
                        }
                    } else {
                        nlist.add(list.get(i));
                    }
                }

            } else {
                nlist.addAll(list);
            }

            // clear non item group header
            for (int i = 0; i < nlist.size() - 1; i++) {
                if(nlist.get(i).getType().equals(FilterListItem.ItemType.TYPE_FILTER_TITLE)
                        && nlist.get(i + 1).getType().equals(FilterListItem.ItemType.TYPE_FILTER_TITLE)) {
                    nlist.remove(i);
                    i--;
                    continue;
                }
            }

            if(nlist.get(nlist.size() - 1).getType() == FilterListItem.ItemType.TYPE_FILTER_TITLE){
                nlist.remove(nlist.size() - 1);
            }

            results.values = nlist;
            results.count = nlist.size();
            return results;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<FilterListItem>) results.values;
            notifyDataSetChanged();
        }

    }

    public ArrayList<FilterListItem> getOriginalData() {
        return originalData;
    }

    public Integer getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(Integer selectedItemId) {
        if (selectedItemId >= 0) {
            filteredData.get(selectedItemId).setSelected(!filteredData.get(selectedItemId).isSelected());
        }
    }

}
