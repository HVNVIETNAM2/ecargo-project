package com.mm.main.app.record;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.utils.ObjectUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by henrytung on 16/12/2015.
 */
public class SearchHistory implements Serializable{

    private static SearchHistory mySearchHistory = null;
//    List<String> searchedWords = new ArrayList<>();
    HashMap<String, WeightedWord> searchedWords = new HashMap<>();

    public static SearchHistory getInstance() {
        if (mySearchHistory == null) {
            SearchHistory sh = loadSearchHistory();
            if (sh != null) {
                mySearchHistory = sh;
            } else {
                mySearchHistory = new SearchHistory();
                saveSearchHistory(mySearchHistory);
            }
        }

        return mySearchHistory;
    }

    private SearchHistory(){
    }

    public List<String> getSearchedWords() {
//        return searchedWords;

        List<String> sortedList = new ArrayList<>();
        List<WeightedWord> peopleByAge = new ArrayList<WeightedWord>(searchedWords.values());

        Collections.sort(peopleByAge, new Comparator<WeightedWord>() {

            public int compare(WeightedWord o1, WeightedWord o2) {
                return o1.getWeight() - o2.getWeight();
            }
        });

        for (int i = 0; i < peopleByAge.size(); i++) {
            sortedList.add(peopleByAge.get(i).getWord());
        }

        return sortedList;
    }

    public void addSearchWord(String word) {
//        searchedWords.add(word);
        if (searchedWords.containsKey(word)) {
            Integer number = searchedWords.get(word).getWeight() + 1;
            searchedWords.put(word, new WeightedWord(word, number));
        } else {
            searchedWords.put(word, new WeightedWord(word, 1));
        }
        saveSearchHistory();
    }

    public void cleanHistory() {
        searchedWords.clear();
        saveSearchHistory();
    }
//    public void setSearchedWords(List<String> searchedWords) {
//        this.searchedWords = searchedWords;
//    }

    private void saveSearchHistory() {
        saveSearchHistory(this);
    }

    private static void saveSearchHistory(SearchHistory searchHistory) {
        ObjectUtil.saveComplexObject(MyApplication.getContext(), searchHistory, Constant.SEARCH_HISTORY);
    }

    private static SearchHistory loadSearchHistory() {
        return ObjectUtil.loadComplexObject(MyApplication.getContext(), SearchHistory.class, Constant.SEARCH_HISTORY);
    }

    class WeightedWord implements Serializable{
        String word;
        Integer weight;

        public WeightedWord(String word, Integer weight) {
            this.word = word;
            this.weight = weight;
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }

        public Integer getWeight() {
            return weight;
        }

        public void setWeight(Integer weight) {
            this.weight = weight;
        }
    }
}
