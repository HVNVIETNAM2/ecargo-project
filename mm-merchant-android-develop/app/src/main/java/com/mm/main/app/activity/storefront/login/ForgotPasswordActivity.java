package com.mm.main.app.activity.storefront.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.merchant.setting.SelectRegionActivity;
import com.mm.main.app.layout.RequestSmsSlidingDrawer;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.schema.Country;
import com.mm.main.app.schema.ForgotPassword;
import com.mm.main.app.schema.request.MobileVerifySmsCheckRequest;
import com.mm.main.app.schema.request.MobileVerifySmsSendRequest;
import com.mm.main.app.schema.response.MobileVerifySmsSendResponse;
import com.mm.main.app.utils.AnimateUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.mm.storefront.app.wxapi.WXEntryActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

public class ForgotPasswordActivity extends AppCompatActivity {

    @Bind(R.id.requestSmsSlidingDrawer)
    RequestSmsSlidingDrawer requestSmsSlidingDrawer;

    @Bind(R.id.rlLayoutForMobile)
    RelativeLayout rlLayoutForMobile;

    @Bind(R.id.edPhoneNumber)
    EditText edPhoneNumber;

    @Bind(R.id.edVerCode)
    EditText edVerCode;

    @Bind(R.id.edCountryCode)
    EditText edCountryCode;

    @Bind(R.id.edPassword)
    EditText edPassword;

    @Bind(R.id.edConfirmPassword)
    EditText edConfirmPassword;

    @Bind(R.id.edHide)
    EditText edHide;

    @Bind(R.id.tvError)
    TextView tvError;

    @Bind(R.id.tvCountry)
    TextView tvCountry;

    boolean isOpened;


    private Country currentCountry;
    List<Country> countryList;
    boolean isResetViewCase;
    boolean isEditPhoneFocus;

    private final int SPEED = 15;
    private final int TIME_CLOSE_ERROR = 5;
    Handler handlerShowError = new Handler();
    Runnable closeErrorCallback;
    ForgotPassword forgotPasswordRequest;
    MobileVerifySmsCheckRequest mobileVerifySmsCheckRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        isResetViewCase = false;
        isEditPhoneFocus = false;
        setUpAccountEditText();
        setUpSlider();
        setUpVerifyCodeEditText();
        setUpCountryCodeEditText();
        setUpPasswordEditText();
        fetchMobileCode();
        forgotPasswordRequest = new ForgotPassword();
        mobileVerifySmsCheckRequest = new MobileVerifySmsCheckRequest();
        setListenerToRootView();

    }

    //Group function setting view
    public void setListenerToRootView() {
        final View activityRootView = getWindow().getDecorView().findViewById(android.R.id.content);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > 100) { // 99% of the time the height diff will be due to a keyboard.

                    isOpened = true;
                } else if (isOpened == true) {

                    if (isEditPhoneFocus && validatePhoneNumber(edPhoneNumber.getText().toString())) {
                        requestSmsSlidingDrawer.unlockSliding();
                    }
                    isOpened = false;
                }
            }
        });
    }

    private void setUpSlider() {
        requestSmsSlidingDrawer.bindRequestSmsDrawer(60, getResources().getString(R.string.LB_CA_SR_REQUEST_VERCODE),
                new RequestSmsSlidingDrawer.SignUpCallBack() {

                    @Override
                    public void startCountDown() {
                        sendMobileVerifySms();
                        isResetViewCase = false;
                    }

                    @Override
                    public void endCountDown() {
                        resetView();
                    }
                });


        requestSmsSlidingDrawer.lockSliding();
    }

    private void setUpAccountEditText() {
        edPhoneNumber.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if (validatePhoneNumber(edPhoneNumber.getText().toString())) {
                        requestSmsSlidingDrawer.unlockSliding();
                    }
                }
                return false;
            }
        });

        edPhoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    requestSmsSlidingDrawer.lockSliding();
                    isEditPhoneFocus = true;
                } else {
                    isEditPhoneFocus = false;
                }
            }
        });
    }

    private boolean validatePhoneNumber(String accountStr) {

        String error = null;

        if (ValidationUtil.isEmpty(accountStr)) {
            error = getString(R.string.MSG_ERR_CA_ACCOUNT_NIL);
        }

        if (error == null && !ValidationUtil.isValidPhone(accountStr)) {
            error = getString(R.string.MSG_ERR_CA_ACCOUNT_PATTERN);
        }

        if (error != null) {
            showError(error);
            return false;
        }
        return true;
    }


    private void showError(String errorStr) {

        tvError.setText(errorStr);
        if (tvError.getVisibility() != View.VISIBLE) {
            AnimateUtil.expand(tvError, SPEED, 1, UiUtil.getPixelFromDp(35), null);

            if (closeErrorCallback != null) {
                handlerShowError.removeCallbacks(closeErrorCallback);
            }

            closeErrorCallback = new Runnable() {
                @Override
                public void run() {
                    closeError();
                }
            };
            handlerShowError.postDelayed(closeErrorCallback, TIME_CLOSE_ERROR * 1000);
        }
    }

    private void closeError() {
        if (tvError.getVisibility() == View.VISIBLE) {
            AnimateUtil.collapse(tvError, SPEED, null);
            tvError.setVisibility(View.INVISIBLE);
        }
    }

    private void setUpPasswordEditText() {

        edPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && !isResetViewCase) {
                    validatePassword();
                }
            }
        });

        edConfirmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && !isResetViewCase) {
                    validateConfirmPassword();
                }
            }
        });

        edConfirmPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    if (validatePassword() && validateConfirmPassword()) {
                        resetPasswordService();
                    }
                }

                return false;
            }
        });

        edHide.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edHide.getWindowToken(), 0);
                }
            }

        });

    }

    public void setUpCountryCodeEditText() {

        edCountryCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s)) {
                    edCountryCode.setText("+");
                    edCountryCode.setSelection(edCountryCode.getText().length());
                }

                boolean foundCountry = false;
                for (Country country : countryList) {
                    if (s.toString().equals(country.getMobileCode())) {
                        currentCountry = country;
                        tvCountry.setText(currentCountry.getGeoCountryName());
                        foundCountry = true;
                        break;
                    }
                }

                if (foundCountry == false) {
                    currentCountry = null;
                    tvCountry.setText(getString(R.string.LB_COUNTRY_PICK));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    @OnClick(R.id.btnRegion)
    public void onClickRegionButton() {
        Intent intent = new Intent(this, SelectRegionActivity.class);
        startActivityForResult(intent, SelectRegionActivity.SELECT_COUNTRY_CODE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == SelectRegionActivity.SELECT_COUNTRY_CODE_REQUEST) {
            currentCountry = (Country) data.getExtras().get(SelectRegionActivity.SELECTED_COUNTRY_CODE);
            updateCountryUI();
        }
    }

    private void updateCountryUI() {
        edCountryCode.setText(currentCountry.getMobileCode());
        tvCountry.setText(currentCountry.getGeoCountryName());
    }

    private void resetView() {
        isResetViewCase = true;
        //tempView.setVisibility(View.VISIBLE);
        requestSmsSlidingDrawer.resetView();
        requestSmsSlidingDrawer.lockSliding();
        edVerCode.setText("");
        edPassword.setEnabled(false);
        edConfirmPassword.setEnabled(false);
        edPassword.setText("");
        edConfirmPassword.setText("");
    }

    //Group function validate
    private boolean validatePassword() {
        String password = edPassword.getText().toString();
        String error = null;

        if (ValidationUtil.isEmpty(password)) {
            error = getString(R.string.MSG_ERR_CA_PW_NIL);
        }

        if (error == null && !ValidationUtil.isValidPassword(password)) {
            error = getString(R.string.MSG_ERR_CA_PW_PATTERN);
        }

        if (error != null) {
            showError(error);
            return false;
        }

        return true;
    }

    private boolean validateConfirmPassword() {
        String passwordConfirm = edConfirmPassword.getText().toString();
        String password = edPassword.getText().toString();

        String error = null;

        if (ValidationUtil.isEmpty(passwordConfirm)) {
            error = getString(R.string.MSG_ERR_CA_PW_NIL);
        }
        if (error == null && !passwordConfirm.equals(password)) {
            error = getString(R.string.MSG_ERR_CA_CFM_PW_NOT_MATCH);
        }
        if (error != null) {
            showError(error);
            return false;
        }
        return true;
    }

    private void setUpVerifyCodeEditText() {
        edVerCode.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    mobileVerifySmsCheckRequest.setMobileVerificationToken(edVerCode.getText().toString());

                    APIManager.getInstance().getAuthService().mobileVerifySmsCheck(mobileVerifySmsCheckRequest)
                            .enqueue(new MmCallBack<Boolean>(getApplication(), MmCallBack.ERROR_520) {

                                @Override
                                public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                                    final boolean success = (response.body() != null && response.body());
                                    if (success) {

                                        edPassword.setEnabled(true);
                                        edConfirmPassword.setEnabled(true);
                                    }
                                }
                            });

                    /*if (forgotPasswordRequest.getMobileVerificationToken() != null && forgotPasswordRequest.getMobileVerificationToken().equals(edVerCode.getText().toString())) {
                        edPassword.setEnabled(true);
                        edConfirmPassword.setEnabled(true);
                    }*/
                }
                return false;
            }
        });
    }


    //Group functiion call API
    private void resetPasswordService() {
        forgotPasswordRequest.setMobileVerificationToken(edVerCode.getText().toString());
        forgotPasswordRequest.setPassword(edPassword.getText().toString());

        APIManager.getInstance().getAuthService().resetPassword(forgotPasswordRequest)
                .enqueue(new MmCallBack<Boolean>(ForgotPasswordActivity.this) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                        Intent intent = new Intent(ForgotPasswordActivity.this, WXEntryActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
    }

    private void sendMobileVerifySms() {
        final MobileVerifySmsSendRequest data = new MobileVerifySmsSendRequest();
        data.setMobileCode(edCountryCode.getText().toString());
        data.setMobileNumber(edPhoneNumber.getText().toString());

        APIManager.getInstance().getAuthService().mobileVerifySmsSend(data)
                .enqueue(new MmCallBack<MobileVerifySmsSendResponse>(getApplication()) {
                    @Override
                    public void onSuccess(Response<MobileVerifySmsSendResponse> response, Retrofit retrofit) {
                        forgotPasswordRequest.setMobileNumber(data.getMobileNumber());
                        forgotPasswordRequest.setMobileCode(data.getMobileCode());
                        forgotPasswordRequest.setMobileVerificationId(response.body().getMobileVerificationId());
                        forgotPasswordRequest.setMobileVerificationToken(response.body().getMobileVerificationToken());

                        //TODO test
                        Log.d("TAG", response.body().getMobileVerificationId() + "---" + response.body().getMobileVerificationToken());

                        mobileVerifySmsCheckRequest.setMobileNumber(data.getMobileNumber());
                        mobileVerifySmsCheckRequest.setMobileCode(data.getMobileCode());
                        mobileVerifySmsCheckRequest.setMobileVerificationId(response.body().getMobileVerificationId());
                        rlLayoutForMobile.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void fetchMobileCode() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getGeoService().storefrontCountry()
                .enqueue(new MmCallBack<List<Country>>(this) {
                    @Override
                    public void onSuccess(Response<List<Country>> response, Retrofit retrofit) {
                        countryList = response.body();
                        if (countryList != null) {
                            currentCountry = countryList.get(0);
                            updateCountryUI();
                        }
                        MmProgressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        MmProgressDialog.dismiss();
                    }
                });
    }

}
