package com.mm.main.app.adapter.strorefront.interest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.BrandMerchantsListItem;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.utils.CircleProcessUtil;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by haivu on 2/15/2016.
 */
public class InterestBrandMerchantsRvAdapter extends RecyclerView.Adapter<InterestBrandMerchantsRvAdapter.BrandMerchantsViewHolder> {

    public interface OnItemListener {
        void onClick(boolean isSelected, int position);
    }

    public static final int MAX_ITEM_COUNT = 9999;
    private Context context;
    private ArrayList<BrandMerchantsListItem> data;
    private OnItemListener listener;
    int centerItem;
    RecyclerView recyclerView;
    boolean isInit = true;

    public InterestBrandMerchantsRvAdapter(Context context, ArrayList<BrandMerchantsListItem> data, RecyclerView recyclerView, OnItemListener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
        centerItem = UiUtil.findCenterItemPosition(recyclerView, context, data.size(), MAX_ITEM_COUNT);
        this.recyclerView = recyclerView;
    }

    @Override
    public BrandMerchantsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.merchant_item_view, parent, false);
        return new BrandMerchantsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BrandMerchantsViewHolder holder, final int position) {
        if (position == centerItem && isInit) {
            recyclerView.smoothScrollBy(0, 100);
            isInit = false;
        }
        final int realPos = position % data.size();
        final BrandMerchantsListItem item = data.get(realPos);

        String url = PathUtil.getMerchantImageUrl(item.getMerchant().getLargeLogoImage());
        Picasso.with(MyApplication.getContext()).load(url).into(holder.imageMerchantImageView);

        holder.imageMerchantImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getIsCenter()) {
                    item.setIsSelected(!item.getIsSelected());
                    upDateViewSelected(item.getIsSelected(), holder);

                    listener.onClick(item.getIsSelected(), realPos);
                }
            }
        });

        upDateViewSelectedOnBind(item.getIsSelected(), holder);
        upDateViewCenter(item, holder);
    }

    private void upDateViewSelectedOnBind(boolean isSelected, BrandMerchantsViewHolder holder) {

        Log.d("upDateViewSelected", "upDateViewSelectedOnBind" + isSelected);
        holder.selectedCheckbox.setScaleX(1f);
        holder.selectedCheckbox.setScaleY(1f);
        if (isSelected) {
            holder.selectedCheckbox.setVisibility(View.VISIBLE);
            holder.circleProcess.setPercentNoAnim(100);
        } else {
            holder.selectedCheckbox.setVisibility(View.INVISIBLE);
            holder.circleProcess.setPercentNoAnim(0);
        }
    }

    private void upDateViewSelected(boolean isSelected, BrandMerchantsViewHolder holder) {
        Log.d("upDateViewSelected", "" + isSelected);
        final View cbx = holder.selectedCheckbox;
        if (isSelected) {
            holder.selectedCheckbox.setScaleX(0f);
            holder.selectedCheckbox.setScaleY(0f);
            holder.selectedCheckbox.setVisibility(View.VISIBLE);
            holder.circleProcess.setPercentAnim(100, 45);
            holder.selectedCheckbox.animate().scaleXBy(1.4f)
                    .scaleYBy(1.4f).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                cbx.animate().scaleXBy(-0.4f)
                                        .scaleYBy(-0.4f)
                                        .setDuration(180).start();
                            }
                        })
                    .setDuration(600).start();


        } else {

            holder.circleProcess.setPercentAnim(0, 45);

            holder.selectedCheckbox.animate().scaleX(1f).scaleXBy(-1f)
                    .scaleY(1f).scaleYBy(-1f)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            cbx.setVisibility(View.INVISIBLE);
                        }
                    })
                    .setDuration(600).start();
        }
    }

    public void upDateViewCenter(BrandMerchantsListItem item, BrandMerchantsViewHolder holder) {
        Merchant merchant = item.getMerchant();

        //TODO : ...
        holder.merchantNameSCTextView.setText(merchant.getMerchantName());
        holder.merchantNameENTextView.setText(merchant.getMerchantNameInvariant());
//
//        if (merchant.getCount() > Constant.MIN_FOLLOWER_COUNT) {
//            holder.merchantFollowTextView.setVisibility(View.VISIBLE);
//            holder.merchantFollowTextView.setText(context.getString(R.string.LB_CA_CURATORS_FOLLOWER_NO) + " " +
//                    FormatUtil.formatNumber(merchant.getCount() * Constant.DEMO_FACTOR_FOR_FOLLOWER));
//        } else {
//            holder.merchantFollowTextView.setVisibility(View.GONE);
//        }

        if (item.getIsCenter()) {
            holder.merchantInfoLL.setVisibility(View.VISIBLE);
            holder.imageMerchantImageView.setAlpha(1.0f);
        } else {
            holder.merchantInfoLL.setVisibility(View.INVISIBLE);
            holder.imageMerchantImageView.setAlpha(0.5f);
        }
    }

    public void updateViewTheSame(boolean isSelected, View view) {
        if (isSelected) {
            CheckBox cbx = (CheckBox) view.findViewById(R.id.selectedCheckbox);
            cbx.setVisibility(View.VISIBLE);
            cbx.setScaleX(1f);
            cbx.setScaleY(1f);

            CircleProcessUtil circleProcessUtil = (CircleProcessUtil) view.findViewById(R.id.circleProcess);
            circleProcessUtil.setPercentNoAnim(100);
        } else {
            view.findViewById(R.id.selectedCheckbox).setVisibility(View.INVISIBLE);
            CircleProcessUtil circleProcessUtil = (CircleProcessUtil) view.findViewById(R.id.circleProcess);
            circleProcessUtil.setPercentNoAnim(0);
        }
    }

    @Override
    public int getItemCount() {
        return MAX_ITEM_COUNT;
    }

    public void updateCenterView(int position, View view, boolean isCenter) {
        if (data.size() < 1) {
            return;
        }

        BrandMerchantsListItem itemData = data.get(position % data.size());
        itemData.setIsCenter(isCenter);

        if (isCenter) {
            view.findViewById(R.id.merchantInfoLL).setVisibility(View.VISIBLE);
            view.findViewById(R.id.imageMerchantImageView).setAlpha(1.0f);
        } else {
            view.findViewById(R.id.merchantInfoLL).setVisibility(View.INVISIBLE);
            view.findViewById(R.id.imageMerchantImageView).setAlpha(0.5f);
        }

        if (itemData.getIsSelected()) {
            view.findViewById(R.id.selectedCheckbox).setScaleX(1f);
            view.findViewById(R.id.selectedCheckbox).setScaleY(1f);
            view.findViewById(R.id.selectedCheckbox).setVisibility(View.VISIBLE);
            CircleProcessUtil circleProcessUtil = (CircleProcessUtil) view.findViewById(R.id.circleProcess);
            circleProcessUtil.setPercentNoAnim(100);
        } else {
            view.findViewById(R.id.selectedCheckbox).setVisibility(View.INVISIBLE);
            CircleProcessUtil circleProcessUtil = (CircleProcessUtil) view.findViewById(R.id.circleProcess);
            circleProcessUtil.setPercentNoAnim(0);
        }
    }

    public class BrandMerchantsViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imageMerchantImageView)
        public ImageView imageMerchantImageView;

        @Bind(R.id.merchantNameSCTextView)
        public TextView merchantNameSCTextView;

        @Bind(R.id.merchantNameENTextView)
        public TextView merchantNameENTextView;

        @Bind(R.id.merchantFollowTextView)
        public TextView merchantFollowTextView;

        @Bind(R.id.selectedCheckbox)
        public CheckBox selectedCheckbox;

        @Bind(R.id.merchantInfoLL)
        LinearLayout merchantInfoLL;

        @Bind(R.id.circleProcess)
        CircleProcessUtil circleProcess;

        public View view;

        public BrandMerchantsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;

            circleProcess.setWidth(2);
            circleProcess.setWidthPercent(1);
            circleProcess.setColor(R.color.primary1);
        }
    }
}
