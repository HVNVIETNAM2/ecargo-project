
package com.mm.main.app.schema;

import java.util.ArrayList;
import java.util.List;

public class Aggregations {

    private List<Integer> CategoryArray = new ArrayList<Integer>();
    private List<Integer> BrandArray = new ArrayList<Integer>();
    private List<Integer> MerchantArray = new ArrayList<Integer>();
    private List<Integer> SizeArray = new ArrayList<Integer>();
    private List<Integer> ColorArray = new ArrayList<Integer>();
    private List<Integer> BadgeArray = new ArrayList<Integer>();

    /**
     * 
     * @return
     *     The CategoryArray
     */
    public List<Integer> getCategoryArray() {
        return CategoryArray;
    }

    /**
     * 
     * @param CategoryArray
     *     The CategoryArray
     */
    public void setCategoryArray(List<Integer> CategoryArray) {
        this.CategoryArray = CategoryArray;
    }

    /**
     * 
     * @return
     *     The BrandArray
     */
    public List<Integer> getBrandArray() {
        return BrandArray;
    }

    /**
     * 
     * @param BrandArray
     *     The BrandArray
     */
    public void setBrandArray(List<Integer> BrandArray) {
        this.BrandArray = BrandArray;
    }

    /**
     * 
     * @return
     *     The MerchantArray
     */
    public List<Integer> getMerchantArray() {
        return MerchantArray;
    }

    /**
     * 
     * @param MerchantArray
     *     The MerchantArray
     */
    public void setMerchantArray(List<Integer> MerchantArray) {
        this.MerchantArray = MerchantArray;
    }

    /**
     * 
     * @return
     *     The SizeArray
     */
    public List<Integer> getSizeArray() {
        return SizeArray;
    }

    /**
     * 
     * @param SizeArray
     *     The SizeArray
     */
    public void setSizeArray(List<Integer> SizeArray) {
        this.SizeArray = SizeArray;
    }

    /**
     * 
     * @return
     *     The ColorArray
     */
    public List<Integer> getColorArray() {
        return ColorArray;
    }

    /**
     * 
     * @param ColorArray
     *     The ColorArray
     */
    public void setColorArray(List<Integer> ColorArray) {
        this.ColorArray = ColorArray;
    }

    public List<Integer> getBadgeArray() {
        return BadgeArray;
    }

    public void setBadgeArray(List<Integer> badgeArray) {
        BadgeArray = badgeArray;
    }
}
