package com.mm.main.app.activity.merchant.register;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.mm.main.app.R;
import com.mm.main.app.activity.merchant.user.UserActivity;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.factory.ValidatorFactory;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.LifeCycleManager;
import com.mm.main.app.schema.request.ActivateRequest;
import com.mm.main.app.schema.response.LoginResponse;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.concurrent.Callable;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;


public class RegistrationPasswordActivity extends ActionBarActivity {
    public static final String TAG = RegistrationPasswordActivity.class.toString();
    @Bind(R.id.passwordEditText)
    EditText passwordEditText;
    @Bind(R.id.reEnterPasswordEditText)
    EditText reEnterPasswordEditText;
    @BindString(R.string.LB_COMPLETE_REGISTRATION)
    String title;

    @Bind(R.id.inputPasswordLayout)
    TextInputLayout inputPasswordLayout;

    @Bind(R.id.reEnterPasswordLayout)
    TextInputLayout reEnterPasswordLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_confirm_password);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, title);

        setupValidation();
    }

    private void setupValidation() {
        passwordEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validatePassword();
            }
        }, inputPasswordLayout));

        reEnterPasswordEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateReEnterPassword();
            }
        }, reEnterPasswordLayout));
    }

    @OnClick(R.id.actionButton)
    public void confirmPassword() {
        if (validatePassword() & validateReEnterPassword() & matchPasswords()) {
            if (passwordEditText.getText().toString().equals(reEnterPasswordEditText.getText().toString())) {
                MmProgressDialog.show(this);
                APIManager.getInstance().getAuthService()
                        .activate(new ActivateRequest(getIntent().getStringExtra(Constant.Extra.EXTRA_ACTIVATION_TOKEN), getIntent().getStringExtra(Constant.Extra.EXTRA_USER_KEY), passwordEditText.getText().toString()))
                        .enqueue(new MmCallBack<LoginResponse>(RegistrationPasswordActivity.this) {
                            @Override
                            public void onSuccess(Response<LoginResponse> response, Retrofit retrofit) {
                                LifeCycleManager.login(response);
                                LifeCycleManager.cleanStart(RegistrationPasswordActivity.this, UserActivity.class, null);
                            }
                        });
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private boolean validatePassword() {
        String error = null;
        String fieldName = getResources().getString(R.string.LB_PASSWORD);
        String password = passwordEditText.getText().toString();

        if (!ValidationUtil.validate(ValidationUtil.PASSWORD_REGEX, password)) {
            error = getResources().getString(R.string.MSG_ERR_FIELDNAME_PATTERN)
                    .replace(Constant.TEXT_REPLACE_FIELD_NAME, fieldName);
        }
        if (password.trim().isEmpty()) {
            error = getResources().getString(R.string.MSG_ERR_RESET_PASSWORD_NIL);
        }

        ValidationUtil.setErrorMessage(inputPasswordLayout, error);

        return error == null;
    }

    private boolean validateReEnterPassword() {
        String error = null;
        String fieldName = getResources().getString(R.string.LB_REENTER_PASSWORD);
        String password = reEnterPasswordEditText.getText().toString();

        if (!ValidationUtil.validate(ValidationUtil.PASSWORD_REGEX, password)) {
            error = getResources().getString(R.string.MSG_ERR_FIELDNAME_PATTERN)
                    .replace(Constant.TEXT_REPLACE_FIELD_NAME, fieldName);
        }
        if (password.trim().isEmpty()) {
            error = getResources().getString(R.string.MSG_ERR_RESET_PASSWORD_REENTER_NIL);
        }

        ValidationUtil.setErrorMessage(reEnterPasswordLayout, error);

        return error == null;
    }

    private boolean matchPasswords() {
        if (!passwordEditText.getText().toString().equals(reEnterPasswordEditText.getText().toString())) {
            ErrorUtil.showErrorDialog(this, null, getResources().getString(R.string.MSG_ERR_PASSWORD_REENTER_NOT_MATCH));
            return false;
        }

        return true;
    }
}
