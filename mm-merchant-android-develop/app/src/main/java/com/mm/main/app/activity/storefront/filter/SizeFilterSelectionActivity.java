package com.mm.main.app.activity.storefront.filter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.SearchView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.filter.SizeFilterSelectionRVAdapter;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Size;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.UiUtil;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SizeFilterSelectionActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    public static final String EXTRA_RESULT = "result";
    public static final String EXTRA_RESULT_DATA = "resultData";

    public static final String EXTRA_HEADER = "extra_header";
    public static final String EXTRA_SELECTIONS = "selection";
    public static final String EXTRA_SELECTED = "selected";
    private static final int GRID_SIZE_PADDING_IN_DP = 60;
    private static final int SIZE_ITEM_WIDTH_IN_DP = 69;

    @Bind(R.id.recyclerViewFilter)
    RecyclerView recyclerViewFilter;

    @Bind(R.id.searchView)
    SearchView searchView;
    ArrayList<FilterListItem> filterList;

    @Bind(R.id.textViewHeader)
    TextView textViewHeader;

    int[] selected;

    SizeFilterSelectionRVAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeNoActionBar);
        setContentView(R.layout.activity_filter_size_selection);
        ButterKnife.bind(this);
        setupToolbar();

        selected = getIntent().getIntArrayExtra(EXTRA_SELECTED);
        Bundle bundle = (Bundle) getIntent().getBundleExtra(EXTRA_SELECTIONS);

        renderFilterList((ArrayList<FilterListItem>) bundle.getSerializable(FilterActivity.EXTRA_TEXT_DATA));

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        textViewHeader.setText(getIntent().getStringExtra(EXTRA_HEADER));
        setupSearchView();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private ArrayList<FilterListItem> createSizeListItem(ArrayList<FilterListItem> fullSizeList) {
        // add header item
        ArrayList<FilterListItem> listSizeFilter = new ArrayList<>();
        int count = fullSizeList.size();

        if (count > 0) {
            listSizeFilter.add(new FilterListItem(((Size)fullSizeList.get(0).getT()).getSizeGroupName(), false, FilterListItem.ItemType.TYPE_FILTER_TITLE));

            for (int i = 0; i < count - 1; i++) {

                FilterListItem item = fullSizeList.get(i);
                Size currentItem = (Size)item.getT();
                Size nextItem = (Size)fullSizeList.get(i + 1).getT();

                item.setType(FilterListItem.ItemType.TYPE_FILTER_LIST);
                listSizeFilter.add(item);

                if (!currentItem.getSizeGroupId().equals(nextItem.getSizeGroupId())) {
                    listSizeFilter.add(new FilterListItem(nextItem.getSizeGroupName(), false, FilterListItem.ItemType.TYPE_FILTER_TITLE));
                }
            }

            fullSizeList.get(count - 1).setType(FilterListItem.ItemType.TYPE_FILTER_LIST);
            listSizeFilter.add(fullSizeList.get(count - 1));
        }

        return listSizeFilter;

    }

    private void initSelection() {
        if (selected != null && selected.length > 0) {
            for (int i = 0; i < selected.length; i++) {
                listAdapter.setSelectedItemId(selected[i]);
            }
        }
    }

    private void setupSearchView() {
//        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setSubmitButtonEnabled(true);
//        searchView.setQueryHint("<span id="IL_AD7" class="IL_AD">Search</span> Here");
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void finish() {
        super.finish();
    }

    private void renderFilterList(ArrayList<FilterListItem> filters) {
        filterList = createSizeListItem(filters);
        listAdapter = new SizeFilterSelectionRVAdapter(this, filterList, -1);
        recyclerViewFilter.setAdapter(listAdapter);

        final int columnNum = UiUtil.getNumberColumnGrid(GRID_SIZE_PADDING_IN_DP, SIZE_ITEM_WIDTH_IN_DP);
        GridLayoutManager manager = new GridLayoutManager(this, columnNum);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {

                if (listAdapter.getItemViewType(position) == FilterListItem.ItemType.TYPE_FILTER_TITLE.ordinal()) {
                    return columnNum;
                }
                return 1;
            }
        });

        recyclerViewFilter.setHasFixedSize(true);
        recyclerViewFilter.setLayoutManager(manager);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, UiUtil.getPixelFromDp(10), columnNum);

        recyclerViewFilter.addItemDecoration(itemDecoration);

        listAdapter.notifyDataSetChanged();

        initSelection();
    }

    private void proceedOk() {
        ActivityUtil.closeKeyboard(this);
        Intent intent = this.getIntent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_RESULT_DATA, createResultList());
        intent.putExtra(EXTRA_RESULT, bundle);

        setResult(RESULT_OK, intent);

        finish();
    }

    private ArrayList<Size> createResultList() {
        ArrayList<FilterListItem> filterListItemList = listAdapter.getOriginalData();
        ArrayList<Size> resultList = new ArrayList<>();

        for (int i = 0; i < filterListItemList.size(); i++) {
            FilterListItem item = filterListItemList.get(i);
            if (item.getT() instanceof Size &&  item.isSelected()) {
                resultList.add((Size)item.getT());
            }
        }

        return resultList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_brand_filter_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_ok) {
            proceedOk();
        }
        if (id == R.id.action_reset) {
            resetSelection();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listAdapter.getFilter().filter(newText);
        return true;
    }

    @OnClick(R.id.buttonOk)
    public void okButtonClick() {
        proceedOk();
    }

    private void resetSelection() {

        for (int i = 0; i < filterList.size(); i++) {
            filterList.get(i).setSelected(false);
        }
        listAdapter.notifyDataSetChanged();
    }


    private class DividerItemDecoration extends RecyclerView.ItemDecoration {

        private int margin;
        private int spanNumber;


        public DividerItemDecoration(Context context, int margin, int spanNumber) {
            this.margin = margin;
            this.spanNumber = spanNumber;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

            GridLayoutManager.LayoutParams params = (GridLayoutManager.LayoutParams) view.getLayoutParams();
            int spanIndex = params.getSpanIndex();
            int spanSize = params.getSpanSize();
            if (spanSize == 1) {
                if (spanIndex == 0) {
                    outRect.set(2 * margin, margin, margin, margin);
                } else if (spanIndex == spanNumber - 1) {
                    outRect.set(margin, margin, 2 * margin, margin);
                } else {
                    outRect.set(margin, margin, margin, margin);
                }
            } else {
                outRect.set(0, 0, 0, 0);
            }
        }
    }
}

