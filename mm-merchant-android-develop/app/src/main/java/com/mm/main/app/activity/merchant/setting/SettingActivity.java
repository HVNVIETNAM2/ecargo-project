package com.mm.main.app.activity.merchant.setting;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.alexbbb.uploadservice.MultipartUploadRequest;
import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.base.BaseListItemAdapter;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.event.UpdatePhotoEvent;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.log.Logger;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.manager.LifeCycleManager;
import com.mm.main.app.listitem.BaseListItem;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import de.greenrobot.event.EventBus;
import retrofit.Response;
import retrofit.Retrofit;


public class SettingActivity extends ActionBarActivity {

    final public static String EXTRA_USER_DETAIL = "USER_DETAIL";

    @Bind(R.id.listView)
    protected ListView settingListView;
    private List<BaseListItem> settingList;
    private BaseListItemAdapter settingListAdapter;
    private static final int CAMERA_REQUEST = 1888;
    private static final int CHANGE_NAME_REQUEST = CAMERA_REQUEST + 1;
    public static final int CHANGE_PASSWORD_REQUEST = CHANGE_NAME_REQUEST + 1;
    public static final int CHANGE_EMAIL_REQUEST = CHANGE_PASSWORD_REQUEST + 1;
    public static final int CHANGE_LANGUAGE_REQUEST = CHANGE_EMAIL_REQUEST + 1;
    public static final int REPORT_A_PROBLEM_REQUEST = CHANGE_LANGUAGE_REQUEST + 1;
    public static final int CHANGE_MOBILE_REQUEST = REPORT_A_PROBLEM_REQUEST + 1;
    private User user;
    private static final String TAG = "Setting Activity: ";

    @BindString(R.string.LB_PROFILE_PIC)
    String setting_item_profile;
    @BindString(R.string.LB_DISP_NAME)
    String setting_item_name;
    @BindString(R.string.setting_item_divider)
    String setting_item_divider;
    @BindString(R.string.LB_PASSWORD)
    String setting_item_password;
    @BindString(R.string.LB_EMAIL)
    String setting_item_email;
    @BindString(R.string.LB_MOBILE)
    String setting_item_mobile;
    @BindString(R.string.LB_LANGUAGE)
    String setting_item_language;
    @BindString(R.string.LB_REPORT_PROBLEM)
    String setting_item_report;
    @BindString(R.string.LB_LOGOUT)
    String setting_item_sign_out;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        render();
    }

    private void render() {
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, getResources().getString(R.string.LB_SETTING));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnItemClick(R.id.listView)
    public void onItemClicked(int position) {
        switch (position) {
            case 0:
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                break;
            case 1:
                Intent changeNameIntent = new Intent(this, ChangeNameActivity.class);
                changeNameIntent.putExtra(ChangeNameActivity.EXTRA_USER_DETAIL, user);
                startActivityForResult(changeNameIntent, CHANGE_NAME_REQUEST);
                break;
            case 2:
                break;
            case 3:
                Intent changePasswordIntent = new Intent(this, ChangePasswordActivity.class);
                changePasswordIntent.putExtra(ChangePasswordActivity.EXTRA_USER_DETAIL, user);
                startActivityForResult(changePasswordIntent, CHANGE_PASSWORD_REQUEST);
                break;
            case 4:
                Intent changeEmailIntent = new Intent(this, ChangeEmailActivity.class);
                changeEmailIntent.putExtra(Constant.Extra.EXTRA_USER_DETAIL, user);
                startActivityForResult(changeEmailIntent, CHANGE_EMAIL_REQUEST);
                break;
            case 5:
                Intent changeMobileIntent = new Intent(this, ChangeMobileActivity.class);
                changeMobileIntent.putExtra(Constant.Extra.EXTRA_USER_DETAIL, user);
                startActivityForResult(changeMobileIntent, CHANGE_MOBILE_REQUEST);
                break;
            case 7:
                Intent changeLanguageIntent = new Intent(this, ChangeLanguageActivity.class);
                changeLanguageIntent.putExtra(Constant.Extra.EXTRA_USER_DETAIL, user);
                startActivityForResult(changeLanguageIntent, CHANGE_LANGUAGE_REQUEST);
                overridePendingTransition(R.anim.right_to_left, R.anim.not_move);
                break;
            case 9:
                Intent reportAProblemIntent = new Intent(this, ReportProblemActivity.class);
                reportAProblemIntent.putExtra(Constant.Extra.EXTRA_USER_DETAIL, user);
                startActivityForResult(reportAProblemIntent, REPORT_A_PROBLEM_REQUEST);
                break;
            case 10:
                break;
            case 11:
                LifeCycleManager.relogin(this);

        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAMERA_REQUEST:
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    File f = new File(MyApplication.getContext().getCacheDir(), "iosFile.jpg");
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                    byte[] bitmapData = bos.toByteArray();

                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(f);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    try {
                        fos.write(bitmapData);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        fos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    final MultipartUploadRequest request =
                            new MultipartUploadRequest(MyApplication.getContext(),
                                    "uploadProfileImage",
                                    Constant.getApiURL() + "image/upload");
                    request.addFileToUpload(f.getPath(),
                            "file",
                            "androidImg.jpg",
                            "image/jpeg");

                    request.addHeader("Authorization", MmGlobal.getToken());
                    request.setNotificationConfig(android.R.drawable.ic_menu_upload,
                            "notification title",
                            "upload in progress text",
                            "upload completed successfully text",
                            "upload error text",
                            false);

                    // set the maximum number of automatic upload retries on error
                    request.setMaxRetries(2);

                    try {
                        //Start upload service and display the notification
                        request.startUpload();

                    } catch (Exception exc) {
                        //You will end up here only if you pass an incomplete upload request
                        Logger.e("AndroidUploadService", exc.getLocalizedMessage(), exc);
                    }


                    break;
                case CHANGE_NAME_REQUEST:
                    break;
                case CHANGE_PASSWORD_REQUEST:
                    break;
            }
        }
        settingListAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        render();
        viewUser(MmGlobal.getUserId());

    }

    private void viewUser(Integer userId) {
        MmProgressDialog.show(this);
        APIManager.getInstance().getUserService().viewUser(userId)
                .enqueue(new MmCallBack<User>(SettingActivity.this) {
                    @Override
                    public void onSuccess(Response<User> response, Retrofit retrofit) {
                        user = response.body();
                        LanguageManager.getInstance().updateCurrentCultureCode(user.getLanguageId());
                        renderSettingListView();
                    }

                });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void renderSettingListView() {
        settingList = new ArrayList<>();
        settingList.add(new BaseListItem(setting_item_profile, PathUtil.getProfileImageUrl(user.getProfileImage(), PathUtil.DEFAULT_PROFILE_PIC_SIZE), BaseListItem.ItemType.TYPE_PHOTO));
        settingList.add(new BaseListItem(setting_item_name, user.getDisplayName(), BaseListItem.ItemType.TYPE_TEXT));
        settingList.add(new BaseListItem(setting_item_divider, "", BaseListItem.ItemType.TYPE_DIVIDER));
        settingList.add(new BaseListItem(setting_item_password, "", BaseListItem.ItemType.TYPE_TEXT));
        settingList.add(new BaseListItem(setting_item_email, user.getEmail(), BaseListItem.ItemType.TYPE_TEXT));
        settingList.add(new BaseListItem(setting_item_mobile, user.getMobileCode() + " " + user.getMobileNumber(), BaseListItem.ItemType.TYPE_TEXT));
        settingList.add(new BaseListItem(setting_item_divider, "", BaseListItem.ItemType.TYPE_DIVIDER));
        settingList.add(new BaseListItem(setting_item_language, "", BaseListItem.ItemType.TYPE_TEXT));
        settingList.add(new BaseListItem(setting_item_divider, "", BaseListItem.ItemType.TYPE_DIVIDER));
        settingList.add(new BaseListItem(setting_item_report, "", BaseListItem.ItemType.TYPE_TEXT));
        settingList.add(new BaseListItem(setting_item_divider, "", BaseListItem.ItemType.TYPE_DIVIDER));
        settingList.add(new BaseListItem(setting_item_sign_out, "", BaseListItem.ItemType.TYPE_RED_TEXT));
        settingList.add(new BaseListItem(setting_item_divider, "", BaseListItem.ItemType.TYPE_DIVIDER));
        settingListAdapter = new BaseListItemAdapter(this, settingList);
        settingListView.setAdapter(settingListAdapter);
    }

//    @Override
//    public void onSuccess(Object content) {
//        viewUser(MmGlobal.getUserId());
//    }
//
//    @Override
//    public void onFailure(String message) {
//
//    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(UpdatePhotoEvent event) {
        Logger.i(TAG, "Success");
        viewUser(MmGlobal.getUserId());
    }

}
