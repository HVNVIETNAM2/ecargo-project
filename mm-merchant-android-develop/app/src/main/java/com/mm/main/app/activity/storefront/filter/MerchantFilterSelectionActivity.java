package com.mm.main.app.activity.storefront.filter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.filter.MerchantFilterSelectionListAdapter;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.view.MmSearchBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class MerchantFilterSelectionActivity extends AppCompatActivity implements MmSearchBar.MmSearchBarListener {

    public static final String EXTRA_HEADER = "extra_header";
    public static final String EXTRA_SELECTIONS = "selection";

    public static final String EXTRA_RESULT = "result";
    public static final String EXTRA_RESULT_DATA = "resultData";

    public static String EXTRA_MERCHANT_DATA = "merchantData";

    @Bind(R.id.listViewFilter)
    ListView listViewFilter;

    @Bind(R.id.searchView)
    MmSearchBar searchView;
    List<FilterListItem<Merchant>> filterList;

    @Bind(R.id.textViewHeader)
    TextView textViewHeader;

    @BindString(R.string.LB_CA_NUM_PRODUCTS_1)
    String LB_CA_NUM_PRODUCTS_1;

    @BindString(R.string.LB_CA_NUM_PRODUCTS_2)
    String LB_CA_NUM_PRODUCTS_2;

    @Bind(R.id.textViewProductNumber)
    TextView textViewProductNumber;

    MerchantFilterSelectionListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeNoActionBar);
        setContentView(R.layout.activity_brand_filter_selection);
        ButterKnife.bind(this);
        setupToolbar();

        Bundle bundle = (Bundle) getIntent().getBundleExtra(EXTRA_SELECTIONS);
        renderFilterList((List<FilterListItem<Merchant>>) bundle.getSerializable(EXTRA_MERCHANT_DATA));
//        ActivityUtil.setTitle(this, getIntent().getStringExtra(EXTRA_HEADER));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        textViewHeader.setText(getIntent().getStringExtra(EXTRA_HEADER));
        setupSearchView();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    private void setupSearchView() {
        searchView.setMmSearchBarListener(this);
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void finish() {
        super.finish();
    }

    private void renderFilterList(List<FilterListItem<Merchant>> filters) {
        filterList = filters;
        listAdapter = new MerchantFilterSelectionListAdapter(this, filterList, -1);
        listViewFilter.setAdapter(listAdapter);
        listViewFilter.setTextFilterEnabled(false);
        listViewFilter.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }

    @OnItemClick(R.id.listViewFilter)
    public void onItemClicked(int position) {
        listViewFilter.setItemChecked(position, listViewFilter.isItemChecked(position));
        listAdapter.setSelectedItemId(position);
//        submitSearch();
    }

    @OnClick(R.id.buttonOk)
    protected void proceedOk() {
        ActivityUtil.closeKeyboard(this);
        Intent intent = this.getIntent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_RESULT_DATA, createResultList());
        intent.putExtra(EXTRA_RESULT, bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    private ArrayList<Merchant> createResultList() {
        List<FilterListItem<Merchant>> filterListItemList = listAdapter.getOriginalData();
        ArrayList<Merchant> resultList = new ArrayList<>();

        for (int i = 0; i < filterListItemList.size(); i++) {
            if (filterListItemList.get(i).isSelected()) {
                resultList.add(filterListItemList.get(i).getT());
            }
        }

        return resultList;
    }

    private void resetSelection() {
        List<FilterListItem<Merchant>> filterListItemList = listAdapter.getOriginalData();

        for (int i = 0; i < filterListItemList.size(); i++) {
            filterListItemList.get(i).setSelected(false);
        }

        for (int i = 0; i < listViewFilter.getCount(); i++) {
            listViewFilter.setItemChecked(i, false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_brand_filter_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reset) {
//            proceedOk();
            resetSelection();
//            submitSearch();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEnterText(CharSequence s) {
        listAdapter.getFilter().filter(s);
    }

    @Override
    public void onCancelSearch() {
        listViewFilter.clearTextFilter();
    }

//    private void setNumberOfProduct(int number) {
//        textViewProductNumber.setText(LB_CA_NUM_PRODUCTS_1 + number +
//                LB_CA_NUM_PRODUCTS_2);
//    }

//    private void submitSearch() {
//        SearchServiceBuilder.searchStyleFilteredByEntryBrand(new MmCallBack<SearchResponse>(this) {
//            @Override
//            public void onSuccess(Response<SearchResponse> response, Retrofit retrofit) {
//                List<Style> suggestProductList = response.body().getPageData();
////                ProductFilterHelper.setFullStyleList(suggestProductList);
////                renderStyleList(viewHolderSuggestion);
//                setNumberOfProduct(suggestProductList.size());
//
//            }
//        }, getBrands());
//    }


    private Merchant[] getMerchants() {
        List<FilterListItem<Merchant>> filterListItemList = listAdapter.getOriginalData();
        ArrayList<Merchant> resultList = new ArrayList<>();

        for (int i = 0; i < filterListItemList.size(); i++) {
            if (filterListItemList.get(i).isSelected()) {
                resultList.add(filterListItemList.get(i).getT());
            }
        }

        Merchant[] resultMerchant = new Merchant[resultList.size()];
        resultList.toArray(resultMerchant);

        return resultMerchant;
    }

}
