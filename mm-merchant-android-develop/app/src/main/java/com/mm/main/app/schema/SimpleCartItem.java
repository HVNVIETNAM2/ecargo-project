package com.mm.main.app.schema;

/**
 * Created by ductranvt on 3/11/2016.
 */
public class SimpleCartItem {
    Integer SkuId;
    Integer Qty;

    public Integer getSkuId() {
        return SkuId;
    }

    public Integer getQty() {
        return Qty;
    }

    public SimpleCartItem(Integer skuId, Integer qty) {
        SkuId = skuId;
        Qty = qty;
    }
}
