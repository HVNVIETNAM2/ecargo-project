package com.mm.main.app.adapter.strorefront.friend;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.FriendRequestRvItem;
import com.mm.main.app.schema.User;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.utils.UiUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by haivu on 3/4/2016.
 */
public class RequestFriendRvAdapter extends RecyclerView.Adapter<RequestFriendRvAdapter.RequestFriendItemViewHolder> {

    public interface MyCallBack {

        void deleteRequest(int position);

        void acceptRequest(int position);

        void seeProfile(int position);
    }

    MyCallBack callback;
    Context context;
    List<FriendRequestRvItem> data;

    public RequestFriendRvAdapter(Context context, List<FriendRequestRvItem> data, MyCallBack callback) {
        this.context = context;
        this.data = data;
        this.callback = callback;
    }

    @Override
    public RequestFriendItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RequestFriendItemViewHolder viewHolder = null;

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_friend_list_item, parent, false);
        viewHolder = new RequestFriendItemViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RequestFriendItemViewHolder holder, final int position) {

        final FriendRequestRvItem item = data.get(position);

        upDateView(holder, item);

        holder.tvUserName.setText(item.getUser().getDisplayName());

        String url = PathUtil.getUserImageUrl(item.getUser().getProfileImage());
        Picasso.with(MyApplication.getContext()).load(url).placeholder(R.drawable.placeholder).into(holder.imgProfile);

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                item.setTypeAction(1);
                upDateView(holder, item);
                callback.deleteRequest(position);
            }
        });

        holder.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setTypeAction(2);
                upDateView(holder, item);
                callback.acceptRequest(position);
            }
        });

        holder.rlProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.seeProfile(position);
            }
        });

    }

    private void upDateView(RequestFriendItemViewHolder holder, FriendRequestRvItem item) {

        if (item.getTypeAction() == 0) {
            holder.btnDelete.setVisibility(View.VISIBLE);
            holder.btnAccept.setVisibility(View.VISIBLE);
            holder.tvDelete.setVisibility(View.GONE);

        } else {
            holder.btnDelete.setVisibility(View.GONE);
            holder.btnAccept.setVisibility(View.GONE);
            holder.tvDelete.setVisibility(View.VISIBLE);
            if (item.getTypeAction() == 1) { //delete
                String text = context.getString(R.string.LB_CA_IM_DELETED);
                holder.tvDelete.setText(text);

            } else {//accept
                String text = context.getString(R.string.LB_CA_IM_ACCEPTED);
                holder.tvDelete.setText(text);
            }
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class RequestFriendItemViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imgProfile)
        CircleImageView imgProfile;

        @Bind(R.id.tvUserName)
        TextView tvUserName;

        @Bind(R.id.btnAccept)
        ImageView btnAccept;

        @Bind(R.id.tvDelete)
        TextView tvDelete;

        @Bind(R.id.btnDelete)
        ImageView btnDelete;

        @Bind(R.id.rlProfile)
        RelativeLayout rlProfile;

        public RequestFriendItemViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            ButterKnife.bind(this, itemLayoutView);
        }
    }
}
