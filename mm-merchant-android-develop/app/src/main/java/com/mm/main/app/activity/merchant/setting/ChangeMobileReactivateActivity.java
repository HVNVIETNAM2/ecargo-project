package com.mm.main.app.activity.merchant.setting;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.mm.main.app.R;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.factory.ValidatorFactory;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.LifeCycleManager;
import com.mm.main.app.schema.request.ReactivateRequest;
import com.mm.main.app.schema.request.ResendRequest;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.ValidationUtil;
import com.mm.main.app.view.MmProgressDialog;

import java.util.concurrent.Callable;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

public class ChangeMobileReactivateActivity extends ActionBarActivity {
    public static final String TAG = ChangeMobileReactivateActivity.class.toString();
    @Bind(R.id.userNameEditText)
    EditText userNameEditText;
    @Bind(R.id.activationCodeEditText)
    EditText activationCodeEditText;

    @Bind(R.id.inputUserNameLayout)
    TextInputLayout inputUserNameLayout;
    @Bind(R.id.inputActivationCodeLayout)
    TextInputLayout inputActivationCodeLayout;

    @BindString(R.string.LB_CHANGE_MOBILE)
    String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mobile_reactivate);
        ButterKnife.bind(this);
        ActivityUtil.setTitle(this, title);
        if(!TextUtils.isEmpty(MmGlobal.getUserName())) {
            userNameEditText.setText(MmGlobal.getUserName());
        }

        setupValidation();
    }

    public void setupValidation() {
        userNameEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateUserName();
            }
        }, inputUserNameLayout));

        activationCodeEditText.setOnFocusChangeListener(ValidatorFactory.createOnFocusValidateListener(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return validateActivationCode();
            }
        }, inputActivationCodeLayout));
    }

    @OnClick(R.id.changeMobileResendActivationCodeTextView)
    public void resendActivationCode() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getAuthService()
                .resend(new ResendRequest(userNameEditText.getText().toString()))
                .enqueue(new MmCallBack<Boolean>(ChangeMobileReactivateActivity.this) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                        Toast.makeText(ChangeMobileReactivateActivity.this,
                                getResources().getText(R.string.MSG_SUC_RESEND_ACTIVATION_CODE),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @OnClick(R.id.actionButton)
    public void reactivate() {
        if (validateUserName() & validateActivationCode()) {
            MmProgressDialog.show(this);
            APIManager.getInstance().getAuthService()
                    .reactivate(new ReactivateRequest(userNameEditText.getText().toString(),
                            activationCodeEditText.getText().toString()))
                    .enqueue(new MmCallBack<Boolean>(ChangeMobileReactivateActivity.this) {
                        @Override
                        public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                            LifeCycleManager.relogin(ChangeMobileReactivateActivity.this);
                        }
                    });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private boolean validateUserName() {
        String error = null;
        String fieldName = getResources().getString(R.string.LB_EMAIL_OR_MOBILE);

        String userName = userNameEditText.getText().toString();
        if (ValidationUtil.isEmpty(userName)) {
            error = getResources().getString(R.string.MSG_ERR_FIELDNAME_PATTERN)
                    .replace(Constant.TEXT_REPLACE_FIELD_NAME, fieldName);
        }

        if (!ValidationUtil.isValidEmail(userName) && !ValidationUtil.isValidPhone(userName)) {
            error = getResources().getString(R.string.MSG_ERR_FIELDNAME_PATTERN)
                    .replace(Constant.TEXT_REPLACE_FIELD_NAME, fieldName);
        }

        ValidationUtil.setErrorMessage(inputUserNameLayout, error);

        return error == null;
    }


    private boolean validateActivationCode() {
        String error = null;

        String fieldName = getResources().getString(R.string.LB_ACTIVATION_CODE);

        if (activationCodeEditText.getText().toString().trim().isEmpty()) {
            error = getResources().getString(R.string.MSG_ERR_FIELDNAME_PATTERN)
                    .replace(Constant.TEXT_REPLACE_FIELD_NAME, fieldName);
        }

        ValidationUtil.setErrorMessage(inputActivationCodeLayout, error);

        return error == null;
    }
}
