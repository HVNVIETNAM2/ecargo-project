package com.mm.main.app.schema.request;

import com.mm.main.app.manager.LanguageManager;
import com.mm.main.app.schema.CartItem;
import com.mm.main.app.schema.SimpleCartItem;

import java.util.List;

/**
 * Created by ductranvt on 3/11/2016.
 */
public class OrderCreateRequest {
    String UserKey;
    String UserAddressKey;
    String CultureCode;
    List<SimpleCartItem> Skus;

    public OrderCreateRequest(String userKey, String userAddressKey, List<SimpleCartItem> skus) {
        UserKey = userKey;
        UserAddressKey = userAddressKey;
        Skus = skus;
        CultureCode = LanguageManager.getInstance().getCurrentCultureCode();
    }
}
