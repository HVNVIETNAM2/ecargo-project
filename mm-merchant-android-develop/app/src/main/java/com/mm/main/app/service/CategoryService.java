package com.mm.main.app.service;

import com.mm.main.app.schema.Category;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by henrytung on 24/11/2015.
 */
public interface CategoryService {

    String CATEGORY_PATH = "category";

    @GET(CATEGORY_PATH + "/list")
    Call<List<Category>> list(@Query("id") Integer id);
}
