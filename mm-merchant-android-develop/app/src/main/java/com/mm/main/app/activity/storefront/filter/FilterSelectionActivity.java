package com.mm.main.app.activity.storefront.filter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.adapter.strorefront.filter.FilterSelectionListAdapter;
import com.mm.main.app.listitem.FilterListItem;
import com.mm.main.app.schema.Size;
import com.mm.main.app.utils.ActivityUtil;
import com.mm.main.app.view.MmSearchBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class FilterSelectionActivity extends AppCompatActivity implements MmSearchBar.MmSearchBarListener {

    public static final String EXTRA_HEADER = "extra_header";
    public static final String EXTRA_SELECTIONS = "selection";

    public static final String EXTRA_RESULT = "result";
    public static final String EXTRA_RESULT_DATA = "resultData";

    @Bind(R.id.listViewFilter)
    ListView listViewFilter;

    @Bind(R.id.searchView)
    MmSearchBar searchView;
    List<FilterListItem<Size>> filterList;

    @Bind(R.id.textViewHeader)
    TextView textViewHeader;

    FilterSelectionListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(R.style.AppThemeNoActionBar);
        setContentView(R.layout.activity_filter_selection);
        ButterKnife.bind(this);
        setupToolbar();

        Bundle bundle = (Bundle)getIntent().getBundleExtra(EXTRA_SELECTIONS);
        renderFilterList((List<FilterListItem<Size>>) bundle.getSerializable(FilterActivity.EXTRA_TEXT_DATA));
//        ActivityUtil.setTitle(this, getIntent().getStringExtra(EXTRA_HEADER));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        textViewHeader.setText(getIntent().getStringExtra(EXTRA_HEADER));
        setupSearchView();
//        searchView.setIconifiedByDefault(false);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void setupSearchView() {
        searchView.setMmSearchBarListener(this);
    }

    private void setupToolbar() {
        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.mm_toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void finish() {
        super.finish();
    }

    private void renderFilterList(List<FilterListItem<Size>> filters) {
        filterList = filters;
        listAdapter = new FilterSelectionListAdapter(this, filterList, -1);
        listViewFilter.setAdapter(listAdapter);
        listViewFilter.setTextFilterEnabled(false);
        listViewFilter.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }

    @OnItemClick(R.id.listViewFilter)
    public void onItemClicked(int position) {
        listViewFilter.setItemChecked(position, listViewFilter.isItemChecked(position));
        listAdapter.setSelectedItemId(position);
    }

    private void proceedOk() {
        ActivityUtil.closeKeyboard(this);
        Intent intent = this.getIntent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_RESULT_DATA, createResultList());
        intent.putExtra(EXTRA_RESULT, bundle);
        setResult(RESULT_OK, intent);
//        finishAfterTransition();
        finish();
    }

    private ArrayList<Size> createResultList() {
        List<FilterListItem<Size>> filterListItemList = listAdapter.getOriginalData();
        ArrayList<Size> resultList = new ArrayList<>();

        for (int i = 0; i < filterListItemList.size(); i++) {
            if (filterListItemList.get(i).isSelected()) {
                resultList.add(filterListItemList.get(i).getT());
            }
        }

        return resultList;
    }

    private int convertToOriginalPosition(int position) {
        String item = (String)listAdapter.getItem(position);

        for (int i = 0; i < filterList.size(); i++) {
            if (item.equals(filterList.get(i))) {
                return i;
            }
        }
        return 0;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_filter_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_ok) {
            proceedOk();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEnterText(CharSequence s) {
        listAdapter.getFilter().filter(s);
    }

    @Override
    public void onCancelSearch() {
        listViewFilter.clearTextFilter();
    }
}