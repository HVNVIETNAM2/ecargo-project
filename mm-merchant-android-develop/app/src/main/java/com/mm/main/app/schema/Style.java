
package com.mm.main.app.schema;

import android.text.TextUtils;

import com.mm.main.app.manager.CacheManager;
import com.mm.main.app.utils.PathUtil;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Style implements Serializable {

    private Integer BadgeId;
    private String BadgeName;
    private String BadgeNameInvariant;
    private String BrandNameInvariant;
    private List<com.mm.main.app.schema.CategoryList> CategoryList = new ArrayList<com.mm.main.app.schema.CategoryList>();
    private List<com.mm.main.app.schema.CategoryPriorityList> CategoryPriorityList = new ArrayList<com.mm.main.app.schema.CategoryPriorityList>();
    private Integer GeoCountryId;
    private String GeoCountryName;
    private String GeoCountryNameInvariant;
    private Integer LaunchYear;
    private String ManufacturerName;
    private Integer PrimarySkuId;
    private Integer SeasonId;
    private String SeasonName;
    private String SeasonNameInvariant;
    private String SkuDescInvariant;
    private String SkuFeature;
    private String SkuFeatureInvariant;
    private String SkuNameInvariant;
    private String StatusNameInvariant;
    private String AvailableFrom;
    private String AvailableTo;
    private Integer BrandId;
    private String BrandName;
    private String BrandImage;
    private String BrandHeaderLogoImage;
    private String BrandSmallLogoImage;
    private List<com.mm.main.app.schema.ProductColor> ColorList = new ArrayList<>();
    private String ImageDefault;
    private List<ColorImage> ColorImageList = new ArrayList<>();
    private List<ImageData> DescriptionImageList = new ArrayList<>();
    private List<ImageData> FeaturedImageList = new ArrayList<>();
    private Integer MerchantId;
    private Double PriceRetail;
    private Double PriceSale;
    private Integer PrimaryCategoryId;
    private List<com.mm.main.app.schema.CategoryPath> PrimaryCategoryPathList = new ArrayList<>();
    private String SaleFrom;
    private String SaleTo;
    private List<com.mm.main.app.schema.ProductSize> SizeList = new ArrayList<>();
    private String SkuName;
    private String SkuDesc;
    private Integer StatusId;
    private String StatusName;
    private String StyleCode;
    private List<Sku> SkuList = new ArrayList<>();
    Date LastCreated;
    Integer IsNew;
    Integer IsSale;
    Integer totalLocationCount;

    /**
     * @return The AvailableFrom
     */
    public String getAvailableFrom() {
        return AvailableFrom;
    }

    /**
     * @param AvailableFrom The AvailableFrom
     */
    public void setAvailableFrom(String AvailableFrom) {
        this.AvailableFrom = AvailableFrom;
    }

    /**
     * @return The AvailableTo
     */
    public String getAvailableTo() {
        return AvailableTo;
    }

    /**
     * @param AvailableTo The AvailableTo
     */
    public void setAvailableTo(String AvailableTo) {
        this.AvailableTo = AvailableTo;
    }

    /**
     * @return The BrandId
     */
    public Integer getBrandId() {
        return BrandId;
    }

    /**
     * @param BrandId The BrandId
     */
    public void setBrandId(Integer BrandId) {
        this.BrandId = BrandId;
    }

    /**
     * @return The BrandName
     */
    public String getBrandName() {
        return BrandName;
    }

    /**
     * @param BrandName The BrandName
     */
    public void setBrandName(String BrandName) {
        this.BrandName = BrandName;
    }

    /**
     * @return The ColorList
     */
    public List<com.mm.main.app.schema.ProductColor> getColorList() {
        return ColorList;
    }

    /**
     * @param ColorList The ColorList
     */
    public void setColorList(List<com.mm.main.app.schema.ProductColor> ColorList) {
        this.ColorList = ColorList;
    }

    /**
     * @return The ImageDefault
     */
    public String getImageDefault() {
        return ImageDefault;
    }

    /**
     * @param ImageDefault The ImageDefault
     */
    public void setImageDefault(String ImageDefault) {
        this.ImageDefault = ImageDefault;
    }

    /**
     * @return The MerchantId
     */
    public Integer getMerchantId() {
        return MerchantId;
    }

    /**
     * @param MerchantId The MerchantId
     */
    public void setMerchantId(Integer MerchantId) {
        this.MerchantId = MerchantId;
    }

    /**
     * @return The PriceRetail
     */
    public Double getPriceRetail() {
        return PriceRetail;
    }

    /**
     * @param PriceRetail The PriceRetail
     */
    public void setPriceRetail(Double PriceRetail) {
        this.PriceRetail = PriceRetail;
    }

    /**
     * @return The PriceSale
     */
    public Double getPriceSale() {
        return PriceSale;
    }

    /**
     * @param PriceSale The PriceSale
     */
    public void setPriceSale(Double PriceSale) {
        this.PriceSale = PriceSale;
    }

    /**
     * @return The PrimaryCategoryId
     */
    public Integer getPrimaryCategoryId() {
        return PrimaryCategoryId;
    }

    /**
     * @param PrimaryCategoryId The PrimaryCategoryId
     */
    public void setPrimaryCategoryId(Integer PrimaryCategoryId) {
        this.PrimaryCategoryId = PrimaryCategoryId;
    }

    /**
     * @return The PrimaryCategoryPathList
     */
    public List<com.mm.main.app.schema.CategoryPath> getPrimaryCategoryPathList() {
        return PrimaryCategoryPathList;
    }

    /**
     * @param PrimaryCategoryPathList The PrimaryCategoryPathList
     */
    public void setPrimaryCategoryPathList(List<com.mm.main.app.schema.CategoryPath> PrimaryCategoryPathList) {
        this.PrimaryCategoryPathList = PrimaryCategoryPathList;
    }

    /**
     * @return The SaleFrom
     */
    public String getSaleFrom() {
        return SaleFrom;
    }

    /**
     * @param SaleFrom The SaleFrom
     */
    public void setSaleFrom(String SaleFrom) {
        this.SaleFrom = SaleFrom;
    }

    /**
     * @return The SaleTo
     */
    public String getSaleTo() {
        return SaleTo;
    }

    /**
     * @param SaleTo The SaleTo
     */
    public void setSaleTo(String SaleTo) {
        this.SaleTo = SaleTo;
    }

    /**
     * @return The SizeList
     */
    public List<com.mm.main.app.schema.ProductSize> getSizeList() {
        return SizeList;
    }

    /**
     * @param SizeList The SizeList
     */
    public void setSizeList(List<com.mm.main.app.schema.ProductSize> SizeList) {
        this.SizeList = SizeList;
    }

    /**
     * @return The SkuName
     */
    public String getSkuName() {
        return SkuName;
    }

    /**
     * @param SkuName The SkuName
     */
    public void setSkuName(String SkuName) {
        this.SkuName = SkuName;
    }

    /**
     * @return The StatusId
     */
    public Integer getStatusId() {
        return StatusId;
    }

    /**
     * @param StatusId The StatusId
     */
    public void setStatusId(Integer StatusId) {
        this.StatusId = StatusId;
    }

    /**
     * @return The StatusName
     */
    public String getStatusName() {
        return StatusName;
    }

    /**
     * @param StatusName The StatusName
     */
    public void setStatusName(String StatusName) {
        this.StatusName = StatusName;
    }

    /**
     * @return The StyleCode
     */
    public String getStyleCode() {
        return StyleCode;
    }

    /**
     * @param StyleCode The StyleCode
     */
    public void setStyleCode(String StyleCode) {
        this.StyleCode = StyleCode;
    }

    public List<ColorImage> getColorImageList() {
        return ColorImageList;
    }

    public void setColorImageList(List<ColorImage> colorImageList) {
        ColorImageList = colorImageList;
    }

    public List<ImageData> getDescriptionImageList() {
        return DescriptionImageList;
    }

    public void setDescriptionImageList(List<ImageData> descriptionImageList) {
        DescriptionImageList = descriptionImageList;
    }

    public List<ImageData> getFeaturedImageList() {
        return FeaturedImageList;
    }

    public void setFeaturedImageList(List<ImageData> featuredImageList) {
        FeaturedImageList = featuredImageList;
    }

    public String getSkuDesc() {
        return SkuDesc;
    }

    public void setSkuDesc(String skuDesc) {
        SkuDesc = skuDesc;
    }

    public List<Sku> getSkuList() {
        return SkuList;
    }

    public void setSkuList(List<Sku> skuList) {
        SkuList = skuList;
    }

    public Integer getBadgeId() {
        return BadgeId;
    }

    public void setBadgeId(Integer badgeId) {
        BadgeId = badgeId;
    }

    public String getBadgeName() {
        return BadgeName;
    }

    public void setBadgeName(String badgeName) {
        BadgeName = badgeName;
    }

    public String getBadgeNameInvariant() {
        return BadgeNameInvariant;
    }

    public void setBadgeNameInvariant(String badgeNameInvariant) {
        BadgeNameInvariant = badgeNameInvariant;
    }

    public String getBrandNameInvariant() {
        return BrandNameInvariant;
    }

    public void setBrandNameInvariant(String brandNameInvariant) {
        BrandNameInvariant = brandNameInvariant;
    }

    public List<com.mm.main.app.schema.CategoryList> getCategoryList() {
        return CategoryList;
    }

    public void setCategoryList(List<com.mm.main.app.schema.CategoryList> categoryList) {
        CategoryList = categoryList;
    }

    public List<com.mm.main.app.schema.CategoryPriorityList> getCategoryPriorityList() {
        return CategoryPriorityList;
    }

    public void setCategoryPriorityList(List<com.mm.main.app.schema.CategoryPriorityList> categoryPriorityList) {
        CategoryPriorityList = categoryPriorityList;
    }

    public Integer getGeoCountryId() {
        return GeoCountryId;
    }

    public void setGeoCountryId(Integer geoCountryId) {
        GeoCountryId = geoCountryId;
    }

    public String getGeoCountryName() {
        return GeoCountryName;
    }

    public void setGeoCountryName(String geoCountryName) {
        GeoCountryName = geoCountryName;
    }

    public String getGeoCountryNameInvariant() {
        return GeoCountryNameInvariant;
    }

    public void setGeoCountryNameInvariant(String geoCountryNameInvariant) {
        GeoCountryNameInvariant = geoCountryNameInvariant;
    }

    public Integer getLaunchYear() {
        return LaunchYear;
    }

    public void setLaunchYear(Integer launchYear) {
        LaunchYear = launchYear;
    }

    public String getManufacturerName() {
        return ManufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        ManufacturerName = manufacturerName;
    }

    public Integer getPrimarySkuId() {
        return PrimarySkuId;
    }

    public void setPrimarySkuId(Integer primarySkuId) {
        PrimarySkuId = primarySkuId;
    }

    public Integer getSeasonId() {
        return SeasonId;
    }

    public void setSeasonId(Integer seasonId) {
        SeasonId = seasonId;
    }

    public String getSeasonName() {
        return SeasonName;
    }

    public void setSeasonName(String seasonName) {
        SeasonName = seasonName;
    }

    public String getSeasonNameInvariant() {
        return SeasonNameInvariant;
    }

    public void setSeasonNameInvariant(String seasonNameInvariant) {
        SeasonNameInvariant = seasonNameInvariant;
    }

    public String getSkuDescInvariant() {
        return SkuDescInvariant;
    }

    public void setSkuDescInvariant(String skuDescInvariant) {
        SkuDescInvariant = skuDescInvariant;
    }

    public String getSkuFeature() {
        return SkuFeature;
    }

    public void setSkuFeature(String skuFeature) {
        SkuFeature = skuFeature;
    }

    public String getSkuFeatureInvariant() {
        return SkuFeatureInvariant;
    }

    public void setSkuFeatureInvariant(String skuFeatureInvariant) {
        SkuFeatureInvariant = skuFeatureInvariant;
    }

    public String getSkuNameInvariant() {
        return SkuNameInvariant;
    }

    public void setSkuNameInvariant(String skuNameInvariant) {
        SkuNameInvariant = skuNameInvariant;
    }

    public String getStatusNameInvariant() {
        return StatusNameInvariant;
    }

    public void setStatusNameInvariant(String statusNameInvariant) {
        StatusNameInvariant = statusNameInvariant;
    }

    public String getBrandImage() {
        return BrandImage;
    }

    public void setBrandImage(String brandImage) {
        BrandImage = brandImage;
    }

    public String getBrandHeaderLogoImage() {
        return BrandHeaderLogoImage;
    }

    public void setBrandHeaderLogoImage(String brandHeaderLogoImage) {
        BrandHeaderLogoImage = brandHeaderLogoImage;
    }

    public String getBrandSmallLogoImage() {
        return BrandSmallLogoImage;
    }

    public void setBrandSmallLogoImage(String brandSmallLogoImage) {
        BrandSmallLogoImage = brandSmallLogoImage;
    }

    public double getPrice() {
        if (PriceSale != 0) {
            return PriceSale;
        } else {
            return PriceRetail;
        }
    }

    public Sku getSku(ProductSize size, ProductColor color) {

        if (SizeList.size() == 0 && color != null) {
            for (Sku sku : SkuList) {
                if (sku.getColorId().intValue() == color.getColorId().intValue()) {
                    return sku;
                }
            }
        }

        if (size != null && ColorList.size() == 0) {
            for (Sku sku : SkuList) {
                if (sku.getSizeId().intValue() == size.getSizeId().intValue()) {
                    return sku;
                }
            }
        }

        if (size != null && color != null) {
            for (Sku sku : SkuList) {
                if (sku.getSizeId().intValue() == size.getSizeId().intValue() && sku.getColorId().intValue() == color.getColorId().intValue()) {
                    return sku;
                }
            }
        }

        return null;
    }

    public Boolean isWished() {
        CartItem cartItem = CacheManager.getInstance().wishlistItemForStyle(this);
        return cartItem != null;
    }

    public Date getLastCreated() {
        return LastCreated;
    }

    public void setLastCreated(Date lastCreated) {
        LastCreated = lastCreated;
    }

    public Integer getIsNew() {
        return IsNew;
    }

    public void setIsNew(Integer isNew) {
        IsNew = isNew;
    }

    public Integer getIsSale() {
        return IsSale;
    }

    public void setIsSale(Integer isSale) {
        IsSale = isSale;
    }

    public Integer getTotalLocationCount() {
        return totalLocationCount;
    }

    public void setTotalLocationCount(Integer totalLocationCount) {
        this.totalLocationCount = totalLocationCount;
    }

    public Sku getLowestPriceSku() {

        List<Sku> list = getSkuList();

        if (list.size() > 0) {

            Sku target = list.get(0);
            double lowestPrice = target.getPriceRetail();

            for (Sku sku : list) {

                if (sku.getPriceRetail() > 0 && sku.getPriceRetail() < lowestPrice) {
                    lowestPrice = sku.getPriceRetail();
                    target = sku;
                }

                if (sku.getPriceSale() > 0 && sku.getPriceSale() < lowestPrice) {
                    lowestPrice = sku.getPriceSale();
                    target = sku;
                }

            }
            return target;

        }
        return null;

    }

    public Sku findSkuBySkuId(Integer skuId) {
        List<Sku> list = getSkuList();
        for (Sku sku : list) {
            if (sku.getSkuId().intValue() == skuId.intValue()) {
                return sku;
            }
        }
        return null;
    }

    public ProductColor findColorByColorKey(String colorKey) {
        if (TextUtils.isEmpty(colorKey)) {
            return null;
        }
        for (ProductColor color : ColorList) {
            if (color.getColorKey().toUpperCase().equals(colorKey.toUpperCase())) {
                return color;
            }
        }
        return null;
    }

    public ProductSize findSizeBySizeId(Integer sizeId) {
        for (ProductSize size : SizeList) {
            if (size.getSizeId().intValue() == sizeId.intValue()) {
                return size;
            }
        }
        return null;
    }

    public String getImageBaseColor(String colorKey) {
        String imageUrl = "";
        if (!TextUtils.isEmpty(colorKey)) {
            for (ColorImage colorImage :  ColorImageList) {
                if (colorKey.toUpperCase().equals(colorImage.getColorKey().toUpperCase())) {
                    imageUrl = PathUtil.getProductImageUrl(colorImage.getImageKey());
                    break;
                }
            }
        }
        if (TextUtils.isEmpty(imageUrl) && FeaturedImageList != null && FeaturedImageList.size() != 0) {
            imageUrl = PathUtil.getProductImageUrl(FeaturedImageList.get(0).getImageKey());
        }
        return imageUrl;
    }

    public String getImageBaseColor(ProductColor color) {
        String colorKey = "";
        if (color != null) {
            colorKey = color.getColorKey();
        }
        return getImageBaseColor(colorKey);
    }
}
