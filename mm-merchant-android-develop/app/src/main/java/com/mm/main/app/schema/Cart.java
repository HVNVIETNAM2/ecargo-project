package com.mm.main.app.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ductranvt on 1/15/2016.
 */
public class Cart implements  Serializable{
    Integer CartId;
    String CartKey;
    String WishlistKey;
    Integer CartTypeId;
    Integer UserId;
    String UserKey;
    Date LastCreated;
    Date LastModified;
    List<CartMerchant> MerchantList = new ArrayList<CartMerchant>();


    public class CartMerchant implements Serializable{
        Integer MerchantId;
        String MerchantName;
        String MerchantNameInvariant;
        String MerchantImage;

        List<CartItem> ItemList = new ArrayList<CartItem>();

        public Integer getMerchantId() {
            return MerchantId;
        }

        public String getMerchantName() {
            return MerchantName;
        }

        public String getMerchantNameInvariant() {
            return MerchantNameInvariant;
        }

        public String getMerchantImage() {
            return MerchantImage;
        }

        public List<CartItem> getItemList() {
            return ItemList;
        }
    }

    public Integer getCartId() {
        return CartId;
    }

    public Integer getUserId() {
        return UserId;
    }

    public Date getLastCreated() {
        return LastCreated;
    }

    public Date getLastModified() {
        return LastModified;
    }

    public List<CartMerchant> getMerchantList() {
        return MerchantList;
    }

    public String getCartKey() {
        return CartKey;
    }

    public Integer getCartTypeId() {
        return CartTypeId;
    }

    public String getWishlistKey() {
        return WishlistKey;
    }

    public String getUserKey() {
        return UserKey;
    }
}
