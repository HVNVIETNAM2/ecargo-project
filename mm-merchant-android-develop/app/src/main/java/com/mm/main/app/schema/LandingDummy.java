package com.mm.main.app.schema;

/**
 * Created by haivu on 3/3/2016.
 */
public class LandingDummy {

    private int Profile;
    private String UserName;
    private String ChatContent;
    private String Time;
    private boolean IsUser;

    public boolean isUser() {
        return IsUser;
    }

    public void setIsUser(boolean isUser) {
        IsUser = isUser;
    }

    public int getProfile() {
        return Profile;
    }

    public void setProfile(int profile) {
        Profile = profile;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getChatContent() {
        return ChatContent;
    }

    public void setChatContent(String chatContent) {
        ChatContent = chatContent;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}
