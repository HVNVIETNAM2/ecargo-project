package com.mm.main.app.schema.request;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class CartCreateRequest {
    String CultureCode;
    String UserKey;

    public String getCultureCode() {
        return CultureCode;
    }

    public void setCultureCode(String cultureCode) {
        CultureCode = cultureCode;
    }

    public String getUserKey() {
        return UserKey;
    }

    public void setUserKey(String userKey) {
        UserKey = userKey;
    }

    public CartCreateRequest(String cultureCode, String userKey) {
        CultureCode = cultureCode;
        UserKey = userKey;
    }
}
