package com.mm.main.app.utils;

import android.animation.Animator;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;

/**
 * Created by henrytung on 21/12/2015.
 */
public class AnimateUtil {

    public static void fadeOut(final View v, Animator.AnimatorListener animationListener) {
        fadeOut(v, 500, animationListener);
    }

    public static void fadeOut(final View v, int time, Animator.AnimatorListener animationListener) {
        v.animate()
                .alpha(0.0f)
                .setDuration(500)
                .setListener(animationListener);
    }

    public static void fadeIn(final View v, Animator.AnimatorListener animationListener) {
        fadeIn(v, 500, animationListener);
    }

    public static void fadeIn(final View v, int time, Animator.AnimatorListener animationListener) {
        v.animate()
                .alpha(1.0f)
                .setDuration(500)
                .setListener(animationListener);
    }

    public static void expand(final View v, Animation.AnimationListener animationListener) {
        expand(v, 2, 1, 0, animationListener);
    }


    public static void expand(final View v, int speed, final int itemCount, final int itemHeight, Animation.AnimationListener animationListener) {
        v.measure(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = itemHeight == 0 ? v.getMeasuredHeight()*itemCount : itemHeight;

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                 int height = interpolatedTime == 1
                        ? RelativeLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                if(itemHeight != 0){
                    height = interpolatedTime == 1
                            ? itemHeight
                            : (int)(targetHeight * interpolatedTime);
                }
                v.getLayoutParams().height = height;
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        if (animationListener != null) {
            a.setAnimationListener(animationListener);
        }

        // 0.5dp/ms
        a.setDuration((int)((targetHeight / v.getContext().getResources().getDisplayMetrics().density)*speed));
        v.startAnimation(a);
    }

    public static void collapse(final View v, Animation.AnimationListener animationListener) {
        collapse(v, 1, animationListener);
    }

    public static void collapse(final View v) {
        if (v.getVisibility() == View.VISIBLE) {
            collapse(v, 10, null);
            v.setVisibility(View.INVISIBLE);
        }
    }

    public static void collapse(final View v, int speed, Animation.AnimationListener animationListener) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
//                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        if (animationListener != null) {
            a.setAnimationListener(animationListener);
        }

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density)*speed);
        v.startAnimation(a);
    }
}
