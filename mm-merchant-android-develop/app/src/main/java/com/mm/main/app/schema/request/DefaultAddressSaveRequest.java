package com.mm.main.app.schema.request;

/**
 * Created by ductranvt on 3/10/2016.
 */
public class DefaultAddressSaveRequest {
    String UserKey;
    String UserAddressKey;

    public DefaultAddressSaveRequest(String userKey, String userAddressKey) {
        UserKey = userKey;
        UserAddressKey = userAddressKey;
    }
}
