package com.mm.main.app.schema;

/**
 * Created by haivu on 2/24/2016.
 */
public class ForgotPassword {
    String MobileVerificationId;
    String MobileVerificationToken;
    String MobileCode;
    String MobileNumber;
    String Password;

    public ForgotPassword(String mobileVerificationId, String mobileVerificationToken, String mobileCode, String mobileNumber, String password) {
        MobileVerificationId = mobileVerificationId;
        MobileVerificationToken = mobileVerificationToken;
        MobileCode = mobileCode;
        MobileNumber = mobileNumber;
        Password = password;
    }

    public ForgotPassword() {

    }

    public String getMobileCode() {
        return MobileCode;
    }

    public void setMobileCode(String mobileCode) {
        MobileCode = mobileCode;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getMobileVerificationId() {
        return MobileVerificationId;
    }

    public void setMobileVerificationId(String mobileVerificationId) {
        MobileVerificationId = mobileVerificationId;
    }

    public String getMobileVerificationToken() {
        return MobileVerificationToken;
    }

    public void setMobileVerificationToken(String mobileVerificationToken) {
        MobileVerificationToken = mobileVerificationToken;
    }

}
