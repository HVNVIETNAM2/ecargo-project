package com.mm.main.app.listitem;

import android.graphics.Bitmap;

import com.mm.main.app.schema.Category;

/**
 * Created by henrytung on 17/12/2015.
 */
public class DiscoverCategoryListItem {
    String key;
    String title;
    boolean open;
    Bitmap openedImage;
    Category category;

    public DiscoverCategoryListItem(String key, String title, Category category) {
        this.title = title;
        this.key = key;
        this.category = category;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public Bitmap getOpenedImage() {
        return openedImage;
    }

    public void setOpenedImage(Bitmap openedImage) {
        this.openedImage = openedImage;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
