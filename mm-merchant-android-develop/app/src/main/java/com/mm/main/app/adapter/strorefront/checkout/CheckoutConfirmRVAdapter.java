package com.mm.main.app.adapter.strorefront.checkout;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.schema.Address;
import com.mm.main.app.utils.PathUtil;
import com.mm.main.app.activity.storefront.checkout.AddAddressActivity;
import com.mm.main.app.activity.storefront.checkout.AddressSelectionActivity;
import com.mm.main.app.application.MyApplication;
import com.mm.main.app.listitem.CheckoutConfirmMerchantRvItem;
import com.mm.main.app.listitem.CheckoutConfirmMerchantTotalPriceRvItem;
import com.mm.main.app.listitem.CheckoutConfirmProductRvItem;
import com.mm.main.app.listitem.CheckoutConfirmRvItem;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import com.mm.main.app.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ductranvt on 1/18/2016.
 */
public class CheckoutConfirmRVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<CheckoutConfirmRvItem> items;
    Activity activity;
    String cartKey;
    NumberFormat format;
    Address address;

    public CheckoutConfirmRVAdapter(Activity activity, List<CheckoutConfirmRvItem> items, String cartKey) {
        this.activity = activity;
        this.items = items;
        this.cartKey = cartKey;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().ordinal();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        format = NumberFormat.getCurrencyInstance(Locale.CHINA);
        format.setMaximumFractionDigits(0);

        CheckoutConfirmRvItem.ItemType itemType = CheckoutConfirmRvItem.ItemType.values()[viewType];

        RecyclerView.ViewHolder viewHolder = null;

        switch (itemType) {
            case TYPE_SHIPPING_ADDRESS:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkout_confirm_shipping_address_item_view, parent, false);
                viewHolder = new ShippingAddressViewHolder(view);
                break;

            case TYPE_MERCHANT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkout_confirm_brand_item_view, parent, false);
                viewHolder = new MerchantViewHolder(view);
                break;

            case TYPE_PRODUCT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkout_confirm_item_view, parent, false);
                viewHolder = new ProductViewHolder(view);
                break;

            case TYPE_MERCHANT_TOTAL_PRICE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkout_confirm_total_price_for_brand_item_view, parent, false);
                viewHolder = new MerChantTotalPriceViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        final CheckoutConfirmRvItem item = items.get(position);

        switch (item.getType()) {
            case TYPE_MERCHANT:
                MerchantViewHolder merchantViewHolder = (MerchantViewHolder) viewHolder;

                merchantViewHolder.textViewDesc.setText(((CheckoutConfirmMerchantRvItem) item).getMerchant().getMerchantName());

                if (((CheckoutConfirmMerchantRvItem) item).getMerchant().getMerchantImage() != null) {
                    Picasso.with(MyApplication.getContext()).load(PathUtil.getMerchantImageUrl(((CheckoutConfirmMerchantRvItem) item).getMerchant().getMerchantImage())).into(merchantViewHolder.imageView);
                }

                break;

            case TYPE_PRODUCT:
                final ProductViewHolder productViewHolder = (ProductViewHolder) viewHolder;

                productViewHolder.productName.setText(((CheckoutConfirmProductRvItem) item).getProduct().getSkuName());
                productViewHolder.productColor.setText(((CheckoutConfirmProductRvItem) item).getProduct().getSkuColor());
                productViewHolder.productSize.setText(((CheckoutConfirmProductRvItem) item).getProduct().getSizeName());


                double retailPrice = ((CheckoutConfirmProductRvItem) item).getProduct().getPriceRetail();
                double salePrice = ((CheckoutConfirmProductRvItem) item).getProduct().getPriceSale();

                if (salePrice > 0) { //exits sale price
                    productViewHolder.productPrice.setText(format.format(salePrice));
                    productViewHolder.productPrice.setTextColor(activity.getResources().getColor(R.color.primary1));
                    productViewHolder.originalPrice.setVisibility(View.VISIBLE);
                    productViewHolder.originalPrice.setText(format.format(retailPrice));
                    productViewHolder.originalPrice.setPaintFlags(productViewHolder.originalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                } else {
                    productViewHolder.productPrice.setText(format.format(retailPrice));
                    productViewHolder.productPrice.setTextColor(activity.getResources().getColor(R.color.secondary2));
                    productViewHolder.originalPrice.setVisibility(View.GONE);
                }

                Picasso.with(MyApplication.getContext()).load(PathUtil.getProductImageUrl(((CheckoutConfirmProductRvItem) item).getProduct().getProductImage())).into(productViewHolder.productImage);

                Picasso.with(MyApplication.getContext()).load(PathUtil.getBrandImageUrl(((CheckoutConfirmProductRvItem) item).getProduct().getBrandImage())).into(productViewHolder.brandImage);

                productViewHolder.quantity.setText("X" + ((CheckoutConfirmProductRvItem) item).getProduct().getQty());

                break;

            case TYPE_SHIPPING_ADDRESS:
                ShippingAddressViewHolder addressViewHolder = (ShippingAddressViewHolder) viewHolder;

                addressViewHolder.viewParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent;

                        if (address != null) {
                            intent = new Intent(activity, AddressSelectionActivity.class);
                            intent.putExtra(AddressSelectionActivity.SELECTED_ADDRESS_KEY, address.getUserAddressKey());
                        } else {
                            intent = new Intent(activity, AddAddressActivity.class);
                        }

                        activity.startActivityForResult(intent, AddressSelectionActivity.ADD_ADDRESS_CODE_REQUEST);
                    }
                });

                if (address != null) {
                    addressViewHolder.txtReceiver.setText(address.getRecipientName());
                    addressViewHolder.txtAddress.setText(address.getFullAddress());
                    addressViewHolder.txtPhone.setText(address.getFullPhoneNumber());
                } else {
                    addressViewHolder.txtReceiver.setText("");
                    addressViewHolder.txtAddress.setText(activity.getResources().getString(R.string.LB_CA_NEW_SHIPPING_ADDR));
                    addressViewHolder.txtPhone.setText("");
                }

                break;

            case TYPE_MERCHANT_TOTAL_PRICE:
                MerChantTotalPriceViewHolder priceViewHolder = (MerChantTotalPriceViewHolder) viewHolder;
                double merchant_total_price = 0;

                CheckoutConfirmMerchantRvItem merchant = ((CheckoutConfirmMerchantTotalPriceRvItem) item).getMerchant();
                List<CheckoutConfirmProductRvItem> products = merchant.getProductItems();

                for (CheckoutConfirmProductRvItem cartItem : products) {
                    merchant_total_price += cartItem.getProduct().getPrice() * cartItem.getProduct().getQty();
                }
                priceViewHolder.price.setText(format.format(merchant_total_price));
                ((CheckoutConfirmMerchantTotalPriceRvItem) item).setTotalPrice(merchant_total_price);

                break;
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class ProductViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.shopping_cart_product_name)
        TextView productName;

        @Bind(R.id.shopping_cart_color)
        TextView productColor;

        @Bind(R.id.shopping_cart_size)
        TextView productSize;

        @Bind(R.id.shopping_cart_image)
        ImageView productImage;

        @Bind(R.id.product_brand_image)
        ImageView brandImage;

        @Bind(R.id.shopping_cart_price)
        TextView productPrice;

        @Bind(R.id.original_price)
        TextView originalPrice;

        @Bind(R.id.quantity)
        TextView quantity;

        public ProductViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            ButterKnife.bind(this, itemLayoutView);
        }
    }

    public static class MerchantViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.shopping_cart_image_brand)
        public ImageView imageView;

        @Bind(R.id.textViewDesc)
        public TextView textViewDesc;

        public MerchantViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            ButterKnife.bind(this, itemLayoutView);
        }
    }

    public static class ShippingAddressViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.viewParent)
        public RelativeLayout viewParent;

        @Bind(R.id.txtReceiver)
        public TextView txtReceiver;

        @Bind(R.id.txtAddress)
        public TextView txtAddress;

        @Bind(R.id.txtPhone)
        public TextView txtPhone;

        public ShippingAddressViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            ButterKnife.bind(this, itemLayoutView);
        }
    }

    public static class MerChantTotalPriceViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.price)
        public TextView price;

        @Bind(R.id.comment_box)
        public EditText comment_box;

        @Bind(R.id.edFapiao)
        EditText edFapiao;

        public MerChantTotalPriceViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            ButterKnife.bind(this, itemLayoutView);
        }
    }
}




