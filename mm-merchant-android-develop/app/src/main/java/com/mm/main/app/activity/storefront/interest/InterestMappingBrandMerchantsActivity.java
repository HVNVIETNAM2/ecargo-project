package com.mm.main.app.activity.storefront.interest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.adapter.strorefront.animation.AlphaInAnimationAdapter;
import com.mm.main.app.adapter.strorefront.animation.ScaleInAnimationAdapter;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.listitem.BrandMerchantsListItem;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.LifeCycleManager;
import com.mm.main.app.schema.Follow;
import com.mm.main.app.schema.Merchant;
import com.mm.main.app.utils.CenterLockListener;
import com.mm.main.app.utils.CircleProcessUtil;
import com.mm.main.app.utils.FadeInAnimator;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.UiUtil;
import com.mm.main.app.view.MmProgressDialog;
import com.mm.main.app.adapter.strorefront.interest.InterestBrandMerchantsRvAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Response;
import retrofit.Retrofit;

public class InterestMappingBrandMerchantsActivity extends AppCompatActivity {

    @Bind(R.id.numRecommendedTextView)
    TextView numRecommendedTextView;

    @Bind(R.id.nextStepButton)
    Button nextStepButton;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.circleProcess)
    CircleProcessUtil circleProcess;

    @Bind(R.id.centerAnimateView)
    CircleImageView centerAnimateView;


    private ArrayList<BrandMerchantsListItem> brandMerchantsListItems;
    private InterestBrandMerchantsRvAdapter adapter;
    private int percentProcess;
    private int numSelectedItem;
    private int centerItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_mapping_brand_merchants);
        ButterKnife.bind(this);
        percentProcess = 0;
        numSelectedItem = 0;
        circleProcess.setPercent(percentProcess);
        getBrandMerchantsService();
        setUpNextButton(4);
    }

    private void setUpNextButton(int minSelected) {
        if (numSelectedItem >= minSelected) {
            nextStepButton.setEnabled(true);
            nextStepButton.setAlpha(1.0f);
        } else {
            nextStepButton.setEnabled(false);
            nextStepButton.setAlpha(0.5f);
        }
    }

    @OnClick(R.id.nextStepButton)
    public void onClickNextStepButton() {
        String brandMerchantKey = "";

        for (int i = 0; i < brandMerchantsListItems.size(); i++) {
            if (brandMerchantsListItems.get(i).getIsSelected()) {
                brandMerchantKey += brandMerchantsListItems.get(i).getMerchant().getMerchantId();
                if (i != brandMerchantsListItems.size() - 1) {
                    brandMerchantKey += ",";
                }
            }
        }
        saveFollow(brandMerchantKey);
    }

    @OnClick(R.id.skipTextView)
    public void onClickSkipTextView() {

        int size = Math.min(Constant.NUM_FOLLOWER_SKIP, brandMerchantsListItems.size());

        String listFollower = "";
        for (int i = 0; i < size; i++) {
            listFollower += brandMerchantsListItems.get(i).getMerchant().getMerchantId();
            if (i != size - 1) {
                listFollower += ",";
            }
        }
        saveFollow(listFollower);
    }

    private void saveFollow(String listFollower) {
        MmProgressDialog.show(getApplicationContext());
        Follow follow = new Follow();
        follow.setUserKey(MmGlobal.getUserKey());
        follow.setToMerchantId(listFollower);

        APIManager.getInstance().getFollowService().saveFollowMerchant(follow)
                .enqueue(new MmCallBack<Boolean>(this) {
                    @Override
                    public void onSuccess(Response<Boolean> response, Retrofit retrofit) {
                        if (response.body().booleanValue()) {
                            LifeCycleManager.cleanStart(InterestMappingBrandMerchantsActivity.this, StorefrontMainActivity.class, null);
                        }
                    }
                });
    }

    public void getBrandMerchantsService() {
        MmProgressDialog.show(this);
        APIManager.getInstance().getTagService().merchant(MmGlobal.getUserKey())
                .enqueue(new MmCallBack<List<Merchant>>(InterestMappingBrandMerchantsActivity.this) {
                    @Override
                    public void onSuccess(Response<List<Merchant>> response, Retrofit retrofit) {
                        brandMerchantsListItems = new ArrayList<>();
                        List<Merchant> merchants = response.body();
                        for (Merchant item : merchants) {
                            brandMerchantsListItems.add(new BrandMerchantsListItem(item));
                        }
                        setUpView(4);
                    }
                });
    }

    private void setUpView(int minSelected) {

        if (brandMerchantsListItems != null) {
            numRecommendedTextView.setText(brandMerchantsListItems.size() + "");
            //Set up adapter.
            if (brandMerchantsListItems.size() > 0) {
                setUpAdapter(minSelected);
            }
        }
    }

    private void setUpAdapter(final int minSelected) {

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setItemAnimator(new FadeInAnimator());

        adapter = new InterestBrandMerchantsRvAdapter(getApplicationContext(), brandMerchantsListItems, recyclerView, new InterestBrandMerchantsRvAdapter.OnItemListener() {
            @Override
            public void onClick(boolean isSelected, int position) {
                brandMerchantsListItems.get(position).setIsSelected(isSelected);

                if (isSelected) {
                    numSelectedItem++;
                } else {
                    numSelectedItem--;
                }
                if (numSelectedItem == 1) {
                    percentProcess = ((360 * 1 / minSelected) * 100) / 360;
                } else {
                    percentProcess = ((Math.min(360 * numSelectedItem / minSelected, 360)) * 100) / 360;
                }
                circleProcess.setPercent(percentProcess);
                setUpNextButton(minSelected);

                int first =layoutManager.findFirstVisibleItemPosition();
                int last = layoutManager.findLastVisibleItemPosition();
                for(int i=first; i <= last; i++){
                    if(i%brandMerchantsListItems.size() == position){
                        adapter.updateViewTheSame(isSelected, layoutManager.findViewByPosition(i));
                    }
                }
            }
        });

        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        final ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(alphaAdapter);
        recyclerView.setAdapter(scaleAdapter);

        centerItem = UiUtil.findCenterItemPosition(recyclerView, getApplicationContext(), brandMerchantsListItems.size(), InterestBrandMerchantsRvAdapter.MAX_ITEM_COUNT);

        recyclerView.addOnScrollListener(new CenterLockListener(getApplicationContext(), centerAnimateView, recyclerView, scaleAdapter,
                new CenterLockListener.RecyclerViewCallBack() {
                    @Override
                    public void updateView(View newView, View oldView, int oldCenterPosition, int newCenterPosition) {
                        if (oldCenterPosition >= 0) {
                            if (oldCenterPosition != newCenterPosition) {
                                adapter.updateCenterView(oldCenterPosition, oldView, false);
                                adapter.updateCenterView(newCenterPosition, newView, true);
                            } else {
                                adapter.updateCenterView(newCenterPosition, newView, true);
                            }
                        }
                    }
                }));

        recyclerView.scrollToPosition(centerItem);
    }
}
