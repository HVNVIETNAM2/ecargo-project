package com.mm.storefront.app.wxapi;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.mm.main.app.R;
import com.mm.main.app.activity.storefront.StorefrontMainActivity;
import com.mm.main.app.activity.storefront.friend.ImLandingActivity;
import com.mm.main.app.activity.storefront.interest.InterestMappingKeyWordActivity;
import com.mm.main.app.activity.storefront.login.StorefrontMobileLoginActivity;
import com.mm.main.app.activity.storefront.signup.GeneralSignupActivity;
import com.mm.main.app.activity.storefront.signup.MobileSignupProfileActivity;
import com.mm.main.app.activity.storefront.signup.StoreFrontMobileSignupActivity;
import com.mm.main.app.activity.storefront.temp.DemoActivity;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.global.MmGlobal;
import com.mm.main.app.manager.APIManager;
import com.mm.main.app.manager.CheckoutFlowManager;
import com.mm.main.app.manager.ConfigManager;
import com.mm.main.app.manager.LifeCycleManager;
import com.mm.main.app.manager.WeChatManager;
import com.mm.main.app.schema.response.LoginResponse;
import com.mm.main.app.utils.ErrorUtil;
import com.mm.main.app.utils.MmCallBack;
import com.mm.main.app.utils.ObjectUtil;
import com.mm.main.app.utils.StringUtil;
import com.tencent.mm.sdk.openapi.BaseReq;
import com.tencent.mm.sdk.openapi.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.SendAuth;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Response;
import retrofit.Retrofit;

public class WXEntryActivity extends GeneralSignupActivity implements IWXAPIEventHandler {

    @Bind(R.id.guest_login_in_button)
    Button guestLoginInButton;

    @Bind(R.id.btnClose)
    Button btnClose;

    private long openDemoCount = 1;
    private final long MAX_DEMO_COUNTER = 3;
    private int mLoginRequestCode = -1;
    public static boolean IS_COME_BACK_FROM_WECHAT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_front_login);
        ButterKnife.bind(this);
        WeChatManager.getInstance().getApi().handleIntent(getIntent(), this);

        mLoginRequestCode = getIntent().getIntExtra(Constant.LOGIN_IN_REQUEST_CODE_KEY, -1);

        initUI();

        if (IS_COME_BACK_FROM_WECHAT) {
            loginInApp = LoginInAppType.TYPE_CHECK_OUT_NORMAL;
            IS_COME_BACK_FROM_WECHAT = false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == StorefrontMainActivity.STORE_FRONT_LOGIN_REQUEST_CODE ||
                requestCode == StorefrontMainActivity.FRIEND_LOGIN_REQUEST_CODE) {
            setResult(resultCode);
            finish();
        }
    }

    private void initUI() {
        //check loginInApp when the user clicks on wechat app and presses back the storefront app
        LoginInAppType logginAppObject = ObjectUtil.loadComplexObject(this, LoginInAppType.class, Constant.LOGIN_IN_USING_APP_KEY);
        if(logginAppObject != null){
            loginInApp = logginAppObject;
            ObjectUtil.removeObject(this, Constant.LOGIN_IN_USING_APP_KEY); //reset loginInApp status in shared preferences
        }

        if (loginInApp != null) {
            guestLoginInButton.setVisibility(View.INVISIBLE);
            btnClose.setVisibility(View.VISIBLE);
        } else {
            guestLoginInButton.setVisibility(View.VISIBLE);
            btnClose.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.btnWeChatLogin)
    public void onClickWechatButton() {
        proceedWechatLogin();
    }

    @OnClick(R.id.tvWeChatLogin)
    public void onClickWechatTextView() {
        proceedWechatLogin();
    }

    public void proceedWechatLogin() {
        if(loginInApp != null){
            ObjectUtil.saveComplexObject(this, loginInApp, Constant.LOGIN_IN_USING_APP_KEY);
        }

        // send oauth request
        if (WeChatManager.getInstance().getApi().isWXAppInstalled()) {
            final SendAuth.Req req = new SendAuth.Req();
            req.scope = "snsapi_userinfo";
            req.state = "wechat_sdk_demo_test";
            WeChatManager.getInstance().getApi().sendReq(req);
            IS_COME_BACK_FROM_WECHAT = true;
            finish();
        } else {
            ErrorUtil.showConfirmDialog(WXEntryActivity.this, StringUtil.getResourceString("MSG_ERR_CA_WECHAT_NOT_INSTALLED"), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
    }

    @OnClick(R.id.btnClose)
    public void closeLogin() {
        finish();
    }

    @OnClick(R.id.btnMobileLogin)
    public void onClickMobileButton() {
        proceedMobileSignup();
    }

    @OnClick(R.id.tvMobileLogin)
    public void onClickMobileTextView() {
        proceedMobileSignup();
    }

    public void proceedMobileSignup() {
        Intent intent = new Intent(this, StoreFrontMobileSignupActivity.class);
        startActivityWithLoginKey(intent);
        overridePendingTransition(R.anim.bottom_to_top, R.anim.not_move);
    }

    @OnClick(R.id.guest_login_in_button)
    public void proceedGuestLogin() {
        startActivity(new Intent(this, StorefrontMainActivity.class));
        finish();
    }

    @OnClick(R.id.login_in_button)
    public void proceedMobileLogin() {
        Intent intent = new Intent(this, StorefrontMobileLoginActivity.class);
        intent.putExtra(Constant.LOGIN_IN_REQUEST_CODE_KEY, mLoginRequestCode);
        startActivityWithLoginKey(intent, mLoginRequestCode);
        overridePendingTransition(R.anim.bottom_to_top, R.anim.not_move);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) && (openDemoCount >= MAX_DEMO_COUNTER)) {
            if (ConfigManager.getInstance().getConfig().isEnableDebugMode()) {
                openDemoCount = 1; //reset the counter
                openDemo();
                return true;
            }
        }
        openDemoCount++;
        return super.onKeyDown(keyCode, event);
    }

    public void openDemo() {
        Intent intent = new Intent(this, DemoActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.bottom_to_top, R.anim.not_move);
        return;
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    @Override
    public void onResp(BaseResp resp) {
        String code = "";

        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                code = ((SendAuth.Resp) resp).token;
                APIManager.getInstance().getAuthService().loginWeChat(code).enqueue(new MmCallBack<LoginResponse>(this) {
                    @Override
                    public void onSuccess(Response<LoginResponse> response, Retrofit retrofit) {
                        LifeCycleManager.login(response);
                        LoginResponse loginResponse = response.body();
                        if (loginResponse.getSignup()) {
                            if (loginInApp == LoginInAppType.TYPE_CHECK_OUT_NORMAL && CheckoutFlowManager.getInstance().getActivitiesFlow() != null && CheckoutFlowManager.getInstance().getActivitiesFlow().size() > 0) {
                                CheckoutFlowManager.getInstance().actionCompleteSignUpRequest();
                            } else {
                                startActivity(new Intent(WXEntryActivity.this, InterestMappingKeyWordActivity.class));
                                finish();
                            }

                        } else {
                            if (loginInApp == LoginInAppType.TYPE_CHECK_OUT_NORMAL && CheckoutFlowManager.getInstance().getActivitiesFlow() != null && CheckoutFlowManager.getInstance().getActivitiesFlow().size() > 0) {
                                CheckoutFlowManager.getInstance().actionCompleteLoginRequest();
                            } else {
                                startActivity(new Intent(WXEntryActivity.this, StorefrontMainActivity.class));
                                finish();
                            }
                        }

                    }
                });
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                break;
            default:
                break;
        }


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        WeChatManager.getInstance().getApi().handleIntent(intent, this);
    }
}

