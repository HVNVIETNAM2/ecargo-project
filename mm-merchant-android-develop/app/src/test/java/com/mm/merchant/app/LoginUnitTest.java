package com.mm.main.app;

import android.util.Log;

import com.mm.main.app.application.MyApplication;
import com.mm.main.app.constant.Constant;
import com.mm.main.app.schema.Token;
import com.mm.main.app.service.AuthService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import retrofit.*;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
*Unit test for log in service
 **/
@RunWith(RobolectricTestRunner.class)
@Config(application = MyApplication.class, manifest = "src/main/AndroidManifest.xml", sdk = 21)
public class LoginUnitTest {
    private CountDownLatch lock = new CountDownLatch(1);
    private String tokenString;
    @Test
    public void verifyLogin() throws InterruptedException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AuthService service = retrofit.create(AuthService.class);
        Call<Token> call = service.login("adallow@hotmail.com", "Bart");
        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Response<Token> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    tokenString = response.body().getToken();
                    lock.countDown();
                }
                else{
                    Log.i("Unit Test", response.toString());
                }
            }


            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        lock.await(20, TimeUnit.SECONDS);
        assertNotNull(tokenString);

    }

}