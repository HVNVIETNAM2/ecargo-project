'use strict';

function inventoryLocationCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, 
  Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService, InventoryService) {

  $scope.breadcrumbs = [
    { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchHome", icon : "icon-home" },
    { text : mainService.UserReference.TranslationMap.LB_INVENTORY_LOCATION }
  ];

  BaseInventoryLocationCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, 
  Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService, InventoryService);
}

inventoryLocationCtrl.prototype = Object.create(BaseInventoryLocationCtrl.prototype);
inventoryLocationCtrl.prototype.constructor = inventoryLocationCtrl;

angular.module('mmApp').controller('inventoryLocationCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal', 
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', 
  'FileUploadService', 'generalTranslationService', 'geoService', 'InventoryService', inventoryLocationCtrl]);