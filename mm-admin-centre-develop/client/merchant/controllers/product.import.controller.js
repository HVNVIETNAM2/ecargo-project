'use strict';

function ProductImportCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams) {

  $scope.MerchantId = $rootScope.User.MerchantId;

  $scope.breadcrumbs = [{
    text: mainService.UserReference.TranslationMap.LB_MERCHANT,
    link: "merchHome",
    icon: "icon-home"
  }, {
    text: mainService.UserReference.TranslationMap.LB_PRODUCT_LIST,
    link: "productList",
  }, {
    text: mainService.UserReference.TranslationMap.LB_IMPORT_PRODUCTS
  }];


  BaseProductImportCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams);
}

ProductImportCtrl.prototype = Object.create(BaseProductImportCtrl);
ProductImportCtrl.prototype.constructor = ProductImportCtrl;

app.controller('productImportCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', ProductImportCtrl
]);
