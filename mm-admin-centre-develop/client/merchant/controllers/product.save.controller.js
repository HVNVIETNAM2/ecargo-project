'use strict';

function ProductSaveCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $q, $loading, generalTranslationService, geoService, $anchorScroll, DialogService) {
  $scope.saveMode = $state.current.saveMode;
  $scope.t = mainService.UserReference.TranslationMap;
  $scope.merchantId = $rootScope.User.MerchantId;
  BaseProductSaveCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $q, $loading, generalTranslationService, geoService, $anchorScroll, DialogService);
  $scope.breadcrumbs = [{
    text: mainService.UserReference.TranslationMap.LB_MERCHANT,
    link: "merchHome",
    icon: "icon-home"
  }, {
    text: $scope.t.LB_PRODUCT_LIST,
    link: "productList"
  }, {
    text: ($scope.saveMode)?$scope.t.LB_PROD_CREATE: Util.format($scope.t.LB_PROD_EDIT,[''])
  }];
}

ProductSaveCtrl.prototype = Object.create(BaseProductSaveCtrl);
ProductSaveCtrl.prototype.constructor = ProductSaveCtrl;

app.controller('productSaveCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', '$q', '$loading', 'generalTranslationService', 'geoService', '$anchorScroll', 'DialogService', ProductSaveCtrl
]);
