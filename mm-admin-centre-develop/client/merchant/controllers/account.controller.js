'use strict';

function AccountCtrl($rootScope, $scope, $timeout, Constants, $location, $window,
  $modal, $log, $http, Upload, $state, $stateParams, jwtHelper, translationService, mainService) {

  //for translation
  $scope.t = mainService.UserReference.TranslationMap;
  
  BaseAccountCtrl.call(this, $rootScope, $scope, $timeout, Constants, $location, $window,
    $modal, $log, $http, Upload, $state, $stateParams, jwtHelper, translationService, mainService);
}

AccountCtrl.prototype = Object.create(BaseAccountCtrl);
AccountCtrl.prototype.constructor = AccountCtrl;


angular.module('mmApp').controller('AccountCtrl', ['$rootScope', '$scope', '$timeout', 'Constants', '$location', '$window',
'$modal', '$log', '$http', 'Upload', '$state', '$stateParams', 'jwtHelper', 'translationService', 'mainService', AccountCtrl]);
