'use strict';

function ProductUploadCtrl($rootScope, $scope, $q, $location, $anchorScroll, $window, $modal, $log, $http, $timeout,
  Upload, mainService, $state, $stateParams, FileUploadService, CONSTANTS) {

  $scope.MerchantId = $rootScope.User.MerchantId;

  $scope.breadcrumbs = [{
    text: mainService.UserReference.TranslationMap.LB_MERCHANT,
    link: "merchHome",
    icon: "icon-home"
  }, {
    link: "productList",
    text: mainService.UserReference.TranslationMap.LB_PRODUCT_LIST
  }, {
    text: mainService.UserReference.TranslationMap.LB_UPLOAD_IMAGE
  }];

  BaseProductUploadCtrl.call(this, $rootScope, $scope, $q, $location, $anchorScroll, $window, $modal, $log, $http, $timeout,
    Upload, mainService, $state, $stateParams, FileUploadService, CONSTANTS);
}

ProductUploadCtrl.prototype = Object.create(BaseProductUploadCtrl);
ProductUploadCtrl.prototype.constructor = ProductUploadCtrl;

app.controller('productUploadCtrl', [
  '$rootScope', '$scope', '$q', '$location', '$anchorScroll', '$window', '$modal',
  '$log', '$http', '$timeout', 'Upload', 'mainService', '$state',
  '$stateParams', 'FileUploadService', 'CONSTANTS', ProductUploadCtrl
]);
