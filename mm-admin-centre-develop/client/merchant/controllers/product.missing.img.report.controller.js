'use strict';


/**
 * Product Missing Image Report Controller
 * @param $rootScope
 * @param $scope
 * @param $state
 * @param $window
 * @param RequestService
 * @param mainService
 * @constructor
 */
function ProductMissingImagesReportCtrl($rootScope, $scope, $state, $window, RequestService, mainService) {
    var CONSTANTS = setScopeConstants();

    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        CONSTANTS.PRODUCT_IMG_TYPE = {
            FEATURE: 1,
            COLOR: 2,
            DESC: 3
        };

        CONSTANTS.STATUS.ALL = -1;

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }

    this.$scope = $scope;
    this.$window = $window;
    $scope.t = mainService.UserReference.TranslationMap;
    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.RequestService = RequestService;
    $scope.isReportAvailable = false;
    $scope.messages = [];
    $scope.$window = $window;
    $scope.merchantId = $state.params.merchantId;

    $scope.breadcrumbs = [{
        text: $scope.t.LB_MERCHANT,
        link: 'home',
        icon: "icon-home"
    }, {
        link: 'productList',
        text: $scope.t.LB_PRODUCT_LIST
    }, {
        link: 'productUpload({merchantId: ' + $scope.merchantId + ' })',
        text: $scope.t.LB_IMAGE_UPLOAD
    }, {
        text: $scope.t.LB_AC_PROD_MISSING_IMG_REPORT
    }];

    if ($scope.merchantId) {
        RequestService.get('/merch/view', {
            params: {
                merchantid: $scope.merchantId,
                cc: $rootScope.User.CultureCode
            }
        }).then(function (result) {
            $scope.Merchant = result.Merchant;
            $scope.breadcrumbs.splice(2, 0, {
                text: $scope.Merchant['MerchantName' + $rootScope.language],
                link: "merchantDetailsPage({id: '" + $scope.Merchant.MerchantId + "'})"
            });
        }, function (errObj, status, headers, config) {
            $scope.messages = [{
                msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
                type: 'danger'
            }];
        });
    }


    $scope.filters = {
        files: [
            {
                label: $scope.t.LB_FEATURE_IMAGE,
                value: CONSTANTS.PRODUCT_IMG_TYPE.FEATURE,
                disabled: true
            },
            {
                label: $scope.t.LB_DESCRIPTION_IMAGE,
                value: CONSTANTS.PRODUCT_IMG_TYPE.DESC
            },
            {
                label: $scope.t.LB_COLOR_IMAGE,
                value: CONSTANTS.PRODUCT_IMG_TYPE.COLOR
            }
        ],
        status: [
            {
                label: $scope.t.LB_ALL_STATUS,
                value: CONSTANTS.STATUS.ALL
            },
            {
                label: $scope.t.LB_ACTIVE,
                value: CONSTANTS.STATUS.ACTIVE
            },
            {
                label: $scope.t.LB_PENDING,
                value: CONSTANTS.STATUS.PENDING
            },
            {
                label: $scope.t.LB_INACTIVE,
                value: CONSTANTS.STATUS.INACTIVE
            }
        ]
    };

    $scope.form = {
        filefilter: [CONSTANTS.PRODUCT_IMG_TYPE.FEATURE, CONSTANTS.PRODUCT_IMG_TYPE.DESC, CONSTANTS.PRODUCT_IMG_TYPE.COLOR],
        status: CONSTANTS.STATUS.ALL,
        merchantid: $scope.merchantId || $rootScope.User.MerchantId, //merchant id
        cc: $rootScope.User.CultureCode //culture code
    };


    $scope.run = function () {
        $scope.loading = true;
        $scope.isReportAvailable = false;
        $scope.reportFilters = angular.copy($scope.form);
        RequestService.get('/productimage/report/missing-img', {params: $scope.form}).then(function (data) {
            $scope.report = data;
            $scope.isReportAvailable = true;
        }, function (err) {
            $scope.messages = [{
                type: 'danger',
                msg: $scope.t[err.AppCode] || err.Message || err.toString()
            }];
            $scope.isReportAvailable = false;
        }).then(function () {
            $scope.loading = false;
        });
    };

    $scope.print = function () {
        $scope.$window.print();
    };

    $scope.fileFilterExists = function (filter) {
        return _.indexOf($scope.reportFilters.filefilter, filter) !== -1;
    };

    $scope.onCopySuccess = function () {
        return function (e) {
            $(e).tooltip({
                title: $scope.t.LB_COPIED
            })
                .tooltip('show')
                .on('hidden.bs.tooltip', function () {
                    $(e).tooltip('destroy');
                });
        };
    };

    $scope.onCopyError = function () {
        return function (e) {
            //todo: Simplistic detection, do not use it in production
            var actionMsg = '';
            if (/Mac/i.test($window.navigator.userAgent)) {
                actionMsg = $scope.t.LB_PRESS_TO_COPY.replace('{0}', '⌘-C');
            }
            else {
                actionMsg = $scope.t.LB_PRESS_TO_COPY.replace('{0}', 'Ctrl-C');//
            }

            if (actionMsg) {
                $(e).tooltip({
                    title: actionMsg
                })
                    .tooltip('show')
                    .on('hidden.bs.tooltip', function () {
                        $(e).tooltip('destroy');
                    });
            }
        };
    };
}

app.controller('productMissingImagesReportCtrl', ProductMissingImagesReportCtrl);