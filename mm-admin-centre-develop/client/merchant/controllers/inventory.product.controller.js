'use strict';

function InventoryProductListCtrl($rootScope, $scope, $state, $modal, $http, $loading, RequestService, InventoryService, mainService, generalTranslationService) {
    this.InventoryService = InventoryService;
    $scope.t = mainService.UserReference.TranslationMap;
//  $scope.breadcrumbs = [
//    { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "#/home", icon : "icon-home" },
//    {
//      text : mainService.UserReference.TranslationMap.LB_INVENTORY_LOCATION,
//      link: 'inventoryLocation'
//    },
//    {
//      text: $scope.locationName
//    }
//  ];

    BaseInventoryProductListCtrl.call(this, $rootScope, $scope, $state, $modal, $http, $loading, RequestService, InventoryService, mainService, generalTranslationService);
}

InventoryProductListCtrl.prototype = Object.create(BaseInventoryProductListCtrl.prototype);
InventoryProductListCtrl.prototype.constructor = BaseInventoryProductListCtrl;

app.controller('InventoryProductListCtrl', InventoryProductListCtrl);
