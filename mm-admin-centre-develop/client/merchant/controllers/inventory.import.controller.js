'use strict';

function InventoryImportCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, InventoryService) {

    $scope.MerchantId = $rootScope.User.MerchantId;

    $scope.breadcrumbs = [{text: mainService.UserReference.TranslationMap.LB_MM, link: "adminHome", icon: "icon-home"},
        {
            text: mainService.UserReference.TranslationMap.LB_INVENTORY_LOCATION,
            link: 'inventoryLocation'
        },
        {text: mainService.UserReference.TranslationMap.LB_IMPORT_PRODUCT_INVENTORY}];


    InventoryService.getLocation({
        params: {
            cc: $rootScope.User.CultureCode,
            id: $state.params.locationId,
            merchantid: $scope.MerchantId
        }
    }).then(function (location) {
        $scope.locationName = location.LocationName;
        $scope.breadcrumbs.splice(-1, 0, {
            text: location.LocationName,
            link: 'inventoryProduct({merchantId: "' + $scope.merchantId + '", locationId: "' + location.InventoryLocationId + '"})'
        })
    }, function () {
        $scope.messages = [{
            msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
            type: 'danger'
        }];
    });

    BaseInventoryImportCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams);
}

InventoryImportCtrl.prototype = Object.create(BaseInventoryImportCtrl);
InventoryImportCtrl.prototype.constructor = InventoryImportCtrl;

app.controller('InventoryImportCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
    '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', 'InventoryService', InventoryImportCtrl
]);
