'use strict';

angular.module('mmApp').controller('HomeCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  	'$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', 'FileUploadService', 'DialogService',
  	function ($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, FileUploadService, DialogService) {

		$rootScope.settings = $state.current.settings;
		$scope.mainService = mainService;
		$scope.t = mainService.UserReference.TranslationMap;

		//this page need flash player to support IE9, check if browser IE9 has installed flash.
		if(Util.isIE9Browser()&&!Util.isFlashInstalled()){
			DialogService.confirm($scope.t.LB_SORRY, $scope.t.MSG_ERR_INSTALL_FLASH,
				function(){$window.open('https://get.adobe.com/cn/flashplayer/', '_blank');}, null, $scope.t.LB_GET_FLASH);
		}

		$scope.breadcrumbs = [{
			text: $scope.t.LB_MERCHANT,
			icon: "icon-home"
		}];

		//messages
		if ($state.params.msg && $state.params.type) {
			$scope.messages = [{
				msg: $scope.t[$state.params.msg] || $state.params.msg,
				type: $state.params.type
			}];
		}
	}
]);