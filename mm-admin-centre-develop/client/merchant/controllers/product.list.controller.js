'use strict';

function ProductListCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $loading, generalTranslationService) {
  //1.specify breadcrumbs;
  $scope.breadcrumbs = [{
    text: mainService.UserReference.TranslationMap.LB_MERCHANT,
    link: "merchHome",
    icon: "icon-home"
  }, {
    text: mainService.UserReference.TranslationMap.LB_PRODUCT_LIST
  }];
  //2.specify merchantId;
  $scope.merchantId = $rootScope.User.MerchantId;

  BaseProductListCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $loading, generalTranslationService);
}

ProductListCtrl.prototype = Object.create(BaseProductListCtrl);
ProductListCtrl.prototype.constructor = ProductListCtrl;

app.controller('productListCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', '$loading', 'generalTranslationService', ProductListCtrl
]);
