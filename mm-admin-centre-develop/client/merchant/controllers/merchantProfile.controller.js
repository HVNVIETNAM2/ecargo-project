'use strict';

function merchantProfileCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams) {

  $rootScope.settings = $state.current.settings;
  $scope.mainService = mainService;
  $scope.t = mainService.UserReference.TranslationMap;
  $scope.MerchantProfile = {};
  $scope.breadcrumbs = [{
    text: mainService.UserReference.TranslationMap.LB_MERCHANT,
    link: "merchHome",
    icon: "icon-home"
  }, {
    text: mainService.UserReference.TranslationMap.LB_PERSONAL_INFO
  }, ];

  $scope.init = function() {
    //1. retrive the merchant profile.
    var hpromise = $http.get('/api/merch/view/profile', {
      params: {
        merchantid: $rootScope.User.MerchantId,
        cc: $rootScope.User.CultureCode
      }
    });
    hpromise.then(function(result) {
      $scope.MerchantProfile = result.data.MerchantProfile;
    }, function(errObj, status, headers, config) {
      $scope.messages = [{
        msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
        type: 'danger'
      }];
    });
  };

  $scope.init();
}

app.controller('merchantProfileCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams',merchantProfileCtrl
]);
