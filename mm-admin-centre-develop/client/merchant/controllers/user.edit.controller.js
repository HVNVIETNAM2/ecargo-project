'use strict';

function UserEditCtrl($rootScope, $scope, $location, $window, $modal,
  $log, $http, Upload, mainService, translationService, $state, $stateParams, $anchorScroll, FileUploadService, generalTranslationService) {

  $scope.pageSide = "merchant";

  $scope.breadcrumbs = [
    { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchHome", icon : "icon-home" },
    { text :  mainService.UserReference.TranslationMap.LB_USER, link : "user" },
    { text :  mainService.UserReference.TranslationMap.LB_EDIT_USER }
  ];

  $scope.init = function() {
    var hpromise = $http.get($rootScope.servicePrefix+'/user/view', {
      params: {
        userkey: $stateParams.userKey,
        merchantid: $rootScope.User.MerchantId
      }
    });
    hpromise.then(function(result) {
      $scope.form = result.data;
      $scope.firstName = $scope.form.FirstName;
      $scope.lastName = $scope.form.LastName;
      $scope.displayName = $scope.form.DisplayName;
      $scope.MerchantId = result.data.MerchantId;

      var hpromise2 = $http.get($rootScope.servicePrefix+'/inventory/location/list', { params: { merchantid : $scope.MerchantId, cc : $rootScope.language } });
      hpromise2.then(function(resultObj){
        $scope.inventoryLocations = resultObj.data;
      });
    }, function(errObj, status, headers, config) {
      $scope.messages = [
        {msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR, type: 'danger'}
      ];
    });
  };

  $scope.init();  // load user profile

  $scope.changeMobileCode = function(code) {
    $scope.form.MobileCode = code;
  };

  BaseUserEditCtrl.call(this, $rootScope, $scope, $location, $window, $modal,
    $log, $http, Upload, mainService, translationService, $state, $stateParams, $anchorScroll, FileUploadService, generalTranslationService);
}

UserEditCtrl.prototype = Object.create(BaseUserEditCtrl.prototype);
UserEditCtrl.prototype.constructor = UserEditCtrl;
angular.module('mmApp').controller('UserEditCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', 'translationService', '$state', '$stateParams', '$anchorScroll', 'FileUploadService', 'generalTranslationService', UserEditCtrl]);

