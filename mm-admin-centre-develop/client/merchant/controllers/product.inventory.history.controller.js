'use strict';

/**
 * Product Inventory Controller
 * @param $rootScope
 * @param $scope
 * @param $state
 * @param $window
 * @param $loading
 * @param RequestService
 * @param InventoryService
 * @param mainService
 * @constructor
 */
function ProductInventoryHistoryCtrl($rootScope, $scope, $state, $window, $loading, RequestService, InventoryService, mainService) {
    this.$scope = $scope;
    this.$state = $state;
    this.InventoryService = InventoryService;
    $scope.t = mainService.UserReference.TranslationMap;
    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.RequestService = RequestService;

    $scope.messages = [];
    $scope.inventoryLocationId = $state.params.inventorylocationid;
    $scope.skuId = $state.params.skuid;
    $scope.inventoryId = $state.params.inventoryid;
    $scope.data = [];

    $scope.routePrefix = $state.is('locationInventory-create-history') || $state.is('locationInventory-edit-history') ? 'location' : 'product';
    $scope.route = $state.is('productInventory-edit-history') || $state.is('locationInventory-edit-history') ? 'edit' : 'create';


    $scope.merchantId = $state.params.merchantId || $rootScope.User.MerchantId;
    if ($rootScope.pageSide === 'admin') {
        $scope.breadcrumbs = [
            { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
            { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant({id: '" + $scope.merchantId + "'})" }
        ];
    } else {
        $scope.breadcrumbs = [{
            text: $scope.t.LB_MERCHANT,
            link: 'home',
            icon: "icon-home"
        }];
    }


    if ($scope.merchantId) {
        RequestService.get('/merch/view', {
            params: {
                merchantid: $scope.merchantId,
                cc: $rootScope.User.CultureCode
            }
        }).then(function(result) {
            $scope.Merchant = result.Merchant;
            $scope.breadcrumbs.splice(2, 0, { text: $scope.Merchant['MerchantName' + $rootScope.language], link: "merchantDetailsPage({id: '" + $scope.Merchant.MerchantId + "'})"});
        }, function(errObj, status, headers, config) {
            $scope.messages = [{
                msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
                type: 'danger'
            }];
        });
    }



    $scope.back = function () {
        $state.go($scope.routePrefix + 'Inventory-' + $scope.route, {
            skuid: $scope.skuId,
            inventoryid: $scope.inventoryId,
            inventorylocationid: $scope.inventoryLocationId,
            merchantId: $scope.merchantId
        });
    };


    $loading.start('loading');
    $scope.loading = true;
    $scope.sku = {};
    InventoryService.getHistory({
        params: {
            inventoryid: $scope.inventoryId,
            inventorylocationid: $scope.inventoryLocationId,
            skuid: $scope.skuId,
            cc: $rootScope.User.CultureCode,
            merchantid: $scope.merchantId
        }
    }).then(function (data) {
        $scope.data = data;

        if ($scope.routePrefix !== 'location') {
            $scope.breadcrumbs.push({
                text: $scope.t.LB_PRODUCT_LIST,
                link: 'productList({merchantId:'+$scope.merchantId+'})'
            },{
                text: Util.format($scope.t.LB_PROD_EDIT_TITLE,[data.Sku.StyleCode]),
                link: 'productEdit({stylecode: "' + data.Sku.StyleCode + '"})'
            }, {
                text: $scope.t.LB_INVENTORY_LIST,
                link: 'productInventory({skuid: "' + data.Sku.SkuId + '"})'
            });
        } else {
            $scope.breadcrumbs.push({
                text: $scope.t.LB_INVENTORY_LOCATION,
                link: 'inventoryLocation({merchantId: "' + $scope.merchantId + '"})'
            }, {
                //todo: get the location name
                text: $state.params.locname,
                link: 'inventoryProduct({locationId: "' + $scope.inventoryLocationId + '", merchantId: "' + $scope.merchantId + '"})'
            });
        }
        $scope.breadcrumbs.push({
            text: ($scope.route === 'edit' ? $scope.t.LB_EDIT_PRODUCT_INVENTORY : $scope.t.LB_PRODUCT_NEW_INVENTRY),
            link: $scope.routePrefix + 'Inventory-' + $scope.route + '({skuid: "' + $scope.skuId + '", inventoryid: "' + $scope.inventoryId + '", inventorylocationid: "' + $scope.inventoryLocationId + '", merchantId: "' + $scope.merchantId + '"})'
        }, {
            text: $scope.t.LB_INVENTORY_UPDATE_HISTORY
        });

    }, function (err) {
        $scope.messages = [{
            type: 'danger',
            msg: $scope.t[err.AppCode] || err.Message || err.toString()
        }];
    }).finally(function () {
        $scope.loading = false;
        $loading.finish('loading');
    });

}

app.controller('productInventoryHistoryCtrl', ['$rootScope', '$scope', '$state', '$window', '$loading', 'RequestService', 'InventoryService', 'mainService', ProductInventoryHistoryCtrl]);
