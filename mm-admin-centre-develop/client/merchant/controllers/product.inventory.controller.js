'use strict';

/**
 * Product Inventory Controller
 * @constructor
 */
function ProductInventoryCtrl($rootScope, $scope, $state, $timeout, $window, $modal, $loading, $location, $anchorScroll, RequestService, InventoryService, mainService) {
    this.$scope = $scope;
    this.$state = $state;
    this.InventoryService = InventoryService;
    $scope.t = mainService.UserReference.TranslationMap;
    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.RequestService = RequestService;

    $scope.messages = [];

    $scope.skuId = $state.params.skuid;
    $scope.inventoryId = $state.params.inventoryid;
    $scope.inventoryLocationId = $state.params.inventorylocationid;

    $scope.isNewInventory = typeof $scope.inventoryId === 'undefined' || $scope.inventoryId === '';
    $scope.isEdit = $state.is('productInventory-edit') || $state.is('locationInventory-edit');
    $scope.IsPerpetual = false;

    $scope.routePrefix = $state.is('locationInventory-create') || $state.is('locationInventory-edit') ? 'location' : 'product';
    $scope.route = $state.is('productInventory-edit') || $state.is('locationInventory-edit') ? 'edit' : 'create';

    $scope.breadcrumbs = [{
            text: $scope.t.LB_MERCHANT,
            link: 'home',
            icon: "icon-home"
        }];

    BaseProductInventoryCtrl.call(this, $rootScope, $scope, $state, $timeout, $window, $modal, $loading, $location, $anchorScroll, RequestService, InventoryService, mainService);
}

ProductInventoryCtrl.prototype = Object.create(BaseProductInventoryCtrl.prototype);
ProductInventoryCtrl.prototype.constructor = BaseProductInventoryCtrl;

app.controller('productInventoryCtrl', ['$rootScope', '$scope', '$state', '$timeout', '$window', '$modal', '$loading', '$location', '$anchorScroll', 'RequestService', 'InventoryService', 'mainService', ProductInventoryCtrl]);
