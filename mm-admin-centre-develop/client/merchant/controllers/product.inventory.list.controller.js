'use strict';

/**
 * Product Inventory List Controller
 * @param {type} $rootScope
 * @param {type} $scope
 * @param {type} $state
 * @param {type} $modal
 * @param {type} $loading
 * @param {type} $http
 * @param {type} RequestService
 * @param {type} InventoryService
 * @param {type} mainService
 * @param {type} generalTranslationService
 * @constructor
 */
function ProductInventoryListCtrl($rootScope, $scope, $state, $modal, $loading, $http, RequestService, InventoryService, mainService, generalTranslationService) {
    this.$state = $state;
    this.$modal = $modal;
    this.InventoryService = InventoryService;
    $scope.t = mainService.UserReference.TranslationMap;
    $scope.merchantId = $rootScope.User.MerchantId;
    BaseProductInventoryListCtrl.call(this, $rootScope, $scope, $state, $modal, $loading, $http, RequestService, InventoryService, mainService, generalTranslationService);
}

ProductInventoryListCtrl.prototype = Object.create(BaseProductInventoryListCtrl.prototype);
ProductInventoryListCtrl.prototype.constructor = BaseProductInventoryListCtrl;

app.controller('productInventoryListCtrl', ProductInventoryListCtrl);
