'use strict';

function inventoryLocationEditCtrl($rootScope, $scope, $location, $anchorScroll, $window, $modal, $log, $http,
  Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService, InventoryService) {

  $scope.breadcrumbs = [
    { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "home", icon : "icon-home" }
  ];

  BaseInventoryLocationEditCtrl.call(this, $rootScope, $scope, $location, $anchorScroll, $window, $modal, $log, $http,
  Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService, InventoryService);
}

inventoryLocationEditCtrl.prototype = Object.create(BaseInventoryLocationEditCtrl.prototype);
inventoryLocationEditCtrl.prototype.constructor = inventoryLocationEditCtrl;

angular.module('mmApp').controller('inventoryLocationEditCtrl', ['$rootScope', '$scope', '$location', '$anchorScroll', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', 
  'FileUploadService', 'generalTranslationService', 'geoService', 'InventoryService', inventoryLocationEditCtrl]);