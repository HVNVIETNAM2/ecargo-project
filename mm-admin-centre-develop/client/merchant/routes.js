'use strict';

app.config(function ($stateProvider) {
  $stateProvider
    .state('login', {
      url: '/login?at&err',
      templateUrl: 'views/login.html',
      controller: 'AccountCtrl',
      resolve: {
        removeModal: function ($modalStack) {
          return $modalStack.dismissAll();
        },
        translationService: function(TranslationService) {
          return TranslationService.initialize('readonly');
        },
        generalTranslationService : function(GeneralTranslationService){
          return GeneralTranslationService.initialize('readonly');
        },
        mainService: function(MainService) {
          return MainService.initialize('readonly');
        },
        activate: function($rootScope, $state, $stateParams, $http) {
          if ($stateParams.at) {
            var hpromise = $http.post($rootScope.servicePrefix + '/auth/reactivate', {
              "ActivationToken": $stateParams.at
            });
            return hpromise.then(function () {
              return true;
            }, function () {
              $state.go('linkExpire');
            });
          }
        }
      }
    })
    .state('accountPending', {
      url: '/account/pending',
      templateUrl: '/shared/views/account-pending.html',
      controller: 'AccountCtrl',
      resolve: {
        translationService: function($rootScope, TranslationService) {
          var defaultLang = $rootScope.preference.lang;
          return TranslationService.loadTranslationData(defaultLang);
        },
        mainService: function(MainService) {
          return MainService.initialize();
        },
      }
    })
    .state('accountLocked', {
      url: '/account/locked',
      templateUrl: '/shared/views/account-locked.html',
      controller: 'AccountCtrl',
      resolve: {
        translationService: function($rootScope, TranslationService) {
          var defaultLang = $rootScope.preference.lang;
          return TranslationService.loadTranslationData(defaultLang);
        },
        mainService: function(MainService) {
          return MainService.initialize();
        },
      }
    })
    .state('linkExpire', {
      url: '/link/expire',
      templateUrl: '/shared/views/link-expire.html',
      controller: 'AccountCtrl',
      resolve: {
        translationService: function($rootScope, TranslationService) {
          var defaultLang = $rootScope.preference.lang;
          return TranslationService.loadTranslationData(defaultLang);
        },
        mainService: function(MainService) {
          return MainService.initialize();
        },
      }
    })
    .state('pageNotFound', {
      url: '/404',
      settings: {
        showMenus: false
      },
      templateUrl: '/shared/views/404.html',
      controller: ['$rootScope', '$scope', '$state', 'mainService', function($rootScope, $scope, $state, mainService) {
        $scope.mainService = mainService;
        $scope.t = mainService.UserReference.TranslationMap;
        $rootScope.settings = $state.current.settings;
        $scope.isAuthenicated = $rootScope.User && $rootScope.User.UserKey;
        if ($rootScope.fromState.name === '') {
          if ($scope.isAuthenicated) {
            $state.go('home')
          } else {
            $state.go('login')
          }
        }
      }],
      resolve: {
        translationService: function(TranslationService) {
          return TranslationService.initialize('readonly');
        },
        generalTranslationService : function(GeneralTranslationService){
          return GeneralTranslationService.initialize('readonly');
        },
        mainService: function(MainService) {
          return MainService.initialize('readonly');
        }
      }
    })
    .state('home', {
      url: '/',
      authenticate: true,
      settings: {
        showMenus: true
      },
      controller: ['$rootScope', '$state', function($rootScope, $state) {
        if ($rootScope.User && $rootScope.User.UserKey) {
          if (!$rootScope.isMerchantAdmin()) {
            $state.go('userEdit', {userKey: $rootScope.User.UserKey});
          } else {
            $state.go('merchHome');
          }
        } else {
          $state.go('login');
        }
      }]
    })
    .state('merchHome', {
      url: '/home?msg&type',
      authenticate: true,
      settings: {
        showMenus: true
      },
      resolve: {
        mainService: function(MainService) {
          return MainService.initialize();
        }
      },
      controller: 'HomeCtrl',
      templateUrl : 'views/home.html'
    })
    .state('inventoryLocation', {
      url: '/inventoryLocation?msg&type',
      authenticate: true,
      settings: {
        showMenus: true
      },
      resolve: {
        mainService: function(MainService) {
          return MainService.initialize();
        },
        generalTranslationService: function(GeneralTranslationService) {
          return GeneralTranslationService.initialize();
        },
        geoService : function(geoService){
          return geoService.initialize();
        }
      },
      controller: 'inventoryLocationCtrl',
      templateUrl : '/shared/views/inventoryLocation.html'
    })
      .state('createInventoryLocation', {
        url: '/inventoryLocation/create-inventory?msg&type',
        authenticate: true,
        settings: {
          showMenus: true
        },
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          },
          geoService : function(geoService){
            return geoService.initialize();
          }
        },
        controller: 'inventoryLocationEditCtrl',
        templateUrl : "/shared/views/edit-inventory-location.html"
      })
    .state('editInventoryLocation', {
      url: '/inventoryLocation/{inventorylocationid}/edit-inventory',
      authenticate: true,
      settings: {
        showMenus: true
      },
      resolve: {
        mainService: function(MainService) {
          return MainService.initialize();
        },
        generalTranslationService: function(GeneralTranslationService) {
          return GeneralTranslationService.initialize();
        },
        geoService : function(geoService){
          return geoService.initialize();
        }
      },
      controller: 'inventoryLocationEditCtrl',
      templateUrl : "/shared/views/edit-inventory-location.html"
    })
      .state("locationInventory-edit", {
        url: '/inventoryLocation/{inventorylocationid}/sku/{skuid}/edit-inventory/{inventoryid}',
        templateUrl: '/shared/views/product-inventory.html',
        controller: 'productInventoryCtrl as inventoryCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          }
        }
      })
      .state("locationInventory-edit-history", {
        url: '/inventoryLocation/{inventorylocationid}/sku/{skuid}/edit-inventory/{inventoryid}/history?locname',
        templateUrl: '/shared/views/product-inventory-history.html',
        controller: 'productInventoryHistoryCtrl as inventoryCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          }
        }
      })
      .state("locationInventory-create", {
        url: '/inventoryLocation/{inventorylocationid}/sku/{skuid}/create-inventory',
        templateUrl: '/shared/views/product-inventory.html',
        controller: 'productInventoryCtrl as inventoryCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          }
        }
      })
      .state("locationInventory-create-history", {
        url: '/inventoryLocation/{inventorylocationid}/sku/{skuid}/create-inventory/history?locname',
        templateUrl: '/shared/views/product-inventory-history.html',
        controller: 'productInventoryHistoryCtrl as inventoryCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          }
        }
      })
    .state("inventoryProduct", {
      url: '/inventoryLocation/:locationId/inventory?msg&type',
      templateUrl: '/shared/views/inventory-product-list.html',
      controller: 'InventoryProductListCtrl as inventoryCtrl',
      authenticate: true,
      settings: {
        showMenus: true
      },
      resolve: {
        mainService: function(MainService) {
          return MainService.initialize();
        },
        generalTranslationService: function(GeneralTranslationService) {
          return GeneralTranslationService.initialize();
        }
      }
    })
    .state("inventoryImport", {
      url: '/inventoryLocation/:locationId/inventory/import',
      templateUrl: '/shared/views/inventory-import.html',
      controller: 'InventoryImportCtrl',
      settings: {
        showMenus: true
      },
        authenticate: true,
      resolve: {
        mainService: function(MainService) {
          return MainService.initialize();
        },
        generalTranslationService: function(GeneralTranslationService) {
          return GeneralTranslationService.initialize();
        },
        geoService : function(geoService){
          return geoService.initialize();
        }
      }
    })

    .state('user', {
      url: '/user',
      templateUrl: '/shared/views/list.html',
      controller: 'UserListCtrl',
      settings: {
        showMenus: true
      },
      resolve: {
        isAdminOrSelf: function($rootScope, $state, $timeout, $q) {
          var deferred = $q.defer();

          // $timeout is an example; it also can be an xhr request or any other async function
          $timeout(function() {
            if ($rootScope.isMerchantAdmin()){
              deferred.resolve();
            } else {
              $state.go('editMyProfile');
              deferred.reject();
            }
          });

          return deferred.promise;
        },
        mainService: function(MainService) {
          return MainService.initialize();
        },
        generalTranslationService: function(GeneralTranslationService) {
          return GeneralTranslationService.initialize();
        }
      },
      authenticate: true
    })

    .state('userEdit', {
      url: '/user/edit/:userKey',
      templateUrl: '/shared/views/user-edit.html',
      controller: 'UserEditCtrl',
      settings: {
        showMenus: true
      },
      resolve: {
        isAdminOrSelf: function($rootScope, $state, $stateParams, $timeout, $q) {
          var deferred = $q.defer();

          // $timeout is an example; it also can be an xhr request or any other async function
          $timeout(function() {
            if ($rootScope.isMerchantAdmin() || $rootScope.isSelf($stateParams.userKey)){
              deferred.resolve();
            } else {
              $state.go('home');
              deferred.reject();
            }
          });

          return deferred.promise;
        },
        mainService: function(MainService) {
          return MainService.initialize();
        },
        translationService: function(TranslationService) {
          return TranslationService.loadTranslationData();
        },
        generalTranslationService: function(GeneralTranslationService) {
          return GeneralTranslationService.initialize();
        }
      },
      authenticate: true
    })
    .state('activate', {
      url: '/activate?at',
      templateUrl: '/shared/views/activate.html',
      controller: 'AccountCtrl',
      resolve: {
        validateToken: function ($state, $stateParams, UserService) {
          return UserService.validateToken($stateParams.at).then(function () {
            return true;
          }, function () {
            $state.go('linkExpire');
          });
        },
        translationService: function($rootScope, TranslationService) {
          var defaultLang = $rootScope.preference.lang;
          return TranslationService.loadTranslationData(defaultLang);
        },
        mainService: function(MainService) {
          return MainService.initialize();
        },
      }
    })
    .state('activateSuccess', {
      url: '/activate/success',
      templateUrl: '/shared/views/activate-success.html',
      controller: 'AccountCtrl',
      resolve: {
        translationService: function($rootScope, TranslationService) {
          var defaultLang = $rootScope.preference.lang;
          return TranslationService.loadTranslationData(defaultLang);
        },
        mainService: function(MainService) {
          return MainService.initialize();
        },
      }
    })
      .state('unlock', {
        url: '/unlock?at',
        templateUrl: '/shared/views/account-unlock.html',
        controller: 'AccountCtrl',
        resolve: {
          validateToken: function ($state, $stateParams, UserService) {
            return UserService.validateToken($stateParams.at).then(function () {
              return true;
            }, function () {
              $state.go('linkExpire');
            });
          },
          translationService: function($rootScope, TranslationService) {
            var defaultLang = $rootScope.preference.lang;
            return TranslationService.loadTranslationData(defaultLang);
          },
          mainService: function(MainService) {
            return MainService.initialize();
          },
        }
      })
      .state('unlockSuccess', {
        url: '/unlock/success',
        templateUrl: '/shared/views/account-unlock-success.html',
        controller: 'AccountCtrl',
        resolve: {
          translationService: function($rootScope, TranslationService) {
            var defaultLang = $rootScope.preference.lang;
            return TranslationService.loadTranslationData(defaultLang);
          },
          mainService: function(MainService) {
            return MainService.initialize();
          },
        }
      })
    .state('resetPass', {
      url: '/resetPass?at',
      templateUrl: '/shared/views/reset-password-target.html',
      controller: 'AccountCtrl',
      resolve: {
        validateToken: function ($state, $stateParams, UserService) {
          return UserService.validateToken($stateParams.at).then(function () {
            return true;
          }, function () {
            $state.go('linkExpire');
          });
        },
        translationService: function($rootScope, TranslationService) {
          var defaultLang = $rootScope.preference.lang;
          return TranslationService.loadTranslationData(defaultLang);
        },
        mainService: function(MainService) {
          return MainService.initialize();
        },
      }
    })
    .state('resetPassSuccess', {
      url: '/resetPass/success',
      templateUrl: '/shared/views/reset-password-success.html',
      controller: 'AccountCtrl',
      resolve: {
        translationService: function($rootScope, TranslationService) {
          var defaultLang = $rootScope.preference.lang;
          return TranslationService.loadTranslationData(defaultLang);
        },
        mainService: function(MainService) {
          return MainService.initialize();
        },
      }
    })
    .state('editMyProfile', {
      url: '/user/editMe',
      settings: {
        showMenus: true
      },
      controller: function($rootScope, $state) {
        if ($rootScope.User) {
          $state.go('userEdit', {userKey: $rootScope.User.UserKey});
        }
        else {
          $state.go('login');
        }
      }
    })
    .state('logout', {
      url: '/logout',
      resolve: {
        mainService: function(MainService) {
          return MainService.initialize();
        }
      },
      controller: ['$rootScope', '$state', '$modal', 'mainService', function($rootScope, $state, $modal, mainService) {
        $modal.open({
            templateUrl: "/shared/views/confirm-logout-dialog.html",
            keyboard: false,
            backdrop : 'static',
            resolve: {
              mainService: function() {
                return mainService;
              }
            },
            controller: ['$scope', 'mainService', function($scope, mainService) {
              // Translation data
              $scope.t = mainService.UserReference.TranslationMap;
              $scope.confirm = function() {
                  $scope.$close(true);
                  // Remove any stale tokens (in rootScope and sessionStorage)
                  $rootScope.tokenDelete();
                  $rootScope.User = null;
                  $rootScope.menus = null;//when logout, it need clear the menus data.
                  mainService.UserReference = null;// When logout, it need to clear the reference data.
                  $state.go('login');
              };
              $scope.cancel = function() {
                $scope.$dismiss();
                $state.go($rootScope.fromState, $rootScope.fromParams);
              };
            }]
        });
      }]
    })
    .state("passiveLogout", {
      url: "/logout/force",
      onEnter: ['$state', '$modal', function($state, $modal) {
          $modal.open({
            keyboard: false,
            backdrop : 'static',
            templateUrl: "/shared/views/passive-logout-dialog.html",
            resolve: {},
            controller: ['$scope', function($scope) {
              $scope.confirm = function() {
                $scope.$close(true);
              };
            }]
          }).result.finally(function() {
              $state.go('login');
          });
      }]
    })
    .state("profile", {
      url: '/merchant/profile',
      templateUrl: 'views/merchant-profile.html',
      controller: 'merchantProfileCtrl',
      settings: {
        showMenus: true
      },
        authenticate: true,
      resolve: {
        mainService: function(MainService) {
          return MainService.initialize();
        },
        generalTranslationService: function(GeneralTranslationService) {
          return GeneralTranslationService.initialize();
        },
        geoService : function(geoService){
          return geoService.initialize();
        }
      }
    })
    .state("productList", {
      url: '/product/list',
      templateUrl: '/shared/views/product-list.html',
      controller: 'productListCtrl',
      settings: {
        showMenus: true
      },
      authenticate: true,
      resolve: {
        mainService: function(MainService) {
          return MainService.initialize();
        },
        generalTranslationService: function(GeneralTranslationService) {
          return GeneralTranslationService.initialize();
        },
        geoService : function(geoService){
          return geoService.initialize();
        }
      }
    })
    .state("productUpload", {
      url: '/product/upload',
      templateUrl: '/shared/views/product-upload.html',
      controller: 'productUploadCtrl',
      authenticate: true,
      settings: {
        showMenus: true
      },
      resolve: {
        mainService: function(MainService) {
          return MainService.initialize();
        },
        generalTranslationService: function(GeneralTranslationService) {
          return GeneralTranslationService.initialize();
        }
      }
    })
      .state("productMissingImgReport", {
          url: '/product/missing-img-report',
          templateUrl: '/shared/views/product-missing-img-report.html',
          controller: 'productMissingImagesReportCtrl as prodController',
          settings: {
              showMenus: true
          },
          authenticate: true,
          resolve: {
              mainService: function (MainService) {
                  return MainService.initialize();
              },
              generalTranslationService: function (GeneralTranslationService) {
                  return GeneralTranslationService.initialize();
              }
          }
      })
    .state("productImport", {
      url: '/product/import',
      templateUrl: '/shared/views/product-import.html',
      controller: 'productImportCtrl',
      settings: {
        showMenus: true
      },
        authenticate: true,
      resolve: {
        mainService: function(MainService) {
          return MainService.initialize();
        },
        generalTranslationService: function(GeneralTranslationService) {
          return GeneralTranslationService.initialize();
        },
        geoService : function(geoService){
          return geoService.initialize();
        }
      }
    })
      .state("productInventory", {
        url: '/sku/{skuid}/inventory-list?msg&type',
        templateUrl: '/shared/views/product-inventory-list.html',
        controller: 'productInventoryListCtrl as inventoryCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          }
        }
      })
      .state("productInventory-edit", {
        url: '/sku/{skuid}/inventory-list/{inventorylocationid}/edit-inventory/{inventoryid}',
        templateUrl: '/shared/views/product-inventory.html',
        controller: 'productInventoryCtrl as inventoryCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          }
        }
      })
      .state("productInventory-edit-history", {
        url: '/sku/{skuid}/inventory-list/{inventorylocationid}/edit-inventory/{inventoryid}/history',
        templateUrl: '/shared/views/product-inventory-history.html',
        controller: 'productInventoryHistoryCtrl as inventoryCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          }
        }
      })
      .state("productInventory-create", {
        url: '/sku/{skuid}/inventory-list/{inventorylocationid}/create-inventory',
        templateUrl: '/shared/views/product-inventory.html',
        controller: 'productInventoryCtrl as inventoryCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          }
        }
      })
      .state("productInventory-create-history", {
        url: '/sku/{skuid}/inventory-list/{inventorylocationid}/create-inventory/history',
        templateUrl: '/shared/views/product-inventory-history.html',
        controller: 'productInventoryHistoryCtrl as inventoryCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          }
        }
      })
      .state("productCreate", {
        url: '/product/create',
        templateUrl: '/shared/views/product-save.html',
        controller: 'productSaveCtrl as productSaveCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        saveMode: true,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          },
          geoService : function(geoService){
            return geoService.initialize();
          }
        }
      })
      .state("productEdit", {
        url: '/product/edit/{stylecode}?msg&type',
        templateUrl: '/shared/views/product-save.html',
        controller: 'productSaveCtrl as productSaveCtrl',
        settings: {
          showMenus: true
        },
        authenticate: true,
        saveMode: false,
        resolve: {
          mainService: function(MainService) {
            return MainService.initialize();
          },
          generalTranslationService: function(GeneralTranslationService) {
            return GeneralTranslationService.initialize();
          },
          geoService : function(geoService){
            return geoService.initialize();
          }
        }
      })
    ;
});
