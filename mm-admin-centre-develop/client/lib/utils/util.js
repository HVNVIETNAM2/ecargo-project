'use strict';
/**
  global util module which provide string format function and others later.
**/
var Util = (function(){
  var module = {};
  /**
    @Desc add format extension for String object
    @Desc eg: var str = "hello world {0}";  print(str.format(['JavaScript'])); it would print "hello world JavaScript";
    @Param args string array-['a','b',...];
  **/
  module.format=function(str, args){
    if (typeof str !== 'string') return str;
    var reg = new RegExp("{-?[0-9]+}", "g");
    return str.replace(reg, function(item) {
      var intVal = parseInt(item.substring(1, item.length - 1));
      var replace;
      if (intVal >= 0) {
        replace = args[intVal];
      } else if (intVal === -1) {
        replace = "{";
      } else if (intVal === -2) {
        replace = "}";
      } else {
        replace = "";
      }
      return replace;
    });
  }
  
    module.formatDate = function (inputDate) {
        var resultDate = null;
//        var dataParseFormat = "YYYY-MM-DD'T'HH:mm:ss'Z'";
        var dataOutputFormat = "YYYY-MM-DD HH:mm:ss";
//        var momentDate = moment(inputDate, dataParseFormat);
        resultDate = moment(inputDate).format(dataOutputFormat);
        return resultDate;
    };
    
    module.bytesToSize = function (bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0){
            return '0 Byte';
        }
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        
        if(i > sizes.length){
            i = sizes.length-1;
        }
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    };

    /**
     @Desc check if IE browser have installed the flash.
     @Return true-installed or not IE browser; false-IE Browser has not installed yet.
     **/
    module.isFlashInstalled = function(){
        var flash = 'None';
        // Count down from 10.
        for(var i = 10; i > 0; i--)
        {
            try{
                flash = new ActiveXObject("ShockwaveFlash.ShockwaveFlash."+String(i));
            }catch(e){
                //console.log(e);
            }
            if(flash != 'None')
                return true;//flash.GetVariable("$version");

        }
        return false;
    };

    /**
     @Desc check if IE browser or note
     @Return true-IE, false-not IE.
     **/
    module.isIE9Browser = function(){
        var isIE  = (navigator.appVersion.indexOf("MSIE 9") != -1) ? true : false;
        return isIE;
    };
    
    /**
     @Desc check if IE browser or note
     @Return true-IE, false-not IE.
     **/
    module.isIE = function(){
        return (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.userAgent.indexOf('Edge/') !== -1
            || navigator.appVersion.indexOf('Trident/') > 0);
    };
    
  return module;
}());