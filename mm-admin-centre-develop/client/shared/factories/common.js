app.factory('authInterceptor', ['$rootScope', '$q', '$location',
	function ($rootScope, $q, $location) {
	  return {
	    // Intercept 401s and redirect to passive logout page
	    responseError: function(response) {
	      if (response.status === 401) {
	        // Remove any stale tokens (in rootScope and sessionStorage)
	        $rootScope.tokenDelete();
	        $rootScope.User = null;

	        $location.path('/login').search({err: 'MSG_PASSIVE_LOGOUT'});
	        return $q.reject(response);
	      } else if (response.status === 403) {
			  $location.path('/home').search({msg: 'MSG_ERR_USER_UNAUTHORIZED', type: 'danger'});
			  return $q.reject(response);
		  }
	      else {
	        return $q.reject(response);
	      }
	    }
	  };
	}
])

app.factory('MainService', ['$rootScope', '$http', '$filter', '$q',
	function($rootScope, $http, $filter, $q) {
	  var refData = {
	    UserReference: null
	  };
	  return {
	    initialize: function(readonly) {
	      if (!refData.UserReference) {
	        var cc = (sessionStorage.getItem('sessionLang') || 'EN');
	        if ($rootScope.User && $rootScope.User.CultureCode)
	          cc = $rootScope.User.CultureCode;
	        var url = $rootScope.servicePrefix +"/reference/translation?cc=" + cc;

	        var hpromise = $http.get(url).then(function(response) {

	          if(readonly){
	          	return {
	          		UserReference : response.data
	          	}
	          }else{
	          	refData.UserReference = response.data;
	          }


	          if($rootScope.menus===undefined||$rootScope.menus===null){
	            //initiate menu array
	            var t = refData.UserReference.TranslationMap;

	            $rootScope.menus = {
	              home : t.LB_HOME,
	              merchant : t.LB_MERCHANT,
								products : t.LB_AC_PRODUCT,
                brand: t.LB_AC_BRAND,
                category: t.LB_AC_CATEGORY,
	              user : t.LB_USER,
	              logout : t.LB_TL_LOGOUT,
	              search : t.LB_SEARCH,
	              inventoryLocation : t.LB_INVENTORY_LOCATION,
	              editprofile: t.LB_USER_EDIT_MY_PROFILE
	            };
				  //init the footer policies text
				  $rootScope.policies = {
					  privacy:t.LB_PRIVACY_POLICY,
					  cookie: t.LB_COOKIE_POLICY,
					  copyright: t.LB_COPYRIGHT_POLICY
				  };
	          }
	          return refData;
	        });
	        return hpromise;
	      }
	      else {
	        if ($rootScope.menus === undefined || $rootScope.menus === null) {
	          //initiate menu array
	          var t = refData.UserReference.TranslationMap;
	          $rootScope.menus = {
	            home: t.LB_HOME,
	            merchant: t.LB_MERCHANT,
							products : t.LB_PRODUCT_LIST,
              brand: t.LB_AC_BRAND,
              category: t.LB_AC_CATEGORY,
	            user: t.LB_USER,
	            logout: t.LB_TL_LOGOUT,
	            inventoryLocation : t.LB_INVENTORY_LOCATION,
	            editprofile: t.LB_USER_EDIT_MY_PROFILE
	          };
				//init the footer policies text
				$rootScope.policies = {
					privacy:t.LB_PRIVACY_POLICY,
					cookie: t.LB_COOKIE_POLICY,
					copyright: t.LB_COPYRIGHT_POLICY
				};
	        }
	        return refData;
	      }
	    }
	  }
	}
])

app.factory('GeneralTranslationService', ['$rootScope', '$http', '$filter', '$q',
	function($rootScope, $http, $filter, $q) {
	  var refData = {
	    UserReference: null
	  };
	  return {
	    initialize: function(readonly) {
	      if (!refData.UserReference) {
	        var cc = (sessionStorage.getItem('sessionLang') || 'EN');
	        if ($rootScope.language)
	          cc = $rootScope.language;

	        var url =  $rootScope.servicePrefix + "/reference/general?cc=" + cc;
	        var hpromise = $http.get(url).then(function(response) {


	         if( readonly ){
	          	return {
	          		UserReference : response.data
	          	}
	          } else{
	          	refData.UserReference = response.data;
	          }


	          return refData;
	        });

	        return hpromise;
	      }
	      else {

	        return refData;
	      }
	    }
	  }
	}
])

app.factory('TranslationService', ['$rootScope', '$http', '$filter', '$q',
	function($rootScope, $http, $filter, $q) {
	  var refData = {
	    UserReference: null,
	    TranslationMap : null
	  };

	  var refData2 = {
	    TranslationMap : null
	  }
	  return {
	    initialize: function(readonly) {
	      if (!refData.UserReference) {
	       var cc = (sessionStorage.getItem('sessionLang') || 'EN');
	        if ($rootScope.language)
	          cc = $rootScope.language;
	        var url =  $rootScope.servicePrefix+"/reference/translation?cc=" + cc;
	        var hpromise = $http.get(url).then(function(response) {


	          if( readonly ){
	          	return {
	          		UserReference : response.data,
	          		TranslationMap : response.data.TranslationMap
	          	}
	          } else{
	          	refData.UserReference = response.data;
	          	refData.TranslationMap = response.data.TranslationMap;
	          }

	          return refData;
	        });

	        return hpromise;
	      }
	      else {

	        return refData;
	      }
	    },
	    loadTranslationData : function(){
	      if (!refData2.TranslationMap) {
	        var cc = (sessionStorage.getItem('sessionLang') || 'EN');
	        if ($rootScope.language)
	          cc = $rootScope.language;
	        var url = $rootScope.servicePrefix+"/reference/translation?cc=" + cc;
	        var hpromise = $http.get(url).then(function(response) {
	          refData2.TranslationMap = response.data.TranslationMap;
	          return refData2;
	        });

	        return hpromise;
	      }
	      else {

	        return refData2;
	      }
	    }
	  }
	}
])

app.factory('geoService', ['$http', '$q', '$rootScope',
	function($http, $q, $rootScope){
	  var o = {
	    countries : null,
	    provinces : {},
	    cities : {},
	    getProvince : function(value){
	     var cc = (sessionStorage.getItem('sessionLang') || 'EN');
	      if($rootScope.language){ cc = $rootScope.language; };
	      var tmpVal = value + '-' + cc;
	      if(o.provinces[tmpVal]){
	        var deferred = $q.defer();
	        deferred.resolve(o.provinces[tmpVal]);
	        return deferred.promise;
	      }else{
	        return $http.get($rootScope.servicePrefix+"/geo/province?q="+value+"&cc=" + cc).then(function(response) {
	          o.provinces[tmpVal] = response.data;
	          return o.provinces[tmpVal];
	        });
	      }
	    },
	    getCities : function(value){
	      var cc = 'EN';
	      if($rootScope.language){ cc = $rootScope.language; };
	      var tmpVal = value + '-' + cc;
	      if(o.cities[tmpVal]){
	        var deferred = $q.defer();
	        deferred.resolve(o.cities[tmpVal]);
	        return deferred.promise;
	      }else{
	        return $http.get($rootScope.servicePrefix+"/geo/city?q="+value+"&cc=" + cc).then(function(response) {
	          o.cities[tmpVal] = response.data;
	          return o.cities[tmpVal];
	        });
	      }
	    },
	  };

	  return {
	    initialize: function(){
	      var cc = (sessionStorage.getItem('sessionLang') || 'EN');
	      if($rootScope.language){ cc = $rootScope.language; };
	      if (!o.countries){
	        return $http.get($rootScope.servicePrefix+"/geo/country?cc=" + cc).then(function(response) {
				o.countries = response.data.filter(function (node) { //remove the root geo country "---";
					return node.GeoCountryId !== 0;
				});
	          return o;
	        });
	      }else{
	        return o;
	      }
	    }
	  }
	}
])

app.factory('FileUploadService', ['Upload', '$rootScope',
	function(Upload, $rootScope) {
	  var uploadBis = function(files, form, bucket) {
	    form.ProfileImage = ''; //reset as we are re uploading this will only get a value if upload is successful
	    form.ProfileImageProgress = '';
	    var fields = {
	      id: '1'
	    };

	    if (files && files.length) {
	      for (var i = 0; i < files.length; i++) {
	        var file = files[i];
	        Upload.upload({
	          url: $rootScope.servicePrefix+'/image/upload?b='+bucket+'&AccessToken=Bearer '+$rootScope.AuthToken,//add accessToken as param to support IE9 since IE9 use flash to upload files. JWT will put accessToken in the header, but flash would not put anything JWT defined in header.
	          //fields: fields,
	          file: file
	        }).progress(function(evt) {
	          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	          form.ProfileImageProgress = progressPercentage + '% uploaded.';
	        }).error(function(data, status, headers, config) {
                    if (window.console) {
                        console.log("upload error!", data);
                    }
	          form.ProfileImageProgress = 'Error:' + status;
	          form.ProfileImage = '';
	        }).success(function(data, status, headers, config) {
	          form.ProfileImageProgress = '';
	          form.ProfileImage = data.ImageKey;
	        });
	      }
	    }
	  };

		/**
		 * upload file for any model
		 * @param form. the image prop belongs to.
		 * @param imagePropName. image key property name
		 * @param progressPropName. image progress property name
		 **/
		var uploadBisCommon = function (files, form, imagePropName, progressPropName, bucket) {
			if(!files){
				return;
			}
			if(files[0].$error){
				console.log("file upload failed caused by ", files[0].$error);
				return;
			}

			if (!bucket) {
				if (window.console) {
					console.log("bucket must be specified.");
				}
				alert("bucket must be specified, since change to use the new api!");
				return;
			}
			if (imagePropName === undefined) {
				imagePropName = 'ProfileImage';
			}
			if (progressPropName === undefined) {
				progressPropName = 'ProfileImageProgress';
			}
			form[imagePropName] = ''; //reset as we are re uploading this will only get a value if upload is successful
			form[progressPropName] = '';
			var fields = {
				id: '1'
			};
			if (files && files.length) {
				for (var i = 0; i < files.length; i++) {
					var file = files[i];
					Upload.upload({
						url: $rootScope.servicePrefix + '/image/upload?b=' + bucket + '&AccessToken=Bearer ' + $rootScope.AuthToken, //add accessToken as param to support IE9 since IE9 use flash to upload files. JWT will put accessToken in the header, but flash would not put anything JWT defined in header.
						//fields: fields,
						file: file
					}).progress(function (evt) {
						var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
						form[progressPropName] = progressPercentage + '% uploaded.';
					}).error(function (data, status, headers, config) {
						if (window.console) {
							console.log("upload error!", data);
						}
						form[progressPropName] = 'Error:' + status;
						form[imagePropName] = '';
					}).success(function (data, status, headers, config) {
						form[progressPropName] = '';
						form[imagePropName] = data.ImageKey;
					});
				}
			}
		};

		/**
		 * upload file for merchant document
		 * @param form. the image prop belongs to.
		 * @return documentObjs. document object{DocumentKey:DocumentKey, DocumentOriginalName: DocumentOriginalName, DocumentMimeType: DocumentMimeType, CreateTime: CreateTime}
		 **/
        var uploadFile = function (files, documentObjs, $scope) {
            // clear error message on every time picking a file
            if (typeof $scope.uploadError !== 'undefined') {
                $scope.uploadError = '';
            }
            
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    if (file.$error) {
                        switch(file.$error){
                            case "maxSize":
                                $scope.uploadError = 'MSG_ERR_FILE_SIZE';
                                break;
                            default:
                                $scope.uploadError = file.$error+' '+file.$errorParam;
                                break;
                        }
                    } else {

                        Upload.upload({
                            url: $rootScope.servicePrefix + '/document/upload?AccessToken=Bearer ' + $rootScope.AuthToken, //add accessToken as param to support IE9 since IE9 use flash to upload files. JWT will put accessToken in the header, but flash would not put anything JWT defined in header.
                            file: file
                        }).progress(function (evt) {

                        }).error(function (data, status, headers, config) {
                            if (typeof $scope.uploadError !== 'undefined') {
                                $scope.uploadError = data.AppCode;
                            } else {
                                alert("upload error, please try again!");
                            }
//						console.log("upload error!", data);
//						
                        }).success(function (data, status, headers, config) {
                            documentObjs.push({
                                DocumentKey: data.Key,
                                DocumentOriginalName: data.Originalname,
                                DocumentMimeType: data.MimeType,
                                CreateTime: new Date()
                            });
                        });
                    }
                }
            }
        };
        return {
            uploadBis: uploadBis,
            uploadBisCommon: uploadBisCommon,
            uploadFile: uploadFile
        };
    }
])

app.factory('changeLanguageMain', ['$rootScope', '$http',
	function($rootScope, $http) {
	  var refData = {
	    LanguageList: null,
	    init : function(){
	      var url = "/api/reference/changelanguage";
	      var hpromise = $http.get(url).then(function(response) {
	        refData.LanguageList = response.data.LanguageList;
	        $rootScope.selectLanguage = refData.LanguageList;
	        return refData.LanguageList;
	      });
	      return hpromise;
	    },
	    change : function(UserKey, CultureCode, cb){
	      var hpromise = $http.post($rootScope.servicePrefix+'/user/change/language?userkey=' + UserKey, { UserKey : UserKey, CultureCode : CultureCode }).then(function(res){
	        if(res.status == 200){
	          $rootScope.saveUserData({ CultureCode : CultureCode, LanguageId : parseInt(res.data.LanguageId) });
	        }

	        if(cb){
	          return cb();
	        }
	        return res;
	      });
	      return hpromise;
	    }
	  };
	  refData.init();
	  return refData;
	}
])

app.factory('tokenService', ['$rootScope', '$q', '$location', 'jwtHelper', 'changeLanguageMain', 'CONSTANTS',
	function ($rootScope, $q, $location, jwtHelper, changeLanguageMain, CONSTANTS) {
	  return {
	    changeLanguage : function(language){
	    	if(!language){ language = "EN"; }
		    sessionStorage.setItem('sessionLang', language);
		    if($rootScope.User){
		      $rootScope.User.CultureCode = language;
		      changeLanguageMain.change($rootScope.User.UserKey, language, function(){
		        window.location.reload();
		      });
		      return true;
		    }
		    window.location.reload();
		  },
		  saveUserData : function(obj, updateSessionLang){
		    var tmpUserData = sessionStorage.getItem('UserData');
		    if(tmpUserData != null){
		         tmpUserData = JSON.parse( tmpUserData );
		         var keys = Object.keys(obj);
		         keys.forEach(function(k){
		          tmpUserData[ k ] = obj[ k ];
		         });
		         sessionStorage.setItem('UserData', JSON.stringify( tmpUserData ));
             $rootScope.User = tmpUserData;
		    }

		    if(updateSessionLang && obj.CultureCode){
		      sessionStorage.setItem('sessionLang', obj.CultureCode);
		    }
		  },
		  tokenSave : function() {
		    var tokenString = JSON.stringify($rootScope.AuthToken);
		    if( $rootScope.User ){
		      sessionStorage.setItem('UserData', JSON.stringify( $rootScope.User ));
		      if($rootScope.User && $rootScope.User.CultureCode){
		      	sessionStorage.setItem('sessionLang', $rootScope.User.CultureCode);
		      }
		    }
		    sessionStorage.setItem('AuthToken', tokenString);
		  },
		  tokenGet : function() {
		    var AuthToken = sessionStorage.getItem('AuthToken');
		    var UserData = sessionStorage.getItem('UserData');

		    if (AuthToken != null && UserData != null) {
		      $rootScope.AuthToken = AuthToken;
		      var tokenPayload = jwtHelper.decodeToken($rootScope.AuthToken);
		      $rootScope.User = JSON.parse( UserData );
		    }
		  },
		  tokenDelete : function() {
		    $rootScope.AuthToken = null;
		    sessionStorage.removeItem('AuthToken');
		  },
		  isAdmin : function() {
		    var user = $rootScope.User;
		    if(user){
		      return _.contains(user.UserSecurityGroupArray, CONSTANTS.USER_SECURITY_GROUP.MM_ADMIN);
		    }else{
		      return false;
		    }
		  },
		  isMerchantAdmin: function () {
			  var user = $rootScope.User;
			  if(user){
				  return _.contains(user.UserSecurityGroupArray, CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ADMIN);
			  }else{
				  return false;
			  }
		  },
		  isMMUser: function () {
			  var user = $rootScope.User;
			  if(user){
				  return user.UserTypeId === CONSTANTS.USER_TYPE.MM;
			  }else{
				  return false;
			  }
		  },
		  isMerchantUser: function () {
			  var user = $rootScope.User;
			  if(user){
				  return user.UserTypeId === CONSTANTS.USER_TYPE.MERCHANT;
			  }else{
				  return false;
			  }
		  },
		  isSelf: function (userKey) {
			  var user = $rootScope.User;
			  if(user){
				  return user.UserKey === userKey;
			  }else{
				  return false;
			  }
		  },
		  isPendingMerchant: function (merchant) {
			  if (merchant) {
				  return merchant.StatusId === CONSTANTS.STATUS.PENDING
			  }

			  return $rootScope.User && $rootScope.User.Merchant &&
				  $rootScope.User.Merchant.StatusId === CONSTANTS.STATUS.PENDING;
		  }
	  };
	}
])

app.factory('DialogService', ['$modal', 'MainService', function ($modal, MainService){
  var mainService = MainService.initialize();
  return {
    confirm: function(title, content, confirmCallback, cancelCallback, confirmBtnText, cancelBtnText){
      $modal.open({
        templateUrl: "/shared/views/confirm-common.html",
        backdrop: 'static',
        controller: ['$scope', function ($scope) {
          	$scope.title=title;
          	$scope.content=content;
			$scope.confirmBtnText=confirmBtnText;
			$scope.cancelBtnText=cancelBtnText;
          	$scope.t = mainService.UserReference.TranslationMap;
          	$scope.confirm = function () {
            confirmCallback();
            $scope.$dismiss();
          };
          $scope.cancel = function () {
						if (cancelCallback) {
							cancelCallback();
						}
            $scope.$dismiss();
          };
          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
      });
    },
		alert: function(title, content, cancelCallback){
      $modal.open({
        templateUrl: "/shared/views/alert-common.html",
        backdrop: 'static',
        controller: ['$scope', function ($scope) {
          $scope.title=title;
          $scope.content=content;
          $scope.t = mainService.UserReference.TranslationMap;          
          $scope.cancel = function () {
						if (cancelCallback) {
							cancelCallback();
						}
            $scope.$dismiss();
          };
          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
      });
    }
  }
}]);
