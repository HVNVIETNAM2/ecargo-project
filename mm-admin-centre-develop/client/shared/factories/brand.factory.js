'use strict';

/**
 * Provide common operations for inventory management logic
 * @param {angular.$http} $http
 * @param {object} $state
 * @param {object} RequestService
 * @param {object} MainService
 * @constructor
 */
function BrandService($http, $state, RequestService, MainService) {
    var mainService = MainService.initialize();
    var t = mainService.UserReference.TranslationMap;

    return {
        get: function (brandId) {
            return RequestService.get('/brand/view', {params: {brandid: brandId}});
        },
        save: function (brand, brandImageList, isTouchedBrandImages) {
            return RequestService.post('/brand/save', {
                Brand: brand,
                BrandImageList: brandImageList,
                IsTouchedBrandImages: isTouchedBrandImages
            });
        }
    };
}

app.factory('BrandService', ['$http', '$state', 'RequestService', 'MainService', BrandService]);
