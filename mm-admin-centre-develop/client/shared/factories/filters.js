app.filter('offset', function () {
    return function (input, start) {
        start = parseInt(start, 10);
        return input.slice(start);
    };
});

app.filter('momentFilter', function ($rootScope, CONSTANTS) {
    return function (value, format) {
        if (!value || !$rootScope.User) {
            return;
        }

        var user = $rootScope.User;
        var dateFormat = format ? format : CONSTANTS.DATE_FORMAT;

        //todo: should get timezone invariant name or timezone name which fits to moment timezone format
        var timezone = null;
        switch(user['TimeZoneId']) {
            case 1:
                //beijing ==> same as hong kong timezone
                timezone = 'Hongkong';
                break;
            case 2:
                //hong kong
                timezone = 'Hongkong';
                break;
            default:
                //convert from utc to local time
                return moment(moment.utc(value).toDate()).format(dateFormat);
                //break;
        }

        //convert from utc to specified timezone
        return moment.tz(moment.utc(value), timezone).format(dateFormat);
    };
});
