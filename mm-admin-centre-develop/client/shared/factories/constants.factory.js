'use strict'
/* 
 * Service for centralize the behaviours of status type, user type in AC/ MC 
 */
function ConstantsService() {

    var CONSTANTS = {
        STATUS: {},
        STATUS_NAME: {},
        STATUS_CSS: {},
        INVENTORY_STATUS: {},
        INVENTORY_STATUS_NAME: {},
        INVENTORY_STATUS_CSS: {},
        USER_TYPE: {},
        MERCHANT_TYPE: {},
        MERCHANT_TYPE_NAME: {},
        USER_SECURITY_GROUP: {},
        INVENTORY_LOCATION_TYPE: {},
        INVENTORY_LOCATION_TYPE_NAME: {},
        FILE_UPLOAD_SIZE: {},
        NAME_REGX: {},
        FILE_FORMAT: {},
        PERPETUAL_ALLOCATION_NO: 0,
        NUMBER_INPUT: {
            DEFAULT_MIN: 0,
            DEFAULT_MAX: 99999999
        },
        LANGUAGE:{
            CHS:'简',
            CHT:'繁',
            EN:'EN'
        }
    };

//    PRODUCT_IMG_TYPE_CODE : {
//      FEATURE: 'Feature',
//      COLOR: 'Color',
//      DESC: 'Desc'
//    },

    /*
     * Internal string to key mapping function
     */
    function str2Key(str) {
        var key = str.replace('/', '').replace(/ /g, '_').replace('-', '_').toUpperCase();
        return key;
    }

    // item type 1: db data 
    // get status Id to name map
    // get status Invaniant to id map
    // get status css class (TO DO)

    //var cc = (sessionStorage.getItem('sessionLang') || 'EN');
    var generalData = JSON.parse(sessionStorage.getItem('generalData'));
    var translationData = JSON.parse(sessionStorage.getItem('translationData'));


    var data = generalData;
    var t = translationData.TranslationMap;

    var inventoryStatusList = data.InventoryStatusList;
    var statusList = data.StatusList;
    var userTypeList = data.UserTypeList;
    var merchantTypeList = data.MerchantTypeList;
    var securityGroupList = data.SecurityGroupList;
    var inventoryLocationTypeList = data.InventoryLocationTypeList;

    CONSTANTS.FILE_UPLOAD_SIZE_BYTES = data.FileUploadSize;
    CONSTANTS.FILE_UPLOAD_SIZE.DOCUMENT = CONSTANTS.FILE_UPLOAD_SIZE_BYTES.document/1024/1024+'MB';
    CONSTANTS.FILE_UPLOAD_SIZE.IMAGE = CONSTANTS.FILE_UPLOAD_SIZE_BYTES.image/1024/1024+'MB';
    CONSTANTS.FILE_UPLOAD_SIZE.ZIP = CONSTANTS.FILE_UPLOAD_SIZE_BYTES.zip/1024/1024+'MB';

    CONSTANTS.FILE_FORMAT.PROD_IMG = '.jpg,.jpeg,.png,.zip';
    CONSTANTS.FILE_FORMAT.IMG = '.jpg,.jpeg,.png';
    CONSTANTS.NAME_REGX.PROD_IMG = /^([A-Z0-9]+)_?([A-Z0-9-_]+)?_([0-9]+)(.\w+)$/i;

    var statusIdMap = {};
    angular.forEach(statusList, function (status, key) {
        statusIdMap[str2Key(status.StatusNameInvariant)] = status.StatusId;
//            CONSTANTS.STATUS_NAME[status.StatusId] = status.StatusName;
    });

    var inventoryStatusIdMap = {};
    angular.forEach(inventoryStatusList, function (status, key) {
        inventoryStatusIdMap[str2Key(status.InventoryStatusNameInvariant)] = status.InventoryStatusId;
        CONSTANTS.INVENTORY_STATUS_NAME[status.InventoryStatusId] = status.InventoryStatusName;
    });

    var userTypeMap = {};
    angular.forEach(userTypeList, function (userType, key) {
        userTypeMap[str2Key(userType.UserTypeNameInvariant)] = userType.UserTypeId;
    });

    var merchantTypeMap = {};
    angular.forEach(merchantTypeList, function (merchantType, key) {
        merchantTypeMap[str2Key(merchantType.MerchantTypeNameInvariant)] = merchantType.MerchantTypeId;
        CONSTANTS.MERCHANT_TYPE_NAME[merchantType.MerchantTypeId] = merchantType.MerchantTypeName;
    });

    var securityGroupMap = {};
    angular.forEach(securityGroupList, function (securityGroup, key) {
        securityGroupMap[str2Key(securityGroup.SecurityGroupNameInvariant)] = securityGroup.SecurityGroupId;
    });

    angular.forEach(inventoryLocationTypeList, function (inventoryLocationType, key) {
        CONSTANTS.INVENTORY_LOCATION_TYPE[str2Key(inventoryLocationType.InventoryLocationTypeNameInvariant)] = inventoryLocationType.InventoryLocationTypeId;
        CONSTANTS.INVENTORY_LOCATION_TYPE_NAME[inventoryLocationType.InventoryLocationTypeId] = inventoryLocationType.InventoryLocationTypeName;
    });

    CONSTANTS.STATUS = {
        DELETED: statusIdMap['DELETED'],
        ACTIVE: statusIdMap['ACTIVE'],
        PENDING: statusIdMap['PENDING'],
        INACTIVE: statusIdMap['INACTIVE']
    };

    CONSTANTS.STATUS_NAME['-1'] = t.LB_ALL_STATUS;
    CONSTANTS.STATUS_NAME[CONSTANTS.STATUS.DELETED] = t.LB_DELETE;
    CONSTANTS.STATUS_NAME[CONSTANTS.STATUS.ACTIVE] = t.LB_ACTIVE;
    CONSTANTS.STATUS_NAME[CONSTANTS.STATUS.PENDING] = t.LB_PENDING;
    CONSTANTS.STATUS_NAME[CONSTANTS.STATUS.INACTIVE] = t.LB_INACTIVE;

    CONSTANTS.STATUS_CSS[CONSTANTS.STATUS.DELETED] = 'label-danger';
    CONSTANTS.STATUS_CSS[CONSTANTS.STATUS.ACTIVE] = 'label-success';
    CONSTANTS.STATUS_CSS[CONSTANTS.STATUS.PENDING] = 'label-warning';
    CONSTANTS.STATUS_CSS[CONSTANTS.STATUS.INACTIVE] = 'label-default';

    CONSTANTS.INVENTORY_STATUS = {
        IN_STOCK: inventoryStatusIdMap['INSTOCK'],
        LOW_STOCK: inventoryStatusIdMap['LOWSTOCK'],
        OUT_OF_STOCK: inventoryStatusIdMap['OUTOFSTOCK'],
        NA: inventoryStatusIdMap['NA']
    };

    CONSTANTS.INVENTORY_STATUS_CSS[CONSTANTS.INVENTORY_STATUS.IN_STOCK] = 'label label-success';
    CONSTANTS.INVENTORY_STATUS_CSS[CONSTANTS.INVENTORY_STATUS.LOW_STOCK] = 'label label-warning';
    CONSTANTS.INVENTORY_STATUS_CSS[CONSTANTS.INVENTORY_STATUS.OUT_OF_STOCK] = 'label label-danger';
    CONSTANTS.INVENTORY_STATUS_CSS[CONSTANTS.INVENTORY_STATUS.NA] = '';

    CONSTANTS.USER_TYPE = {
        MM: userTypeMap['MM'],
        MERCHANT: userTypeMap['MERCHANT']
    };

    CONSTANTS.MERCHANT_TYPE = {
        BRAND_FLAGSHIP: merchantTypeMap['BRAND_FLAGSHIP'],
        MONO_BRAND_AGENT: merchantTypeMap['MONO_BRAND_AGENT'],
        MULTI_BRAND_AGENT: merchantTypeMap['MULTI_BRAND_AGENT']
    };

    CONSTANTS.USER_SECURITY_GROUP = {
        MM_ADMIN: securityGroupMap['MM_ADMIN'],
        MM_CUSTOMER_SERVICE: securityGroupMap['MM_CUSTOMER_SERVICE'],
        MM_FINANCE: securityGroupMap['MM_FINANCE'],
        MM_TECH_OPS: securityGroupMap['MM_TECH_OPS'],
        MERCHANT_FINANCE: securityGroupMap['MERCHANT_FINANCE'],
        MERCHANT_ADMIN: securityGroupMap['MERCHANT_ADMIN'],
        MERCHANT_CUSTOMER_SERVICE: securityGroupMap['MERCHANT_CUSTOMER_SERVICE'],
        MERCHANT_CONTENT_MANAGER: securityGroupMap['MERCHANT_CONTENT_MANAGER'],
        MERCHANT_MERCHANDISER: securityGroupMap['MERCHANT_MERCHANDISER'],
        MERCHANT_ORDER_PROCESSSING: securityGroupMap['MERCHANT_ORDER_PROCESSSING'],
        MERCHANT_PRODUCT_RETURN: securityGroupMap['MERCHANT_PRODUCT_RETURN']
    };

        // DEFAULT value for pagination
        CONSTANTS.DEFAULT_PAGE_NO = 1;
    CONSTANTS.DEFAULT_PAGE_COUNT = 5;
    CONSTANTS.ITEMS_PER_PAGE = [10, 25, 100];
    CONSTANTS.DEFAULT_ITEMS_PER_PAGE = CONSTANTS.ITEMS_PER_PAGE[0];
    CONSTANTS.PAGE_SPAN = 4;

    CONSTANTS.PERPETUAL_ALLOCATION_NO = data.PerpetualAllocationNo;

    CONSTANTS.DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

    // item type 2: view page defined filter
    // make constant in the corresponding controller

    return CONSTANTS;
}

app.factory('CONSTANTS', ConstantsService);