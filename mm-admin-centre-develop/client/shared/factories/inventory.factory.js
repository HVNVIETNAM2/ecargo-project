'use strict';

/**
 * Provide common operations for inventory management logic
 * @param {object} $state
 * @apram {object} RequestService
 * @param {object} MainService
 * @constructor
 */
function InventoryService($rootScope, $state, RequestService, MainService) {
    var CONSTANTS = $rootScope.CONSTANTS;
    var $state = $state;
    var mainService = MainService.initialize();
//    var generalTranslationService = GeneralTranslationService.initialize();
    var t = mainService.UserReference.TranslationMap;

    return {
        get: function (params) {
            return RequestService.get('/inventory/view', params);
        },
        reset: function (inventory) {
            return RequestService.post('/inventory/status/change', inventory);
        },
        save: function (inventory) {
            return RequestService.post('/inventory/save', inventory);
        },
        getHistory: function (params) {
            return RequestService.get('/inventory/list/journal', params);
        },
        getLocation: function (params) {
            return RequestService.get('/inventory/location/view', params);
        },
        createInventory: function (item, isFromLocation) {
            $state.go((isFromLocation ? 'location' : 'product') + 'Inventory-create', {
                skuid: item.SkuId,
                inventorylocationid: item.InventoryLocationId,
                merchantId: $state.params.merchantId
            });
        },
        editInventory: function (item, isFromLocation) {
            $state.go((isFromLocation ? 'location' : 'product') + 'Inventory-edit', {
                skuid: item.SkuId,
                inventorylocationid: item.InventoryLocationId,
                inventoryid: item.InventoryId,
                merchantId: $state.params.merchantId
            });
        },
        getATS: function (inventory) {
            return inventory.IsPerpetual ? t.LB_INVENTORY_UNLIMITED : (!_.isNaN(_.parseInt(inventory.QtyAts)) ? inventory.QtyAts : t.LB_NA);
        },
        countrySort: function (country) {
            //china, hong kong, macau, taiwan should be placed at top of the list
            return country.GeoCountryId !== 47 && country.GeoCountryId !== 95 && country.GeoCountryId !== 149;
        }
    };
}

app.factory('InventoryService', InventoryService);

