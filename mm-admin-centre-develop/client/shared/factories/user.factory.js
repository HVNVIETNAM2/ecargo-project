'use strict';

/**
 * Provide common operations for inventory management logic
 * @param {angular.$http} $http
 * @param {object} $state
 * @param {object} RequestService
 * @constructor
 */
function UserService($http, $state, RequestService) {

    return {
        validateToken: function (token) {
            return RequestService.post('/auth/token/validate', {ActivationToken: token});
        }
    }
}

app.factory('UserService', ['$http', '$state', 'RequestService', UserService]);
