'use strict';

function MMSubdomainInput($compile) {
    var link = function (scope, element) {
        scope.$parent.$fakeLoop = [1];

        var template = '<div ng-repeat="l in $fakeLoop" class="form-group col-md-12">' +
            '<label class="col-md-2 text-right mm_form_label control-label">{{::t.LB_SUBDOMAIN}} ' +
            '<mm-tooltip tooltip-title="t[\'' + scope.tooltipTitle + '\']"></mm-tooltip>' +
            '</label>' +
            '<div class="col-md-6"' +
            'ng-class="{\'has-error\': (validationForm.$submitted || validationForm[\'' + scope.prop + '\'].$touched) && ' +
            '(validationForm[\'' + scope.prop + '\'].$error.required || validationForm[\'' + scope.prop + '\'].$error.pattern)}">' +
            '<div class="input-group">' +
            '<span class="input-group-addon">http://</span>' +
            '<input type="text" placeholder="{{::t.LB_SUBDOMAIN}}"' +
            'class="form-control"' +
            'placeholder="{{::t.LB_SUBDOMAIN}}" name="' + scope.prop + '" ' +
            'ng-model="' + scope.model + '" required ' +
            'ng-pattern="/^([a-zA-Z0-9]){0,25}$/" />' +
            '<span class="input-group-addon">.mymm.com</span>' +
            '</div>' +
            '</div>' +
            '<div ng-show="validationForm[\'' + scope.prop + '\'].$invalid && (validationForm.$submitted || validationForm[\'' + scope.prop + '\'].$touched)"' +
            'class="help-block login-error col-md-offset-2 col-md-10">' +
            '<span ng-show="validationForm[\'' + scope.prop + '\'].$error.required">{{::t.MSG_ERR_SUBDOMAIN}}</span>' +
            '<span ng-show="validationForm[\'' + scope.prop + '\'].$error.pattern">{{::t.MSG_ERR_SUBDOMAIN}}</span>' +
            '</div>' +
            '</div>';

        element.replaceWith($compile(angular.element(template))(scope.$parent));
    };

    return {
        restrict: 'E',
        link: link,
        scope: {
            prop: '@',
            model: '@',
            tooltipTitle: '@'
        }
    };
}

app.directive('mmSubdomainInput', MMSubdomainInput);