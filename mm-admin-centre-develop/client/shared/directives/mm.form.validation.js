'use strict';

function MMFormValidation($timeout) {
    return {
        restrict: 'A',
        scope: {
            isButton: '=',
            formName: '@',
            customValidations: '='
        },
        link: function (scope, element) {
            element.on(scope.isButton ? 'click' : 'submit', function () {
                //validation
                var errorEle = angular.element('form[name=' + (scope.formName ? scope.formName : 'validationForm') + ']').find('.ng-invalid:first');
                if (errorEle.hasClass('hide-on-focus')) {
                    errorEle.show();
                    errorEle.blur();
                    errorEle.focus();
                    errorEle.hide();
                } else if (errorEle.length) {
                    errorEle.data('$ngModelController').$touched = true;
                    errorEle.blur();
                    errorEle.focus();
                } else {
                    _.each(scope.customValidations, function (validation) {
                        if (!validation.isValid()) {
                            errorEle = _.isFunction(validation.element) ? validation.element() : validation.element;
                            errorEle.show();
                            errorEle.blur();
                            errorEle.focus();
                            if (validation.isHidden) {
                                errorEle.hide();
                            }
                        }
                    });
                }
            });
        }
    };
}

app.directive('mmFormValidation', MMFormValidation);


