'use strict';

function MMFromToDatepicker() {
    return {
        restrict: 'E',
        controller: ['$scope', '$timeout', 'MainService', 'CONSTANTS', function ($scope, $timeout, MainService, CONSTANTS) {
            var mainService = MainService.initialize();
            $scope.t = mainService.UserReference.TranslationMap;
            $scope.CONSTANTS = CONSTANTS;
            $scope.showStartTimeErr = false;
            $scope.showEndTimeErr = false;
            $scope.isStartTimeOpen = false;
            $scope.isEndTimeOpen = false;
            $scope.required = false;

            CONSTANTS.DATETIME_PICKER = {
                IS_FOREVER: 1,
                IS_NOT_FOREVER: 0
            };

            $scope.setError = function () {
                if ($scope.showEndTimeErr || $scope.showStartTimeErr ||
                    $scope.showEndMaxTimeErr || $scope.showStartMinTimeErr) {
                    $scope.isError = null; //true value for validation form of error "required"
                } else {
                    $scope.isError = false;
                }
            };

            /**
            function isDateInRange(date, minTime, maxTime) {
                var hasError = false;
                if (!date && !minTime) {
                    return hasError;
                }

                var targetCheckDate = moment(date).format(CONSTANTS.DATE_FORMAT);

                if (minTime) {
                    var minTimeDate = moment(minTime).format(CONSTANTS.DATE_FORMAT);

                    if (!(moment(targetCheckDate).isSameOrAfter(minTimeDate))) {
                        hasError = true;
                    }
                }

                if (maxTime) {
                    var maxTimeDate = moment(maxTime).format(CONSTANTS.DATE_FORMAT);

                    if (!(moment(targetCheckDate).isSameOrBefore(maxTimeDate))) {
                        hasError = true;
                    }
                }
                return hasError;
            }
             **/


            $scope.$watch('isForever', function (val) {
                $scope.disable = !!val;
                if ($scope.disable) {
                    $scope.startTime = $scope.endTime = null;
                }
            });

            $scope.$watchGroup(['startTime', 'endTime'], function (newVal) {
                $timeout(function () {
                    if(!$scope.startTime && !$scope.endTime) {
                        $scope.disable = true;
                        $scope.isForever = CONSTANTS.DATETIME_PICKER.IS_FOREVER;
                        $scope.startTime = $scope.endTime = null;
                    } else {
                        $scope.disable = false;
                        $scope.isForever = CONSTANTS.DATETIME_PICKER.IS_NOT_FOREVER;
                    }
                });
            });

            $scope.openCalendar = function (e, propName) {
                e.preventDefault();
                e.stopPropagation();
                $scope[propName] = true;
            };

            $scope.clearTime = function (time) {
                $scope[time] = null;
            };

            $scope.disableForever = function () {
                return $scope.endMaxTime || $scope.startMinTime;
            };

            $scope.shouldShowClearBtn = function (time) {
                return ((time === 'startTime' && $scope[time] && !$scope.startMinTime) ||
                (time === 'endTime' && $scope[time] && !$scope.endMaxTime));
            };

            $scope.$watch('isStartTimeOpen', function (val) {
                if (!val) {
                    if ($scope.startTime && moment($scope.startTime).isValid() && $scope.endTime && moment($scope.startTime).isValid() &&
                        moment($scope.startTime).isAfter($scope.endTime)) {
                        $scope.startTime = $scope.endTime;
                    }
                    $scope.showStartTimeErr = false; //isDateInRange($scope.startTime, $scope.startMinTime, $scope.startMaxTime);
                } else {
                    $scope.isEndTimeOpen = false;
                }

                
            });

            $scope.$watch('isEndTimeOpen', function (val) {
                if (!val) {
                    if ($scope.endTime && moment($scope.endTime).isValid() && $scope.startTime && moment($scope.endTime).isValid() &&
                        moment($scope.endTime).isBefore($scope.startTime)) {
                        $scope.endTime = $scope.startTime;
                    }
                    $scope.showEndTimeErr = false; //isDateInRange($scope.endTime, $scope.endMinTime, $scope.endMaxTime);
                } else {
                    $scope.isStartTimeOpen = false;
                }

                
            });

            $scope.$watchGroup(['startTime', 'startMinTime', 'startMaxTime'], function (newVal) {
                /**
                if (typeof $scope.startTime !== 'undefined' && $scope.startTime !== null) {
                    if ($scope.startTime && moment($scope.startTime).isValid() && $scope.endTime && moment($scope.endTime).isValid() &&
                        moment($scope.startTime).isAfter($scope.endTime)) {
                        $scope.showStartTimeErr = true;
                    } else {
                        $scope.showStartTimeErr = isDateInRange($scope.startTime, $scope.startMinTime, $scope.startMaxTime);
                    }
                }
                 **/

                if (($scope.startMinTime && !$scope.startTime) ||
                    ($scope.startMinTime && $scope.startTime && moment($scope.startTime).isBefore($scope.startMinTime))) {
                    if (moment($scope.startMinTime).isBefore($scope.endTime) || !$scope.startTime) {
                        $scope.startTime = $scope.startMinTime;
                    }

                    $scope.showStartMinTimeErr = true;
                } else {
                    if ($scope.startTime && moment($scope.startTime).isValid() && $scope.endTime && moment($scope.endTime).isValid() &&
                        moment($scope.startTime).isAfter($scope.endTime)) {
                        $scope.showStartTimeErr = $scope.isStartTimeOpen;
                    } else {
                        $scope.showStartTimeErr = false;
                    }

                    $scope.showStartMinTimeErr = false;
                }

                
            });

            $scope.$watchGroup(['endTime', 'endMinTime', 'endMaxTime'], function (newVal) {
                /**
                if (typeof $scope.endTime !== 'undefined' && $scope.endTime !== null) {
                    if ($scope.endTime && moment($scope.endTime).isValid() && $scope.endTime && moment($scope.endTime).isValid() &&
                        moment($scope.endTime).isBefore($scope.startTime)) {
                        $scope.showEndTimeErr = true;
                    } else {
                        $scope.showEndTimeErr = isDateInRange($scope.endTime, $scope.endMinTime, $scope.endMaxTime);
                    }
                } else
                 **/

                if (($scope.endMaxTime && !$scope.endTime) ||
                    ($scope.endMaxTime && $scope.endTime && moment($scope.endTime).isAfter($scope.endMaxTime))) {
                    if (moment($scope.endMaxTime).isAfter($scope.startTime) || !$scope.endTime) {
                        $scope.endTime = $scope.endMaxTime;
                    }

                    $scope.showEndMaxTimeErr = true;
                } else {
                    if ($scope.endTime && moment($scope.endTime).isValid() && $scope.startTime && moment($scope.startTime).isValid() &&
                        moment($scope.endTime).isBefore($scope.startTime)) {
                        $scope.showEndTimeErr = $scope.isEndTimeOpen;
                    } else {
                        $scope.showEndTimeErr = false;
                    }

                    $scope.showEndMaxTimeErr = false;
                }

                
            });

            /**
            $scope.$watchGroup(['requiredField', 'startTime', 'endTime'], function (val) {
                if ($scope.requiredField > 0 &&
                    ((!$scope.startTime && $scope.endTime) || ($scope.startTime && !$scope.endTime))) {
                    $scope.required = true;
                    $scope.isError = true;
                    if (!$scope.startTime) {
                        $scope.showStartTimeErr = true;
                    } else {
                        $scope.showEndTimeErr  = true;
                    }
                } else {
                    $scope.required = false;
                    $scope.isError = $scope.showEndTimeErr || $scope.showStartTimeErr;
                }
            });

            $scope.setStartMaxTime = function () {
                $scope.startMaxTime = $scope.endTime;
            };

            $scope.setStartMinTime = function () {
                $scope.startMinTime = $scope.endTime;
            };
             **/

            $scope.$watchGroup(['showEndTimeErr', 'showStartTimeErr', 'showEndMaxTimeErr', 'showStartMinTimeErr'], $scope.setError);



        }],
        scope: {
            isError: '=',
            isForever: '=',
            startTime: '=',
            endTime: '=',
            startMinTime: '=',
            startMaxTime: '=',
            endMinTime: '=',
            endMaxTime: '=',
            startMinTimeError: '@',
            endMaxTimeError: '@',
            //todo: fix ie9 before activating this wrapperStyle
            //wrapperStyle: '@',
            wrapperClass: '@',
            tooltipTitle: '@',
            label: '@',
            foreverLabel: '@' //optional, if not specified, default value would be "forever";
        },
        replace: true,
        templateUrl: '/shared/directives/templates/mm-from-to-datepicker.html'
    };
}

app.directive('mmFromToDatepicker', MMFromToDatepicker);