'use strict';

function MMNumberInput($compile, CONSTANTS) {
    return {
        restrict: 'E',
        scope: {
            label: '@',
            placeholder: '@',
            model: '@',
            prop: '@',
            allowDecimal: '@',
            decimalUpto: '@',
            required: '@',
            min: '@',
            max: '@',
            labelClass: '@',
            inputClass: '@',
            inputStyle: '@',
            isPercentage: '@',
            requiredErrMsg: '@',
            minErrMsg: '@',
            maxErrMsg: '@',
            isOptional: '@'
        },
        link: function (scope, element) {
            scope.$parent.Util = Util;

            scope.$parent.$fakeLoop = [1];
            if (scope.isPercentage) {
                scope.min = 0;
                scope.max = 100;
                scope.allowDecimal = 'true';
            } else {
                scope.min = _.isUndefined(scope.min) ? CONSTANTS.NUMBER_INPUT.DEFAULT_MIN : scope.min;
                scope.max = _.isUndefined(scope.max) ? CONSTANTS.NUMBER_INPUT.DEFAULT_MAX : scope.max;
            }

            scope.minErrMsg = scope.minErrMsg ? scope.minErrMsg : 'MSG_ERR_NUM_LESS_THAN';
            scope.maxErrMsg = scope.maxErrMsg ? scope.maxErrMsg : 'MSG_ERR_NUM_GREATER_THAN';

            var template = '<div ng-repeat="l in $fakeLoop" class="form-inline form-group col-md-12" style="min-width: 120px">' +
                (scope.label ? '<label class="' + (scope.labelClass ? scope.labelClass : 'col-md-2') + ' mm_form_label control-label">{{::t.' + scope.label + '}}</label>' : '') +
                '<div class="' + (scope.inputClass ? scope.inputClass : 'col-md-10') + '" ng-class="{\'has-error\': (validationForm.$submitted || validationForm[\'' + scope.prop + '\'].$touched) && ' +
                '(validationForm[\'' + scope.prop + '\'].$error.max || validationForm[\'' + scope.prop + '\'].$error.min ' +
                (scope.required ? ' || validationForm[\'' + scope.prop + '\'].$error.required ' : '' ) + ')}">' +
                '<input type="text" ' + (scope.placeholder ? 'placeholder="{{t.' + scope.placeholder + '}} ' + (scope.isOptional ? '{{::t.LB_OPTIONAL}}' : '') + '"' : '') + ' class="form-control" mm-number-validation ' +
                'min="' + scope.min + '" max="' + scope.max + '" ' + (scope.allowDecimal ? 'allow-decimal="true" decimal-upto="' + (scope.decimalUpto ? scope.decimalUpto : '2') + '" ' : '') + (scope.isPercentage ? 'is-percentage="true"' : '') + ' style="' + (scope.inputStyle ? scope.inputStyle : 'width:80px') + '" min="0" ' +
                'name="' + scope.prop + '" ' +
                'ng-model="' + scope.model + '" ' + (scope.required ? 'required' : '') + '>' +
                (scope.isPercentage ? '<span style="display:inline-block; margin:10px 0 0 4px">%</span>' : '') +
                '<div ng-show="validationForm.$submitted || validationForm[\'' + scope.prop + '\'].$touched" class="help-block login-error">' +
                (scope.required ? '<span ng-show="validationForm[\'' + scope.prop + '\'].$error.required">{{::t.' + scope.requiredErrMsg + '}}</span>' : '') +
                '<span ng-show="validationForm[\'' + scope.prop + '\'].$error.min">{{::Util.format(t.' + scope.minErrMsg + ', [\'' + scope.min + '\'])}}</span>' +
                '<span ng-show="validationForm[\'' + scope.prop + '\'].$error.max">{{::Util.format(t.' + scope.maxErrMsg + ', [\'' + scope.max + '\'])}}</span>' +
                '</div>' +
                '</div>' +
                '</div>';

            element.replaceWith($compile(angular.element(template))(scope.$parent));
        }
    };
}

app.directive('mmNumberInput', MMNumberInput);