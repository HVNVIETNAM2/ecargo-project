'use strict';

function MMMultilingualInputs($compile, $parse, CONSTANTS) {

    var link = function (scope, element) {
        scope.$parent.CONSTANTS = CONSTANTS;
        scope.$parent.$languagesMapping = CONSTANTS.LANGUAGE;
        scope.$parent.$languages = _.keys(scope.$parent.$languagesMapping);
        var otherLanguages = _.filter(scope.$parent.$languages, function (lang) {
            return lang !== 'CHS';
        });
        scope.formName = scope.formName ? scope.formName : 'validationForm';
        scope.prop = _.trim(scope.propPrefix) + _.trim(scope.prop);

        scope.$parent.$hasErrorMultiLingual = function (validationForm, constraints, prop, language) {
            if (!validationForm) {
                return false;
            }

            var p = scope.$parent.$getNameMultiLingual(prop, language);
            return (validationForm.$submitted || validationForm[p].$touched) && _.any(JSON.parse(constraints.replace(/\*/g, '\"')), function (constraint) {
                    return validationForm[p].$error[constraint.validator];
                });
        };

        scope.$parent.$showErrorMultiLingual = function (validationForm, prop, language, validator) {
            if (!validationForm) {
                return false;
            }

            var p = scope.$parent.$getNameMultiLingual(prop, language);
            return validationForm[p].$error[validator];
        };

        scope.$parent.$getNameMultiLingual = function (prop, language) {
            return ''.concat(prop, language);
        };

        scope.$parent.$watch(scope.model.replace('language', "'CHS'"), function (newVal, oldVal) {
            var prop = _.trim(scope.propPrefix) + _.trim(scope.prop);

            oldVal = typeof oldVal === 'undefined' ? '' : oldVal;
            //copy chs to other languages if they are identical
            var isAllIdentical = _.all(otherLanguages, function (language) {
                var val = $parse(scope.model.replace('language', "'" + language + "'"))(scope.$parent);
                val = typeof val === 'undefined' ? '' : val;
                return val === oldVal;
            });

            if (isAllIdentical) {
                _.each(otherLanguages, function (language) {
                    var name = scope.$parent.$getNameMultiLingual(prop, language);
                    scope.$parent.$eval(scope.formName + '.' + name + '.$touched=false;');
                    scope.$parent.$eval(scope.model.replace('language', "'" + language + "'") + '=' + scope.model.replace('language', "'CHS'"));
                });
            }
        });

        var template = '<div class="' + (scope.wrapperClass ? scope.wrapperClass : 'form-group col-md-12 col-sm-12 col-xs-12') + '" ng-repeat="language in $languages">' +
            '<label class="' + (scope.labelClass ? scope.labelClass : 'col-md-2 col-sm-2 col-xs-2 mm_form_label control-label') + '"><span ng-if="$first">' + (scope.labelTransclude ? scope.labelTransclude : '{{::t.' + scope.label + '}}') + '</span></label>' +
            '<div class="' + (scope.inputDivClass ? scope.inputDivClass : 'col-md-6 col-sm-6 col-xs-6') + '" ng-class="{\'has-error\': $hasErrorMultiLingual(' + scope.formName + ', \'' + JSON.stringify(scope.constraints).replace(/"/g, '\*') + '\', \'' + scope.prop + '\', language)}">' +
            '<div class="col-md-12 input-group">' +
            '<' + (scope.isTextArea ? 'textarea' : 'input type="text"') + ' ng-model-options="{allowInvalid: true}" ' +
            'class="form-control" placeholder="{{::t.' + scope.label + '}} ' + (scope.placeholderLanguage ? '({{$languagesMapping[language]}})' : '') + ' ' + (scope.isOptional ? ' {{t.LB_OPTIONAL}}' : '') + '" name="{{$getNameMultiLingual(\'' + scope.prop + '\',language)}}" ' +
            'ng-model="' + scope.model + '"' +
            (scope.disable ? 'ng-disabled="' + scope.disable + '" ' : '') +
            (scope.required ? 'required ' : '') +
            (scope.maxLength ? 'ng-maxlength="' + scope.maxLength + '" ' : '') +
            (scope.pattern ? 'ng-pattern="' + scope.pattern + '" ' : '') +
            (scope.isTextArea ? ' rows="4" cols="50"/></textarea>' : '/>') +
            '<span class="input-group-addon" id="' + scope.prop + 'ENAddOn" style="padding:0 12px 0 10px">{{CONSTANTS.LANGUAGE[language]}}</span>' +
            '</div>' +
            '<div ng-show="' + scope.formName + '[$getNameMultiLingual(\'' + scope.prop + '\',language)].$invalid && (' + scope.formName + '.$submitted || ' + scope.formName + '[$getNameMultiLingual(\'' + scope.prop + '\',language)].$touched)" class="help-block login-error">' +
            _.reduce(scope.constraints, function (result, constraint) {
                return result + '<div ng-show="$showErrorMultiLingual(' + scope.formName + ', \'' + scope.prop + '\', language, \'' + constraint.validator + '\')">{{::t[\'' + constraint.message + '\']}}</div>';
            }, '') +
            '</div>' +
            '</div>' +
            '</div>';

        element.replaceWith($compile(angular.element(template))(scope.$parent));
    };

    return {
        restrict: 'E',
        link: link,
        scope: {
            formName: '@',
            labelTransclude: '@',
            disable: '@',
            label: '@',
            prop: '@',
            propPrefix: '=',
            model: '@',
            isTextArea: '@',
            isOptional: '@',
            constraints: '=',
            maxLength: '@',
            required: '@',
            pattern: '@',
            placeholderLanguage: '@',
            labelClass: '@',
            inputDivClass: '@',
            wrapperClass: '@'
        }
    };
}

app.directive('mmMultilingualInputs', MMMultilingualInputs);