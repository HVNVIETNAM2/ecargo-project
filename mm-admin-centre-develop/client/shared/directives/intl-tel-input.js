app.directive('intlTelInput', ['$timeout', function($timeout) {
  // intl-tel-input options
  var options = {
    allowExtensions: false,
    autoFormat: true,
    autoHideDialCode: true,
    autoPlaceholder: true,
    customPlaceholder: null,
    defaultCountry: "",
    geoIpLookup: null,
    nationalMode: true,
    numberType: "MOBILE",
    preferredCountries: ['hk', 'cn'],
    skipUtilScriptDownload: false,
    utilsScript: '../lib/intl-tel-input/js/utils.js'
  };

  // map mobile code to country code
  var codeMap = {}
  _.each($.fn.intlTelInput.getCountryData(), function (data) {
    codeMap['+' + data.dialCode] = data.iso2;
  });

  return {
    restrict: 'A',
    require: '^ngModel',
    scope: {
      ngModel: '=',
      mobileCode: '='
    },
    link: function(scope, element, attrs, ctrl) {
      element.intlTelInput(options);

      scope.$watch('mobileCode', function(newValue) {
        if (codeMap[newValue]) {
          element.intlTelInput("setCountry", codeMap[newValue]);
        }
      });
      ctrl.$formatters.push(function(value) {
        if (!value) return value;
        element.intlTelInput('setNumber', value);
        return element.val();
      });
      ctrl.$parsers.push(function(value) {
        if (typeof value !== 'string') return value;
        return value.replace(/[^\d]/g, '');
      });
      ctrl.$validators.pattern = function(value) {
        if (!value) {
          return true;
        }

        var oldVal = element.val();
        element.intlTelInput('setNumber', ''.concat(scope.mobileCode, value));
        var isValid = element.intlTelInput("isValidNumber");
        element.intlTelInput('setNumber', oldVal);
        return isValid;
      };
      element.on('blur keyup change', function(event) {
        scope.$apply(function () {
          scope.mobileCode = '+' + element.intlTelInput("getSelectedCountryData").dialCode;
          ctrl.$setViewValue(element.val());
        });
      });
      element.on('$destroy', function() {
        element.intlTelInput('destroy');
        element.off('blur keyup change');
      });
    }
  };
}]);
