'use strict';

//could be improved by http://gsferreira.com/archive/2014/05/angularjs-smart-float-directive/
function MMNumberValidation(CONSTANTS) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            //why dont we use input type number?
            //http://stackoverflow.com/questions/18852244/how-to-get-the-raw-value-an-input-type-number-field
            //therefore, the newValue for invalid input cannot be caught
            //if we need spinner, we may custom add them

            //todo: report from QA team, if you type fast, you see a blink of invalid characters,
            // may use keydown event

            var validate = function (newValue, oldValue) {
                if (newValue === undefined || _.trim(newValue) === '') {
                    ngModel.$setValidity('min', true);
                    ngModel.$setValidity('max', true);
                    return;
                }

                var min = attrs.min === undefined ? CONSTANTS.NUMBER_INPUT.DEFAULT_MIN :
                    (!_.isNaN(parseFloat(attrs.min)) ? parseFloat(attrs.min) : parseFloat(scope.$parent.$eval(attrs.min)) + 1);
                var max = attrs.max === undefined ? CONSTANTS.NUMBER_INPUT.DEFAULT_MAX :
                    (!_.isNaN(parseFloat(attrs.max)) ? parseFloat(attrs.max) : parseFloat(scope.$parent.$eval(attrs.max)) - 1);

                if (attrs.isPercentage) {
                    min = 0;
                    max = 100;
                    attrs.allowDecimal = 'true';
                }

                if(attrs.allowDecimal === "false") {
                    newValue = parseInt(newValue);
                    ngModel.$setViewValue(newValue);
                    ngModel.$render();
                }

                if(attrs.allowDecimal !== "false") {
                    if(attrs.decimalUpto) {
                        var n = String(newValue).split(".");
                        if(n[1]) {
                            var n2 = n[1].slice(0, attrs.decimalUpto);
                            newValue = [n[0], n2].join(".");
                            ngModel.$setViewValue(newValue);
                            ngModel.$render();
                        }
                    }
                }

                if (parseFloat(newValue) < min) {
                    //newValue = min;
                    //ngModel.$setViewValue(newValue);
                    ngModel.$render();
                    ngModel.$setValidity('min', false);
                } else {
                    ngModel.$setValidity('min', true);
                }

                if (parseFloat(newValue) > max) {
                    //newValue = max;
                    //ngModel.$setViewValue(newValue);
                    ngModel.$render();
                    ngModel.$setValidity('max', false);
                } else {
                    ngModel.$setValidity('max', true);
                }

                ngModel.$touched = true;

                /*Check it is number or not.*/
                if (isNaN(newValue)) {
                    ngModel.$setViewValue(oldValue);
                    ngModel.$render();
                }
            };

            scope.$watchGroup([attrs.min, attrs.max], function() {
                validate(ngModel.$modelValue, ngModel.$modelValue);
            });

            scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                validate(newValue, oldValue);
            });
        }
    };
}

app.directive('mmNumberValidation', MMNumberValidation);