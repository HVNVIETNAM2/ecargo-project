function ModalUserFormCtrl($scope, $modalInstance, mainService, generalTranslationService, FileUploadService, Http, $rootScope) {
    var CONSTANTS = setScopeConstants();
    
    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }
    
  $scope.mainService = mainService;
  $scope.generalTranslationService = generalTranslationService;

  $scope.FileUploadService = FileUploadService;
  $scope.inventoryLocations = [];

  $scope.displayUserNames = []; //store the user names which will alert after finish the whole creating users.
  $scope.isCreated = false; //identify wheather add users or not. [true - add user | false - no add user]
  $scope.isConfirmBtnDisabled = false; //identify the confirm btn's disable and enable.

  $scope.showInventoryCheck = false;

  $scope.form = {
    MobileCode: '+852',
    LanguageId: 1,
    TimeZoneId: 1,
    UserInventoryLocationArray : []
  };

  if($scope.pageSide === "merchant"){
    //todo: should be renamed to a common style, e.g. merchantId
    var hpromise = Http.get($rootScope.servicePrefix+'/inventory/location/list', { params: { merchantid : $scope.MerchantId || $scope.merchantId, cc : $rootScope.language } });
    hpromise.then(function(resultObj){
      $scope.inventoryLocations = resultObj.data;
    });
  }

  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts = [];
  };

  // $scope.confirm = function () {
  //   $modalInstance.close('Some state');
  // };

  $scope.cancel = function () {
    if($scope.isCreated){
      $scope.getData();
      $scope.showMessage({
        msg: $scope.t.MSG_USER_CREATED,
        users: angular.copy($scope.displayUserNames),
        type: 'success'
      });
    }
    $modalInstance.dismiss('cancel');
  };

  $scope.changeMobileCode = function(code) {
    $scope.form.MobileCode = code;
  };

  $scope.createUser = function(validationForm) {
    validationForm.$submitted = true;
    
    //angular form validation.
    if (validationForm.$valid && roleValidation() && inventoryLocationValidation()) {
      //set confirm btn disabled
      $scope.isConfirmBtnDisabled = true;
      var isCreateAnother = $scope.form.IsCreateAnother;//true - create another new; false - just close the dialog
      // set user type ID
      $scope.form.UserTypeId = 1;  // Admin
      $scope.form.MerchantId = 0;  // Admin

      if($scope.pageSide === "merchant"){
        //todo: should be renamed to a common style, e.g. merchantId
        $scope.form.MerchantId = $scope.MerchantId || $scope.merchantId || 0;
        $scope.form.UserTypeId = 2;
      }
      
      var hpromise = Http.post($rootScope.servicePrefix+'/user/save', $scope.form);
      hpromise.then(function(response) {
        $scope.isCreated = true; //if create user successfully, after closing the diolag, it will show the alert.
        // $scope.Error = false;
        // $scope.Msg = data;
        var user = response.data.User;
        $scope.displayUserNames.push(user);//push new user object(response from server side) into users array which would show in success alert.
        if(!isCreateAnother){
          $scope.getData();
          $modalInstance.close('Some state');
          // $scope.showModal = false;

          $scope.showMessage({
            msg: $scope.t.MSG_USER_CREATED,
            users: angular.copy($scope.displayUserNames),
            type: 'success'
          });

          $scope.displayUserNames = [];//clear the users array;
        }else{
          //reset the form
          if (validationForm) {
            validationForm.$setPristine();
            validationForm.$setUntouched();
            $scope.form = {
              MobileCode: '+852',
              LanguageId: 1,
              TimeZoneId: 1,
              IsCreateAnother: true
            };
          }
          $scope.closeAlert();
        }
        //set confirm btn enabled
        $scope.isConfirmBtnDisabled = false;
      }, function(errObj, status, headers, config) {
        var errorCodeMsg = ($scope.t[errObj.data.AppCode]===undefined)?(errObj.data.AppCode):($scope.t[errObj.data.AppCode]);
        $scope.alerts = [{
          msg: errorCodeMsg,
          type: 'danger'
        }];
        //set confirm btn enabled
        $scope.isConfirmBtnDisabled = false;
      });

    }

    /**
      @Desc role checkbox validation(at least choose one role)
      @Return {boolean} true-valid; false-invalid
    **/
    function roleValidation() {
      if ($scope.form.UserSecurityGroupArray === undefined || $scope.form.UserSecurityGroupArray.length === 0) {
        $scope.isShowSecurityGroupError = true;
        return false;
      } else {
        $scope.isShowSecurityGroupError = false;
        return true;
      }
    }

    function inventoryLocationValidation(){
//      var id1 = _.result(_.find($scope.generalTranslationService.UserReference.SecurityGroupList, { SecurityGroupNameInvariant : "Merchant Order Processsing" }), 'SecurityGroupId');
//      var id2 = _.result(_.find($scope.generalTranslationService.UserReference.SecurityGroupList, { SecurityGroupNameInvariant : "Merchant Product Return" }), 'SecurityGroupId');

      if($scope.form.UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ORDER_PROCESSSING) >= 0 || 
         $scope.form.UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_PRODUCT_RETURN) >= 0){
        if ($scope.form.UserInventoryLocationArray === undefined || $scope.form.UserInventoryLocationArray.length === 0) {
          $scope.isShowInventoryLocationError = true;
          return false;
        } else {
          $scope.isShowInventoryLocationError = false;
          return true;
        }
      }else{
        return true;
      }
    }
  };

  $scope.checkSecurity = function(){
      
//    var id1 = _.result(_.find($scope.generalTranslationService.UserReference.SecurityGroupList, { SecurityGroupNameInvariant : "Merchant Order Processsing" }), 'SecurityGroupId');
//    var id2 = _.result(_.find($scope.generalTranslationService.UserReference.SecurityGroupList, { SecurityGroupNameInvariant : "Merchant Product Return" }), 'SecurityGroupId');
    
    if($scope.form.UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ORDER_PROCESSSING) >= 0 || 
       $scope.form.UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_PRODUCT_RETURN) >= 0){
      $scope.showInventoryCheck = true;
    }else{
      $scope.showInventoryCheck = false;
    }
  };

  $scope.$watch('form.UserName', function(newVal, oldVal, scope){
    if(!scope.form.DisplayName || scope.form.DisplayName===oldVal){
      scope.form.DisplayName = scope.form.UserName;
    }
  });
}
