'use strict';

function BaseInventoryLocationCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, 
	Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService, InventoryService) {
            
	var CONSTANTS = setScopeConstants();
    
        function setScopeConstants() {
            var CONSTANTS = $rootScope.CONSTANTS;
            
            CONSTANTS.CITY={
                ALL : '-1'
            };
            
            CONSTANTS.PROVINCE={
                ALL : '-1'
            };
            
            CONSTANTS.COUNTRY={
                ALL : '-1'
            };
            
            CONSTANTS.INVENTORY_LOCATION_TYPE.ALL = '-1';
            
            // Sync back to $scope
            $scope.CONSTANTS = CONSTANTS;

            return CONSTANTS;
        }
    
	$rootScope.settings = $state.current.settings;
	$scope.mainService = mainService;
	$scope.t = mainService.UserReference.TranslationMap;
  $scope.selectT = generalTranslationService.UserReference;

  var parentScope = $scope;
  parentScope.MerchantId = parentScope.MerchantId || $rootScope.User.MerchantId;
  // Table Filters Definitions
//  $scope.types = $scope.selectT["InventoryLocationTypeList"];
  $scope.countries = geoService.countries;
  $scope.provinces = [];
  $scope.cities = [];      
  $scope.filter = {};

  // Scopes for Getting Table Data's..
  //var tmpAllData = [];
  $scope.allData = [];
  $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
  $scope.sortBy = 'LocationName';
  $scope.reverse = false;
  //$scope.filter = "";

  //messages
  if ($state.params.msg && $state.params.type) {
    $scope.messages = [{
      msg: $scope.t[$state.params.msg] || $state.params.msg,
      type: $state.params.type
    }];
  }

  $scope.inventoryLocationActionsByStatus = {
    'Active': [
      {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
      {lable: $scope.t.LB_EDIT_PRODUCT_INVENTORY, value:'EditInventory'},
      {lable: $scope.t.LB_INACTIVATE_INVENTORY_LOC, value:'Inactivate'},
      {lable: $scope.t.LB_DELETE_INVENTORY_LOC, value:'Delete'}
    ],
    'Pending': [
      {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
      {lable: $scope.t.LB_EDIT_PRODUCT_INVENTORY, value:'EditInventory'},
      {lable: $scope.t.LB_ACTIVATE_INVENTORY_LOC, value:'Activate'},
      {lable: $scope.t.LB_DELETE_INVENTORY_LOC, value:'Delete'}
    ],
    'Inactive': [
      {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
      {lable: $scope.t.LB_EDIT_PRODUCT_INVENTORY, value:'EditInventory'},
      {lable: $scope.t.LB_ACTIVATE_INVENTORY_LOC, value:'Activate'},
      {lable: $scope.t.LB_DELETE_INVENTORY_LOC, value:'Delete'}
    ]
  };

  $scope.countrySort = InventoryService.countrySort;

  $scope.updateData = function(data, update){
    for(var i = 0; i < $scope.allData.length; i++){
      if( $scope.allData[i]['$$hashKey'] === data['$$hashKey'] ){
        update.rowActions = "None";
        $scope.allData[i] = update;
      }
    }
  };

  $scope.onClick = function(item){
    $state.go('editInventoryLocation', { inventorylocationid: item.InventoryLocationId, merchantId: $scope.MerchantId } );
  };

  $scope.selectAction = function(id, action, item){
    if(action){
      switch (action) {
        case 'Edit':
          $state.go('editInventoryLocation', { inventorylocationid: item.InventoryLocationId, merchantId: $scope.MerchantId } );
          // $scope.CreateNewInventoryLocation(JSON.parse( JSON.stringify(item) ));
          break;
        case 'EditInventory':
          $state.go('inventoryProduct', { locationId: item.InventoryLocationId, merchantId: $scope.MerchantId  });
          break;
        case 'Inactivate':
          $scope.changeStatus(item);
          break;
        case 'Activate':
          $scope.changeStatus(item);
          break;
        case 'Delete':
          $scope.deleteLocation(item);
          break;
      }
      item.rowActions = "None";
    }
  };

  $scope.changeStatus = function(item){
    var modalInstance = $modal.open({
          templateUrl: "/shared/views/change-status-location-dialog.html",
          backdrop : 'static',
          resolve: {
            mainService: function() {
              return mainService;
            }
          },
          controller: ['$scope', 'mainService', function($scope, mainService) {
            $scope.item = item;
            //global util module outside of angular.
            // Translation data
            $scope.t = mainService.UserReference.TranslationMap;
            $scope.cancel = function() {
              $scope.$dismiss();
            };

            $scope.confirm = function() {
              var newStatus;
              if (item.StatusNameInvariant === 'Active') {
                newStatus = 'Inactive';
              }
              else if (item.StatusNameInvariant === 'Inactive' || item.StatusNameInvariant === 'Pending') {
                newStatus = 'Active';
              }

              //set confirm btn disabled;
              $scope.isConfirmBtnDisabled=true;

              var hpromise = $http.post($rootScope.servicePrefix+'/inventory/location/status/change', {
                "InventoryLocationId": item.InventoryLocationId,
                'Status': newStatus,
                MerchantId: parentScope.MerchantId
              });

              hpromise.then(function(result) {
                item.StatusNameInvariant = newStatus;
                item.StatusId = CONSTANTS.STATUS[newStatus.toUpperCase()];
                $scope.$close(item);
              }, function(errObj, status, headers, config) {
                $scope.messages = [
                  {msg: 'Failed to change status. Please try again later.', type: 'danger'}
                ];
              }).finally(function(){
                //set confirm btn enabled;
                $scope.isConfirmBtnDisabled=false;
              });
            };

            $scope.closeMessage = function(index) {
              $scope.messages.splice(index, 1);
            };
          }]
      });

    modalInstance.result.then(function(data) {
      // Update the affected row's Status field
      $scope.updateData(item, data);
    });
  };

  $scope.deleteLocation = function(item){
    var modalInstance = $modal.open({
          templateUrl: "/shared/views/delete-location-dialog.html",
          backdrop : 'static',
          resolve: {
            mainService: function() {
              return mainService;
            }
          },
          controller: ['$scope', 'mainService', function($scope, mainService) {
            $scope.item = item;
            //global util module outside of angular.
            // Translation data
            $scope.t = mainService.UserReference.TranslationMap;
            $scope.cancel = function() {
              $scope.$dismiss();
            };

            $scope.confirm = function() {
              $scope.isConfirmBtnDisabled = true;
              var hpromise = $http.post($rootScope.servicePrefix+'/inventory/location/status/change', {
                "InventoryLocationId": item.InventoryLocationId,
                'Status': 'Deleted',
                MerchantId: parentScope.MerchantId
              });

              hpromise.then(function(result) {
                $scope.$close(true);
              }, function(errObj, status, headers, config) {
                $scope.messages = [
                  {msg: $scope.t[errObj.data.AppCode] || 'Failed to delete location. Please try again later.', type: 'danger'}
                ];
              }).finally(function(){
                //set confirm btn enabled
                $scope.isConfirmBtnDisabled = false;
              });
            };

            $scope.closeMessage = function(index) {
              $scope.messages.splice(index, 1);
            };
          }]
      });

    modalInstance.result.then(function(data){
      $scope.getData();
    });
  };

  $scope.filterBySearch = function(item){
    var lowercaseTerm = $scope.inventorySearchTerm ? $scope.inventorySearchTerm.toLowerCase() : '';

    if (!item) {
      return false;
    }

    if (item.LocationName && item.LocationName.toLowerCase().indexOf(lowercaseTerm) > -1) {
      return true;
    }
    if (item.LocationExternalCode && item.LocationExternalCode.toLowerCase().indexOf(lowercaseTerm) > -1) {
      return true;
    }
    return false;
  };

  $scope.filterByTypes = function(item){
    var value = parseInt($scope.filter.type);
    if(value < 0){ return true; }

    if (value == item.InventoryLocationTypeId){
      return true;
    }
  };

  $scope.filterByCountry = function(item){
    var value = parseInt($scope.filter.country);

    if(value < 0){ return true; }
    if (value == item.GeoCountryId){
      return true;
    }

    return false;
  };

  $scope.filterByProvince = function(item){
    var value = parseInt($scope.filter.province);

    if(value < 0){ return true; }

    if (value == item.GeoIdProvince){
      return true;
    }

    return false;
  };

  $scope.filterByCity = function(item){
    var value = parseInt($scope.filter.city);
    
    if(value < 0){ return true; }

    if (value == item.GeoIdCity){
      return true;
    }

    return false;
  };

  $scope.changeCountry = function (value) {
    $scope.filter.province = CONSTANTS.PROVINCE.ALL;
    $scope.provinces = [];
    $scope.filter.city = CONSTANTS.CITY.ALL;
    $scope.cities = [];
    value = parseInt(value);

    if(value < 0){ return true; }

    geoService.getProvince(value).then(function(data){
      $scope.provinces = data;
    });
  };

  $scope.changeProvince = function (value) {
    $scope.filter.city = CONSTANTS.CITY.ALL;
    $scope.cities = [];
    value = parseInt(value);

    if(value < 0){ return true; }

    geoService.getCities(value).then(function(data){
      $scope.cities = data;
    });
  };


  $scope.getStatusDisplayName = function(StatusNameInvariant){
    var displayName;
    switch (StatusNameInvariant) {
      case 'Active':
        displayName = $scope.t.LB_ACTIVE;
        break;
      case 'Inactive':
        displayName = $scope.t.LB_INACTIVE;
        break;
      case 'Pending':
        displayName = $scope.t.LB_PENDING;
        break;
      case 'Deleted':
        displayName = $scope.t.LB_DELETE_USER;
        break;
    }
    return displayName;
  };

  $scope.getData = function(){
    $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
    $scope.allData = [];
    var merchantid = $rootScope.User.MerchantId;
    if($scope.pageSide == 'admin'){ merchantid = $scope.MerchantId; }

    var hpromise = $http.get($rootScope.servicePrefix+'/inventory/location/list', { params: { merchantid : merchantid, cc : $rootScope.language } });
    hpromise.then(function(resultObj){
        
      var data = _.map(resultObj.data, function(row){
        row.LastStatus = Util.formatDate(row.LastStatus);
        row.LastModified = Util.formatDate(row.LastModified);
//        row.InventoryLocationTypeName = _.result(_.find($scope.types, { InventoryLocationTypeId : row.InventoryLocationTypeId }), 'InventoryLocationTypeName');
        row.InventoryLocationTypeName = CONSTANTS.INVENTORY_LOCATION_TYPE_NAME[row.InventoryLocationTypeId];
        return row;
      });

      $scope.allData = data;
    });
  };

  $scope.sortTable = function(sortBy){
    if($scope.sortBy === sortBy){
      $scope.reverse = $scope.reverse === false ? true: false;
    }else{
      $scope.reverse = false;
    }
    $scope.sortBy = sortBy;
    $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
  };

  $scope.getData();


  // Scopes for Creating New Location...
  $scope.CreateNewInventoryLocation = function() {
    $state.go('createInventoryLocation', {merchantId: $scope.MerchantId});
  };
}
