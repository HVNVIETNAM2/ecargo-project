'use strict';

function BaseUserEditCtrl($rootScope, $scope, $location, $window, $modal,
  $log, $http, Upload, mainService, translationService, $state, $stateParams, $anchorScroll, FileUploadService, generalTranslationService) {
      
    var CONSTANTS = setScopeConstants();

    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }

  // Set some variables in $rootScope to hide the side/top menu based on state
  $rootScope.settings = $state.current.settings;

  $scope.showInventoryCheck = false;

  $scope.mainService = mainService;
  $scope.generalTranslationService = generalTranslationService;
  $scope.t = mainService.UserReference.TranslationMap;

  $scope.form = {
    UserInventoryLocationArray : []
  };
  
  $scope.role_checker = false;
  
  //checking if user admin...
  var u_data = sessionStorage.getItem('UserData');
      u_data = JSON.parse(u_data);
  var a_data = u_data.UserSecurityGroupArray;

  /**
   * @description check if current login user is an admin
   * @type {boolean}
   * */
  $scope.isAdmin = $rootScope.isAdmin() || $rootScope.isMerchantAdmin();

  if (document.location.pathname === "/admin/") {
      if (a_data.indexOf(CONSTANTS.USER_SECURITY_GROUP.MM_ADMIN) > -1) {
        $scope.role_checker = true;
      }
  }else{
      $scope.role_checker = true;
  }

  //MM-2080 User should not edit his/her own role
  if ($stateParams.userKey === u_data.UserKey) {
    $scope.role_checker = false;
  }

  $scope.SecurityGroupList = [];
  $scope.StatusList = [];

  $scope.cancel = function() {
        if ($state.previous.name) {
            $state.go($state.previous.name, $state.previous.params);
        } else {
            $state.go('home');
        }
//    if ($scope.merchantId) {
//      $state.go('merchantUserListing', {merchantId: $scope.merchantId})
//    } else {
//      $state.go('user', {merchantId: $scope.merchantId})
//    }
  };

  generalTranslationService.UserReference.StatusList.forEach(function(item){
    if(item.StatusId !== CONSTANTS.STATUS.DELETED){
      $scope.StatusList.push(item);
    }
  });
  if($scope.pageSide === "merchant"){


    generalTranslationService.UserReference.SecurityGroupList.forEach(function(item){
      if(item.UserTypeId === CONSTANTS.USER_TYPE.MERCHANT){
        $scope.SecurityGroupList.push(item);
      }
    });
  }

  if($scope.pageSide === "admin"){
    generalTranslationService.UserReference.SecurityGroupList.forEach(function(item){
      if(item.UserTypeId === CONSTANTS.USER_TYPE.MM){
        $scope.SecurityGroupList.push(item);
      }
    });
  }

  $scope.FileUploadService = FileUploadService;

  //global util module outside of angular.

  $scope.messages = [];

  $scope.closeMessage = function(index) {
    $scope.messages.splice(index, 1);
  };

  $scope.openChangeStatusDialog = function(user) {
    var modalInstance = $modal.open({
        templateUrl: "/shared/views/change-status-dialog.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          },
          translationService: function() {
            return translationService;
          }
        },
        controller: ['$scope', 'mainService', 'translationService',
          function($scope, mainService, translationService) {
            $scope.user = user;
            //global util module outside of angular.
            // Translation data
            $scope.t = translationService.TranslationMap;
            $scope.cancel = function() {
              /*
              // User cancelled the change, revert the switch control's value. Use the intermediate value to
              // avoid infinite watch.
              $scope.form.StatusSwitchValue = undefined;
              $scope.form.StatusSwitchValue = oldStatus;
              */
              $scope.$dismiss();
            };

            $scope.confirm = function() {
              var newStatus;
              if (user.StatusNameInvariant === 'Active') {
                newStatus = 'Inactive';
              }
              else if (user.StatusNameInvariant === 'Inactive') {
                newStatus = 'Active';
              }
              //set confirm btn disabled
              $scope.isConfirmBtnDisabled = true;
              var hpromise = $http.post($rootScope.servicePrefix+'/user/status/change', {
                "UserKey": user.UserKey,
                'Status': newStatus,
                MerchantId: user.MerchantId
              });
              hpromise.then(function(result) {
                $scope.$close(newStatus);
              }, function(errObj, status, headers, config) {
                $scope.messages = [{
                  msg: $scope.t.MSG_ERR_CHANGE_STATUS_FAIL + errObj.data.AppCode,
                  type: 'danger'
                }];
              }).finally(function(){
                //set confirm btn enabled
                $scope.isConfirmBtnDisabled = false;
              });
            };

            $scope.closeMessage = function(index) {
              $scope.messages.splice(index, 1);
            };
          }]
    });
    modalInstance.result.then(function(newStatus) {
      $scope.form.StatusNameInvariant = newStatus;
      if (newStatus === 'Active') {
        $scope.form.StatusId = CONSTANTS.STATUS.ACTIVE;
      }else if (newStatus === 'Inactive') {
        $scope.form.StatusId = CONSTANTS.STATUS.INACTIVE;
      }
    });
  };

  $scope.openResetPasswordDialog = function(user) {
    $modal.open({
        templateUrl: "/shared/views/reset-password-dialog.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          },
          translationService: function() {
            return translationService;
          }
        },
        controller: ['$scope', 'mainService', 'translationService',
          function($scope, mainService, translationService) {
            $scope.user = user;
            //global util module outside of angular.
            // Translation data
            $scope.t = translationService.TranslationMap;
            $scope.cancel = function() {
              $scope.$dismiss();
            };

            $scope.confirm = function() {
              //set confirm btn disabled
              $scope.isConfirmBtnDisabled = true;
              var hpromise = $http.post($rootScope.servicePrefix+'/user/password/change', {
                "UserKey": user.UserKey,
                MerchantId: user.MerchantId
              });
              hpromise.then(function(result) {
                //should get the result from server with updated server object
                user.StatusId = CONSTANTS.STATUS.PENDING;
                $scope.$close(true);
              }, function(errObj, status, headers, config) {
                $scope.messages = [
                  {
                    msg: $scope.t.MSG_ERR_RESET_PASSWORD_FAIL+errObj.data.AppCode,
                    type: 'danger'
                  }
                ];
              }).finally(function(){
                //set confirm btn enabled
                $scope.isConfirmBtnDisabled = false;
              });;
            };

            $scope.closeMessage = function(index) {
              $scope.messages.splice(index, 1);
            };
          }]
    })/*.result.finally(function() {
        $state.go('userEdit');
    });*/
  };

  $scope.openChangeEmailDialog = function(user) {
    var modalInstance = $modal.open({
        templateUrl: "/shared/views/change-email-dialog.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          },
          translationService: function() {
            return translationService;
          }
        },
        controller: ['$scope', 'mainService', 'translationService',
          function($scope, mainService, translationService) {
            $scope.user = user;
            //global util module outside of angular.
            // Translation data
            $scope.t = translationService.TranslationMap;
            $scope.cancel = function() {
              $scope.$dismiss();
            };

            $scope.confirm = function(validationForm) {
              validationForm.$submitted = true;

              if (validationForm.$valid && isEmailChanged()) {
                // Check if emails match. Should implement this using directive or 3rd-party plugin
                if ($scope.dialogForm.email !== $scope.dialogForm.confirmEmail) {
                  return;
                }
                //set confirm btn disabled
                $scope.isConfirmBtnDisabled = true;

                var hpromise = $http.post($rootScope.servicePrefix+'/user/email/change', {
                  "UserKey": user.UserKey,
                  "Email": $scope.dialogForm.confirmEmail,
                  "MerchantId": user.MerchantId
                });
                hpromise.then(function(result) {
                  //should get the result from server with updated server object
                  user.StatusId = CONSTANTS.STATUS.PENDING;
                  $scope.$close($scope.dialogForm.confirmEmail);
                }, function(errObj, status, headers, config) {
                  var errorCodeMsg = ($scope.t[errObj.data.AppCode]===undefined)?(errObj.data.AppCode):($scope.t[errObj.data.AppCode]);
                  $scope.messages = [{
                      msg: errorCodeMsg,
                      type: 'danger'
                  }];
                }).finally(function(){
                  //set confirm btn enabled
                  $scope.isConfirmBtnDisabled = false;
                });
              }

              //check if email is the same as original one, if it is return false; then return true;
              function isEmailChanged(){
                if($scope.dialogForm.confirmEmail === user.Email){
                  $scope.messages = [
                    {
                      msg: $scope.t.MSG_ERR_EMAIL_NO_CHANGE,
                      type: 'danger'
                    }
                  ];
                  return false;
                }else{
                  return true;
                }
              }
            };

            $scope.closeMessage = function(index) {
              $scope.messages.splice(index, 1);
            };
          }]
    });
    modalInstance.result.then(function(newEmail) {
      // Update the email binding in Edit User form.
      $scope.form.Email = newEmail;
    });
  };

  $scope.openChangeMobileDialog = function(user) {
    var mainScope = $scope;
    var modalInstance = $modal.open({
        templateUrl: "/shared/views/change-mobile-dialog.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          },
          translationService: function() {
            return translationService;
          },
          generalTranslationService : function(GeneralTranslationService){
            return GeneralTranslationService.initialize();
          },
        },
        controller: ['$scope', 'mainService', 'translationService', 'generalTranslationService', '$state', '$rootScope',
          function($scope, mainService, translationService, generalTranslationService, $state, $rootScope) {
            $scope.user = user;
            //global util module outside of angular.
            // Translation data
            $scope.t = translationService.TranslationMap;
            $scope.generalTranslationService = generalTranslationService;

            $scope.dialogForm = {
              MobileCode : $scope.user.MobileCode
            };

            var isMobile = false;
//            var id1 = _.result(_.find(mainScope.generalTranslationService.UserReference.SecurityGroupList, { SecurityGroupNameInvariant : "Merchant Order Processsing" }), 'SecurityGroupId');
//            var id2 = _.result(_.find(mainScope.generalTranslationService.UserReference.SecurityGroupList, { SecurityGroupNameInvariant : "Merchant Product Return" }), 'SecurityGroupId');
            
            if($scope.user.UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ORDER_PROCESSSING) >= 0 || 
               $scope.user.UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_PRODUCT_RETURN) >= 0){
              $scope.isMobile = true;
            }else{
              $scope.isMobile = false;
            }

            $scope.changeMobileCode = function(code) {
              $scope.dialogForm.MobileCode = code;
            };

            $scope.cancel = function() {
              $scope.$dismiss();
            };

            $scope.confirm = function(validationForm) {
              validationForm.$submitted = true;

              if (validationForm.$valid && isMobileChanged()) {
                // Check if emails match. Should implement this using directive or 3rd-party plugin
                if ($scope.dialogForm.MobileNumber !== $scope.dialogForm.ConfirmMobileNumber) {
                  return;
                }

                //set confirm btn disabled
                $scope.isConfirmBtnDisabled = true;

                var url = $rootScope.servicePrefix + "/user/mobile/change/immediate";
                if(isMobile){ url = $rootScope.servicePrefix+"/user/mobile/change"; }

                var hpromise = $http.post(url, {
                  "UserKey": $scope.user.UserKey,
                  "MobileCode": $scope.dialogForm.MobileCode,
                  "MobileNumber": $scope.dialogForm.MobileNumber,
                  "MerchantId": $scope.user.MerchantId
                });

                hpromise.then(function(result) {
                  if(isMobile){
                    if($scope.user.UserKey === $rootScope.User.UserKey){
                      $state.go('passiveLogout');
                      return true;
                    }
                  }else{
                    mainScope.messages = [{
                      msg: $scope.t.MSG_MOBILE_CHANGE_SUCCESS + " " + $scope.t.MSG_MOBILE_SENT_SMS,
                      type: 'success'
                    }];

                    $rootScope.User.MobileCode = $scope.dialogForm.MobileCode;
                    $rootScope.User.MobileNumber = $scope.dialogForm.MobileNumber;
                  }

                  //should get the result from server with updated server object
                  user.StatusId = CONSTANTS.STATUS.PENDING;

                  $scope.$close($scope.dialogForm);
                }, function(errObj, status, headers, config) {
                  var errorCodeMsg = ($scope.t[errObj.data.AppCode]===undefined)?(errObj.data.AppCode):($scope.t[errObj.data.AppCode]);
                  $scope.messages = [{
                      msg: errorCodeMsg,
                      type: 'danger'
                  }];
                }).finally(function(){
                  //set confirm btn enabled
                  $scope.isConfirmBtnDisabled = false;
                });
              }

              //check if email is the same as original one, if it is return false; then return true;
              function isMobileChanged(){
                if( ($scope.dialogForm.MobileCode + $scope.dialogForm.ConfirmMobileNumber) === (user.MobileCode + user.MobileNumber)){
                  $scope.messages = [
                    {
                      msg: $scope.t.MSG_ERR_MOBILE_NO_CHANGE,
                      type: 'danger'
                    }
                  ];
                  return false;
                }else{
                  return true;
                }
              }
            };

            $scope.closeMessage = function(index) {
              $scope.messages.splice(index, 1);
            };
          }]
    });
    modalInstance.result.then(function(newMobile) {
      // Update the email binding in Edit User form.
      //$scope.form.Email = newEmail;
      $scope.form.MobileCode = newMobile.MobileCode;
      $scope.form.MobileNumber = newMobile.MobileNumber;
    });
  };

  $scope.saveUser = function(validationForm) {
    validationForm.$submitted = true;

    if (validationForm.$valid && roleValidation() && inventoryLocationValidation()) {
      //set save btn disabled
      $scope.isSaveBtnDisabled = true;

//      $scope.form.MerchantId = $scope.form.MerchantId || $scope.merchantId;
      var hpromise = $http.post($rootScope.servicePrefix+'/user/save', $scope.form);
      hpromise.then(function(result) {

        //updated by gerald
        var changedLanguage = false;
        var CurrentLanguageId = $rootScope.User;
        if(CurrentLanguageId !== null){
//          CurrentLanguageId = CurrentLanguageId;
//          CurrentLanguageId = CurrentLanguageId.LanguageId;
          if(CurrentLanguageId.LanguageId !== result.data.LanguageId){
            changedLanguage = true;
          }
        }

        var User = result.data;
        var langId = parseInt(result.data.LanguageId);
        // Commented as not being used in the code
//        var langCC = "EN";
//        generalTranslationService.UserReference.LanguageList.forEach(function(l){
//          if(l.LanguageId === langId){
//            langCC = l.LanguageNameInvariant;
//          }
//        });
        User.LanguageId = langId;
        $scope.firstName = User.FirstName;
        $scope.lastName = User.LastName;

        $scope.messages = [{
          msg:  $scope.t.MSG_USER_EDITED,
          type: 'success'
        }];


        // update profile if self
        if ($rootScope.User.UserKey === User.UserKey) {
          $rootScope.saveUserData(User, true);

          //to refetch the new language after change
          if(changedLanguage){
            window.location.reload();
          }
        }

      }, function(errObj, status, headers, config) {
        var errorCodeMsg = ($scope.t[errObj.data.AppCode]===undefined)?(errObj.data.AppCode):($scope.t[errObj.data.AppCode]);
        $scope.messages = [{
          msg: errorCodeMsg,
          type: 'danger'
        }];
      }).finally(function(){
        //set save btn enabled
        $scope.isSaveBtnDisabled = false;
        $location.hash('page_top');
        // call $anchorScroll()
        $anchorScroll();
      });
    }

    /**
      @Desc role checkbox validation(at least choose one role)
      @Return {boolean} true-valid; false-invalid
    **/
    function roleValidation(){
      if($scope.form.UserSecurityGroupArray===undefined||$scope.form.UserSecurityGroupArray.length===0){
        $scope.isShowSecurityGroupError = true;
        return false;
      }else{
        $scope.isShowSecurityGroupError = false;
        return true;
      }
    }

    function inventoryLocationValidation(){
//      var id1 = _.result(_.find($scope.generalTranslationService.UserReference.SecurityGroupList, { SecurityGroupNameInvariant : "Merchant Order Processsing" }), 'SecurityGroupId');
//      var id2 = _.result(_.find($scope.generalTranslationService.UserReference.SecurityGroupList, { SecurityGroupNameInvariant : "Merchant Product Return" }), 'SecurityGroupId');

      if($scope.form.UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ORDER_PROCESSSING) >= 0 ||
         $scope.form.UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_PRODUCT_RETURN) >= 0){
        if ($scope.form.UserInventoryLocationArray === undefined || $scope.form.UserInventoryLocationArray.length <= 0) {
          $scope.isShowInventoryLocationError = true;
          return false;
        } else {
          $scope.isShowInventoryLocationError = false;
          return true;
        }
      }else{
        return true;
      }
    }
  };

  $scope.checkSecurity = function(){
//    var id1 = _.result(_.find($scope.generalTranslationService.UserReference.SecurityGroupList, { SecurityGroupNameInvariant : "Merchant Order Processsing" }), 'SecurityGroupId');
//    var id2 = _.result(_.find($scope.generalTranslationService.UserReference.SecurityGroupList, { SecurityGroupNameInvariant : "Merchant Product Return" }), 'SecurityGroupId');
    if($scope.form.UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ORDER_PROCESSSING) >= 0 || 
       $scope.form.UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_PRODUCT_RETURN) >= 0){
      $scope.showInventoryCheck = true;
    }else{
      $scope.showInventoryCheck = false;
    }
  };

}
