'use strict';

function BaseInventoryProductListCtrl($rootScope, $scope, $state, $modal, $http, $loading, RequestService, InventoryService, mainService) {

    var CONSTANTS = setScopeConstants();

    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        CONSTANTS.ROWACTION = {
            EDIT: '1',
            CREATE: '2'
        };

        CONSTANTS.INVENTORY_FILTER = {
            ALL: '-1',
            WITH_INVENTORY: '1',
            WITHOUT_INVENTORY: '0'
        };

        CONSTANTS.INVENTORY_STATUS.ALL = '-1';

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }

    $scope.merchantId = $scope.merchantId || $rootScope.User.MerchantId;

    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
        $scope.resetPage();
    };

    function setBreadcrumb() {
        if ($scope.pageSide === 'admin') {
            $scope.breadcrumbs.push({
                    text: mainService.UserReference.TranslationMap.LB_INVENTORY_LOCATION,
                    link: 'inventoryLocation({merchantId: "' + $scope.merchantId + '"})'
                },
                {
                    text: $scope.locationName
                });
        } else {
            $scope.breadcrumbs = [
                {text: mainService.UserReference.TranslationMap.LB_MERCHANT, link: "home", icon: "icon-home"},
                {
                    text: mainService.UserReference.TranslationMap.LB_INVENTORY_LOCATION,
                    link: 'inventoryLocation'
                },
                {
                    text: $scope.locationName
                }
            ];
        }
    }

    $scope.resetPage = function () {
        $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
        $scope.getSkuList();
    };

//    $scope.searchText = function (product) {
//        if (!$scope.search) {
//            return true;
//        }
//
//        var search = $scope.search.toLowerCase();
//
//        //sku, barcode, product name, color name, size
//        return _.trim(product.SkuName).toLowerCase().indexOf(search) !== -1 ||
//            _.trim(product.Bar).toLowerCase().indexOf(search) !== -1 ||
//            _.trim(product.SkuCode).toLowerCase().indexOf(search) !== -1 ||
//            _.trim(product.SkuColor).toLowerCase().indexOf(search) !== -1 ||
//            _.trim(product.SizeName).toLowerCase().indexOf(search) !== -1;
//    };

//    $scope.filterInventory = function (product) {
//        var filterCriteria = $scope.criteria.inventory;
//        var isPassed = false;
//        switch (filterCriteria) {
//            case CONSTANTS.INVENTORY_FILTER.ALL:
//                isPassed = true;
//                break;
//            case CONSTANTS.INVENTORY_FILTER.WITH_INVENTORY:
//                if (product.InventoryLocationId > 0) {
//                    isPassed = true;
//                }
//                break;
//            case CONSTANTS.INVENTORY_FILTER.WITHOUT_INVENTORY:
//                if (!product.InventoryLocationId) {
//                    isPassed = true;
//                }
//                break;
//        }
//        return isPassed;
//    };
//
//    $scope.filterStatus = function (product) {
//        var isPassed = false;
//        var filterStatusId = parseInt($scope.criteria.statusId);
//        var productStatusId = parseInt(product.InventoryStatusId);
//
//        switch (filterStatusId) {
//            case CONSTANTS.INVENTORY_STATUS.IN_STOCK:
//            case CONSTANTS.INVENTORY_STATUS.LOW_STOCK:
//            case CONSTANTS.INVENTORY_STATUS.OUT_OF_STOCK:
//                if (product.InventoryId && filterStatusId === productStatusId) {
//                    isPassed = true;
//                }
//                break;
//            case CONSTANTS.INVENTORY_STATUS.NA:
//                if (product.InventoryId === null) {
//                    isPassed = true;
//                }
//                break;
//            default:
//                isPassed = true;
//                break;
//
//        }
//        return isPassed;
//    };

    $scope.$watch('locationName', function (newValue, oldValue) {
        if (newValue !== oldValue) {
            // Set breadcrumb
            setBreadcrumb();
        }
    });

    $scope.getSkuList = function () {
        $loading.start('loading');
        var params = {
            cc: $rootScope.User.CultureCode, //culture code
            inventorylocationid: $scope.locationId, //location id
            size: $scope.itemsPerPage,
            page: $scope.currentPage,
            predicate: $scope.predicate,
            order: $scope.reverse?'DESC':'ASC',
            hasInventory: $scope.criteria.inventory,
            inventoryStatusId: $scope.criteria.statusId,
            search: $scope.search,
            merchantid: $scope.merchantId
        };
        
        var hpromise = $http.get($rootScope.servicePrefix + '/inventory/list/location', {
            params: params
        });

        $scope.products = [];
        hpromise.then(function (result) {
            // Inventory Location
            $scope.locationName = result.data.InventoryLocation.LocationName;
            // Product list
            $scope.products = result.data.SkuList.PageData;
            $scope.total = result.data.SkuList.HitsTotal;
            $loading.finish('loading');
        }, function (errObj, status, headers, config) {
            if (window.console) {
                console.log(errObj, "Failed to retrieve product list. Please try again later.");
            }
            $scope.messages = [{
                msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
                type: 'danger'
            }];
            $loading.finish('loading');
        });
    };

    $scope.selectAction = function (item) {
        var action = item.rowAction;
        switch (action) {
            //edit
            case CONSTANTS.ROWACTION.EDIT:
                InventoryService.editInventory({
                    SkuId: item.SkuId,
                    InventoryLocationId: $state.params.locationId,
                    InventoryId: item.InventoryId
                }, true);
                break;
            //create
            case CONSTANTS.ROWACTION.CREATE:
                InventoryService.createInventory({
                    SkuId: item.SkuId,
                    InventoryLocationId: $state.params.locationId,
                    InventoryId: item.InventoryId
                }, true);
                break;
            //delete
            case 3:
                $scope.openDeleteDialog(item);
                break;
        }
        item.rowAction = '';
    };

    $scope.goImportDialog = function () {
        $state.go('inventoryImport', {locationId: $state.params.locationId, merchantId: $scope.merchantId});
    };

    $scope.openExportDialog = function () {
        return $modal.open({
            templateUrl: '/shared/views/confirm-export-inventory-dialog.html',
            backdrop: 'static',
            resolve: {
                mainService: function () {
                    return mainService;
                }
            },
            controller: ['$scope', '$http', function ($scope, $http) {
                // Translation data
                // Remove unused no.
//                $scope.count = 10;
                $scope.isConfirmBtnDisabled = false;
                $scope.t = mainService.UserReference.TranslationMap;
                $scope.messages = [];
                $scope.cancel = function () {
                    $scope.$dismiss();
                };

                $scope.closeMessage = function (index) {
                    $scope.messages.splice(index, 1);
                };

                $scope.export = function () {
                    $scope.isConfirmBtnDisabled = true;
                    var id = $rootScope.User.MerchantId;
                    var InventoryLocationId = $state.params.locationId;
                    var url = $rootScope.servicePrefix + '/inventory/sheet/export?merchantid=' + id + '&inventorylocationid=' + InventoryLocationId;

                    $http.get(url).then(function (response) {
                        if (response.status === 200) {
                            window.location = "/api/document/sheets?folder=sheetexports&file=" + response.data.fileName + '&merchantid=' + id + '&AccessToken=Bearer ' + $rootScope.AuthToken;
                            $scope.$dismiss();
                        }
                    }, function (err) {
                        $scope.isConfirmBtnDisabled = false;
                        $scope.messages = [];
                        $scope.messages.push({type: "danger", msg: err.data.AppCode});
                    });

                };


            }]
        });
    };

    $scope.init = function () {
        // Default order
        $scope.predicate = 'LastModified';
//        $scope.reverse = false;
        $scope.reverse = true;

        $scope.t = mainService.UserReference.TranslationMap;
        $rootScope.settings = $state.current.settings;
        $scope.mainService = mainService;
        $scope.RequestService = RequestService;

        $scope.go = $state.go;

        $scope.messages = [];
        $scope.locationId = $state.params.locationId;

        //messages
        if ($state.params.msg && $state.params.type) {
            $scope.messages = [{
                msg: $scope.t[$state.params.msg] || $state.params.msg,
                type: $state.params.type
            }];
        }

        $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
        // default loading of the inventory product list
        //$scope.getSkuList();
    };

    $scope.init();
}
