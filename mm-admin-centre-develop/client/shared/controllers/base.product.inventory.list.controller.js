function BaseProductInventoryListCtrl($rootScope, $scope, $state, $modal, $loading, $http, RequestService, InventoryService, mainService) {
    var CONSTANTS = setScopeConstants();

    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        CONSTANTS.ROWACTION = {
            EDIT: '1',
            DELETE: '3'
        };

        CONSTANTS.INVENTORY_STATUS.ALL = '-1';

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }
    
    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.RequestService = RequestService;

    $scope.messages = [];
    $scope.skuId = $state.params.skuid;

    //messages
    if ($state.params.msg && $state.params.type) {
        $scope.messages = [{
            msg: $scope.t[$state.params.msg] || $state.params.msg,
            type: $state.params.type
        }];
    }
    $scope.actions = [{
            label: $scope.t.LB_EDIT,
            value: CONSTANTS.ROWACTION.EDIT
        }, {
            label: $scope.t.LB_DELETE,
            value: CONSTANTS.ROWACTION.DELETE
        }];

    // Default order
    $scope.predicate = 'InventoryStatusId';
    $scope.reverse = false;

    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    /**
     * Paginations
     */
    $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
    $scope.pageCount = CONSTANTS.DEFAULT_PAGE_COUNT;
    /**
     * end Paginations
     */

    function setBreadCrumb() {
        if ($scope.pageSide === 'admin') {
            $scope.breadcrumbs.push({
               text: $scope.t.LB_PRODUCT_LIST,
               link:"productList({merchantId:"+$scope.merchantId+"})"
            }, {
                text: Util.format($scope.t.LB_PROD_EDIT_TITLE,[$scope.sku.StyleCode]),
                link: 'productEdit({merchantId:"'+$scope.merchantId+'", stylecode: "' + $scope.sku.StyleCode + '",merchantId:"'+$scope.merchantId+'"})'
            }, {
                text: $scope.t.LB_INVENTORY_LIST
            });
        } else {
            $scope.breadcrumbs = [{
                text: $scope.t.LB_MERCHANT,
                link: 'home',
                icon: "icon-home"
            }, {
                link: 'productList',
                text: $scope.t.LB_PRODUCT_LIST
            }, {
                text: Util.format($scope.t.LB_PROD_EDIT_TITLE,[$scope.sku.StyleCode]),
                link: 'productEdit({stylecode: "' + $scope.sku.StyleCode + '"})'
            }, {
                text: $scope.t.LB_INVENTORY_LIST
            }];
        }
    }

    /**
     * Get Sku's inventory
     */
    $scope.getInventories = function () {
        $loading.start('loading');
        var params = {
            merchantid: $scope.merchantId,
            cc: $rootScope.User.CultureCode, //culture code
            skuid: $scope.skuId //location id
        };
        
        var hpromise = $http.get($rootScope.servicePrefix + '/inventory/list/sku', {
            params: params
        });

        $scope.inventories = [];
        hpromise.then(function (result) {
            $scope.sku = result.data.Sku; //product list
            $scope.inventories = result.data.InventoryList; //product list
            setBreadCrumb();
            $loading.finish('loading');
        }, function (errObj, status, headers, config) {
            if (window.console) {
                console.log(errObj, "Failed to retrieve product list. Please try again later.");
            }
            $scope.messages = [{
                    msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
                    type: 'danger'
                }];
            $loading.finish('loading');
        });
    };

    $scope.filterInventory = function (inventory) {
        var isPassed = false;
        if (typeof $scope.query !== 'undefined' && $scope.query !== '') {
            var query = $scope.query.toLowerCase();
            if (inventory.LocationName.toLowerCase().indexOf(query) !== -1 ||
                    inventory.LocationExternalCode.toLowerCase().indexOf(query) !== -1 ){
                isPassed = true;
            }
        } else {
            isPassed = true;
        }
        return isPassed;
    };

    $scope.filterStatus = function (inventory) {
        var isPassed = false;
        var filterStatusId = parseInt($scope.statusId);
        var productStatusId = parseInt(inventory.InventoryStatusId);

        switch (filterStatusId) {
            case CONSTANTS.INVENTORY_STATUS.IN_STOCK:
            case CONSTANTS.INVENTORY_STATUS.LOW_STOCK:
            case CONSTANTS.INVENTORY_STATUS.OUT_OF_STOCK:
                if (inventory.InventoryId && filterStatusId === productStatusId) {
                    isPassed = true;
                }
                break;
            case CONSTANTS.INVENTORY_STATUS.NA:
                if (inventory.InventoryId === null) {
                    isPassed = true;
                }
                break;
            default:
                isPassed = true;
                break;

        }
        return isPassed;
    }

    $scope.resetPage = function () {
        $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
    }

// Get inventories
    $scope.getInventories();
}