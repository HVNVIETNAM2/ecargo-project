'use strict';

function BaseProductChangeStatusCtrl($rootScope, $scope, $http, $state, $stateParams, mainService, style) {
    var CONSTANTS = setScopeConstants();

    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }
    
  // Translation data
  $scope.t = mainService.UserReference.TranslationMap;
  $scope.style = style;
  $scope.isConfirmBtnDisabled = false;

  $scope.cancel = function() {
    $scope.$dismiss();
  };

  $scope.closeMessage = function(){
    $scope.messages = [];
  }

  $scope.confirm = function() {
    var changedStatusInvariantName = '';
    var changedStatusName = '';
    var changedStatusId = '';
    switch ($scope.style.StatusId) {
      case CONSTANTS.STATUS.ACTIVE:
        changedStatusInvariantName = 'Inactive';
        changedStatusId = CONSTANTS.STATUS.INACTIVE;
        changedStatusName = CONSTANTS.STATUS_NAME[changedStatusId];
        break;
      case CONSTANTS.STATUS.INACTIVE:
        changedStatusInvariantName = 'Active';
        changedStatusId = CONSTANTS.STATUS.ACTIVE;
        changedStatusName = CONSTANTS.STATUS_NAME[changedStatusId];
        break;
      case CONSTANTS.STATUS.PENDING:
        changedStatusInvariantName = 'Active';
        changedStatusId = CONSTANTS.STATUS.ACTIVE;
        changedStatusName = CONSTANTS.STATUS_NAME[changedStatusId];
        break;
      default:
        throw "MSG_ERR_SKU_STATUS_UNKOWN";
    };
    $scope.isConfirmBtnDisabled = true;
    var hpromise = $http.post($rootScope.servicePrefix + '/product/style/status/change', {
      Status: changedStatusInvariantName,
      MerchantId: $stateParams.merchantId || $rootScope.User.MerchantId, //merchant id
      StyleCode: style.StyleCode
    });
    hpromise.then(function(result) {
      $scope.style.StatusId = changedStatusId;
      $scope.style.StatusName = changedStatusName;
      $scope.isConfirmBtnDisabled = false;
      $scope.$dismiss();
    }, function(errObj, status, headers, config) {
      $scope.isConfirmBtnDisabled = false;
      if (window.console) {
          console.log(errObj, "Failed to change product status. Please try again later.");
      }
      $scope.messages = [{
        msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
        type: 'danger'
      }];
    });
  };
}
