'use strict';

function BaseProductImportCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams) {
    var CONSTANTS = setScopeConstants();
    
    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }
    
  $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.t = mainService.UserReference.TranslationMap;

    //init variables
    $scope.canUpload = false;
    $scope.filetoupload = null;
    $scope.overwrite = 1;
    $scope.messages = [];
    $scope.uploadDone = false;
    $scope.importDone = false;
    $scope.uploading = false;
    $scope.importing = false;
    $scope.isBtnClicked = false;
    $scope.shownTab = 'ImportFile';


    $scope.allData = [];
    $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
    $scope.sortBy = 'LastModified';
    $scope.reverse = true;
    $scope.filter = "";
    $scope.tableMessage = $scope.t.LOADING_DATA;

    $scope.GUID = false;

    $scope.returnedData = {};
  $scope.MerchantId = $scope.MerchantId || $rootScope.User.MerchantId;

  $scope.downloadExcel = function (url) {
    url = url + '&merchantid=' + $scope.MerchantId + '&AccessToken=Bearer ' + $rootScope.AuthToken;
    var config = {
      method: 'GET',
      url: url,
      headers: {
        'Accept': 'application/vnd.ms-excel'
      }
    };
    $http(config)
        .success(function(res){
          location.href = url;
        })
        .error(function(res){
          alert($scope.t[res.AppCode] || res.Message);
        });
  };

    $scope.fetchHistory = function(){
      $http.get($rootScope.servicePrefix + '/product/sku/sheet/history?merchantid=' + $scope.MerchantId).then(function(response){
        if(response.status === 200){
//          tmpAllData = response.data;
          $scope.allData = response.data;
          $scope.tableMessage = $scope.t.LB_NO_AVAILABLE_DATA;
        }
      });
    };

    $scope.sortTable = function(sortBy){
      if($scope.sortBy === sortBy){
        $scope.reverse = $scope.reverse === false ? true: false;
      }else{
        $scope.reverse = false;
      }
      $scope.sortBy = sortBy;
    };

    $scope.newMessage = function(data){
      $scope.messages = [];
      $scope.messages.push(data);
    };

    $scope.cancelImport = function(){
        $state.go('productList', {merchantId: $scope.MerchantId});
    };

    $scope.closeMessage = function(index){
      $scope.messages.splice(index, 1);
    };

    $scope.callButton = function(){
      setTimeout(function() {
          document.getElementById('import-excel-input').click();
      }, 0);
    };

    $scope.importNow = function(){
      $scope.importing = true;
      $http.post($rootScope.servicePrefix + '/product/sku/sheet/import', {
        Guid : $scope.GUID,
        MerchantId: $scope.MerchantId
      }).then(function(response){
        //todo: better to return the updated record from the server
        $scope.fetchHistory();

        $scope.canImport = false;
        $scope.importing = false;
        $scope.importDone = true;
        $scope.isBtnClicked = false;
        $scope.returnedData = response.data.chunks;
        $scope.fileError = response.data.file;
      }, function(resp){
          $scope.canImport = false;
          $scope.filetoupload = null;
          $scope.importing = false;
          $scope.isBtnClicked = false;
          var errorCodeMsg = ($scope.t[resp.data.AppCode] === undefined) ? (resp.data.AppCode) : ($scope.t[resp.data.AppCode]);
          $scope.newMessage({ type : "danger", msg : errorCodeMsg});
      });
    };

    $scope.readUploadFile = function(){
      $scope.isBtnClicked = false;
      $scope.canImport = false;
      $scope.progressUpload = 0;
      if($scope.file===null){
        return;
      }

      if($scope.file.type.indexOf('sheet') > -1 || $scope.file.type.indexOf('spreadsheet') > -1 || $scope.file.type.indexOf('xls') > -1 || $scope.file.name.indexOf('xls') > -1){ //file.type is empty string in IE10, so need to use file.name to determine in IE10.
        $scope.canUpload = true;
        $scope.filetoupload = $scope.file.name;
        $scope.messages = [];

        $scope.uploading = true;

        Upload.upload({
            //add AccessToken as param to support IE9 since IE9 use flash to upload files. JWT will put accessToken in the header, but flash would not put anything JWT defined in header.
            url: $rootScope.servicePrefix+'/product/sku/sheet/upload?merchantid=' + $scope.MerchantId + '&AccessToken=Bearer ' + $rootScope.AuthToken,
            file : $scope.file,
            data : { Overwrite : $scope.overwrite, MerchantId : $scope.MerchantId }
        }).then(function (resp){
          $scope.GUID = resp.data.Guid;
          if ($scope.GUID) {
            //todo: better to return the inserted record from the server
            //$scope.allData.push(resp.data);
            $scope.fetchHistory();
          }
          $scope.canUpload = false;
          $scope.uploading = false;
          $scope.uploadDone = true;
          $scope.canImport = true;
          $scope.progressUpload = 0;
        }, function (resp) {
            $scope.filetoupload = null;
            $scope.uploading = false;
            var errorCodeMsg = ($scope.t[resp.data.AppCode] === undefined) ? (resp.data.AppCode) : ($scope.t[resp.data.AppCode]);
            $scope.newMessage({ type : "danger", msg : errorCodeMsg});
        }, function(evt){
          $scope.progressUpload = parseInt(100.0 * evt.loaded / evt.total);
        });

      }else{
        var errorCodeMsg = Util.format($scope.t.MSG_ERR_PRODCUT_IMPORT_FILE_FORMAT, ['excel']);
        $scope.newMessage({ type : "danger", msg : errorCodeMsg});
      }

      $scope.$apply();

    };

    $scope.fetchHistory();
}
