'use strict';

/**
 * Product Inventory Base Controller
 * @constructor
 */
function BaseProductInventoryCtrl($rootScope, $scope, $state, $timeout, $window, $modal, $loading, $location, $anchorScroll, RequestService, InventoryService, mainService) {
    var CONSTANTS = setScopeConstants();

    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        CONSTANTS.PERPETUAL = {
            TRUE: 1,
            FALSE: 0
        };
        
        CONSTANTS.INVENTORY_NEW_ID=0;

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }

    $scope.merchantId = $scope.merchantId || $rootScope.User.MerchantId;
    
    $loading.start('loading');
    InventoryService.get({
        params: {
            cc: $rootScope.User.CultureCode,
            skuid: $scope.skuId,
            inventorylocationid: $scope.inventoryLocationId,
            merchantid: $scope.merchantId
        }
    }).then(function (inventory) {
        $scope.inventory = inventory;

        if (!inventory.Inventory) {
            inventory.Inventory = {
                IsPerpetual: CONSTANTS.PERPETUAL.FALSE
            };
        }
        
        //make this static
        $scope.isPerpetual = inventory.Inventory.IsPerpetual;
        $scope.qtyAllocated = (inventory.Inventory.IsPerpetual)?'':inventory.Inventory.QtyAllocated;
        
        //breadcrumb propagation
        if ($scope.routePrefix !== 'location') {
            $scope.breadcrumbs.push({
                text: $scope.t.LB_PRODUCT_LIST,
                link: 'productList({merchantId:'+$scope.merchantId+'})'
            }, {
                text: Util.format($scope.t.LB_PROD_EDIT_TITLE,[inventory.Sku.StyleCode]),
                link: 'productEdit({stylecode: "' + inventory.Sku.StyleCode + '", merchantId: "' + $scope.merchantId + '"})'
            }, {
                text: $scope.t.LB_INVENTORY_LIST,
                link: 'productInventory({skuid: "' + inventory.Sku.SkuId + '", merchantId: "' + $scope.merchantId + '"})'
            });
        } else {
            $scope.breadcrumbs.push({
                text: $scope.t.LB_INVENTORY_LOCATION,
                link: 'inventoryLocation({merchantId: "' + $scope.merchantId + '"})'
            }, {
                text: inventory.InventoryLocation.LocationName,
                link: 'inventoryProduct({locationId: "' + $scope.inventoryLocationId + '", merchantId: "' + $scope.merchantId + '"})'
            });
        }


        $scope.breadcrumbs.push({
            text: ($scope.isEdit ? $scope.t.LB_EDIT_PRODUCT_INVENTORY : $scope.t.LB_PRODUCT_NEW_INVENTRY)
        });
        
        //Get status map
        
    }, function (err) {
        $scope.messages = [{
            msg: $scope.t[err.AppCode] || err.Message || err.toString(),
            type: 'danger'
        }];
    }).finally(function () {
        $loading.finish('loading');
    });

    $scope.listHistory = function () {
        $state.go($scope.routePrefix + 'Inventory-' + ($scope.isEdit ? 'edit' : 'create') + '-history', {
            skuid: $scope.skuId,
            inventoryid: $scope.inventoryId,
            inventorylocationid: $scope.inventoryLocationId,
            locname: $scope.inventory.InventoryLocation.LocationName,
            merchantId: $scope.merchantId
        });
    };
    
    $scope.$watch('isPerpetual', function (newValue, oldValue) {
        if (typeof oldValue !== 'undefined') {
            if (newValue == CONSTANTS.PERPETUAL.FALSE) {
                $timeout(function () {
                    angular.element('#allocation').focus();
                });
            } else {
                $scope.qtyAllocated = '';
            }
        }
    });

    /**
     * save or create new inventory
     */
    $scope.save = function () {
        InventoryService.save({
            InventoryId: $scope.isNewInventory ? CONSTANTS.INVENTORY_NEW_ID : $scope.inventoryId,
            QtyAllocated: $scope.qtyAllocated,
            IsPerpetual: $scope.isPerpetual,
            SkuId: $scope.skuId,
            InventoryLocationId: $scope.inventoryLocationId,
            MerchantId: $scope.merchantId
        }).then(function (inventory) {
            // Update current scope inventory
            $scope.inventory.Inventory = inventory;
            
            if (inventory && inventory.InventoryId) {
                $scope.messages = [{
                    msg: $scope.t['MSG_SUC_INVENTORY_EDIT'],
                    type: 'success'
                }];
                $location.hash('page_top');
                // call $anchorScroll()
                $anchorScroll();
            } else {
                alert('Unexpected Error, please try again');
            }
            
            if($scope.isNewInventory) {
                $scope.inventoryId = inventory.inventoryId;
                $scope.isNewInventory = false;
                
                if ($state.is('productInventory-create')) {
                    $state.go("productInventory", {skuid: $scope.skuId, merchantId: $scope.merchantId});
                }
                else if ($state.is('locationInventory-create')) {
                    $state.go('inventoryProduct', {locationId: $scope.inventoryLocationId, merchantId: $scope.merchantId});
                }
            }
        }, function (err) {
            $scope.messages = [{
                msg: $scope.t[err.AppCode] || err.Message || err.toString(),
                type: 'danger'
            }];
        });
    };

    /**
     * back to inventory list
     */
    $scope.back = function (noSendMsg) {
        var param = {};
        if (!noSendMsg) {
            param = {
                msg: $scope.isEdit ? 'MSG_SUC_INVENTORY_EDIT' : 'MSG_SUC_INVENTORY_CREATE',
                type: 'success'
            };
        }
        if ($scope.routePrefix === 'location') {
            $state.go('inventoryProduct', angular.extend({locationId: $scope.inventoryLocationId, merchantId: $scope.merchantId}, param));
        } else {
            $state.go('productInventory', angular.extend({skuid: $scope.skuId, merchantId: $scope.merchantId}, param));
        }
    };
}