'use strict';

function BaseAccountCtrl($rootScope, $scope, $timeout, Constants, $location, $window,
                         $modal, $log, $http, Upload, $state, $stateParams, jwtHelper, translationService, mainService) {

    // Set some variables in $rootScope to hide the side/top menu based on state
    $rootScope.settings = $state.current.settings;
    $scope.t = mainService.UserReference.TranslationMap;
    $scope.user = {};

    $scope.PASSWORD_REGEX = /^(?=.*[A-Z])(?=.*[!-/:-@\[-`{-~])(?=.*[0-9])(?=.*[a-z]).{8,}$/

    if ($stateParams.err) {
        $scope.paramErr = $stateParams.err;
    }

    $scope.$on('invalid.platform.login', function () {
        $scope.validationForm.$submitted = false;
        $scope.paramErr = 'MSG_ERR_USER_AUTHENTICATION_FAIL';
        $scope.errCode = '';
    });

    $scope.login = function (validationForm) {
        validationForm.$submitted = true;
        //ie fix
        $('form[name=validationForm]').find('input, textarea, select').trigger('input').trigger('change').trigger('keydown');
        $timeout(function () {
            if (validationForm.$valid) {
                //set login btn disabled
                $scope.isConfirmBtnDisabled = true;
                $http({
                    method: "POST",
                    url: $rootScope.servicePrefix + "/auth/login",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'},
                    data: $.param({
                        Username: $scope.user.email,
                        Password: $scope.user.password
                    })
                }).then(function (result) {
                    $rootScope.AuthToken = result.data.Token;

                    $http.get($rootScope.servicePrefix + "/user/view?userkey=" + result.data.UserKey + '&merchantid=' + result.data.MerchantId).then(function (result) {
                        var tokenPayload = result.data;
                        $rootScope.User = tokenPayload;
                        $rootScope.tokenSave();
                        $state.go('home');
                    });
                }, function (errObj, status, headers, config) {

                    //global util module outside of angular.
                    if (errObj.data.AppCode === 'MSG_ERR_USER_STATUS_PENDING') {
                        $state.go("accountPending");
                    }
                    else if (errObj.data.AppCode === 'MSG_ERR_USER_ATTEMPTS_EXCEEDED') {
                        $state.go("accountLocked");
                    }
                    else if (errObj.data.AppCode === 'MSG_ERR_USER_AUTHENTICATION_INVALID') {

                        $scope.errCode = errObj.data.AppCode;
                        $scope.errParams = [(Constants.MAX_LOGIN_ATTEMPTS_COUNT - errObj.data.LoginAttempts).toString()];
                    }
                    else {
                        $scope.errCode = errObj.data.AppCode;
                    }
                }).finally(function () {
                    //set login btn enable
                    $scope.isConfirmBtnDisabled = false;
                });
            }
        });
    };

    $scope.completeRegistration = function (validationForm, successRoute) {
        validationForm.$submitted = true;

        if (validationForm.$valid) {
            // Should build a custom directive to check confirm password.
            if ($scope.user.password && ($scope.user.password === $scope.user.confirmPassword)) {
                var activationToken = $stateParams.at;
                var hpromise = $http.post($rootScope.servicePrefix + '/auth/activate', {
                    "ActivationToken": activationToken,
                    "Password": $scope.user.password
                });
                hpromise.then(function (result) {
                    $state.go(successRoute ? successRoute : 'activateSuccess');
                }, function (errObj, status, headers, config) {
                    $scope.errMsg = $scope.t[errObj.data.AppCode] || errObj.data.AppCode;
                });
            }
        }
    };

    $scope.resetPassword = function (validationForm) {
        validationForm.$submitted = true;

        if (validationForm.$valid) {
            // Should build a custom directive to check confirm password.
            if ($scope.user.password && ($scope.user.password === $scope.user.confirmPassword)) {
                var activationToken = $stateParams.at;
                var hpromise = $http.post($rootScope.servicePrefix + '/auth/activate', {
                    "ActivationToken": activationToken,
                    "Password": $scope.user.password
                });
                hpromise.then(function (result) {
                    $state.go('resetPassSuccess');
                }, function (errObj, status, headers, config) {
                    $scope.errMsg = $scope.t[errObj.data.AppCode] || errObj.data.AppCode;
                });
            }
        }
    };


    $scope.openForgotPasswordDialog = function () {
        $modal.open({
            templateUrl: "/shared/views/forgot-password-dialog.html",
            backdrop: 'static',
            resolve: {
                translationService: function (TranslationService) {
                    var defaultLang = $rootScope.preference.lang;
                    return TranslationService.loadTranslationData(defaultLang);
                }
            },
            controller: ['$scope', 'translationService', function ($scope, translationService) {
                $scope.user = {};
                $scope.t = translationService.TranslationMap;
                $scope.closeMessage = function (index) {
                    $scope.messages.splice(index, 1);
                };

                $scope.openConfirmEmailSentDialog = function () {
                    $modal.open({
                        templateUrl: "/shared/views/confirm-email-sent-dialog.html",
                        backdrop: 'static',
                        controller: ['$scope', function ($scope) {
                            $scope.t = translationService.TranslationMap;
                            $scope.confirm = function () {
                                $scope.$close(true);
                            };
                        }]
                    });
                };

                $scope.cancel = function () {
                    $scope.$dismiss();
                };

                $scope.confirm = function (validationForm) {
                    validationForm.$submitted = true;

                    // Clear the messages
                    $scope.messages = [];

                    if (validationForm.$valid) {
                        //set confirm btn disabled
                        $scope.isConfirmBtnDisabled = true;
                        var hpromise = $http.post($rootScope.servicePrefix + '/auth/password/forgot', {
                            "Email": $scope.user.email
                        });
                        hpromise.then(function (result) {
                            $scope.openConfirmEmailSentDialog();
                            $scope.$close(true);
                        }, function (errObj, status, headers, config) {
                            var errorCodeMsg = ($scope.t[errObj.data.AppCode] === undefined) ? (errObj.data.AppCode) : ($scope.t[errObj.data.AppCode]);
                            $scope.messages = [{
                                msg: errorCodeMsg,
                                type: 'danger'
                            }];
                        }).finally(function () {
                            //set confirm btn enabled
                            $scope.isConfirmBtnDisabled = false;
                        });
                        ;
                    }
                };
            }]
        });
    };

}
