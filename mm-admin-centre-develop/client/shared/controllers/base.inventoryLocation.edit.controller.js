'use strict';

function BaseInventoryLocationEditCtrl($rootScope, $scope, $location, $anchorScroll, $window, $modal, $log, $http,
                                       Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService, InventoryService) {
    var CONSTANTS = $rootScope.CONSTANTS;
    
    $scope.t = mainService.UserReference.TranslationMap;
    $rootScope.settings = $state.current.settings;
    $scope.selectT = generalTranslationService.UserReference;

    $scope.inventoryLocationId = $state.params.inventorylocationid;
    $scope.isEdit = $state.is('editInventoryLocation');
    $scope.merchantId = $scope.merchantId || $rootScope.User.MerchantId;

    $scope.breadcrumbs.push(
        {text: $scope.t.LB_INVENTORY_LOCATION, link: 'inventoryLocation({merchantId: "' + $scope.merchantId + '"})'},
        {text: $scope.isEdit ? $scope.t.LB_EDIT : $scope.t.LB_CREATE}
    );

    $scope.location = {};
    $scope.addMore = false;

    //initialize translation
    $scope.selectT = generalTranslationService.UserReference;

    //geoData
    $scope.geoCountries = geoService.countries;
    $scope.geoProvinces = [];
    $scope.geoCities = [];
    var parentScope = $scope;

    //messages
    if ($state.params.msg && $state.params.type) {
        $scope.messages = [{
            msg: $scope.t[$state.params.msg] || $state.params.msg,
            type: $state.params.type
        }];
    }

    if ($scope.isEdit) {
        InventoryService.getLocation({
            params: {
                id: $scope.inventoryLocationId,
                cc: $rootScope.User.CultureCode,
                merchantid: $scope.merchantId
            }
        }).then(function (location) {
            $scope.location = location;
            $scope.locationName = location.Culture[$rootScope.language].LocationName;
            $scope.location.InventoryLocationTypeId = $scope.location.InventoryLocationTypeId.toString();
            $scope.location.InventoryLocationId = $scope.location.InventoryLocationId.toString();

            $scope.location.GeoCountryId = $scope.location.GeoCountryId.toString();
            $scope.location.GeoIdProvince = $scope.location.GeoIdProvince.toString();
            $scope.location.GeoIdCity = $scope.location.GeoIdCity.toString();

            $scope.countrySelected();
            $scope.provinceSelected();
        });
    }

    $scope.countrySort = InventoryService.countrySort;


    $scope.countrySelected = function () {
        if (!$scope.location || _.isUndefined($scope.location.GeoCountryId)) {
            return;
        }

        geoService.getProvince($scope.location.GeoCountryId).then(function (data) {
            $scope.geoProvinces = data;
            if (!_.any(data, function (d) {
                    return d.GeoId === _.parseInt($scope.location.GeoIdProvince);
                })) {
                $scope.location.GeoIdProvince = '';
                $scope.location.GeoIdCity = '';
            }
        });
    };

    $scope.provinceSelected = function () {
        if (!$scope.location || _.isUndefined($scope.location.GeoIdProvince)) {
            return;
        }

        geoService.getCities($scope.location.GeoIdProvince).then(function (data) {
            $scope.geoCities = data;
            if (!_.any(data, function (d) {
                    return d.GeoId === _.parseInt($scope.location.GeoIdCity);
                })) {
                $scope.location.GeoIdCity = '';
            }
        });
    };



    $scope.changeStatus = function (item) {
        var modalInstance = $modal.open({
            templateUrl: "/shared/views/change-status-location-dialog.html",
            backdrop: 'static',
            resolve: {
                mainService: function () {
                    return mainService;
                }
            },
            controller: ['$scope', 'mainService', function ($scope, mainService) {
                $scope.item = item;
                //global util module outside of angular.
                // Translation data
                $scope.t = mainService.UserReference.TranslationMap;
                $scope.cancel = function () {
                    $scope.$dismiss();
                };

                $scope.confirm = function () {
                    var newStatus, newStatusId;
                    if (item.StatusNameInvariant === 'Active') {
                        newStatus = 'Inactive';
                        newStatusId = CONSTANTS.STATUS.INACTIVE;
                    }
                    else if (item.StatusNameInvariant === 'Inactive' ||
                        item.StatusNameInvariant === 'Pending') {
                        newStatus = 'Active';
                        newStatusId = CONSTANTS.STATUS.ACTIVE;
                    }

                    //set confirm btn disabled;
                    $scope.isConfirmBtnDisabled = true;

                    var hpromise = $http.post($rootScope.servicePrefix + '/inventory/location/status/change', {
                        "InventoryLocationId": item.InventoryLocationId,
                        'Status': newStatus,
                        MerchantId: parentScope.merchantId
                    });

                    hpromise.then(function (result) {
                        item.StatusNameInvariant = newStatus;
                        item.StatusId = newStatusId;
                        $scope.$close(item);
                    }, function (errObj, status, headers, config) {
                        $scope.messages = [
                            {msg: 'Failed to change status. Please try again later.', type: 'danger'}
                        ];
                    }).finally(function () {
                        //set confirm btn enabled;
                        $scope.isConfirmBtnDisabled = false;
                    });
                };

                $scope.closeMessage = function (index) {
                    $scope.messages.splice(index, 1);
                };
            }]
        });

        modalInstance.result.then(function (data) {
            // Update the affected row's Status field
            $scope.location = data;
        });
    };


    $scope.closeMessage = function (index) {
        $scope.messages.splice(index, 1);
    };

    $scope.cancel = function () {
        // $scope.$dismiss();
        $state.go('inventoryLocation', {merchantId: $scope.merchantId});
    };

    $scope.submit = function (validationForm) {

        validationForm.$submitted = true;
        // Clear the messages
        $scope.messages = [];

        if (validationForm.$valid) {
            var cloned = angular.copy($scope.location);
            //parse
            $scope.location.InventoryLocationTypeId = parseInt($scope.location.InventoryLocationTypeId);

            //for GEO
            $scope.location.GeoCountryId = parseInt($scope.location.GeoCountryId);
            $scope.location.GeoIdProvince = parseInt($scope.location.GeoIdProvince);
            $scope.location.GeoIdCity = parseInt($scope.location.GeoIdCity);


            if ($scope.isEdit) {
                $scope.location.InventoryLocationId = parseInt($scope.location.InventoryLocationId);
                //set confirm btn disabled
                $scope.isConfirmBtnDisabled = true;
                var hpromise = $http.post($rootScope.servicePrefix + '/inventory/location/save', $scope.location);
                hpromise.then(function (result) {

                    $scope.location.InventoryLocationTypeId = $scope.location.InventoryLocationTypeId.toString();
                    $scope.location.GeoCountryId = $scope.location.GeoCountryId.toString();
                    $scope.location.GeoIdProvince = $scope.location.GeoIdProvince.toString();
                    $scope.location.GeoIdCity = $scope.location.GeoIdCity.toString();


                    $scope.messages = [
                        {
                            msg: ($scope.t.MSG_SUC_INVENTORY_LOCAL_UPDATE || "Inventory Location successfully updated"),
                            type: 'success'
                        }
                    ];

                }, function (errObj, status, headers, config) {
                    $scope.messages = [
                        {msg: ($scope.t[errObj.data.AppCode] || errObj.data.AppCode), type: 'danger'}
                    ];
                }).finally(function () {
                    $scope.location = cloned;
                    //set confirm btn enabled
                    $scope.isConfirmBtnDisabled = false;
                    validationForm.$submitted = false;
                    validationForm.LocationNameEN.$touched = false;
                    validationForm.LocationNameCHS.$touched = false;
                    validationForm.LocationNameCHT.$touched = false;
                    validationForm.LocationExternalCode.$touched = false;
                    validationForm.InventoryLocationTypeId.$touched = false;
                    validationForm.GeoCountryId.$touched = false;
                    validationForm.GeoIdProvince.$touched = false;
                    validationForm.GeoIdCity.$touched = false;
                    $location.hash('page_top');
                    $anchorScroll();
                });
            } else {
                $scope.location.InventoryLocationId = '';

                $scope.location.QtySafetyThreshold = $scope.location.QtySafetyThreshold ? $scope.location.QtySafetyThreshold : 0;

                //pass the merchantId;
                var merchantid = $rootScope.User.MerchantId || 0;
                if ($scope.pageSide === 'admin') {
                    merchantid = $scope.merchantId;
                }
                $scope.location.MerchantId = merchantid;

                //set confirm btn disabled
                $scope.isConfirmBtnDisabled = true;
                var hpromise = $http.post($rootScope.servicePrefix + '/inventory/location/save', $scope.location);
                hpromise.then(function (result) {
                    if ($scope.location.addMore) {
                        //create more
                        $scope.location = {};
                        $scope.location.addMore=true;
                        $scope.messages = [{
                            msg: $scope.t['MSG_SUC_INV_LOC_CREATE'] || $state.params.msg,
                            type: 'success'
                        }];
                    } else {
                        //back to list
                        $state.go('inventoryLocation', {merchantId: $scope.merchantId, msg: 'MSG_SUC_INV_LOC_CREATE', type: 'success'});
                    }
                }, function (errObj, status, headers, config) {
                    $scope.messages = [
                        {msg: ($scope.t[errObj.data.AppCode] || errObj.data.AppCode), type: 'danger'}
                    ];
                    $scope.location = cloned;
                }).finally(function () {
                    //set confirm btn enabled
                    $scope.isConfirmBtnDisabled = false;
                    validationForm.$submitted = false;
                    validationForm.LocationNameEN.$touched = false;
                    validationForm.LocationNameCHS.$touched = false;
                    validationForm.LocationNameCHT.$touched = false;
                    validationForm.LocationExternalCode.$touched = false;
                    validationForm.InventoryLocationTypeId.$touched = false;
                    validationForm.GeoCountryId.$touched = false;
                    validationForm.GeoIdProvince.$touched = false;
                    validationForm.GeoIdCity.$touched = false;
                    $location.hash('page_top');
                    $anchorScroll();
                });
            }
        }

    };

}