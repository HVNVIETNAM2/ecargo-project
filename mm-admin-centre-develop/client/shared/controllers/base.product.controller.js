'use strict';

function BaseProductCtrl($rootScope, $scope, Constants, $location, $window,
  $modal, $log, $http, Upload, $state, $stateParams, translationService, mainService) {

  // Set some variables in $rootScope to hide the side/top menu based on state
  $rootScope.settings = $state.current.settings;
  $scope.products = {};

}
