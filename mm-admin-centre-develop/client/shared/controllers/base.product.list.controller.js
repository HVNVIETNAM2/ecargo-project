'use strict';

function BaseProductListCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $loading, generalTranslationService) {
    var CONSTANTS = setScopeConstants();

    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }

    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.t = mainService.UserReference.TranslationMap;
    $scope.generalTranslationService = generalTranslationService;

    $scope.productSearchTerm = '';
    $scope.categoryId = '';
    $scope.missingImgType = -1;
    $scope.statusId = '';
    $scope.styles = [];
    $scope.statusList = []; //status list for filter the style list.
    $scope.categoryList = []; //category list for filter the style list.

    //pagination params
    $scope.itemsPerPage = CONSTANTS.DEFAULT_ITEMS_PER_PAGE; // page size
    $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO; //current page number; page number start from 1.
    $scope.PageTotal = 0; //total pages number

    var merchantId = $scope.merchantId || $rootScope.User.MerchantId;

    //filter function definition
    $scope.filterProductBySearch = function () {
        $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
        $scope.getSkuList();
    };
    $scope.filterProductByStatus = function () {
        $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
        $scope.getSkuList();
    };
    $scope.filterProductByCategory = function () {
        $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
        $scope.getSkuList();
    };
    $scope.filterProductByMissingImgType = function () {
        $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
        $scope.getSkuList();
    };

    //get sku list function
    $scope.getSkuList = function () {
        $loading.start('loading');
        var pageNum = $scope.currentPage;
        var pageSize = $scope.itemsPerPage;
        var searchKeyword = $scope.productSearchTerm;
        var categoryId = $scope.categoryId;
        var statusId = $scope.statusId;
        var hpromise = $http.get($rootScope.servicePrefix + '/product/style/list', {
            params: {
                merchantid: $scope.merchantId, //merchant id
                cc: $rootScope.User.CultureCode, //culture code
                categoryid: (categoryId === undefined || categoryId == '-1') ? '' : categoryId, //categoryid
                s: (searchKeyword === undefined) ? '' : searchKeyword, //search keywords
                statusid: (statusId === undefined || statusId == '-1') ? '' : statusId, //statusId
                page: pageNum,
                size: pageSize,
                missingimgtype: $scope.missingImgType
            }
        });
        hpromise.then(function (result) {
            $scope.styles = result.data.PageData; //product list
//      $scope.currentPage = result.data.PageCurrent;
            $scope.PageTotal = result.data.PageTotal;
            $loading.finish('loading');
        }, function (errObj, status, headers, config) {
            if (window.console) {
                console.log(errObj, "Failed to retrieve product list. Please try again later.");
            }
            $scope.messages = [{
                msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
                type: 'danger'
            }];
            $loading.finish('loading');
        });
    };

    // convertion function definition

    //controller init function
    $scope.init = function () {
        //1.retrive sku list
        //the pagination would call this
        //$scope.getSkuList($scope.currentPage, $scope.itemsPerPage);

        //2.build up the status list
        generalTranslationService.UserReference.StatusList.forEach(function (item) {
            if (item.StatusId !== CONSTANTS.STATUS.DELETED) {
                $scope.statusList.push(item);
            }
        });
        // $scope.statusList.unshift({StatusId:-1, StatusNameInvariant: $scope.t.LB_ALL_STATUS});

        //3.build up category list
        getCategoryList();

        /**
        function buildCategoryTree (categories) {
            $scope.categoryTree = [];

            while(categories.length > 0) {
                var c = categories.shift();
                if (c.ParentCategoryId === 0) {
                    $scope.categoryTree.push(c);
                } else {
                    (function traverse (categories, c) {
                        _.each(categories, function (cat) {
                            if (cat.ParentCategoryId === c.CategoryId) {
                                if (!c.ChildCategories) {
                                    c.ChildCategories = [];
                                }
                                c.ChildCategories.push(cat);
                            } else if (cat.ChildCategories) {
                                traverse(cat.ChildCategories, cat);
                            }
                        });
                    })(categories, c);
                }
            }

            $scope.categoryList = [];

            (function applyLevelStyle (categories, level) {
                _.each(categories, function (category) {
                    category.CategoryName = _.repeat(' ', level) + category.CategoryName;
                    $scope.categoryList.push(category);
                    if (category.ChildCategories && category.ChildCategories.length) {
                        applyLevelStyle(category.ChildCategories, level + 1);
                    }
                });
            })($scope.categoryTree, 0);
        }
         **/

        // get category list
        function getCategoryList() {
            //retrive the category list
            var hpromise = $http.get($rootScope.servicePrefix + '/category/list', {
                params: {
                    cc: $rootScope.User.CultureCode //culture code
                }
            });
            hpromise.then(function (result) {
                $scope.categoryList = result.data;
            }, function (errObj, status, headers, config) {
                if (window.console) {
                    console.log(errObj, "Failed to retrieve category list. Please try again later.");
                }
                $scope.messages = [{
                    msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
                    type: 'danger'
                }];
            });
        }

    };

    //operation function definition
    $scope.onDoubleClick = function (styleCode, merchantId) {
        $state.go("");
    };

    $scope.importInit = function (merchantId) {
        //call to import
        $state.go('productImport', {
            merchantId: merchantId
        });
    };

    $scope.selectAction = function (style) {
        var action = style.rowAction.value;
        if (action === 'edit') {
            $state.go('productEdit', {
                stylecode: style.StyleCode,
                merchantId: $scope.merchantId
            });
        } else if (action === 'activate' || action === 'inactivate') {
            $modal.open({
                templateUrl: "/shared/views/change-product-status.html",
                backdrop: 'static',
                resolve: {
                    mainService: function () {
                        return mainService;
                    },
                    style: function () {
                        return style;
                    }
                },
                controller: "productChangeStatusCtrl"
            });
        } else if (action === 'delete') {
            alert("wait for development!");
        } else {
            if (window.console) {
                console.log('action is not existed!');
            }
        }
        style.rowAction = '';
    };

    $scope.activatePending = function () {
        $modal.open({
            templateUrl: "/shared/views/activate-product.html",
            keyboard: false,
            backdrop: 'static',
            resolve: {
                mainService: function () {
                    return mainService;
                }
            },
            controller: ['$scope', 'mainService',
                function ($scope, mainService) {
                    // Translation data
                    $scope.isConfirmBtnDisabled = false;
                    $scope.t = mainService.UserReference.TranslationMap;
                    $scope.messages = [];
                    $scope.closeMessage = function (index) {
                        $scope.messages.splice(index, 1);
                    };
                    $scope.confirm = function () {

                        //reset messages
                        $scope.messages = [];
                        $scope.isConfirmBtnDisabled = true;
                        var url = $rootScope.servicePrefix + '/product/sku/activate/all';

                        $http.post(url, {MerchantId: merchantId}).then(function (response) {
                            if (response.status === 200) {
                                $scope.$dismiss();
                                setTimeout(function () {
                                    window.location.reload();
                                }, 300);
                            }
                        }, function (e) {
                            $scope.isConfirmBtnDisabled = false;
                            $scope.messages.push({
                                msg: ((e && e.data && e.data.AppCode) || "Server Internal Error. Try again later."),
                                type: "danger"
                            });
                        });

                    };
                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                }
            ]
        });
    };

    $scope.openExportDialog = function () {
        $modal.open({
            templateUrl: "/shared/views/export-product.html",
            keyboard: false,
            backdrop: 'static',
            resolve: {
                mainService: function () {
                    return mainService;
                }
            },
            controller: ['$scope', 'mainService',
                function ($scope, mainService) {
                    // Translation data
                    $scope.t = mainService.UserReference.TranslationMap;
                    $scope.confirm = function () {
                        // this value is undefined;
                        // var id = $scope.merchantId; //merchant id
                        var id = merchantId;
                        var cc = $rootScope.User.CultureCode;
                        var url = $rootScope.servicePrefix + '/product/export?merchantid=' + id + '&cc=' + cc;
                        if (window.console) {
                            console.log(url);
                        }
                        $http.get(url).then(function (response) {
                            if (response.status === 200) {
                                window.location = "/api/document/sheets?folder=sheetexports&file=" + response.data.fileName + '&merchantid=' + id + '&AccessToken=Bearer ' + $rootScope.AuthToken;
                                $scope.$dismiss();
                            }
                        });
                        // window.location.assign(url);
                        //window.open(url, "_blank");
                    };
                    $scope.cancel = function () {
                        $scope.$dismiss();
                    };
                }
            ]
        });
    };

    $scope.uploadImage = function (merchantId) {
        $state.go('productUpload', {
            merchantId: merchantId
        });
    };

    $scope.editProduct = function (stylecode, merchantId) {
        $state.go('productEdit', {
            stylecode: stylecode,
            merchantId: merchantId
        });
    };

    //operation action definetion
    $scope.activateActions = [{
        lable: $scope.t.LB_EDIT,
        value: 'edit'
    }, {
        lable: $scope.t.LB_PRODUCT_ACTIVATION,
        value: 'activate'
    }];

    $scope.inactivateActions = [{
        lable: $scope.t.LB_EDIT,
        value: 'edit'
    }, {
        lable: $scope.t.LB_PRODUCT_INACTIVATION,
        value: 'inactivate'
    }];

    $scope.missingImgTypeList = [{
        label: $scope.t.LB_AC_MISSING_FEATURE_IMG,
        value: 1
    }, {
        label: $scope.t.LB_AC_MISSING_COLOR_IMG,
        value: 2
    }, {
        label: $scope.t.LB_AC_MISSING_DESC_IMG,
        value: 3
    }];

    //build the category text by category list
    $scope.buildCategoryText = function (CategoryPathList) {
        return _.reduce(CategoryPathList, function (result, val, index) {
            if (!val.ParentCategoryId && index !== 0) {
                result += ', ' + val.CategoryName;
            } else {
                result += (index !== 0 ? ' > ' : '') + val.CategoryName;
            }

            return result;
        }, '');
    };

    $scope.init();
}
