'use strict';

function BaseUserListCtrl($rootScope, $scope, $location, $window, $modal, 
  $log, $http, mainService, FileUploadService, $state, generalTranslationService) {
    var CONSTANTS = setScopeConstants();
    
    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;
        
        CONSTANTS.STATUS.ALL = '-1';
        CONSTANTS.ROLE = {
            ALL:'-1'
        };

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }
    
  $scope.generalTranslationService = generalTranslationService;
  $rootScope.settings = $state.current.settings;
  $scope.mainService = mainService;
  $scope.t = mainService.UserReference.TranslationMap;
  
  $scope.SecurityGroupList = [];
  $scope.StatusList = [];


  generalTranslationService.UserReference.StatusList.forEach(function(item){
    if(item.StatusId !== CONSTANTS.STATUS.DELETED){
      $scope.StatusList.push(item);
    }
  });

  if($scope.pageSide === "merchant"){
    generalTranslationService.UserReference.SecurityGroupList.forEach(function(item){
      if(item.UserTypeId === CONSTANTS.USER_TYPE.MERCHANT){
        $scope.SecurityGroupList.push(item);
      }
    });
  }

  if($scope.pageSide === "admin"){
    generalTranslationService.UserReference.SecurityGroupList.forEach(function(item){
      if(item.UserTypeId === CONSTANTS.USER_TYPE.MM){
        $scope.SecurityGroupList.push(item);
      }
    });
  }

  $scope.messages = [];

  $scope.userListActionsByStatus = {
    'Active': [
      {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
      {lable: $scope.t.LB_INACTIVATE_USER, value:'Inactivate'},
      {lable: $scope.t.LB_TL_RESET_PASSWORD, value:'Reset Password'},
      {lable: $scope.t.LB_RESEND_CHANGE_EMAIL_LINK, value:'Resend Change Email Link'},
      {lable: $scope.t.LB_DELETE_USER, value:'Delete'}
    ],
    'Inactive': [
      {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
      {lable: $scope.t.LB_ACTIVATE_USER, value:'Activate'},
      {lable: $scope.t.LB_DELETE_USER, value:'Delete'}
    ],
    'Pending': [
      {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
      {lable: $scope.t.LB_TL_RESET_PASSWORD, value:'Reset Password'},
      {lable: $scope.t.LB_RESEND_REG_LINK, value:'Resend Registration Link'},
      {lable: $scope.t.LB_RESEND_CHANGE_EMAIL_LINK, value:'Resend Change Email Link'},
      {lable: $scope.t.LB_DELETE_USER, value:'Delete'}
    ]
  };


  $scope.getSelfListActions = function (userInfo) {
    //may always be active for self account as he cannot login...but for safety, make it a redundant check here
    if (userInfo.StatusId === CONSTANTS.STATUS.ACTIVE) {
      return [
        {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
        {lable: $scope.t.LB_TL_RESET_PASSWORD, value:'Reset Password'},
        {lable: $scope.t.LB_RESEND_CHANGE_EMAIL_LINK, value:'Resend Change Email Link'}
      ];
    }

    return [
      {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
      {lable: $scope.t.LB_TL_RESET_PASSWORD, value:'Reset Password'},
      {lable: $scope.t.LB_RESEND_REG_LINK, value:'Resend Registration Link'},
      {lable: $scope.t.LB_RESEND_CHANGE_EMAIL_LINK, value:'Resend Change Email Link'}
    ];
  };

  $scope.allUserData = [];
  $scope.tmpAllData = [];
  $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
  $scope.sortBy = 'FirstName';
  $scope.rowActions = '';
  $scope.reverse = false;
  $scope.filter = "";

  $scope.filterStatus = window.localStorage.getItem($scope.pageSide + 'userFilterStatus') || CONSTANTS.STATUS.ALL;
  $scope.filterRole = window.localStorage.getItem($scope.pageSide + 'userFilterRole') || CONSTANTS.ROLE.ALL;
  $scope.filterSearch = window.localStorage.getItem($scope.pageSide + 'userFilterSearch') || "";

  $scope.getData();

  $scope.sortTable = function(sortBy){
    if($scope.sortBy === sortBy){
      $scope.reverse = $scope.reverse === false;
    }else{
      $scope.reverse = false;
    }
    $scope.sortBy = sortBy;
    $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
  };

  $scope.getStatusDisplayName = function(StatusNameInvariant){
    var displayName;
    switch (StatusNameInvariant) {
      case 'Active':
        displayName = $scope.t.LB_ACTIVE;
        break;
      case 'Inactive':
        displayName = $scope.t.LB_INACTIVE;
        break;
      case 'Pending':
        displayName = $scope.t.LB_PENDING;
        break;
      case 'Deleted':
        displayName = $scope.t.LB_DELETE_USER;
        break;
    }
    return displayName;
  };

  $scope.filterUserBySearch = function(user){
    var lowercaseTerm = $scope.userSearchTerm ? $scope.userSearchTerm.toLowerCase() : '';

    if (!user) {
      return false;
    }

    if (user.FirstName && user.FirstName.toLowerCase().indexOf(lowercaseTerm) > -1) {
      return true;
    }
    if (user.LastName && user.LastName.toLowerCase().indexOf(lowercaseTerm) > -1) {
      return true;
    }
    if (user.DisplayName && user.DisplayName.toLowerCase().indexOf(lowercaseTerm) > -1) {
      return true;
    }
    if (user.Mobile_Num && user.Mobile_Num.toLowerCase().indexOf(lowercaseTerm) > -1) {
      return true;
    }
    if (user.MobileCode && user.MobileCode.toLowerCase().indexOf(lowercaseTerm) > -1) {
      return true;
    }
    if (user.Email && user.Email.toLowerCase().indexOf(lowercaseTerm) > -1) {
      return true;
    }

    return false;
  };
  
  $scope.filterUserByStatus = function(item){
    var value = parseInt($scope.filter.status);
    window.localStorage.setItem($scope.pageSide + 'userFilterStatus', value);
    if(value < 0){ return true; }

    if (value === item.StatusId){
      return true;
    }

    return false;
  };

  $scope.filterUserByRole = function(item){
    var value = parseInt($scope.filter.role);
    window.localStorage.setItem($scope.pageSide + 'userFilterRole', value);
    if(value < 0){ return true; }

    if(_.contains(item.UserSecurityGroupArray, value)){
      return true;
    }

    return false;
  };

  $scope.openCreateUserDialog = function() {
    var modalInstance = $modal.open({
      templateUrl: '/shared/views/modal-user-form.html',
      controller: 'ModalUserFormCtrl',
      scope: $scope,
      size: 'md',
      backdrop : 'static',
      resolve: {
        mainService: function() {
          return mainService;
        },
        generalTranslationService: function() {
          return generalTranslationService;
        },
        FileUploadService: function() {
          return FileUploadService;
        },
        Http: function() {
          return $http;
        }
      }
    });
    modalInstance.result.then(function(state) {
      // Do something when $close() is called.
    }, function() {
      // Do something when $dismiss() is called.
    });
  };

  $scope.showMessage = function(msg) {
    $scope.messages.push(msg);
  };

  $scope.closeMessage = function(index) {
    $scope.messages.splice(index, 1);
  };

  $scope.selectAction = function(row){
    var userKey = row.UserKey;
    var rowActions = row.rowActions;
    var action = rowActions.value;

    if (!rowActions) {
      return;
    }

    row.rowActions = null;

    if(action === 'Edit'){
      // $state.go('userEdit', { userId: userId });
      $scope.onDoubleClick(userKey);
    }else if(action === 'Activate' || action === 'Inactivate') {
      $scope.openChangeStatusDialog(row);
    }else if (action === 'Reset Password'){
      $scope.openResetPasswordDialog(row);
    }else if (action === 'Resend Registration Link'){
      $scope.openResendLinkDialog({
        user: row,
        templateUrl: '/shared/views/resend-registration-link-dialog.html',
        linkType: 'Registration',
        errorMsg: $scope.t.MSG_ERR_RESEND_REG_FAIL
      });
    }else if (action === 'Resend Change Email Link'){
      $scope.openResendLinkDialog({
        user: row,
        templateUrl: '/shared/views/resend-change-email-link-dialog.html',
        linkType: 'Email',
        errorMsg: $scope.t.MSG_ERR_RESEND_CHNG_EMAIL_FAIL
      });
    }else if (action === 'Delete') {
      $scope.openDeleteUserDialog(row);
    }else if(action === "RRCS"){
      $scope.openResendRegistrationCodeSMS(row, rowActions);
    }else if(action === "RRPC"){
      $scope.openResendResetPasswordCodeSMS(row, rowActions);
    }else if(action === "RCMCS"){
	  $scope.openResendChangeMobileCodeSMS(row, rowActions);
    }
  };

  $scope.openChangeStatusDialog = function(user) {
    var modalInstance = $modal.open({
        templateUrl: "/shared/views/change-status-dialog.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          }
        },
        controller: ['$scope', 'mainService', function($scope, mainService) {
          $scope.user = user;
          //global util module outside of angular.
          // Translation data
          $scope.t = mainService.UserReference.TranslationMap;
          $scope.cancel = function() {
            /*
            // User cancelled the change, revert the switch control's value. Use the intermediate value to
            // avoid infinite watch.
            $scope.form.StatusSwitchValue = undefined;
            $scope.form.StatusSwitchValue = oldStatus;
            */
            $scope.$dismiss();
          };

          $scope.confirm = function() {
            /*
            // Change the model value and submit the change
            // $scope.form.StatusNameInvariant = newStatus;
            */
           
            var newStatus, newStatusId;
            if (user.StatusNameInvariant === 'Active') {
              newStatus = 'Inactive';
              newStatusId = CONSTANTS.STATUS.INACTIVE;
            }
            else if (user.StatusNameInvariant === 'Inactive') {
              newStatus = 'Active';
              newStatusId = CONSTANTS.STATUS.ACTIVE;
            }
            //set confirm btn disabled;
            $scope.isConfirmBtnDisabled=true;
            var hpromise = $http.post($rootScope.servicePrefix+'/user/status/change', {
              "UserKey": user.UserKey,
              'Status': newStatus,
              MerchantId: user.MerchantId
            });
            hpromise.then(function(result) {
              $scope.$close({user: user, newStatus: newStatus, newStatusId: newStatusId});
            }, function(errObj, status, headers, config) {
              $scope.messages = [
                {msg: 'Failed to change status. Please try again later.', type: 'danger'}
              ];
            }).finally(function(){
              //set confirm btn enabled;
              $scope.isConfirmBtnDisabled=false;
            });
          };

          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
    });
    modalInstance.result.then(function(data) {
      // Update the affected row's Status field
      _.each($scope.allUserData, function(row) {
        if (row.UserKey === data.user.UserKey) {
          row.StatusNameInvariant = data.newStatus;
          row.StatusId = data.newStatusId;
        }
      });
    });
  };

  $scope.openResetPasswordDialog = function(user) {
    $modal.open({
        templateUrl: "/shared/views/reset-password-dialog.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          }
        },
        controller: ['$scope', 'mainService', function($scope, mainService) {
          $scope.user = user;
          //global util module outside of angular.
          // Translation data
          $scope.t = mainService.UserReference.TranslationMap;
          $scope.cancel = function() {
            $scope.$dismiss();
          };

          $scope.confirm = function() {
            //set confirm btn disabled
            $scope.isConfirmBtnDisabled = true;
            var hpromise = $http.post($rootScope.servicePrefix+'/user/password/change', {
              "UserKey": user.UserKey,
              MerchantId: user.MerchantId
            });
            hpromise.then(function(result) {
              $scope.$close(true);
              user.StatusId=CONSTANTS.STATUS.PENDING;
              user.StatusNameInvariant='Pending';
            }, function(errObj, status, headers, config) {
              $scope.messages = [
                {msg: 'Failed to reset password. Please try again later.', type: 'danger'}
              ];
            }).finally(function(){
              //set confirm btn enabled
              $scope.isConfirmBtnDisabled = false;
            });
          };

          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
    })/*.result.finally(function() {
        $state.go('userEdit');
    });*/
  };


  $scope.openResendChangeMobileCodeSMS = function(user, action){

  		$modal.open({
        templateUrl: "/shared/views/resend-change-mobile-code-sms.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          }
        },
        controller: ['$scope', 'mainService', function($scope, mainService) {
          $scope.user = user;
          //global util module outside of angular.
          $scope.mobile = $scope.user.MobileCode + $scope.user.MobileNumber;
          // Translation data
          $scope.t = mainService.UserReference.TranslationMap;
          $scope.cancel = function() {
          	action.value = "";
            $scope.$dismiss();
          };

          $scope.confirm = function() {
     
            $scope.isConfirmBtnDisabled=true;
            var hpromise = $http.post($rootScope.servicePrefix+'/auth/code/resend', {
            	UserKey : $scope.mobile
            });
            hpromise.then(function(result) {
              action.value = "";
              $scope.$close();
            }, function(errObj, status, headers, config) {
              $scope.messages = [
                {msg: 'Failed to send request. Please try again later.', type: 'danger'}
              ];
            }).finally(function(){
              //set confirm btn enabled;
              $scope.isConfirmBtnDisabled=false;
            });

          };

          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
    });

  };

   $scope.openResendResetPasswordCodeSMS = function(user, action) {
   	
  	$modal.open({
        templateUrl: "/shared/views/resend-reset-password-code.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          }
        },
        controller: ['$scope', 'mainService', function($scope, mainService) {
          $scope.user = user;
          //global util module outside of angular.
           $scope.mobile = $scope.user.MobileCode + $scope.user.MobileNumber;
          // Translation data
          $scope.t = mainService.UserReference.TranslationMap;
          $scope.cancel = function() {
          	action.value = "";
            $scope.$dismiss();
          };

          $scope.confirm = function() {
            $scope.isConfirmBtnDisabled=true;
            var hpromise = $http.post($rootScope.servicePrefix+'/auth/password/forgot', {
            	UserKey : $scope.mobile
            });
            hpromise.then(function(result) {
              action.value = "";
              $scope.$close();
            }, function(errObj, status, headers, config) {
              $scope.messages = [
                {msg: 'Failed to send request. Please try again later.', type: 'danger'}
              ];
            }).finally(function(){
              //set confirm btn enabled;
              $scope.isConfirmBtnDisabled=false;
            });

          };

          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
    });


  };


  $scope.openResendRegistrationCodeSMS = function(user, action) {
   	
  	$modal.open({
        templateUrl: "/shared/views/resend-registration-code-sms.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          }
        },
        controller: ['$scope', 'mainService', function($scope, mainService) {
          $scope.user = user;
          //global util module outside of angular.
           $scope.mobile = $scope.user.MobileCode + $scope.user.MobileNumber;
          // Translation data
          $scope.t = mainService.UserReference.TranslationMap;
          $scope.cancel = function() {
          	action.value = "";
            $scope.$dismiss();
          };

          $scope.confirm = function() {
            $scope.isConfirmBtnDisabled=true;
            var hpromise = $http.post($rootScope.servicePrefix+'/auth/password/forgot', {
            	UserKey : $scope.mobile
            });
            hpromise.then(function(result) {
              action.value = "";
              $scope.$close();
            }, function(errObj, status, headers, config) {
              $scope.messages = [
                {msg: 'Failed to send request. Please try again later.', type: 'danger'}
              ];
            }).finally(function(){
              //set confirm btn enabled;
              $scope.isConfirmBtnDisabled=false;
            });

          };

          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
    });


  };





  $scope.openResendLinkDialog = function(options) {
    $modal.open({
        templateUrl: options.templateUrl,
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          }
        },
        controller: ['$scope', 'mainService',function($scope, mainService) {
          //global util module outside of angular.
          // Translation data
          $scope.t = mainService.UserReference.TranslationMap;
          $scope.user = options.user;
          $scope.cancel = function() {
            $scope.$dismiss();
          };

          $scope.confirm = function() {
            //set confirm btn disabled
            $scope.isConfirmBtnDisabled = true;
            var hpromise = $http.post($rootScope.servicePrefix+'/user/resend/link', {
              "UserKey": options.user.UserKey,
              "Link": options.linkType,
              "MerchantId": options.user.MerchantId
            });
            hpromise.then(function(result) {
              $scope.$close(true);
            }, function(errObj, status, headers, config) {
              $scope.messages= [
                {msg: options.errorMsg, type: 'danger'}
              ];
            }).finally(function(){
              //set confirm btn enable
              $scope.isConfirmBtnDisabled = false;
            });
          };

          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
    })/*.result.finally(function() {
        $state.go('userEdit');
    });*/
  };

  $scope.openDeleteUserDialog = function(user) {
    var modalInstance = $modal.open({
        templateUrl: "/shared/views/confirm-delete-user-dialog.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          }
        },
        controller: ['$scope', function($scope) {
          $scope.user = user;
          //global util module outside of angular.
          // Translation data
          $scope.t = mainService.UserReference.TranslationMap;
          $scope.cancel = function() {
            $scope.$dismiss();
          };

          $scope.confirm = function() {
            //set confirm btn disabled
            $scope.isConfirmBtnDisabled = true;
            var hpromise = $http.post($rootScope.servicePrefix+'/user/status/change', {
              "UserKey": user.UserKey,
              'Status': 'Deleted',
              MerchantId: user.MerchantId
            });
            hpromise.then(function(result) {
              $scope.$close(true);
            }, function(errObj, status, headers, config) {
              $scope.messages = [
                {msg: 'Failed to delete user. Please try again later.', type: 'danger'}
              ];
            }).finally(function(){
              //set confirm btn enabled
              $scope.isConfirmBtnDisabled = false;
            });
          };

          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
    });
    modalInstance.result.then(function(data) {
      // Refresh the list to remove the deleted user row
      $scope.getData();
    });
  };



}


angular.module('mmApp').controller('ModalUserFormCtrl', ['$scope', '$modalInstance', 'mainService', 'generalTranslationService', 'FileUploadService', 'Http', '$rootScope', ModalUserFormCtrl]);
