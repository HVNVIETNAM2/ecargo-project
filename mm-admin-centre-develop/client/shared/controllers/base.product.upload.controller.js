'use strict';

function BaseProductUploadCtrl($rootScope, $scope, $q, $location, $anchorScroll, $window, $modal, $log, $http, $timeout,
  Upload, mainService, $state, $stateParams, FileUploadService, CONSTANTS) {

  $rootScope.settings = $state.current.settings;
  $scope.mainService = mainService;
  $scope.t = mainService.UserReference.TranslationMap;
  $scope.files = [];
  $scope.loading = false;

  var queue = $q.when();
  $scope.loading = false;

  $scope.handleUploadError = function (file) {
    return function(err) {
      if (err && angular.isObject(err.data)) {
        file.error = err.data;
      } else {
        //In case of an error response (http code >= 400) the custom error message returned from the server may not be
        //available. For some error codes flash just provide a generic error message and ignores the response text. #310
        //https://github.com/danialfarid/ng-file-upload/issues/310
        //https://github.com/danialfarid/ng-file-upload
        //temp workaround for IE or other browser does not support html5 upload, i.e. use flash upload, we show a generic error
        //ALSO for unexpected error e.g. server down, ngix server error
        file.error = {
          AppCode: 'MSG_ERR_IMAGE_UPLOAD'
        };
      }
    }
  };

  $scope.upload = function(file) {
    if (file.$error) {
      file.error = {};
      file.progress = 100;
      return;
    }
    if (file.name && file.type !== 'application/zip') {
      var matches = file.name.match(CONSTANTS.NAME_REGX.PROD_IMG);
      if (!matches) {
        file.$error = 'fileName';
        file.error = {};
        file.progress = 100;
        return;
      }
    }

    //validate size
    if ((file.type === 'application/zip' && file.size > CONSTANTS.FILE_UPLOAD_SIZE_BYTES.PROD_ZIP) ||
        file.size > CONSTANTS.FILE_UPLOAD_SIZE_BYTES.PROD_IMG) {
      file.$error = 'maxSize';
      file.error = {};
      file.progress = 100;
    }


    if (!file || file.upload || file.error || file.result) return;
    file.upload = true;

    queue = queue.then(function() {
        $scope.loading = true;

        if (file.type === 'application/zip') {
          return Upload.upload({
              url: '/api/productimage/upload/archive?AccessToken=Bearer '+$rootScope.AuthToken, //add accessToken as param to support IE9 since IE9 use flash to upload files. JWT will put accessToken in the header, but flash would not put anything JWT defined in header.
              headers: {
                'Content-Type': 'multipart/form-data'
              },
              method: 'POST',
              file: file,
              data: {MerchantId: $scope.MerchantId, existsAction: $scope.form.existsAction}
            })
            .then(function(res) {
              var idx = $scope.files.indexOf(file);
              Array.prototype.splice.apply(
                $scope.files, [idx, 1].concat(res.data)
              );
            }, $scope.handleUploadError(file), function(evt) {
              file.progress = parseInt(100 * evt.loaded / evt.total, 10);
            });
        } else {
          return Upload.upload({
              url: '/api/productimage/upload?AccessToken=Bearer '+$rootScope.AuthToken, //add accessToken as param to support IE9 since IE9 use flash to upload files. JWT will put accessToken in the header, but flash would not put anything JWT defined in header.
              headers: {
                'Content-Type': 'multipart/form-data'
              },
              method: 'POST',
              file: file,
              data: {MerchantId: $scope.MerchantId, existsAction: $scope.form.existsAction}
            })
            .then(function(res) {
              file.result = res.data;
            }, $scope.handleUploadError(file), function(evt) {
              file.progress = parseInt(100 * evt.loaded / evt.total, 10);
            });
        }
      })
      .then(function() {
        $scope.loading = false;
      });
  };

  $scope.remove = function($index) {
    var file = $scope.files[$index];
    $scope.loading = true;
    // todo remove sku and StyleImage record then splice array
    if (!(file && file.result)) {
      $scope.loading = false;
      return;
    }
    return $http.post('/api/productimage/delete', {
      StyleImageId: file.result.StyleImage.StyleImageId,
      MerchantId: $scope.MerchantId
    }).then(function() {
      $scope.loading = false;
      $scope.files.splice($index, 1);
    }, function() {
      $scope.loading = false;
    });
  };

  $scope.validFilter = function(file) {
    return file && !file.$error;
  };
  $scope.successFilter = function(file) {
    return file && file.result && !file.error;
  };
  $scope.failedFilter = function(file) {
    if (file.$error === 'maxSize') {
      file.error.Message = Util.format($scope.t.MSG_ERR_GEN_IMG_SIZE, [(file.type === 'application/zip' ? CONSTANTS.FILE_UPLOAD_SIZE.ZIP : CONSTANTS.FILE_UPLOAD_SIZE.IMAGE)]);
    } else if (file.$error === 'pattern') {
      file.error.Message = $scope.t.MSG_ERR_PRODUCT_IMAGE_FORMAT.replace('{CONST_PRODUCT_IMG_FILE_FORMAT}', CONSTANTS.FILE_FORMAT.PROD_IMG);
    } else if (file.$error === 'fileName') {
      file.error.Message = $scope.t.ERR_MSG_PROD_FILE_NAME;
    }
    return file && file.error && !file.result;
  };

  $scope.form = {
    existsAction: 'replace'
  };

  $scope.scrollTo = function(id) {
    $location.hash(id);
    $anchorScroll();
  };

  $scope.filePreview = function(file) {
    if (file.type === 'application/zip') {
      return 'assets/images/icon_image_zip.png';
    } else if (file.result) {
      return '/api/resizer/view?s=200&b=productimages&key=' + file.result.StyleImage.ProductImage;
    } else if (file.error || file.$error) {
      return 'assets/images/icon_image_fail.png';
    } else {
      return 'assets/images/icon_image.png';
    }
  };

  $scope.options = {
    existsAction: [{
      label: mainService.UserReference.TranslationMap.RBTN_IMG_IGNORE,
      value: 'ignore'
    }, {
      label: mainService.UserReference.TranslationMap.RBTN_IMG_OVERWRITE,
      value: 'replace'
    }]
  };
}
