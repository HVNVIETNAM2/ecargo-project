'use strict';

function BaseProductSaveCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $q, $loading, generalTranslationService, geoService, $anchorScroll, DialogService) {

  //global util module outside of angular.
  $rootScope.settings = $state.current.settings;
  $scope.mainService = mainService;
  $scope.generalTranslationService = generalTranslationService;
  $scope.loading = false;
  $scope.Util = Util;
  //define style properties
  $scope.isShowAddMerchanCategory = true;
  $scope.isCategoryChanged = false; // update product use only
  $scope.isImageChanged = false; // update product use only
  $scope.isStyleCodeChanged = false; // update product use only
  $scope.originalStyleCode; // update product use only
  $scope.style = {}; //product object;
  $scope.style.SkuList = [];
  $scope.DeleteSkuIdList = []; // only edit product, this list will be pass to the server side.
  $scope.style.ColorList = [];
  $scope.style.FeaturedImageList = []; //define FeatureImageList;
  $scope.style.DescriptionImageList = []; //define DescriptionImageList;
  $scope.style.ColorImageListMap = {}; //color image list map;
  $scope.LanguageCode = $rootScope.User.CultureCode;
  var stylecode = $stateParams.stylecode;

  //variable for Display
  $scope.Brands = []; //for brand selection display
  $scope.geoCountries = []; //for country selection display
  $scope.LaunchYears = []; //for year of lunch selection display
  $scope.Seasons = []; //for season selection display
  $scope.Badges = []; //for badge selection display

  $scope.init = function() {
    //messages
    if ($state.params.msg && $state.params.type) {
      $scope.messages = [{
        msg: $scope.t[$state.params.msg] || $state.params.msg,
        type: $state.params.type
      }];
    }

    //get product
    if ($scope.saveMode == false)
      $scope.getProduct($rootScope.User.CultureCode);

    //init data from server
    //1.retrieve brands which define in merchant profile, not all brands from brands table.
    var brandpromise = $http.get($rootScope.servicePrefix + '/brand/list', {
      params: {
        cc: $rootScope.User.CultureCode,
        merchantid: $scope.merchantId
      }
    });
    brandpromise.then(function(response) {
      $scope.Brands = response.data;
      //if Brands length is one, then give the brands[0] to brand selection as default value.
      if($scope.Brands.length==1){
        $scope.style.BrandId = $scope.Brands[0].BrandId;
      }
    });

    //2.get geo countries
    $scope.geoCountries = geoService.countries;

    //3.init year lunch
    var initYear = 1970;
    $scope.LaunchYears.push({key:0, value:'---'});
    for (var i = 0; i <= 100; i++) {
      $scope.LaunchYears.push({key:initYear, value:initYear});
      initYear++;
    }

    //4.init season
    $scope.Seasons = $scope.generalTranslationService.UserReference.SeasonList; //for season selection display

    //5.init badge
    $scope.Badges = $scope.generalTranslationService.UserReference.BadgeList; //for badge selection display

    //6. init and define product category list
    $scope.productCategoryList = [{
      CategoryType: $scope.t.LB_PRODUCT_PRIMARY_CATEGORY,
      CategoryTypeTitle: $scope.t.LB_PROD_SELECT_PRIM_CAT,
      CategoryName: '',
      CategoryId: '',
      Priority: 0
    }];
    //if($scope.saveMode){
    //  $scope.productCategoryList.push({
    //    CategoryType: $scope.t.LB_PRODUCT_MERCHANDISE_CATEGORY,
    //    CategoryTypeTitle: $scope.t.LB_PROD_SELECT_MER_CAT,
    //    CategoryName: '',
    //    CategoryId: '',
    //    Priority: 1
    //  });
    //}

  };

  $scope.addMerchandizeCategory = function(){
    $scope.isCategoryChanged = true;
    var lastCategory = $scope.productCategoryList[$scope.productCategoryList.length-1];
    if(lastCategory.CategoryId==''&& $scope.productCategoryList.length>1){
      DialogService.alert($scope.t.LB_ALERT, $scope.t.LB_MERCHANDIZE_CATEGORY_NIL);
      return;
    }
    $scope.productCategoryList.push({
      CategoryType: $scope.t.LB_PRODUCT_MERCHANDISE_CATEGORY,
      CategoryTypeTitle: $scope.t.LB_PROD_SELECT_MER_CAT,
      CategoryName: '',
      CategoryId: '',
      Priority: $scope.productCategoryList.length
    });
    if($scope.productCategoryList.length===6){
      $scope.isShowAddMerchanCategory = false;
    }
  }

  $scope.getProduct = function(cultureCode) {
    if ($scope.style.StyleCode && $scope.style.StyleCode !== stylecode) {
      $state.go('productEdit', {stylecode: $scope.style.StyleCode, msg: 'MSG_PRODUCT_EDITED', type:'success'});
      return;
    }

    $loading.start('loading');
    //1.get style view
    var hpromise = $http.get($rootScope.servicePrefix + '/product/style/view', {
      params: {
        merchantid: $scope.merchantId,
        cc: cultureCode,
        stylecode: $scope.style.StyleCode || stylecode
      }
    });
    hpromise.then(function(result) {
      $scope.style = result.data;
      $scope.originalStyleCode = $scope.style.StyleCode;
      if ($scope.style.ColorImageList) {
        $scope.style.ColorImageListMap = _.groupBy($scope.style.ColorImageList, function(item) {
          return item.ColorKey;
        });
      }
      initAfterRetriveProduct();
      sortColorList();
      $loading.finish('loading');
    }, function(errObj, status, headers, config) {
        if (window.console) {
            console.log(errObj, "Failed to retrieve product view. Please try again later.");
        }
      $scope.messages = [{
        msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
        type: 'danger'
      }];
      $loading.finish('loading');
    });

    function initAfterRetriveProduct() {
      $scope.style.CategoryPriorityList = _.sortBy($scope.style.CategoryPriorityList, function(category){
        return category.Priority;
      });

      $scope.productCategoryList = [{
        CategoryType: $scope.t.LB_PRODUCT_PRIMARY_CATEGORY,
        CategoryTypeTitle: $scope.t.LB_PROD_SELECT_PRIM_CAT,
        CategoryName: '',
        CategoryId: '',
        Priority: 0
      }];

      //since the backend api will return the whole path for subcategory, so the operation below is to remove the parent categories.
      var tmpPrimaryCatePathList = _.filter($scope.style.CategoryPriorityList, function(n){
        return n.Priority==0;
      });

      if(tmpPrimaryCatePathList.length>1){
        var tmpPrimaryCate = tmpPrimaryCatePathList[tmpPrimaryCatePathList.length-1];
        _.remove($scope.style.CategoryPriorityList, function(n){
          return n.Priority==0;
        });
        $scope.style.CategoryPriorityList.push(tmpPrimaryCate);
        $scope.style.CategoryPriorityList = _.sortBy($scope.style.CategoryPriorityList, function(n){
          return n.Priority;
        });
      }

      $scope.style.CategoryPriorityList.forEach(function(category,$index) {
        if($index==0){
          var productCategory = _.find($scope.productCategoryList, function(n){
            return category.Priority == n.Priority;
          });
          if(productCategory) {
            productCategory.CategoryName = category.CategoryName;
            productCategory.CategoryId = category.CategoryId;
            productCategory.Priority = category.Priority;
          }else{
            if (window.console) {
                console.log("category list api return wrong data, please contact developer.");
            }
          }
        }else{
          $scope.productCategoryList.push({
            CategoryType: $scope.t.LB_PRODUCT_MERCHANDISE_CATEGORY,
            CategoryTypeTitle: $scope.t.LB_PROD_SELECT_MER_CAT,
            CategoryName: category.CategoryName,
            CategoryId: category.CategoryId,
            Priority: category.Priority
          });
        }
      });
      //if($scope.productCategoryList.length===1){
      //  $scope.productCategoryList.push({
      //    CategoryType: $scope.t.LB_PRODUCT_MERCHANDISE_CATEGORY,
      //    CategoryTypeTitle: $scope.t.LB_PROD_SELECT_MER_CAT,
      //    CategoryName: '',
      //    CategoryId: '',
      //    Priority: 1
      //  });
      //}
    }

    function sortColorList(){
      var colorList = [];
      var skuColorIdList = $scope.style.SkuList.forEach(function(n){
        var ColorKey = n.ColorKey;
        var color = _.find($scope.style.ColorList, function(color){
          return color.ColorKey == ColorKey;
        });
        if(color&&colorList.indexOf(color)==-1){
          colorList.push(color);
        }
      });
      $scope.style.ColorList = colorList;
    }
  };

  //build the category text by category list
  $scope.buildCategoryText = function(CategoryPathList) {
    var CategoryPathNameList = _.map(CategoryPathList, function(n) {
      return n.CategoryName;
    });
    var CategoryPathNamesStr = CategoryPathNameList.join(' > ');
    return CategoryPathNamesStr;
  };

  $scope.changeProductLangeuage = function(languageCode) {
    $scope.getProduct(languageCode);
  };

  $scope.back = function() {
    $state.go('productList', {
      merchantId: $scope.merchantId
    });
  };

  $scope.openChangeStatusDialog = function(style) {
    $modal.open({
      templateUrl: "/shared/views/change-product-status.html",
      backdrop: 'static',
      resolve: {
        mainService: function() {
          return mainService;
        },
        style: function() {
          return style;
        }
      },
      controller: "productChangeStatusCtrl",
    });
  };

  $scope.openCreateSkuDialog = function(style) {
    $modal.open({
      templateUrl: "/shared/views/product-create-sku.html",
      backdrop: 'static',
      resolve: {
        mainService: function() {
          return mainService;
        },
        generalTranslationService: function() {
          return generalTranslationService;
        },
        geoService: function(geoService) {
          return geoService;
        },
        style: function() {
          return style;
        }
      },
      controller: "productCreateSkuCtrl",
      windowClass: 'large-Modal'
    });
  };

  $scope.resetCategory = function(category){
    $scope.isCategoryChanged = true;
    _.remove($scope.productCategoryList, function(n) {
      return category.CategoryId == n.CategoryId;
    });
    _.forEach($scope.productCategoryList, function(n, $index){
      n.Priority = $index;
    });
    if($scope.productCategoryList.length<6){
      $scope.isShowAddMerchanCategory = true;
    }
  };

  var categoryData;
  $scope.openCategoryDialog = function(category) {
    $scope.isCategoryChanged = true;
    var productCategoryList = $scope.productCategoryList;
    $modal.open({
      templateUrl: "/shared/views/product-category.html",
      backdrop: 'static',
      resolve: {
        categoryData: function() {
          if (categoryData) return categoryData; // cached
          return $http.get($rootScope.servicePrefix + '/category/list', {
            params: {
              cc: $rootScope.language
            }
          }).then(function(result) {
            categoryData = result.data.slice(1);
            _.remove(categoryData, function(n){
              return n.IsMerchCanSelect==0;
            });
            return categoryData;
          });
        }
      },
      controller: ['$scope', 'categoryData', function($scope, data) {
        $scope.t = mainService.UserReference.TranslationMap;
        $scope.data = data;
        $scope.category = category;
        $scope.selected = null;
        $scope.cancel = function() {
          $scope.$dismiss();
        };
        $scope.select = function(node) {
          $scope.selected = node;
        }
        $scope.toggle = function(node) {
          node.expanded = !node.expanded;
        }
        $scope.reset = function() {
          category.CategoryId = '';
          category.CategoryName = '';
          $scope.$close();
        }
        $scope.confirm = function() {
          // ignore if no selected;
          if (!$scope.selected) return $scope.cancel();

          var isExists = _.findIndex(productCategoryList, function(item) {
            return item.CategoryId === $scope.selected.CategoryId;
          }) > -1;
          // ignore duplicate
          if (isExists) return $scope.cancel();

          category.CategoryName = $scope.selected.CategoryName;
          category.CategoryId = $scope.selected.CategoryId;
          $scope.$close($scope.selected);
        }
      }]
    });
  };

  //delete sku and also check if need to delete related color images
  $scope.deleteSku = function(skuIndex) {
    DialogService.confirm($scope.t.LB_DELETE, $scope.t.LB_AC_DELETE_SKU_CONFIRM, doDeleteSku);

    function doDeleteSku() {
      var sku = $scope.style.SkuList[skuIndex];
      var skuColorKey = sku.ColorKey;
      if (!$scope.saveMode && sku.SkuId) { // only edit product will record the deleted sku
        $scope.DeleteSkuIdList.push(sku.SkuId);
      }
      $scope.style.SkuList.splice(skuIndex, 1);
      //see if colorkey in sku which has been deleted existed in other skus'.
      var tmpSku = _.find($scope.style.SkuList, function(n) {
        return n.ColorKey == skuColorKey;
      });
      if (!tmpSku) { //delete color image
        $scope.isImageChanged = true;
        delete $scope.style.ColorImageListMap[skuColorKey];
        //remove from colorlist
        _.remove($scope.style.ColorList, function(color) {
          return skuColorKey == color.ColorKey;
        });
      }
    };

  };

  $scope.deleteImage = function(ImageKey, imageType, colorKey) {
    $scope.isImageChanged = true;
    if (imageType == 'Feature') { //featrue image
      $scope.validationForm.FeaturedImage.$error = {};
      _.remove($scope.style.FeaturedImageList, function(n) {
        return (n.ImageKey === ImageKey);
      });
    } else if (imageType == 'Desc') { //desc image
      $scope.validationForm.DescriptionImage.$error = {};
      _.remove($scope.style.DescriptionImageList, function(n) {
        return (n.ImageKey === ImageKey);
      });
    } else if (imageType == 'Color') { //color image
      $scope.validationForm[colorKey].$error = {};
      _.remove($scope.style.ColorImageListMap[colorKey], function(n) {
        return n.ImageKey == ImageKey;
      });
    } else {
        if (window.console) {
            console.log('image type must be specified');
        }
      return;
    }
  }

  $scope.uploadImage = function(files, imageType, colorKey) {
    $scope.isImageChanged = true;
    //1.calculate position and build up empty color image list if there is.
    //2.calculate the the number of files which can be uploaded. because for each type of image section, there is a limit number, like for Feature image maxium is 5.
    var position;
    var numOfUpload;
    if (imageType == 'Feature') { //featrue image
      position = $scope.style.FeaturedImageList.length;
      numOfUpload = 5 - position;
    } else if (imageType == 'Desc') { //desc image
      position = $scope.style.DescriptionImageList.length;
      numOfUpload = 15 - position;
    } else if (imageType == 'Color') { //color image
      var colorImageList = $scope.style.ColorImageListMap[colorKey];
      if (colorImageList) {
        position = colorImageList.length;
      } else {
        $scope.style.ColorImageListMap[colorKey] = [];
        position = 0;
      }
      numOfUpload = 5 - position;
    } else {
        if (window.console) {
            console.log('image type must be specified');
        }
      return;
    }

    //3. upload image
    if (!files || files.$error || files.upload) return;
    files.forEach(function(file, index) {
      if (numOfUpload <= 0) {
        if (window.console) {
            console.log('no more file allowed be upload.');
        }
        return;
      }
      file.upload = {};
      Upload.upload({
        url: $rootScope.servicePrefix + '/image/upload?b=productimages&AccessToken=Bearer ' + $rootScope.AuthToken, //add accessToken as param to support IE9 since IE9 use flash to upload files. JWT will put accessToken in the header, but flash would not put anything JWT defined in header.
        //fields: fields,
        file: file
      }).error(function(data, status, headers, config) {
        if (window.console) {
            console.log("update file failed!");
        }
      }).success(function(data, status, headers, config) {
        var resObj = data;
        if (imageType == 'Feature') {
          $scope.style.FeaturedImageList.push({
            ImageKey: resObj.ImageKey,
            Position: ++index
          });
          $scope.style.FeaturedImageList = _.sortBy($scope.style.FeaturedImageList, function(n) {
            return n.Position;
          });
        } else if (imageType == 'Desc') {
          $scope.style.DescriptionImageList.push({
            ImageKey: resObj.ImageKey,
            Position: ++index
          });
          $scope.style.DescriptionImageList = _.sortBy($scope.style.DescriptionImageList, function(n) {
            return n.Position;
          });
        } else if (imageType == 'Color') {
          $scope.style.ColorImageListMap[colorKey].push({
            ImageKey: resObj.ImageKey,
            Position: ++index
          });
          $scope.style.ColorImageListMap[colorKey] = _.sortBy($scope.style.ColorImageListMap[colorKey], function(n) {
            return n.Position;
          });
        } else {
            if (window.console) {
                console.log('image type must be specified');
            }
          return;
        }

      });
      numOfUpload--;
    });

  };

  $scope.onDrop = function(target, source, imageType, colorKey) {
    $scope.isImageChanged = true;
    if (target.ImageKey === source.ImageKey)
      return;
    var imageList;
    var orignalList; //in case reposition failed in server side, it will restroe to the old position
    if (imageType == 'Feature') { //featrue image
      orignalList = $scope.style.FeaturedImageList;
      imageList = $scope.style.FeaturedImageList;
      imageList = buildPosition(target, source, imageList);
      $scope.style.FeaturedImageList = imageList;
    } else if (imageType == 'Desc') { //desc image
      orignalList = $scope.style.DescriptionImageList;
      imageList = $scope.style.DescriptionImageList;
      imageList = buildPosition(target, source, imageList);
      $scope.style.DescriptionImageList = imageList;
    } else if (imageType == 'Color') { //color image
      orignalList = $scope.style.ColorImageListMap[colorKey];
      imageList = $scope.style.ColorImageListMap[colorKey];
      imageList = buildPosition(target, source, imageList);
      $scope.style.ColorImageListMap[colorKey] = imageList;
    }

    //inner function for build positioned list
    function buildPosition(target, source, imageList) {
      var tmpTargetPosition = target.Position;
      var tmpSourcePosition = source.Position;
      //if swap is forward, target position need to be the next postion.
      if (tmpSourcePosition < tmpTargetPosition) {
        tmpTargetPosition++;
      }
      //1.foreach object in the list and add 1 in position for these object which position is greater equal than targetOrigin.Position
      imageList = _.forEach(imageList, function(n) {
        if (n.Position >= tmpTargetPosition) {
          n.Position++;
        }
      });
      //2.set source object position as target's
      var sourceOrigin = _.find(imageList, function(n) {
        return n.ImageKey === source.ImageKey;
      });
      sourceOrigin.Position = tmpTargetPosition;
      //3.sort the list
      imageList = _.sortBy(imageList, function(n) {
        return n.Position;
      });
      //4.reset position for each image starting from 1;
      var startPosition = 1;
      imageList = _.forEach(imageList, function(n) {
        n.Position = startPosition++;
      });
      return imageList;
    }

  };

  $scope.customValidations = [{
    isValid: function () {
      return $scope.style.SkuList.length !== 0;
    },
    element: angular.element('input.sku-list')
  }];

  $scope.saveProduct = function(validationForm, isDraft) {
    validationForm.$submitted = true;
//    if (!validationForm.$valid || !validateCategory() || !validateSku() ) {
    if (!validationForm.$valid || !validateCategory() || !$scope.style.SkuList.length) {
        if (window.console) {
            console.log("product save failed due to the invalid input.");
        }

      return false;
    }
    $scope.isSaveBtnDisabled = true;
    var hpromise;
    $scope.style.CategoryPriorityList = $scope.productCategoryList; //set category selection list to style object.
    if ($scope.saveMode) {
      hpromise = $http.post($rootScope.servicePrefix + '/product/create', {
        style: $scope.style,
        MerchantId: $scope.merchantId,
        isDraft: isDraft
      });
    } else {
      if($scope.originalStyleCode != $scope.style.StyleCode){
        $scope.isStyleCodeChanged = true;
      }
      hpromise = $http.post($rootScope.servicePrefix + '/product/update', {
        style: $scope.style,
        MerchantId: $scope.merchantId,
        deleteSkuIdList: $scope.DeleteSkuIdList,
        isCategoryChanged: $scope.isCategoryChanged,
        isImageChanged: $scope.isImageChanged,
        isStyleCodeChanged: $scope.isStyleCodeChanged,
        originalStyleCode: $scope.originalStyleCode
      });
    }
    hpromise.then(function(result) {
        // Update product after saved
        if (result.status === 200) {
            $scope.getProduct($rootScope.User.CultureCode);
        }
        
      if ($scope.saveMode) {
        if(!$scope.isCreateOther){
          $state.go('productList', {
            merchantId: $scope.merchantId
          });
        }else{
          $scope.isSaveBtnDisabled = false;
          $scope.messages = [{
            msg: $scope.t.MSG_PRODUCT_CREATED,
            type: 'success'
          }];
          $scope.createOther(validationForm);
          //scroll to the top of page
          $location.hash('page_top');
          $anchorScroll();
        }
        $scope.isSaveBtnDisabled = false;
      } else {
        $scope.messages = [{
          msg: $scope.t.MSG_PRODUCT_EDITED,
          type: 'success'
        }];
        $scope.isSaveBtnDisabled = false;
        //scroll to the top of page
        $location.hash('page_top');
        $anchorScroll();
      }
    }, function(errObj, status, headers, config) {
      var errorCodeMsg = ($scope.t[errObj.data.AppCode] === undefined) ? (errObj.data.AppCode) : ($scope.t[errObj.data.AppCode]);
      var appCode = errObj.data.AppCode;
      var href = '';
      if (appCode === 'MSG_ERR_PRODUCT_BARCODE_UNIQUE_WITHIN_MERCHANT') {
        href = 'skuList';
      } else if (appCode === 'MSG_ERR_PRODUCT_SKUCODE_UNIQUE_WITHIN_MERCHANT') {
        href = 'skuList';
      } else if (appCode === 'MSG_ERR_PRODUCT_STYLECODE_UNIQUE') {
        href = 'styleCode';
      } else if (appCode === 'MSG_ERR_PROD_NAME_DUPLICATE') {
        href = 'productName';
      }

      $scope.messages = [{
        msg: errorCodeMsg,
        type: 'danger',
        hash: href
      }];
      $scope.isSaveBtnDisabled = false;
      //scroll to the top of page
      $location.hash('page_top');
      $anchorScroll();
    });

    $scope.jumpToSection = function (hash) {
      $location.hash(hash);
      $anchorScroll();
    };

// Use angular form validation instead
//    function validateSku() {
//      //1. check if sku list has items
//      if($scope.style.SkuList.length===0){
//        $scope.messages = [{
//          msg: $scope.t.MSG_ERR_SKU_LIST_NIL,
//          type: 'danger'
//        }];
//        return false;
//      }
//
//      //2. check if fields in sku’item has been filled
//      var isValidateSuccess = true;
//      var errorMsg = '';
//      $scope.style.SkuList.forEach(function(sku) {
//        if (sku.SkuCode === '' || sku.SkuCode === null) {
//          isValidateSuccess = false;
//          errorMsg = $scope.t.MSG_ERR_SKU_SKUCODE_NIL;
//        }else if (sku.Bar === '' || sku.Bar === null) {
//          isValidateSuccess = false;
//          errorMsg = $scope.t.MSG_ERR_SKU_BARCODE_NIL;
//        }else if (sku.QtySafetyThreshold === '' || sku.QtySafetyThreshold === null) {
//          isValidateSuccess = false;
//          errorMsg = $scope.t.MSG_ERR_SKU_QTYATS_NIL;
//        }
//      });
//      if (!isValidateSuccess) {
//        $scope.messages = [{
//          msg: errorMsg,
//          type: 'danger'
//        }];
//        return false;
//      }
//
//      //3. check if sku code is not unique in the sku list and check is sku code is valid or not.
//      var tmpSkuCodeArray = [];
//      var skuRgExp = /^[0-9]{1,12}$/i;
//      var errorMsg = "";
//      $scope.style.SkuList.forEach(function(sku){
//        if(!isValidateSuccess){
//          return;
//        }
//        if(!sku.SkuCode.match(skuRgExp)){
//          isValidateSuccess = false;
//          errorMsg = $scope.t.MSG_ERR_SKU_INVALID;
//        }
//        if(tmpSkuCodeArray.indexOf(sku.SkuCode)==-1){
//          tmpSkuCodeArray.push(sku.SkuCode);
//        }else{
//          isValidateSuccess = false;
//          errorMsg = $scope.t.MSG_ERR_PRODUCT_SKUCODE_UNIQUE_WITHIN_MERCHANT;
//        }
//      });
//      if (!isValidateSuccess) {
//        $scope.messages = [{
//          msg: errorMsg,
//          type: 'danger'
//        }];
//        return false;
//      }
//
//      //4. check if bar code is not unique in the sku list and check bar code is invalid or not.
//      var tmpBarCodeArray = [];
//      var barRgExp = /^[0-9]{1,12}$/i;
//      $scope.style.SkuList.forEach(function(sku){
//        if(!isValidateSuccess){
//          return;
//        }
//        if(!sku.Bar.match(barRgExp)){
//          isValidateSuccess = false;
//          errorMsg = $scope.t.MSG_ERR_BAR_INVALID;
//        }
//        if(tmpBarCodeArray.indexOf(sku.Bar)==-1){
//          tmpBarCodeArray.push(sku.Bar);
//        }else{
//          isValidateSuccess = false;
//          errorMsg = $scope.t.MSG_ERR_PRODUCT_BARCODE_UNIQUE_WITHIN_MERCHANT;
//        }
//      });
//      if (!isValidateSuccess) {
//        $scope.messages = [{
//          msg: errorMsg,
//          type: 'danger'
//        }];
//        return false;
//      }
//
//      return isValidateSuccess;
//    }

    function validateCategory(){
      if($scope.productCategoryList[0].CategoryId===''){
        $scope.messages = [{
          msg: $scope.t.MSG_ERR_PRIMARY_CATEGORY_NIL,
          type: 'danger'
        }];
        return false;
      }
      return true;
    }

  };

  $scope.createOther = function(validationForm){
    // primary category, country of original, brand name, year launched and season launched
    validationForm.$submitted = false;

    $scope.style.SkuNameEN = '';
    validationForm.SkuNameEN.$touched = false;
    validationForm.SkuNameEN.$untouched = true;
    $scope.style.SkuNameCHS = '';
    validationForm.SkuNameCHS.$touched = false;
    validationForm.SkuNameCHS.$untouched = true;
    $scope.style.SkuNameCHT = '';
    validationForm.SkuNameCHT.$touched = false;
    validationForm.SkuNameCHT.$untouched = true;

    $scope.style.StyleCode = '';
    validationForm.StyleCode.$touched = false;
    validationForm.StyleCode.$untouched = true;

    $scope.style.PriceSale = '';
    validationForm.PriceSale.$touched = false;
    validationForm.PriceSale.$untouched = true;
    $scope.style.PriceRetail = '';
    validationForm.PriceRetail.$touched = false;
    validationForm.PriceRetail.$untouched = true;


    var otherGeoCountryId = $scope.style.GeoCountryId;
    var otherLaunchYear  = $scope.style.LaunchYear;
    var otherSeasonId  = $scope.style.SeasonId;
    var otherBrandId  =   $scope.style.BrandId;

    $scope.style = {}; //product object;
    $scope.style.SkuList = [];
    $scope.DeleteSkuIdList = []; // only edit product, this list will be pass to the server side.
    $scope.style.ColorList = [];
    $scope.style.FeaturedImageList = []; //define FeatureImageList;
    $scope.style.DescriptionImageList = []; //define DescriptionImageList;
    $scope.style.ColorImageListMap = {}; //color image list map;

    $scope.style.GeoCountryId = otherGeoCountryId;
    $scope.style.LaunchYear = otherLaunchYear;
    $scope.style.SeasonId = otherSeasonId
    $scope.style.BrandId = otherBrandId;

  };

  $scope.closeMessage = function(index) {
    $scope.messages.splice(index, 1);
  };

  //init datetime component
  var that = this;
  this.isAvailableFromOpen = false;
  this.isAvailableToOpen = false;
  this.isSaleFromOpen = false;
  this.isSaleToOpen = false;
  this.openCalendar = function(e, date, propName) {
    e.preventDefault();
    e.stopPropagation();
    that[propName] = true;
  };

  $scope.init();
}
