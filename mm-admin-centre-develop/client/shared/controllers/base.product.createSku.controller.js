'use strict';

function BaseProductCreateSkuCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $q, $loading, generalTranslationService, geoService, style) {
  //define common util
  $scope.t = mainService.UserReference.TranslationMap;

  //define varaibles
  $scope.isNoColorSize = 0;
  $scope.SizeGroupMap = {};
  $scope.SizeIdNameMap = {};
  $scope.ChoosedSizeIdList = [];
  $scope.colors = {};
  $scope.ChoosedColorCodeList = [];

  //define ui function
  $scope.cancel = function() {
    $scope.$dismiss();
  };

  $scope.operateSize = function(sizeId) {
    if ($scope.ChoosedSizeIdList.indexOf(sizeId) === -1) {
      $scope.ChoosedSizeIdList.push(sizeId);
    } else {
      $scope.ChoosedSizeIdList.pop(sizeId);
    }
  }

  $scope.clearChoosedSizeList = function() {
    $scope.ChoosedSizeIdList = [];
  }

  $scope.operateColor = function(colorCode, isChoose) {
    if ($scope.ChoosedColorCodeList.indexOf(colorCode) === -1&&(isChoose==1)) {
      $scope.ChoosedColorCodeList.push(colorCode);
    } else if(isChoose==0){
      $scope.ChoosedColorCodeList.pop(colorCode);
    }
    $scope.skuColorForm[colorCode+'ColorNameEN'].$touched = false;
    $scope.skuColorForm[colorCode+'ColorNameEN'].$untouched = true;
    $scope.skuColorForm[colorCode+'ColorNameCHS'].$touched = false;
    $scope.skuColorForm[colorCode+'ColorNameCHS'].$untouched = true;
    $scope.skuColorForm[colorCode+'ColorNameCHT'].$touched = false;
    $scope.skuColorForm[colorCode+'ColorNameCHT'].$untouched = true;
  }

  $scope.checkInit = function(){
    $scope.messages = [];//empty error message
  }

  $scope.closeMessage = function(index) {
    $scope.messages.splice(index, 1);
  };

  $scope.generateSkus = function() {
    //1.check avaliable, at least choose one color or one size or check without color&size
    if($scope.isNoColorSize==0&&$scope.ChoosedColorCodeList.length===0&&$scope.ChoosedSizeIdList.length===0){
      $scope.messages = [{
        msg: $scope.t.MSG_ERR_SKU_COLOR_SIZE_CHOOSE,
        type: 'danger'
      }];
      return;
    }

    //2.check if new color-size pairs existed in skuList, since the color-size pair need to be unique.
    var tmpChoosedColorCodeList = [];
    var tmpChoosedSizeIdList = [];
    if($scope.isNoColorSize==0){
      if($scope.ChoosedColorCodeList.length==0){
        tmpChoosedColorCodeList.push('0');
      }else{
        tmpChoosedColorCodeList = $scope.ChoosedColorCodeList;
      }
      if($scope.ChoosedSizeIdList.length==0){
        tmpChoosedSizeIdList.push('0');
      }else{
        tmpChoosedSizeIdList = $scope.ChoosedSizeIdList;
      }
    }else{
      tmpChoosedColorCodeList.push('0');
      tmpChoosedSizeIdList.push('0');
    }
    var colorSizePair = [];
    var isExisted = false;
    var isNull = false;
    tmpChoosedColorCodeList.forEach(function(colorCode) {
      var color = $scope.colors[colorCode];
      //check is color name of 3 languages is filled.
      if(color!=undefined&&(_.trim(color.nameEN)==''|| _.trim(color.nameCHS)==''||_.trim(color.nameCHT)=='')){
        isNull=true;
      }
      //check if sku is existed
      tmpChoosedSizeIdList.forEach(function(sizeId) {
        var colorKey = (color)?color.nameEN:'';
        var tmpSku = _.find(style.SkuList, function(sku){
            if(sku.ColorKey===null){
                return (sku.ColorCode === colorCode);
            }
            else{
                return (sku.ColorKey.toLowerCase()==colorKey.toLowerCase())&&(sku.SizeId==sizeId);
            }
        });
        if(tmpSku){
          isExisted = true;
        }
      });
    });

    if(isNull){
      $scope.messages = [{
        msg: $scope.t.MSG_ERR_SKU_COLOR_NIL,
        type: 'danger'
      }];
      return;
    }
    if(isExisted){
      $scope.messages = [{
        msg: $scope.t.MSG_ERR_SKU_EXISTED,
        type: 'danger'
      }];
      return;
    }

    //2.Add Sku To SkuList
    tmpChoosedColorCodeList.forEach(function(colorCode) {
      var color = $scope.colors[colorCode];
      tmpChoosedSizeIdList.forEach(function(sizeId) {
        var sizeName = $scope.SizeIdNameMap[sizeId];
        var SkuColor;
        if ($rootScope.User.CultureCode == 'EN') {
          SkuColor = (color)?color.nameEN:'';
        } else if ($rootScope.User.CultureCode == 'CHS') {
          SkuColor = (color)?color.nameCHS:'';
        } else if ($rootScope.User.CultureCode == 'CHT') {
          SkuColor = (color)?color.nameCHT:'';
        }
        var sku = {
          ColorId: (color)?color.ColorId:'',
          ColorCode: (color)?color.ColorCode:'',
          ColorKey: (color)?color.nameEN:'', //color key is equal to color english name
          SkuColor: SkuColor, //sku color name
          SkuColorEN: (color)?color.nameEN:'', //sku color name
          SkuColorCHS: (color)?color.nameCHS:'', //sku color name
          SkuColorCHT: (color)?color.nameCHT:'', //sku color name
          SizeId: sizeId,
          SizeName: sizeName,
          SkuCode: '',
          Bar: '',
          QtySafetyThreshold: 0,
          IsNewAdd: 1 //this indicator tells this sku is newly create.
        };
        style.SkuList.push(sku);
      });
    });

    //3.Add new Color to ColorList
    var skuChoosedColorList = style.SkuList.map(function(n) {
      return {
        ColorKey: n.ColorKey,
        SkuColor: n.SkuColor
      };
    });
    skuChoosedColorList.forEach(function(n) {
      var ColorKey = n.ColorKey;
      if(ColorKey==''){
        return;
      }
      var isFound = _.find(style.ColorList, function(color) {
        return ColorKey == color.ColorKey;
      });
      if (!isFound) {
        style.ColorList.push(n);
      }
    });

    $scope.$dismiss();
  };

  //init the controller
  $scope.init = function() {
    //build sizeMap
    $scope.SizeGroupMap = _.groupBy(generalTranslationService.UserReference.SizeList, function(n) {
      return n.SizeGroupName;
    })
    if ($scope.SizeGroupMap['null']) {
      delete $scope.SizeGroupMap['null']
    }

    //build size id-name map
    _.forEach(generalTranslationService.UserReference.SizeList, function(n) {
      $scope.SizeIdNameMap[n.SizeId] = n.SizeName;
    });

    //build colors
    _.forEach(generalTranslationService.UserReference.ColorList, function(n) {
      $scope.colors[n.ColorCode] = {
        isChoose: 0,
        nameEN: '',
        nameCHS: '',
        nameCHT: '',
        ColorId: n.ColorId,
        ColorCode: n.ColorCode,
        ColorName: n.ColorName,
      }
    });
    if ($scope.colors['0']) {
      delete $scope.colors['0']
    }
    $scope.sizeList = $scope.SizeGroupMap['ALL'];
  }

  $scope.init();

}
