'use strict';

function BrandListCtrl($rootScope, $scope, $state, $modal, $loading, $http, RequestService, mainService) {
    var CONSTANTS = setScopeConstants();

    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        CONSTANTS.ROWACTION = {
            EDIT: 1,
            CHANGE_STATUS: 2
        };

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }

    $scope.t = mainService.UserReference.TranslationMap;
    $scope.breadcrumbs = [
        {text: $scope.t.LB_MM, link: "adminHome", icon: "icon-home"},
        {
            text: $scope.t.LB_AC_BRAND,
            link: 'brand'
        }
    ];

    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.RequestService = RequestService;

    $scope.go = $state.go;

    $scope.messages = [];
    $scope.locationId = $state.params.locationId;

    $scope.items = [];
    $scope.reverse = false;

    //messages
    if ($state.params.msg && $state.params.type) {
        $scope.messages = [{
            msg: $scope.t[$state.params.msg] || $state.params.msg,
            type: $state.params.type
        }];
    }

    $scope.brandListActionsByStatus = {
        2: [
            {lable: $scope.t.LB_EDIT, value: CONSTANTS.ROWACTION.EDIT},
            {lable: $scope.t.LB_PRODUCT_INACTIVATION, value: CONSTANTS.ROWACTION.CHANGE_STATUS}
        ],
        3: [
            {lable: $scope.t.LB_EDIT, value: CONSTANTS.ROWACTION.EDIT},
            {lable: $scope.t.LB_PRODUCT_ACTIVATION, value: CONSTANTS.ROWACTION.CHANGE_STATUS}
        ],
        4: [
            {lable: $scope.t.LB_EDIT, value: CONSTANTS.ROWACTION.EDIT},
            {lable: $scope.t.LB_PRODUCT_ACTIVATION, value: CONSTANTS.ROWACTION.CHANGE_STATUS}
        ]
    };

    // paginations
    $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
    $scope.sortTable = function (name) {
        if (name === $scope.sortBy) {
            $scope.reverse = !$scope.reverse;
        } else {
            $scope.reverse = false;
        }
        $scope.sortBy = name;
        $scope.refresh();
    };

    $scope.refresh = function () {
        $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
    };

    $scope.items = [];
    $scope.$watch('loading', function (flag) {
        if (flag) $loading.start('loading');
        else $loading.finish('loading');
    });

    $scope.brandFilter = function (item) {
        if (!$scope.brandCriteria || $scope.brandCriteria === '') return true;
        return !!item[$scope.brandCriteria];
    };

    $scope.getData = function () {
        $scope.loading = true;
        return $http.get($rootScope.servicePrefix + '/brand/list', {
            params: {
                "cc": $rootScope.language
            }
        })
            .then(function (result) {
                $scope.items = result.data;
            }, function () {
                $scope.messages = [
                    {msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR, type: 'danger'}
                ]
            })
            .then(function () {
                $scope.loading = false;
            });
    };

    $scope.getData();

    $scope.selectAction = function (item) {
        if (!item.rowAction) {
            return;
        }

        var action = parseInt(item.rowAction.value);
        switch (action) {
            // edit
            case CONSTANTS.ROWACTION.EDIT:
                $state.go('brand-edit', {brandId: item.BrandId});
                break;
            // change status
            case CONSTANTS.ROWACTION.CHANGE_STATUS:
                $scope.openStatusDialog(item);
                break;
        }
        item.rowAction = '';
    };

    $scope.openStatusDialog = function (item) {
        var _$scope = $scope;
        return $modal.open({
            templateUrl: "/shared/views/change-status-dialog-brand.html",
            backdrop: 'static',
            resolve: {
                mainService: function () {
                    return mainService;
                }
            },
            controller: ['$scope', function ($scope) {
                // Translation data
                $scope.t = mainService.UserReference.TranslationMap;
                $scope.brand = item;
                $scope.BrandName = item.BrandName;
                $scope.cancel = function () {
                    $scope.$dismiss();
                };
                $scope.confirm = function () {
                    var newStatus;
                    if (item.StatusId === CONSTANTS.STATUS.ACTIVE || item.StatusNameInvariant === 'Active') {
                        newStatus = 'Inactive';
                    }
                    else if (item.StatusId === CONSTANTS.STATUS.INACTIVE || item.StatusNameInvariant === 'Inactive' ||
                        item.StatusId === CONSTANTS.STATUS.PENDING || item.StatusNameInvariant === 'Pending') {
                        newStatus = 'Active';
                    }
                    //set confirm btn disabled
                    $scope.isConfirmBtnDisabled = true;
                    var hpromise = $http.post($rootScope.servicePrefix + '/brand/status/change', {
                        BrandId: $scope.brand.BrandId,
                        Status: newStatus
                    });
                    hpromise.then(function (result) {
                        $scope.$close(newStatus);
                        _$scope.getData();
                    }, function (errObj, status, headers, config) {
                        $scope.messages = [{
                            msg: $scope.t.MSG_ERR_CHANGE_STATUS_FAIL + errObj.data.AppCode,
                            type: 'danger'
                        }];
                    }).finally(function () {
                        //set confirm btn enabled
                        $scope.isConfirmBtnDisabled = false;
                    });
                };

                $scope.closeMessage = function (index) {
                    $scope.messages.splice(index, 1);
                };
            }]
        });
    };

    $scope.onDoubleClick = function (brandId) {
        $state.go("brand-edit", {brandId: brandId});
    };

}

app.controller('BrandListCtrl', ['$rootScope', '$scope', '$state', '$modal', '$loading', '$http', 'RequestService', 'mainService', BrandListCtrl]);
