'use strict';

function inventoryLocationEditCtrl($rootScope, $scope, $location, $anchorScroll, $window, $modal, $log, $http,
  Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService, InventoryService) {
  $scope.pageSide = 'admin';

  $scope.merchantId = $stateParams.merchantId;
  $scope.breadcrumbs = [
    { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
    { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant({id: '" + $scope.merchantId + "'})" }
  ];


  var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
    params: {
      merchantid: $scope.merchantId,
      cc: $rootScope.User.CultureCode
    }
  });
  hpromise.then(function(result) {
    $scope.Merchant = result.data.Merchant;
    $scope.breadcrumbs.splice(2, 0, { text: $scope.Merchant['MerchantName' + $rootScope.language], link: "merchantDetailsPage({id: '" + $scope.Merchant.MerchantId + "'})"});
  }, function(errObj, status, headers, config) {
    $scope.messages = [{
      msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
      type: 'danger'
    }];
  });

  BaseInventoryLocationEditCtrl.call(this, $rootScope, $scope, $location, $anchorScroll, $window, $modal, $log, $http,
  Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService, InventoryService);
}

inventoryLocationEditCtrl.prototype = Object.create(BaseInventoryLocationEditCtrl.prototype);
inventoryLocationEditCtrl.prototype.constructor = inventoryLocationEditCtrl;

angular.module('mmApp').controller('inventoryLocationEditCtrl', ['$rootScope', '$scope', '$location', '$anchorScroll', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', 
  'FileUploadService', 'generalTranslationService', 'geoService', 'InventoryService', inventoryLocationEditCtrl]);