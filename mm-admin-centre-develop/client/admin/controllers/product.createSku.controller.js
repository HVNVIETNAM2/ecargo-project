'use strict';

function ProductCreateSkuCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $q, $loading, generalTranslationService, geoService, style) {

  BaseProductCreateSkuCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $q, $loading, generalTranslationService, geoService, style);
}

ProductCreateSkuCtrl.prototype = Object.create(BaseProductCreateSkuCtrl);
ProductCreateSkuCtrl.prototype.constructor = ProductCreateSkuCtrl;

app.controller('productCreateSkuCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', '$q', '$loading', 'generalTranslationService', 'geoService', 'style', ProductCreateSkuCtrl
]);
