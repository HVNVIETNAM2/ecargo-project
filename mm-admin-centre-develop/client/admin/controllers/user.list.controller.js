'use strict';

function UserListCtrl($rootScope, $scope, $location, $window,
  $modal, $log, $http, mainService, FileUploadService, $state, generalTranslationService) {

  $scope.pageSide = "admin";
  $scope.isMerchant = false;
  $scope.breadcrumbs = [
    { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
    { text : mainService.UserReference.TranslationMap.LB_USER }
  ];

  $scope.getData = function(){
    var hpromise = $http.get($rootScope.servicePrefix+'/user/list/mm');
    return hpromise.then(function(resultObj){
      var data = _.map(resultObj.data, function(row) {
        row.LastLogin = Util.formatDate(row.LastLogin);
        row.LastModified = Util.formatDate(row.LastModified);
        row.Mobile_Num = row.MobileCode + row.MobileNumber;
        return row;
      });

      $scope.allUserData = data;
      $scope.tmpAllData = data;
    });
  };

  $scope.onDoubleClick = function(UserKey){
    $state.go('userEdit', { userKey : UserKey });
  };

  BaseUserListCtrl.call(this, $rootScope, $scope, $location, $window,
  $modal, $log, $http, mainService, FileUploadService, $state, generalTranslationService);
}


UserListCtrl.prototype = Object.create(BaseUserListCtrl.prototype);
UserListCtrl.prototype.constructor = UserListCtrl;

angular.module('mmApp').controller('UserListCtrl', ['$rootScope', '$scope', '$location', '$window',
'$modal', '$log', '$http', 'mainService', 'FileUploadService', '$state', 'generalTranslationService', UserListCtrl]);
