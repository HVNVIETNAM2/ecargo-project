'use strict';

app.controller('merchantCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', 'FileUploadService', 'generalTranslationService', 'geoService',
  function ($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService) {
    var CONSTANTS = $rootScope.CONSTANTS;
    
    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.generalTranslationService = generalTranslationService;
    $scope.t = mainService.UserReference.TranslationMap;

    $scope.breadcrumbs = [
	    { text : mainService.UserReference.TranslationMap.LB_MM , link : "adminHome", icon : "icon-home" },
	    { text : mainService.UserReference.TranslationMap.LB_MERCHANT }
	  ];

    $scope.CountryList = [];
    $scope.StatusList = [];
    $scope.MerchantTypeList = [];

    generalTranslationService.UserReference.StatusList.forEach(function(item){
      if (item.StatusId !== CONSTANTS.STATUS.DELETED) {
        $scope.StatusList.push(item);
      }
    });

    geoService.countries.forEach(function(item){
      $scope.CountryList.push(item);
    });

    generalTranslationService.UserReference.MerchantTypeList.forEach(function(item){
      $scope.MerchantTypeList.push(item);
    });

    $scope.messages = [];

    $scope.userListActionsByStatus = {
      'Active': [
        {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
        {lable: $scope.t.LB_INACTIVATE_MERC_USER, value:'Inactivate'},
        //{lable: $scope.t.LB_TL_RESET_PASSWORD, value:'Reset Password'},
        //{lable: $scope.t.LB_RESEND_REG_LINK, value:'Resend Registration Link'},
        //{lable: $scope.t.LB_RESEND_CHANGE_EMAIL_LINK, value:'Resend Change Email Link'},
        //{lable: $scope.t.LB_DELETE_USER, value:'Delete'}
      ],
      'Inactive': [
        {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
        {lable: $scope.t.LB_ACTIVATE_MERC_USER, value:'Activate'},
        //{lable: $scope.t.LB_TL_RESET_PASSWORD, value:'Reset Password'},
        //{lable: $scope.t.LB_RESEND_REG_LINK, value:'Resend Registration Link'},
        //{lable: $scope.t.LB_RESEND_CHANGE_EMAIL_LINK, value:'Resend Change Email Link'},
        //{lable: $scope.t.LB_DELETE_USER, value:'Delete'}
      ],
      'Pending': [
        {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
        {lable: $scope.t.LB_ACTIVATE_MERC_USER, value:'Activate'},
        //{lable: $scope.t.LB_TL_RESET_PASSWORD, value:'Reset Password'},
        //{lable: $scope.t.LB_RESEND_REG_LINK, value:'Resend Registration Link'},
        //{lable: $scope.t.LB_RESEND_CHANGE_EMAIL_LINK, value:'Resend Change Email Link'},
        //{lable: $scope.t.LB_DELETE_USER, value:'Delete'}
      ]
    };

    $scope.allUserData = [];
    $scope.tmpAllData = [];
    $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
    $scope.sortBy = 'MerchantDisplayName';
    $scope.rowActions = '';
    $scope.reverse = false;
    $scope.filter = "";

    $scope.getData = function(){
      var hpromise = $http.get($rootScope.servicePrefix+'/merch/list?cc=' + $rootScope.language);
      hpromise.then(function(resultObj){
        var dataParseFormat = "YYYY-MM-DD'T'HH:mm:ss'Z'";
        var dataOutputFormat = "YYYY-MM-DD HH:mm:ss";
        var data = _.map(resultObj.data, function(row) {
          row.LastLogin = moment(row.LastLogin, dataParseFormat).format(dataOutputFormat);
          row.LastModified = moment(row.LastModified, dataParseFormat).format(dataOutputFormat);
          return row;
        });

        $scope.allUserData = data;
        $scope.tmpAllData = data;
      });
    };

    $scope.onDoubleClick = function(MerchantId){
      $state.go('merchantDetailsPage', { id : MerchantId });
    };

    $scope.getData();

    $scope.sortTable = function(sortBy){
      if($scope.sortBy === sortBy){
        $scope.reverse = $scope.reverse === false ? true: false;
      }else{
        $scope.reverse = false;
      }
      $scope.sortBy = sortBy;
      $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
    };

    $scope.filterUserBySearch = function(user){
      $scope.currnetPage = CONSTANTS.DEFAULT_PAGE_NO;
      var lowercaseTerm = $scope.userSearchTerm ? $scope.userSearchTerm.toLowerCase() : '';

      if (!user) {
        return false;
      }

      if (user.MerchantName && user.MerchantName.toLowerCase().indexOf(lowercaseTerm) > -1) {
        return true;
      }
      if (user.MerchantCompanyName && user.MerchantCompanyName.toLowerCase().indexOf(lowercaseTerm) > -1) {
        return true;
      }

      return false;
    };

    $scope.filterUserByStatus = function(item){
      var value = parseInt($scope.filter.status);
      if(value < 0){ return true; }

      if (value === item.StatusId){
        return true;
      }

      return false;
    };

    $scope.filterUserByMerchantTypes = function(item){
      var value = parseInt($scope.filter.merchantTypes);
      if(value < 0){ return true; }

      if (value === item.MerchantTypeId){
        return true;
      }

      return false;
    };

    $scope.filterUserByCountry = function(item){
      var value = parseInt($scope.filter.country);
      if(value < 0){ return true; }

      if (value === item.GeoCountryId){
        return true;
      }

      return false;
    };

    $scope.selectAction = function(userKey, rowActions, row){
      if(!rowActions || !rowActions.value){ return true; }
      var action = rowActions.value;

      if(action === 'Edit'){
        $scope.onDoubleClick(userKey);
      }else if(action === 'Activate' || action === 'Inactivate') {
        $scope.openChangeStatusDialog(row);
      }else if (action === 'Reset Password'){
        $scope.openResetPasswordDialog(row);
      }

      // else if (action === 'Resend Registration Link'){
      //   $scope.openResendLinkDialog({
      //     user: row,
      //     templateUrl: '/shared/views/resend-registration-link-dialog.html',
      //     linkType: 'Registration',
      //     errorMsg: 'Failed to resend registration link. Please try again later.'
      //   });
      // }else if (action === 'Resend Change Email Link'){
      //   $scope.openResendLinkDialog({
      //     user: row,
      //     templateUrl: '/shared/views/resend-change-email-link-dialog.html',
      //     linkType: 'Email',
      //     errorMsg: 'Failed to resend change email link. Please try again later.'
      //   });
      // }else if (action === 'Delete') {
      //   $scope.openDeleteUserDialog(row);
      // }else if(action === "RRCS"){
      //   $scope.openResendRegistrationCodeSMS(row, rowActions);
      // }else if(action === "RRPC"){
      //   $scope.openResendResetPasswordCodeSMS(row, rowActions);
      // }else if(action === "RCMCS"){
      // $scope.openResendChangeMobileCodeSMS(row, rowActions);
      // }
    };

    $scope.openChangeStatusDialog = function(user) {
      var modalInstance = $modal.open({
          templateUrl: "/shared/views/change-status-dialog-merchant.html",
          backdrop : 'static',
          resolve: {
            mainService: function() {
              return mainService;
            }
          },
          controller: ['$scope', 'mainService', function($scope, mainService) {
            $scope.user = user;
            //global util module outside of angular.
            // Translation data
            $scope.t = mainService.UserReference.TranslationMap;
            $scope.cancel = function() {
              /*
              // User cancelled the change, revert the switch control's value. Use the intermediate value to
              // avoid infinite watch.
              $scope.form.StatusSwitchValue = undefined;
              $scope.form.StatusSwitchValue = oldStatus;
              */
              $scope.$dismiss();
            };

            $scope.confirm = function() {
              /*
              // Change the model value and submit the change
              // $scope.form.StatusNameInvariant = newStatus;
              */

              var newStatus, newStatusId;
              if (user.StatusNameInvariant === 'Active') {
                newStatus = 'Inactive';
                newStatusId = CONSTANTS.STATUS.INACTIVE;
              }
              else if (user.StatusNameInvariant === 'Inactive' || user.StatusNameInvariant === 'Pending') {
                newStatus = 'Active';
                newStatusId = CONSTANTS.STATUS.ACTIVE;
              }
              //set confirm btn disabled;
              $scope.isConfirmBtnDisabled=true;
              var hpromise = $http.post($rootScope.servicePrefix+'/merch/status/change', {
                "MerchantId": user.MerchantId,
                'Status': newStatus,
                newStatusId: newStatusId
              });
              hpromise.then(function(result) {
                $scope.$close({user: user, newStatus: newStatus, newStatusId: newStatusId});
              }, function(errObj, status, headers, config) {
                $scope.messages = [
                  {msg: 'Failed to change status. Please try again later.', type: 'danger'}
                ];
              }).finally(function(){
                //set confirm btn enabled;
                $scope.isConfirmBtnDisabled=false;
              });
            };

            $scope.closeMessage = function(index) {
              $scope.messages.splice(index, 1);
            };
          }]
      });

      modalInstance.result.then(function(data) {
        // Update the affected row's Status field
        _.each($scope.allUserData, function(row) {
          if (row.MerchantId === data.user.MerchantId) {
            row.StatusNameInvariant = data.newStatus;
            row.StatusId = data.newStatusId;
          }
        });
      });
    };

    $scope.openCreateUserDialog = function(){
      $state.go("merchantSave");
    };
  }
]);
