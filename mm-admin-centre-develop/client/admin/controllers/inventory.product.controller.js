'use strict';

function InventoryProductListCtrl($rootScope, $scope, $state, $modal, $http, $loading, RequestService, InventoryService, mainService, generalTranslationService) {
    this.InventoryService = InventoryService;
    $scope.t = mainService.UserReference.TranslationMap;

    $scope.pageSide = 'admin';

    $scope.merchantId = $state.params.merchantId;
    $scope.breadcrumbs = [
        { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
        { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant({id: '" + $scope.merchantId + "'})" }
    ];


    var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
        params: {
            merchantid: $scope.merchantId,
            cc: $rootScope.User.CultureCode
        }
    });
    hpromise.then(function(result) {
        $scope.Merchant = result.data.Merchant;
        $scope.breadcrumbs.splice(2, 0, { text: $scope.Merchant['MerchantName' + $rootScope.language], link: "merchantDetailsPage({id: '" + $scope.Merchant.MerchantId + "'})"});
    }, function(errObj, status, headers, config) {
        $scope.messages = [{
            msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
            type: 'danger'
        }];
    });

    BaseInventoryProductListCtrl.call(this, $rootScope, $scope, $state, $modal, $http, $loading, RequestService, InventoryService, mainService, generalTranslationService);
}

InventoryProductListCtrl.prototype = Object.create(BaseInventoryProductListCtrl.prototype);
InventoryProductListCtrl.prototype.constructor = BaseInventoryProductListCtrl;

app.controller('InventoryProductListCtrl', InventoryProductListCtrl);
