'use strict';

function ProductChangeStatusCtrl($rootScope, $scope, $http, $state, $stateParams, mainService, style) {

  BaseProductChangeStatusCtrl.call(this, $rootScope, $scope, $http, $state, $stateParams, mainService, style);
}

ProductChangeStatusCtrl.prototype = Object.create(BaseProductChangeStatusCtrl);
ProductChangeStatusCtrl.prototype.constructor = ProductChangeStatusCtrl;

app.controller('productChangeStatusCtrl', ['$rootScope', '$scope', '$http', '$state', '$stateParams', 'mainService', 'style', ProductChangeStatusCtrl]);
