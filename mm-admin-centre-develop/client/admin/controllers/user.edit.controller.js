'use strict';

function UserEditCtrl($rootScope, $scope, $location, $window, $modal,
  $log, $http, Upload, mainService, translationService, $state, $stateParams, $anchorScroll, FileUploadService, generalTranslationService) {

  $scope.pageSide = "admin";
  $scope.breadcrumbs = [
    { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
    { text : mainService.UserReference.TranslationMap.LB_EDIT_USER }
  ];

  if ($rootScope.isAdmin()) {
    $scope.breadcrumbs.splice(1, 0, { text : mainService.UserReference.TranslationMap.LB_USER, link : "user" });
  }

  $scope.init = function() {
    var hpromise = $http.get($rootScope.servicePrefix+'/user/view', {
      params: {
        userkey: $stateParams.userKey
      }
    });
    hpromise.then(function(result) {
      $scope.form = result.data;
      $scope.firstName = $scope.form.FirstName;
      $scope.lastName = $scope.form.LastName;
      $scope.displayName = $scope.form.DisplayName;
    }, function(errObj, status, headers, config) {
      $scope.messages = [
        {msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR, type: 'danger'}
      ];
    });
  };

  $scope.init();  // load user profile

  $scope.changeMobileCode = function(code) {
    $scope.form.MobileCode = code;
  };

  BaseUserEditCtrl.call(this, $rootScope, $scope, $location, $window, $modal,
    $log, $http, Upload, mainService, translationService, $state, $stateParams, $anchorScroll, FileUploadService, generalTranslationService);
}

UserEditCtrl.prototype = Object.create(BaseUserEditCtrl.prototype);
UserEditCtrl.prototype.constructor = UserEditCtrl;
angular.module('mmApp').controller('UserEditCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', 'translationService', '$state', '$stateParams', '$anchorScroll', 'FileUploadService', 'generalTranslationService', UserEditCtrl]);

