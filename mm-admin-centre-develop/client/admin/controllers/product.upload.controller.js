'use strict';

function ProductUploadCtrl($rootScope, $scope, $q, $location, $anchorScroll, $window, $modal, $log, $http, $timeout,
  Upload, mainService, $state, $stateParams, FileUploadService, CONSTANTS) {

  $scope.MerchantId = $stateParams.merchantId;

  /**
   * route merchant id
   */
  $scope.merchantId = $state.params.merchantId;

  var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
    params: {
      merchantid: $scope.MerchantId,
      cc: $rootScope.User.CultureCode
    }
  });
  hpromise.then(function(result) {
    $scope.Merchant = result.data.Merchant;
    //build breadcrumbs
    $scope.breadcrumbs = [{
      text: mainService.UserReference.TranslationMap.LB_MM,
      link: "adminHome",
      icon: "icon-home"
    }, {
      text: mainService.UserReference.TranslationMap.LB_MERCHANT,
      link: "merchant"
    }, {
      text: $scope.Merchant['MerchantName' + $rootScope.language],
      link: "merchantDetailsPage({id: " + $scope.Merchant.MerchantId + "})"
    }, {
      text: mainService.UserReference.TranslationMap.LB_PRODUCT_LIST,
      link: "productList({merchantId: "+$scope.Merchant.MerchantId+"})"
    }, {
      text: mainService.UserReference.TranslationMap.LB_UPLOAD_IMAGE
    }];
  }, function(errObj, status, headers, config) {
    $scope.messages = [{
      msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
      type: 'danger'
    }];
  });

  BaseProductUploadCtrl.call(this, $rootScope, $scope, $q, $location, $anchorScroll, $window, $modal, $log, $http, $timeout,
    Upload, mainService, $state, $stateParams, FileUploadService, CONSTANTS);
}

ProductUploadCtrl.prototype = Object.create(BaseProductUploadCtrl);
ProductUploadCtrl.prototype.constructor = ProductUploadCtrl;

app.controller('productUploadCtrl', [
  '$rootScope', '$scope', '$q', '$location', '$anchorScroll', '$window', '$modal',
  '$log', '$http', '$timeout', 'Upload', 'mainService', '$state',
  '$stateParams', 'FileUploadService', 'CONSTANTS', ProductUploadCtrl
]);
