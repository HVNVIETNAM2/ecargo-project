'use strict';

function categoryController($rootScope, $scope, $location, $window, $modal,
                            $log, $http, Upload, mainService, translationService, $state, $stateParams, $anchorScroll,
                            FileUploadService, generalTranslationService, geoService) {
    var CONSTANTS = setScopeConstants();

    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        CONSTANTS.CATEGORY = {
            IS_FOREVER: 1,
            IS_NOT_FOREVER: 0
        };

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }


    //init setting object
    $rootScope.settings = $state.current.settings;
    $scope.saveMode = $state.current.saveMode;
    $scope.t = mainService.UserReference.TranslationMap;
    $scope.FileUploadService = FileUploadService;
    $scope.generalTranslationService = generalTranslationService;

    //init category related object
    $scope.messages = [];
    $scope.Category = {};
    $scope.ParentCategory = {};
    $scope.Category.CategoryBrandMerchantList = [];
    $scope.BrandMerchantSelectionList = [];
    if ($scope.saveMode) {
        $scope.Category.ParentCategoryId = $stateParams.parentCategoryId;
    } else {
        $scope.Category.CategoryId = $stateParams.categoryId;
    }

    $scope.init = function () {

        //1.init category BrandMerchantSelectionList
        var brandpromise = $http.get($rootScope.servicePrefix + '/brand/list/combined', {
            params: {
                cc: $rootScope.User.CultureCode
            }
        });
        brandpromise.then(function (response) {
            $scope.BrandMerchantSelectionList = _.filter(response.data, function (item) {
                return item.EntityId && item.StatusId !== CONSTANTS.STATUS.PENDING;
            });
        });

        //2.init category object
        $scope.breadcrumbs = [
            {text: $scope.t.LB_MM, link: "adminHome", icon: "icon-home"},
            {text: mainService.UserReference.TranslationMap.LB_AC_CATEGORY, link: "category"},
            {text: ($scope.saveMode) ? $scope.t.LB_CREATE_NEW_CATEGORY : $scope.t.LB_EDIT_CATEGORY}
        ];
        var queryCategoryId;
        if ($scope.saveMode) { //create new Category
            queryCategoryId = $scope.Category.ParentCategoryId;
        } else {//edit Category
            queryCategoryId = $scope.Category.CategoryId;
        }
        var hpromise = $http.get($rootScope.servicePrefix + '/category/view', {
            params: {
                categoryId: queryCategoryId,
                // cc: $rootScope.Category.CultureCode
            }
        });
        hpromise.then(function (result) {
            if ($scope.saveMode) {//create new Category
                $scope.ParentCategory = result.data;
                $scope.Category.ParentCategoryCode = $scope.ParentCategory.CategoryCode; //to get the ParentCategoryCode
                //set default value
                $scope.Category.IsForever = CONSTANTS.CATEGORY.IS_FOREVER;
                if($scope.ParentCategory.CategoryId!=0){//if it is not root category, then inherit commission rate from its parent.
                    $scope.Category.DefaultCommissionRate = $scope.ParentCategory.DefaultCommissionRate;
                }
            } else {//edit category
                $scope.Category = result.data;
                $scope.CategoryName = $scope.Category['CategoryName' + $rootScope.language];
                //set display value
                if ($scope.Category.StartPublishTime === '1970-01-01T00:00:00.000Z') {
                    $scope.Category.StartPublishTime = null;
                }
                if ($scope.Category.EndPublishTime === '1970-01-01T00:00:00.000Z') {
                    $scope.Category.EndPublishTime = null;
                }
                if ($scope.Category.StartPublishTime || $scope.Category.EndPublishTime) {
                    $scope.Category.IsForever = CONSTANTS.CATEGORY.IS_NOT_FOREVER;
                } else {
                    $scope.Category.IsForever = CONSTANTS.CATEGORY.IS_FOREVER;
                }
                //rebuild CategoryBrandMerchantList to match BrandMerchantSelectionList's object
                $scope.Category.CategoryBrandMerchantList = _.filter($scope.BrandMerchantSelectionList, function (item) {
                    return _.any($scope.Category.CategoryBrandMerchantList, function (element) {
                        return element.Entity === item.Entity && element.EntityId === item.EntityId;
                    });
                });

//            console.log($scope.CategoryBrandMerchantList);
            }
        }, function (errObj, status, headers, config) {
            var errorCodeMsg = ($scope.t[errObj.data.AppCode] === undefined) ? (errObj.data.AppCode) : ($scope.t[errObj.data.AppCode]);
            $scope.messages = [{
                msg: errorCodeMsg,
                type: 'danger'
            }];
        });

        //scroll to the top of page
        // the element you wish to scroll to.
        $location.hash('page_top');
        // call $anchorScroll()
        $anchorScroll();
    };

    $scope.saveCategory = function (validationForm, statusId) {
        validationForm.$submitted = true;
        if (!validationForm.$valid) {
            if (window.console) {
                console.log("category save failed due to the invalid input.");
            }
            return;
        }
        $scope.isSaveBtnDisabled = true;
        setValues();
        var hpromise = $http.post($rootScope.servicePrefix + '/category/save', {Category: $scope.Category});
        hpromise.then(function (result) {
            $scope.messages = [{
                msg: ($scope.saveMode) ? $scope.t.MSG_CATEGORY_CREATED : $scope.t.MSG_CATEGORY_EDITED,
                type: 'success'
            }];
            $scope.isSaveBtnDisabled = false;
            if ($scope.saveMode) {
                $state.go("category");
            }
        }, function (errObj, status, headers, config) {
            var errorCodeMsg = ($scope.t[errObj.data.AppCode] === undefined) ? (errObj.data.AppCode) : ($scope.t[errObj.data.AppCode]);
            $scope.messages = [{
                msg: errorCodeMsg,
                type: 'danger'
            }];
            $scope.isSaveBtnDisabled = false;
        })
            .finally(function () {
                //scroll to the top of page
                // the element you wish to scroll to.
                $location.hash('page_top');
                // call $anchorScroll()
                $anchorScroll();
            });

        function setValues() {
            //set value
            if ($scope.Category.IsForever === CONSTANTS.CATEGORY.IS_FOREVER) {
                $scope.Category.StartPublishTime = null;
                $scope.Category.EndPublishTime = null;
            }
            if (statusId)
                $scope.Category.StatusId = statusId;
        }
    };

    $scope.openChangeCategoryStatusDialog = function (category) {
        var modalInstance = $modal.open({
            templateUrl: "views/confirm-change-status-category-dialog.html",
            backdrop: 'static',
            resolve: {
                mainService: function () {
                    return mainService;
                }
            },
            controller: ['$scope', function ($scope) {
                $scope.category = category;
                //global util module outside of angular.
                // Translation data
                $scope.t = mainService.UserReference.TranslationMap;
                $scope.cancel = function () {
                    $scope.$dismiss();
                };

                $scope.confirm = function () {
                    //set confirm btn disabled
                    $scope.isConfirmBtnDisabled = true;
                    var changeStatusId = (category.StatusId == 2) ? 4 : 2;
                    var hpromise = $http.post($rootScope.servicePrefix + '/category/status/change', {
                        "CategoryId": category.CategoryId,
                        "StatusId": changeStatusId
                    });
                    hpromise.then(function (result) {
                        $scope.$close(true);
                        category.StatusId = changeStatusId;
                        $scope.isSaveBtnDisabled = false;
                    }, function (errObj, status, headers, config) {
                        var errorCodeMsg = ($scope.t[errObj.data.AppCode] === undefined) ? (errObj.data.AppCode) : ($scope.t[errObj.data.AppCode]);
                        $scope.messages = [{
                            msg: errorCodeMsg,
                            type: 'danger'
                        }];
                    }).finally(function () {
                        //set confirm btn enabled
                        $scope.isConfirmBtnDisabled = false;
                    });
                };

                $scope.closeMessage = function (index) {
                    $scope.messages.splice(index, 1);
                };
            }]
        });
//      modalInstance.result.then(function(data) {
//        // Refresh the list to remove the deleted category row
//        $scope.getData();
//      });
    };

    $scope.openDeleteCategoryDialog = function () {
        var category = $scope.Category;
        var modalInstance = $modal.open({
            templateUrl: "views/confirm-delete-category-dialog.html",
            backdrop: 'static',
            controller: ['$scope', function ($scope) {
                $scope.category = category;
                //global util module outside of angular.
                // Translation data
                $scope.t = mainService.UserReference.TranslationMap;
                $scope.cancel = function () {
                    $scope.$dismiss();
                };

                $scope.confirm = function () {
                    //set confirm btn disabled
                    $scope.isConfirmBtnDisabled = true;
                    var hpromise = $http.post($rootScope.servicePrefix + '/category/delete', {
                        "CategoryId": category.CategoryId,
                    });
                    hpromise.then(function (result) {
                        $scope.$close(true);
                    }, function (errObj, status, headers, config) {
                        var errorCodeMsg = ($scope.t[errObj.data.AppCode] === undefined) ? (errObj.data.AppCode) : ($scope.t[errObj.data.AppCode]);
                        $scope.messages = [{
                            msg: errorCodeMsg,
                            type: 'danger'
                        }];
                    }).finally(function () {
                        //set confirm btn enabled
                        $scope.isConfirmBtnDisabled = false;
                    });
                };

                $scope.closeMessage = function (index) {
                    $scope.messages.splice(index, 1);
                };
            }]
        });
        modalInstance.result.then(function (data) {
            // go to list if successful
            $state.go('category');
        });
    };

    $scope.cancel = function () {
        $state.go("category");
    }

    $scope.closeMessage = function (index) {
        $scope.messages.splice(index, 1);
    };

    $scope.init();
}

angular.module('mmApp').controller('CategoryCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
    '$log', '$http', 'Upload', 'mainService', 'translationService', '$state', '$stateParams', '$anchorScroll', 'FileUploadService',
    'generalTranslationService', 'geoService', categoryController
]);
