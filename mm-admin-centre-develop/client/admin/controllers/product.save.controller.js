'use strict';

function ProductSaveCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $q, $loading, generalTranslationService, geoService, $anchorScroll, DialogService) {
  $scope.saveMode = $state.current.saveMode;
  $scope.t = mainService.UserReference.TranslationMap;
  $scope.merchantId = $stateParams.merchantId;

  var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
    params: {
      merchantid: $scope.merchantId,
      cc: $rootScope.User.CultureCode
    }
  });
  hpromise.then(function(result) {
    $scope.Merchant = result.data.Merchant;
    //build breadcrumbs
    $scope.breadcrumbs = [{
      text: mainService.UserReference.TranslationMap.LB_MM,
      link: "adminHome",
      icon: "icon-home"
    }, {
      text: mainService.UserReference.TranslationMap.LB_MERCHANT,
      link: "merchant"
    }, {
      text: $scope.Merchant['MerchantName' + $rootScope.language],
      link: "merchantDetailsPage({id: " + $scope.Merchant.MerchantId + "})"
    }, {
      text: mainService.UserReference.TranslationMap.LB_PRODUCT_LIST,
      link: "productList({merchantId: "+$scope.Merchant.MerchantId+"})"
    }, {
      text: ($scope.saveMode)?$scope.t.LB_PROD_CREATE: Util.format($scope.t.LB_PROD_EDIT,[''])
    }];
  }, function(errObj, status, headers, config) {
    $scope.messages = [{
      msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
      type: 'danger'
    }];
  });

  BaseProductSaveCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $q, $loading, generalTranslationService, geoService, $anchorScroll, DialogService);
}

ProductSaveCtrl.prototype = Object.create(BaseProductSaveCtrl);
ProductSaveCtrl.prototype.constructor = ProductSaveCtrl;

app.controller('productSaveCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', '$q', '$loading', 'generalTranslationService', 'geoService', '$anchorScroll', 'DialogService', ProductSaveCtrl
]);
