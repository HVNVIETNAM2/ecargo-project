'use strict';

app.controller('merchantDetailsCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', 'FileUploadService',
  function ($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, FileUploadService) {
    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.t = mainService.UserReference.TranslationMap;

    $scope.MerchantId = $stateParams.id;
    $scope.Merchant = {};//merchant object.

    $scope.breadcrumbs = [
     { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" }
    ];
   $scope.init = function() {
     //retrive the merchant.
     var hpromise = $http.get($rootScope.servicePrefix+'/merch/view', {
       params: {
         merchantid: $scope.MerchantId,
         cc: $rootScope.User.CultureCode
       }
     });
     hpromise.then(function(result) {
       $scope.Merchant = result.data.Merchant;
       //build breadcrumbs
       $scope.breadcrumbs = [
   	    { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
   	    { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant" },
        { text : $scope.Merchant['MerchantName' + $rootScope.language] }
   	  ];
     }, function(errObj, status, headers, config) {
        if (window.console) {
           console.log(errObj);
        }
     });
   };

   $scope.init();
  }
]);
