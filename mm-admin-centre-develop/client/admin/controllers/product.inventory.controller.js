'use strict';

/**
 * Product Inventory Controller
 * @constructor
 */
function ProductInventoryCtrl($rootScope, $http, $scope, $state, $timeout, $window, $modal, $loading, $location, $anchorScroll, RequestService, InventoryService, mainService) {
    this.$scope = $scope;
    this.$state = $state;
    this.InventoryService = InventoryService;
    $scope.pageSide = 'admin';
    $scope.t = mainService.UserReference.TranslationMap;
    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.RequestService = RequestService;

    $scope.messages = [];

    $scope.skuId = $state.params.skuid;
    $scope.inventoryId = $state.params.inventoryid;
    $scope.inventoryLocationId = $state.params.inventorylocationid;

    $scope.isNewInventory = typeof $scope.inventoryId === 'undefined' || $scope.inventoryId === '';
    $scope.isEdit = $state.is('productInventory-edit') || $state.is('locationInventory-edit');
    $scope.IsPerpetual = false;

    $scope.routePrefix = $state.is('locationInventory-create') || $state.is('locationInventory-edit') ? 'location' : 'product';
    $scope.route = $state.is('productInventory-edit') || $state.is('locationInventory-edit') ? 'edit' : 'create';


    $scope.merchantId = $state.params.merchantId;
    $scope.breadcrumbs = [
        { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
        { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant({id: '" + $scope.merchantId + "'})" }
    ];


    var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
        params: {
            merchantid: $scope.merchantId,
            cc: $rootScope.User.CultureCode
        }
    });
    hpromise.then(function(result) {
        $scope.Merchant = result.data.Merchant;
        $scope.breadcrumbs.splice(2, 0, { text: $scope.Merchant['MerchantName' + $rootScope.language], link: "merchantDetailsPage({id: '" + $scope.Merchant.MerchantId + "'})"});
    }, function(errObj, status, headers, config) {
        $scope.messages = [{
            msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
            type: 'danger'
        }];
    });



    BaseProductInventoryCtrl.call(this, $rootScope, $scope, $state, $timeout, $window, $modal, $loading, $location, $anchorScroll, RequestService, InventoryService, mainService);
}

ProductInventoryCtrl.prototype = Object.create(BaseProductInventoryCtrl.prototype);
ProductInventoryCtrl.prototype.constructor = BaseProductInventoryCtrl;

app.controller('productInventoryCtrl', ['$rootScope', '$http', '$scope', '$state', '$timeout', '$window', '$modal', '$loading', '$location', '$anchorScroll', 'RequestService', 'InventoryService', 'mainService', ProductInventoryCtrl]);
