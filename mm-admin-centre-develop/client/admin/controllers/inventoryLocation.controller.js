'use strict';

function MerchantInventoryLocationCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, 
  Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService, InventoryService) {

  $scope.MerchantId = $stateParams.merchantId;
  $scope.pageSide = "admin";


  var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
    params: {
      merchantid: $scope.MerchantId,
      cc: $rootScope.User.CultureCode
    }
  });
  hpromise.then(function(result) {
    $scope.Merchant = result.data.Merchant;
    $scope.breadcrumbs = [
      { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
      { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant" },
      { text: $scope.Merchant['MerchantName' + $rootScope.language], link: "merchantDetailsPage({id: " + $scope.Merchant.MerchantId + '})'},
      { text : mainService.UserReference.TranslationMap.LB_INVENTORY_LOCATION }
    ];
  }, function(errObj, status, headers, config) {
    $scope.messages = [{
      msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
      type: 'danger'
    }];
  });



  BaseInventoryLocationCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, 
  Upload, mainService, $state, $stateParams, FileUploadService, generalTranslationService, geoService, InventoryService);
}

MerchantInventoryLocationCtrl.prototype = Object.create(BaseInventoryLocationCtrl.prototype);
MerchantInventoryLocationCtrl.prototype.constructor = MerchantInventoryLocationCtrl;

angular.module('mmApp').controller('MerchantInventoryLocationCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal', 
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', 
  'FileUploadService', 'generalTranslationService', 'geoService', 'InventoryService', MerchantInventoryLocationCtrl]);