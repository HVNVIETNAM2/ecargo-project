'use strict';

function MerchantUserListCtrl($rootScope, $scope, $location, $window,
  $modal, $log, $http, mainService, FileUploadService, $state, generalTranslationService, $stateParams) {

  $scope.pageSide = "merchant";
  $scope.merchantId = $stateParams.merchantId;
  $scope.isMerchant = true;

  if ($scope.merchantId) {
    var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
      params: {
        merchantid: $scope.merchantId,
        cc: $rootScope.User.CultureCode
      }
    });
    hpromise.then(function(result) {
      $scope.Merchant = result.data.Merchant;
      //build breadcrumbs
      $scope.breadcrumbs.splice(2, 0, { text: $scope.Merchant['MerchantName' + $rootScope.language], link: "merchantDetailsPage({id: '" + $scope.Merchant.MerchantId + "'})"});
    }, function(errObj, status, headers, config) {
      $scope.messages = [{
        msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
        type: 'danger'
      }];
    });
  }


  $scope.breadcrumbs = [
    { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
    { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant" },
    { text : mainService.UserReference.TranslationMap.LB_USER }
  ];

  $scope.t = mainService.UserReference.TranslationMap;

  $scope.userListActionsByStatus = {
    'Active': [
      {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
      {lable: $scope.t.LB_INACTIVATE_USER, value:'Inactivate'},
      {lable: $scope.t.LB_TL_RESET_PASSWORD, value:'Reset Password'},
      {lable: $scope.t.LB_RESEND_REG_LINK, value:'Resend Registration Link'},
      {lable: $scope.t.LB_RESEND_CHANGE_EMAIL_LINK, value:'Resend Change Email Link'},
      {lable: $scope.t.LB_DELETE_USER, value:'Delete'}
    ],
    'Inactive': [
      {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
      {lable: $scope.t.LB_ACTIVATE_USER, value:'Activate'},
      {lable: $scope.t.LB_TL_RESET_PASSWORD, value:'Reset Password'},
      {lable: $scope.t.LB_RESEND_REG_LINK, value:'Resend Registration Link'},
      {lable: $scope.t.LB_RESEND_CHANGE_EMAIL_LINK, value:'Resend Change Email Link'},
      {lable: $scope.t.LB_DELETE_USER, value:'Delete'}
    ],
    'Pending': [
      {lable: $scope.t.LB_EDIT_USER, value:'Edit'},
      {lable: $scope.t.LB_TL_RESET_PASSWORD, value:'Reset Password'},
      {lable: $scope.t.LB_RESEND_REG_LINK, value:'Resend Registration Link'},
      {lable: $scope.t.LB_RESEND_CHANGE_EMAIL_LINK, value:'Resend Change Email Link'},
      {lable: $scope.t.LB_DELETE_USER, value:'Delete'}
    ]
  };


  $scope.getData = function(){
    var hpromise = $http.get($rootScope.servicePrefix+'/user/list/merchant?merchantid='+ $scope.merchantId);
    return hpromise.then(function(resultObj){
      var data = _.map(resultObj.data, function(row) {
        row.LastLogin = Util.formatDate(row.LastLogin);
        row.LastModified = Util.formatDate(row.LastModified);
        row.Mobile_Num = row.MobileCode + row.MobileNumber;

        if( row.isMobileUser ){
          row.options = JSON.parse(JSON.stringify($scope.userListActionsByStatus[ row.StatusNameInvariant ]));
          row.options.splice( (row.options.length - 1) , 0, { lable : $scope.t.LB_USER_COMPLETE_REGISTRATION_TITLE, value : "RRCS" });
          row.options.splice( (row.options.length - 1) , 0, { lable : $scope.t.LB_RESET_PASSWORD_SMS, value : "RRPC" });
          row.options.splice( (row.options.length - 1) , 0, { lable : "Resend Change Mobile Code SMS", value : "RCMCS" });
        }
        return row;
      });

      $scope.allUserData = data;
      $scope.tmpAllData = data;

    });
  };

  $scope.onDoubleClick = function(UserKey){
    $state.go('merchantUserEdit', { merchantId: $scope.merchantId, userKey : UserKey });
  };

  BaseUserListCtrl.call(this, $rootScope, $scope, $location, $window,
  $modal, $log, $http, mainService, FileUploadService, $state, generalTranslationService);
}


MerchantUserListCtrl.prototype = Object.create(BaseUserListCtrl.prototype);
MerchantUserListCtrl.prototype.constructor = MerchantUserListCtrl;

angular.module('mmApp').controller('MerchantUserListCtrl', ['$rootScope', '$scope', '$location', '$window',
'$modal', '$log', '$http', 'mainService', 'FileUploadService', '$state', 'generalTranslationService', '$stateParams', MerchantUserListCtrl]);

