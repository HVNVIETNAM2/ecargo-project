
app.controller('SearchCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$http', 'mainService', '$state', '$stateParams', 'TranslationService', 'GeneralTranslationService',
  function ($rootScope, $scope, $location, $window, $modal, $http, mainService, $state, $stateParams, TranslationService, GeneralTranslationService) {

    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.t = mainService.UserReference.TranslationMap;

    $scope.MerchantId = $stateParams.id;
    $scope.Merchant = {};
    
  }
]);
