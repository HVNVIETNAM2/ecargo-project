'use strict';

function ProductListCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $loading, generalTranslationService) {

  //if merchantid is not specified, go to home page directly.
  if ($stateParams.merchantId === undefined || $stateParams.merchantId == null || $stateParams.merchantId == '') {
    $state.go('home');
  }

  $scope.merchantId = $stateParams.merchantId;
  var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
    params: {
      merchantid: $scope.merchantId,
      cc: $rootScope.User.CultureCode
    }
  });
  hpromise.then(function(result) {
    $scope.Merchant = result.data.Merchant;
    //build breadcrumbs
    $scope.breadcrumbs = [{
      text: mainService.UserReference.TranslationMap.LB_MM,
      link: "adminHome",
      icon: "icon-home"
    }, {
      text: mainService.UserReference.TranslationMap.LB_MERCHANT,
      link: "merchant"
    }, {
      text: $scope.Merchant['MerchantName' + $rootScope.language],
      link: "merchantDetailsPage({id: " + $scope.Merchant.MerchantId +"})"
    }, {
      text: mainService.UserReference.TranslationMap.LB_PRODUCT_LIST
    }];
  }, function(errObj, status, headers, config) {
    $scope.messages = [{
      msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
      type: 'danger'
    }];
  });



  BaseProductListCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, $loading, generalTranslationService);
}

ProductListCtrl.prototype = Object.create(BaseProductListCtrl);
ProductListCtrl.prototype.constructor = ProductListCtrl;

app.controller('productListCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', '$loading','generalTranslationService', ProductListCtrl
]);
