'use strict';

function ProductImportCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams) {

  $scope.MerchantId = $stateParams.merchantId;

  var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
    params: {
      merchantid: $scope.MerchantId,
      cc: $rootScope.User.CultureCode
    }
  });
  hpromise.then(function(result) {
    $scope.Merchant = result.data.Merchant;
    //build breadcrumbs
    $scope.breadcrumbs = [{
      text: mainService.UserReference.TranslationMap.LB_MM,
      link: "adminHome",
      icon: "icon-home"
    }, {
      text: mainService.UserReference.TranslationMap.LB_MERCHANT,
      link: "merchant"
    }, {
      text: $scope.Merchant['MerchantName' + $rootScope.language],
      link: "merchantDetailsPage({id: " + $scope.Merchant.MerchantId + '})'
    }, {
      text: mainService.UserReference.TranslationMap.LB_PRODUCT_LIST,
      link: "productList({merchantId: "+$scope.Merchant.MerchantId+"})"
    }, {
      text: mainService.UserReference.TranslationMap.LB_IMPORT_PRODUCTS
    }];
  }, function(errObj, status, headers, config) {
    $scope.messages = [{
      msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
      type: 'danger'
    }];
  });

  BaseProductImportCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams);
}

ProductImportCtrl.prototype = Object.create(BaseProductImportCtrl);
ProductImportCtrl.prototype.constructor = ProductImportCtrl;

app.controller('productImportCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', ProductImportCtrl
]);