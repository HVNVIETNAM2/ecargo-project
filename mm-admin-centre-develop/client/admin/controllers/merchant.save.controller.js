'use strict';

function merchantController($rootScope, $scope, $location, $window, $modal,
  $log, $http, Upload, mainService, translationService, $state, $stateParams, $anchorScroll,
  FileUploadService, generalTranslationService, geoService, InventoryService) {
  var CONSTANTS = setScopeConstants();

  function setScopeConstants() {
      var CONSTANTS = $rootScope.CONSTANTS;

      CONSTANTS.MAX_IMAGE_UPLOAD_NO = 5;
      CONSTANTS.DEFAULT_IMG_START_POS = 1;

      // Sync back to $scope
      $scope.CONSTANTS = CONSTANTS;

      return CONSTANTS;
  }

  var self = this;
  this.$scope = $scope;
  // Set some variables in $rootScope to hide the side/top menu based on state
  $scope.merchantId = $stateParams.merchantId;
  $rootScope.settings = $state.current.settings;
  $scope.saveMode = $state.current.saveMode;
  //initialize translation
  $scope.t = mainService.UserReference.TranslationMap;
  $scope.FileUploadService = FileUploadService;
  $scope.generalTranslationService = generalTranslationService;
  $scope.messages = [];
  //model data object
  $scope.mmUsers = []; //cache all mm user for filter use.
  $scope.mmUsersFiltered = []; //display mm use which is filtered by role.
  // Upload error on merchant file upload
  $scope.uploadError = '';

  $scope.form = {};
  $scope.form.Merchant = {};
  $scope.form.MerchantUserIds = []; //only store userid.
  $scope.form.MerchantDocuments = [];
  $scope.form.MerchantBrandIds = []; // brand id array which merchant choosed. note: id stored is not MerchantBrandId, but BrandId which merchant choose
  $scope.form.BillingCatory = [];

  $scope.form.MerchantImageList = [];
  $scope.form.IsTouchedMerchantImages = false;
  
  $scope.closeMessage = function(index) {
    $scope.messages.splice(index, 1);
  };

  $scope.countrySort = InventoryService.countrySort;
  //geoData
  $scope.geoCountries = geoService.countries;
  $scope.geoProvinces = [];
  $scope.geoCities = [];
  
//  $scope.breadcrumbs = [
//    { text : mainService.UserReference.TranslationMap.LB_MM, link : "#/home", icon : "icon-home" }
//  ];
  
    /**
     * Set breadcrumb 
     */
    function setBreadcrumb() {
        $scope.breadcrumbs = [
          { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
          { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant" },
          { text : $scope.form.Merchant['MerchantName' + $rootScope.language], link : "merchantDetailsPage({id: "+$scope.form.Merchant.MerchantId + "})"},
          { text : mainService.UserReference.TranslationMap.LB_EDIT_MERCHANT}
          ];
    }
  
  
  $scope.init = function() {
    // 1.retrieve brands
    var brandpromise = $http.get($rootScope.servicePrefix+'/brand/list', {
      params: {
        cc: $rootScope.User.CultureCode
      }
    });
    brandpromise.then(function(response) {
      $scope.Brands = _.filter(response.data, function (b) {
        return b.StatusId !== CONSTANTS.STATUS.PENDING;
      });
    });

    // 2.retrieve all admin User
    if ($rootScope.isAdmin()) {
      var adminpromise = $http.get($rootScope.servicePrefix+'/user/list/mm');
      adminpromise.then(function(response) {
        $scope.mmUsers = response.data;
      });
    }

    if(!$scope.saveMode){
      //3. retrive the merchant and other data, only for edit merchant
      var hpromise = $http.get($rootScope.servicePrefix+'/merch/view', {
        params: {
          merchantid: $stateParams.merchantId,
          cc: $rootScope.User.CultureCode
        }
      });
      hpromise.then(function(result) {
        $scope.form.Merchant = result.data.Merchant;
        $scope.MerchantName = result.data.Merchant['MerchantName' + $rootScope.language];
        $scope.form.MerchantUserIds = result.data.MerchantUserIds;
        $scope.form.MerchantBrandIds = result.data.MerchantBrands.map(function(item){return item.BrandId});
        $scope.form.MerchantDocuments = result.data.MerchantDocuments;
        $scope.form.MerchantImageList = result.data.MerchantImageList;
        $scope.countrySelected();
        $scope.provinceSelected();
        //build breadcrumbs
        setBreadcrumb();
      }, function(errObj, status, headers, config) {
        $scope.messages = [{
          msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
          type: 'danger'
        }];
      });
    }else{
      $scope.breadcrumbs = [
        { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
        { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant" },
        { text : mainService.UserReference.TranslationMap.LB_CREATE_MERCHANT },
        ];
    }

    // 4.retrieve billings
    var billingpromise = $http.get($rootScope.servicePrefix+'/category/list/billing', {
      params: {
        merchantid: $stateParams.merchantId,
        cc: $rootScope.User.CultureCode
      }
    });
    billingpromise.then(function(result){
      $scope.form.BillingCatory = result.data;
    }, function(errObj, status, headers, config) {
      $scope.messages = [{
        msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
        type: 'danger'
      }];
    });

  };

  $scope.countrySelected = function() {
    if ($scope.form.Merchant&&$scope.form.Merchant.GeoCountryId) {
      geoService.getProvince($scope.form.Merchant.GeoCountryId).then(function(data) {
        $scope.geoProvinces = data;
        if (!_.any(data, function (d) {
              return d.GeoId === _.parseInt($scope.form.Merchant.GeoIdProvince);
            })) {
          $scope.form.Merchant.GeoIdProvince = '';
          $scope.form.Merchant.GeoIdCity = '';
        }
      });
    }
  };

  $scope.provinceSelected = function() {
    if ($scope.form.Merchant&&$scope.form.Merchant.GeoCountryId) {
      geoService.getCities($scope.form.Merchant.GeoIdProvince).then(function(data) {
        $scope.geoCities = data;
        if (!_.any(data, function (d) {
              return d.GeoId === _.parseInt($scope.form.Merchant.GeoIdCity);
            })) {
          $scope.form.Merchant.GeoIdCity = '';
        }
      });
    }
  };

  //select merchant brand
  $scope.$watch('form.MerchantBrandIds', function(newValue, oldValue){
    if($scope.form.MerchantBrandIds.length==1 || newValue.length===0){
      return;
    }
    var merchantTypeId = $scope.form.Merchant.MerchantTypeId;
    if (merchantTypeId === CONSTANTS.MERCHANT_TYPE.BRAND_FLAGSHIP ||
        merchantTypeId === CONSTANTS.MERCHANT_TYPE.MONO_BRAND_AGENT) { //if choose single brand merchant type, remove the old brands, and add new one;
      $scope.form.MerchantBrandIds = [newValue[1]];
    }
  });

  //select merchant type
  $scope.selectMerchantType = function() {
    var merchantTypeId = $scope.form.Merchant.MerchantTypeId;
    if (merchantTypeId === CONSTANTS.MERCHANT_TYPE.BRAND_FLAGSHIP || 
        merchantTypeId === CONSTANTS.MERCHANT_TYPE.MONO_BRAND_AGENT) { //single brand merchant type
      if ($scope.form.MerchantBrandIds.length > 1) {
        $scope.form.MerchantBrandIds = [];
      }
    }
  };

  //only client side delete. after click save btn, it will persist the changes.
  $scope.deleteDocument = function(DocumentKey) {
    _.remove($scope.form.MerchantDocuments, function(item) {
      if (item.DocumentKey === DocumentKey) {
        return true;
      }
    });
  };

  $scope.downloadDocument=function(DocumentKey, DocumentOriginalName){
    var url = $rootScope.servicePrefix+'/document/download?documentkey='+DocumentKey+"&documentoriginalname="+DocumentOriginalName+"&merchantid="+$scope.merchantId
    +"&AccessToken=Bearer "+$rootScope.AuthToken;
    window.location.assign(url);
  }

  $scope.customValidations = [{
    isValid: function () {
      return $scope.form.MerchantBrandIds.length!==0;
    },
    element: function () {
      return angular.element('[name=MerchantBrandIds]').find('input');
    },
    isHidden: false
  }];

  $scope.saveMerchant = function(validationForm) {
    validationForm.$submitted = true;
    if (!validationForm.$valid || !validateBrands()){
      return;
    }
    //set save btn disabled
    $scope.isSaveBtnDisabled = true;
    var params = {
      Merchant: $scope.form.Merchant,
      MerchantUserIds: $scope.form.MerchantUserIds,
      MerchantBrandIds: $scope.form.MerchantBrandIds,
    };
    var hpromise = $http.post($rootScope.servicePrefix+'/merch/save', $scope.form);
    hpromise.then(function(result) {
//      console.log("save success", result);
      // update breadcrumb
      setBreadcrumb();
      $scope.form.IsTouchedMerchantImages = false;
      $scope.messages = [{
        msg: ($scope.saveMode)?$scope.t.MSG_MERCHANT_CREATED:$scope.t.MSG_MERCHANT_EDITED,
        type: 'success'
      }];
      $scope.isSaveBtnDisabled = false;
      if($scope.saveMode){
        $state.go("merchant");
      }
    }, function(errObj, status, headers, config) {
      var errorCodeMsg = ($scope.t[errObj.data.AppCode] === undefined) ? (errObj.data.AppCode) : ($scope.t[errObj.data.AppCode]);
      $scope.messages = [{
        msg: errorCodeMsg,
        type: 'danger'
      }];
      $scope.isSaveBtnDisabled = false;
      $scope.form.IsTouchedMerchantImages = false;
    })
    .finally(function(){
      //scroll to the top of page
      // the element you wish to scroll to.
      $location.hash('page_top');
      // call $anchorScroll()
      $anchorScroll();
    });

    function validateBrands(){
      if($scope.form.MerchantBrandIds.length===0){
        return false;
      }else{
        return true;
      }
    }
  };

  $scope.openChangeStatusDialog = function () {
    var modalInstance = $modal.open({
      templateUrl: "/shared/views/change-status-dialog-merchant.html",
      backdrop: 'static',
      controller: ['$scope', function ($scope) {
        $scope.t = self.$scope.t;
        $scope.user = self.$scope.form.Merchant;

        if ($scope.user.StatusId === CONSTANTS.STATUS.ACTIVE) {
          $scope.user.StatusNameInvariant = 'Active';
        } else if ($scope.user.StatusId === CONSTANTS.STATUS.INACTIVE || $scope.user.StatusId === CONSTANTS.STATUS.PENDING) {
          $scope.user.StatusNameInvariant = 'Inactive';
        }

        $scope.cancel = function () {
          $scope.$dismiss();
        };

        $scope.confirm = function () {
          var newStatus;
          if ($scope.user.StatusNameInvariant === 'Active') {
            newStatus = 'Inactive';
          }
          else if ($scope.user.StatusNameInvariant === 'Inactive') {
            newStatus = 'Active';
          }
          //set confirm btn disabled
          $scope.isConfirmBtnDisabled = true;
          var hpromise = $http.post($rootScope.servicePrefix + '/merch/status/change', {
            MerchantId: $scope.user.MerchantId,
            Status: newStatus
          });
          hpromise.then(function (result) {
            $scope.$close(newStatus);
          }, function (errObj, status, headers, config) {
            $scope.messages = [{
              msg: self.$scope.t.MSG_ERR_CHANGE_STATUS_FAIL + errObj.data.AppCode,
              type: 'danger'
            }];
          }).finally(function () {
            //set confirm btn enabled
            $scope.isConfirmBtnDisabled = false;
          });
        };

        $scope.closeMessage = function (index) {
          $scope.messages.splice(index, 1);
        };
      }]
    });
    modalInstance.result.then(function (newStatus) {
      self.$scope.form.Merchant.StatusNameInvariant = newStatus;
      if (newStatus === 'Active') {
        self.$scope.form.Merchant.StatusId = CONSTANTS.STATUS.ACTIVE;
      } else if (newStatus === 'Inactive') {
        self.$scope.form.Merchant.StatusId = CONSTANTS.STATUS.INACTIVE;
      }
    });
  };

  $scope.cancel = function(){
    $state.go("merchant");
  }

  $scope.uploadImage = function(files) {
    //1.calculate position and build up empty color image list if there is.
    //2.calculate the the number of files which can be uploaded.upload image maximum is 5.
    var position, numOfUpload;
    position = $scope.form.MerchantImageList.length;
    numOfUpload = CONSTANTS.MAX_IMAGE_UPLOAD_NO - position;
    //3. upload image
    if (!files || files.$error || files.upload) return;
    $scope.form.IsTouchedMerchantImages = true; //touch the merchant image list which would trigger the back end merchant image list update.
    files.forEach(function(file) {
      file.upload = {};
      Upload.upload({
        url: $rootScope.servicePrefix+'/image/upload?b=merchantimages&AccessToken=Bearer '+$rootScope.AuthToken, //add accessToken as param to support IE9 since IE9 use flash to upload files. JWT will put accessToken in the header, but flash would not put anything JWT defined in header.
        //fields: fields,
        file: file
      }).error(function(data, status, headers, config) {
        if (window.console) {
            console.log("update file failed!");
        }
      }).success(function(data, status, headers, config) {
        var imageObj = {
          ImageTypeCode: 'Desc',
          MerchantImage: data.ImageKey,
          Position: position++,
        };
        $scope.form.MerchantImageList.push(imageObj);
        $scope.form.MerchantImageList = _.sortBy($scope.form.MerchantImageList, function(n) {
          return n.Position;
        });
      });
      numOfUpload--;
    });
  };

  $scope.deleteImage = function(MerchantImage){
    $scope.form.IsTouchedMerchantImages = true; //touch the merchant image list which would trigger the back end merchant image list update.
    _.remove($scope.form.MerchantImageList, function(n) {
      return (n.MerchantImage === MerchantImage);
    });
  };

  $scope.onDrop = function(target, source) {
    if(target.MerchantImage===source.MerchantImage)
      return;
    $scope.form.IsTouchedMerchantImages = true; //touch the merchant image list which would trigger the back end merchant image list update.
    var imageList = $scope.form.MerchantImageList;
    $scope.form.MerchantImageList = buildPosition(target, source, imageList);

    //inner function for build positioned list
    function buildPosition(target, source, imageList){
      var tmpTargetPosition = target.Position;
      var tmpSourcePosition = source.Position;
      //if swap is forward, target position need to be the next postion.
      if (tmpSourcePosition < tmpTargetPosition) {
        tmpTargetPosition++;
      }
      //1.foreach object in the list and add 1 in position for these object which position is greater equal than targetOrigin.Position
      imageList = _.forEach(imageList, function(n) {
        if (n.Position >= tmpTargetPosition) {
          n.Position++;
        }
      });
      //2.set source object position as target's
      var sourceOrigin = _.find(imageList, function(n) {
        return n.MerchantImage === source.MerchantImage;
      });
      sourceOrigin.Position = tmpTargetPosition;
      //3.sort the list
      imageList = _.sortBy(imageList, function(n) {
        return n.Position;
      });
      //4.reset position for each image starting from 1;
      var startPosition = CONSTANTS.DEFAULT_IMG_START_POS;
      imageList = _.forEach(imageList, function(n) {
        n.Position = startPosition++;
      });
      return imageList;
    }

  };

  $scope.init();
}

angular.module('mmApp').controller('MerchantSaveCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', 'translationService', '$state', '$stateParams', '$anchorScroll', 'FileUploadService',
  'generalTranslationService', 'geoService', 'InventoryService', merchantController
]);
