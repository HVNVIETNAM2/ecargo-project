'use strict';

function InventoryImportCtrl($rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams, InventoryService) {

  $scope.pageSide = 'admin';

  $scope.merchantId = $state.params.merchantId;
  $scope.breadcrumbs = [
    { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
    { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant({id: '" + $scope.merchantId + "'})" },
    {
      text: mainService.UserReference.TranslationMap.LB_INVENTORY_LOCATION,
      link: 'inventoryLocation({merchantId: "' + $scope.merchantId + '"})'
    },
    { text: mainService.UserReference.TranslationMap.LB_IMPORT_PRODUCT_INVENTORY }
  ];

  var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
    params: {
      merchantid: $scope.merchantId,
      cc: $rootScope.User.CultureCode
    }
  });
  hpromise.then(function(result) {
    $scope.Merchant = result.data.Merchant;
    $scope.breadcrumbs.splice(2, 0, { text: $scope.Merchant['MerchantName' + $rootScope.language], link: "merchantDetailsPage({id: '" + $scope.Merchant.MerchantId + "'})"});
  }, function(errObj, status, headers, config) {
    $scope.messages = [{
      msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
      type: 'danger'
    }];
  });

  InventoryService.getLocation({
    params: {
      cc: $rootScope.User.CultureCode,
      id: $state.params.locationId,
      merchantid: $scope.merchantId
    }
  }).then(function (location) {
    $scope.locationName = location.LocationName;
    $scope.breadcrumbs.splice(-1, 0, {
      text: location.LocationName,
      link: 'inventoryProduct({merchantId: "' + $scope.merchantId + '", locationId: "' + location.InventoryLocationId + '"})'
    })
  }, function () {
    $scope.messages = [{
      msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
      type: 'danger'
    }];
  });


  BaseInventoryImportCtrl.call(this, $rootScope, $scope, $location, $window, $modal, $log, $http, Upload, mainService, $state, $stateParams);
}

InventoryImportCtrl.prototype = Object.create(BaseInventoryImportCtrl);
InventoryImportCtrl.prototype.constructor = InventoryImportCtrl;

app.controller('InventoryImportCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', '$state', '$stateParams', 'InventoryService', InventoryImportCtrl
]);
