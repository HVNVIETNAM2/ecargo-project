'use strict';

function MerchantUserEditCtrl($rootScope, $scope, $location, $window, $modal,
  $log, $http, Upload, mainService, translationService, $state, $stateParams, $anchorScroll, FileUploadService, generalTranslationService) {

  $scope.pageSide = "merchant";
  $scope.merchantId = $state.params.merchantId || $rootScope.User.MerchantId;

  $scope.breadcrumbs = [
    { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
    { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant" },
    { text : mainService.UserReference.TranslationMap.LB_USER, link : "merchantUserListing({merchantId: '"+$scope.merchantId+"'})"   },
    { text : mainService.UserReference.TranslationMap.LB_EDIT_USER}
  ];

  if ($scope.merchantId) {
    var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
      params: {
        merchantid: $scope.merchantId,
        cc: $rootScope.User.CultureCode
      }
    });
    hpromise.then(function(result) {
      $scope.Merchant = result.data.Merchant;
      //build breadcrumbs
      $scope.breadcrumbs.splice(2, 0, { text: $scope.Merchant['MerchantName' + $rootScope.language], link: "merchantDetailsPage({id: '" + $scope.Merchant.MerchantId + "'})"});
    }, function(errObj, status, headers, config) {
      $scope.messages = [{
        msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
        type: 'danger'
      }];
    });
  }

  $scope.init = function() {
    var hpromise = $http.get($rootScope.servicePrefix+'/user/view', {
      params: {
        userkey: $stateParams.userKey,
        merchantid: $scope.merchantId
      }
    });
    hpromise.then(function(result) {
      $scope.form = result.data;
      $scope.firstName = $scope.form.FirstName;
      $scope.lastName = $scope.form.LastName;
//      $scope.MerchantId = result.data.MerchantId;

      var hpromise2 = $http.get($rootScope.servicePrefix+'/inventory/location/list', { params: { merchantid : result.data.MerchantId, cc : $rootScope.language } });
      hpromise2.then(function(resultObj){
        $scope.inventoryLocations = resultObj.data;
      });
    }, function(errObj, status, headers, config) {
      $scope.messages = [
        {msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR, type: 'danger'}
      ];
    });
  };

  $scope.init();  // load user profile

  $scope.changeMobileCode = function(code) {
    $scope.form.MobileCode = code;
  };

  BaseUserEditCtrl.call(this, $rootScope, $scope, $location, $window, $modal,
    $log, $http, Upload, mainService, translationService, $state, $stateParams, $anchorScroll, FileUploadService, generalTranslationService);
}

MerchantUserEditCtrl.prototype = Object.create(BaseUserEditCtrl.prototype);
MerchantUserEditCtrl.prototype.constructor = MerchantUserEditCtrl;
angular.module('mmApp').controller('MerchantUserEditCtrl', ['$rootScope', '$scope', '$location', '$window', '$modal',
  '$log', '$http', 'Upload', 'mainService', 'translationService', '$state', '$stateParams', '$anchorScroll', 'FileUploadService', 'generalTranslationService', MerchantUserEditCtrl]);

