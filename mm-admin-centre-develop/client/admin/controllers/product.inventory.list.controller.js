'use strict';

/**
 * Product Inventory List Controller
 * @param {type} $rootScope
 * @param {type} $scope
 * @param {type} $state
 * @param {type} $modal
 * @param {type} $loading
 * @param {type} $http
 * @param {type} RequestService
 * @param {type} InventoryService
 * @param {type} mainService
 * @param {type} generalTranslationService
 * @constructor
 */
function ProductInventoryListCtrl($rootScope, $scope, $state, $modal, $loading, $http, RequestService, InventoryService, mainService, generalTranslationService) {
    this.$state = $state;
    this.$modal = $modal;
    $scope.pageSide = 'admin';
    this.InventoryService = InventoryService;
    $scope.t = mainService.UserReference.TranslationMap;

    $scope.merchantId = $state.params.merchantId;
    $scope.breadcrumbs = [
        { text : mainService.UserReference.TranslationMap.LB_MM, link : "adminHome", icon : "icon-home" },
        { text : mainService.UserReference.TranslationMap.LB_MERCHANT, link : "merchant({id: '" + $scope.merchantId + "'})" }
    ];


    var hpromise = $http.get($rootScope.servicePrefix + '/merch/view', {
        params: {
            merchantid: $scope.merchantId,
            cc: $rootScope.User.CultureCode
        }
    });
    hpromise.then(function(result) {
        $scope.Merchant = result.data.Merchant;
        $scope.breadcrumbs.splice(2, 0, { text: $scope.Merchant['MerchantName' + $rootScope.language], link: "merchantDetailsPage({id: '" + $scope.Merchant.MerchantId + "'})"});
    }, function(errObj, status, headers, config) {
        $scope.messages = [{
            msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR,
            type: 'danger'
        }];
    });



    BaseProductInventoryListCtrl.call(this, $rootScope, $scope, $state, $modal, $loading, $http, RequestService, InventoryService, mainService, generalTranslationService);
}

ProductInventoryListCtrl.prototype = Object.create(BaseProductInventoryListCtrl.prototype);
ProductInventoryListCtrl.prototype.constructor = BaseProductInventoryListCtrl;

app.controller('productInventoryListCtrl', ProductInventoryListCtrl);
