
app.controller('SearchCustomCtrl', ['$q','$rootScope', '$scope', '$location', '$window', '$modal',
  '$http', 'mainService', '$state', '$stateParams', 'TranslationService', 'GeneralTranslationService',
  function ($q, $rootScope, $scope, $location, $window, $modal, $http, mainService, $state, $stateParams, TranslationService, GeneralTranslationService) {
    var CONSTANTS = $rootScope.CONSTANTS;
    
    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.t = mainService.UserReference.TranslationMap;

    $scope.MerchantId = $stateParams.id;
    $scope.Merchant = {};

    // Scopes for Getting Table Data's..
		var tmpAllData = [];
		$scope.allData = [];
		$scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
		$scope.sortBy = 'Priority';
		$scope.reverse = true;
		$scope.filter = "";

		$scope.TypeCode = "Custom";

		$scope.moving = false;

		$scope.searchTerm = "";

		$scope.getData = function(){
	    $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
	    $scope.allData = [];
	    
	    var hpromise = $http.get($rootScope.servicePrefix + '/terms/list', { params: { typecode : $scope.TypeCode, cc : $rootScope.language } });
	    hpromise.then(function(resultObj){
	    	var data = resultObj.data;
	      $scope.allData = data;
	      tmpAllData = data;
	    });
	  };

		$scope.addSearchTerm = function(){
			if($scope.searchTerm && $scope.searchTerm !== ""){
				$http.post($rootScope.servicePrefix + '/terms/insert', {
					SearchTermTypeCode : $scope.TypeCode,
					SearchTerm : $scope.searchTerm,
					Priority : tmpAllData.length
				}).then(function(response){
					if(response.status === 200){
						tmpAllData.push(response.data);
					}
				})
			}
		};

		$scope.filterBySearch = function(){
	    $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
	    $scope.allData = tmpAllData;
	    var lowercaseTerm = $scope.search.toLowerCase();

	    $scope.allData = _.filter($scope.allData, function(item, index){
	      if (item.SearchTerm && item.SearchTerm.toLowerCase().indexOf(lowercaseTerm) > -1) {
	        return true;
	      }
	    });
	  };

		$scope.save = function(item){
			var oldItem = item;
			var modalInstance = $modal.open({
	          templateUrl: "/shared/views/confirm-search-dialog.html",
	          backdrop : 'static',
	          resolve: {
	            mainService: function() {
	              return mainService;
	            }
	          },
	          controller: ['$scope', 'mainService', function($scope, mainService) {
	            $scope.item = item;
	            $scope.t = mainService.UserReference.TranslationMap;

	            $scope.cancel = function() {
	              $scope.$dismiss();
	            };

	            $scope.confirm = function() {
	              $scope.isConfirmBtnDisabled = true;
	              var hpromise = $http.post($rootScope.servicePrefix + '/terms/update', item);

	              hpromise.then(function(response){
	              	if(response.status === 200){
	              		$scope.$close(true);
	              	}else{
	              		$scope.messages = [
		                  {msg: 'Failed to update search term. Please try again later.', type: 'danger'}
		                ];
	              	}
	              }, function(errObj, status, headers, config) {
	                $scope.messages = [
	                  {msg: 'Failed to update search term. Please try again later.', type: 'danger'}
	                ];
	              }).finally(function(){
	                $scope.isConfirmBtnDisabled = false;
	              });
	            };

	            $scope.closeMessage = function(index) {
	              $scope.messages.splice(index, 1);
	            };
	          }]
      		});

		    modalInstance.result.then(function(newEmail){
		      _.pull($scope.allData, oldItem);
		      $scope.allData.push(item);
		    });
		};

		$scope.delete = function(item, index){
			var modalInstance = $modal.open({
	          templateUrl: "/shared/views/delete-search-dialog.html",
	          backdrop : 'static',
	          resolve: {
	            mainService: function() {
	              return mainService;
	            }
	          },
	          controller: ['$scope', 'mainService', function($scope, mainService) {
	            $scope.item = item;
	            $scope.t = mainService.UserReference.TranslationMap;

	            $scope.cancel = function() {
	              $scope.$dismiss();
	            };

	            $scope.confirm = function() {
	              $scope.isConfirmBtnDisabled = true;
	              var hpromise = $http.post($rootScope.servicePrefix + '/terms/delete', item);

	              hpromise.then(function(response){
	              	if(response.status === 200){
	              		$scope.$close(true);
	              	}else{
	              		$scope.messages = [
		                  {msg: 'Failed to delete search term. Please try again later.', type: 'danger'}
		                ];
	              	}
	              }, function(errObj, status, headers, config) {
	                $scope.messages = [
	                  {msg: 'Failed to delete search term. Please try again later.', type: 'danger'}
	                ];
	              }).finally(function(){
	                $scope.isConfirmBtnDisabled = false;
	              });
	            };

	            $scope.closeMessage = function(index) {
	              $scope.messages.splice(index, 1);
	            };
	          }]
      		});

			  modalInstance.result.then(function(data){
			    _.pull($scope.allData, item);
			  });
		};


	  $scope.sortTable = function(sortBy){
              $scope.currentPage = CONSTANTS.DEFAULT_PAGE_NO;
	    if($scope.sortBy === sortBy){
	      $scope.reverse = $scope.reverse === false ? true: false;
	    }else{
	      $scope.reverse = false;
	    }
	    $scope.sortBy = sortBy;
	  };

	  $scope.getData();
    
  }
]);
