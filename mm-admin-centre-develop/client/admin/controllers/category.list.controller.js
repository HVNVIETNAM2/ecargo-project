'use strict';

app.factory('CategoryListService', function () {
  return {
    expanded: {}
  };
});

function CategoryListCtrl ($rootScope, $scope, $q, $state, $timeout, $modal, $loading, $http, RequestService, mainService, CategoryListService) {
    var CONSTANTS = setScopeConstants();
    
    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;

        CONSTANTS.ROWACTION = {
            EDIT: 1,
            CHANGE_STATUS: 2,
            //DELETE_CATEGORY: 3,
            CREATE_CATEGORY: 4
        };

        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }
    
  $scope.t = mainService.UserReference.TranslationMap;
  $scope.breadcrumbs = [
    { text : $scope.t.LB_MM, link : "adminHome", icon : "icon-home" },
    {
      text : $scope.t.LB_AC_CATEGORY,
      link: 'category'
    }
  ];

  $rootScope.settings = $state.current.settings;
  $scope.mainService = mainService;
  $scope.RequestService = RequestService;

  $scope.go = $state.go;
  $scope.expanded = CategoryListService.expanded;

  $scope.messages = [];
  $scope.locationId = $state.params.locationId;

  $scope.data = [];
  $scope.reverse = false;

  //messages
  if ($state.params.msg && $state.params.type) {
    $scope.messages = [{
      msg: $state.params.msg,
      type: $state.params.type
    }];
  }

  $scope.$watch('loading', function (flag) {
    if (flag) $loading.start('loading');
    else $loading.finish('loading');
  });

  $scope.getData = function () {
    $scope.loading = true;
    return $http.get($rootScope.servicePrefix+'/category/list', {
      params: {
        'cc': $rootScope.language,
        'showAll': true
      }
    })
    .then(function (result) {
      $scope.data = result.data.slice(1);
    }, function () {
      $scope.messages = [
        {msg: $scope.t.MSG_ERR_ADMIN_GENERAL_ERR, type: 'danger'}
      ]
    })
    .then(function () {
      $scope.loading = false;
    });
  };

  $scope.getData();

  $scope.treeOptions = {
    /*
    accept: function (source, dest) {
      // only accept drop within same list
      return dest.$modelValue.indexOf(source.$modelValue) > -1;
    },
    */
    beforeDrop: function (evt) {
      var CategoryId = evt.source.nodeScope.$modelValue.CategoryId;
      var ParentCategoryId = evt.dest.nodesScope.$nodeScope
        ? evt.dest.nodesScope.$nodeScope.$modelValue.CategoryId
        : 0;
      var Priority = evt.dest.index + 1;

      return $http.post($rootScope.servicePrefix+'/category/reposition', {
        CategoryId: CategoryId,
        ParentCategoryId: ParentCategoryId,
        Priority: Priority
      }).then(function (result) {
        return true;
      }).catch(function (err) {
        $scope.messages = [
          {msg: 'Failed to reposition list. Please try again later.', type: 'danger'}
        ];
        throw err;
      })
    }
  };

  $scope.selectAction = function (item, scope) {
    var action = parseInt(item.rowAction);
    switch (action) {
      // edit
      case CONSTANTS.ROWACTION.EDIT:
        $state.go('categoryEdit', {categoryId: item.CategoryId});
        break;
      // change status
      case CONSTANTS.ROWACTION.CHANGE_STATUS:
        $scope.openStatusDialog(item, scope);
        break;
      //case CONSTANTS.ROWACTION.DELETE_CATEGORY:
        //$scope.openDeleteCategoryDialog(item, scope);
        //break;
      // create sub category
      case CONSTANTS.ROWACTION.CREATE_CATEGORY:
        $state.go('categoryCreate', {
          parentCategoryId: item.CategoryId
        });
        break;
    }
    item.rowAction = '';
  };

  $scope.openStatusDialog = function (item) {
    var _$scope = $scope;
    return $modal.open({
      templateUrl: "/shared/views/change-status-dialog-brand.html",
      backdrop : 'static',
      resolve: {
        mainService: function() {
          return mainService;
        }
      },
      controller: ['$scope', function($scope) {
        // Translation data
        $scope.t = mainService.UserReference.TranslationMap;
        $scope.brand = item;
        $scope.BrandName = item.BrandName;
        $scope.cancel = function() {
          $scope.$dismiss();
        };
        $scope.confirm = function () {
            var newStatus;
            if (item.StatusId === CONSTANTS.STATUS.ACTIVE || item.StatusNameInvariant === 'Active') {
                newStatus = 'Inactive';
            }
            else if (item.StatusId === CONSTANTS.STATUS.INACTIVE || item.StatusNameInvariant === 'Inactive') {
                newStatus = 'Active';
            }
            //set confirm btn disabled
            $scope.isConfirmBtnDisabled = true;
            var hpromise = $http.post($rootScope.servicePrefix + '/brand/status/change', {
                BrandId: $scope.brand.BrandId,
                Status: newStatus
            });
            hpromise.then(function (result) {
                $scope.$close(newStatus);
                _$scope.getData();
            }, function (errObj, status, headers, config) {
                $scope.messages = [{
                    msg: $scope.t.MSG_ERR_CHANGE_STATUS_FAIL + errObj.data.AppCode,
                    type: 'danger'
                }];
            }).finally(function () {
                //set confirm btn enabled
                $scope.isConfirmBtnDisabled = false;
            });
        };

        $scope.closeMessage = function (index) {
          $scope.messages.splice(index, 1);
        };
      }]
    });
  }

  //delete category
    /**
  $scope.openDeleteCategoryDialog = function(category, scope) {
    var modalInstance = $modal.open({
        templateUrl: "views/confirm-delete-category-dialog.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          }
        },
        controller: ['$scope', function($scope) {
          $scope.category = category;
          //global util module outside of angular.
          // Translation data
          $scope.t = mainService.UserReference.TranslationMap;
          $scope.cancel = function() {
            $scope.$dismiss();
          };

          $scope.confirm = function() {
            //set confirm btn disabled
            $scope.isConfirmBtnDisabled = true;
            var hpromise = $http.post($rootScope.servicePrefix+'/category/delete', {
              "CategoryId": category.CategoryId,
            });
            hpromise.then(function(result) {
              $scope.$close(true);
            }, function(errObj, status, headers, config) {
              var errorCodeMsg = ($scope.t[errObj.data.AppCode] === undefined) ? (errObj.data.AppCode) : ($scope.t[errObj.data.AppCode]);
              $scope.messages = [{
                msg: errorCodeMsg,
                type: 'danger'
              }];
            }).finally(function(){
              //set confirm btn enabled
              $scope.isConfirmBtnDisabled = false;
            });
          };

          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
    });
    modalInstance.result.then(function(data) {
      // remove item from list if successful
      scope.remove();
    });
  };
**/
  // activate pending
  $scope.openActivatePendingDialog = function() {
    var _$scope = $scope;
    var modalInstance = $modal.open({
        templateUrl: "views/confirm-activate-pending-category-dialog.html",
        backdrop : 'static',
        resolve: {
          mainService: function() {
            return mainService;
          }
        },
        controller: ['$scope', function($scope) {
          //global util module outside of angular.
          // Translation data
          $scope.t = mainService.UserReference.TranslationMap;
          $scope.cancel = function() {
            $scope.$dismiss();
          };

          $scope.confirm = function() {
            //set confirm btn disabled
            $scope.isConfirmBtnDisabled = true;

            return $http.post($rootScope.servicePrefix+'/category/status/activate')
            .then(function () {
              return _$scope.getData();
            })
            .then(function() {
              $scope.$close(true);
            })
            .catch(function(err, status, headers, config) {
              $scope.messages = [{
                msg: err.data.message || err.data,
                type: 'danger'
              }];
            })
            .then(function(){
              //set confirm btn enabled
              $scope.isConfirmBtnDisabled = false;
            });
          };

          $scope.closeMessage = function(index) {
            $scope.messages.splice(index, 1);
          };
        }]
    });
    modalInstance.result.then(function(data) {
      // remove item from list if successful
      scope.remove();
    });
  };
}

app.controller('CategoryListCtrl', ['$rootScope', '$scope', '$q', '$state', '$timeout', '$modal', '$loading', '$http', 'RequestService', 'mainService', 'CategoryListService', CategoryListCtrl]);
