'use strict';

/**
 * Brand Management Controller
 * @param $rootScope
 * @param $scope
 * @param $state
 * @param $http
 * @param $modal
 * @param $loading
 * @param mainService
 * @param generalTranslationService
 * @param FileUploadService
 * @param Upload
 * @param BrandService
 * @constructor
 */
function BrandCtrl($rootScope, $scope, $state, $http, $modal, $loading, mainService, generalTranslationService, FileUploadService, Upload, $location, BrandService) {
    var CONSTANTS = setScopeConstants();
    
    function setScopeConstants() {
        var CONSTANTS = $rootScope.CONSTANTS;
        
        CONSTANTS.MAX_IMAGE_UPLOAD_NO = 5;
        CONSTANTS.DEFAULT_IMG_START_POS = 1;
        
        // Sync back to $scope
        $scope.CONSTANTS = CONSTANTS;

        return CONSTANTS;
    }
    
    var self = this;
    this.$scope = $scope;
    $scope.FileUploadService = FileUploadService;
    $scope.t = mainService.UserReference.TranslationMap;
    $rootScope.settings = $state.current.settings;
    $scope.mainService = mainService;
    $scope.generalTranslationService = generalTranslationService;
    $scope.LanguageCode = $rootScope.User.CultureCode;
    $scope.IsTouchedBrandImages = false;
    $scope.BrandImageList = [];
    $scope.brand = {};
    $scope.loading = false;
    $scope.BrandService = BrandService;

    /**
     * brand id for current view
     */
    $scope.brandId = $state.params.brandId;
    /**
     * is current view a editing view
     */
    $scope.isEdit = $scope.brandId !== undefined;
    $scope.messages = [];

    $scope.breadcrumbs = [{
        text: $scope.t.LB_STOREFRONT_APP_NAME,
        link: 'home',
        icon: "icon-home"
    }, {
        link: 'brand',
        text: $scope.t.LB_AC_BRAND
    }, {
        text: $scope.isEdit ? $scope.t.LB_AC_EDIT_BRAND : $scope.t.LB_AC_CREATE_BRAND
    }];


    if ($scope.isEdit) {
        $scope.loading = true;
        $loading.start('loading');
        //get from server and show loading icon
        BrandService.get($scope.brandId).then(function (data) {
            if (!_.isUndefined(data.Brand.BrandId)) {
                $scope.brand = data.Brand;
                $scope.BrandName = $scope.brand.DisplayName[$scope.LanguageCode];
                $scope.BrandImageList = data.BrandImageList;
            } else {
                alert('No Such Brand Exists');
                $state.go('brand');
            }
        }, function (err) {
            if (window.console) {
                console.log(err);
            }
            $scope.messages = [{
                type: 'danger',
                msg: $scope.t[err.AppCode] || err.Message || err.toString()
            }];
        }).finally(function () {
            $scope.loading = false;
            $loading.finish('loading');
        });
    }

    $scope.openChangeStatusDialog = function () {
        var modalInstance = $modal.open({
            templateUrl: "/shared/views/change-status-dialog-brand.html",
            backdrop: 'static',
            controller: ['$scope', function ($scope) {
                $scope.t = self.$scope.t;
                $scope.brand = self.$scope.brand;
                $scope.BrandName = $scope.brand.DisplayName[self.$scope.LanguageCode];

                $scope.cancel = function () {
                    $scope.$dismiss();
                };

                $scope.confirm = function () {
                    var newStatus;
                    if ($scope.brand.StatusNameInvariant === 'Active') {
                        newStatus = 'Inactive';
                    }
                    else if ($scope.brand.StatusNameInvariant === 'Inactive' ||
                        $scope.brand.StatusNameInvariant === 'Pending') {
                        newStatus = 'Active';
                    }
                    //set confirm btn disabled
                    $scope.isConfirmBtnDisabled = true;
                    var hpromise = $http.post($rootScope.servicePrefix + '/brand/status/change', {
                        BrandId: $scope.brand.BrandId,
                        Status: newStatus
                    });
                    hpromise.then(function (result) {
                        $scope.$close(newStatus);
                    }, function (errObj, status, headers, config) {
                        $scope.messages = [{
                            msg: self.$scope.t.MSG_ERR_CHANGE_STATUS_FAIL + errObj.data.AppCode,
                            type: 'danger'
                        }];
                    }).finally(function () {
                        //set confirm btn enabled
                        $scope.isConfirmBtnDisabled = false;
                    });
                };

                $scope.closeMessage = function (index) {
                    $scope.messages.splice(index, 1);
                };
            }]
        });
        modalInstance.result.then(function (newStatus) {
            self.$scope.brand.StatusNameInvariant = newStatus;
            if (newStatus === 'Active') {
                self.$scope.brand.StatusId = CONSTANTS.STATUS.ACTIVE;
            } else if (newStatus === 'Inactive') {
                self.$scope.brand.StatusId = CONSTANTS.STATUS.INACTIVE;
            }
        });
    };

    $scope.uploadImage = function (files) {
        //1.calculate position and build up empty color image list if there is.
        //2.calculate the the number of files which can be uploaded.upload image maximum is 5.
        var position, numOfUpload;
        position = 0;
        numOfUpload = CONSTANTS.MAX_IMAGE_UPLOAD_NO;
        //3. upload image
        if (!files || files.$error || files.upload) return;
        $scope.IsTouchedBrandImages = true; //touch the Brand image list which would trigger the back end Brand image list update.
        files.forEach(function (file) {
            if (numOfUpload <= 0) {
                if (window.console) {
                    console.log('no more file allowed be upload.');
                }   
                return;
            }
            file.upload = {};
            Upload.upload({
                url: $rootScope.servicePrefix + '/image/upload?b=brandimages&AccessToken=Bearer ' + $rootScope.AuthToken, //add accessToken as param to support IE9 since IE9 use flash to upload files. JWT will put accessToken in the header, but flash would not put anything JWT defined in header.
                //fields: fields,
                file: file
            }).error(function (data, status, headers, config) {
                if (window.console) {
                    console.log("update file failed!");
                }
            }).success(function (data, status, headers, config) {
                var imageObj = {
                    ImageTypeCode: 'Desc',
                    BrandImage: data.ImageKey,
                    Position: position++,
                };
                $scope.BrandImageList.push(imageObj);
                $scope.BrandImageList = _.sortBy($scope.BrandImageList, function (n) {
                    return n.Position;
                });
            });
            numOfUpload--;
        });
    };

    $scope.deleteImage = function (BrandImage) {
        $scope.IsTouchedBrandImages = true; //touch the Brand image list which would trigger the back end Brand image list update.
        _.remove($scope.BrandImageList, function (n) {
            return (n.BrandImage === BrandImage);
        });
    };

    $scope.onDrop = function (target, source) {
        if (target.BrandImage === source.BrandImage)
            return;
        $scope.IsTouchedBrandImages = true; //touch the Brand image list which would trigger the back end Brand image list update.
        var imageList = $scope.BrandImageList;
        $scope.BrandImageList = buildPosition(target, source, imageList);

        //inner function for build positioned list
        function buildPosition(target, source, imageList) {
            var tmpTargetPosition = target.Position;
            var tmpSourcePosition = source.Position;
            //if swap is forward, target position need to be the next postion.
            if (tmpSourcePosition < tmpTargetPosition) {
                tmpTargetPosition++;
            }
            //1.foreach object in the list and add 1 in position for these object which position is greater equal than targetOrigin.Position
            imageList = _.forEach(imageList, function (n) {
                if (n.Position >= tmpTargetPosition) {
                    n.Position++;
                }
            });
            //2.set source object position as target's
            var sourceOrigin = _.find(imageList, function (n) {
                return n.BrandImage === source.BrandImage;
            });
            sourceOrigin.Position = tmpTargetPosition;
            //3.sort the list
            imageList = _.sortBy(imageList, function (n) {
                return n.Position;
            });
            //4.reset position for each image starting from 1;
            var startPosition = CONSTANTS.DEFAULT_IMG_START_POS;
            imageList = _.forEach(imageList, function (n) {
                n.Position = startPosition++;
            });
            return imageList;
        }
    };


    $scope.save = function () {
        $scope.loading = true;
        BrandService.save($scope.brand, $scope.BrandImageList, $scope.IsTouchedBrandImages).then(function (brand) {
            if (brand && brand.BrandId) {
                if ($scope.isEdit) {
                    $scope.messages = [{
                        type: 'success',
                        msg: $scope.t.MSG_SUC_BRAND_EDIT
                    }];
                } else {
                    $state.go('brand', {msg: 'MSG_SUC_BRAND_CREATE', type: 'success'});
                }
            }
        }, function (err) {
            if (window.console) {
                console.log(err);
            }
            $scope.messages = [{
                type: 'danger',
                msg: $scope.t[err.AppCode] || err.Message || err.toString()
            }];
        }).finally(function () {
            $scope.loading = false;
            //scroll to the top of page
            $location.hash('page_top');
            $anchorScroll();
        });
    };
}

app.controller('BrandCtrl', ['$rootScope', '$scope', '$state', '$http', '$modal', '$loading', 'mainService', 'generalTranslationService', 'FileUploadService', 'Upload', '$location', 'BrandService', BrandCtrl]);
