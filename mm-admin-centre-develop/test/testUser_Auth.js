var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

var app = require('../services/main.js');
var CONSTANT = require('../logic/constants.js');

describe('User with valid authentication', function(){

  it('should list MM Users', function(done){
    request
      .get('/api/user/list/mm')
      .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(200, done);
  })

  it('should view a user', function(done){
    request
      .get('/api/user/view?id=349')
      .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(200, done);
  })
});
