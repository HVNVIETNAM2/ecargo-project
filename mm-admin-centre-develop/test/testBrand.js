var testCommon = require('./test.common.js')();
var request = testCommon.supertest();
var CONSTANT = require('../logic/constants.js');

var randomNameDigi = Math.random().toString().substring(0,7);

var newBrand = {
  BrandCode: 'BrandCode ' + randomNameDigi,
  BrandSubdomain: 'Subdomain ' + randomNameDigi,
  DisplayName: {
    EN: 'New Brand (EN) ' + randomNameDigi,
    CHT: 'New Brand (CHT) ' + randomNameDigi,
    CHS: 'New Brand (CHS) ' + randomNameDigi
  }
}

describe('Brand Service', function(){
  it('should list all brands with missing parameter', function(done){
    request
      .get('/api/brand/list')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })


  it('should list all brands', function(done){
    request
      .get('/api/brand/list?cc=EN')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })


  it('should create a new brand', function(done){
    request
        .post('/api/brand/save')
        .send({Brand: newBrand})
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res){

          if (err) return done(err);
          newBrand.BrandId = res.body.BrandId;
          done();
        });
  })

  it('should update a brand just created', function(done){
    newBrand.BrandCode = 'Updated Code ' + randomNameDigi;
    request
        .post('/api/brand/save')
        .send({Brand: newBrand})
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res){

          if (err) return done(err);
          done();
        });
  })

  it('should get a brand', function(done){
    request
        .get('/api/brand/view?brandid=' + newBrand.BrandId)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
  })

});
