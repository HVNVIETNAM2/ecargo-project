var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

describe('Public Reference Service', function(){


   it('Brand List', function(done){
	    request
	      .get('/api/brand/list?cc=EN')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(function(res){
	      	if(res.body && res.body.length > 0){

	      	}else{
	      		 throw new Error("Brand list failed");
	      	}
	      })
	      .expect(200, done);
	})

    it('Category List', function(done){
	    request
	      .get('/api/category/list?cc=EN')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(function(res){
	      	if(res.body && res.body.length > 0){

	      	}else{
	      		 throw new Error("Category list failed");
	      	}
	      })
	      .expect(200, done);
	})

	it('Category List Adjacency', function(done){
	    request
	      .get('/api/category/list/adjacency?cc=EN')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(function(res){
	      	if(res.body && res.body.length > 0){

	      	}else{
	      		 throw new Error("Category list adjacency failed");
	      	}
	      })
	      .expect(200, done);
	})

   it('Size List', function(done){
	    request
	      .get('/api/size/list?cc=EN')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(function(res){
	      	if(res.body && res.body.length > 0){

	      	}else{
	      		 throw new Error("Size list failed");
	      	}
	      })
	      .expect(200, done);
	})

    it('Color List', function(done){
	    request
	      .get('/api/color/list?cc=EN')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(function(res){
	      	if(res.body && res.body.length > 0){

	      	}else{
	      		 throw new Error("Color list failed");
	      	}
	      })
	      .expect(200, done);
	})

	it('Badge List', function(done){
	    request
	      .get('/api/badge/list?cc=EN')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(function(res){
	      	if(res.body && res.body.length > 0){

	      	}else{
	      		 throw new Error("Badge list failed");
	      	}
	      })
	      .expect(200, done);
	})
 
});	
