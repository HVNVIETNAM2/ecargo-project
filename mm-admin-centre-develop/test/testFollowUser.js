var testCommon = require('./test.common.js')();
var request = testCommon.supertest();
var assert = require('assert');

var CONSTANT = require('../logic/constants.js');
var UserKey = null;
var ToUserKey = null;
var UserInfo = {};

describe('Test Follow User', function(){

  it('should login as test@ecargo.com', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "test@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      UserKey = res.body.UserKey;
      done();
    });
  })

  it('should login as merch@ecargo.com', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "merch@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      ToUserKey = res.body.UserKey;
      done();
    });
  })

  it('should delete follow to user', function(done){
    request
    .post('/api/follow/user/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToUserKey : ToUserKey })
    .expect(200, done)
  })

  it('should get details of user', function(done){
    request
    .get('/api/view/user?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        UserInfo = res.body;
      }
    })
    .expect(200, done)
  })

  it('should follow a user 1', function(done){
    request
    .post('/api/follow/user/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToUserKey : ToUserKey })
    .expect(200, done)
  })

  it('should follow a user 2', function(done){
    request
    .post('/api/follow/user/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToUserKey : ToUserKey })
    .expect(200, done)
  })

  it('should increment FollowingUserCount by 1', function(done){
    request
    .get('/api/view/user?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body){
        if((UserInfo.FollowingUserCount + 1) == res.body.FollowingUserCount){
          UserInfo = res.body;  
        }else{
          throw 'should increment FollowingUserCount by 1';
        }
      }else{
        throw 'should increment FollowingUserCount by 1';
      }
    })
    .expect(200, done)
  })

  it('first item should be equal to ToUserKey', function(done){
    request
    .get('/api/follow/user/followed?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body && res.body[0].UserKey == ToUserKey){

      }else{
        throw new Error("first item should be equal to ToUserKey");
      }
    })
    .expect(200, done);
  })

  it('first item should be equal to UserKey which is self', function(done){
    request
    .get('/api/follow/user/following?UserKey=' + ToUserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body && res.body[0].UserKey == UserKey){
        console.log(res.body)
      }else{
        throw new Error("first item should be equal to UserKey which is self");
      }
    })
    .expect(200, done);
  })

  it('should delete follow to user 1', function(done){
    request
    .post('/api/follow/user/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToUserKey : ToUserKey })
    .expect(200, done)
  })

  it('should delete follow to user 2', function(done){
    request
    .post('/api/follow/user/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToUserKey : ToUserKey })
    .expect(200, done)
  })

  it('should decrement FollowingUserCount by 1', function(done){
    request
    .get('/api/view/user?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body){
        if((UserInfo.FollowingUserCount - 1) == res.body.FollowingUserCount){
          UserInfo = res.body;  
        }else{
          throw 'should decrement FollowingUserCount by 1';
        }
      }else{
        throw 'should decrement FollowingUserCount by 1';
      }
    })
    .expect(200, done)
  })
}); 