var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

var CONSTANT = require('../logic/constants.js');


var UserKey = null;
var cc = "EN";

var InsertedTagId = null;
var InsertTag = {
  TagTypeId : null,
  TagNameEN : "Test Tag EN",
  TagNameCHT : "Test Tag CHT",
  TagNameCHS : "Test Tag CHS",
};

var InsertedTagTypeId = null;
var InsertTagType = {
  TagTypeName : "Test Tag Type"
};

var MerchantTag = {
  MerchantId : null,
  TagIds : [
    { TagId : 2, Priority : 0 }
  ]
};

var UserTag = {
  UserKey : null,
  TagIds : [
    { TagId : 2, Priority : 0 }
  ]
};

describe('Tags', function(){

  before('should get sample UserKey from User', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "merch@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      MerchantTag.MerchantId = res.body.MerchantId;
      UserKey = testCommon.getUserKey();
      UserTag.UserKey = testCommon.getUserKey();
      done();
    });
  })

  // Tag Type
  it('should save a new tag type', function(done){
    request
    .post('/api/tags/type/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send(InsertTagType)
    .expect(function(res){
      if(!res.body){
        throw new Error("should save a new tag type failed");
      }else{
        InsertedTagTypeId = res.body.TagTypeId;
        InsertTag.TagTypeId = res.body.TagTypeId;
      }
    })
    .expect(200, done)
  })

  // Tags
  it('should save a new tag', function(done){
    request
    .post('/api/tags/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send(InsertTag)
    .expect(function(res){
      if(!res.body){
        throw new Error("should save a new tag failed");
      }else{
        InsertedTagId = res.body.TagId;
      }
    })
    .expect(200, done)
  })

  it('should list all tag per tagtypeid', function(done){
    request
    .get('/api/tags/list?tagtypeid=' + InsertedTagTypeId + "&cc=" + cc)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all tag per tagtypeid failed");
      }
    })
    .expect(200, done);
  })

  it('should delete tag', function(done){
    request
    .post('/api/tags/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ TagId : InsertedTagId })
    .expect(200, done)
  })

  
  it('should delete tag type', function(done){
    request
    .post('/api/tags/type/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ TagTypeId : InsertedTagTypeId })
    .expect(200, done)
  })

  // Merchant Tag
  it('should save merchant tag list', function(done){
    request
    .post('/api/tags/merchant/save')
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send(MerchantTag)
    .expect(200, done)
  })

  // User Tag
  it('should save user tag list', function(done){
    request
    .post('/api/tags/user/save')
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send(UserTag)
    .expect(200, done)
  })

  it('should list all merchants listed per user tags', function(done){
    request
    .get('/api/tags/user/merchant?userkey=' + UserKey + "&cc=" + cc)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all merchants listed per user tags failed");
      }
    })
    .expect(200, done);
  })

  it('should list all user curators per user tags', function(done){
    request
    .get('/api/tags/user/curator?userkey=' + UserKey + "&cc=" + cc)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all user curators per user tags failed");
      }
    })
    .expect(200, done);
  })

}); 