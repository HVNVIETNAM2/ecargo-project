var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

var CartKey = null;
var CartItemId = null;
var CartUser = "1fca2b93-c3e5-11e5-9195-06e517c0a113";
var ItemSku = 518;
var MergeId = null;
var continueTest = true;

describe('Cart', function(){

	it('Get 1 SKU from DB', function(done){
	    request
	      .get('/api/product/sku/list?cc=EN&page=1&size=25')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(function(res){
	      		if(res.body && res.body.PageData){
	      			if(res.body.PageData.length > 0){
	      				ItemSku = res.body.PageData[ res.body.PageData.length - 1 ].SkuId;
	      			}else{
	      				continueTest = false;
	      				throw new Error("FAILED TO GET SKU");
	      			}
	      		}else{
	      			continueTest = false;
	      			throw new Error("FAILED TO GET SKU");
	      		}
	      })
	      .expect(200, done);
	})

   it('should create a new cart for user id 0', function(done){
    request
      .post('/api/cart/create')
	  .send({ CultureCode : "EN", UserKey : 0 })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		if (res.body && res.body.CartKey){
				CartKey = res.body.CartKey;
	  		}else{
	  			throw new Error("Cart Creation failed");
	  		}
      })
      .expect(200, done);
  })

  it('should update the cart into the authenticated user', function(done){
    request
      .post('/api/cart/user/update')
	  .send({ CultureCode : "EN", UserKey : CartUser, CartKey : CartKey })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		// if(!res.body.UserId || !(res.body.UserId == CartUser))
	  		// 	throw new Error("Cart User Update failed");
      })
      .expect(200, done);
  })

  it('add new item on the cart', function(done){
    request
      .post('/api/cart/item/add')
	  .send({ CultureCode : "EN", CartKey : CartKey, SkuId : ItemSku, Qty : 10 })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		//console.log(res.body);
	  		if(res.body.MerchantList.length > 0){
	  			if(res.body.MerchantList[0].ItemList.length > 0){
	  				CartItemId = res.body.MerchantList[0].ItemList[0].CartItemId;
	  			}
	  		}
      })
      .expect(200, done);
  })

  it('update item on the cart add QTY', function(done){
    request
      .post('/api/cart/item/update')
	  .send({ CultureCode : "EN", CartKey : CartKey, SkuId : ItemSku, Qty : 20, CartItemId : CartItemId })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		//console.log(res.body);
      })
      .expect(200, done);
  })

  it('remove item on the cart', function(done){
    request
      .post('/api/cart/item/remove')
	  .send({ CultureCode : "EN", CartKey : CartKey, CartItemId : 1 })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		//console.log(res.body);
      })
      .expect(200, done);
  })

   it('View cart by user key', function(done){
	    request
	      .get('/api/cart/view/user?cc=EN&userkey=' + CartUser)
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(200, done);
	})

   it('View cart by cart id', function(done){
	    request
	      .get('/api/cart/view?cc=EN&cartkey=' + CartKey)
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(200, done);
	})


   it('add cart item to wish list', function(done){
    request
      .post('/api/cart/item/add/wishlist')
	  .send({ CultureCode : "EN", CartKey : CartKey, CartItemId : CartItemId })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		//console.log(res.body);
      })
      .expect(200, done);
  })



   it('Create new Cart for Merge', function(done){
    request
      .post('/api/cart/create')
	  .send({ CultureCode : "EN", UserKey : CartUser })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		if (res.body && res.body.CartKey){
				MergeId = res.body.CartKey;
	  		}else{
	  			throw new Error("Cart Creation failed");
	  		}
      })
      .expect(200, done);
  })


   it('add new item on the merge cart', function(done){
    request
      .post('/api/cart/item/add')
	  .send({ CultureCode : "EN", CartKey : MergeId, SkuId : ItemSku, Qty : 10 })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		//console.log(res.body);
      })
      .expect(200, done);
  })



   it('Merge 2 cart', function(done){
    request
      .post('/api/cart/merge')
	  .send({ CultureCode : "EN", CartKey : CartKey, MergeCartKey : MergeId })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		console.log(res.body);
      })
      .expect(200, done);
  })


 
});	
