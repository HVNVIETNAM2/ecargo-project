var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

var language = [];

describe('Reference Service', function(){


  it('get all language', function(done){
    request
      .get('/api/reference/changelanguage')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res){
         language = res.body.LanguageList; 
      })
      .expect(200, done);
  })

  it('get general translations with missing parameter', function(done){
    request
      .get('/api/reference/general')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })

  it('get general translations', function(done){
    request
      .get('/api/reference/general?cc=EN')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })


  it('get translations data with missing parameter', function(done){
    request
      .get('/api/reference/translation')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })


  it('get translations data', function(done){
    request
      .get('/api/reference/translation?cc=EN')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })

});