var testCommon = require('./test.common.js')();
var request = testCommon.supertest();
var CONSTANT = require('../logic/constants.js');
var assert = require('assert');
var moment = require('moment');
var util = require('util');
var data_driven = require('data-driven');
//you can add Overwrite to POST data to Overwrite the product to DB;

var product = {
  "MerchantId": 25,
  "StyleCode": "WW00187",
  "SkuCode": "100052489",
  "Barcode": "100052489",
  "BrandCode": "BROOKSBROTHERS",
  "BadgeCode": "EXCLUSIVE",
  "CountryOriginCode": "US",
  "LaunchYear": "2016",
  "SeasonCode": "SPRING",
  "ManufacturerName": "BROOKS BROTHERS GROUP, INC",
  "RetailPrice": 3500,
  "SalePrice": "",
  "AvailableFrom": "2016-01-01 00:00:00",
  "AvailableTo": "2016-12-31 23:59:59",
  "QtySafetyThreshold": 5,
  "PrimaryCategoryCode": "women-apparel-jackets-downjackets",
  "ShippingWeightKg": 3.5,
  "LengthCm": 10,
  "WidthCm": 10,
  "HeightCm": 10,
  "ColorCode": "GREY",
  "ColorName-English": "Grey",
  "ColorName-CHS": "灰色",
  "ColorName-CHT": "灰色",
  "SkuName-English": "Loose Fit Short Jacket",
  "SkuName-CHS": "女士短外套",
  "SkuName-CHT": "女士短外套",
  "Description-English": "Our jacket is crafted in leather with long sleeves and front pockets. One hook-and-eye closure at center front neckline \"",
  "Description-CHS": "长袖，双口袋，挡风立领，轻松专业人士清洁，进口；大方潇洒，尽显成功人士风范",
  "Description-CHT": "長袖，雙口袋，擋風立領，輕鬆專業人士清潔，進口；大方瀟灑，盡顯成功人士風範",
  "Feature-English": "Spring 2016 New Design",
  "Feature-CHS": "2016年春季最新款式",
  "Feature-CHT": "2016年春季最新款式",
  "Material-English": "80% cotton",
  "Material-CHS": "80% 棉",
  "Material-CHT": "80% 棉"
};

var productHans = {
  "MerchantId": 25,
  "StyleCode": "WW00187",
  "SkuCode": "100052489",
  "Barcode": "100052489",
  "BrandCode": "BROOKSBROTHERS",
  "BadgeCode": "EXCLUSIVE",
  "CountryOriginCode": "US",
  "LaunchYear": "2016",
  "SeasonCode": "SPRING",
  "ManufacturerName": "BROOKS BROTHERS GROUP, INC",
  "RetailPrice": 3500,
  "SalePrice": "",
  "AvailableFrom": "2016-01-01 00:00:00",
  "AvailableTo": "2016-12-31 23:59:59",
  "QtySafetyThreshold": 5,
  "PrimaryCategoryCode": "women-apparel-jackets-downjackets",
  "ShippingWeightKg": 3.5,
  "LengthCm": 10,
  "WidthCm": 10,
  "HeightCm": 10,
  "ColorCode": "GREY",
  "ColorName-English": "Grey",
  "ColorName-Hans": "灰色",
  "ColorName-Hant": "灰色",
  "SkuName-English": "Loose Fit Short Jacket",
  "SkuName-Hans": "女士短外套",
  "SkuName-Hant": "女士短外套",
  "Description-English": "Our jacket is crafted in leather with long sleeves and front pockets. One hook-and-eye closure at center front neckline \"",
  "Description-Hans": "长袖，双口袋，挡风立领，轻松专业人士清洁，进口；大方潇洒，尽显成功人士风范",
  "Description-Hant": "長袖，雙口袋，擋風立領，輕鬆專業人士清潔，進口；大方瀟灑，盡顯成功人士風範",
  "Feature-English": "Spring 2016 New Design",
  "Feature-Hans": "2016年春季最新款式",
  "Feature-Hant": "2016年春季最新款式",
  "Material-English": "80% cotton",
  "Material-Hans": "80% 棉",
  "Material-Hant": "80% 棉"
};

var minProduct = {
  'MerchantId': 25,
  'RetailPrice': 1234.5,
  'ShippingWeightKg': 1.1,
  'BrandCode': 'QABRAND',
  'PrimaryCategoryCode': 'women-apparel-jackets'
};

function assertSaveSkuSuccess(res) {
  var log = JSON.stringify({
    'Save SKU': {
      'Path': res.request.req.path,
      'Request': res.request._data,
      'Response': res.body
    }
  }, null, 2);

  assert.equal(res.body.success, true, 'res.body.success should be true\n' + log);
  assert.equal(res.body.message, 'inserted', 'res.body.message should be "inserted"\n' + log);
  assert.equal(res.body.errors, 0, 'res.body.errors should be 0\n' + log);
  assert.equal(res.body.errorList.length, 0, 'res.body.errorList should be empty\n' + log);
}

function saveSkuSuccess(done, product) {
  var query = {
    'cc': 'CHS',
    'merchantid': product['MerchantId'],
    'stylecode': product['StyleCode']
  }

  if (product['SkuName-CHT'] != undefined || product['SkuName-Hant'] != undefined)
    query['cc'] = 'CHT';
  else if (product['SkuName-English'] != undefined)
    query['cc'] = 'EN';

  request
    .post('/api/product/sku/save')
    .send(product)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(assertSaveSkuSuccess)
    .expect(200)
    .end(function(err, res) {
      if (err)
        return done(err);
      request
        .get('/api/product/style/view')
        .query(query)
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err2, res2) {
          if (err2)
            return done(err2);

          var log = JSON.stringify({
            'Save SKU': {
              'Path': res.request.req.path,
              'Request': res.request._data,
              'Response': res.body
            },
            'Get Product': {
              'Path': res2.request.req.path,
              'Response': res2.body
            }
          }, null, 2);

          try {
            // matched key names
            var keys = ['StyleCode', 'WeightKg', 'HeightCm', 'WidthCm', 'LengthCm', 'StatusId'];
            for (var i = 0; i < keys.length; i++) {
              if (product[keys[i]] != undefined)
                assert.equal(res2.body[keys[i]], product[keys[i]], util.format('%s should be the same\n%s', keys[i], log));
            }

            // unmatched key names
            var keys = [{
              'a': 'SkuNameCHS',
              'x': 'SkuName-CHS'
            }, {
              'a': 'SkuNameCHT',
              'x': 'SkuName-CHT'
            }, {
              'a': 'SkuNameEN',
              'x': 'SkuName-English'
            }, {
              'a': 'SkuNameCHS',
              'x': 'SkuName-Hans'
            }, {
              'a': 'SkuNameCHT',
              'x': 'SkuName-Hant'
            }, {
              'a': 'SkuDescCHS',
              'x': 'Description-CHS'
            }, {
              'a': 'SkuDescCHT',
              'x': 'Description-CHT'
            }, {
              'a': 'SkuDescEN',
              'x': 'Description-English'
            }, {
              'a': 'SkuDescCHS',
              'x': 'Description-Hans'
            }, {
              'a': 'SkuDescCHT',
              'x': 'Description-Hant'
            }, {
              'a': 'SkuSizeCommentCHS',
              'x': 'SizeComment-CHS'
            }, {
              'a': 'SkuSizeCommentCHT',
              'x': 'SizeComment-CHT'
            }, {
              'a': 'SkuSizeCommentEN',
              'x': 'SizeComment-English'
            }, {
              'a': 'SkuSizeCommentCHS',
              'x': 'SizeComment-Hans'
            }, {
              'a': 'SkuSizeCommentCHT',
              'x': 'SizeComment-Hant'
            }, {
              'a': 'SkuMaterialCHS',
              'x': 'Material-CHS'
            }, {
              'a': 'SkuMaterialCHT',
              'x': 'Material-CHT'
            }, {
              'a': 'SkuMaterialEN',
              'x': 'Material-English'
            }, {
              'a': 'SkuFeatureCHS',
              'x': 'Feature-CHS'
            }, {
              'a': 'SkuFeatureCHT',
              'x': 'Feature-CHT'
            }, {
              'a': 'SkuFeatureEN',
              'x': 'Feature-English'
            }, {
              'a': 'WeightKg',
              'x': 'ShippingWeightKg'
            }];
            for (var i = 0; i < keys.length; i++) {
              if (product[keys[i].x] != undefined)
                assert.equal(res2.body[keys[i].a], product[keys[i].x], util.format('%s/%s should be the same\n%s', keys[i].a, keys[i].x, log));
            }

            // SkuList keys
            var keys = ['SkuCode', 'ColorId'];
            for (var i = 0; i < keys.length; i++) {
              if (product[keys[i]] != undefined)
                assert.equal(res2.body['SkuList'][0][keys[i]], product[keys[i]], util.format('%s should be the same\n%s', keys[i], log));
            }

            if (product['ColorName-CHS'] != undefined)
              assert.equal(res2.body['SkuList'][0].SkuColor, product['ColorName-CHS'], 'SkuColor/ColorName-CHS should be the same\n' + log);
            if (product['ColorName-CHT'] != undefined)
              assert.equal(res2.body['SkuList'][0].SkuColor, product['ColorName-CHT'], 'SkuColor/ColorName-CHT should be the same\n' + log);
            if (product['ColorName-English'] != undefined)
              assert.equal(res2.body['SkuList'][0].SkuColor, product['ColorName-English'], 'SkuColor/ColorName-English should be the same\n' + log);
            if (product['ColorName-Hans'] != undefined)
              assert.equal(res2.body['SkuList'][0].SkuColor, product['ColorName-Hans'], 'SkuColor/ColorName-Hans should be the same\n' + log);
            if (product['ColorName-Hant'] != undefined)
              assert.equal(res2.body['SkuList'][0].SkuColor, product['ColorName-Hant'], 'SkuColor/ColorName-Hant should be the same\n' + log);

            if (product['PrimaryCategoryCode'] != undefined)
              assert.equal(res2.body['CategoryPriorityList'][0].CategoryCode, product['PrimaryCategoryCode'], 'CategoryCode/PrimaryCategoryCode should be the same\n' + log);
            if (product['MerchandisingCategoryCode1'] != undefined)
              assert.equal(res2.body['CategoryPriorityList'][1].CategoryCode, product['MerchandisingCategoryCode1'], 'CategoryCode/MerchandisingCategoryCode1 should be the same\n' + log);
            if (product['MerchandisingCategoryCode2'] != undefined)
              assert.equal(res2.body['CategoryPriorityList'][2].CategoryCode, product['MerchandisingCategoryCode2'], 'CategoryCode/MerchandisingCategoryCode2 should be the same\n' + log);
            if (product['MerchandisingCategoryCode3'] != undefined)
              assert.equal(res2.body['CategoryPriorityList'][3].CategoryCode, product['MerchandisingCategoryCode3'], 'CategoryCode/MerchandisingCategoryCode3 should be the same\n' + log);
            if (product['MerchandisingCategoryCode4'] != undefined)
              assert.equal(res2.body['CategoryPriorityList'][4].CategoryCode, product['MerchandisingCategoryCode4'], 'CategoryCode/MerchandisingCategoryCode4 should be the same\n' + log);
            if (product['MerchandisingCategoryCode5'] != undefined)
              assert.equal(res2.body['CategoryPriorityList'][5].CategoryCode, product['MerchandisingCategoryCode5'], 'CategoryCode/MerchandisingCategoryCode5 should be the same\n' + log);

            done();
          } catch (e) {
            done(e);
          }
        });
    });
}

function saveSkuSuccessWithKey(done, keyMap) {
  var ms = new Date().getTime().toString();

  var testProduct = JSON.parse(JSON.stringify(minProduct));

  for (var key in keyMap)
    testProduct[key] = keyMap[key];

  if (testProduct['StyleCode'] == undefined)
    testProduct['StyleCode'] = util.format('SC%s', ms.substring(ms.length - 10));
  if (testProduct['SkuCode'] == undefined)
    testProduct['SkuCode'] = util.format('SKU%s', ms.substring(ms.length - 9));
  if (testProduct['SkuName-CHS'] == undefined && testProduct['SkuName-Hans'] == undefined)
    testProduct['SkuName-CHS'] = util.format('女士绒面革夹克 %s', ms);

  saveSkuSuccess(done, testProduct);
}

function saveSkuFail(done, product, expectedErrorList) {
  request
    .post('/api/product/sku/save')
    .send(product)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(200)
    .end(function(err, res) {
      if (err)
        return done(err);

      var log = JSON.stringify({
        'Save SKU': {
          'Path': res.request.req.path,
          'Request': res.request._data,
          'Response': res.body
        }
      }, null, 2);

      try {
        assert.equal(res.body.success, false, 'res.body.success should be false\n' + log);
        assert.equal(res.body.message, 'Error', 'res.body.message should be "Error"\n' + log);
        assert.equal(res.body.errors, expectedErrorList.length, util.format('res.body.errors should be %d\n%s', expectedErrorList.length, log));
        assert.equal(res.body.errorList.length, expectedErrorList.length, util.format('res.body.errorList size should be %d\n%s', expectedErrorList.length, log));
        for (var i = 0; i < expectedErrorList.length; i++) {
          assert.equal(res.body.errorList[i].AppCode, expectedErrorList[i].AppCode, util.format('res.body.errorList[%d].AppCode should be "%s"\n%s', i, expectedErrorList[i].AppCode, log));
          assert.equal(res.body.errorList[i].Message, expectedErrorList[i].Message, util.format('res.body.errorList[%d].Message should be "%s"\n%s', i, expectedErrorList[i].Message, log));
        }
        done();
      } catch (e) {
        done(e);
      }
    });
}

function saveSkuFailWithKey(done, key, value, expectedErrorList) {
  var ms = new Date().getTime().toString();

  var testProduct = JSON.parse(JSON.stringify(minProduct));

  if (key != undefined && value != undefined) {
    if (typeof key == 'string') {
      testProduct[key] = value;
    } else if (typeof key == 'object') {
      for (var i = 0; i < key.length; i++) {
        testProduct[key[i]] = value[i]
      }
    }
  }

  if (testProduct['StyleCode'] == undefined)
    testProduct['StyleCode'] = util.format('SC%s', ms.substring(ms.length - 10));
  if (testProduct['SkuCode'] == undefined)
    testProduct['SkuCode'] = util.format('SKU%s', ms.substring(ms.length - 9));
  if (testProduct['SkuName-CHS'] == undefined && testProduct['SkuName-Hans'] == undefined)
    testProduct['SkuName-CHS'] = util.format('女士绒面革夹克 %s', ms);

  saveSkuFail(done, testProduct, expectedErrorList);
}

describe('Public Product - Save SKU', function() {
  it('with mandatory fields only', function(done) {
    saveSkuSuccessWithKey(done);
  })

  it('with Barcode', function(done) {
    var ms = new Date().getTime().toString();
    saveSkuSuccessWithKey(done, {
      'Barcode': ms.substring(ms.length - 12)
    });
  })

  var colorIds = [];
  for (var i = 0; i < 17; i++) {
    colorIds.push({
      v: i
    });
  }
  data_driven(colorIds, function() {
    it('with ColorId = {v}', function(ctx, done) {
      saveSkuSuccessWithKey(done, {
        'ColorId': ctx.v
      });
    });
  });

  var colorCodes = [{
    v: '0'
  }, {
    v: 'RED',
    x: '红色'
  }, {
    v: 'PINK',
    x: '粉红色'
  }, {
    v: 'ORANGE',
    x: '橙色'
  }, {
    v: 'YELLOW',
    x: '黄色'
  }, {
    v: 'GREEN',
    x: '绿色'
  }, {
    v: 'BLUE',
    x: '蓝色'
  }, {
    v: 'PURPLE',
    x: '紫色'
  }, {
    v: 'BROWN',
    x: '褐色'
  }, {
    v: 'GREY',
    x: '灰色'
  }, {
    v: 'WHITE',
    x: '白色'
  }, {
    v: 'BLACK',
    x: '黑色'
  }, {
    v: 'PRINT',
    x: '印花'
  }, {
    v: 'STRIP',
    x: '条纹'
  }, {
    v: 'FLORAL',
    x: '花卉'
  }, {
    v: 'CHECK',
    x: '格子'
  }, {
    v: 'DOT',
    x: '波点'
  }];
  data_driven(colorCodes, function() {
    it('with ColorCode = {v}, ColorName-CHS = {x}', function(ctx, done) {
      saveSkuSuccessWithKey(done, {
        'ColorCode': ctx.v,
        'ColorName-CHS': ctx.x
      });
    });
  });

  it('with PriceSale = 987.6, SalePriceFrom = now, SalePriceTo = now +1 year', function(done) {
    saveSkuSuccessWithKey(done, {
      'PriceSale': 987.6,
      'SalePriceFrom': moment().format('YYYY-MM-DD HH:mm:ss'),
      'SalePriceTo': moment().add(1, 'year').format('YYYY-MM-DD HH:mm:ss')
    });
  })

  it('with AvailableFrom = now, AvailableTo = now +1 year', function(done) {
    saveSkuSuccessWithKey(done, {
      'AvailableFrom': moment().format('YYYY-MM-DD HH:mm:ss'),
      'AvailableTo': moment().add(1, 'year').format('YYYY-MM-DD HH:mm:ss')
    });
  })

  var statusIds = [];
  for (var i = 1; i < 5; i++) {
    statusIds.push({
      v: i
    });
  }
  data_driven(statusIds, function() {
    it('with StatusId = {v}', function(ctx, done) {
      saveSkuSuccessWithKey(done, {
        'StatusId': ctx.v
      });
    });
  });

  var skuNames = [{
    v: 'SkuName-CHS',
    x: '女士绒面革夹克'
  }, {
    v: 'SkuName-CHT',
    x: '女士絨面革夾克(繁)'
  }, {
    v: 'SkuName-English',
    x: 'Suede Jacket'
  }, {
    v: 'SkuName-Hans',
    x: '女士绒面革夹克'
  }, {
    v: 'SkuName-Hant',
    x: '女士絨面革夾克(繁)'
  }];
  data_driven(skuNames, function() {
    it('with {v} = {x}', function(ctx, done) {
      var obj = {};
      obj[ctx.v] = util.format('%s %s', ctx.x, new Date().getTime().toString());
      saveSkuSuccessWithKey(done, obj);
    });
  });

  var descriptions = [{
    v: 'Description-CHS',
    x: '这种多用途的风格特点中等重量柔滑山羊麂皮设计有来自衬衫和一件夹克属性。它塑造了翻领和抛光的金钉卡扣。\n\n换股领;超大扣盖口袋在胸前;内衬袖子;轻微的高低缺口衬衣下摆;在背双肩枷锁; 27“;专业清洁;进口。'
  }, {
    v: 'Description-CHT',
    x: '這種多用途的風格特點中等重量柔滑山羊麂皮設計有來自襯衫和一件夾克屬性。它塑造了翻領和拋光的金釘卡扣。\n\n換股領;超大扣蓋口袋在胸前;內襯袖子;輕微的高低缺口襯衣下擺;在背雙肩枷鎖; 27“;專業清潔;進口。'
  }, {
    v: 'Description-English',
    x: 'This versatile style features mid-weight silky goat suede and is designed with attributes from both a shirt and a jacket. It is fashioned with a convertible collar and polished gold stud snaps.\n\nConvertible collar; oversized snap-flap pockets at chest; lined sleeves; slight hi-low notched shirttail hem; shoulder yoke at back; 27"; professionally clean; imported.'
  }, {
    v: 'Description-Hans',
    x: '这种多用途的风格特点中等重量柔滑山羊麂皮设计有来自衬衫和一件夹克属性。它塑造了翻领和抛光的金钉卡扣。\n\n换股领;超大扣盖口袋在胸前;内衬袖子;轻微的高低缺口衬衣下摆;在背双肩枷锁; 27“;专业清洁;进口。'
  }, {
    v: 'Description-Hant',
    x: '這種多用途的風格特點中等重量柔滑山羊麂皮設計有來自襯衫和一件夾克屬性。它塑造了翻領和拋光的金釘卡扣。\n\n換股領;超大扣蓋口袋在胸前;內襯袖子;輕微的高低缺口襯衣下擺;在背雙肩枷鎖; 27“;專業清潔;進口。'
  }];
  data_driven(descriptions, function() {
    it('with {v} = {x}', function(ctx, done) {
      var obj = {};
      obj[ctx.v] = ctx.x
      saveSkuSuccessWithKey(done, obj);
    });
  });

  var sizeComments = [{
    v: 'SizeComment-CHS',
    x: '备注 1'
  }, {
    v: 'SizeComment-CHT',
    x: '備註 1'
  }, {
    v: 'SizeComment-English',
    x: 'Comment 1'
  }, {
    v: 'SizeComment-Hans',
    x: '备注 1'
  }, {
    v: 'SizeComment-Hant',
    x: '備註 1'
  }];
  data_driven(sizeComments, function() {
    it('with {v} = {x}', function(ctx, done) {
      var obj = {};
      obj[ctx.v] = ctx.x
      saveSkuSuccessWithKey(done, obj);
    });
  });

  var materials = [{
    v: 'Material-CHS',
    x: '100％涤纶'
  }, {
    v: 'Material-CHT',
    x: '100％滌綸'
  }, {
    v: 'Material-English',
    x: '100% polyester'
  }];
  data_driven(materials, function() {
    it('with {v} = {x}', function(ctx, done) {
      var obj = {};
      obj[ctx.v] = ctx.x
      saveSkuSuccessWithKey(done, obj);
    });
  });

  var features = [{
    v: 'Feature-CHS',
    x: '船领\n帽套\n开切出回来蝴蝶结细节'
  }, {
    v: 'Feature-CHT',
    x: '船領\n帽套\n開切出回來蝴蝶結細節'
  }, {
    v: 'Feature-English',
    x: 'boat neck\ncap sleeve\nopen cut out back with bow detail'
  }];
  data_driven(features, function() {
    it('with {v} = {x}', function(ctx, done) {
      var obj = {};
      obj[ctx.v] = ctx.x
      saveSkuSuccessWithKey(done, obj);
    });
  });

  it('with MerchandisingCategoryCode', function(done) {
    saveSkuSuccessWithKey(done, {
      'MerchandisingCategoryCode1': 'women-apparel-jackets-downjackets',
      'MerchandisingCategoryCode2': 'women-apparel-jackets-coats',
      'MerchandisingCategoryCode3': 'women-apparel-jackets-windbreakers',
      'MerchandisingCategoryCode4': 'women-apparel-jackets-vests',
      'MerchandisingCategoryCode5': 'women-apparel-jackets-sweaters'
    });
  });

  var badgeCodes = [{
    v: 'NEW'
  }, {
    v: 'HOT'
  }, {
    v: 'ONSALE'
  }, {
    v: 'FREESHIPPING'
  }, {
    v: 'CELEBERTY'
  }, {
    v: 'TV'
  }, {
    v: 'EXCLUSIVE'
  }];
  data_driven(badgeCodes, function() {
    it('with BadgeCode = {v}', function(ctx, done) {
      saveSkuSuccessWithKey(done, {
        'BadgeCode': ctx.v
      });
    });
  });

  it('with wrong BadgeCode', function(done) {
    saveSkuFailWithKey(done, 'BadgeCode', 'WRONG', [{
      'AppCode': 'MSG_ERR_FIELD_NOT_VALID',
      'Message': 'BadgeCode'
    }]);
  });

  var manufacturerNames = [{
    v: '布克兄弟'
  }, {
    v: 'BROOKS BROTHERS GROUP, INC'
  }];
  data_driven(manufacturerNames, function() {
    it('with ManufacturerName = {v}', function(ctx, done) {
      saveSkuSuccessWithKey(done, {
        'ManufacturerName': ctx.v
      });
    });
  });
});

describe('Public Product - Bugs', function() {
  it('MM-4366', function(done) {
    saveSkuSuccessWithKey(done, {
      'WeightKg': minProduct['ShippingWeightKg'],
      'HeightCm': 2.2,
      'WidthCm': 3.3,
      'LengthCm': 4.4
    });
  })

  it('MM-4370', function(done) {
    var ms = new Date().getTime().toString();
    saveSkuFailWithKey(done, 'SkuCode', 'SKU' + ms.substring(ms.length - 9) + new Array(245).join('M'), [{
      'AppCode': 'MSG_ERR_FIELD_NOT_VALID',
      'Message': 'SkuCode'
    }]); // 256 characters
  })

  it('MM-4378', function(done) {
    saveSkuFailWithKey(done, 'ColorId', 99, [{
      'AppCode': 'MSG_ERR_FIELD_NOT_VALID',
      'Message': 'ColorId'
    }]);
  })

  it('MM-4382', function(done) {
    saveSkuFailWithKey(done, 'ColorCode', 'DOT', [{
      'AppCode': 'MSG_ERR_REQUIRED_FIELD_MISSING',
      'Message': 'ColorName-CHS'
    }]);
  })

  it('MM-4400', function(done) {
    saveSkuFailWithKey(done, ['PriceSale', 'SalePriceFrom', 'SalePriceTo'], [987.6, 'I AM NOT A DATE', 'I AM NOT A DATE'], [{
      'AppCode': 'MSG_ERR_FIELD_NOT_VALID',
      'Message': 'SalePriceFrom'
    }, {
      'AppCode': 'MSG_ERR_FIELD_NOT_VALID',
      'Message': 'SalePriceTo'
    }]);
  })

  it('MM-4521', function(done) {
    saveSkuFailWithKey(done, ['MerchandisingCategoryCode5'], ['women-apparel-tops'], [{
      'AppCode': 'MSG_ERR_FIELD_NOT_VALID',
      'Message': 'MerchandisingCategoryCode5'
    }]);
  });
});

describe('Public Product - Others', function() {
  it('SKU List', function(done) {
    request
      .get('/api/product/sku/list?cc=EN&page=1&size=25')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        if (!(res && res.body && res.body.PageData && res.body.PageData.length > 1)) {
          throw new Error("FAILED TO GET SKU LIST");
        }
      })
      .expect(200, done);
  })

  it('Style List', function(done) {
    request
      .get('/api/product/style/list?cc=EN&page=1&size=25')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        if (!(res && res.body && res.body.PageData && res.body.PageData.length > 1)) {
          throw new Error("FAILED TO GET STYLE LIST");
        }
      })
      .expect(200, done);
  })

  it('Should change sku Status', function(done) {
    var payload = {};
    payload.SkuId = 518;
    payload.Status = 'Active';

    request
      .post('/api/product/sku/status/change')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function(res) {
        if (res && res.body && (res.body === true || res.body == 'true')) {

        } else {
          throw new Error("Sku change status failed");
        }
      })
      .expect(200, done);
  })

  it('Should change style Status', function(done) {
    var payload = {};
    payload.StyleCode = "RY00024";
    payload.Status = 'Active';
    payload.MerchantId = 25;

    request
      .post('/api/product/style/status/change')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function(res) {
        if (res && res.body && (res.body === true || res.body == 'true')) {

        } else {
          throw new Error("Sku change status failed");
        }
      })
      .expect(200, done);
  })
});