var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

var CONSTANT = require('../logic/constants.js');

var UserKey = null;
var ToUserKey = null;
var MerchantId = null;
var BrandId = 2;

describe('Follow', function(){

  it('should get sample UserKey from User', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "merch@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      ToUserKey = res.body.UserKey;
      MerchantId = res.body.MerchantId;
      UserKey = testCommon.getUserKey();
      done();
    });
  })

  // Follow API
  it('should follow a user', function(done){
    request
    .post('/api/follow/user/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToUserKey : ToUserKey })
    .expect(200, done)
  })

  it('should follow a merchant', function(done){
    request
    .post('/api/follow/merchant/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToMerchantId : MerchantId })
    .expect(200, done)
  })

  it('should follow a brand', function(done){
    request
    .post('/api/follow/brand/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToBrandId : BrandId })
    .expect(200, done)
  })

  // List API
  it('should list all users following a certain user', function(done){
    request
    .get('/api/follow/user/following?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all users following a certain user failed");
      }
    })
    .expect(200, done);
  })

  it('should list all users followed by a certain user', function(done){
    request
    .get('/api/follow/user/followed?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all users followed by a certain user failed");
      }
    })
    .expect(200, done);
  })
 
  it('should list all users following a certain merchant', function(done){
    request
    .get('/api/follow/merchant/following?MerchantId=' + MerchantId)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all users following a certain merchant failed");
      }
    })
    .expect(200, done);
  })

  it('should list all merchants followed by a certain user', function(done){
    request
    .get('/api/follow/merchant/followed?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all merchants followed by a certain user failed");
      }
    })
    .expect(200, done);
  })

  it('should list all users following a certain brand', function(done){
    request
    .get('/api/follow/brand/following?BrandId=' + BrandId)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all users following a certain brand failed");
      }
    })
    .expect(200, done);
  })

  it('should list all brands followed by a certain user', function(done){
    request
    .get('/api/follow/brand/followed?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all brands followed by a certain user failed");
      }
    })
    .expect(200, done);
  })

  // Delete API
  it('should delete a user follow', function(done){
    request
    .post('/api/follow/user/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToUserKey : ToUserKey })
    .expect(200, done)
  })

  it('should delete a merchant follow', function(done){
    request
    .post('/api/follow/merchant/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToMerchantId : MerchantId })
    .expect(200, done)
  })

  it('should delete a brand follow', function(done){
    request
    .post('/api/follow/brand/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToBrandId : BrandId })
    .expect(200, done)
  })
}); 