var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

var CONSTANT = require('../logic/constants.js');

var UserKey = null;
var ToUserKey = null;

describe('Friend', function(){

  before('should get sample UserKey from User', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "merch@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      ToUserKey = res.body.UserKey;
      UserKey = testCommon.getUserKey();
      done();
    });
  })

  // Friend Request API
  it('should send a friend request', function(done){
    request
    .post('/api/friend/request')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToUserKey : ToUserKey })
    .expect(200, done)
  })

  // Accept Friend Request API
  it('should accept a friend request', function(done){
    request
    .post('/api/friend/request/accept')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : ToUserKey, ToUserKey : UserKey })
    .expect(200, done)
  })

  // List API
  it('should list all friends by a certain user', function(done){
    request
    .get('/api/friend/list?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all friends by a certain user failed");
      }
    })
    .expect(200, done);
  })

  it('should list all friend request by a certain user', function(done){
    request
    .get('/api/friend/request/list?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all friend request by a certain user failed");
      }
    })
    .expect(200, done);
  })

  it('should list all friend request recieve by a certain user', function(done){
    request
    .get('/api/friend/request/receive?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list all friend request recieve by a certain user failed");
      }
    })
    .expect(200, done);
  })

  // Delete Friend Request API
  it('should delete a friend request', function(done){
    request
    .post('/api/friend/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToUserKey : ToUserKey })
    .expect(200, done)
  })

}); 
