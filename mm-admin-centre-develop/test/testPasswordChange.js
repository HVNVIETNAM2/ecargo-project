var request = require('supertest');
var app = require('../services/main.js');
var CONSTANT = require('../logic/constants.js');
var lu=require('../logic/logicuser.js');
var cm = require('../lib/commonmariasql.js');
var config=require('../config');
var assert = require('assert');
var testCommon = require('./test.common.js')();
request=request(app);

var Userkey = null;
var Token = null;
var NewPassword = "Bart123"


describe('Change Password', function(){

    it('should get sample UserKey from User', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "test@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      Userkey = res.body.UserKey;
      Token = res.body.Token;
      done();
    });
  })

  it('should change password with invalid CurrentPassword and returns error', function(done){
       request
      .post('/api/user/passwordchange')
      .set('Accept', 'application/json')
      .set('authorization', Token)
      .send({
         UserKey : Userkey,
         CurrentPassword : "Bart123",
         Password : "Bart"
      })
      .expect(500, function(err,res){
        done();
      })
  })

  it('it should change password', function(done){
    request
    .post('/api/user/passwordchange')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', Token)
    .send({ UserKey : Userkey, CurrentPassword: "Bart" , Password: NewPassword })
    .expect(200, function(err, res){
        done();
    })
  })

  it('it should relogin', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "test@ecargo.com", Password : NewPassword })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      Userkey = res.body.UserKey;
      Token = res.body.Token;
      done();
    });
  })

  it('it should change back the original password', function(done){
    request
    .post('/api/user/passwordchange')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', Token)
    .send({ UserKey : Userkey, CurrentPassword: NewPassword , Password: "Bart" })
    .expect(200, done)
  })

}); 
