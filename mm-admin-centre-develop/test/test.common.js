var supertest = require('supertest');
var config = require('../config.json')
var app = require('../services/main.js');
var assert = require('assert');

function TestCommon() {
    if (!(this instanceof TestCommon)) {
        return new TestCommon();
    }

    if (process.env.URL) {
        this.st = supertest(process.env.URL);
    } else {
        this.st = supertest(app);
    }

    this.token = null;
    this.userkey = null;

    // get the login token
    var tc = this;

    before(function(done) {
        tc.st
            .post('/api/auth/login')
            .send({
                Username: "test@ecargo.com",
                Password: "Bart",
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(function(res) {
                tc.token = res.body.Token;
                tc.userkey = res.body.UserKey;

                assert.notEqual(res.body.Token, undefined, 'res.body.Token is undefined');
                assert.notEqual(res.body.UserId, undefined, 'res.body.UserId is undefined');
                assert.notEqual(res.body.UserKey, undefined, 'res.body.UserKey is undefined');
                assert.notEqual(res.body.MerchantId, undefined, 'res.body.MerchantId is undefined');
            })
            .expect(200, done);
    });
}

TestCommon.prototype.supertest = function supertest() {
    return this.st;
};

TestCommon.prototype.getToken = function getToken() {
    return this.token;
};

TestCommon.prototype.getUserKey = function getToken() {
    return this.userkey;
};

module.exports = TestCommon;