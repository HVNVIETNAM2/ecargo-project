var testCommon = require('./test.common.js')();
var request = testCommon.supertest();


describe('Size Service', function(){
  it('should list all sizes with missing parameter', function(done){
    request
      .get('/api/size/list')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })


  it('should list all sizes', function(done){
    request
      .get('/api/size/list?cc=EN')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })
});