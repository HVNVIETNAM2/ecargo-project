var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

var assert = require('assert');
var CONSTANT = require('../logic/constants.js');

var StyleImage, ImageList;

describe('Public Product Image Service', function(){
  it('Upload invalid image returns error', function(done){
    request
      .post('/api/productimage/upload')
      .attach('file', __dirname + '/data/productimage/167199.png')
		  .set('authorization', testCommon.getToken())
      .field('data', JSON.stringify({ MerchantId: 1 }))
      .expect(function (res) {
        assert.equal(res.body.AppCode, 'MSG_ERR_IMAGE_UPLOAD');
        assert.equal(res.body.Message, 'Invalid file name format.');
      })
      .expect(500, done);
  })

  it('Upload non-exists Style Code returns error', function (done) {
    request
      .post('/api/productimage/upload')
      .attach('file', __dirname + '/data/productimage/M167199_1.jpg')
		  .set('authorization', testCommon.getToken())
      .field('data', JSON.stringify({ MerchantId: 1 }))
      .expect(function (res) {
        assert.equal(res.body.AppCode, 'MSG_ERR_IMAGE_UPLOAD');
        assert.equal(res.body.Message, 'StyleCode or ColorKey not exists.');
      })
      .expect(500, done);
  });



   it('should return image list', function(done){
    request
        .get('/api/productimage/list?merchantid=1&stylecode=MD00068')
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(function (res) {
          if(res && res.body.FeaturedImageList && res.body.FeaturedImageList.length > 0){
            ImageList = res.body.FeaturedImageList[0];
          }else{
            throw new Error("product image listing failed for stylecode MD00068");
          }
        })
        .expect(200, done);
  })


  //delete existing styleimageid
  it('should delete product image', function (done) {
    request
      .post('/api/productimage/delete')
      .send({ MerchantId : 1, StyleImageId : ImageList.StyleImageId })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function(res) {
        console.log(res.body);
        if(res && res.body && (res.body === true || res.body == 'true')){

        }else{
          throw new Error("product image delete failed");
        }
      })
      .expect(200, done);
  })

  it('Upload success with correct StyleCode and ColorCode', function (done) {
    request
      .post('/api/productimage/upload')
      .attach('file', __dirname + '/data/productimage/MD00068_1.jpg')
		  .set('authorization', testCommon.getToken())
      .field('data', JSON.stringify({ MerchantId: 1 }))
      .expect(function (res) {
        StyleImage = res.body.StyleImage;
        assert.equal(StyleImage.StyleCode, 'MD00068');
        assert.equal(StyleImage.Position, 1);
        assert.equal(StyleImage.ImageTypeCode, 'Feature');
      })
      .expect(200, done);
  });

  it('View successfully uploaded product image', function (done) {
    request
      .get('/api/resizer/view?b=productimages&s=200&key='+ StyleImage.ProductImage)
      .expect(200, done);
  })


  it('should show missing image report with error returns', function(done){
    request
        .get('/api/productimage/report/missing-img')
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(500, done);
  })

  it('should show missing image report successfully', function(done){
    request
        .get('/api/productimage/report/missing-img?cc=EN&filefilter=1&filefilter=3&filefilter=2&merchantid=1&status=-1')
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(200, done);
  })



});
