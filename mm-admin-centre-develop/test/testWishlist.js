var testCommon = require('./test.common.js');
var app = require('../services/main.js');

testCommon = testCommon(app);
var request = testCommon.supertest();

var CartKey = null;
var CartItemId = null;
var CartUser = "1fca2b93-c3e5-11e5-9195-06e517c0a113";

var ItemStyleCode = "RY00024";
var ItemMerchantId = 1;
var continueTest = true;

describe('Wishlist', function(){

	it('Get 1 StyleCode from DB', function(done){
	    request
	      .get('/api/product/style/list?cc=EN&page=1&size=1')
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(function(res){
	      		if(res.body && res.body.PageData){
	      			if(res.body.PageData.length > 0){
	      				ItemStyleCode = res.body.PageData[ res.body.PageData.length - 1 ].StyleCode;
	      				ItemMerchantId = res.body.PageData[ res.body.PageData.length - 1 ].MerchantId;
	      			}else{
	      				continueTest = false;
	      				throw new Error("FAILED TO GET Style Code");
	      			}
	      		}else{
	      			continueTest = false;
	      			throw new Error("FAILED TO GET Style Code");
	      		}
	      })
	      .expect(200, done);
	})

	//break the code
	if(!continueTest){
		return true;
	}

   it('should create a new wish list for user id 0', function(done){
    request
      .post('/api/wishlist/create')
	  .send({ CultureCode : "EN", UserKey : 0 })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		if (res.body && res.body.CartKey){
				CartKey = res.body.CartKey;
	  		}else{
	  			throw new Error("Wishlist Creation failed");
	  		}
      })
      .expect(200, done);
  })

  it('should update the wish list into the authenticated user', function(done){
    request
      .post('/api/wishlist/user/update')
	  .send({ CultureCode : "EN", UserKey : CartUser, CartKey : CartKey })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		// if(!res.body.UserId || !(res.body.UserId == CartUser))
	  		// 	throw new Error("Wishlist User Update failed");
      })
      .expect(200, done);
  })

  it('add new item on the wishlist', function(done){
    request
      .post('/api/wishlist/item/add')
	  .send({ CultureCode : "EN", CartKey : CartKey, StyleCode : ItemStyleCode, MerchantId : ItemMerchantId })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		//console.log(res.body);
      })
      .expect(200, done);
  })


  it('remove item on the wishlist', function(done){
    request
      .post('/api/wishlist/item/remove')
	  .send({ CultureCode : "EN", CartKey : CartKey, CartItemId : 1 })
      .set('Accept', 'application/json')	  	  
      .expect('Content-Type', /json/)
	  .expect(function(res) {
	  		//console.log(res.body);
      })
      .expect(200, done);
  })

   it('View wishlist by user key', function(done){
	    request
	      .get('/api/wishlist/view/user?cc=EN&userkey=' + CartUser)
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(200, done);
	})

   it('View wishlist by cart id', function(done){
	    request
	      .get('/api/wishlist/view?cc=EN&cartkey=' + CartKey)
	      .set('Accept', 'application/json')
	      .expect('Content-Type', /json/)
	      .expect(function(res){
	      	console.log(res.body);
	      })
	      .expect(200, done);
	})

 
});	
