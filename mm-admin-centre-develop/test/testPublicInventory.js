var testCommon = require('./test.common.js')();
var request = testCommon.supertest();
var CONSTANT = require('../logic/constants.js');


var inventorylocationid = 2; //inventory location id derived from MerchantId
var skuid = 519;
var merchantid = 1;

var InvLoc = null;

describe('Public Inventory', function(){

  it('InventoryLocation List', function(done){
      request
        .get('/api/inventory/location/list?cc=EN&merchantid='+ merchantid)
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(function(res){
            if(res.body && res.body.length > 0){

            }else{
              throw new Error("InventoryLocation list failed");
            }
        })
        .expect(200, done);
  })

  it('View Single InventoryLocation', function(done){
      request
        .get('/api/inventory/location/view?cc=EN&id='+ inventorylocationid + "&merchantid=1")
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(function(res){
            if(res.body && res.body.InventoryLocationId && res.body.InventoryLocationId == inventorylocationid){
              InvLoc = res.body;
            }else{
              throw new Error("view single InventoryLocation failed");
            }
        })
        .expect(200, done);
  })

  it('Should change location Status', function(done){         
    var payload={};
    payload.InventoryLocationId=InvLoc.InventoryLocationId;
    payload.MerchantId=1;
    payload.Status='Active';
      
    request
      .post('/api/inventory/location/status/change')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function(res){
          if(res && res.body && (res.body === true || res.body == 'true')){

          }else{
            throw new Error("Inventory change location status failed");
          }
      })
      .expect(200, done);
  })


  it('View Single InventoryLocation without merchantid', function(done){
      request
        .get('/api/inventory/location/view?cc=EN&id='+ inventorylocationid)
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(function(res){

        })
        .expect(500, done);
  })


   it('View Single Inventory object', function(done){
      request
        .get('/api/inventory/view?cc=EN&inventorylocationid='+ inventorylocationid +'&skuid='+ skuid + "&merchantid=" + merchantid)
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(function(res){
            if(res.body && res.body.Inventory && res.body.InventoryLocation){

            }else{
              throw new Error("View single inventory object failed");
            }
        })
        .expect(200, done);
  })


   it('Inventory List by Sku', function(done){
      request
        .get('/api/inventory/list/sku?cc=EN&merchantid='+ merchantid +'&skuid='+ skuid)
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(function(res){
          if(res.body && res.body.InventoryList && res.body.InventoryList.length > 0){

          }else{
            throw new Error("Inventory list by sku failed");
          }
        })
        .expect(200, done);
   })


   it('Inventory List By Location', function(done){
      request
        .get('/api/inventory/list/location?cc=EN&page=1&size=25&merchantid='+ merchantid + "&inventorylocationid=" + inventorylocationid)
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .expect('Content-Type', /json/)
        .expect(function(res){
        	if(!res.body.InventoryLocation || !res.body.SkuList){
        			throw new Error("inventory list by location");
        	}
        })
        .expect(200, done);
  })


    it('should try insert but failed due to existing', function(done){          
    request
      .post('/api/inventory/save')
      .send({InventoryId:0,InventoryLocationId:2,SkuId:519,IsPerpetual:0,QtyAllocated:55,MerchantId:1})
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function(res){
	    	
	   })
	   .expect(500, done);
    })

    it('Should Save (Update) Inventory', function(done){          
    request
      .post('/api/inventory/save')
      .send({InventoryId:53,InventoryLocationId:2,SkuId:519,IsPerpetual:0,QtyAllocated:55,MerchantId:1})
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function(res){
	    	if(res.body && res.body.InventoryId){

        }else{
            throw new Error("Update inventory failed");
        }
	   })
	   .expect(200, done);
    })

 
}); 
