var request = require('./test.common.js');
var assert = require('assert');

var app = require('../services/main.js');
var CONSTANT = require('../logic/constants.js');
request = request(app);

var LoginToken = null;

describe('Product Service', function () {
    it('require auth for creating product', function (done) {
        request.post('/api/product/create').expect(401, done);
    })

    it('Login as admin', function (done) {
        request
            .post('/api/auth/login')
            .send({ Username : "test@ecargo.com", Password : "Bart",  })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(function (res) {
                LoginToken = res.body.Token;
            })
            .expect(200, done);
    });

    var merchantId = 1;
    var styleCode = "test" + Date.now();
    it('creates new product', function (done) {
        var product = {
            MerchantId: merchantId,
            style: {
                AvailableFrom: "2016-02-25T16:00:00.000Z",
                AvailableTo: "2016-02-26T16:00:00.000Z",
                BadgeId: 3,
                BrandId: 5,
                CategoryPriorityList: [{
                    CategoryType: "基本类别",
                    CategoryTypeTitle: "选择基本类别",
                    CategoryName: "裤子",
                    CategoryId: 4,
                    Priority: 0
                }, {
                    CategoryType: "专题类别 {0}",
                    CategoryTypeTitle: "选择专题类别",
                    CategoryName: "裙子",
                    CategoryId: 3,
                    Priority: 1
                }],
                ColorImageListMap: {Red: [{ImageKey: "9228abecaa174fc8edeb9208d9ef0930", Position: 1}]},
                ColorList: [{ColorKey: "Red", SkuColor: "红色"}, {ColorKey: "Pink", SkuColor: "粉色"}],
                DescriptionImageList: [{ImageKey: "98a2f5186115d72871b6ed4cf615c5b4", Position: 1}],
                FeaturedImageList: [{ImageKey: "43f768658c3e1f933004ed348a8288f8", Position: 1}],
                GeoCountryId: 47,
                HeightCm: 1,
                LaunchYear: 2016,
                LengthCm: 10,
                ManufacturerName: "testdemo0225",
                PriceRetail: "19998",
                PriceSale: "19998",
                SaleFrom: "2016-02-16T16:00:00.000Z",
                SaleTo: "2016-02-25T16:00:00.000Z",
                SeasonId: 1,
                SkuDescCHS: "testdemo0225 description",
                SkuDescCHT: "testdemo0225 description",
                SkuDescEN: "testdemo0225 description",
                SkuFeatureCHS: "testdemo0225 description",
                SkuFeatureCHT: "testdemo0225 description",
                SkuFeatureEN: "testdemo0225 description",
                SkuList: [{
                    Bar: Date.now()+'1',
                    ColorCode: "RED",
                    ColorId: 1,
                    ColorKey: "Red",
                    IsNewAdd: 1,
                    QtySafetyThreshold: 0,
                    SizeId: 5,
                    SizeName: "M",
                    SkuCode: Date.now()+'1',
                    SkuColor: "红色",
                    SkuColorCHS: "红色",
                    SkuColorCHT: "红色",
                    SkuColorEN: "Red"
                }, {
                    Bar: Date.now()+'2',
                    ColorCode: "RED",
                    ColorId: 1,
                    ColorKey: "Red",
                    IsNewAdd: 1,
                    QtySafetyThreshold: 1,
                    SizeId: 4,
                    SizeName: "S",
                    SkuCode: Date.now()+'2',
                    SkuColor: "红色",
                    SkuColorCHS: "红色",
                    SkuColorCHT: "红色",
                    SkuColorEN: "Red"
                }, {
                    Bar: Date.now()+'3',
                    ColorCode: "RED",
                    ColorId: 1,
                    ColorKey: "Red",
                    IsNewAdd: 1,
                    QtySafetyThreshold: 2,
                    SizeId: 6,
                    SizeName: "L",
                    SkuCode: Date.now()+'3',
                    SkuColor: "红色",
                    SkuColorCHS: "红色",
                    SkuColorCHT: "红色",
                    SkuColorEN: "Red"
                }, {
                    Bar: Date.now()+'4',
                    ColorCode: "PINK",
                    ColorId: 2,
                    ColorKey: "Pink",
                    IsNewAdd: 1,
                    QtySafetyThreshold: 3,
                    SizeId: 5,
                    SizeName: "M",
                    SkuCode: Date.now()+'4',
                    SkuColor: "粉色",
                    SkuColorCHS: "粉色",
                    SkuColorCHT: "粉色",
                    SkuColorEN: "Pink"
                }, {
                    Bar: Date.now()+'5',
                    ColorCode: "PINK",
                    ColorId: 2,
                    ColorKey: "Pink",
                    IsNewAdd: 1,
                    QtySafetyThreshold: 4,
                    SizeId: 4,
                    SizeName: "S",
                    SkuCode: Date.now()+'5',
                    SkuColor: "粉色",
                    SkuColorCHS: "粉色",
                    SkuColorCHT: "粉色",
                    SkuColorEN: "Pink"
                }, {
                    Bar: Date.now()+'6',
                    ColorCode: "PINK",
                    ColorId: 2,
                    ColorKey: "Pink",
                    IsNewAdd: 1,
                    QtySafetyThreshold: 5,
                    SizeId: 6,
                    SizeName: "L",
                    SkuCode: Date.now()+'6',
                    SkuColor: "粉色",
                    SkuColorCHS: "粉色",
                    SkuColorCHT: "粉色",
                    SkuColorEN: "Pink"
                }],
                SkuMaterialCHS: "testdemo0225 description",
                SkuMaterialCHT: "testdemo0225 description",
                SkuMaterialEN: "testdemo0225 description",
                SkuNameCHS: "testdemo0225",
                SkuNameCHT: "testdemo0225",
                SkuNameEN: "testdemo0225",
                SkuSizeCommentCHS: "testdemo0225 description",
                SkuSizeCommentCHT: "testdemo0225 description",
                SkuSizeCommentEN: "testdemo0225 description",
                StyleCode: styleCode,
                WeightKg: 1,
                WidthCm: 20
            }
        };
        request
            .post('/api/product/create')
            .set('authorization', LoginToken)
            .send(product)
            .expect(200, done);
    });

    it('view created product', function (done) {
        request
            .get('/api/product/style/view')
            .query({
                cc:'CHS',
                merchantid:merchantId,
                stylecode:styleCode
            })
            .expect(200, done);
    });

});
