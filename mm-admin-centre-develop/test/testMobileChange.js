var request = require('supertest');
var app = require('../services/main.js');
var CONSTANT = require('../logic/constants.js');
var lu=require('../logic/logicuser.js');
var cm = require('../lib/commonmariasql.js');
var config=require('../config');
var assert = require('assert');
var cm = require('../lib/commonmariasql.js');
var testCommon = require('./test.common.js')();
request=request(app);

var Userkey = null;
var Token = null;
var MobileCode = "+952";
var MobileNumber = "79958245";
var MobileVerificationId = null;
var MobileVerificationToken = null;


describe('Mobile Change', function(){

    it('should get sample UserKey from User', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "test@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      Userkey = res.body.UserKey;
      Token = res.body.Token;
      done();
    });
  })


  it('should change mobilecode and mobilenumber with invalid case', function(done){
       request
      .post('/api/user/mobilechange')
      .set('Accept', 'application/json')
      .set('authorization', Token)
      .send({
         "MobileVerificationId": "111", "MobileCode": "+852", "MobileNumber": "45345685678", "MobileVerificationToken": "294809", "UserKey": Userkey
      })
      .expect(500, function(err,res){
        done();
      })
  })

   it('should save mobileverification and change mobilecode and mobilenumber', function(done){
      var tr= new cm();
      tr.begin()
      .then(function () {
        return lu.saveMobileVerification({
          MobileCode: MobileCode,
          MobileNumber: MobileNumber
        }, tr);
      })
      .then(function (data) {
        MobileVerificationId = data.MobileVerificationId;
        MobileVerificationToken = data.MobileVerificationToken;
        return tr.commit();
      })
      .then(function () {
         request
          .post('/api/user/mobilechange')
          .set('Accept', 'application/json')
          .set('authorization', Token)
          .send({
             "MobileVerificationId": MobileVerificationId,
             "MobileCode": MobileCode,
             "MobileNumber": MobileNumber,
             "MobileVerificationToken": MobileVerificationToken,
             "UserKey": Userkey
          })
          .expect(200, function(err,res){
            done();
          })
      });
  });

  it('should relogin the new mobilenumber and mobilecode', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : MobileCode+MobileNumber, Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      Userkey = res.body.UserKey;
      Token = res.body.Token;
      done();
    });
  })
 
}); 
