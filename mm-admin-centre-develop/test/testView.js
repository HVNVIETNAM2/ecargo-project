var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

var CONSTANT = require('../logic/constants.js');

var UserKey = null;
var MerchantId = 1;
var CultureCode = "EN";

describe('View', function(){

  it('should get sample UserKey from User', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "merch@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      UserKey = res.body.UserKey;
      MerchantId = res.body.MerchantId;
      done();
    });
  })

  it('should list the details of user per UserKey', function(done){
    request
    .get('/api/view/user?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list the details of user per UserKey failed");
      }
    })
    .expect(200, done);
  })

  it('should list the details of merchant per MerchantId with cc/CultureCode', function(done){
    request
    .get('/api/view/merchant?MerchantId=' + MerchantId + "&CultureCode=" + CultureCode)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        throw new Error("should list the details of merchant per MerchantId with cc/CultureCode");
      }
    })
    .expect(200, done);
  })

}); 