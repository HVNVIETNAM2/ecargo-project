var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

describe('Color Service', function(){
  
  it('should list all color with missing parameter', function(done){
    request
      .get('/api/color/list')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })

  it('should list all color', function(done){
    request
      .get('/api/color/list?cc=EN')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })

});