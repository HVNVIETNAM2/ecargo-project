var testCommon = require('./test.common.js')();
var request = testCommon.supertest();
var assert = require('assert');

var CONSTANT = require('../logic/constants.js');
var UserKey = null;
var ToBrandId = 1;
var UserInfo = {};

describe('Test Follow Brand', function(){

  it('should login as test@ecargo.com', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "test@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      UserKey = res.body.UserKey;
      done();
    });
  })

  it('should delete follow to brand', function(done){
    request
    .post('/api/follow/brand/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToBrandId : ToBrandId })
    .expect(200, done)
  })

  it('should get details of user', function(done){
    request
    .get('/api/view/user?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        UserInfo = res.body;
      }
    })
    .expect(200, done)
  })

  it('should follow a brand 1', function(done){
    request
    .post('/api/follow/brand/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToBrandId : ToBrandId })
    .expect(200, done)
  })

  it('should follow a brand 2', function(done){
    request
    .post('/api/follow/brand/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToBrandId : ToBrandId })
    .expect(200, done)
  })

  it('should increment FollowingBrandCount by 1', function(done){
    request
    .get('/api/view/user?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body){
        if((UserInfo.FollowingBrandCount + 1) == res.body.FollowingBrandCount){
          UserInfo = res.body;  
        }else{
          throw 'should increment FollowingBrandCount by 1';
        }
      }else{
        throw 'should increment FollowingBrandCount by 1';
      }
    })
    .expect(200, done)
  })

  it('first item should be equal to BrandId which is 1', function(done){
    request
    .get('/api/follow/brand/followed?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body && res.body[0].BrandId == ToBrandId){

      }else{
        throw new Error("first item should be equal to BrandId which is 1");
      }
    })
    .expect(200, done);
  })

  it('first item should be equal to UserKey which is self', function(done){
    request
    .get('/api/follow/brand/following?BrandId=' + ToBrandId)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body && res.body[0].UserKey == UserKey){

      }else{
        throw new Error("first item should be equal to UserKey which is self");
      }
    })
    .expect(200, done);
  })

  it('should delete follow to brand 1', function(done){
    request
    .post('/api/follow/brand/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToBrandId : ToBrandId })
    .expect(200, done)
  })

  it('should delete follow to brand 2', function(done){
    request
    .post('/api/follow/brand/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToBrandId : ToBrandId })
    .expect(200, done)
  })

  it('should decrement FollowingBrandCount by 1', function(done){
    request
    .get('/api/view/user?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body){
        if((UserInfo.FollowingBrandCount - 1) == res.body.FollowingBrandCount){
          UserInfo = res.body;  
        }else{
          throw 'should decrement FollowingBrandCount by 1';
        }
      }else{
        throw 'should decrement FollowingBrandCount by 1';
      }
    })
    .expect(200, done)
  })
}); 