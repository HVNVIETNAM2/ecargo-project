var testCommon = require('./test.common.js');

var app = require('../services/main.js');

testCommon = testCommon(app);
var request = testCommon.supertest();

describe('User with NO valid authentication', function(){
  it('should not allow list MM users', function(done){
    request
      .get('/api/user/list/mm')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(401, done);
  })


  it('should not allow user view', function(done){
    request
      .get('/api/user/view')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(401, done);
  })

  it('should not allow user save', function(done){
    request
      .post('/api/user/save')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(401, done);
  })

  it('should not allow user status change', function(done){
    request
      .post('/api/user/status/change')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(401, done);
  })

  it('should not allow user password change', function(done){
    request
      .post('/api/user/password/change')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(401, done);
  })

  it('should not allow user email change', function(done){
    request
      .post('/api/user/email/change')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(401, done);
  })

  it('should not allow user resend link', function(done){
    request
      .post('/api/user/resend/link')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(401, done);
  })
});
