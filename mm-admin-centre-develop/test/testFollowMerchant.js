var testCommon = require('./test.common.js')();
var request = testCommon.supertest();
var assert = require('assert');

var CONSTANT = require('../logic/constants.js');
var UserKey = null;
var ToMerchantId = 1;
var UserInfo = {};

describe('Test Follow Merchant', function(){

  it('should login as test@ecargo.com', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "test@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      UserKey = res.body.UserKey;
      done();
    });
  })

  it('should delete follow to merchant', function(done){
    request
    .post('/api/follow/merchant/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToMerchantId : ToMerchantId })
    .expect(200, done)
  })

  it('should get details of user', function(done){
    request
    .get('/api/view/user?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(!res.body){
        UserInfo = res.body;
      }
    })
    .expect(200, done)
  })

  it('should follow a merchant 1', function(done){
    request
    .post('/api/follow/merchant/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToMerchantId : ToMerchantId })
    .expect(200, done)
  })

  it('should follow a merchant 2', function(done){
    request
    .post('/api/follow/merchant/save')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToMerchantId : ToMerchantId })
    .expect(200, done)
  })

  it('should increment FollowingMerchantCount by 1', function(done){
    request
    .get('/api/view/user?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body){
        if((UserInfo.FollowingMerchantCount + 1) == res.body.FollowingMerchantCount){
          UserInfo = res.body;  
        }else{
          throw 'should increment FollowingMerchantCount by 1';
        }
      }else{
        throw 'should increment FollowingMerchantCount by 1';
      }
    })
    .expect(200, done)
  })

  it('first item should be equal to MerchantId which is 1', function(done){
    request
    .get('/api/follow/merchant/followed?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body && res.body[0].MerchantId == ToMerchantId){

      }else{
        throw new Error("first item should be equal to MerchantId which is 1");
      }
    })
    .expect(200, done);
  })

  it('first item should be equal to UserKey which is self', function(done){
    request
    .get('/api/follow/merchant/following?MerchantId=' + ToMerchantId)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body && res.body[0].UserKey == UserKey){

      }else{
        throw new Error("first item should be equal to UserKey which is self");
      }
    })
    .expect(200, done);
  })

  it('should delete follow to merchant 1', function(done){
    request
    .post('/api/follow/merchant/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToMerchantId : ToMerchantId })
    .expect(200, done)
  })

  it('should delete follow to merchant 2', function(done){
    request
    .post('/api/follow/merchant/delete')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, ToMerchantId : ToMerchantId })
    .expect(200, done)
  })

  it('should decrement FollowingMerchantCount by 1', function(done){
    request
    .get('/api/view/user?UserKey=' + UserKey)
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .expect('Content-Type', /json/)
    .expect(function(res){
      if(res.body){
        if((UserInfo.FollowingMerchantCount - 1) == res.body.FollowingMerchantCount){
          UserInfo = res.body;  
        }else{
          throw 'should decrement FollowingMerchantCount by 1';
        }
      }else{
        throw 'should decrement FollowingMerchantCount by 1';
      }
    })
    .expect(200, done)
  })
}); 