var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

var CONSTANT = require('../logic/constants.js');

var InvLoc=null;
var Sku=null;

describe('Inventory', function(){

	  it('should update/save location', function(done){		 		  
		request
		  .post('/api/inventory/location/save')
		  .send({InventoryLocationId:2,MerchantId:1,InventoryLocationTypeId:1,LocationName:"Nicer, Newer, Shop",LocationExternalCode:"123456", GeoCountryId:48,GeoIdProvince:2035607,GeoIdCity:1806529,District:'Lower East Side',PostalCode:'2126',Apartment:'None',Floor:'17',BlockNo:"10a",Building:"SkyTower",StreetNo:"45b","Street":"Emily St", QtySafetyThreshold:1})
		  .set('Accept', 'application/json')
		  .set('authorization', testCommon.getToken())
		  
		  .expect('Content-Type', /json/)
		  .end(function(err, res){
			if (res.body.AppCode === 'MSG_ERR_INVENTORY_EXISTS') {
				done();
			}
			if (err) return done(err);
			done();
		  });
	  })
	  	  	  
	  it('should list locations for a merchant', function(done){		 		  
		request
		  .get('/api/inventory/location/list?merchantid=1&cc=CHS')
		  .set('Accept', 'application/json')
		  .set('authorization', testCommon.getToken())
		  
		  .expect('Content-Type', /json/)
		  .expect(200)
		  .end(function(err, res){
			  
			if (err) return done(err);
			done();
		  });
	  })
	  
	  it('should view a single location', function(done){		 		  
		request
		  .get('/api/inventory/location/view?id=2&cc=CHS&merchantid=1')
		  .set('Accept', 'application/json')
		  .set('authorization', testCommon.getToken())
		  
		  .expect('Content-Type', /json/)
		  .expect(200)
		  .end(function(err, res){
			  
			if (err) return done(err);
			InvLoc  = res.body;
			done();
		  });
	  })


	 it('should view a single location without merchantid', function(done){
	      request
	        .get('/api/inventory/location/view?cc=EN&id=2')
	        .set('Accept', 'application/json')
	        .set('authorization', testCommon.getToken())
	        .expect('Content-Type', /json/)
	        .expect(function(res){

	        })
	        .expect(500, done);
	  })

	  
	  it('should change location Status', function(done){		 		  
		var payload={};
		payload.InventoryLocationId=InvLoc.InventoryLocationId;
		  payload.MerchantId=1;
		if (InvLoc.StatusId==2)
			payload.Status='Inactive';
		else
			payload.Status='Active';
			
		request
		  .post('/api/inventory/location/status/change')
		  .send(payload)
		  .set('Accept', 'application/json')
		  .set('authorization', testCommon.getToken())  
		  .expect('Content-Type', /json/)
		  .expect(200)
		  .end(function(err, res){
			  
			if (err) return done(err);
			done();
		  });
	  })
	  
	  
	  it('should get a SKU for further testing', function(done){		 		  
		request
		  .get('/api/product/sku/list?merchantid=1&cc=EN')
		  .set('Accept', 'application/json')
		  .set('authorization', testCommon.getToken())
		  
		  .expect('Content-Type', /json/)
		  .expect(200)
		  .end(function(err, res){
			if (err) return done(err);
			if (!res.body.PageData)
				return done(Error("SkuList PageData null"));
			Sku=res.body.PageData[0];		
			if (!Sku)
				return done(Error("Sku null"));
			done();
		  });
	  })
	  
	it('should try insert but failed due to existing', function(done){          
    request
      .post('/api/inventory/save')
      .send({InventoryId:0,InventoryLocationId:2,SkuId:519,IsPerpetual:0,QtyAllocated:55,MerchantId:1})
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function(res){
	    	
	   })
	   .expect(500, done);
    })

    it('Should Save (Update) Inventory', function(done){          
    request
      .post('/api/inventory/save')
      .send({InventoryId:53,InventoryLocationId:2,SkuId:519,IsPerpetual:0,QtyAllocated:55,MerchantId:1})
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function(res){
	    if(res.body && res.body.InventoryId){

        }else{
            throw new Error("Update inventory failed");
        }
	   })
	   .expect(200, done);
    })
	  
});
