var request = require('supertest');
var app = require('../services/main.js');
var CONSTANT = require('../logic/constants.js');
var lu=require('../logic/logicuser.js');
var cm = require('../lib/commonmariasql.js');
var config=require('../config');
var assert = require('assert');
request=request(app);

var LoginToken = null;
var MobileNumber = '6' + Date.now().toString().slice(-7);
var MobileVerification = null;
var CultureCode = 'EN';

describe('MobileVerification', function(){

  it('should send mobileverification', function(done){
    var tr= new cm();
    tr.begin()
    .then(function () {
      return lu.saveMobileVerification({
        MobileCode: '+852',
        MobileNumber: MobileNumber
      }, tr);
    })
    .then(function (data) {
      MobileVerification = data;
      return tr.commit();
    })
    .then(function () {
      request
      .post('/api/auth/mobileverification/check')
      .set('Accept', 'application/json')
      .send({
        MobileCode: '+852',
        MobileNumber: MobileNumber,
        MobileVerificationId: MobileVerification.MobileVerificationId,
        MobileVerificationToken: MobileVerification.MobileVerificationToken
      })
      .expect(200, done)
    });
  })

  it('user not found error for non-exists mobile', function(done){
    request
    .post('/api/auth/passwordreset')
    .set('Accept', 'application/json')
    .send({
      MobileVerificationId: MobileVerification.MobileVerificationId,
      MobileVerificationToken: MobileVerification.MobileVerificationToken,
      MobileCode : "+852", 
      MobileNumber: MobileNumber, 
      Password: "Bart"
    })
    .expect(function (res) {
      var data = JSON.parse(res.error.text);
      assert.equal(data.AppCode, 'MSG_ERR_USER_NOT_FOUND');
    })
    .expect(500, done);
  })

}); 
