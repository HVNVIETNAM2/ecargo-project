var app = require('../services/main.js');
var testCommon = require('./test.common.js')(app);
var assert = require('assert');
var request = testCommon.supertest();
var _ = require('lodash');

var CONSTANT = require('../logic/constants.js');
var UserOriginal;
var moment = require('moment');

var UserUpdate = {
  FirstName: 'FirstName_test_' + Date.now().toString(36),
  LastName: 'LastName_test_' + Date.now().toString(36),
  MiddleName: 'MiddleName_test_' + Date.now().toString(36),
  DisplayName: 'DisplayName_test_' + Date.now().toString(36),
  GeoCountryId: '47',
  GeoProvinceId: '11000000',
  GeoCityId: '11000031',
  DateOfBirth: Date.now(),
  Gender: 'M'
};

describe('User My Account Update', function(){

  it('should view user', function(done){
    request
      .get('/api/user/view')
      .query({
        UserKey: testCommon.getUserKey()
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        assert.ok(res.body);
        UserOriginal = res.body;
        UserUpdate.UserKey = testCommon.getUserKey();
      })
      .expect(200, done);
  });

  _.forEach(UserUpdate, function (value, field) {
    it('User Update ' + field, function (done) {
      var obj = {
        UserKey: testCommon.getUserKey()
      };
      obj[field] = value;

      request
        .post('/api/user/update')
        .set('Accept', 'application/json')
        .set('authorization', testCommon.getToken())
        .send(obj)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          if (field === 'DateOfBirth') {
            // same day as timestamp
            assert.ok(moment(res.body[field]).isSame(value, 'day'));
          } else {
            assert.deepEqual(res.body[field], value);
          }
        })
        .expect(200, done);
    })
  });

  it('Revert User Update original', function (done) {
    var fields = Object.keys(UserUpdate);
    request
      .post('/api/user/update')
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .send(UserOriginal)
      .expect('Content-Type', /json/)
      .expect(function (res) {
        assert.ok(res.body);
        assert.deepEqual(_.pick(res.body, fields), _.pick(UserOriginal, fields));
        UserUpdate.UserKey = testCommon.getUserKey();
      })
      .expect(200, done);
  })
});
