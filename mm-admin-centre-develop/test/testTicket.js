var testCommon = require('./test.common.js')();
var request = testCommon.supertest();
var assert = require('assert');

var CONSTANT = require('../logic/constants.js');

var UserKey = null;

describe('Ticket', function(){

  it('should get sample UserKey from User', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "merch@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      UserKey = res.body.UserKey;
      done();
    });
  })

  // Follow API
  it('should create a new ticket', function(done){
    request
    .post('/api/ticket/create')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, TicketTypeId : 1, TicketSummary : "Ticket Summary from testTicket.js" })
    .expect(200, done)
  })
}); 