var testCommon = require('./test.common.js')();
var request = testCommon.supertest();


describe('Geo Service', function(){

  it('should list all country with missing parameter', function(done){
    request
      .get('/api/geo/country')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })


  it('should list all country', function(done){
    request
      .get('/api/geo/country?cc=EN')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })

    

  it('should list all province by GeoCountryId but with missing GeoCountryId', function(done){
    request
      .get('/api/geo/province?cc=EN')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })

   it('should list all province by GeoCountryId but with missing CultureCode', function(done){
    request
      .get('/api/geo/province?q=1')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })


  it('should list all province by GeoCountryId', function(done){
    request
      .get('/api/geo/province?cc=EN&q=1')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })


  it('should list all city by GeoId with missing GeoId', function(done){
    request
      .get('/api/geo/city?cc=EN')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })

  it('should list all city by GeoId with missing CultureCode', function(done){
    request
      .get('/api/geo/city?q=1')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })

  it('should list all city by GeoId', function(done){
    request
      .get('/api/geo/city?cc=EN&q=3038816')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })

});
