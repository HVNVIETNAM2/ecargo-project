var testCommon = require('./test.common.js')();
var assert = require('assert');
var request = testCommon.supertest();

var app = require('../services/main.js');
var CONSTANT = require('../logic/constants.js');
var len, UserAddressKey, UserAddressKey2;

describe('UserAddress', function(){

  it('should not create user address with invalid geo', function(done){
    request
      .post('/api/address/save')
      .send({
        UserKey: testCommon.getUserKey(),
        RecipientName: 'Foo Bar',
        GeoCountryId: '47',
        GeoProvinceId: 'whatever',
        GeoCityId: 'foobar',
        PhoneCode: '+852',
        PhoneNumber: '98765432',
        Address: 'TODO'
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        var data = JSON.parse(res.error.text);
        assert.equal(data.AppCode, 'MSG_ERR_GEO_NOT_EXISTS');
      })
      .expect(500, done);
  })

  it('should create default user address', function(done){
    request
      .post('/api/address/save')
      .send({
        UserKey: testCommon.getUserKey(),
        RecipientName: 'Foo Bar',
        GeoCountryId: '47',
        GeoProvinceId: '11000000',
        GeoCityId: '11000031',
        PhoneCode: '+852',
        PhoneNumber: '98765432',
        IsDefault: true,
        Address: 'TODO'
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        assert.ok(res.body.UserAddressKey)
        assert.ok(!res.body.UserAddressId)
        assert.ok(!res.body.UserId)
        assert.ok(res.body.IsDefault)
        assert.ok(res.body.CultureCode)
        assert.ok(res.body.Country)
        assert.ok(res.body.Province)
        assert.ok(res.body.City)
        UserAddressKey = res.body.UserAddressKey;
      })
      .expect(200, done);
  });

  it('should create non default user address', function(done){
    request
      .post('/api/address/save')
      .send({
        UserKey: testCommon.getUserKey(),
        UserAddressKey: '',
        RecipientName: 'Hello World',
        GeoCountryId: '47',
        CultureCode: 'CHS',
        GeoProvinceId: '11000000',
        GeoCityId: '11000031',
        PhoneCode: '+852',
        PhoneNumber: '98765432',
        Address: 'TODO'
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        assert.ok(res.body.UserAddressKey)
        assert.ok(!res.body.IsDefault)
        assert.ok(!res.body.UserId)
        assert.ok(!res.body.UserAddressId)
        UserAddressKey2 = res.body.UserAddressKey;
      })
      .expect(200, done);
  });

  it('should error update non exists user address', function(done){
    request
      .post('/api/address/save')
      .send({
        UserKey: testCommon.getUserKey(),
        UserAddressKey: 'whatever',
        RecipientName: 'Foo Bar',
        GeoCountryId: '47',
        GeoProvinceId: '11000000',
        GeoCityId: '11000031',
        PhoneCode: '+852',
        PhoneNumber: '98765432',
        CultureCode: 'CHS',
        Address: 'TODO',
        IsDefault: true
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        var data = JSON.parse(res.error.text);
        assert.equal(data.AppCode, 'MSG_ERR_USER_ADDRESS_NOT_EXISTS');
      })
      .expect(500, done);
  });

  it('should update user address', function(done){
    request
      .post('/api/address/save')
      .send({
        UserKey: testCommon.getUserKey(),
        UserAddressKey: UserAddressKey2,
        RecipientName: 'Foo Bar',
        GeoCountryId: '47',
        GeoProvinceId: '11000000',
        GeoCityId: '11000031',
        PhoneCode: '+852',
        PhoneNumber: '98765432',
        CultureCode: 'CHS',
        Address: 'TODO'
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        assert.equal(res.body.UserAddressKey, UserAddressKey2);
        assert.ok(!res.body.UserId)
        assert.ok(!res.body.UserAddressId)
        assert.ok(!res.body.IsDefault)
      })
      .expect(200, done);
  });

  it('should set default user address', function(done){
    request
      .post('/api/address/default/save')
      .send({
        UserKey: testCommon.getUserKey(),
        UserAddressKey: UserAddressKey2,
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(200, done);
  });

  it('should view user address', function(done){
    request
      .get('/api/address/view')
      .query({
        UserKey: testCommon.getUserKey(),
        UserAddressKey: UserAddressKey2
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        assert.ok(res.body.UserAddressKey)
        assert.ok(res.body.IsDefault)
        assert.ok(!res.body.UserId)
        assert.ok(!res.body.UserAddressId)
      })
      .expect(200, done);
  });

  it('should view default user address', function(done){
    request
      .get('/api/address/default/view')
      .query({
        UserKey: testCommon.getUserKey()
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        assert.equal(res.body.UserAddressKey, UserAddressKey2)
        assert.ok(res.body.IsDefault)
        assert.ok(!res.body.UserId)
        assert.ok(!res.body.UserAddressId)
      })
      .expect(200, done);
  });

  it('should list user address', function(done){
    request
      .get('/api/address/list?userkey=' + testCommon.getUserKey())
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        assert.ok(Array.isArray(res.body));
        assert.ok(!res.body[0].UserId);
        assert.ok(!res.body[0].UserAddressId);
        len = res.body.length;
      })
      .expect(200, done);
  });

  it('should delete non-exist user address return error', function(done){
    request
      .post('/api/address/delete')
      .send({
        UserKey: testCommon.getUserKey(),
        UserAddressKey: '00000000-cb20-11e5-aaa0-000000000000'
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        var data = JSON.parse(res.error.text);
        assert.equal(data.AppCode, 'MSG_ERR_USER_ADDRESS_NOT_EXISTS');
      })
      .expect(500, done);
  });

  it('should delete user address', function(done){
    request
      .post('/api/address/delete')
      .send({
        UserKey: testCommon.getUserKey(),
        UserAddressKey: UserAddressKey
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(200, done);
  });

  it('should delete user address 2', function(done){
    request
      .post('/api/address/delete')
      .send({
        UserKey: testCommon.getUserKey(),
        UserAddressKey: UserAddressKey2
      })
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(200, done);
  });

  it('should list 2 less user address after 2 deletes', function(done){
    request
      .get('/api/address/list?userkey=' + testCommon.getUserKey())
      .set('Accept', 'application/json')
      .set('authorization', testCommon.getToken())
      .expect('Content-Type', /json/)
      .expect(function (res) {
        assert.ok(Array.isArray(res.body));
        assert.equal(res.body.length, len - 2);
      })
      .expect(200, done);
  });
});
