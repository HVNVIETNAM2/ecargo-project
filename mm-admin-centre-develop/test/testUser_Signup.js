var testCommon = require('./test.common.js');
var assert = require('assert');

var app = require('../services/main.js');
var CONSTANT = require('../logic/constants.js');
var lu=require('../logic/logicuser.js');
var cm = require('../lib/commonmariasql.js');
var config=require('../config');

testCommon = testCommon(app);
var request = testCommon.supertest();

var UserName = 'test_'+Date.now().toString(36);
var MobileNumber = '6' + Date.now().toString().slice(-7);

var MobileVerification = null;
var loginToken = null;
var UserKey = null;
var ProfileImage = null;
var CoverImage = null;

describe('Consumer Signup and Login', function(){
  // skip sms message test logicuser
  it('should save and check mobile verification', function (done) {
    var tr= new cm();
    tr.begin()
    .then(function () {
      return lu.saveMobileVerification({
        MobileCode: '+852',
        MobileNumber: MobileNumber
      }, tr);
    })
    .then(function (data) {
      MobileVerification = data;
      return tr.commit();
    })
    .then(function () {
      request
        .post('/api/auth/mobileverification/check')
        .send({
          MobileCode: '+852',
          MobileNumber: MobileNumber,
          MobileVerificationId: MobileVerification.MobileVerificationId,
          MobileVerificationToken: MobileVerification.MobileVerificationToken
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    })
  })
  it('should error for invalid mobile verification', function (done) {
    request
      .post('/api/auth/mobileverification/check')
      .send({
        MobileCode: '+852',
        MobileNumber: MobileNumber,
        MobileVerificationId: MobileVerification.MobileVerificationId - 1,
        MobileVerificationToken: MobileVerification.MobileVerificationToken
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function (res) {
        var data = JSON.parse(res.error.text);
        assert.equal(data.AppCode, 'LB_CA_VERCODE_INVALID');
      })
      .expect(520, done);
  })
  it('should not signup with invalid mobile verification', function (done) {
    request
      .post('/api/auth/signup')
      .send({
        UserName: UserName,
        DisplayName: "Test " + UserName,
        Password: "pass",
        MobileCode: '+852',
        MobileNumber: MobileNumber,
        MobileVerificationId: MobileVerification.MobileVerificationId - 1,
        MobileVerificationToken: MobileVerification.MobileVerificationToken
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function (res) {
        var data = JSON.parse(res.error.text);
        assert.equal(data.AppCode, 'LB_CA_VERCODE_INVALID');
      })
      .expect(500, done);
  })

  it('should return login token on signup success', function (done) {
    request
      .post('/api/auth/signup')
      .send({
        UserName: UserName,
        DisplayName: "Test " + UserName,
        Password: "pass",
        MobileCode: '+852',
        MobileNumber: MobileNumber,
        MobileVerificationId: MobileVerification.MobileVerificationId,
        MobileVerificationToken: MobileVerification.MobileVerificationToken
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        assert.ok(res.body.Token);
        assert.ok(res.body.UserKey);
        assert.ok(res.body.UserId);
        loginToken = res.body.Token;
        UserKey = res.body.UserKey;
      })
      .expect(200, done);
  })

  it('should upload profile image', function (done) {
    request
      .post('/api/user/upload/profileimage')
      .attach('file', __dirname + '/data/productimage/MD00068_1.jpg')
		  .set('authorization', loginToken)
      .expect(function (res) {
        assert.ok(res.body.ProfileImage);
        ProfileImage = res.body.ProfileImage;
      })
      .expect(200, done);
  })

  it('should upload cover image', function (done) {
    request
      .post('/api/user/upload/coverimage')
      .attach('file', __dirname + '/data/productimage/MD00068_1.jpg')
		  .set('authorization', loginToken)
      .expect(function (res) {
        assert.ok(res.body.CoverImage);
        CoverImage = res.body.CoverImage;
      })
      .expect(200, done);
  })

  it('should remove cover image', function (done) {
    request
      .post('/api/user/upload/coverimage')
		  .set('authorization', loginToken)
      .expect(function (res) {
        assert.equal(res.body.CoverImage, '');
        CoverImage = res.body.CoverImage;
      })
      .expect(200, done);
  })

  it('should view a user', function(done){
    request
      .get('/api/user/view?userkey=' + UserKey) 
      .set('Accept', 'application/json')
      .set('authorization', loginToken)
      .expect('Content-Type', /json/)
      .expect(function (res) {
        assert.equal(res.body.UserName, UserName);
        assert.equal(res.body.UserKey, UserKey);
        assert.equal(res.body.ProfileImage, ProfileImage);
        assert.equal(res.body.UserTypeId, 3); // consumer
      })
      .expect(200, done);
  })

  it('Login by UserName', function (done) {
    request
      .post('/api/auth/login')
      .send({ Username : UserName, Password : "pass" })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        assert.ok(res.body.Token);
        LoginToken = res.body.Token;
      })
      .expect(200, done);
  });

  it('Login by HK mobile', function (done) {
    request
      .post('/api/auth/login')
      .send({ Username : '+852 ' + MobileNumber, Password : "pass" })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        assert.ok(res.body.Token);
        LoginToken = res.body.Token;
      })
      .expect(200, done);
  });

  it('Login by HK MobileNumber without MobileCode', function (done) {
    request
      .post('/api/auth/login')
      .send({ Username : MobileNumber, Password : "pass" })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        assert.ok(res.body.Token);
        loginToken = res.body.Token;
      })
      .expect(200, done);
  });

  it('should reset password by mobile', function(done){
    var tr= new cm();
    tr.begin()
    .then(function () {
      return lu.saveMobileVerification({
        MobileCode: '+852',
        MobileNumber: MobileNumber
      }, tr);
    })
    .then(function (data) {
      MobileVerification = data;
      return tr.commit();
    })
    .then(function () {
      request
      .post('/api/auth/passwordreset')
      .set('Accept', 'application/json')
      .send({
        MobileCode: '+852',
        MobileNumber: MobileNumber,
        MobileVerificationId: MobileVerification.MobileVerificationId,
        MobileVerificationToken: MobileVerification.MobileVerificationToken,
        Password: 'Bart'
      })
      .expect(200, done)
    });
  });

  it('Login with new password', function (done) {
    request
      .post('/api/auth/login')
      .send({ Username : MobileNumber, Password : "Bart" })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        assert.ok(res.body.Token);
        LoginToken = res.body.Token;
      })
      .expect(200, done);
  });

  for (var i = 1; i <= 3; i++) {
    it('should not reset password with used mobile token ' + i, function(done){
      request
      .post('/api/auth/passwordreset')
      .set('Accept', 'application/json')
      .send({
        MobileCode: '+852',
        MobileNumber: MobileNumber,
        MobileVerificationId: MobileVerification.MobileVerificationId,
        MobileVerificationToken: MobileVerification.MobileVerificationToken,
        Password: 'Bart'
      })
      .expect(function (res) {
        var data = JSON.parse(res.error.text);
        assert.equal(data.AppCode, 'LB_CA_VERCODE_INVALID');
      })
      .expect(500, done)
    });
  }

  it('should block save mobile verification at 4th time', function (done) {
    Promise.all([1, 2, 3, 4].map(function () {
      var tr= new cm();
      return tr.begin()
      .then(function () {
        return lu.saveMobileVerification({
          MobileCode: '+852',
          MobileNumber: MobileNumber
        }, tr);
      })
      .then(function (data) {
        return tr.commit();
      })
      .catch(function (err) {
        tr.rollback();
        throw err;
      })
    })).then(done).catch(function (err) {
      assert.equal(err.AppCode, 'MSG_ERR_MOBILE_VERIFICATION_COUNT_EXCEED');
      done();
    })
  });

  it('should block reset password attempt at 4th time', function(done){
    request
    .post('/api/auth/passwordreset')
    .set('Accept', 'application/json')
    .send({
      MobileCode: '+852',
      MobileNumber: MobileNumber,
      MobileVerificationId: 'foobar',
      MobileVerificationToken: 'whatever',
      Password: 'Bart'
    })
    .expect(function (res) {
      var data = JSON.parse(res.error.text);
      assert.equal(data.AppCode, 'MSG_ERR_MOBILE_VERIFICATION_ATTEMPT_COUNT_EXCEED');
    })
    .expect(500, done)
  });

});
