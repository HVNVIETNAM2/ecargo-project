var testCommon = require('./test.common.js')();
var request = testCommon.supertest();

var assert = require('assert');

var CONSTANT = require('../logic/constants.js');

describe('Category Service', function(){
  it('returns error with missing parameter', function(done){
    request
      .get('/api/category/list')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500, done);
  })

  function assertList (res) {
    (function loop (list, parentId) {
      // assert priority ordering
      assert.deepEqual(
        list
        .slice()
        .sort(function (a, b) {
          return a.Priority - b.Priority;
        })
      , list);

      list.forEach(function (item) {
        // assert ParentId
        assert.equal(item.ParentCategoryId, parentId);
        loop(item.CategoryList, item.CategoryId);
      });
    })(res.body, 0);
  }

  it('should list all category sorted by priority', function(done){
    request
      .get('/api/category/list?cc=EN')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(assertList)
      .expect(200, done);
  })

  it('require auth for saving list', function (done) {
    request.post('/api/category/save').expect(401, done);
  })

  var CategoryId;
  it('creates new category', function (done) {
    var name = "test" + Date.now();
    var Category = {
      ParentCategoryId: 0,
      IsIncludeInAutocomplete: 0,
      IsMerchCanSelect: 0,
      ParentCategoryCode: 'CAT0',
      IsForever: 1,
      IsSearchableCategory: 0,
      CategoryNameEN: name + '-en',
      CategoryNameCHS: name + '-chs',
      CategoryNameCHT: name + '-cht',
      CategoryCode: name,
      DefaultCommissionRate: 0.1,
      StatusId: 3 // pending
    };
    request
      .post('/api/category/save')
      .set('authorization', testCommon.getToken())
      .send({ Category: Category })
      .expect(function (res) {
        CategoryId = res.body.Category.CategoryId;
        assert.ok(CategoryId);
      })
      .expect(200, done);
  });

  it('view created category', function (done) {
    request
      .get('/api/category/view')
      .query({ categoryId: CategoryId })
      .expect(200, done);
  });

  it('reorder category', function (done) {
    request
      .post('/api/category/reposition')
      .set('authorization', testCommon.getToken())
      .send({
        CategoryId: CategoryId,
        ParentCategoryId: 0,
        Priority: 1 // top of the list
      })
      .expect(200, done);
  });

  it('activate all pending', function (done) {
    request
      .post('/api/category/status/activate')
      .set('authorization', testCommon.getToken())
      .expect(200, done);
  })

  it('list reordered', function (done) {
    request
      .get('/api/category/list')
      .query({ cc: 'EN' })
      .expect(assertList)
      .expect(function (res) {
        // Reordered item should appear at list[1] 
        // First item is placeholder
        assert.equal(res.body[1].CategoryId, CategoryId);
      })
      .expect(200, done)
  })

  it('delete created category', function (done) {
    request
      .post('/api/category/delete')
      .set('authorization', testCommon.getToken())
      .send({ CategoryId: CategoryId })
      .expect(200, done);
  });

  /*
  it('view deleted category returns error', function (done) {
    request
      .get('/api/category/view')
      .query({ categoryId: CategoryId })
      .expect(404, done);
  });
  */
});
