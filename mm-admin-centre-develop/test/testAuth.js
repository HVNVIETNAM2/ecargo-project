var testCommon = require('./test.common.js')();
var request = testCommon.supertest();
var assert = require('assert');
var CONSTANT = require('../logic/constants.js');

describe('Auth', function() {
  it('should perform simple request', function(done) {
    request
      .get('/api/auth/test')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })

  it('should call activate with missing input fields', function(done) {
    request
      .post('/api/auth/activate')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        assert.equal(res.body.AppCode, 'MSG_ERR_USER_REQUIRED_FIELD_MISSING', 'Unexpected Error AppCode');
      })
      .expect(500, done);
  })

  it('call login with invalid password', function(done) {
    request
      .post('/api/auth/login')
      .send({
        Username: "test@ecargo.com",
        Password: "qqqqwwwwee",
        App: CONSTANT.APP.MERCHANT
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        assert.equal(res.body.AppCode, 'MSG_ERR_USER_AUTHENTICATION_FAIL', 'Unexpected Error AppCode');
      })
      .expect(500, done)
  })

  it('call login with invalid user', function(done) {
    request
      .post('/api/auth/login')
      .send({
        Username: "test22@ecargo.com",
        Password: "Bart",
        App: CONSTANT.APP.MERCHANT
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        assert.equal(res.body.AppCode, 'MSG_ERR_USER_AUTHENTICATION_FAIL', 'Unexpected Error AppCode');
      })
      .expect(500, done);
  })
});