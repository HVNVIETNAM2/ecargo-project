var testCommon = require('./test.common.js')();
var request = testCommon.supertest();
var assert = require('assert');

var CONSTANT = require('../logic/constants.js');
var UserKey = null;
var UserName = 'user_' + new Date().getTime();
console.log(UserName);

describe('Test Change UserName', function(){

  it('should login as test@ecargo.com', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : "test@ecargo.com", Password : "Bart" })
    .expect('Content-Type', /json/)
    .end(function(err, res){
      if (err) return done(err);
      UserKey = res.body.UserKey;
      done();
    });
  })

  it('change UserName to andrew should fail', function(done){
    request
    .post('/api/user/usernamechange')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, UserName : 'andrew' })
    .expect(function(res){
      if(res.body.AppCode != "MSG_ERR_USERNAME_ALREADY_EXISTS"){
        throw 'change UserName to andrew should fail';
      }
    })
    .expect(500, done);
  })

  it('should change UserName to some random stings', function(done){
    request
    .post('/api/user/usernamechange')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .set('authorization', testCommon.getToken())
    .send({ UserKey : UserKey, UserName : UserName })
    .expect(200, done)
  })

  it('should login as the new UserName', function(done){
    request
    .post('/api/auth/login')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Accept', 'application/json')
    .send({ Username : UserName, Password : "Bart" })
    .expect(200, done)
  })
}); 