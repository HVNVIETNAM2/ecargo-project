"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var fs = require('fs');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var lu = require('../logic/logicuser.js');
var path = require('path');
var multer = require('multer');

var UPLOAD_DIR = path.resolve(__dirname, '../uploads/userimages');

var upload = multer({
  dest: UPLOAD_DIR,
  limits: {
    fileSize: 5 * CONSTANTS.MEGABYTE,
    files: 2
  }
});

router.post('/save', upload.fields([
  { name: 'FrontImage', maxCount: 1 },
  { name: 'BackImage', maxCount: 1 }
]));

router.use('/*', secure.applySecurity([
	secure.checkMMAdmin(),
	secure.checkSelf()
]));

router.post('/save', function (req, res) {
  var User;
  var Bale = req.body;
  var tr = new cm();

  Q.when()
  .then(function () {
    if (cu.checkIdBlank(Bale.UserKey))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
    if (cu.checkIdBlank(Bale.FirstName))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "FirstName" };
    if (cu.checkIdBlank(Bale.LastName))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "LastName" };
    if (cu.checkIdBlank(Bale.IdentificationNumber))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "IdentificationNumber" };

    if (!(req.files.FrontImage && req.files.FrontImage.length === 1))
      throw { AppCode: 'MSG_ERR_FILE_INVALID', Message : "FrontImage" };
    if (!(req.files.BackImage && req.files.BackImage.length === 1))
      throw { AppCode: 'MSG_ERR_FILE_INVALID', Message : "BackImage" };

    Bale.FrontImage = req.files.FrontImage[0].filename;
    Bale.BackImage = req.files.BackImage[0].filename;

    return tr.begin();
  })
  .then(function () {
		return lu.getUser(tr, Bale.UserKey).then(function(data) {
			User = data;
      Bale.UserId = User.UserId;
		});
  })
  .then(function () {
    // non-secure version for unblocking alans team
    // todo security concerns
    return tr.queryExecute("INSERT INTO UserIdentification (UserIdentificationKey, UserId, FirstName, LastName, IdentificationNumber, FrontImage, BackImage) VALUES (uuid(), :UserId, :FirstName, :LastName, :IdentificationNumber, :FrontImage, :BackImage)", Bale)
    .then(function (data) {
      return tr.queryOne("SELECT UserIdentificationKey FROM UserIdentification WHERE UserIdentificationId = :insertId", data);
    })
    .then(function (data) {
      User.UserIdentificationKey = data.UserIdentificationKey;
      Bale.UserIdentificationKey = data.UserIdentificationKey;
    });
  })
  .then(function () {
    if (!cu.checkIdBlank(Bale.OrderKey)) {
      // set UserIdentificationKey to Order
      return tr.queryExecute("UPDATE `Order` SET UserIdentificationKey=:UserIdentificationKey WHERE UserId=:UserId AND OrderKey=:OrderKey", Bale)
      .then(function (data) {
        if (parseInt(data.affectedRows) !== 1) throw { AppCode: 'MSG_ERR_ORDER_NOT_EXISTS' };
      });
    }
  })
  .then(function () {
    // Update UserIdentificationKey in My Account
    return tr.queryExecute("UPDATE User SET UserIdentificationKey=:UserIdentificationKey WHERE UserId=:UserId", User)
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json(true);
  })
  .catch(function (error) {
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_USER_IDENTIFICATION_SAVE"));
    return tr.rollback();
  });
});

router.use('/*', function (err, req, res, next) {
  res.status(500).json(cu.createErrorResult(err, "MSG_ERR_USER_IDENTIFICATION_UPLOAD"));
});

module.exports = router;
