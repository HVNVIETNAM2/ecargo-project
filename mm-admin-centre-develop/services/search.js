"use strict";
var express = require('express'), router = express.Router();
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var elasticsearch = require('elasticsearch');

var secure = require('./secure.js');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var lp = require('../logic/logicproduct.js');
var lsearch = require('../logic/logicsearch.js');

var DEFAULT_ELASTICSEARCH_PORT = '9200';
var DEFAULT_ELASTICSEARCH_HOST = 'localhost';
var DEFAULT_ELASTICSEARCH_LOG = 'error';
var DEFAULT_MAX_RESULTS = 10000000;

var client = new elasticsearch.Client({
	host: DEFAULT_ELASTICSEARCH_HOST.concat(':', DEFAULT_ELASTICSEARCH_PORT),
});	

router.get('/style', function (req, res) {
			
	var main = function*(){ 
		try {
			
			var s=req.query.s;	
			var CultureCode=req.query.cc;
			var pagesize=parseInt(req.query.pagesize);
			var pageno=parseInt(req.query.pageno);
			var from= ((pageno-1) * pagesize);
			var qf=null;
			var list=null;
			var pricefrom=null;
			var priceto=null;
			var queryString='(*)';
			
			if (s)
			{
				let v={
					analyzer:"mm_synonyms",
					format:"text",
					text:s,
					index:'mm-synonym'			
				};
				
				let SynonymList=yield client.indices.analyze(v);
				queryString=s;
				console.dir(SynonymList);
				for (let syn of SynonymList.tokens)
				{
					queryString+=" OR " + syn.token;
				}
				queryString="(" + queryString + ")";				
			}
			
			queryString+=lsearch.buildQuery('StyleCode',req.query.stylecode );
			queryString+=lsearch.buildQuery('MerchantId',req.query.merchantid );
			queryString+=lsearch.buildQuery('BadgeId',req.query.badgeid );
			queryString+=lsearch.buildQuery('BrandId',req.query.brandid );
			queryString+=lsearch.buildQuery('CategoryPriorityList.CategoryId',req.query.categoryid );
			queryString+=lsearch.buildQuery('SizeList.SizeId',req.query.sizeid );
			queryString+=lsearch.buildQuery('ColorList.ColorId',req.query.colorid );
			queryString+=lsearch.buildQuery('IsNew',req.query.isnew);
			queryString+=lsearch.buildQuery('IsSale',req.query.issale);


			pricefrom=req.query.pricefrom;
			priceto=req.query.priceto;
			if (pricefrom || priceto)
			{
				if (!pricefrom)
					pricefrom=0;
				if (!priceto)
					priceto=999999999;
				queryString+= " AND (PriceRetail:[" + pricefrom + " TO " + priceto + "])"
			}
			
			if (cu.isBlank(CultureCode))
				throw new Error("cc required to define CultureCode");
				
			if (!pagesize)
				pagesize=1000;
			
			if (!pageno)
				pageno=1;
					
			var query={
				"from" : from, "size" : pagesize,
				"query": {
					"query_string": {						
						"query": queryString,
						"use_dis_max" : true
					}
				},
				"aggs" : {
					"Brand" : {
						"terms" : { "field" : "BrandId" }
					},
					"Merchant" : {
						"terms" : { "field" : "MerchantId" }
					},
					"Badge" : {
						"terms" : { "field" : "BadgeId" }
					},
					"Color" : {
						"terms" : { "field" : "ColorList.ColorId" }
					},
					"Size" : {
						"terms" : { "field" : "SizeList.SizeId" }
					},
					"Category" : {
						"terms" : { "field" : "CategoryPriorityList.CategoryId" }
					},
					"IsNew" : { 
						"sum" : { "field" : "IsNew" } 
					},
					"IsSale" : { 
						"sum" : { "field" : "IsSale" } 
					}
				}
			}
			
			if (req.query.sort)
			{
				var order="asc";
				if (req.query.order=='desc')
					order=req.query.order;
				var sortObj={};
				sortObj[req.query.sort]={"order" : order}
				query.sort = [];
				query.sort.push(sortObj);
			}
			
			var indexName='mm-' + CultureCode.toLowerCase();
						
			var sr=yield client.search({
			  index: indexName,
			  type: 'style',
			  body: query
			});
			
			var naturalResult=_.pluck(sr.hits.hits,'_source');
			var doc={ 
				HitsTotal : sr.hits.total,
				PageTotal : naturalResult.length, 
				PageSize : pagesize, 
				PageCurrent : pageno, 
				PageData : naturalResult,
				Aggregations:{
					CategoryArray:_.pluck(sr.aggregations.Category.buckets,'key'),
					BrandArray:_.pluck(sr.aggregations.Brand.buckets,'key'),
					MerchantArray:_.pluck(sr.aggregations.Merchant.buckets,'key'),
					BadgeArray:_.pluck(sr.aggregations.Badge.buckets,'key'),
					SizeArray:_.pluck(sr.aggregations.Size.buckets,'key'),
					ColorArray:_.pluck(sr.aggregations.Color.buckets,'key'),
					IsSaleCount:sr.aggregations.IsSale.value,
					IsNewCount:sr.aggregations.IsNew.value
				}
			};			
			res.json(doc);
			//res.json(sr);
		}
		catch(err) {			
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_FAIL"));
		}
	};
	Q.spawn(main);			
});

router.get('/complete', function (req, res) {
			
	var main = function*(){
		try {
			
			var s=req.query.s;	
			var CultureCode=req.query.cc;
			var pagesize=parseInt(req.query.pagesize);
			var pageno=parseInt(req.query.pageno);
			var from= ((pageno-1) * pagesize);
			var qf=null;
			var list=null;
			var pricefrom=null;
			var priceto=null;
			var queryString=null;

			if (s==null || s=='')
				queryString='Entity:Trend AND SearchTerm:*';
			else
				queryString='(NOT Entity:Trend) AND ((SearchTermIn:' + s + '*) OR (SearchTerm:' + s + '*))';
				
			if (cu.isBlank(CultureCode))
				throw new Error("cc required to define CultureCode");
				
			if (!pagesize)
				pagesize=1000;
			
			if (!pageno)
				pageno=1;
			
			console.log(queryString);
			
			var query={
				"from" : from, "size" : pagesize,
				"query": {
					"query_string": {
						"query": queryString,
						"use_dis_max" : true
					}
				}				
			}
			
			if (req.query.sort)
			{
				var order="asc";
				if (req.query.order=='desc')
					order=req.query.order;
				var sortObj={};
				sortObj[req.query.sort]={"order" : order}
				query.sort = [];
				query.sort.push(sortObj);
			}
			
			var indexName='mm-' + CultureCode.toLowerCase();
			
			var sr=yield client.search({
			  index: indexName,
			  type : "term",
			  body: query
			});
			
			var naturalResult=_.pluck(sr.hits.hits,'_source');
			//naturalResult=_.pluck(naturalResult,'SearchTerm');
			var doc={ 
				HitsTotal : sr.hits.total,
				PageTotal : naturalResult.length, 
				PageSize : pagesize, 
				PageCurrent : pageno, 
				PageData : naturalResult  
			};			
			res.json(naturalResult);
		}
		catch(err) {			
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_FAIL"));
		}
	};
	Q.spawn(main);			
});

router.get('/brand', function (req, res) {			
	var main = function*(){
		try {
			var naturalResult=yield lsearch.standardSearch(client,'brand',req.query.cc, req.query.pagesize,req.query.sort,req.query.order,req.query.s);
			res.json(naturalResult);
		}
		catch(err) {			
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_FAIL"));
		}
	};
	Q.spawn(main);			
});

router.get('/brand/combined', function (req, res) {			
	var main = function*(){
		try {
			var naturalResult=yield lsearch.standardSearch(client,'brand-combined',req.query.cc, req.query.pagesize,req.query.sort,req.query.order,req.query.s);
			res.json(naturalResult);
		}
		catch(err) {			
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_FAIL"));
		}
	};
	Q.spawn(main);			
});

router.get('/merchant', function (req, res) {			
	var main = function*(){
		try {
			var naturalResult=yield lsearch.standardSearch(client,'merchant',req.query.cc, req.query.pagesize,req.query.sort,req.query.order,req.query.s);
			res.json(naturalResult);
		}
		catch(err) {			
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_FAIL"));
		}
	};
	Q.spawn(main);			
});

router.get('/category', function (req, res) {			
	var main = function*(){
		try {
			var naturalResult=yield lsearch.standardSearch(client,'category',req.query.cc, req.query.pagesize,req.query.sort,req.query.order,req.query.s);
			res.json(naturalResult);
		}
		catch(err) {			
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_FAIL"));
		}
	};
	Q.spawn(main);			
});

router.get('/size', function (req, res) {			
	var main = function*(){
		try {
			var naturalResult=yield lsearch.standardSearch(client,'size',req.query.cc, req.query.pagesize,req.query.sort,req.query.order,req.query.s);
			res.json(naturalResult);
		}
		catch(err) {			
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_FAIL"));
		}
	};
	Q.spawn(main);			
});

router.get('/size/group', function (req, res) {			
	var main = function*(){
		try {
			var naturalResult=yield lsearch.standardSearch(client,'size-group',req.query.cc, req.query.pagesize,req.query.sort,req.query.order,req.query.s);
			res.json(naturalResult);
		}
		catch(err) {			
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_FAIL"));
		}
	};
	Q.spawn(main);			
});

router.get('/color', function (req, res) {			
	var main = function*(){
		try {
			var naturalResult=yield lsearch.standardSearch(client,'color',req.query.cc, req.query.pagesize,req.query.sort,req.query.order,req.query.s);
			res.json(naturalResult);
		}
		catch(err) {			
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_FAIL"));
		}
	};
	Q.spawn(main);			
});

router.get('/badge', function (req, res) {			
	var main = function*(){
		try {
			var naturalResult=yield lsearch.standardSearch(client,'badge',req.query.cc, req.query.pagesize,req.query.sort,req.query.order,req.query.s);
			res.json(naturalResult);
		}
		catch(err) {			
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_FAIL"));
		}
	};
	Q.spawn(main);			
});

module.exports = router;