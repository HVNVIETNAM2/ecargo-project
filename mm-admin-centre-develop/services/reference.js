"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var fs=require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var li = require('../logic/logicinventory.js');
var lu = require('../logic/logicuser.js');
var lp = require('../logic/logicproduct.js');

//this may need to be auth secured for securoty reason but leaving public for now....

//injected service by gerald
router.get('/changelanguage', function (req,res){
	var tr= new cm();
	var Reference = {};
	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryMany("select * from Language l , LanguageCulture lc where l.LanguageId=lc.LanguageId and lc.CultureCode=l.LanguageNameInvariant").then(function(data) {
			Reference.LanguageList=data;
		})
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Reference);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_REFERENCE_FAIL"));
		return tr.rollback();

	})
	.done();
});
//end injected service by gerald

router.get('/general', function (req, res) {
	var tr= new cm();
	var Reference={};
	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.cc))
			throw new Error("cc required to define culture code");
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryMany("select * from Status s , StatusCulture sc where s.StatusId=sc.StatusId and sc.CultureCode=:cc",{cc:req.query.cc}).then(function(data) {
			Reference.StatusList=data;
		});
	})
	.then(function() {
		return tr.queryMany("select * from Language l , LanguageCulture lc where l.LanguageId=lc.LanguageId and lc.CultureCode=:cc",{cc:req.query.cc}).then(function(data) {
			Reference.LanguageList=data;
		});
	})
	.then(function() {
		return tr.queryMany("select * from TimeZone t , TimeZoneCulture tc where t.TimeZoneId=tc.TimeZoneId and tc.CultureCode=:cc",{cc:req.query.cc}).then(function(data) {
			Reference.TimeZoneList=data;
		});
	})
	.then(function() {
		return tr.queryMany("select * from MobileCode m , MobileCodeCulture mc where m.MobileCodeId=mc.MobileCodeId and mc.CultureCode=:cc",{cc:req.query.cc}).then(function(data) {
			Reference.MobileCodeList=data;
		});
	})
	.then(function() {
		return tr.queryMany("select * from SecurityGroup sg , SecurityGroupCulture sgc where sg.SecurityGroupId=sgc.SecurityGroupId and sgc.CultureCode=:cc order by sg.Priority",{cc:req.query.cc}).then(function(data) {
			Reference.SecurityGroupList=data;
		});
	})
	.then(function() {
		return tr.queryMany("select * from InventoryLocationType il , InventoryLocationTypeCulture ilc where il.InventoryLocationTypeId=ilc.InventoryLocationTypeId and ilc.CultureCode=:cc",{cc:req.query.cc}).then(function(data) {
			Reference.InventoryLocationTypeList=data;
		});
	})
	.then(function() {
		return tr.queryMany("select * from MerchantType mt , MerchantTypeCulture mtc where mt.MerchantTypeId=mtc.MerchantTypeId and mtc.CultureCode=:cc",{cc:req.query.cc}).then(function(data) {
			Reference.MerchantTypeList=data;
		});
	})
	.then(function() {
		return lp.colorList(req.query.cc, tr).then(function(data) {
		//return tr.queryMany("select * from Color cl , ColorCulture clc where cl.ColorId=clc.ColorId and clc.CultureCode=:cc",{cc:req.query.cc}).then(function(data) {
			Reference.ColorList=data;
		});
	})
	.then(function() {
		/*
		return tr.queryMany("select sz.SizeId, sz.SizeCode, szc.SizeName, szc.SizeCultureId, sz.SizeNameInvariant, sg.SizeGroupId, sg.SizeGroupCode, sg.SizeGroupNameInvariant, sgc.SizeGroupName, sgc.CultureCode "
		+ "from Size sz "
		+ "inner join SizeCulture szc on (sz.SizeId=szc.SizeId and szc.CultureCode=:cc) "
		+ "inner join SizeGroup sg on (sz.SizeGroupId=sg.SizeGroupId) "
		+ "inner join SizeGroupCulture sgc on (sg.SizeGroupId=sgc.SizeGroupId and sgc.CultureCode=:cc)",{cc:req.query.cc}).then(function(data) {
		*/
		return lp.sizeList(req.query.cc, tr).then(function(data) {
			Reference.SizeList=data;
		});
	})
	.then(function() {
    return li.getStatusList(req.query.cc, tr).then(function(data){
        Reference.InventoryStatusList=data;
    });
	})
	.then(function() {
		return tr.queryMany("select * from Season s inner join SeasonCulture sc on s.SeasonId=sc.SeasonId where sc.CultureCode=:cc",{cc:req.query.cc}).then(function(data) {
			Reference.SeasonList=data;
		})
	})
	.then(function() {
		return tr.queryMany("select * from Badge s inner join BadgeCulture sc on s.BadgeId=sc.BadgeId where sc.CultureCode=:cc",{cc:req.query.cc}).then(function(data) {
			Reference.BadgeList=data;
		})
	})
	.then(function() {
            return lu.getUserTypeList(tr).then(function(data){
                Reference.UserTypeList=data;
            });
	})
	.then(function() {
            Reference.FileUploadSize = config.fileSize;
	})
	.then(function() {
            Reference.PerpetualAllocationNo = config.inventory.perpetualAllocationNo;
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Reference);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_REFERENCE_FAIL"));
		return tr.rollback();

	})
	.done();

});

router.get('/translation', function (req, res) {
	var tr= new cm();
	var Reference={};
	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.cc))
			throw new Error("cc required to define culture code");
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryMany("select * from Translation t where t.CultureCode=:cc",{cc:req.query.cc}).then(function(data) {
			// Reference.TranslationList=data;
			Reference.TranslationMap = _.reduce(data, function(resultMap, row) {
				resultMap[row.TranslationCode] = row.TranslationName;
				return resultMap;
			}, {});
		})
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Reference);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_REFERENCE_FAIL"));
		return tr.rollback();
	})
	.done();
});

module.exports = router;
