"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var jwt = require('jsonwebtoken');
var path = require('path');
var _ = require('lodash');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var cp = require('../lib/commonphone.js');
var lu = require('../logic/logicuser.js');

var thisRef=this;

var MOBILE_MAYBE_REGEX = /^[^a-zA-Z][^@a-zA-Z]+$/

exports.setup = function(app)
{

	app.use(passport.initialize());

	passport.use(new LocalStrategy(
		{
			usernameField: 'Username',
			passwordField: 'Password',
			session: false,
			passReqToCallback: true
		},
		function(req, username, password, done) {
			var tr = new cm();
			var User={};
			var LoginAttempts=0;

      var IsMobile = MOBILE_MAYBE_REGEX.test(username)

      // HK, CN mobile parsing
      var hkNumber = cp.parseNumber(username, 'HK');
      var cnNumber = cp.parseNumber(username, 'CN');
      if (hkNumber) {
        username = '+852' + hkNumber;
      } else if (cnNumber) {
        username = '+86' + cnNumber;
      }

			Q.when()
			.then(function(){
			 	return tr.begin();
			})
			.then(function() {
				var params = { Username: username };
				//validation rule for username: RID_TB_EMAIL_OR_MOBILE_PATTERN
				return tr.queryOne("select u.UserId,u.UserKey,u.UserTypeId, u.MerchantId, u.InventoryLocationId, u.Hash, u.Salt, u.ActivationToken, u.LoginAttempts, u.UserName, u.Email,u.MobileCode, u.MobileNumber,u.StatusId, u.LastLogin from User u where (u.UserName=:Username or u.Email=:Username or CONCAT(u.MobileCode, u.MobileNumber)=:Username) and u.StatusId <> 1",params).then(function(data) {
					User=data;
					//throw so that no login logic is handled if invalid status
					if (User==null)
						throw {AppCode: 'MSG_ERR_USER_AUTHENTICATION_FAIL'};
					if (User.StatusId==CONSTANTS.STATUS.DELETED || User.StatusId==CONSTANTS.STATUS.INACTIVE)
						throw {AppCode: 'MSG_ERR_USER_STATUS_INVALID'};
					if (User.StatusId==CONSTANTS.STATUS.PENDING)
						throw {AppCode: 'MSG_ERR_USER_STATUS_PENDING'};
					if (cu.isBlank(User.Hash) || cu.isBlank(User.Salt))
						throw {AppCode: 'MSG_ERR_USER_CORRUPT_DATA'};
				});
			})
			.then(function() {
				if (cu.isBlank(User.ErrorAppCode))
				{
					//if we get here we have found a valid User object
					var loginHash = cc.createHash(password, User.Salt);

					if (User.Hash == loginHash) {
						// Login successful
						return tr.queryExecute("update User set LastLogin=UTC_TIMESTAMP(), LoginAttempts=0, TokenAttempts=0 where UserId=:UserId;",User).then(function(data) {
							return lu.getUser(tr, User.UserId).then(function (data) {
								User = data;
							});
						});
					}
					else {
						User.ErrorAppCode = 'MSG_ERR_USER_AUTHENTICATION_FAIL';

						// Update attempts count
						LoginAttempts = User.LoginAttempts + 1;
						return tr.queryExecute("update User set LoginAttempts=LoginAttempts + 1 where UserId=:UserId;", User).then(function (data) {
							if (LoginAttempts >= config.loginAttempts) {
								// Instead of locking account, we will implement captcha logic later.
								// Return an error code to signal UI to show captcha.
								// TODO

								/**
								User.ActivationToken = cc.createSimpleToken();
								return tr.queryExecute("update User set LoginAttempts=LoginAttempts + 1, StatusId=3, LastStatus=UTC_TIMESTAMP(), ActivationToken=:ActivationToken, StatusReasonCode='RC_EXCESS_SIGNIN_LIMIT' where UserId=:UserId;", User).then(function (data) {
									User.ErrorAppCode = "MSG_ERR_USER_ATTEMPTS_EXCEEDED";
									return;
								});
								**/
							}
						});
					}
				}
			})
			.then(function() {
				return tr.commit();
			})
			.catch(function(err){
				console.dir(err);
				if (User==null)
					User={};
				if (err.AppCode)
					User.ErrorAppCode = err.AppCode; //make sure we pass this back out to auth/login
				else {
					User.ErrorAppCode = 'MSG_ERR_USER_AUTHENTICATION_EXCEPTION';
					User.Message = JSON.stringify(err);
				}
				return tr.rollback();
			})
			.done(function () {
				if (!User) {
					return done(null, false, {
						AppCode: 'MSG_ERR_USER_AUTHENTICATION_FAIL',
						Username: username,
						LoginAttempts: 0,
            IsMobile: IsMobile
					});
				}
				if (!cu.isBlank(User.ErrorAppCode)){
					return done(null, false, {
						AppCode: User.ErrorAppCode,
						Username: username,
						LoginAttempts: LoginAttempts,
						User: User,
            IsMobile: IsMobile
					});
				}
				return done(null, User);
			});
		})
	);
};

/**
 *
 * @param {Array.<function>} predicates
 * @returns {Function}
 */
exports.applySecurity = function (predicates) {
	var self = this;
	return function(req, res, next) {
		self.checkAuth(req, res).then(function (user) {
            if (user === req.AccessUser && ((user.UserSecurityGroupArray && user.UserSecurityGroupArray.length > 0) || user.UserTypeId === CONSTANTS.USER_TYPE.CONSUMER)) {
				return Q.allSettled(_.map(predicates, function (predicate) {
                    return predicate(req, user);
                }))
                .then(function (results) {
					if(_.any(results, function (result) {
						if (result.state !== 'fulfilled') {
							throw new Error(result);
						}
                        return result.state === 'fulfilled' && result.value === true;
					})) {
						//check merchant id exists if merchant id is passed
						var merchantId = _.parseInt(typeof req.body.MerchantId === 'undefined' ? req.query.merchantid : req.body.MerchantId);
                                                
                                                //MM-4541: [API] Save Sku - with "MerchantId" = 0, should return "MSG_ERR_MERCHANT_NOT_FOUND" instead of "MSG_ERR_REQUIRED_FIELD_MISSING"
						if (!_.isNaN(merchantId) && merchantId !== 0) {
//						if (!_.isNaN(merchantId)) {
							var tr = new cm();
							return Q.when().then(function () {
								return tr.begin();
							}).then(function () {
								return tr.queryOne('SELECT MerchantId FROM Merchant WHERE MerchantId=:MerchantId', {MerchantId: merchantId});
							}).then(function (data) {
								if (cu.isBlank(data)) {
									throw { AppCode: 'MSG_ERR_MERCHANT_NOT_FOUND' };
								}
								return true;
							}).then(function () {
								return tr.commit();
							}).then(function () {
								next();
							}).catch(function (err) {
								tr.rollback();
								throw err;
							});
						} else {
							next();
						}
                    } else {
                        throw new Error("Unauthorized");
                    }
                }).catch(function (err) {
					throw err;
				});
            } else {
                throw new Error("Unauthorized");
            }
        }).catch(function (err) {
			//should be status code 401 if we want to force logout in frontend
            res.status(403).json(cu.createErrorResult(err, "MSG_ERR_USER_UNAUTHORIZED"));
        });
	}
};

/**
 * @param {Array.<number>} groups
 * @returns {Function}
 */
//exports.matchSecurityGroups = function (groups) {
//    return function (req, user) {
//        return Q.when().then(function () { return _.intersection(user.UserSecurityGroupArray, groups).length > 0; });
//    }
//};

/**
 * @returns {function}
 */
exports.checkMMAdmin = function () {
	return function (req, user) {
		var deferred = Q.defer();
		Q.when()
			.then(function () {
				deferred.resolve(user.UserTypeId === CONSTANTS.USER_TYPE.MM &&
					_.indexOf(user.UserSecurityGroupArray, CONSTANTS.USER_SECURITY_GROUP.MM_ADMIN) !== -1);
			})
			.catch(function(err){
				deferred.reject(err);
			});
		return deferred.promise;
	}
};

/**
 * @returns {function}
 */
exports.checkMMUser = function () {
    return function (req, user) {
        var deferred = Q.defer();
        Q.when()
            .then(function () {
                deferred.resolve(user.UserTypeId === CONSTANTS.USER_TYPE.MM);
            })
            .catch(function(err){
                deferred.reject(err);
            });
        return deferred.promise;
    }
};

/**
 * @returns {function}
 */
exports.checkMMAdminOrMerchantAdmin = function () {
	return function (req, user) {
		var isAuth = false;
		var tr = new cm();
		var merchantId = parseInt(!cu.isBlank(req.body.MerchantId) ? req.body.MerchantId : req.query.merchantid);
		var deferred = Q.defer();
		Q.when()
			.then(function(){
				return tr.begin();
			})
			.then(function () {
				return tr.queryOne('SELECT UserId, UserTypeId, MerchantId FROM User WHERE UserId = :UserId OR UserKey = :UserId', {UserId: cu.reqUser(req)});
			})
			.then(function (u) {
				//edit user is mm => only mmadmin
				//edit user is merch => only mmadmin and merch admin
				return isAuth = (u !== null && ((u.UserTypeId === CONSTANTS.USER_TYPE.MM && user.UserTypeId === CONSTANTS.USER_TYPE.MM &&
					_.indexOf(user.UserSecurityGroupArray, CONSTANTS.USER_SECURITY_GROUP.MM_ADMIN) !== -1) ||
					(u.UserTypeId === CONSTANTS.USER_TYPE.MERCHANT &&
						(user.UserTypeId === CONSTANTS.USER_TYPE.MM || (user.UserTypeId === CONSTANTS.USER_TYPE.MERCHANT &&
					_.indexOf(user.UserSecurityGroupArray, CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ADMIN) !== -1 && merchantId === user.MerchantId)))));
			})
			.then(function () {
				return tr.commit();
			})
			.then(function () {
				deferred.resolve(isAuth);
			})
			.catch(function(err){
				tr.rollback();
				deferred.reject(err);
			});
		return deferred.promise;
	}
};


/**
 * @returns {function}
 */
exports.checkMerchAdmin = function () {
    return function (req, user) {
        var deferred = Q.defer();
        Q.when()
            .then(function () {
                deferred.resolve(user.UserTypeId === CONSTANTS.USER_TYPE.MERCHANT &&
                    _.indexOf(user.UserSecurityGroupArray, CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ADMIN) !== -1 &&
					parseInt(!cu.isBlank(req.body.MerchantId) ? req.body.MerchantId : req.query.merchantid) === user.MerchantId);
            })
            .catch(function(err){
                deferred.reject(err);
            });
        return deferred.promise;
    }
};

/**
 * @returns {function}
 */
exports.checkMerchUser = function () {
    return function (req, user) {
        var deferred = Q.defer();
        Q.when()
            .then(function () {
                deferred.resolve(user.UserTypeId === CONSTANTS.USER_TYPE.MERCHANT &&
					parseInt(!cu.isBlank(req.body.MerchantId) ? req.body.MerchantId : req.query.merchantid) === user.MerchantId);
            })
            .catch(function(err){
                deferred.reject(err);
            });
        return deferred.promise;
    }
};

/**
 * @returns {function}
 */
exports.checkSelf = function (onlyActive) {
	return function (req, user) {
		var deferred = Q.defer();
		Q.when()
			.then(function () {
				deferred.resolve((user.UserId === _.parseInt(cu.reqUser(req)) || user.UserKey === cu.reqUser(req)) &&
					!onlyActive || (onlyActive && user.StatusId === CONSTANTS.STATUS.ACTIVE));
			})
			.catch(function(err){
				deferred.reject(err);
			});
		return deferred.promise;
	}
};

/**
 * @returns {function}
 */
exports.isCreateNewUser = function () {
	return function (req, user) {
		var deferred = Q.defer();
		Q.when()
			.then(function () {
				//create user is mm => only mmadmin
				//create user is merch => only mmadmin and merch admin
				deferred.resolve((cu.checkIdBlank(req.body.UserId) && cu.checkIdBlank(req.body.UserKey) &&
				((req.body.MerchantId === undefined && user.UserTypeId === CONSTANTS.USER_TYPE.MM &&
					_.indexOf(user.UserSecurityGroupArray, CONSTANTS.USER_SECURITY_GROUP.MM_ADMIN) !== -1) ||
				(req.body.MerchantId !== undefined && ((user.UserTypeId === CONSTANTS.USER_TYPE.MM) ||
				(user.UserTypeId === CONSTANTS.USER_TYPE.MERCHANT &&
				_.indexOf(user.UserSecurityGroupArray, CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ADMIN) !== -1 &&
				parseInt(req.body.MerchantId) === user.MerchantId))))));
			})
			.catch(function(err){
				deferred.reject(err);
			});
		return deferred.promise;
	}
};

exports.checkAuth = function (req, res, next) {

	req.AccessUser={};
	var tr = new cm();
	var User=null;
	return Q.when()
	.then(function(){		
		var token = (req.body && req.body.AccessToken) || (req.query && req.query.AccessToken) || req.headers['x-access-token'] || req.headers['authorization'] || req.headers['Authorization'] || req.headers['access-token'];
		if (token)
		{
			var bearerString="Bearer ";
			if (token.indexOf(bearerString)==0)
			{
				//token=token.replace(bearerString,'');
				token=token.replace(/Bearer\s+/g,'');
				token=token.replace(/\"/g,'');
			}
			
			var deco=cc.decodeJwtToken(token);
			console.log(deco);
			req.AccessUser.UserId=deco.UserId;
			req.AccessUser.UserKey=deco.UserKey;
		}
		
		if (!req.AccessUser.UserId && !req.AccessUser.UserKey)
		{
			throw new Error("No Valid Token");
		}
	})
	.then(function(){
		return tr.begin();
		
	})
	.then(function(){
		return lu.getUser(tr, req.AccessUser.UserId || req.AccessUser.UserKey).then(function(data) {
			User=data;
		});
	})
	.then(function(){
		if (User.StatusId!=CONSTANTS.STATUS.ACTIVE)
			throw new Error("Status Invalid");
		if (User.PushLogout==true)
			throw new Error("Forced");
		req.AccessUser=User;
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		if (next === undefined) {
			return req.AccessUser;
		}
		return next();
	})
	.catch(function(err){
		res.status(401).json(cu.createErrorResult(err,"MSG_ERR_USER_UNAUTHORIZED"));
		return tr.rollback();
	});
};

exports.checkMmAdminOrSelf = function (req, res, next) {

	var ok=false;
	if (_.contains(req.AccessUser.UserSecurityGroupArray,CONSTANTS.USER_SECURITY_GROUP.MM_ADMIN))
	{
		ok=true;
	}
	else
	{
    ok = cu.isReqSelf(req);
	}


	if (ok)
		return next();

	return res.status(401).json(cu.createErrorResult(new Error("Invalid Permissions"),"MSG_ERR_USER_UNAUTHORIZED"));
}

exports.checkMmMerchantAdminOrSelf = function (req, res, next) {

	var ok=false;
	if (_.contains(req.AccessUser.UserSecurityGroupArray,CONSTANTS.USER_SECURITY_GROUP.MM_ADMIN) ||
            _.contains(req.AccessUser.UserSecurityGroupArray,CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ADMIN))
	{
		ok=true;
	}
	else
	{
    ok = cu.isReqSelf(req);
	}


	if (ok)
		return next();

	return res.status(401).json(cu.createErrorResult(new Error("Invalid Permissions"),"MSG_ERR_USER_UNAUTHORIZED"));
}

exports.checkMmAdminOrMerchant = function (req, res, next) {

	var ok=false;
	if (_.contains(req.AccessUser.UserSecurityGroupArray,CONSTANTS.USER_SECURITY_GROUP.MM_ADMIN))
	{
		ok=true;
	}
	else
	{
		var MerchantId=0;
		if (req.query.MerchantId)
		{
			MerchantId=req.query.MerchantId;
			delete req.body.MerchantId; //if UserId was specifed on query string then don't also let it be specifed int eh body otherwise someone might use the other to override. userId can only be specifed once.
		}
		if (req.query.merchantid)
		{
			MerchantId=req.query.merchantid;
			delete req.body.MerchantId; //if UserId was specifed on query string then don't also let it be specifed int eh body otherwise someone might use the other to override. userId can only be specifed once.
		}
		else
		{
			MerchantId=req.body.MerchantId;
		}
		if (MerchantId==req.AccessUser.MerchantId)
			ok=true;
	}

	if (ok)
		return next();

	return res.status(401).json(cu.createErrorResult(new Error("Invalid Permissions"),"MSG_ERR_USER_UNAUTHORIZED"));
}

exports.checkMmAdmin = function (req, res, next) {
	if (_.contains(req.AccessUser.UserSecurityGroupArray,CONSTANTS.USER_SECURITY_GROUP.MM_ADMIN))
		return next();
	else
		return res.status(401).json(cu.createErrorResult(new Error("Invalid Permissions"),"MSG_ERR_USER_UNAUTHORIZED"));
}

exports.checkMmMerchantAdmin = function (req, res, next) {
	if (_.contains(req.AccessUser.UserSecurityGroupArray,CONSTANTS.USER_SECURITY_GROUP.MM_ADMIN) || _.contains(req.AccessUser.UserSecurityGroupArray,CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ADMIN))
		return next();
	else
		return res.status(401).json(cu.createErrorResult(new Error("Invalid Permissions"),"MSG_ERR_USER_UNAUTHORIZED"));
}
