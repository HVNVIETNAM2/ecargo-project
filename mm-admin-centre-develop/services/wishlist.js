"use strict";
var express = require('express'), router = express.Router();
var path = require('path');
var Q = require('q');
var throat = require('throat');

var secure = require('./secure.js');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var lp = require('../logic/logicproduct.js');
var lt = require('../logic/logicterm.js');
var lwish = require('../logic/logicwishlist.js');
var lcat = require('../logic/logiccategory.js');
var secure = require('./secure.js');
	
var getInfo = Q.async(function* (tt) {
	var myres=yield tt.info();
	return myres;
});

router.post('/create', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.UserKey))
				throw Error("UserKey is required. Please specify 0 for unauthenticated wishlist.");
			yield tr.begin();
			var ret=yield lwish.create(tr,Bale.CultureCode,Bale.UserKey);
			yield tr.commit();
			res.json(ret);			
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_WISHLIST"));			
		}
	};
	Q.spawn(main);	
});


router.post('/user/update', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				throw Error("CartKey is required.");
			if (cu.isBlank(Bale.UserKey))
				throw Error("UserKey is required.");
			yield tr.begin();
			var ret=yield lwish.userUpdate(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_WISHLIST"));			
		}
	};
	Q.spawn(main);	
});

router.post('/item/add', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				Bale.CartKey = null;
			if(cu.isBlank(Bale.UserKey))
				Bale.UserKey = null;
			// if( !Bale.UserKey && !Bale.CartKey )
			// 	throw Error('CartyKey/UserKey is required.');
			if (cu.isBlank(Bale.StyleCode))
				throw Error("StyleCode is required.");
			if (cu.isBlank(Bale.MerchantId))
				throw Error("MerchantId is required.");

			//optional fields
			//additional notes:
			//SkuID will be 0, ColorKey will be Null to represent not there
			if (cu.checkIdBlank(Bale.SkuId)){
				Bale.SkuId = 0;
			};
			if (cu.isBlank(Bale.ColorKey)){
				Bale.ColorKey = null;
			};

			yield tr.begin();
			var ret=yield lwish.itemAdd(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey,Bale.StyleCode,Bale.SkuId,Bale.ColorKey,Bale.MerchantId);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_WISHLIST"));			
		}
	};
	Q.spawn(main);	
});

router.post('/item/remove', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				Bale.CartKey = null;
			if(cu.isBlank(Bale.UserKey))
				Bale.UserKey = null;
			if( !Bale.UserKey && !Bale.CartKey )
				throw Error('CartyKey/UserKey is required.');
			if (cu.checkIdBlank(Bale.CartItemId))
				throw Error("CartItemId is required.");
			yield tr.begin();
			var ret=yield lwish.itemRemove(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey,Bale.CartItemId);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_WISHLIST"));			
		}
	};
	Q.spawn(main);	
});

router.get('/view/user', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale.CultureCode=req.query.cc;
			Bale.UserKey=req.query.userkey;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.checkIdBlank(Bale.UserKey))
				throw Error("UserKey is required.");
			yield tr.begin();
			var ret=yield lwish.viewByUserKey(tr,Bale.CultureCode,Bale.UserKey);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_WISHLIST"));			
		}
	};
	Q.spawn(main);	
});


router.get('/view', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale.CultureCode=req.query.cc;
			Bale.CartKey=req.query.cartkey;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				throw Error("CartKey is required.");
			yield tr.begin();
			var ret=yield lwish.viewByCartKey(tr,Bale.CultureCode,Bale.CartKey);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_WISHLIST"));			
		}
	};
	Q.spawn(main);	
});

router.post('/merge', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				Bale.CartKey = null;
			if(cu.isBlank(Bale.UserKey))
				Bale.UserKey = null;
			if( !Bale.UserKey && !Bale.CartKey )
				throw Error('CartyKey/UserKey is required.');
			if (cu.checkIdBlank(Bale.MergeCartKey))
				throw Error("MergeCartKey is required.");
			if( Bale.CartKey == Bale.MergeCartKey )
				throw Error("MergeCartKey and CartKey should not be the same.");

			yield tr.begin();
			var ret=yield lwish.mergeWishlist(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey,Bale.MergeCartKey);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_WISHLIST"));			
		}
	};
	Q.spawn(main);	
});

module.exports = router;

