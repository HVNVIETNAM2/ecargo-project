"use strict";
var express = require('express'), router = express.Router();
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');

var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var logicterm = require('../logic/logicterm.js');

router.get('/list', function(req, res){
	var tr = new cm();
	var SearchList = null;

	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.typecode))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicterm.list(tr, req.query.typecode).then(function(data){
			SearchList = data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(SearchList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/insert', secure.applySecurity([secure.checkMMUser()]), function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.SearchTerm))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		if (cu.isBlank(Bale.SearchTermTypeCode))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicterm.insert(tr, Bale).then(function(data){
			Bale.SearchTermId = data.insertId;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Bale);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_INSERT_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/update', secure.applySecurity([secure.checkMMUser()]), function(req, res){
	var tr = new cm();
	var Bale = req.body;

	console.log("TEST HERE")
	console.log(Bale)

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.SearchTerm))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		if (cu.isBlank(Bale.SearchTermTypeCode))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		if (cu.isBlank(Bale.SearchTermId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicterm.update(tr, Bale).then(function(data){
			Bale = data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Bale);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_UPDATE_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/delete', secure.applySecurity([secure.checkMMUser()]), function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.SearchTermId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicterm.delete(tr, Bale).then(function(data){
			Bale = data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Bale);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SEARCH_DELETE_FAIL"));
		return tr.rollback();
	})
	.done();
});

module.exports = router;