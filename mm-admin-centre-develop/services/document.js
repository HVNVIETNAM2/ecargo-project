"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var multer = require('multer');
var config = require('../config');
var fs=require('fs');
var path = require('path');
var cu = require('../lib/commonutil.js');
var ci = require('../lib/commonimage.js');
var secure = require('./secure.js');
var Q = require('q');

var multer = require('multer');
var multerUpload = function (req, res, next) {
    var upload = multer({dest: __dirname + '/../uploads/documents', limits: {fileSize: config.fileSize.document, files: 1}}).single('file');
    upload(req, res, function (err) {
        if (err) {
            // Handle file size too large error
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_FILE_SIZE"));
            return;
        }
        next();
    });
};


//router.use('/*',secure.checkAuth);

/**
  *upload document
 **/
router.post('/upload', [secure.checkAuth,multerUpload], function(req, res) {
  console.log("upload document");
  //1.upload document;
  if (!req.file) {
    return res.status(500).json(cu.createErrorResult(new Error("missing upload file"), "MSG_ERR_MERCHANT_DOCUMENT_UPLOAD"));
  }
  Q.when()
  .then(function(){
    return path.resolve();//save file to disk
  })
  .then(function(){
    //2.insert document into merchant document.
    console.log("upload document complete");
    res.json({
      Key: req.file.filename,
      Originalname: req.file.originalname,
      MimeType: req.file.mimetype,
    });
  })
  .catch(function(error){
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_MERCHANT_DOCUMENT_UPLOAD"));
  })
  .done();

});

/**
 *download document
 *@queryParam {DocumentKey: DocumentKey, DocumentOriginalName: DocumentOriginalName}
 **/
router.get('/download', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function(req, res) {
  console.log("download document");

  var file = null;
  var filePath = null;
  var DocumentKey = req.query.documentkey;
  var DocumentOriginalName = req.query.documentoriginalname;

  //todo: maybe we should check if the file belongs to the merchant
  Q.when()
  .then(function(){
    //check on new path
    filePath = __dirname + '/../uploads/documents/' + DocumentKey;
    filePath = path.resolve(filePath);
    if (fs.existsSync(filePath)){
      file = filePath;
    }
    return true;
  })
  .then(function(){
    if( !file ){
      return res.status(500).json(cu.createErrorResult(new Error("missing file"), "MSG_ERR_MERCHANT_DOCUMENT_NOT_FOUND"));
    }else{
      res.setHeader('Content-type', 'application/octet-stream');
      res.setHeader('Content-Disposition', 'attachment; filename="' + DocumentOriginalName + '"');
      res.sendFile(filePath);
    }
  })
  .catch(function(error){
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_MERCHANT_DOCUMENT_NOT_FOUND"));
  })
  .done();

});


// Download Excel Error Files..
router.get('/sheets', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function(req, res) {
  console.log("download sheets");
  var fileName = req.query.file;
  var folder = req.query.folder;
  var file = null;
  var filePath = null;

  //todo: maybe we should check if the file belongs to the merchant
  Q.when()
  .then(function(){
    //check on new path
    filePath = __dirname + '/../uploads/'+folder+'/' + fileName;
    filePath = path.resolve(filePath);
    if (fs.existsSync(filePath)){
      file = filePath;
    }
    return true;
  })
  .then(function(){
    if( !file ){
      return res.status(500).json(cu.createErrorResult(new Error("missing file"), "MSG_ERR_MERCHANT_DOCUMENT_NOT_FOUND"));
    }else{
      //res.setHeader('Content-type', 'application/octet-stream');
      console.log(fileName);
      res.setHeader('Content-Disposition', 'attachment; filename="' + fileName + '"');
      res.sendFile(filePath);
    }
  })
  .catch(function(error){
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_MERCHANT_DOCUMENT_NOT_FOUND"));
  })
  .done();
});

module.exports = router;