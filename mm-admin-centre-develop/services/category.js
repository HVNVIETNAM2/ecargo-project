"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var fs = require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var lcat = require('../logic/logiccategory.js');
var throat = require('throat');
var util = require('util');


router.get('/list', function (req, res) {
    var tr = new cm();
    var CategoryList = null;

    Q.when()
        .then(function () {
            if (cu.isBlank(req.query.cc))
                throw new Error("cc required to define CultureCode");
        })
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return lcat.listAdjacency(req.query.cc, req.query.showAll, tr).then(function (data) {
                CategoryList = lcat.convertTree(data);
                if (!cu.isBlank(req.query.categoryid))
                    CategoryList = lcat.findSubtree(CategoryList, req.query.categoryid);
            });
        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            res.status(200).json(CategoryList);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_REFERENCE_FAIL"));
            return tr.rollback();
        })
        .done();
});

router.get('/list/adjacency', function (req, res) {
    var tr = new cm();
    var CategoryList = null;

    Q.when()
        .then(function () {
            if (cu.isBlank(req.query.cc))
                throw new Error("cc required to define CultureCode");
        })
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return lcat.listAdjacency(req.query.cc, req.query.showAll, tr).then(function (data) {
               CategoryList = data;
            });
        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            res.status(200).json(CategoryList);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_REFERENCE_FAIL"));
            return tr.rollback();
        })
        .done();
});

router.get('/list/billing', function (req, res) {
    var tr = new cm();

    var params = {
        merchantId: req.query.merchantid,
        cultureCode: req.query.cc
    };
    var categoryMap = {
        0: lcat.createCategory(0, '---') // dummy root category
    };

    var finalCategoryList = [];

    Q.when()
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return tr.queryMany("SELECT c.CategoryId, c.CategoryCode, cc.CategoryName, c.DefaultCommissionRate, c.ParentCategoryId, mc.CommissionRate AS NewRate FROM Category c INNER JOIN CategoryCulture cc ON (c.CategoryId = cc.CategoryId) LEFT JOIN MerchantCategory mc ON (mc.MerchantId = :merchantId) AND (mc.CategoryId = c.CategoryId) WHERE cc.CultureCode = :cultureCode AND c.StatusId!=1 AND c.ParentCategoryId=0 Order by c.CategoryId", params).then(function (rows) {
                _.each(rows, function (row) {
                    // Skip the dummy root category
                    if (row.CategoryId !== 0) {
                        var category = lcat.createCategory(row.CategoryId, row.CategoryCode,
                            row.CategoryName, row.DefaultCommissionRate, row.NewRate);
                        categoryMap[row.CategoryId] = category;
                        // Add child to parent
                        var parentCategory = categoryMap[row.ParentCategoryId];
                        parentCategory.children.push(category);
                    }
                });

                finalCategoryList = categoryMap[0].generateCategoryList();
            })
        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            res.json(finalCategoryList);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_MERCHANT_BILLING_FAIL"));
            return tr.rollback();
        })
        .done();
});

/**
 * @desc get category object by categoryId
 * @param categoryId
 **/
router.get("/view", function (req, res) {
    var CategoryId = req.query.categoryId;
    var Category = {};
    var tr;
    Q.when()
        .then(function () {
            tr = new cm();
            tr.begin();
        })
        .then(function () {
            //1.get category by categoryId
            return lcat.getCategory(CategoryId, tr);
        })
        .then(function (data) {
            Category = data;
            if (cu.isBlank(Category))
                throw {
                    AppCode: 'MSG_ERROR_VIEW_CATEGORY'
                };
        })
        .then(function () {
            //2.get category brands
            return lcat.getCategoryBrandMerchants(CategoryId, tr);
        })
        .then(function (data) {
            Category.CategoryBrandMerchantList = data.map(function (item) {
                return {
                    Entity: item.Entity,
                    EntityId: item.EntityId
                }
            });
        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            res.json(Category);
        })
        .catch(function (error) {
            res.status(500).json(cu.createErrorResult(error, "MSG_ERROR_VIEW_CATEGORY"));
            return tr.rollback();
        })
        .done();

});

router.post('/reposition', secure.applySecurity([secure.checkMMUser()]), function (req, res) {
    var tr = new cm();

    Q.when()
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return lcat.updatePosition(
              req.body.CategoryId,
              req.body.ParentCategoryId,
              req.body.Priority,
              tr
            );
        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            res.json(true)
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_REFERENCE_FAIL"));
            return tr.rollback();
        })
        .done();
});


/**
 * @desc delete category by categoryId, which is soft delete
 * @param categoryId
 **/
//can no longder delete category
/**
router.post("/delete", secure.applySecurity([secure.checkMMUser()]), function (req, res) {
    var Category = req.body; //category object parameters.
    var CategoryId = req.body.CategoryId; //false-save; true-edit;
    var tr;
    Q.when()
        .then(function () {
            tr = new cm();
            tr.begin();
        })
        .then(function () {
            //1.check if there are product and subCategory under this category
            return lcat.checkIfCategoryUsed(tr, CategoryId, CONSTANTS.STATUS.DELETED)
                .then(function (isCategoryUsed) {
                    if (isCategoryUsed) {
                        throw {
                            AppCode: "MSG_AC_DELETE_CATEGORY_FAIL",
                            Msg: "Category is Under product or subCategory."
                        };
                    }
                });
        })
        .then(function () {
            return tr.queryOne('select * from Category where CategoryId=:CategoryId', {
                CategoryId: CategoryId
            }).then(function (Category) {
                if (!Category) throw {AppCode: "MSG_AC_DELETE_CATEGORY_FAIL"};
                //2. swap priority below
                return tr.queryExecute("UPDATE Category SET Priority = Priority - 1 WHERE ParentCategoryId=:ParentCategoryId AND Priority > :Priority", Category);
            })
        })
        .then(function () {
            //3.delete category
            var params={
                CategoryId: CategoryId
            };
            return tr.queryExecute("UPDATE `Category` SET `StatusId`= 1, `Priority` = 0 WHERE `CategoryId`=:CategoryId", params);
        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            res.json(true);
        })
        .catch(function (error) {
            res.status(500).json(cu.createErrorResult(error, "MSG_ERR_REFERENCE_FAIL"));
            return tr.rollback();
        })
        .done();

});
 **/

router.post('/status/activate', secure.applySecurity([secure.checkMMUser()]), function (req, res) {
    var tr = new cm();
    tr.begin()
        .then(function () {
            tr.queryExecute("update Category set StatusId = 2 where StatusId = 3");
        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            res.status(200).json(true);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_CATEGORY_ACTIVATE"));
            tr.rollback();
        })
});

/**
 * @desc change category status by categoryId
 * @param categoryId
 * @param StatusId
 **/
router.post("/status/change", secure.applySecurity([secure.checkMMUser()]), function (req, res) {
    var Category = req.body; //category object parameters.
    var CategoryId = req.body.CategoryId; //false-save; true-edit;
    var StatusId = req.body.StatusId; //false-save; true-edit;
    var tr;
    Q.when()
        .then(function () {
            tr = new cm();
            tr.begin();
        })
        .then(function () {
            if (StatusId === CONSTANTS.STATUS.DELETED) {
                throw {
                    AppCode: "MSG_ERR_STATUS_UNKNOWN"
                };
            }
        })
        .then(function () {
            //1.check if there are product and subCategory under this category
            return lcat.checkIfCategoryUsed(tr, CategoryId, StatusId)
                .then(function (isCategoryUsed) {
                    if (isCategoryUsed) {
                        throw new Error("Category is Under product or subCategory.");
                    }
                });
        })
        .then(function () {
            //2. change category status
            return tr.queryExecute("UPDATE `Category` SET `StatusId`= :StatusId WHERE `CategoryId`=:CategoryId", {
                CategoryId: CategoryId,
                StatusId: StatusId
            });
        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            res.json(true);
        })
        .catch(function (error) {
            res.status(500).json(cu.createErrorResult(error,"MSG_AC_CHANGE_STATUS_CATEGORY_FAIL"));
            return tr.rollback();
        })
        .done();

});

/**
 * @desc cerate or update category
 * @param Category
 **/
router.post("/save", secure.applySecurity([secure.checkMMUser()]), function (req, res) {
    var Category = req.body.Category; //category object parameters.
    var CategoryBrandMerchantList = Category.CategoryBrandMerchantList; //category brand merchant list parameters.
    var saveMode = (Category.CategoryId > 0) ? false : true; //false-save; true-edit;

    var tr;
    Q.when()
        .then(function () {
            tr = new cm();
            tr.begin();
        })
        .then(function () {
            //check data validity before save
            checkDataValidity(Category);
        })
        .then(function () {
            //1.check uniqueness of categoryCode
                if (saveMode) {
                    var params = {
                        CategoryCode: Category.CategoryCode
                    };
                    return tr.queryOne("select * from Category where CategoryCode=:CategoryCode and StatusId<>1", params)
                            .then(function (data) {
                                if (data != null)
                                    throw {
                                        AppCode: "MSG_ERR_CATEGORYCODE_UNIQUE"
                                    };
                            });
            } else {
                var params = {
                    CategoryId: Category.CategoryId,
                    CategoryCode: Category.CategoryCode
                };
                return tr.queryOne("select * from Category where CategoryCode=:CategoryCode and CategoryId<>:CategoryId and StatusId<>1", params)
                            .then(function (data) {
                                if (data != null)
                                    throw {
                                        AppCode: "MSG_ERR_CATEGORYCODE_UNIQUE"
                                    };
                            });
            }
        })
        .then(function () {
            //2. check if brands are all not pending
            var brands = _.remove(CategoryBrandMerchantList, function (entity) {
                return entity === 'Brand';
            });

            if (brands.length) {
                return Q.allSettled(brands.map(function (entity) {
                    return tr.queryOne('select BrandId from Brands where BrandId=:BrandId and StatusId<>:StatusId', {
                        BrandId: entity.EntityId,
                        StatusId: CONSTANTS.STATUS.PENDING
                    });
                })).then(function (results) {
                    results.forEach(function (result) {
                        if (result.state !== 'fulfilled' || (result.value && !cu.isBlank(result.value.BrandId))) {
                            throw {AppCode: "MSG_ERR_BRAND_PENDING"};
                        }
                    });
                });
            }
        })
        .then(function () {
            // set Category priority if create
            if (saveMode) {
                var params = {
                    ParentCategoryId: Category.ParentCategoryId
                };
                return tr.queryOne(
                    "select count(*) + 1 as Priority from Category where StatusId <> 1 and ParentCategoryId = :ParentCategoryId",
                    params
                ).then(function (data) {
                        Category.Priority = data.Priority;
                    });
            }
        })
        .then(function () {
            //2.save Category
            Category.CategoryNameInvariant = Category.CategoryNameEN;
            Category.CategoryDescInvariant = Category.CategoryDescEN;
            Category.CategoryMetaKeywordInvariant = Category.CategoryMetaKeywordEN;
            Category.CategoryMetaDescInvariant = Category.CategoryMetaDescEN;
            Category.SizeGridImageInvariant = Category.SizeGridImageEN;
            if (saveMode) {//create new category
                return tr.queryExecute("INSERT INTO `Category` (`ParentCategoryId`, `CategoryCode`, `CategoryNameInvariant`, `CategoryDescInvariant`, `CategoryMetaKeywordInvariant`, `CategoryMetaDescInvariant`, `IsSearchableCategory`, `IsMerchCanSelect`,`CategoryImage`, `SizeGridImageInvariant`, `DefaultCommissionRate`, `StartPublishTime`, `EndPublishTime`, `Priority`, `StatusId`) VALUES (:ParentCategoryId, :CategoryCode, :CategoryNameInvariant, :CategoryDescInvariant, :CategoryMetaKeywordInvariant, :CategoryMetaDescInvariant, :IsSearchableCategory, :IsMerchCanSelect, :CategoryImage, :SizeGridImageInvariant, :DefaultCommissionRate, :StartPublishTime, :EndPublishTime, :Priority, :StatusId)", Category).then(function (data) {
                    Category.CategoryId = data.insertId;
                    return;
                });
            } else {//update category
                return tr.queryExecute("Update Category set `CategoryCode`=:CategoryCode ,`CategoryNameInvariant`=:CategoryNameInvariant ,`CategoryDescInvariant`=:CategoryDescInvariant ,`CategoryMetaKeywordInvariant`=:CategoryMetaKeywordInvariant ,`CategoryMetaDescInvariant`=:CategoryMetaDescInvariant ,`IsSearchableCategory`=:IsSearchableCategory,`IsMerchCanSelect`=:IsMerchCanSelect ,`CategoryImage`=:CategoryImage ,`SizeGridImageInvariant`=:SizeGridImageInvariant ,`DefaultCommissionRate`=:DefaultCommissionRate, `StartPublishTime`=:StartPublishTime, `EndPublishTime`=:EndPublishTime Where `CategoryId` = :CategoryId", Category).then(function (data) {
                    return;
                })
            }
        })
        .then(function () {
            //2.1save CategoryCulture
            var promises = [];
            var sql = "INSERT INTO CategoryCulture(`CategoryId`, `CultureCode`, `CategoryName`, `CategoryDesc`, `CategoryMetaKeyword`, `CategoryMetaDesc`, `SizeGridImage`) VALUES(:CategoryId, :CultureCode, :CategoryName, :CategoryDesc, :CategoryMetaKeyword, :CategoryMetaDesc, :SizeGridImage)";
            if (saveMode) {
                console.log("create new category culture");
            } else {
                console.log("edit new category culture");
                var deletePromise = tr.queryExecute("delete from CategoryCulture where CategoryId=:CategoryId", {
                    CategoryId: Category.CategoryId
                });
                promises.push(deletePromise);
            }
            // set up array to insert
            var CategoryCultureArray = [];
            CategoryCultureArray.push({
                CategoryId: Category.CategoryId,
                CultureCode: 'EN',
                CategoryName: Category.CategoryNameEN,
                CategoryDesc: Category.CategoryDescEN,
                CategoryMetaKeyword: Category.CategoryMetaKeywordEN,
                CategoryMetaDesc: Category.CategoryMetaDescEN,
                SizeGridImage: Category.SizeGridImageEN
            });
            CategoryCultureArray.push({
                CategoryId: Category.CategoryId,
                CultureCode: 'CHS',
                CategoryName: Category.CategoryNameCHS,
                CategoryDesc: Category.CategoryDescCHS,
                CategoryMetaKeyword: Category.CategoryMetaKeywordCHS,
                CategoryMetaDesc: Category.CategoryMetaDescCHS,
                SizeGridImage: Category.SizeGridImageCHS
            });
            CategoryCultureArray.push({
                CategoryId: Category.CategoryId,
                CultureCode: 'CHT',
                CategoryName: Category.CategoryNameCHT,
                CategoryDesc: Category.CategoryDescCHT,
                CategoryMetaKeyword: Category.CategoryMetaKeywordCHT,
                CategoryMetaDesc: Category.CategoryMetaDescCHT,
                SizeGridImage: Category.SizeGridImageCHT
            });
            var insertPromise = _.map(CategoryCultureArray, function (param) {
                tr.queryExecute(sql, param)
            });
            promises = promises.concat(insertPromise);
            return Q.all(promises);

        })
        .then(function () {
            //3.1.delete Brands of category.
            return tr.queryExecute("delete from CategoryBrandMerchant where CategoryId=:CategoryId", {
                CategoryId: Category.CategoryId
            })
        })
        .then(function () {
            //3.2.insert the category Brands
            var sql = "INSERT INTO CategoryBrandMerchant(`CategoryId`, `Entity`, `EntityId`, `Priority`) VALUES(:CategoryId, :Entity, :EntityId, :Priority)";
            if (!CategoryBrandMerchantList) return;
            var promises = CategoryBrandMerchantList.map(function (entity, index) {
                return tr.queryExecute(sql, {
                    CategoryId: Category.CategoryId,
                    EntityId: entity.EntityId,
                    Entity: entity.Entity,
                    Priority: index + 1
                })
            });
            return Q.all(promises);

        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            console.log("save category complete");
            res.json({Category: Category});
        })
        .catch(function (error) {
            if (saveMode) {
                res.status(500).json(cu.createErrorResult(error, "MSG_AC_CREATE_CATEGORY_FAIL"));
            } else {
                res.status(500).json(cu.createErrorResult(error, "MSG_AC_UPDATE_CATEGORY_FAIL"));
            }
            return tr.rollback();
        })
        .done();

    function checkDataValidity(category) {
        if (!category.CategoryCode || !/^[A-Za-z0-9-]{1,25}$/.test(category.CategoryCode)) {
          throw { AppCode: "MSG_ERR_CATEGORY_CODE_FORMAT" };
        }
        //1.check data range
        if (
          category.StartPublishTime && category.EndPublishTime &&
          new Date(category.EndPublishTime) < new Date(category.StartPublishTime)
        ) {
            throw { AppCode: "MSG_ERR_PUBLISH_DATE" };
        }
        return true;
    }
});

module.exports = router;
