"use strict";
var express = require('express'), router = express.Router();
var fs=require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');

//this may need to be auth secured for securoty reason but leaving public for now....

router.get('/country', function (req, res) {
	var tr= new cm();
	var CountryList=null;
	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.cc))
			throw new Error("cc required to define CultureCode");
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryMany("select * from GeoCountry gc, GeoCountryCulture gcc where gc.GeoCountryId=gcc.GeoCountryId and gcc.CultureCode=:CultureCode order by gcc.GeoCountryName",{CultureCode:req.query.cc}).then(function(data) {
			CountryList=data;
		})
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(CountryList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_REFERENCE_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.get('/country/storefront', function (req, res) {
	var tr= new cm();
	var CountryList=null;

	var IsStoreFrontCountry = req.query.IsStoreFrontCountry;

	if(!IsStoreFrontCountry){ IsStoreFrontCountry = 1; }

	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.cc))
			throw new Error("cc required to define CultureCode");
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryMany("select * from GeoCountry gc, GeoCountryCulture gcc where gc.GeoCountryId=gcc.GeoCountryId and gcc.CultureCode=:CultureCode and gc.IsStoreFrontCountry = :IsStoreFrontCountry order by gcc.GeoCountryName",
			{ CultureCode : req.query.cc, IsStoreFrontCountry : IsStoreFrontCountry }).then(function(data){
			CountryList=data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(CountryList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_REFERENCE_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.get('/province', function (req, res) {
	var tr= new cm();
	var Country=null;
	var CultureCode=null;
	var ProvinceList=null;
	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.cc))
			throw new Error("cc required to define CultureCode");
		if (cu.isBlank(req.query.q))
			throw new Error("q required to define GeoCountryId");

		CultureCode=req.query.cc;
		if (CultureCode!='EN' && CultureCode!='en')
			CultureCode='ZH'
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryOne("select * from GeoCountry where GeoCountryId=:GeoCountryId",{GeoCountryId:req.query.q}).then(function(data) {
			if (data==null)
				throw new Error("Not Found");
			Country=data;
		})
	})
	.then(function() {
		return tr.queryMany("select g.GeoId,g.GeoNameInvariant,ifnull(gc.GeoName,g.GeoNameInvariant) as GeoName from Geo g left outer join GeoCulture gc on g.GeoId=gc.GeoId and gc.CultureCode=:CultureCode  where CountryCode=:CountryCode and FeatureCode='ADM1' order by gc.GeoName",{CountryCode:Country.CountryCode,CultureCode:CultureCode}).then(function(data) {
			if (data==null || data.length==0)
				throw new Error("Not Found");
			ProvinceList=data;
		})
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(ProvinceList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_REFERENCE_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.get('/city', function (req, res) {
	var tr= new cm();
	var GeoProvince=null;
	var CultureCode=null;
	var CityList=null;
	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.cc))
			throw new Error("cc required to define CultureCode");
		if (cu.isBlank(req.query.q))
			throw new Error("q required to define GeoId");

		CultureCode=req.query.cc;
		if (CultureCode!='EN' && CultureCode!='en')
			CultureCode='ZH'
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryOne("select * from Geo where GeoId=:GeoId",{GeoId:req.query.q}).then(function(data) {
			if (data==null)
				throw new Error("Parent Province Not Found");
			GeoProvince=data;
		})
	})
	.then(function() {
		var FeatureCodeSelected='PPLX';
		if (GeoProvince.CountryCode=='CN' || GeoProvince.CountryCode=='HK')
			FeatureCodeSelected='ADM2';
		var citySql="select g.GeoId,g.GeoNameInvariant,ifnull(gc.GeoName,g.GeoNameInvariant) as GeoName from Geo g left outer join GeoCulture gc on g.GeoId=gc.GeoId and gc.CultureCode=:CultureCode where g.CountryCode=:CountryCode and g.AdminCode=:AdminCode and g.FeatureCode = :FeatureCodeSelected order by ifnull(gc.GeoName,g.GeoNameInvariant)";
		var cityParam={CountryCode:GeoProvince.CountryCode,AdminCode:GeoProvince.AdminCode,FeatureCodeSelected:FeatureCodeSelected,CultureCode:CultureCode};

		return tr.queryMany(citySql,cityParam).then(function(data) {
			CityList=data;
		});

	})
	.then(function() {
		if (CityList.length==0)
		{
			//if no cities were found then this just return the province again. This happens for Beijing where both the province and city are Beijing
			return tr.queryMany("select g.GeoId,g.GeoNameInvariant,ifnull(gc.GeoName,g.GeoNameInvariant) as GeoName from Geo g left outer join GeoCulture gc on g.GeoId=gc.GeoId and gc.CultureCode=:CultureCode where g.GeoId=:GeoId order by ifnull(gc.GeoName,g.GeoNameInvariant)",{GeoId:GeoProvince.GeoId,CultureCode:CultureCode}).then(function(data) {
				CityList=data;
			});
		}
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(CityList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_REFERENCE_FAIL"));
		return tr.rollback();
	})
	.done();
});



module.exports = router;
