"use strict";
var express = require('express'), router = express.Router();
var fs=require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var lcat = require('../logic/logiccategory.js');
var throat=require('throat');
var util = require('util');
var lp = require('../logic/logicproduct.js');
//this may need to be auth secured for securoty reason but leaving public for now....

router.get('/list', function(req, res) {
	var tr = new cm();
	var List = null;
	var Bale = { CultureCode : req.query.cc };
	
	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.CultureCode))
			throw new Error("cc required to define CultureCode");
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return lp.badgeList(Bale.CultureCode, tr).then(function(data) {
			List = data;
		});
	})
	.then(function(){
		return tr.commit();
	})
	.then(function(){
		res.json(List);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_Badge_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

module.exports = router;
