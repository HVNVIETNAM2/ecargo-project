"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var fs = require('fs');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var lu = require('../logic/logicuser.js');
var path = require('path');
var multer = require('multer');
var logicaddress = require('../logic/logicaddress.js');

router.use('/*', secure.applySecurity([
	secure.checkMMAdmin(),
	secure.checkSelf()
]));

router.post('/save', function (req, res) {
  var User;
  var UserAddress;
  var Bale = req.body;
  var tr = new cm();

  Q.when()
  .then(function () {
    return tr.begin();
  })
  .then(function () {
		return lu.getUser(tr, Bale.UserKey).then(function(data) {
			User = data;
      Bale.UserId = User.UserId;
      Bale.CultureCode = Bale.CultureCode || User.CultureCode || 'CHS';
		});
  })
  .then(function () {
    return logicaddress.save(tr, Bale).then(function (data) {
      UserAddress = data;
    })
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json(UserAddress);
  })
  .catch(function (error) {
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_USER_ADDRESS_SAVE"));
    return tr.rollback();
  });
});

router.post('/delete', function (req, res) {
  var User;
  var Bale = req.body;
  var tr = new cm();

  Q.when().then(function () {
    if (cu.checkIdBlank(Bale.UserAddressKey))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserAddressKey" };
    return tr.begin();
  })
  .then(function () {
		return lu.getUser(tr, cu.reqUser(req)).then(function(data) {
			User = data;
		});
  })
  .then(function () {
    return logicaddress.delete(tr, User.UserId, Bale.UserAddressKey);
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json(true);
  })
  .catch(function (error) {
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_USER_ADDRESS_DELETE"));
    return tr.rollback();
  });
});

router.post('/default/save', function (req, res) {
  var User;
  var Bale = req.body;
  var tr = new cm();

  Q.when().then(function () {
    if (cu.checkIdBlank(Bale.UserAddressKey))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserAddressKey" };
    return tr.begin();
  })
  .then(function () {
    return lu.getUser(tr, cu.reqUser(req)).then(function(data) {
      User = data;
    });
  })
  .then(function () {
    return logicaddress.saveDefault(tr, User.UserId, Bale.UserAddressKey);
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json(true);
  })
  .catch(function (error) {
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_USER_ADDRESS_DELETE"));
    return tr.rollback();
  });
});

router.get('/default/view', function (req, res) {
  var Bale = req.query;
  var User;
  var UserAddress;
  var tr = new cm();

  tr.begin()
  .then(function () {
		return lu.getUser(tr, cu.reqUser(req)).then(function(data) {
			User = data;
		});
  })
  .then(function () {
    return logicaddress.viewDefault(tr, User.UserId).then(function (data) {
      UserAddress = data;
    });
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json(UserAddress);
  })
  .catch(function (error) {
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_USER_ADDRESS_VIEW"));
    return tr.rollback();
  });
});

router.get('/view', function (req, res) {
  var Bale = req.query;
  var User;
  var UserAddress;
  var tr = new cm();

  Q.when().then(function () {
    if (cu.checkIdBlank(Bale.UserAddressKey))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserAddressKey" };
    return tr.begin();
  })
  .then(function () {
		return lu.getUser(tr, cu.reqUser(req)).then(function(data) {
			User = data;
		});
  })
  .then(function () {
    return logicaddress.view(tr, User.UserId, Bale.UserAddressKey).then(function (data) {
      UserAddress = data;
    });
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json(UserAddress);
  })
  .catch(function (error) {
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_USER_ADDRESS_VIEW"));
    return tr.rollback();
  });
});

router.get('/list', function (req, res) {
  var User;
  var List;
  var tr = new cm();

  tr.begin()
  .then(function () {
		return lu.getUser(tr, cu.reqUser(req)).then(function(data) {
			User = data;
		});
  })
  .then(function () {
    return logicaddress.list(tr, User.UserId).then(function (data) {
      // hide ids
      data.forEach(function (data) {
        delete data.UserId;
        delete data.UserAddressId;
      });
      List = data;
    });
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json(List);
  })
  .catch(function (error) {
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_USER_ADDRESS_LIST"));
    return tr.rollback();
  });
});

module.exports = router;
