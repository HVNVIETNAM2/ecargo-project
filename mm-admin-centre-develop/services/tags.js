"use strict";
var express = require('express'), router = express.Router();
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');

var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var logictags = require('../logic/logictags.js');
var logicuser = require('../logic/logicuser.js');

function TagIdList (list) {
  if (!list) return [];
  if (!Array.isArray(list)) {
    list = String(list).split(',');
  }
  return list.map(function (item) {
    if (typeof item !== 'object') {
      return {
        TagId: item,
        Priority: 0
      };
    } else {
      return {
        TagId: item.TagId,
        Priority: item.Priority || 0
      }
    }
  });
}

// router.use('/*', secure.checkAuth);

// Tag Show all available list.
// Required: tagtypeid
// Required: cc = EN/CHT/CHS
// Optional: limit || 100
// Optional: start || 0
// Returns list of available tags per tag type []
router.get('/list', function(req, res){
	var tr = new cm();
	var TagsList = null;

	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.tagtypeid))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "tagtypeid" };
		if (cu.isBlank(req.query.cc))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "cc" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logictags.list(tr, req.query).then(function(data){
			TagsList = data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(TagsList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_TAGS_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Tag Save ( Insert if no available TagId, else do an Update )
// Required: TagTypeId => Foreign key to TagType
// Required: TagNameEN => String
// Required: TagNameCHT => String
// Required: TagNameCHS => String
router.post('/save', [ secure.checkAuth, secure.checkMmMerchantAdmin ], function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.TagTypeId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TagTypeId" };
		if (cu.isBlank(Bale.TagNameEN))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TagNameEN" };
		if (cu.isBlank(Bale.TagNameCHT))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TagNameCHT" };
		if (cu.isBlank(Bale.TagNameCHS))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TagNameCHS" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		if(Bale.TagId){
			return logictags.updateTag(tr, Bale);
		}else{
			return logictags.insertTag(tr, Bale).then(function(data){
				Bale = data;
			});
		}
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Bale);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_TAG_SAVE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Delete unwanted tag
// Required: TagId
router.post('/delete', [ secure.checkAuth, secure.checkMmMerchantAdmin ], function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.TagId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TagId" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logictags.deleteTag(tr, Bale).then(function(data){
			Bale = data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Bale);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_TAG_DELETE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Tag Type Save ( Insert if no available TagTypeId, else do an Update )
// Required: TagTypeName => String
router.post('/type/save', [ secure.checkAuth, secure.checkMmMerchantAdmin ], function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.TagTypeName))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TagTypeName" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		if(Bale.TagTypeId){
			return logictags.updateTagType(tr, Bale);
		}else{
			return logictags.insertTagType(tr, Bale).then(function(data){
				Bale.TagTypeId = data.insertId;
			});
		}
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Bale);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_TAG_TYPE_SAVE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Delete unwanted tag type
// Required: TagTypeId
router.post('/type/delete', [ secure.checkAuth, secure.checkMmMerchantAdmin ], function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.TagTypeId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TagTypeId" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logictags.deleteTagType(tr, Bale).then(function(data){
			Bale = data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Bale);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_TAG_TYPE_DELETE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// API: Save Merchant Tag List
// Required: MerchantId
// Required: TagIds = Object List [ { TagId, Priority } ]
router.post('/merchant/save', [ secure.checkAuth, secure.checkMmMerchantAdmin ], function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.MerchantId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "MerchantId" };
		if (cu.isBlank(Bale.TagIds))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TagIds" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
    return logictags.deleteTagMerchant(tr, Bale);
	})
	.then(function(){
		var promises = TagIdList(Bale.TagIds).map(throat(1, function(item){
			item.MerchantId = Bale.MerchantId;
			return logictags.insertTagMerchant(tr, item);
		}));

		return Q.all(promises);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_TAG_MERCHANT_SAVE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// API: Save User Tag List
// Required: UserKey/UserId
// Required: TagIds = Object List [ { TagId, Priority } ]
router.post('/user/save', [ secure.checkAuth, secure.checkMmAdminOrSelf ], function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.TagIds))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TagIds" };
	})
	.then(function(){
		return tr.begin();
	})
  .then(function () {
    return logicuser.getUser(tr,cu.reqUser(req)).then(function (User) {
      Bale.UserId = User.UserId;
    });
  })
	.then(function(){
    return logictags.deleteTagUser(tr, Bale);
	})
	.then(function(){
		var promises = TagIdList(Bale.TagIds).map(throat(1, function(item){
			item.UserId = Bale.UserId;
			return logictags.insertTagUser(tr, item);
		}));

		return Q.all(promises);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_TAG_USER_SAVE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// This function will get all merchants listed per user tags.
// Required: userkey/userid
// Required: cc = EN/CHT/CHS
// Returns: List of merchants []
router.get('/user/merchant', [ secure.checkAuth, secure.checkMmAdminOrSelf ], function(req, res){
	var tr = new cm();
	var MerchantList = null;
  var User;

	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.cc))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "cc" };
	})
	.then(function(){
		return tr.begin();
	})
  .then(function () {
    return logicuser.getUser(tr,cu.reqUser(req)).then(function (data) {
      User = data;
    });
  })
	.then(function(){
		return logictags.UserMerchants(tr, User.UserId, req.query.cc).then(function(data){
			MerchantList = data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(MerchantList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_MERCHANT_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

// This function will get all user curators.
// Required: userkey/userid
// Returns: List of user curators []
router.get('/user/curator', [ secure.checkAuth, secure.checkMmAdminOrSelf ], function(req, res){
	var tr = new cm();
	var UserList = null;
  var User;

	Q.when()
	.then(function(){
		return tr.begin();
	})
  .then(function () {
    return logicuser.getUser(tr,cu.reqUser(req)).then(function (data) {
      User = data;
    });
  })
	.then(function(){
		return logictags.UserCurators(tr, User.UserId, req.query.cc).then(function(data){
			UserList = data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(UserList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_CURATOR_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

module.exports = router;