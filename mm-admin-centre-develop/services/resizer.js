"use strict";
var express = require('express'), router = express.Router();
var multer = require('multer');
var fs=require('fs');
var path = require('path');
var cu = require('../lib/commonutil.js');
var ci = require('../lib/commonimage.js');
var secure = require('./secure.js');
var Q = require('q');

router.get('/view', function (req, res) {
    var MAX_SIZE = 1024; //define maximal width&height for image resizer.
	var key=req.query.key;
	
	var size=req.query.s;
	var bucket=req.query.b;
	var width=req.query.w;
	var height=req.query.h;
	
	if (!height)
		height=0;
		
	if (size)
	{
		width=size;
		height=size;
	}
	
	
   	var image = null;
	Q.when()
        .then(function () {
            //check if the width height and size is exceed the maximal size, if so throw error.
            if (width > MAX_SIZE || height > MAX_SIZE || size > MAX_SIZE) {
                throw {AppCode: 'MSG_ERR_IMAGE_INVALID_SIZE'}
            }
        })
        .then(function(){
            width=parseInt(width);
            height=parseInt(height);
            if( /[^a-zA-Z0-9]/.test( key ) ){
                throw {AppCode: 'MSG_ERR_IMAGE_INVALID_KEY'}
            }
            if( /[^a-zA-Z0-9]/.test( bucket ) || bucket.indexOf('images')==-1){
                throw {AppCode: 'MSG_ERR_BUCKET_NAME_INVALID'}
            }
            

        })
        .then(function(){
            //check old path
            var filePath=__dirname + '/../uploads/' + bucket + '/' + key
            filePath=path.resolve(filePath);
            return ci.resizeImageSmart(filePath,width,height).then(function(dataFile){
                image=dataFile;
            })
        })
        .then(function(){
            if( !image ){
                return res.status(500).json({ AppCode : "MSD_ERR_IMAGE_NOT_FOUND" });
            }else{
                res.set('Content-Type', 'image/jpeg');
                res.sendFile(image);
            }
        })
        .catch(function(err){
            return res.status(500).json(cu.createErrorResult(err, "MSG_ERR_RESIZER_VIEW"));
        })
        .done();

});


module.exports = router;
