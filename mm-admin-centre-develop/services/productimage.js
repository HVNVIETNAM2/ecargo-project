"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express');
var Q = require('q');
var path = require('path');
var fs = require('fs');
var _ = require('lodash');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');
var ci = require('../lib/commonimage.js');
var router = express.Router();
// var cu = require('../lib/commonutil.js')
var secure = require('./secure.js');
var multer = require('multer');
var config = require('../config');
var crypto = require('crypto');
var throat = require('throat');

var ProductImage = require('../logic/logicproductimage.js')
var Zip = require('../lib/commonzip.js')

var IMG_REGEX = /\.(jpg|jpeg|png)$/i;
var UPLOAD_DIR = path.resolve(__dirname, '../uploads/productimages');

/**
 * not used anywhere
 */
router.post('/reposition', function (req, res) {
    var tr = new cm();
    var imagePositionList = req.body.ImagePositionList;
    //map to get the style image id list
    var styleImageIdList = _.map(imagePositionList, function (n) {
        return n.StyleImageId;
    });

    Q.when()
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            //1. retrive the styleimage rows which exsited in ImagePositionList;
            return ProductImage.getStyleImagelist(styleImageIdList, tr);
        })
        .then(function (styleImageList) {
            //2. check and upate image position to db
            return ProductImage.updatePosition(imagePositionList, styleImageList, tr);
        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            console.log('success reposition');
            res.json(true)
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_IMAGE_REPOSITION"));
            return tr.rollback();
        })
        .done();
});

router.post('/upload', [multer({
    dest: UPLOAD_DIR,
    limits: {
        fileSize: config.fileSize.image,
        files: 1
    }
}).single('file'), function (req, res, next) {
    var data = JSON.parse(req.body.data);
    req.body.data = data;
    req.body.MerchantId = data.MerchantId;
    next();
}, secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()])], function (req, res) {
    if (!req.file) {
        return res.status(500).json(cu.createErrorResult(new Error("Missing upload file."), "MSG_ERR_IMAGE_UPLOAD"));
    }

    var tr = new cm();
    var result;
    var data = req.body.data;
    var isIgnore = data.existsAction === 'ignore';

    //added octet-stream checking for IE9 fallback
    if (req.file.mimetype.indexOf('image/') !== 0 && req.file.mimetype !== 'application/octet-stream') {
        return res.status(500).json(cu.createErrorResult(new Error("Invalid file type."), "MSG_ERR_IMAGE_UPLOAD"));
    }
    // image handling
    var filename = req.file.originalname;
    var ImageKey = req.file.filename;
    var MerchantId = req.AccessUser.MerchantId || data.MerchantId;

    Q.when()
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return ProductImage.add(MerchantId, filename, ImageKey, isIgnore, tr);
        })
        .then(function (r) {
            result = r;
            return tr.commit();
        })
        .then(function () {
            var filePath = path.resolve(UPLOAD_DIR, result.StyleImage.ProductImage);
            return ci.resizeImage(filePath, 200).then(function () {
                res.json(result);
            })
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_IMAGE_UPLOAD"));
            return tr.rollback();
        });
});

router.post('/upload/archive', [multer({
    dest: UPLOAD_DIR,
    limits: {
        fileSize: config.fileSize.zip,
        files: 1
    }
}).single('file'), function (req, res, next) {
    var data = JSON.parse(req.body.data);
    req.body.data = data;
    req.body.MerchantId = data.MerchantId;
    next();
}, secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()])], function (req, res) {
    if (!req.file) {
        return res.status(500).json(cu.createErrorResult(new Error("Missing upload file."), "MSG_ERR_IMAGE_UPLOAD"));
    }
    var tr = new cm();
    var result;
    var data = req.body.data;
    var isIgnore = data.existsAction === 'ignore';
    var MerchantId = req.AccessUser.MerchantId || data.MerchantId;

    if (req.file.mimetype !== 'application/zip') {
        return res.status(500).json(cu.createErrorResult(new Error("Invalid file type"), "MSG_ERR_IMAGE_UPLOAD_ARCHIVE"));
    }

    function handleImage(entry) {
        // handle image from yauzl entry + destination
        var filename = path.basename(entry.fileName);
        var ImageKey = entry.destFileName;

        return ProductImage.add(
            MerchantId, filename, ImageKey, isIgnore, tr
        )
            .then(function (result) {
                var filePath = path.resolve(UPLOAD_DIR, result.StyleImage.ProductImage);
                return ci.resizeImage(filePath, 200).then(function () {
                    return {name: filename, result: result};
                })
            })
            .catch(function (err) {
                return {
                    name: filename,
                    error: {
                        AppCode: err.AppCode || 'MSG_ERR_IMAGE_UPLOAD',
                        Message: err.message
                    }
                };
            });
    }

    // extract zip then handle images
    Zip.unzip(req.file.path, function (entry) {
        // ignore .hidden files
        if (path.basename(entry.fileName).indexOf('.') === 0) return;
        // ignore all non-image files
        if (!IMG_REGEX.test(entry.fileName)) return;
        // multer style destination
        entry.destFileName = crypto.pseudoRandomBytes(16).toString('hex');
        return path.join(UPLOAD_DIR, entry.destFileName);
    })
        .then(function (entries) {
            return Q.when()
                .then(function () {
                    return tr.begin();
                }).then(function () {
                    return Q.all(
                        entries.map(throat(1, handleImage))
                    );
                }).then(function (r) {
                    result = r;
                    return tr.commit();
                })
        })
        .then(function () {
            res.json(result);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_IMAGE_UPLOAD_ARCHIVE"));
            return tr.rollback();
        });
});

router.post('/delete', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
    var tr = new cm();
    var result;

    Q.when()
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return ProductImage.remove(req.body.StyleImageId, req.body.MerchantId, tr);
        })
        .then(function (r) {
            result = r;
            return tr.commit();
        })
        .then(function () {
            res.json(result);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_IMAGE_UPLOAD_STYLEIMG"));
            return tr.rollback();
        });
});

// Comment out unused api method
///**
// * this route is not used anywhere
// */
//router.post('/upload/add', multer({
//    dest: UPLOAD_DIR,
//    limits: {
//        fileSize: 100 * CONSTANTS.MEGABYTE, // 100MB limit for zip
//        files: 1
//    }
//}).single('file'), function (req, res) {
//    if (!req.file) {
//        return res.status(500).json(cu.createErrorResult(new Error("Missing upload file."), "MSG_ERR_ADD_IMG_FAIL"));
//    }
//    var tr = new cm();
//    var result;
//    var data = JSON.parse(req.body.data);
//    var ImageKey = req.file.filename;
//    var StyleCode = data.StyleCode;
//    var ImageTypeCode = data.ImageTypeCode;
//    var ColorKey = data.ColorKey;
//    var Position = data.Position;
//    var MerchantId = req.AccessUser.MerchantId || data.MerchantId;
//    Q.when()
//        .then(function () {
//            return tr.begin();
//        })
//        .then(function () {
//            return ProductImage.addIndividually(MerchantId, ImageKey, StyleCode, ImageTypeCode, ColorKey, Position, tr);
//        })
//        .then(function (r) {
//            result = r;
//            return tr.commit();
//        })
//        .then(function () {
//            res.json(result);
//        })
//        .catch(function (err) {
//            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_ADD_IMG_FAIL"));
//            return tr.rollback();
//        }).done();
//
//});

router.get('/report/missing-img', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
    var tr = new cm();
    var result;

    Q.when()
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return ProductImage.getMissingImgReportParams(req);
        }).then(function (r) {
            return ProductImage.getMissingImgReport(r.params, r.applyFeatureFilter, r.applyDescFilter, r.applyColorFilter, r.statusFilter, tr)
        }).then(function (r) {
            result = r;
            return tr.commit();
        })
        .then(function () {
            res.json(result);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_PRODUCT_MISSING_IMAGES_ERR"));
            return tr.rollback();
        }).done();
});

router.get('/list', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
    var tr = new cm();
    var result;

    Q.when()
        .then(function() {
            if (cu.isBlank(req.query.stylecode) || cu.isBlank(req.query.merchantid))
            throw {
                AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'
            };
        })
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return ProductImage.imageList(req.query.stylecode, req.query.merchantid, tr).then(function(data) {
                result=data;
            });
        })
        .then(function (r) {
            return tr.commit();
        })
        .then(function () {
            res.json(result);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_IMAGE_LIST"));
            return tr.rollback();
        });
});


//handle file too large error or other event related error
router.use('/upload*', function (err, req, res, next) {
    res.status(500).json(cu.createErrorResult(err, "MSG_ERR_IMAGE_UPLOAD"));
});


module.exports = router;
