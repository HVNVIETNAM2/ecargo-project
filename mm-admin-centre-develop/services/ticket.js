"use strict";
var express = require('express'), router = express.Router();
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');

var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var logicticket = require('../logic/logicticket.js');
var logicuser = require('../logic/logicuser.js');

router.post('/create', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf()
]), function (req, res) {
	Q.spawn(function*(){
		var tr = new cm();

		try{
			var Bale = req.body || {};

			if(cu.isBlank(Bale.UserKey))
				throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
			if(cu.isBlank(Bale.TicketTypeId))
				throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TicketTypeId" };
			if(cu.isBlank(Bale.TicketSummary))
				throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "TicketSummary" };

			yield tr.begin();

			var UserInfo = yield logicuser.getUser(tr, Bale.UserKey);
			Bale.UserId = UserInfo.UserId;
			Bale.CultureCode = UserInfo.CultureCode;

			var ret = yield logicticket.create(tr, Bale);
			yield tr.commit();
			res.send(true);			
		}catch(err){			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err, "MSG_ERR_TICKET_CREATE"));			
		}
	});	
});

module.exports = router;