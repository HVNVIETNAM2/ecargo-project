var config=require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');

module.exports = function (request, response, next) {
	var tr = new cm();
	response.trScope = function(promise) {
		response.tr=tr;
		response.tr.begin().then(function() {
			promise.then(function(result) {
				response.send(result);
				return response.tr.commit();
			}).catch(function(err) {
				res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SERVICE_FAIL"));
				return response.tr.rollback();
			});
		}).done();
	};	
	next();
}