Note: Always post with Content-Type='application/json' for any JSON services

******Please also see the /test folder for examples of how to call other services ********


***Wish List Create****
http://192.168.33.33:3000/api/wishlist/create
POST
{"CultureCode":"CHS","UserId":349} //for authenticated
OR
{"CultureCode":"CHS","UserId":0} //for unauthenticated

-> Returns Wish List Object (inc CartId) Containing Cart and CartItems (view via browser post tool)

***Wish List User Update****
http://192.168.33.33:3000/api/wishlist/user/update
POST
{"CultureCode":"CHS","CartId":2,"UserId":349} 

-> Returns Wish List Object (inc CartId) Containing Cart and CartItems (view via browser post tool)

***Wish List Item Add****
http://192.168.33.33:3000/api/wishlist/item/add
POST
{"CultureCode":"CHS","CartId":2,"StyleCode":"SC0001"} 
{"CultureCode":"CHS","CartId":2,"StyleCode":"SC0001","SkuId":"1237","ColorKey":"NAVY"} //with optional field(s) SkuId and ColorKey

-> Returns Wish List Object (inc CartId) Containing Cart and CartItems (view via browser post tool)

***Wish List Item Remove****
http://192.168.33.33:3000/api/wishlist/item/remove
POST
{"CultureCode":"CHS","CartId":2,"CartItemId":1} 

-> Returns Wish List Object (inc CartId) Containing Cart and CartItems (view via browser post tool)

***Wish List View By UserId****
http://192.168.33.33:3000/api/wishlist/view/user?cc=EN&userid=349
GET 

-> Returns Wish List Object (inc CartId) Containing Cart and CartItems (view via browser post tool)

***Wish List View By CartId****
http://192.168.33.33:3000/api/wishlist/view?cc=EN&cartid=2
GET

-> Returns Wish List Object (inc CartId) Containing Cart and CartItems (view via browser post tool)