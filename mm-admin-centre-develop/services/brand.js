"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var fs = require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var throat = require('throat');
var util = require('util');
var lbrand = require('../logic/logicbrand');
var lp=require('../logic/logicproduct.js');

/**
 * @desc create or update one brand
 **/
router.post('/save', secure.applySecurity([secure.checkMMUser()]), function (req, res) {
    var tr = new cm();
    var Brand = {};
    var BrandImageList = req.body.BrandImageList;
    var IsTouchedBrandImages = req.body.IsTouchedBrandImages;

    Q.when().then(function () {
        Brand = lbrand.getBrand(req.body.Brand);
        lbrand.validate(Brand);
    }).then(function () {
        return tr.begin();
    }).then(function () {
        return lbrand.save(Brand, BrandImageList, IsTouchedBrandImages, tr);
    }).then(function () {
        return tr.commit();
    }).then(function () {
        res.json(Brand);
    }).catch(function (err) {
        res.status(500).json(cu.createErrorResult(err,"MSG_ERR_BRAND_SAVE_FAIL"));
        return tr.rollback();
    }).done();
});

/**
 * @desc change one brand's status
 **/
router.post('/status/change', secure.applySecurity([secure.checkMMUser()]), function (req, res) {
    var tr = new cm();
    var Bale = req.body;
    Bale.StatusId = 0;

    Q.when()
        .then(function () {
            if (cu.isBlank(Bale.BrandId))
                throw new Error("Brand ID is required");
            if (cu.isBlank(Bale.Status))
                throw new Error("Status is required");
        })
        .then(function () {
            switch (Bale.Status) {
                case 'Active':
                    Bale.StatusId = CONSTANTS.STATUS.ACTIVE;
                    break;
                case 'Inactive':
                    Bale.StatusId = CONSTANTS.STATUS.INACTIVE;
                    break;
                default:
                    throw {
                        AppCode: "MSG_ERR_STATUS_UNKNOWN"
                    };
            }
        })
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return lbrand.changeStatus(Bale, tr);
        })
        .then(function () {
            return tr.commit();
        })
        .then(function () {
            res.json(true);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_BRAND_STATUS_FAIL"));
            return tr.rollback();
        })
        .done();
});

/**
 * @desc retrieve one brand info
 **/
router.get('/view', function (req, res) {
    var tr = new cm();
    var brandId = _.parseInt(req.query.brandid);
    var data = {};

    Q.when()
        .then(function () {
            if (_.isNaN(brandId)) {
                throw new Error("Brand ID is required");
            }
        })
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return lbrand.get(brandId, tr);
        })
        .then(function (result) {
            data = result;
            return tr.commit();
        })
        .then(function () {
            res.json(data);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err,"MSG_ERR_BRAND_VIEW_FAIL"));
            return tr.rollback();
        })
        .done();
});

/**
 * @desc retrieve all of brands or one merchant's brands list which defined in merchant profile.
 **/
router.get('/list', function (req, res) {
    var tr = new cm();
    var data = [];

    Q.when()
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return lbrand.list(req.query.cc, req.query.merchantid, tr);
        })
        .then(function (result) {
            data = result;
            return tr.commit();
        })
        .then(function () {
            res.json(data);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_BRAND_LIST_FAIL"));
            return tr.rollback();
        })
        .done();
});

/**
 * @desc retrieve all of brands and merchants
 **/
router.get('/list/combined', function (req, res) {
    var tr = new cm();
    var data = [];

    Q.when()
        .then(function () {
            return tr.begin();
        })
        .then(function () {
            return lbrand.listCombined(req.query.cc, tr);
        })
        .then(function (result) {
            data = result;
            return tr.commit();
        })
        .then(function () {
            res.json(data);
        })
        .catch(function (err) {
            res.status(500).json(cu.createErrorResult(err, "MSG_ERR_BRAND_LIST_COMBINED_FAIL"));
            return tr.rollback();
        })
        .done();
});

module.exports = router;
