"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var fs = require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var ci = require('../lib/commonimage.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var lu = require('../logic/logicuser.js');
var lum = require('../logic/logicusermessaging.js');
var path = require('path');
var multer = require('multer');
var moment = require('moment');

var UPLOAD_DIR = path.resolve(__dirname, '../uploads/userimages');

router.get('/list/mm', secure.applySecurity([secure.checkMMAdmin()]), function (req, res) {
	var tr = new cm();
	var UserList=null;
	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function() {
            var params ={
                UserTypeId:CONSTANTS.USER_TYPE.MM
            };
		return tr.queryMany("select u.UserId, u.UserKey, u.UserTypeId, u.MerchantId, u.InventoryLocationId, u.FirstName, u.LastName, u.MiddleName, u.DisplayName,u.Email,u.MobileCode, u.MobileNumber, u.ProfileImage,u.StatusId, s.StatusNameInvariant, u.LastLogin, u.LastModified from User u, Status s where u.StatusId=s.StatusId and u.StatusId <> 1 and u.UserTypeId=:UserTypeId order by u.LastModified desc", params).then(function(data) {
			UserList=data;
		})
	})
	.then(function() {
		var promises = UserList.map(function(iUser){
			return tr.queryMany("select SecurityGroupId from UserSecurityGroup where UserId=:UserId",{UserId:iUser.UserId}).then(function(data) {
				iUser.UserSecurityGroupArray=[];
				for (var i in data)
					iUser.UserSecurityGroupArray.push(data[i].SecurityGroupId);
			});
		});
		return Q.all(promises);
	})
	.then(function() {
		for (var i = 0; i < UserList.length; i++) {
			UserList[i].isMobileUser = false;
		}
		res.json(UserList);
		return tr.commit();
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_LIST"));
		return tr.rollback();
	})
	.done();
});

router.get('/list/merchant', secure.applySecurity([secure.checkMMUser(), secure.checkMerchAdmin()]), function (req, res) {
	var tr = new cm();
	var UserList=null;
	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.merchantid))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
                var params = {
                    MerchantId: req.query.merchantid,
                    UserTypeId: CONSTANTS.USER_TYPE.MERCHANT
                };
		return tr.queryMany("select u.UserId, u.UserKey, u.UserTypeId, u.MerchantId, u.InventoryLocationId, u.FirstName, u.LastName, u.MiddleName, u.DisplayName,u.Email,u.MobileCode, u.MobileNumber, u.ProfileImage,u.StatusId, s.StatusNameInvariant, u.LastLogin, u.LastModified from User u, Status s where u.StatusId=s.StatusId and u.StatusId <> 1 and u.UserTypeId=:UserTypeId and u.MerchantId=:MerchantId order by u.LastModified desc",params).then(function(data) {
			UserList=data;
		})
	})
	.then(function() {
		var promises = UserList.map(function(iUser){
			return tr.queryMany("select SecurityGroupId from UserSecurityGroup where UserId=:UserId",{UserId:iUser.UserId}).then(function(data) {
				iUser.UserSecurityGroupArray=[];
				for (var i in data)
					iUser.UserSecurityGroupArray.push(data[i].SecurityGroupId);
			});
		});
		return Q.all(promises);
	})
	.then(function() {

		//checking if user is mobile or not....
		for (var i = 0; i < UserList.length; i++) {
			if (UserList[i].UserSecurityGroupArray) {
	            if (UserList[i].UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ORDER_PROCESSSING) > -1 || 
                        UserList[i].UserSecurityGroupArray.indexOf(CONSTANTS.USER_SECURITY_GROUP.MERCHANT_PRODUCT_RETURN) > -1) {
	             	UserList[i].isMobileUser = true;
	            }else{
	               UserList[i].isMobileUser = false;
	            }
	        }
		}

		res.json(UserList);
		return tr.commit();
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_LIST"));
		return tr.rollback();
	})
	.done();
});

router.get('/list/curator', function (req, res) {
	var tr = new cm();
  var Bale = req.query;
  Bale.limit = parseInt(Bale.limit) || 100;
  Bale.start = parseInt(Bale.start) || 0;

  tr.begin()
  .then(function () {
    return tr.queryMany("SELECT User.UserKey, User.UserName, User.FirstName, User.LastName, User.DisplayName, User.ProfileImage FROM User WHERE User.IsCuratorUser = 1 AND User.StatusId = 2 LIMIT "+Bale.start+","+Bale.limit);
  })
  .then(function (UserList) {
		res.json(UserList);
		return tr.commit();
  })
  .catch(function(err){
    res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_LIST"));
    return tr.rollback();
  });
});

router.get('/view', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf()]), function (req, res) {
	var tr= new cm();
	var User;
	tr.begin()
	.then(function(){
		return lu.getUser(tr,cu.reqUser(req) ,req.query.merchantid).then(function(data) {
			User=data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(User);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_ERROR"));
		return tr.rollback();

	})
	.done();
});


//injected service by Gerald
router.post('/change/language', secure.applySecurity([secure.checkSelf(true)]), function (req, res){
	var tr = new cm();
	var User;
	var CultureCode = req.body.CultureCode;

	Q.when()
	.then(function(){
		//init vars;
		if (cu.isBlank(req.body.CultureCode))
			throw new Error("culture code is required");
	})
	.then(function(){
		return tr.begin();
	})
  .then(function () {
    return lu.getUser(tr,cu.reqUser(req)).then(function (data) {
      User = data;
    })
  })
	.then(function(){
		return tr.queryOne("select LanguageId from Language where LanguageNameInvariant=:cc",{cc:CultureCode}).then(function(data) {
      if (!data) throw {AppCode: 'MSG_ERR_NOT_FOUND'};
			User.LanguageId = data.LanguageId;
		})
	})
	.then(function(){
		//update here
		return tr.queryExecute("update User set LanguageId=:LanguageId where UserId=:UserId;", User);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(User);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_REFERENCE_FAIL"));
		return tr.rollback();
	})
	.done();

});
//end injected service by Gerald

router.post('/save', secure.applySecurity([
	secure.isCreateNewUser(),
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf(true)]), function (req, res) {
	var tr= new cm();
	var Bale, User;

	Q.when()
	.then(function(){
		return tr.begin();
	})
  .then(function () {
    if (cu.checkIdBlank(req.body.UserId) && cu.checkIdBlank(req.body.UserKey)) {
      Bale = _.clone(req.body);
    } else {
      // update: get existing user object, only update changed fields
      return lu.getUser(tr,cu.reqUser(req), req.body.MerchantId).then(function (data) {
        Bale = _.extend(data, req.body, {
          UserId: data.UserId,
          UserKey: data.UserKey
        });
      });
    }
  })
	.then(function() {
		return lu.checkUserName(Bale, tr);
	})
	.then(function() {
    	return lu.checkUserEmail(Bale, tr);
	})
	.then(function() {
    	return lu.checkUserMobile(Bale, tr);
	})
	.then(function(){
		if (cu.checkIdBlank(Bale.UserId))
		{
			if (cu.isBlank(Bale.UserTypeId))
				throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};

			if (lu.isMobileUser(Bale))
				Bale.ActivationToken = cc.createMobileCode();
			else
				Bale.ActivationToken = cc.createSimpleToken();

			Bale.SendMessageFlag=true;
      //note status id set to 3 for pending on initial user create
      Bale.StatusId = CONSTANTS.STATUS.PENDING;
                        
			return tr.queryExecute("insert into User (UserKey, UserTypeId,UserName,FirstName,LastName,MiddleName,DisplayName,Email,MobileCode,MobileNumber,LanguageId,TimeZoneId,ProfileImage,ActivationToken,MerchantId,StatusId,StatusReasonCode,LastStatus) values (uuid(), :UserTypeId,:UserName,:FirstName,:LastName,:MiddleName,:DisplayName,:Email,:MobileCode,:MobileNumber,:LanguageId,:TimeZoneId,:ProfileImage,:ActivationToken,:MerchantId,:StatusId,'RC_PEND_REG',UTC_TIMESTAMP());", Bale).then(function(data) {
				Bale.UserId=data.insertId;
				return;
			});
		}
		else
		{
			//don't allow email update as this goes through a separate services for re-validation
			//mobile removed from update as it own change service now.
			return tr.queryExecute("update User set UserName=:UserName, FirstName=:FirstName, LastName=:LastName,MiddleName=:MiddleName,DisplayName=:DisplayName,LanguageId=:LanguageId,TimeZoneId=:TimeZoneId,ProfileImage=:ProfileImage where UserId=:UserId;", Bale);
		}
	})
	.then(function(){
		if (!Bale.UserSecurityGroupArray) return;
    return lu.saveUserSecurityGroup(Bale, Bale.UserSecurityGroupArray, tr);
	})
	.then(function(){
		if (!Bale.UserInventoryLocationArray) return;
    return lu.saveUserInventoryLocation(Bale, Bale.UserInventoryLocationArray, tr);
	})
	.then(function(){
		return lu.getUser(tr,Bale.UserId).then(function(data) {
			User=data;
		});
	})
	.then(function(){
		if (Bale.SendMessageFlag) {
			return lum.sendUserMessage(User, lum.messageTemplateCodes.CREATE_USER, tr);
		}
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(User);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_SAVE_FAIL"));
		return tr.rollback();

	})
	.done();
});

// update API for common fields for store front my account
router.post('/update', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf()
]), function (req, res) {
  var tr = new cm();
  var Bale = req.body;
  var User;
  
  Q.when()
  .then(function () {
    return tr.begin();
  })
  .then(function () {
    return lu.getUser(tr, Bale.UserKey).then(function (data) {
      User = data;
    })
  })
  .then(function () {
    _.defaults(Bale, User);
    if (Bale.DateOfBirth) {
      Bale.DateOfBirth = moment(Bale.DateOfBirth).format('YYYY-MM-DD') || null;
    }
    // todo validate fields
    return tr.queryExecute("update User set FirstName=:FirstName, LastName=:LastName, MiddleName=:MiddleName, DisplayName=:DisplayName, GeoCountryId=:GeoCountryId, GeoProvinceId=:GeoProvinceId, GeoCityId=:GeoCityId, Gender=:Gender, DateOfBirth=:DateOfBirth, LanguageId=:LanguageId, TimeZoneId=:TimeZoneId where UserId=:UserId;", Bale);
  })
  .then(function () {
    return lu.getUser(tr, User.UserId).then(function (data) {
      User = data;
    })
  })
  .then(function(){
    return tr.commit();
  })
  .then(function(){
    res.json(User);
  })
  .catch(function(err){
    res.status(500).json(cu.createErrorResult(err, "MSG_ERR_USER_ERROR"));
    return tr.rollback();
  })
});

/**
 * this route is not used anywhere
 */
router.post('/inventorylocation/change', secure.checkMmMerchantAdminOrSelf, function (req, res) {
	var tr= new cm();
	var User;
	Q.when()
	.then(function(){
		return tr.begin();
	})
  .then(function () {
    return lu.getUser(tr,cu.reqUser(req)).then(function (data) {
      if (data.StatusId!=CONSTANTS.STATUS.ACTIVE) {
        throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"}; //don't perform if status is not active
      }
      User = data;
    });
  })
	.then(function(){
    User.InventoryLocationId=req.body.InventoryLocationId;
		//don't allow emial update as this goes through a seperate services for re-validation
		return tr.queryExecute("update User set InventoryLocationId=:InventoryLocationId where UserId=:UserId;",User);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_IL_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/status/change', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf(true)]), function (req, res) {
	var tr= new cm();
  var User;
	var StatusIdRequested=0;

	Q.when()
	.then(function(){
		switch(req.body.Status)
		{
		case 'Active':
			 StatusIdRequested=CONSTANTS.STATUS.ACTIVE;
			 break;
		case 'Inactive':
			 StatusIdRequested=CONSTANTS.STATUS.INACTIVE;
			 break;
		case 'Deleted':
			 StatusIdRequested=CONSTANTS.STATUS.DELETED;
			 break;
		default:
			throw {AppCode:"MSG_ERR_USER_STATUS_UNKOWN"};
		};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
    return lu.getUser(tr,cu.reqUser(req), req.body.MerchantId).then(function (data) {
      User = data;
    });
	})
  .then(function () {
    // cannot self delete or inactivate
    if (
      User.UserId === req.AccessUser.UserId &&
      StatusIdRequested !== CONSTANTS.STATUS.ACTIVE
    ) {
      throw {
        AppCode: 'MSG_ERR_USER_STATUS_FAIL',
        Message: 'User cannot self delete.'
      };
    }
  })
	.then(function() {
		User.StatusId=StatusIdRequested;
		return tr.queryExecute("update User set StatusId=:StatusId,StatusReasonCode=null,LastStatus=UTC_TIMESTAMP() where UserId=:UserId;",User).then(function(data) {
			return;
		})

	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_STATUS_FAIL"));
		return tr.rollback();

	})
	.done();
});

// password change for storefront My Account
router.post('/passwordchange', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf()
]), function (req, res) {
	var tr = new cm();
	var Bale = req.body;
	var Ret = null;

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
	    return lu.changePassword(Bale, tr).then(function (data) {
	     	Ret = data;
	    });
	})
	.then(function() {
		return tr.commit();
	})
	.then(function(){
		res.json(Ret);
		return true;
	})
	.catch(function(err){
		console.log(err);
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/usernamechange', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf()
]), function (req, res) {
	Q.spawn(function*(){
		var tr = new cm();

		try{
			var Bale = req.body || {};

			if(cu.isBlank(Bale.UserKey))
				throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
			if(cu.isBlank(Bale.UserName))
				throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserName" };

			yield tr.begin();

			var ret = yield lu.changeUsername(tr, Bale);
			yield tr.commit();
			res.send(true);			
		}catch(err){			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err, "MSG_ERR_USER_CHANGE_USERNAME_FAIL"));			
		}
	});	
});

router.post('/password/change', secure.applySecurity([secure.checkMMAdminOrMerchantAdmin(), secure.checkSelf(true)]), function (req, res) {
	var tr= new cm();
	var User;
	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function() {
    return lu.getUser(tr,cu.reqUser(req), req.body.MerchantId).then(function (data) {
      User = data;
      if (User.StatusId==CONSTANTS.STATUS.INACTIVE || User.StatusId==CONSTANTS.STATUS.DELETED) {
        //don't perform if status is deleted or inactive
        throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
      }
    });
	})
	.then(function() {
		User.ActivationToken = cc.createSimpleToken();
		return tr.queryExecute("update User set StatusId=3,StatusReasonCode='RC_RESET_PASSWORD', LastStatus=UTC_TIMESTAMP(), Hash=null, Salt=null, ActivationToken=:ActivationToken where UserId=:UserId;",User)
	})
	.then(function(){
		return lu.getUser(tr,User.UserId).then(function(data) {
			User=data;
		});
	})
	.then(function() {
		return lum.sendUserMessage(User, lum.messageTemplateCodes.RESET_PASSWORD, tr).then();
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FAIL"));
		return tr.rollback();

	})
	.done();
});



/**
 * this route is not used anywhere
 */
router.post('/password/change/immediate', secure.checkMmMerchantAdminOrSelf, function (req, res) {
	var tr= new cm();
  var Password = req.body.Password;
  var PasswordOld = req.body.PasswordOld;
  var User;

	Q.when()
	.then(function(){
		if (cu.isBlank(Password))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
		if (cu.isBlank(PasswordOld))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
    return lu.getUser(tr,cu.reqUser(req)).then(function (data) {
			User = data;
			if (User.StatusId==CONSTANTS.STATUS.INACTIVE || User.StatusId==CONSTANTS.STATUS.PENDING || User.StatusId==CONSTANTS.STATUS.DELETED) {
        //don't perform if status is deleted or inactive or pending
				throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
      }
			var loginHash=cc.createHash(PasswordOld, User.Salt);
			if (User.Hash!=loginHash)
				throw {AppCode:"MSG_ERR_RESET_OLD_PASSWORD_WRONG"};
    })
	})
	.then(function() {
		User.Salt=cc.createSalt();
		User.Hash=cc.createHash(Password, User.Salt);

		return tr.queryExecute("update User set Salt=:Salt, Hash=:Hash where UserId=:UserId;", User);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FAIL"));
		return tr.rollback();

	})
	.done();
});

router.post('/email/change', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf(true)]), function (req, res) {
	var tr= new cm();
  var Email = req.body.Email;
  var User;

	Q.when().then(function(){
		if (cu.isBlank(Email))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};

		return tr.begin();
	})
  .then(function () {
    return lu.getUser(tr,cu.reqUser(req), req.body.MerchantId).then(function (data) {
      User = data;
      if (!User || User.StatusId==CONSTANTS.STATUS.INACTIVE || User.StatusId==CONSTANTS.STATUS.DELETED) {
        //don't perform if status is inactive
        throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
      }
    });
  })
	.then(function() {
    User.Email = Email;
    return lu.checkUserEmail(User, tr);
	})
	.then(function() {
		if (lu.isMobileUser(User))
			User.ActivationToken = cc.createMobileCode();
		else
			User.ActivationToken = cc.createSimpleToken();

		User.SendMessageFlag=true;
		return tr.queryExecute("update User set StatusId=3,StatusReasonCode='RC_RESET_EMAIL', LastStatus=UTC_TIMESTAMP(), ActivationToken=:ActivationToken, Email=:Email where UserId=:UserId;",User);
	})
	.then(function() {
		if (User.SendMessageFlag==true)
		{
			return lum.sendUserMessage(User, lum.messageTemplateCodes.CHANGE_EMAIL, tr);
		}
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FAIL"));
		return tr.rollback();
	})
	.done();
});

/**
 * this route is not used anywhere
 */
router.post('/email/change/immediate', secure.checkMmMerchantAdminOrSelf, function (req, res) {
	var tr= new cm();
  var Email = req.body.Email;
  var User;

	Q.when().then(function(){
		if (cu.isBlank(Email))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};

		return tr.begin();
	})
  .then(function () {
    return lu.getUser(tr,cu.reqUser(req)).then(function (data) {
      User = data;
      if (User.StatusId==CONSTANTS.STATUS.INACTIVE) {
        //don't perform if status is inactive
        throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
      }
    });
  })
	.then(function() {
    User.Email = Email;
    return lu.checkUserEmail(User, tr);
	})
	.then(function() {
		return tr.queryExecute("update User set Email=:Email where UserId=:UserId;",Bale);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FAIL"));
		return tr.rollback();
	})
	.done();
});


router.post('/resend/link', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf(true)]), function (req, res) {
	var tr= new cm();
	var User;
	var LinkRequested = req.body.Link;

	tr.begin()
	.then(function() {
    return lu.getUser(tr,cu.reqUser(req), req.body.MerchantId).then(function (data) {
      User = data;
      if (User.StatusId != CONSTANTS.STATUS.PENDING) {
        //don't perfom if not Pending
        throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
      }
    })
	})
	.then(function() {
		//uncommnet next line to create  new token, else it will send the existing token (which i thin kis better?)
		User.ActivationToken = cc.createSimpleToken();

		//reset last staus date to make the token valid for longer
		return tr.queryExecute("update User set LastStatus=UTC_TIMESTAMP(), ActivationToken=:ActivationToken where UserId=:UserId;",User);
	})
	.then(function(){
		return lu.getUser(tr,User.UserId).then(function(data) {
			User = data;
		});
	})
	.then(function() {
		switch(LinkRequested)
		{
		case 'Email':
			return lum.sendUserMessage(User, lum.messageTemplateCodes.CHANGE_EMAIL, tr);
			break;
		case 'Registration':
			return lum.sendUserMessage(User, lum.messageTemplateCodes.CREATE_USER, tr);
			break;
		case 'Password':
			return lum.sendUserMessage(User, lum.messageTemplateCodes.RESET_PASSWORD, tr).then();
			break;
		default:
			throw {AppCode:"MSG_ERR_USER_LINK_UNKOWN"};
		}
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_LINK_FAIL"));
		return tr.rollback();
	})
	.done();
});

// mobile change for storefront My Account
router.post('/mobilechange', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf()
]), function (req, res) {
	var tr = new cm();
	var Mobile = req.body;

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return lu.getUser(tr, Mobile.UserKey).then(function(data){
			Mobile.UserId = data.UserId;
		});
	})
	.then(function(){
		return lu.checkMobileVerification(Mobile, tr);
	})
	.then(function(){
		return lu.checkUserMobile(Mobile, tr);
	})
	.then(function(){
		return lu.UpdateMobileCodeandMobileNumber(Mobile,tr);
	})
	.then(function(){
		return tr.commit();
	})
	.then(function(){
		res.json(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_STATUS_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/mobile/change', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf(true)]), function (req, res) {
	var tr= new cm();
	var User;
    var MobileCode = req.body.MobileCode;
  	var MobileNumber = req.body.MobileNumber;

	Q.when()
	.then(function(){
		if (cu.isBlank(MobileCode))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
		if (cu.isBlank(MobileNumber))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};

		return tr.begin();
	})
  .then(function() {
    return lu.getUser(tr,cu.reqUser(req),req.body.MerchantId).then(function (data) {
      User = data;
      User.MobileCode = MobileCode;
      User.MobileNumber = MobileNumber;

      if (User.StatusId==CONSTANTS.STATUS.INACTIVE || User.StatusId==CONSTANTS.STATUS.DELETED) {
        //don't perform if status is deleted or inactive
        throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
      }
    });
  })
  .then(function () {
    return lu.checkUserMobile(User, tr);
  })
	.then(function() {
		if (lu.isMobileUser(User))
			User.ActivationToken = cc.createMobileCode();
		else
			User.ActivationToken = cc.createSimpleToken();

		return tr.queryExecute("update User set StatusId=3,StatusReasonCode='RC_RESET_EMAIL', LastStatus=UTC_TIMESTAMP(), ActivationToken=:ActivationToken, MobileNumber=:MobileNumber, MobileCode=:MobileCode where UserId=:UserId;", User);

	})
	.then(function() {
    return lum.sendUserMessage(User, lum.messageTemplateCodes.CHANGE_MOBILE, tr);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_STATUS_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/mobile/change/immediate', secure.applySecurity([
	secure.checkMMAdminOrMerchantAdmin(),
	secure.checkSelf(true)]), function (req, res) {
	var tr= new cm();
	var User;
  var MobileCode = req.body.MobileCode;
  var MobileNumber = req.body.MobileNumber;

	Q.when()
	.then(function(){
		if (cu.isBlank(MobileCode))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
		if (cu.isBlank(MobileNumber))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};

		return tr.begin();
	})
  .then(function() {
    return lu.getUser(tr,cu.reqUser(req), req.body.MerchantId).then(function (data) {
      User = data;
      User.MobileCode = MobileCode;
      User.MobileNumber = MobileNumber;

      if (User.StatusId==CONSTANTS.STATUS.INACTIVE || User.StatusId==CONSTANTS.STATUS.DELETED) {
        //don't perform if status is deleted or inactive
        throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
      }
    });
  })
  .then(function () {
    return lu.checkUserMobile(User, tr);
  })
	.then(function() {
		return tr.queryExecute("update User set MobileNumber=:MobileNumber, MobileCode=:MobileCode where UserId=:UserId;", User);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_EMAIL_FAIL"));
		return tr.rollback();
	})
	.done();
});

/**
 * this route is not used anywhere
 */
router.post('/problem', function (req, res) {
	var tr= new cm();
	var Bale={};

  Q.when().then(function () {
    return tr.begin();
  })
  .then(function () {
    return lu.getUser(tr,cu.reqUser(req)).then(function (data) {
      Bale.UserId=data.UserId;
      Bale.Subject=req.body.Subject;
      Bale.Message=req.body.Message;
    })
  })
	.then(function() {
		return tr.commit();
	})
  .then(function () {
    res.json(true);
    console.log("Problem from UserId=" + Bale.UserId + "\r\n" + "Subject=" + Bale.Subject + "\r\n" + "Message=" + Bale.Message);
  })
  .catch(function(err){
    res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FAIL"));
    return tr.rollback();
  });
});

/**
 * this route is used by Mobile Storefront
 */
router.use('/upload/*', secure.checkAuth, multer({
  dest: UPLOAD_DIR,
  limits: {
    fileSize: 5 * CONSTANTS.MEGABYTE,
    files: 1 
  }
}).single('file'), function (req, res, next) {
  if (!req.file) return next();

  var filePath = path.resolve(UPLOAD_DIR, req.file.filename);
  ci.resizeImage(filePath, 200).then(function () {
    next();
  }).catch(next);
});

router.post('/upload/profileimage', function (req, res, next) {
  var tr = new cm();
  var filename = req.file ? req.file.filename : '';
  return tr.begin()
  .then(function () {
    return tr.queryExecute(
      "UPDATE User SET ProfileImage=:ProfileImage WHERE UserId=:UserId",
      { ProfileImage: filename, UserId: req.AccessUser.UserId }
    );
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json({ ProfileImage: filename });
  })
  .catch(function (err) {
    next(err);
    return tr.rollback();
  });
});

router.post('/upload/coverimage', function (req, res, next) {
  var tr = new cm();
  var filename = req.file ? req.file.filename : '';

  return tr.begin()
  .then(function () {
    return tr.queryExecute(
      "UPDATE User SET CoverImage=:CoverImage WHERE UserId=:UserId",
      { CoverImage: filename, UserId: req.AccessUser.UserId }
    );
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json({ CoverImage: filename });
  })
  .catch(function (err) {
    next(err);
    return tr.rollback();
  });
});


router.use('/upload/*', function (err, req, res, next) {
  res.status(500).json(cu.createErrorResult(err, "MSG_ERR_IMAGE_UPLOAD"));
});

module.exports = router;
