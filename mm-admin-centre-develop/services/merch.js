"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'),
  router = express.Router();
var fs = require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var lmerch=require('../logic/logicmerch.js');
var lp=require('../logic/logicproduct.js');


router.get('/list', secure.applySecurity([secure.checkMMUser()]), function(req, res) {
  var tr = new cm();
  var MerchantList = null;
  var Bale = {};
  Bale.CultureCode = req.query.cc;

  Q.when()
    .then(function() {
      if (cu.isBlank(Bale.CultureCode))
        throw {
          AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'
        };
    })
    .then(function() {
      return tr.begin();
    })
    .then(function() {
		return lmerch.list(tr,Bale.CultureCode).then(function(data) {
			MerchantList= data;
		});
    })
    .then(function() {
      return tr.commit();
    })
    .then(function() {
      res.json(MerchantList);
      return;
    })
    .catch(function(err) {
      res.status(500).json(cu.createErrorResult(err, "MSG_ERR_MERCHANT_LIST_FAIL"));
      return tr.rollback();
    })
    .done();
});

router.get('/view', secure.applySecurity([secure.checkMMUser(), secure.checkMerchAdmin()]), function(req, res) {
    console.log("view merchant");
    var Merchant = {}; //merchant object.
    var MerchantUserIds = []; //merchant user object array;
    var MerchantDocuments = []; //merchant document object array
    var MerchantBrands = []; //merchant brands object array;
    var MerchantImageList = []; //merchant merchant image list
    var Results = {}; //response result object which will response to client side.
    var Bale = {};
    Bale.CultureCode = req.query.cc;
    Bale.MerchantId = req.query.merchantid;

    var tr = new cm();
    Q.when()
      .then(function() {
        return tr.begin();
      })
    .then(function() { //1.1.get merchant
      return tr.queryOne("select * from Merchant where MerchantId=:MerchantId",Bale).then(function(data) {
        Merchant=data; //store the returned user
        });
    })
    .then(function() { //1.2.get merchant cultrue
      return tr.queryMany("select * from MerchantCulture where MerchantId=:MerchantId",Bale).then(function(data) {
        var MerchantCultureList=data;
        _.forEach(MerchantCultureList, function(MerchantCulture){
          if(MerchantCulture.CultureCode==='EN'){
            Merchant.MerchantNameEN = MerchantCulture.MerchantName;
            Merchant.MerchantDescEN = MerchantCulture.MerchantDesc;
          }else if(MerchantCulture.CultureCode==='CHS'){
            Merchant.MerchantNameCHS = MerchantCulture.MerchantName;
            Merchant.MerchantDescCHS = MerchantCulture.MerchantDesc;
          }else if(MerchantCulture.CultureCode==='CHT'){
            Merchant.MerchantNameCHT = MerchantCulture.MerchantName;
            Merchant.MerchantDescCHT = MerchantCulture.MerchantDesc;
          }
        })
      });
    })
    .then(function() {//2.get merchant staff
      return tr.queryMany("select * from MerchantUser where MerchantId=:MerchantId", {MerchantId: Bale.MerchantId}).then(function(data) {
        MerchantUserIds = data.map(function(n){return n.UserId;});
      })
    })
    .then(function() {//3.get merchant brands
      return tr.queryMany("select * from MerchantBrand where MerchantId=:MerchantId", {MerchantId: Bale.MerchantId}).then(function(data) {
        MerchantBrands = data;
      })
    })
    .then(function() {//4.get merchant documents
      return tr.queryMany("select * from MerchantDocument where MerchantId=:MerchantId", {MerchantId: Bale.MerchantId}).then(function(data) {
        MerchantDocuments = data;
      })
    })
    .then(function() {//5.get merlistchant image list
      return tr.queryMany("select * from MerchantImage where MerchantId=:MerchantId", {MerchantId: Bale.MerchantId}).then(function(data) {
        MerchantImageList = data;
      })
    })
    .then(function() {
      return tr.commit();
    })
    .then(function() {
      Results.Merchant = Merchant;
      Results.MerchantUserIds = MerchantUserIds;
      Results.MerchantDocuments = MerchantDocuments;
      Results.MerchantBrands = MerchantBrands;
      Results.MerchantImageList = MerchantImageList;
      res.json(Results);
      return;
    })
    .catch(function(error) {
      res.status(500).json(cu.createErrorResult(error, "MSG_ERR_MERCHANT_VIEW"));
      return tr.rollback();
    })
    .done();
});

router.post('/status/change', secure.applySecurity([secure.checkMMUser()]), function(req, res) {
  var tr = new cm();
  var Merchant = null;
  var Bale = req.body;
  Bale.StatusId = 0;

  Q.when()
    .then(function() {
      if (cu.isBlank(Bale.MerchantId))
        throw {
          AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'
        };
      if (cu.isBlank(Bale.Status))
        throw {
          AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'
        };
    })
    .then(function() {
      switch (Bale.Status) {
        case 'Active':
          Bale.StatusId = CONSTANTS.STATUS.ACTIVE;
          break;
        case 'Inactive':
          Bale.StatusId = CONSTANTS.STATUS.INACTIVE;
          break;
        default:
          throw {
            AppCode: "MSG_ERR_STATUS_UNKOWN"
          };
      }
    })
    .then(function() {
      return tr.begin();
    })
    .then(function() {
      if (Bale.StatusId === CONSTANTS.STATUS.INACTIVE) {
        //no active product
        return tr.queryOne("select count(*) as total from Sku where MerchantId=:MerchantId and StatusId=:StatusId",
            {MerchantId: Bale.MerchantId, StatusId: CONSTANTS.STATUS.ACTIVE}).then(function(data) {
          if (data.total > 0) {
            throw {
              AppCode: "MSG_ERR_ACTIVE_PRODUCT_EXISTS"
            }
          }
        }).then(function () {
          //no active inventory location
          return tr.queryOne("select count(*) as total from InventoryLocation where MerchantId=:MerchantId and StatusId=:StatusId",
              {MerchantId: Bale.MerchantId, StatusId: CONSTANTS.STATUS.ACTIVE}).then(function(data) {
            if (data.total > 0) {
              throw {
                AppCode: "MSG_ERR_ACTIVE_LOC_EXISTS"
              }
            }
          })
        })
      }
    })
    .then(function() {
      return tr.queryExecute("update Merchant set StatusId=:StatusId,LastStatus=UTC_TIMESTAMP() where MerchantId=:MerchantId;", Bale);
    })
    .then(function() {
      return tr.commit();
    })
    .then(function() {
      res.json(true);
    })
    .catch(function(err) {
      res.status(500).json(cu.createErrorResult(err, "MSG_ERR_MERCHANT_STATUS_FAIL"));
      return tr.rollback();
    })
    .done();
});

/**
 * create and edit merchant
 * @queryParam {Merchant: Merchant Object, MerchantUserIds: MerchantUserIds Array}
 * @response
 **/
router.post('/save', secure.applySecurity([secure.checkMMUser()]), function(req, res) {
  console.log("save merchant");
  //initiate params
  var Bale = req.body;
  var Merchant = Bale.Merchant;
  //set MerchantNameInvariant which is equal to MerchantNameEN
  Merchant.MerchantNameInvariant = Merchant.MerchantNameEN;
  //set MerchantNameInvariant which is equal to MerchantNameEN
  Merchant.MerchantDescInvariant = Merchant.MerchantDescEN;

  var MerchantUserIds = Bale.MerchantUserIds;
  var MerchantBrandIds = Bale.MerchantBrandIds; //brand id array
  var MerchantDocuments = Bale.MerchantDocuments;
  var MerchantCategorys = Bale.BillingCatory;
  var MerchantImageList = Bale.MerchantImageList;

  //set save mode;
  var saveMode=false; //true-create new merchant; false-edit merchant;
  if(cu.checkIdBlank(Merchant.MerchantId)){
    saveMode = true;
  }

  //excute save logic
  var tr = new cm();
  Q.when()
    .then(function() {
      return tr.begin();
    })
    .then(function() { //1.check whether merchant display name is exsited of other merchants.
      return tr.queryOne("select count(*) totalRecord from Merchant m "+
          "inner join MerchantCulture mc on(m.MerchantId=mc.MerchantId) "+
          "where m.MerchantId<>:MerchantId "+
          "and ((mc.CultureCode='EN' and mc.MerchantName=:MerchantNameEN) "+
          "or(mc.CultureCode='CHS' and mc.MerchantName=:MerchantNameCHS) "+
          "or(mc.CultureCode='CHT' and mc.MerchantName=:MerchantNameCHT)) ", {
        MerchantId : (Merchant.MerchantId==undefined)?'':Merchant.MerchantId,
        MerchantNameEN: Merchant.MerchantNameEN,
        MerchantNameCHS: Merchant.MerchantNameCHS,
        MerchantNameCHT: Merchant.MerchantNameCHT,
      }).then(function(data) {
        if (data.totalRecord>0)
          throw {
            AppCode: "MSG_ERR_MERCHANT_DISPLAY_NAME_UNIQUE"
          };
      });
    })
      .then(function () {
        //2. check if brands are all not pending
        if (Bale.MerchantBrandIds.length) {
          return tr.queryOne('select BrandId from Brand where BrandId in (:BrandIds) and StatusId=:StatusId', {
            BrandIds: Bale.MerchantBrandIds,
            StatusId: CONSTANTS.STATUS.PENDING
          }).then(function (data) {
            if (data !== null) {
              throw {
                AppCode: "MSG_ERR_BRAND_STATUS_PENDING",
                Message: "BrandId #"+data.BrandId+" is in pending status"
              }
            }
            //check if any removed brands have product
            return tr.queryOne('select SkuId from Sku ' +
                'where MerchantId=:MerchantId and StatusId<>:StatusId and BrandId in ' +
                '(select mb.BrandId ' +
                'from MerchantBrand mb ' +
                'inner join Brand b on mb.BrandId = b.BrandId ' +
                'where mb.MerchantId=:MerchantId and b.StatusId<>:StatusId and mb.BrandId not in (:BrandIds))',
                {MerchantId: Merchant.MerchantId, StatusId: CONSTANTS.STATUS.DELETED, BrandIds: Bale.MerchantBrandIds}).then( function (data) {
              if (data !== null) {
                  throw {
                    AppCode: "MSG_ERR_PRODUCT_EXIST_UNDER_REMOVED_BRAND"
                  }
                }
            });
          });
        }
      })
    .then(function() {
      //1.1.create or edit merchant.
      if(saveMode){//create new merchant
        console.log("create new merchant");
        //statusId default==3;
        return tr.queryExecute("INSERT INTO `Merchant` (`MerchantTypeId`, `MerchantNameInvariant`, `MerchantCompanyName`, `BusinessRegistrationNo`, `MerchantSubdomain`, `MerchantDescInvariant`, `HeaderLogoImage`, `SmallLogoImage`, `LargeLogoImage`, `ProfileBannerImage`, `IsListedMerchant`, `IsFeaturedMerchant`, `IsRecommendedMerchant`, `IsSearchableMerchant`, `GeoCountryId`, `GeoIdProvince`, `GeoIdCity`, `District`, `PostalCode`, `Apartment`, `Floor`, `BlockNo`, `Building`, `StreetNo`, `Street`, `StatusId`) VALUES (:MerchantTypeId, :MerchantNameInvariant, :MerchantCompanyName, :BusinessRegistrationNo, :MerchantSubdomain, :MerchantDescInvariant, :HeaderLogoImage, :SmallLogoImage, :LargeLogoImage, :ProfileBannerImage, :IsListedMerchant, :IsFeaturedMerchant, :IsRecommendedMerchant, :IsSearchableMerchant, :GeoCountryId, :GeoIdProvince, :GeoIdCity, :District, :PostalCode, :Apartment, :Floor, :BlockNo, :Building, :StreetNo, :Street, 3)", Merchant).then(function(data) {
          Merchant.MerchantId = data.insertId;
          return;
        });
      }else{//update merchant
        console.log("edit merchant");
        return tr.queryExecute("update Merchant set MerchantTypeId=:MerchantTypeId,MerchantNameInvariant=:MerchantNameInvariant,MerchantCompanyName=:MerchantCompanyName,BusinessRegistrationNo=:BusinessRegistrationNo,MerchantSubdomain=:MerchantSubdomain,MerchantDescInvariant=:MerchantDescInvariant,HeaderLogoImage=:HeaderLogoImage,SmallLogoImage=:SmallLogoImage,LargeLogoImage=:LargeLogoImage,ProfileBannerImage=:ProfileBannerImage,IsListedMerchant=:IsListedMerchant,IsFeaturedMerchant=:IsFeaturedMerchant,IsRecommendedMerchant=:IsRecommendedMerchant,IsSearchableMerchant=:IsSearchableMerchant,GeoCountryId=:GeoCountryId,GeoIdProvince=:GeoIdProvince,GeoIdCity=:GeoIdCity,District=:District,PostalCode=:PostalCode,Apartment=:Apartment,Floor=:Floor,BlockNo=:BlockNo,Building=:Building,StreetNo=:StreetNo,Street=:Street,LastStatus=:LastStatus,LastModified=:LastModified where MerchantId=:MerchantId", Merchant);
      }
    })
    .then(function(){
      //1.2.create or edit on merchant culture
      var promises = [];
      var sql;
      if(saveMode){
        console.log("create new merchant culture");
        sql = "INSERT INTO MerchantCulture(`MerchantId`, `CultureCode`, `MerchantName`, `MerchantDesc`) VALUES(:MerchantId, :CultureCode, :MerchantName, :MerchantDesc)";
      }else{
        console.log("edit new merchant culture");
        // sql = "UPATE MerchantCulture SET CultureCode=:CultureCode, MerchantName=:MerchantName, MerchantDesc=:MerchantDesc) WHERE MerchantId=:MerchantId";
        // the reason why choose delete merchantCulture then add new record way to update is for compatible to the old data.
        var deletePromise= tr.queryExecute("delete from MerchantCulture where MerchantId=:MerchantId", {
          MerchantId: Merchant.MerchantId
        });
        promises.push(deletePromise);
        sql = "INSERT INTO MerchantCulture(`MerchantId`, `CultureCode`, `MerchantName`, `MerchantDesc`) VALUES(:MerchantId, :CultureCode, :MerchantName, :MerchantDesc)";
      }
      // set up array to insert
      var MerchantCultureArray = [];
      MerchantCultureArray.push({
        MerchantId: Merchant.MerchantId,
        MerchantName: Merchant.MerchantNameEN,
        MerchantDesc: Merchant.MerchantDescEN,
        CultureCode: 'EN'
      });
      MerchantCultureArray.push({
        MerchantId: Merchant.MerchantId,
        MerchantName: Merchant.MerchantNameCHS,
        MerchantDesc: Merchant.MerchantDescCHS,
        CultureCode: 'CHS'
      });
      MerchantCultureArray.push({
        MerchantId: Merchant.MerchantId,
        MerchantName: Merchant.MerchantNameCHT,
        MerchantDesc: Merchant.MerchantDescCHT,
        CultureCode: 'CHT'
      });
      var insertPromise = _.map(MerchantCultureArray, function(param){
        tr.queryExecute(sql, param)
      });
      promises = promises.concat(insertPromise);
      return Q.all(promises);
    })
    .then(function() {
      //2.1.delete MerchantUser of merchantId's.
      return tr.queryExecute("delete from MerchantUser where MerchantId=:MerchantId", {
        MerchantId: Merchant.MerchantId
      })
    })
    .then(function() {
      //2.2.insert all MerchantUser of merchantId's.
      if (!MerchantUserIds) return;
      var promises = MerchantUserIds.map(function(userId) {
        return tr.queryExecute("insert into MerchantUser (MerchantId, UserId) values(:MerchantId, :UserId)", {
          MerchantId: Merchant.MerchantId,
          UserId: userId,
        })
      });
      return Q.all(promises);
    })
    .then(function() {
      //3.1.delete merchantBrands of merchantId's.
      return tr.queryExecute("delete from MerchantBrand where MerchantId=:MerchantId", {
        MerchantId: Merchant.MerchantId
      })
    })
    .then(function() {
      //3.2.insert all merchantBrands.
      if (MerchantBrandIds){
        var promises = MerchantBrandIds.map(function(brandId) {
          return tr.queryExecute("insert into MerchantBrand (MerchantId, BrandId) values(:MerchantId, :BrandId)", {
            MerchantId: Merchant.MerchantId,
            BrandId: brandId,
          })
        });
        return Q.all(promises);
      }else{
        throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
      }
    })
    .then(function() {
      //4.1.delete merchantDocument of merchantId's.
      return tr.queryExecute("delete from MerchantDocument where MerchantId=:MerchantId", {
        MerchantId: Merchant.MerchantId
      })
    })
    .then(function() {
      //4.2.insert all merchantDocument.
      if (!MerchantDocuments) return;
      var promises = MerchantDocuments.map(function(merchantDocument) {
        return tr.queryExecute("insert into MerchantDocument (MerchantId, DocumentKey, DocumentOriginalName, DocumentMimeType) values(:MerchantId, :DocumentKey, :DocumentOriginalName, :DocumentMimeType)", {
          MerchantId: Merchant.MerchantId,
          DocumentKey: merchantDocument.DocumentKey,
          DocumentOriginalName: merchantDocument.DocumentOriginalName,
          DocumentMimeType: merchantDocument.DocumentMimeType
        })
      });
      return Q.all(promises);
    })
    .then(function() {
      //5.1.delete merchant category of merchantId's, which is to setting category commission rate
      return tr.queryExecute("delete from MerchantCategory where MerchantId=:MerchantId", {
        MerchantId: Merchant.MerchantId
      })
    })
    .then(function() {
      //5.2.insert merchant category of merchantId's
      if (!MerchantCategorys) return;
      var promises = MerchantCategorys.map(function(merchantCategory) {
        if(merchantCategory.newRate===null){
          return;
        }
        return tr.queryExecute("insert into MerchantCategory (MerchantId, CategoryId, CommissionRate) values(:MerchantId, :CategoryId, :CommissionRate)", {
          MerchantId: Merchant.MerchantId,
          CategoryId: merchantCategory.categoryId,
          CommissionRate: merchantCategory.newRate
        })
      });
      return Q.all(promises);
    })
    .then(function(){
      //6.insert or update merchant images
      var promises  = [];
      if(saveMode){
        console.log("insert merchant images");
      }else{
        if(!Bale.IsTouchedMerchantImages){ // if merchant image did not do any changes, just ignore this part.
          console.log("merchant images didn't change, no need to update merchant images");
          return;
        }
        console.log("update merchant images");
        //delete merchant images of one merchant
        var deletePromise= tr.queryExecute("Delete From MerchantImage Where MerchantId=:MerchantId", {
          MerchantId: Merchant.MerchantId,
        })
        promises.push(deletePromise);
      }
      var insertPromises =  _.map(MerchantImageList, function(merchantImage){
        return tr.queryExecute("insert into MerchantImage (MerchantId, ImageTypeCode, MerchantImage, Position) values(:MerchantId, :ImageTypeCode, :MerchantImage, :Position)", {
          MerchantId: Merchant.MerchantId,
          ImageTypeCode: merchantImage.ImageTypeCode,
          MerchantImage: merchantImage.MerchantImage,
          Position: merchantImage.Position
        })
      });
      if(insertPromises) {
        promises = promises.concat(insertPromises);
      }
      return Q.all(promises);
    })
    .then(function() {
      return tr.commit();
    })
    .then(function() {
      console.log("save merchant complete");
      res.json();
    })
    .catch(function(error) {
      if(saveMode){
        res.status(500).json(cu.createErrorResult(error, "MSG_ERR_MERCHANT_SAVE"));
      }else {
        res.status(500).json(cu.createErrorResult(error, "MSG_ERR_MERCHANT_EDIT"));
      }
      return tr.rollback();
    })
    .done();
});


/**
 * view merchant profile
 * @queryParam {Merchant: Merchant Object}
 * @response
 **/
router.get('/view/profile', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function(req, res) {
  console.log("view merchant profile");
  var Results = {}; //response result object which will response to client side.
  var Bale = {};
  Bale.CultureCode = req.query.cc;
  Bale.MerchantId = req.query.merchantid;
  if (Bale.CultureCode != 'EN' && Bale.CultureCode != 'en') {
    Bale.GeoCultureCode = 'ZH';
  } else {
    Bale.GeoCultureCode = Bale.CultureCode;
  }

  var MerchantProfile = {}; //merchant profile object.
  var MerchantBrands = []; //merchant brands object array;
  var tr = new cm();
  Q.when()
    .then(function() {
      return tr.begin();
    })
    .then(function() { //1.get merchant profile
      return tr.queryOne("select mtc.MerchantTypeName, s.StatusNameInvariant, gcoC.GeoCountryName, geoC.GeoName GeoProvinceName," +
        "geoC2.GeoName GeoCityName, mc.MerchantName, mc.MerchantDesc,m.*from Merchant m " +
        "left join MerchantCulture mc on (m.MerchantId=mc.MerchantId and mc.CultureCode=:CultureCode) " +
        "left join MerchantType mt on (m.MerchantTypeId=mt.MerchantTypeId) " +
        "left join MerchantTypeCulture mtc on (mt.MerchantTypeId=mtc.MerchantTypeId and mtc.CultureCode=:CultureCode)" +
        "inner join Status s on (m.StatusId=s.StatusId) " +
        "left join GeoCountry gco on (m.GeoCountryId=gco.GeoCountryId) " +
        "left join GeoCountryCulture gcoC on (gco.GeoCountryId=gcoC.GeoCountryId and gcoC.CultureCode=:CultureCode)" +
        "left join GeoCulture geoC on (m.GeoIdProvince=geoC.GeoId and geoC.CultureCode=:GeoCultureCode)" +
        "left join GeoCulture geoC2 on (m.GeoIdCity=geoC2.GeoId and geoC2.CultureCode=:GeoCultureCode)" +
        "where m.MerchantId=:MerchantId", {
          CultureCode: Bale.CultureCode,
          GeoCultureCode: Bale.GeoCultureCode,
          MerchantId: Bale.MerchantId
        }).then(function(data) {
          if(data){
            MerchantProfile = data; //store the returned user
          }
      });
    })
    .then(function() { //2.get merchant cultrue
      return tr.queryMany("select * from MerchantCulture where MerchantId=:MerchantId",{MerchantId: Bale.MerchantId}).then(function(data) {
        var MerchantCultureList=data;
        _.forEach(MerchantCultureList, function(MerchantCulture){
          if(MerchantCulture.CultureCode==='EN'){
            MerchantProfile.MerchantNameEN = MerchantCulture.MerchantName;
            MerchantProfile.MerchantDescEN = MerchantCulture.MerchantDesc;
          }else if(MerchantCulture.CultureCode==='CHS'){
            MerchantProfile.MerchantNameCHS = MerchantCulture.MerchantName;
            MerchantProfile.MerchantDescCHS = MerchantCulture.MerchantDesc;
          }else if(MerchantCulture.CultureCode==='CHT'){
            MerchantProfile.MerchantNameCHT = MerchantCulture.MerchantName;
            MerchantProfile.MerchantDescCHT = MerchantCulture.MerchantDesc;
          }
        })
      });
    })
    .then(function() { //3.get merchant brands
      return tr.queryMany("select mb.*, bc.BrandName from MerchantBrand mb inner join BrandCulture bc on mb.BrandId = bc.BrandId and bc.CultureCode=:CultureCode where mb.MerchantId=:MerchantId", {
        MerchantId: Bale.MerchantId,
        CultureCode: Bale.CultureCode,
      }).then(function(data) {
        MerchantBrands = data;
      })
    })
    .then(function() {
      return tr.commit();
    })
    .then(function() {
      MerchantProfile.MerchantBrands = MerchantBrands;
      Results.MerchantProfile = MerchantProfile;
      res.json(Results);
      return;
    })
    .catch(function(error) {
      res.status(500).json(cu.createErrorResult(error, "MSG_ERR_MERCHANT_PROFILE_VIEW"));
      return tr.rollback();
    })
    .done();
});

module.exports = router;
