"use strict";
var express = require('express'), app = express()
var bodyParser = require('body-parser');
var secure = require('./secure.js');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var config=require('../config');
var cm = require('../lib/commonmariasql.js');

cm.poolStart(config.dbc);

process.on('SIGTERM', function () {
  console.log('Closing SIGTERM');
  cm.poolStop(config.dbc);
});

process.on ('SIGINT', function () {
  console.log('Closing SIGINT');
  cm.poolStop(config.dbc);
  process.exit()
});

secure.setup(app);

app.use(express.static( __dirname + '/../client'));

app.use(require('./scope.js'));

app.use('/api/user', require('./user.js'));
app.use('/api/auth', require('./auth.js'));
app.use('/api/image', require('./image.js'));
app.use('/api/document', require('./document.js'));
app.use('/api/reference', require('./reference.js'));
app.use('/api/inventory', require('./inventory.js'));
app.use('/api/merch', require('./merch.js'));
app.use('/api/geo', require('./geo.js'));
app.use('/api/category', require('./category.js'));
app.use('/api/product', require('./product.js'));
app.use('/api/productimage', require('./productimage.js'));
app.use('/api/brand', require('./brand.js'));
app.use('/api/color', require('./color.js'));
app.use('/api/badge', require('./badge.js'));
app.use('/api/size', require('./size.js'));
app.use('/api/elastic', require('./elastic.js'));
app.use('/api/search', require('./search.js'));
app.use('/api/terms', require('./terms.js'));
app.use('/api/resizer', require('./resizer.js'));
app.use('/api/tags', require('./tags.js'));
app.use('/api/cart', require('./cart.js'));
app.use('/api/wishlist', require('./wishlist.js'));
app.use('/api/follow', require('./follow.js'));
app.use('/api/friend', require('./friend.js'));
app.use('/api/order', require('./order.js'));
app.use('/api/address', require('./address.js'));
app.use('/api/ticket', require('./ticket.js'));
app.use('/api/view', require('./view.js'));
app.use('/api/identification', require('./identification.js'));

module.exports=app;
