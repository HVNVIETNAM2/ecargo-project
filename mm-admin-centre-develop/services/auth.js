"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var Q=require('q');
var moment = require("moment");
var path = require('path');
var _ = require('lodash');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var cs = require('../lib/commonsql.js');
var lu=require('../logic/logicuser.js');
var lum = require('../logic/logicusermessaging.js');
var la = require('../logic/logicauth.js');

var agent = require('superagent');
var request = require('superagent-promise')(agent, Q.Promise);
var fs = require('fs');
var pump = Q.denodeify(require('pump'));
var crypto = require('crypto');

router.get('/test', function (req, res) {
	var tr= new cm();
	var Reference={};
	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryMany("select * from StatusCulture").then(function(data) {
			Reference.SecurityGroupList=data;
		})
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Reference);
		return;
	})
	.catch(function(err){
		cu.createErrorResult(err,"MSG_ERR_TEST_FAIL");
		return tr.rollback();
	})
	.done();

});

/*
router.get('/spawn', function (req, res) {
	var tr= new cm();
	var Reference={};
	var row={UserId:1,FirstName:'Albert',LastName:'Sipros'};
	console.log(cs.save(row,'User'));

	var main = function*(){
		try {
			yield tr.begin();
			Reference.SecurityGroupList=yield tr.queryMany("select * from StatusCulture")
			yield tr.commit();
			res.json(Reference);
			console.log("Finished!");
		}
		catch(err) {
			cu.createErrorResult(err,"MSG_ERR_SPAWN_FAIL")
			console.log('Rollback');
			console.dir(err);
			return tr.rollback();
		}
	};
	Q.spawn(main);
});
*/

router.post('/login', function(req, res, next) {
  passport.authenticate('local', function (err, user, info) {
  	var error = err || info;
	if (error)
	{
		var tr = new cm();
		Q.when()
		// Now we do not lock user accounts or show login attempts.
		/*
		.then(function(){
			if (error.AppCode=='MSG_ERR_USER_ATTEMPTS_EXCEEDED')
			{
				var User=error.User;
				return tr.begin().then(function () {
					return lum.sendUserMessage(User, lum.messageTemplateCodes.SIGNIN_ATTEMPTS_EXCEEDED, tr);
				}).then(function () {
					return tr.commit();
				});
			}
		})
		*/
		.then(function(){
			if (info && info.message === 'Missing credentials') {
				throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message: info.message};
			}

			//return res.status(500).json(cu.createErrorResult(error,"MSG_ERR_USER_LOGIN_FAIL"));
			var errRes = cu.createErrorResult(error, "MSG_ERR_USER_LOGIN_FAIL");
			//errRes.Username=error.Username;
			//errRes.LoginAttempts = error.LoginAttempts;
			errRes.Message = error.User.Message;
      errRes.IsMobile = error.IsMobile;
			//delete error.User; //remove User object so it is not returned in the response
			return res.status(500).json(errRes);
		}).catch(function (err) {
			res.status(500).json(cu.createErrorResult(err, "MSG_ERR_USER_LOGIN_FAIL"));
			return tr.rollback();
		}).done();
	}
	else
	{
		//line below shoudl not ever occur but retianed in case as a defensive measure.
		//if (!user) return res.status(500).json({AppCode: 'MSG_ERR_USER_AUTHENTICATION_EMPTY'});
		var retUser={};
		retUser.iss=user.UserKey;
		retUser.UserId=user.UserId;
		retUser.UserKey=user.UserKey;
		retUser.MerchantId=user.MerchantId;
		//console.log(user);
		var token=cc.createJwtToken(retUser);
		res.json({Token: token,UserId:retUser.UserId, UserKey:retUser.UserKey, MerchantId: retUser.MerchantId});
	}

  })(req, res, next)
});

router.post('/login/wechat', function (req, res) {
  var Bale = req.body;
  var WeChatExists = false;
  var UserExists = false;
  var WeChat = {};
  var User = {};
	var tr = new cm();
  var IsSignup = false;

  Q.when()
  .then(function () {
    // for dev puporse return access token via refresh token
    if (Bale.RefreshToken) {
      return request.get('https://api.weixin.qq.com/sns/oauth2/refresh_token').query({
        appid: config.wechat.appid,
        refresh_token: Bale.RefreshToken,
        grant_type: 'refresh_token'
      }).end()
    }

    if (cu.isBlank(Bale.AuthorizationCode))
      throw new Error("Missing WeChat Authorization Code.");

    return request.get('https://api.weixin.qq.com/sns/oauth2/access_token').query({
      appid: config.wechat.appid,
      secret: config.wechat.secret,
      code: Bale.AuthorizationCode,
      grant_type: 'authorization_code'
    }).end()
  })
  .then(function (data) {
    var result = JSON.parse(data.text);
    if (result.errcode) throw result;
    WeChat.OpenId = result.openid;
    WeChat.AccessToken = result.access_token;
    WeChat.RefreshToken = result.refresh_token;

    return tr.begin();
  })
  .then(function () {
    // check if WeChat with User exists
    return tr.queryOne('SELECT WeChat.*, User.UserId FROM WeChat LEFT JOIN User ON WeChat.WeChatId = User.WeChatId WHERE OpenId = :OpenId', WeChat);
  })
  .then(function (data) {
    UserExists = !!(data && data.UserId);
    WeChatExists = !!(data && data.WeChatId);

    if (WeChatExists) {
      WeChat.WeChatId = data.WeChatId;
      User.WeChatId = data.WeChatId;
    }

    if (UserExists) {
      User.UserId = data.UserId;
    } else {
      // User not exist, get User Info
      User.UserTypeId = CONSTANTS.USER_TYPE.CONSUMER;
      User.StatusId = CONSTANTS.STATUS.ACTIVE;
      User.TimeZoneId = 1; // Beijing TIme;
      User.Email = ''; // no email for WeChat login

      return request.get('https://api.weixin.qq.com/sns/userinfo').query({
        access_token: WeChat.AccessToken,
        openid: WeChat.OpenId
      }).end().then(function (data) {
        var result = JSON.parse(data.text);
        if (result.errcode) throw result;
        var langMap = {
          'en': 1, // EN
          'zh_CN': 2, // CHS
          'zh_TW': 3 // CHT
        };
        User.DisplayName = result.nickname;
        User.LanguageId = langMap[result.language] || 2;
        User.ProfileImage = null;
        User.Gender = null;
        if (result.sex == 1) User.Gender = 'M';
        if (result.sex == 2) User.Gender = 'F';
        WeChat.UnionId = result.unionid;

        // save profile image, retry 2 times
        function downloadImage (retry) {
          retry = retry || 0;
          if (retry >= 2) {
            // should not block sign up if save profile image fail
            User.ProfileImage = null;
          } else {
            // have to do this since multer does not expose a writeStream
            return pump(
              agent.get(result.headimgurl), // readStream
              fs.createWriteStream(
                path.join(__dirname, '../uploads/userimages', User.ProfileImage)
              )
            ).catch(function (err) {
              return downloadImage(retry + 1)
            });
          }
        }
        return Q.when()
        .then(function () {
          return tr.queryOne(
            "SELECT * FROM GeoCountry WHERE CountryCode = :country", result
          ).then(function (data) {
            if (data) User.GeoCountryId = data.GeoCountryId;
          });
        })
        .then(function () {
          if (!cu.isBlank(result.headimgurl)) {
            User.ProfileImage = crypto.pseudoRandomBytes(16).toString('hex');
            return downloadImage();
          }
        });
      });
    }
  })
  .then(function () {
    if (WeChatExists) {
      return tr.queryExecute('UPDATE WeChat SET AccessToken=:AccessToken, RefreshToken=:RefreshToken WHERE WeChatId=:WeChatId', WeChat);
    } else {
      return tr.queryExecute('INSERT INTO `WeChat` (`OpenId`, `UnionId`, `AccessToken`, `RefreshToken`) VALUES (:OpenId, :UnionId, :AccessToken, :RefreshToken)', WeChat).then(function (data) {
        // User WeChatId
        User.WeChatId = data.insertId;
        WeChat.WeChatId = data.insertId;
      });
    }
  })
  .then(function () {
    if (!UserExists) {
      return tr.queryExecute("insert into User (UserKey, UserName, WeChatId, UserTypeId, DisplayName, Email, LanguageId, Gender, TimeZoneId, ProfileImage, GeoCountryId, StatusId, LastStatus) values (uuid(), UserKey, :WeChatId,:UserTypeId,:DisplayName,:Email,:LanguageId,:Gender,:TimeZoneId,:ProfileImage,:GeoCountryId, :StatusId, UTC_TIMESTAMP());", User).then(function (data) {
        User.UserId = data.insertId;
      });
    }
  })
  .then(function () {
    return lu.getUser(tr, User.UserId).then(function(data) {
      User = data;
    });
  })
  .then(function() {
    return tr.commit();
  })
  .then(function () {
    // Login Token
    var retUser={};
    retUser.iss=User.UserKey;
    retUser.UserId=User.UserId;
    retUser.UserKey=User.UserKey;
    var token=cc.createJwtToken(retUser);
    res.json({
      Token: token,UserId:retUser.UserId,
      UserKey:retUser.UserKey,
      IsSignup: !UserExists
    });
  })
  .catch(function(err){
    res.status(500).json(cu.createErrorResult(err,"MSG_ERR_WECHAT_LOGIN_FAIL"));
    return tr.rollback();
  });
})

/**
 * a pre-check for landing a registration / reset password
 */
router.post('/token/validate', function(req, res) {
	var ActivationToken = req.body.ActivationToken;
	var tr = new cm();

	Q.when()
		.then(function(){
			if (cu.isBlank(req.body.ActivationToken))
				throw new Error("Missing ActivationToken");
		})
		.then(function(){
			return tr.begin();
		})
		.then(function() {
			return la.getUserByToken(ActivationToken, tr);
		})
		.then(function() {
			return tr.commit();
		})
		.then(function() {
			res.json(true);
		})
		.catch(function(err){
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_ACTIVATION_FAIL"));
			return tr.rollback();
		})
		.done();
});


router.post('/activate', function(req, res, next) {
	//this is teh initial activation after a user account is created, where the user will set their password

	var ActivationToken=req.body.ActivationToken;
	var tr = new cm();
	var User={};

	Q.when()
	.then(function(){
		if (cu.isBlank(req.body.ActivationToken) || cu.isBlank(req.body.Password))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return la.getUserByToken(ActivationToken, tr);
	})
    .then(function (data) {
        return User = data;
    })
	.then(function() {
		User.Salt=cc.createSalt();
		User.Hash=cc.createHash(req.body.Password, User.Salt);
                var params = {
                    Salt: User.Salt,
                    Hash: User.Hash,
                    UserId: User.UserId
                };
		return tr.queryExecute("update User set Salt=:Salt, Hash=:Hash, ActivationToken=null, LoginAttempts=0, StatusId=2, StatusReasonCode=null, LastStatus=UTC_TIMESTAMP() where UserId=:UserId;",params);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_ACTIVATION_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/code/activate', function(req, res, next) {
	//this is teh initial activation after a user account is created, where the user will set their password
	var Bale={};
	Bale.ActivationToken=req.body.ActivationToken;
	Bale.UserKey=req.body.UserKey;
	Bale.Password=req.body.Password;

	var tr = new cm();
	var User={};
	var client=null;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.ActivationToken) || cu.isBlank(Bale.UserKey)  || cu.isBlank(Bale.Password))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryOne("select * from User u where (CONCAT(u.MobileCode,u.MobileNumber)=:UserKey or u.Email=:UserKey) and u.StatusId <> 1",Bale).then(function(data) {
			if (data==null)
			{
				throw {AppCode:"MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND"};
			}
			else
			{
				User=data
				if (User.StatusId!=CONSTANTS.STATUS.PENDING) //can only activate a pending account
					throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
				if (User.ActivationToken!=Bale.ActivationToken)
					throw {AppCode:"MSG_ERR_USER_ACTIVATION_TOKEN_INVALID"};
				la.validateUserActivationToken(User); //checks the token stored against the user for expiry etc
			}
		})
	})
	.then(function() {
		Bale.UserId=User.UserId;
		Bale.Salt=cc.createSalt();
		Bale.Hash=cc.createHash(Bale.Password, Bale.Salt);
		return tr.queryExecute("update User set Salt=:Salt, Hash=:Hash, ActivationToken=null, LoginAttempts=0, StatusId=2, StatusReasonCode=null, LastLogin=UTC_TIMESTAMP(), LastStatus=UTC_TIMESTAMP() where UserId=:UserId;",Bale).then(function(data) {
			return;
		})
	})
	.then(function(){
		return lu.getUser(tr, Bale.UserId).then(function(data) {
			User=data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		//res.json(true);

		var retUser={};
    retUser.iss=user.UserKey;
    retUser.UserId=user.UserId;
    retUser.UserKey=user.UserKey;
		var token=cc.createJwtToken(retUser);
    res.json({Token: token,UserId:retUser.UserId, UserKey:retUser.UserKey});
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_ACTIVATION_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/code/resend', function (req, res) {
	var tr= new cm();
	var User=null;
	var Bale={};
	Bale.UserKey=req.body.UserKey;
	var LinkRequested=req.body.Link;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryOne("select * from User u where (CONCAT(u.MobileCode,u.MobileNumber)=:UserKey or u.Email=:UserKey) and u.StatusId<>1",Bale).then(function(data) {
			if (data==null)
				throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
			else
			{
				User=data;
				if (User.StatusId!=CONSTANTS.STATUS.PENDING) //can only resnd a pending account
					throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
			}
		})
	})
	.then(function() {
		//uncommnet next line to create  new token, else it will send the existing token (which i thin kis better?)
		Bale.ActivationToken = cc.createMobileCode();
		Bale.UserId=User.UserId;
		//reset last staus date to make the token valid for longer
		return tr.queryExecute("update User set LastStatus=UTC_TIMESTAMP(), ActivationToken=:ActivationToken where UserId=:UserId;",Bale).then(function(data) {
			return;
		})
	})
	.then(function(){
		return lu.getUser(tr, Bale.UserId).then(function(data) {
			User=data;
		});
	})
	.then(function(){
		return lum.sendUserMessage(User, lum.messageTemplateCodes.CHANGE_MOBILE, tr).then();
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_CODE_RESEND_FAIL"));
		return tr.rollback();

	})
	.done();
});

router.post('/code/validate', function(req, res, next) {
	//reactive a login if it was locked from for exmaple two many login attempts
	//requires an activation code which woudl have been emailed to the user as part of unique link
	//also used to confirm a new email address etc
	var User=null;
	var Bale={};
	Bale.ActivationToken=req.body.ActivationToken;
	Bale.UserKey=req.body.UserKey;

	var tr = new cm();
	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.ActivationToken) || cu.isBlank(Bale.UserKey))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryOne("select * from User u where (CONCAT(u.MobileCode,u.MobileNumber)=:UserKey or u.Email=:UserKey) and u.StatusId <> 1",Bale).then(function(data) {
			User=data;
			if (User==null)
				throw {AppCode:"MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND"};
			return la.validateUserActivationTokenCode(User,Bale.ActivationToken, tr); //checks the token stored against the user for expiry etc
		})
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_CODE_VALIDATE_FAIL"));
		return tr.rollback();

	})
	.done();
});

router.post('/code/reactivate', function(req, res, next) {
	//reactive a login if it was locked from for exmaple two many login attempts
	//requires an activation code which woudl have been emailed to the user as part of unique link
	//also used to confirm a new email address etc
	var User=null;
	var Bale={};
	Bale.ActivationToken=req.body.ActivationToken;
	Bale.UserKey=req.body.UserKey;

	var tr = new cm();
	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.ActivationToken) || cu.isBlank(Bale.UserKey))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryOne("select * from User u where (CONCAT(u.MobileCode,u.MobileNumber)=:UserKey or u.Email=:UserKey) and u.StatusId <> 1",Bale).then(function(data) {
			if (data==null)
			{
				throw {AppCode:"MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND"};
			}
			else
			{
				User=data
				if (User.StatusId!=CONSTANTS.STATUS.PENDING) //can only activate a pending account
					throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
				if (User.ActivationToken!=Bale.ActivationToken)
					throw {AppCode:"MSG_ERR_USER_ACTIVATION_TOKEN_INVALID"};
				la.validateUserActivationToken(User); //checks the token stored against the user for expiry etc
			}
		})
	})
	.then(function() {
		Bale.UserId=User.UserId;
		return tr.queryExecute("update User set StatusId=2, StatusReasonCode=null, LastStatus=UTC_TIMESTAMP(), ActivationToken=null, LoginAttempts=0 where UserId=:UserId;",Bale).then(function(data) {
			return;
		})
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_CODE_REACTIVATE_FAIL"));
		return tr.rollback();

	})
	.done();
});



router.post('/reactivate', function(req, res, next) {
	//reactive a login if it was locked from for exmaple two many login attempts
	//requires an activation code which woudl have been emailed to the user as part of unique link
	//also used to confirm a new email address etc
	var ActivationToken=req.body.ActivationToken;
	var tr = new cm();
	var User={};
	var client=null;

	if (cu.isBlank(req.body.ActivationToken))
		return res.status(500).json(cu.createErrorResult(new Error("token is required"),"MSG_ERR_USER_REQUIRED_FIELD_MISSING"));

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryOne("select * from User where ActivationToken=:ActivationToken",{ActivationToken:req.body.ActivationToken}).then(function(data) {
			if (data==null)
			{
				throw {AppCode:"MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND"};
			}
			else
			{
				User=data;
				la.validateUserActivationToken(User); //checks the token stored against the user for expiry etc
			}
		})
	})
	.then(function() {
                var params = {
                    UserId: User.UserId
                };
		return tr.queryExecute("update User set StatusId=2, StatusReasonCode=null, LastStatus=UTC_TIMESTAMP(), ActivationToken=null, LoginAttempts=0 where UserId=:UserId;",params).then(function(data) {
			return;
		})
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_REACTIVATION_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/validate', function(req, res, next) {
	//Checks to see if an activation token is valid.
	var ActivationToken=req.body.ActivationToken;
	var tr = new cm();
	var User={};
	var client=null;

	Q.when()
	.then(function(){
		if (cu.isBlank(req.body.ActivationToken))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return tr.queryOne("select * from User where ActivationToken=:ActivationToken",{ActivationToken:req.body.ActivationToken}).then(function(data) {
			if (data==null)
			{
				throw {AppCode:"MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND"};
			}
			else
			{
				User=data;
				la.validateUserActivationToken(User); //checks the token stored against the user for expiry etc
			}
		});
	})
	.then(function(){
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_ACTIVATION_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/password/forgot', function (req, res) {
	var tr = new cm();
	var User={};
	if (req.body.Email)
		User.UserKey=req.body.Email;
	else
		User.UserKey=req.body.UserKey;

	Q.when()
	.then(function(){
		if (cu.isBlank(User.UserKey))
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		//only active user can use forgot password
		return tr.queryOne("select u.UserId from User u where (u.Email=:UserKey or CONCAT(u.MobileCode,u.MobileNumber)=:UserKey) AND u.StatusId = 2",User).then(function(data) {
			if (data)
				User=data;
			else
				throw {AppCode:"MSG_ERR_USER_NOT_FOUND"};
		})
	})
	.then(function() {
		return lu.getUser(tr, User.UserId).then(function(data) {
			if (data)
			{
				User=data;
				if (User.StatusId==CONSTANTS.STATUS.INACTIVE || User.StatusId==CONSTANTS.STATUS.DELETED) //don't perform if status is deleted or inactive
					throw {AppCode:"MSG_ERR_USER_STATUS_INVALID"};
			}
			else
			{
				throw {AppCode:"MSG_ERR_USER_NOT_FOUND"};
			}
		});
	})
	.then(function() {
		if (lu.isMobileUser(User))
			User.ActivationToken = cc.createMobileCode();
		else
			User.ActivationToken = cc.createSimpleToken();

		User.SendMessageFlag=true;

                var params = {
                    ActivationToken: User.ActivationToken,
                    UserId: User.UserId
                };

		return tr.queryExecute("update User set StatusId=3,StatusReasonCode='RC_RESET_PASSWORD', LastStatus=UTC_TIMESTAMP(), Hash=null, Salt=null, ActivationToken=:ActivationToken where UserId=:UserId;",params).then(function(data) {
			return;
		})

	})
	.then(function() {
		if (User.SendMessageFlag==true)
		{
			return lum.sendUserMessage(User, lum.messageTemplateCodes.FORGOT_PASSWORD, tr).then();
		}
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_STATUS_FAIL"));
		return tr.rollback();

	})
	.done();
});

router.get('/form', function(req, res){
    res.sendFile(path.resolve(__dirname + '/../views/loginForm.html'));
});

router.post('/mobileverification/send', function (req, res) {
  var MobileVerification;
	var tr = new cm();

  tr.begin()
  .then(function () {
    return lu.saveMobileVerification(req.body, tr);
  })
  .then(function (result) {
    MobileVerification = result;
    var cc = req.body.CultureCode || req.query.cc;
    return lum.sendVerificationSms(result, cc, tr);
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json({
      MobileVerificationId: MobileVerification.MobileVerificationId
    });
  })
  .catch(function (err) {
		res.status(500).json(cu.createErrorResult(err, 'MSG_ERR_MOBILE_VERIFICATION'));
		return tr.rollback();
  });
});

router.post('/mobileverification/send/test', function (req, res) {
  var MobileVerification;
  var tr = new cm();

  tr.begin()
  .then(function () {
    return lu.saveMobileVerification(req.body, tr);
  })
  .then(function (result) {
    MobileVerification = result;
    console.log(MobileVerification);
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json({
      MobileVerificationId: MobileVerification.MobileVerificationId
    });
  })
  .catch(function (err) {
		res.status(500).json(cu.createErrorResult(err, 'MSG_ERR_MOBILE_VERIFICATION'));
		return tr.rollback();
  });
});

router.post('/mobileverification/check', function (req, res) {
	var tr = new cm();
  var code = 500;

  tr.begin()
  .then(function () {
    return lu.checkMobileVerification(req.body, tr);
  })
  .then(function (data) {
    res.json(true);
    return tr.commit();
  })
  .catch(function (err) {
    if (err && err.AppCode === 'LB_CA_VERCODE_INVALID') {
      // commit attempt if vercode error
      code = 520;
      res.status(code).json(cu.createErrorResult(err, "LB_CA_VERCODE_INVALID"));
      return tr.commit();
    } else {
      res.status(code).json(cu.createErrorResult(err, "MSG_ERR_MOBILE_VERIFICATION"));
      return tr.rollback();
    }
  });
});

// consumer signup
router.post('/signup', function (req, res, next) {
  var tr = new cm();
  var Bale = _.clone(req.body);
  var User;

  Q.when().then(function(){
    if (cu.isBlank(Bale.Password)) {
      throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
    }

    Bale.UserId = 0; // insert
    Bale.UserTypeId = CONSTANTS.USER_TYPE.CONSUMER;
    Bale.StatusId = CONSTANTS.STATUS.ACTIVE;
    // todo let user select
    Bale.LanguageId = Bale.LanguageId || 2; // CHS;
    Bale.TimeZoneId = Bale.TimeZoneId || 1; // Beijing TIme;
    // Optional
    Bale.FirstName = Bale.FirstName || '';
    Bale.LastName = Bale.LastName || '';
    Bale.MiddleName = Bale.MiddleName || '';

    return tr.begin();
  })
  .then(function () {
    // mobile verification check
    return lu.checkMobileVerification(Bale, tr);
  })
  .then(function () {
    // mandatary mobile
    return lu.checkUserMobile(Bale, tr);
  })
  .then(function () {
    // mandatary username
    return lu.checkUserUserName(Bale, tr);
  })
  .then(function () {
    // optional email
    if (cu.isBlank(Bale.Email)) {
      Bale.Email = '';
    } else {
      return lu.checkUserEmail(Bale, tr);
    }
  })
  .then(function () {
    // Password hash
    Bale.Salt = cc.createSalt();
    Bale.Hash = cc.createHash(Bale.Password, Bale.Salt);

    return tr.queryExecute("insert into User (UserKey, UserName, UserTypeId,FirstName,LastName,MiddleName,DisplayName,Email,MobileCode,MobileNumber,LanguageId,TimeZoneId,ProfileImage,StatusId, Hash, Salt,LastStatus) values (uuid(), :UserName, :UserTypeId,:FirstName,:LastName,:MiddleName,:DisplayName,:Email,:MobileCode,:MobileNumber,:LanguageId,:TimeZoneId,:ProfileImage,:StatusId, :Hash, :Salt, UTC_TIMESTAMP());",Bale)
    .then(function (data) {
      return lu.getUser(tr, data.insertId).then(function(data) {
        User=data;
      });
    })
  })
  .then(function () {
    // clear verification check if success
    return lu.resetMobileVerification(Bale, tr);
  })
  .then(function() {
    return tr.commit();
  })
  .then(function() {
    // Login Token
    var retUser={};
    retUser.iss=User.UserKey;
    retUser.UserId=User.UserId;
    retUser.UserKey=User.UserKey;
    var token=cc.createJwtToken(retUser);
    res.json({Token: token,UserId:retUser.UserId, UserKey:retUser.UserKey});
  })
  .catch(function(err){
    res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_SAVE_FAIL"));
    tr.rollback();
  });
});

router.post('/passwordreset', function(req, res, next){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.MobileVerificationId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "MobileVerificationId" };
		if (cu.isBlank(Bale.MobileVerificationToken))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "MobileVerificationToken" };
		if (cu.isBlank(Bale.MobileCode))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "MobileCode" };
		if (cu.isBlank(Bale.MobileNumber))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "MobileNumber" };
		if (cu.isBlank(Bale.Password))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "Password" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		var sql = "SELECT * FROM User WHERE MobileCode=:MobileCode and MobileNumber=:MobileNumber";
		return tr.queryOne(sql, Bale).then(function(data){
			if(!data){
				throw { AppCode : "MSG_ERR_USER_NOT_FOUND" };
			}else{
				Bale.UserId = data.UserId;
			}
		});
	})
	.then(function(){
		return lu.checkMobileVerification(Bale, tr);
	})
  .then(function(){
    return lu.updateUserPassword(Bale, tr);
  })
  .then(function () {
    // reset verification check if success
    return lu.resetMobileVerification(Bale, tr);
  })
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.send(true);
	})
	.catch(function(err){
    res.status(500).json(cu.createErrorResult(err,"MSG_ERR_PASSWORD_RESET_FAIL"));
    if (err && err.AppCode === 'LB_CA_VERCODE_INVALID') {
      // commit attempt if vercode error
      return tr.commit();
    } else {
      return tr.rollback();
    }
	})
	.done();
});

module.exports = router;
