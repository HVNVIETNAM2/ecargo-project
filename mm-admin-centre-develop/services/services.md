Note: Always post with Content-Type='application/json' for any JSON services

******Please also see the /test folder for exmaples of how to call other services ********


***Password reset****
http://192.168.33.33:3000/user/password/change
post
{"UserId":349}

***email change***
http://192.168.33.33:3000/user/email/change
post
{"UserId":349,"Email":"albertdallow@ecargo.com"}

Email is the new email

***Status Change***
http://192.168.33.33:3000/user/status/change
post
{"UserId":349,"Status":"Active"}

Status field options are "Active" or "Inactive" or "Deleted"

*** Resend Links ***
http://192.168.33.33:3000/user/resend/link
post
{"UserId":349,"Link":"Email"}

"Link" options are "Email" or "Password" or "Registration"

*** Login ***
http://192.168.33.33:3000/auth/login
Due to how passportJS works you must post as a non json post, so as "Content-Type" = "application/x-www-form-urlencoded; charset=utf-8"

regular form post
Username=blah
Password=blahblah

*** Activate ***
http://192.168.33.33:3000/auth/activate
post
{"ActivationToken":"ec94eb008f59df9e014b939a9719bb1c","Password":"Blah"}

The activation token is teh token that came in the email
Password is teh users new password

*** Password forgot ***
http://192.168.33.33:3000/auth/password/forgot
post
{"Email":"Blah@blah.com""}

Email of the user to trigger the password reset function (which covers forgotten password).
Note: I think this service has a problem with the flow but it is as per the spec, need to revisit with product.

*** Validate Activation Token ***
This service validates an activation token that was sent in an email link to make sure it hasn't expired
http://192.168.33.33:3000/auth/validate
post
{"ActivationToken":"9f4e64a977b9dfb5db9707a4140ef3a2"}

*** Reactive a users account with token ***
Reactive a login if it was locked from for exmaple two many login attempts.
Requires an activetion code which woudl have been emailed to the user as part of unique link.
http://192.168.33.33:3000/auth/reactivate
post
{"ActivationToken":"9f4e64a977b9dfb5db9707a4140ef3a2"}

*** Create Inventory Proper ***
This service will record an amount of inventory at a location for a SKU
http://192.168.33.33:3000/api/inventory/save
post
{InventoryId:0,InventoryLocationId:2,SkuId:Sku.SkuId,IsPerpetual:0,QtyAllocated:54,QtyExported:0,QtyOrdered:0}

