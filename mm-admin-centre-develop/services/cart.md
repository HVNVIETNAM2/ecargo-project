Note: Always post with Content-Type='application/json' for any JSON services

******Please also see the /test folder for examples of how to call other services ********


***Cart Create****
http://192.168.33.33:3000/api/cart/create
POST
{"CultureCode":"CHS","UserId":349} //for authenticated
OR
{"CultureCode":"CHS","UserId":0} //for unauthenticated

-> Returns Cart Object (inc CartId) Containing Cart and CartItems Grouped By Merchant (view via browser post tool)

***Cart User Update****
http://192.168.33.33:3000/api/cart/user/update
POST
{"CultureCode":"CHS","CartId":2,"UserId":349} 

-> Returns Cart Object (inc CartId) Containing Cart and CartItems Grouped By Merchant (view via browser post tool)

***Cart Item Add****
http://192.168.33.33:3000/api/cart/item/add
POST
{"CultureCode":"CHS","CartId":2,"SkuId":518,"Qty":3} 

-> Returns Cart Object (inc CartId) Containing Cart and CartItems Grouped By Merchant (view via browser post tool)

***Cart Item Remove****
http://192.168.33.33:3000/api/cart/item/remove
POST
{"CultureCode":"CHS","CartId":2,"CartItemId":1} 

-> Returns Cart Object (inc CartId) Containing Cart and CartItems Grouped By Merchant (view via browser post tool)

***Cart Item Update****
http://192.168.33.33:3000/api/cart/item/update
POST
{"CultureCode":"CHS","CartId":2,"CartItemId":2,"SkuId":519,Qty:6} 

-> Returns Cart Object (inc CartId) Containing Cart and CartItems Grouped By Merchant (view via browser post tool)

***Cart Item Update****
http://192.168.33.33:3000/api/cart/item/update
POST
{"CultureCode":"CHS","CartId":2,"CartItemId":2,"SkuId":519,Qty:6} 

-> Returns Cart Object (inc CartId) Containing Cart and CartItems Grouped By Merchant (view via browser post tool)

***Cart View By UserId****
http://192.168.33.33:3000/api/cart/view/user?cc=EN&userid=349
GET 

-> Returns Cart Object (inc CartId) Containing Cart and CartItems Grouped By Merchant (view via browser post tool)

***Cart View By CartId****
http://192.168.33.33:3000/api/cart/view?cc=EN&cartid=2
GET

-> Returns Cart Object (inc CartId) Containing Cart and CartItems Grouped By Merchant (view via browser post tool)

***Merge Cart****
http://192.168.33.33:3000/api/cart/merge
POST
{"CultureCode":"CHS","CartId":1,"MergeId":2} 

-> MergeId [2] is the CartId of the Cart you want to merge into existing Cart CartId [1]

-> In this example CartItems of CartId=2 will be merge to CartId=1, CartId=2 will be remove on the Cart List.

-> Returns Cart Object (inc CartId) Containing Cart and CartItems Grouped By Merchant (view via browser post tool)

***Add CartItem to Wish List****
http://192.168.33.33:3000/api/cart/item/add/wishlist
POST
{"CultureCode":"CHS","CartId":1,"CartItemId":2} 

-> Returns Cart Object (inc CartId) Containing Cart and CartItems Grouped By Merchant (view via browser post tool)
