"use strict";
var express = require('express'), router = express.Router();
var path = require('path');
var Q = require('q');
var throat = require('throat');

var secure = require('./secure.js');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var lp = require('../logic/logicproduct.js');
var lt = require('../logic/logicterm.js');
var lcart = require('../logic/logiccart.js');
var lcat = require('../logic/logiccategory.js');
var secure = require('./secure.js');
	
var getInfo = Q.async(function* (tt) {
	var myres=yield tt.info();
	return myres;
});

router.post('/create', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.UserKey))
				throw Error("UserKey is required. Please specify 0 for unauthenticated cart.");
			yield tr.begin();
			var ret=yield lcart.create(tr,Bale.CultureCode,Bale.UserKey);
			yield tr.commit();
			res.json(ret);			
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_CART"));			
		}
	};
	Q.spawn(main);	
});

router.post('/user/update', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				throw Error("CartKey is required.");
			if (cu.isBlank(Bale.UserKey))
				throw Error("UserKey is required.");
			yield tr.begin();
			var ret=yield lcart.userUpdate(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_CART"));			
		}
	};
	Q.spawn(main);	
});

router.post('/item/add', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				Bale.CartKey = null;
			if(cu.isBlank(Bale.UserKey))
				Bale.UserKey = null;
			// if( !Bale.UserKey && !Bale.CartKey )
			// 	throw Error('CartyKey/UserKey is required.');
			if (cu.checkIdBlank(Bale.SkuId))
				throw Error("SkuId is required.");
			if (cu.isBlank(Bale.Qty))
				throw Error("Qty is required.");
			yield tr.begin();
			var ret=yield lcart.itemAdd(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey,Bale.SkuId,Bale.Qty);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_CART"));			
		}
	};
	Q.spawn(main);	
});

router.post('/item/remove', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				Bale.CartKey = null;
			if(cu.isBlank(Bale.UserKey))
				Bale.UserKey = null;
			if( !Bale.UserKey && !Bale.CartKey )
				throw Error('CartyKey/UserKey is required.');
			if (cu.checkIdBlank(Bale.CartItemId))
				throw Error("CartItemId is required.");
			yield tr.begin();
			var ret=yield lcart.itemRemove(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey,Bale.CartItemId);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_CART"));			
		}
	};
	Q.spawn(main);	
});

router.post('/item/update', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				Bale.CartKey = null;
			if(cu.isBlank(Bale.UserKey))
				Bale.UserKey = null;
			if( !Bale.UserKey && !Bale.CartKey )
				throw Error('CartyKey/UserKey is required.');
			if (cu.checkIdBlank(Bale.CartItemId))
				throw Error("CartItemId is required.");
			if (cu.checkIdBlank(Bale.SkuId) && cu.isBlank(Bale.Qty))
				throw Error("Either a SkuId or a Qty or both are required.");
			yield tr.begin();
			var ret=yield lcart.itemUpdate(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey,Bale.CartItemId,Bale.SkuId,Bale.Qty);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_CART"));			
		}
	};
	Q.spawn(main);	
});

router.get('/view/user', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale.CultureCode=req.query.cc;
			Bale.UserKey=req.query.userkey;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.UserKey))
				throw Error("UserKey is required.");
			yield tr.begin();
			var ret=yield lcart.viewByUserKey(tr,Bale.CultureCode,Bale.UserKey);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_CART"));			
		}
	};
	Q.spawn(main);	
});

router.get('/view', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale.CultureCode=req.query.cc;
			Bale.CartKey=req.query.cartkey;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				throw Error("CartKey is required.");
			yield tr.begin();
			var ret=yield lcart.viewByCartKey(tr,Bale.CultureCode,Bale.CartKey);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_CART"));			
		}
	};
	Q.spawn(main);	
});


router.post('/merge', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.isBlank(Bale.CartKey))
				Bale.CartKey = null;
			if(cu.isBlank(Bale.UserKey))
				Bale.UserKey = null;
			if( !Bale.UserKey && !Bale.CartKey )
				throw Error('CartyKey/UserKey is required.');
			if (cu.checkIdBlank(Bale.MergeCartKey))
				throw Error("MergeCartKey is required.");
			if( Bale.CartKey == Bale.MergeCartKey )
				throw Error("MergeCartKey and CartKey should not be the same.");

			yield tr.begin();
			var ret=yield lcart.mergeCarts(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey,Bale.MergeCartKey);
			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_CART"));			
		}
	};
	Q.spawn(main);	
});

router.post('/item/:mode/wishlist', function (req, res) {
	var main = function*(){
		try {
			var Bale={};
			var tr = new cm();
			Bale=req.body;
			if (cu.isBlank(Bale.CultureCode))
				throw Error("CultureCode is required.");
			if (cu.checkIdBlank(Bale.CartItemId))
				throw Error("CartItemId is required.");

			if(cu.isBlank(Bale.UserKey)){
				Bale.UserKey = null;

				if(cu.isBlank(Bale.CartKey)){
					throw Error('CartKey is required if no UserKey');
				}

				if(cu.isBlank(Bale.WishlistKey)){
					Bale.WishlistKey = null;	
				}
				
			}else{
				Bale.WishlistKey = null;
				Bale.CartKey = null;
			}
			
		
			yield tr.begin();
			var ret=null;

			if(req.params.mode == "add"){
				ret = yield lcart.addToWishList(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey,Bale.CartItemId,Bale.WishlistKey);
			}else if(req.params.mode == "move"){
				ret = yield lcart.moveToWishList(tr,Bale.CultureCode,Bale.CartKey,Bale.UserKey,Bale.CartItemId,Bale.WishlistKey);
			}else{
				throw Error('API NOT FOUND');
			}

			yield tr.commit();
			res.json(ret);						
		}
		catch(err) {			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_CART"));			
		}
	};
	Q.spawn(main);	
});


/*
router.get('/test', function (req, res) {
	var p=Q.when()
	.then(function(){
		return lcart.viewByCartId(res.tr,'CHS',2);
	});
	res.trScope(p);
});

router.get('/test2', function (req, res) {
	res.trScope(lcart.viewByCartId(res.tr,'CHS',2));
});
*/

module.exports = router;

