"use strict";
var express = require('express'), router = express.Router();
var path = require('path');
var Q = require('q');
var throat = require('throat');
var elasticsearch = require('elasticsearch');
var agent = require('superagent-promise')(require('superagent'), Q.Promise);
var _ = require('lodash');


var secure = require('./secure.js');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var lp = require('../logic/logicproduct.js');
var lt = require('../logic/logicterm.js');
var lb = require('../logic/logicbrand.js');
var lcat = require('../logic/logiccategory.js');
var lmerch = require('../logic/logicmerch.js');
var lelas = require('../logic/logicelastic.js');
var secure = require('./secure.js');

var DEFAULT_ELASTICSEARCH_PORT = '9200';
var DEFAULT_ELASTICSEARCH_HOST = 'localhost';
var DEFAULT_ELASTICSEARCH_LOG = 'error';
var DEFAULT_MAX_RESULTS = 10000000;

var host = DEFAULT_ELASTICSEARCH_HOST;
var port =  DEFAULT_ELASTICSEARCH_PORT;
var maxResults = DEFAULT_MAX_RESULTS;

var client = new elasticsearch.Client({
	host: host.concat(':', port),
});

var indexSettings={
	"settings": {
		"analysis": {			
			"analyzer": {
				"default": {
					"type": "smartcn"
				}
			}
		}
	}
};
	
var getInfo = Q.async(function* (tt) {
	var myres=yield tt.info();
	return myres;
});

router.get('/import', function (req, res) {
	
		
	var tr = new cm();
	var CultureCode=req.query.cc;
	
		
	var main = function*(){
		try {
			var output={};
			
			
			output.Info=yield getInfo(client);
			output.MessageList=[];
			
			if (cu.isBlank(CultureCode))
				throw Error("cc required to define CultureCode");
						
			var indexExists=null;
			yield tr.begin();
			let StyleList=yield lp.styleList(0,0,'',0,'',CultureCode, 1, 100000,'',tr);
			let TermList=yield lt.listAll(tr);
			let BrandList=yield lb.list(CultureCode,null, tr);
			let BrandListCombined=yield lb.listCombined(CultureCode, tr);
			let CategoryListAdjacency=yield lcat.listAdjacency(CultureCode, false, tr);
			let CategoryList = CategoryListAdjacency.filter(function(item){ return (item.CategoryId!=0 && item.ParentCategoryId==0)	});
			let CategoryBrandMerchantList = yield lcat.listCategoryBrandMerchants(tr);
			let MerchantList=yield lmerch.list(tr,CultureCode);
			let BadgeList=yield lp.badgeList(CultureCode,tr);
			let SizeList=yield lp.sizeList(CultureCode,tr);
			let SizeGroupList=yield lp.sizeGroupList(CultureCode,tr);
			let ColorList=yield lp.colorList(CultureCode,tr);
						
			yield tr.commit();
			var indexName='mm-' + CultureCode.toLowerCase();
			output.MessageList.push("Index name is '" + indexName + "'");
			indexExists=yield client.indices.exists({index:indexName});
			if (indexExists)
			{
				yield client.indices.delete({index:indexName});
				output.MessageList.push('Index deleted');
			}
			
			indexExists=yield client.indices.exists({index:indexName});			
			if (!indexExists)
			{
				var ree=yield client.indices.create({index:indexName,body:indexSettings});
				output.MessageList.push('Index created');
			}
			
			
			//transforms
			let CategoryListFull = lcat.convertTree(CategoryListAdjacency);			
			for (let cati of CategoryListFull)
			{
				cati.CategoryBrandMerchantList=CategoryBrandMerchantList.filter(x=>x.CategoryId==cati.CategoryId);
				for (let cbm of cati.CategoryBrandMerchantList)
				{
					let rec=BrandListCombined.filter(x=>x.Entity==cbm.Entity && x.EntityId==cbm.EntityId);
					if (rec && rec.length>0)
					{
						
						cbm.Name=rec[0].Name;
						cbm.NameInvariant=rec[0].NameInvariant
						cbm.HeaderLogoImage=rec[0].HeaderLogoImage;
						cbm.SmallLogoImage=rec[0].SmallLogoImage;
						cbm.LargeLogoImage=rec[0].LargeLogoImage;
						cbm.ProfileBannerImage=rec[0].ProfileBannerImage;						
					}
				}
			}	
			
			let BrandListCombinedAdaptListed=BrandListCombined.filter(x=>x.IsListed);
			
			let TermListAdaptTerm=TermList.map(y=>(
			{
				Id: y.SearchTermTypeCode + '-' + y.SearchTermId,
				Entity:y.SearchTermTypeCode,
				EntityId:y.SearchTermId,
				EntityImage:null,
				SearchTerm:y.SearchTerm,
				SearchTermIn:y.SearchTermIn
			}));
			
			let TermListSymmoArray=TermList.filter(x=>x.SearchTermTypeCode=='Synonym').map(y=>y.SearchTerm);
			
			let BrandListCombinedAdaptTerm=BrandListCombined.filter(x=>x.IsSearchable).map(y=> y=
			{
				Id: y.Entity + '-' + y.EntityId,
				Entity:y.Entity,
				EntityId:y.EntityId,
				EntityImage:y.SmallLogoImage,
				SearchTerm:y.Name,
				SearchTermIn:y.NameInvariant
			});
							
			let CategoryListAdaptTerm=CategoryList.map(y=>y=
			{
				Id: 'Category-' + y.CategoryId,
				Entity:'Category',
				EntityId:y.CategoryId,
				EntityImage:y.CategoryImage,
				SearchTerm:y.CategoryName,
				SearchTermIn:y.CategoryNameInvariant
			});
			
			//create synoymns index
			
			if (TermListSymmoArray.length==0)
				TermListSymmoArray.push("emptor,buyer,customer"); //default synonyms
			
			var synonymIndexSettings={
				"settings": {
					"analysis": {
						"filter": {
							"mm_synonym_filter": {
							  "type": "synonym",
							  "synonyms": TermListSymmoArray
							}
						  },
						"analyzer": {
							"default": {
								"type": "smartcn"
							},
							"mm_synonyms": {
							  "tokenizer": "standard",
							  "filter": [
								"lowercase",
								"mm_synonym_filter"
							  ]
							}
						}
					}
				}
			};
	
			var synonymIndexName='mm-synonym';
			indexExists=yield client.indices.exists({index:synonymIndexName});
			if (indexExists)
			{
				yield client.indices.delete({index:synonymIndexName});
				output.MessageList.push('Synonym Index deleted');
			}
			
			indexExists=yield client.indices.exists({index:synonymIndexName});			
			if (!indexExists)
			{
				var ree=yield client.indices.create({index:synonymIndexName,body:synonymIndexSettings});
				output.MessageList.push('Synonym Index created');
			}
			
			// imports
			yield lelas.standardImport(client, indexName, 'style', StyleList.PageData, 'StyleCode',output.MessageList);
			yield lelas.standardImport(client, indexName, 'merchant', MerchantList, 'MerchantId',output.MessageList);
			yield lelas.standardImport(client, indexName, 'brand', BrandList, 'BrandId',output.MessageList);
			yield lelas.standardImport(client, indexName, 'category', CategoryListFull, 'CategoryId',output.MessageList);
			yield lelas.standardImport(client, indexName, 'badge', BadgeList, 'BadgeId',output.MessageList);
			yield lelas.standardImport(client, indexName, 'color', ColorList, 'ColorId',output.MessageList);
			yield lelas.standardImport(client, indexName, 'size', SizeList, 'SizeId',output.MessageList);
			yield lelas.standardImport(client, indexName, 'size-group', SizeGroupList, 'SizeGroupId',output.MessageList);
			
			yield lelas.standardImport(client, indexName, 'brand-combined', BrandListCombinedAdaptListed, 'EntityId',output.MessageList);
			
			yield lelas.standardImport(client, indexName, 'term', TermListAdaptTerm, 'Id',output.MessageList);
			yield lelas.standardImport(client, indexName, 'term', BrandListCombinedAdaptTerm, 'Id',output.MessageList);
			yield lelas.standardImport(client, indexName, 'term', CategoryListAdaptTerm, 'Id',output.MessageList);
			
			output.MessageList.push("Finished!");
			
			res.json(output);
			console.log
			
			
		}
		catch(err) {
			res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SPAWN_FAIL"));
			return tr.rollback();
		}
	};
	Q.spawn(main);	
		
});

module.exports = router;

