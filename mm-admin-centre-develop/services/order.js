"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var fs = require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config = require('../config');
var moment = require('moment');
var lu = require('../logic/logicuser.js');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');

var logicaddress = require('../logic/logicaddress.js');
var logicorder = require('../logic/logicorder.js');
var logicprouct = require('../logic/logicproduct.js');

router.use('/*', secure.applySecurity([
	secure.checkMMAdmin(),
	secure.checkSelf()
]));

router.post('/check', function (req, res) {
  var tr = new cm();
  var Order;
  var Bale = req.body;

  Q.when()
  .then(function () {
    if (cu.isBlank(Bale.UserKey))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
    if (cu.isBlank(Bale.UserAddressKey))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserAddressKey" };
    if (cu.isBlank(Bale.Skus))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "Skus" };
    if (cu.isBlank(Bale.CultureCode))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "CultureCode" };

    if (!Array.isArray(Bale.Skus))
      throw { AppCode: 'MSG_ERR_INVALID_FIELD', Message : "Skus" };
    if (Bale.Invoices && !Array.isArray(Bale.Invoices))
      throw { AppCode: 'MSG_ERR_INVALID_FIELD', Message : "Invoices" };
    return tr.begin();
  })
  .then(function () {
    return logicorder.checkOrder(tr, req.body)
    .then(function(data) {
			return logicorder.hideIds(data);
		})
    .then(function (data) {
      Order = data;
    });
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json(Order);
  })
  .catch(function(err){
    if (err.Errors) res.status(500).json(err);
    else res.status(500).json(cu.createErrorResult(err,"MSG_ERR_ORDER_CHECK_FAIL"));
    return tr.rollback();
  });
})

router.post('/create', function (req, res) {
	var tr = new cm();
  var Bale = req.body;
  var Order;

  Q.when()
  .then(function () {
    if (cu.isBlank(Bale.UserKey))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
    if (cu.isBlank(Bale.UserAddressKey))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserAddressKey" };
    if (cu.isBlank(Bale.Skus))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "Skus" };
    if (cu.isBlank(Bale.CultureCode))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "CultureCode" };

    if (!Array.isArray(Bale.Skus))
      throw { AppCode: 'MSG_ERR_INVALID_FIELD', Message : "Skus" };
    if (Bale.Invoices && !Array.isArray(Bale.Invoices))
      throw { AppCode: 'MSG_ERR_INVALID_FIELD', Message : "Invoices" };
    return tr.begin();
  })
  .then(function () {
    return logicorder.createOrder(tr, Bale)
    .then(function (data) {
      return logicorder.viewOrder(tr, data, 'OrderId = :OrderId')
    })
    .then(function(data) {
      return logicorder.hideIds(data);
    })
    .then(function (data) {
      Order = data;
    })
  })
  .then(function() {
    return tr.commit();
  })
  .then(function() {
    res.json(Order);
  })
  .catch(function(err){
    if (err.Errors) res.status(500).json(err);
    else res.status(500).json(cu.createErrorResult(err,"MSG_ERR_ORDER_CREATE_FAIL"));
    return tr.rollback();
  });
});

router.get('/view', function (req, res) {
  var Bale = req.query;
  var Order;
  var tr = new cm();

  Q.when().then(function () {
    if (cu.checkIdBlank(Bale.UserKey))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
    if (cu.checkIdBlank(Bale.OrderKey))
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "OrderKey" };
    return tr.begin();
  })
  .then(function () {
		return lu.getUser(tr, Bale.UserKey).then(function(data) {
			Bale.UserId = data.UserId;
		});
  })
  .then(function () {
    return logicorder.viewOrder(tr, Bale)
    .then(function(data) {
			return logicorder.hideIds(data);
		})
    .then(function (data) {
      Order = data;
    });
  })
  .then(function () {
    return tr.commit();
  })
  .then(function () {
    res.json(Order);
  })
  .catch(function (error) {
    res.status(500).json(cu.createErrorResult(error, "MSG_ERR_ORDER_VIEW"));
    return tr.rollback();
  });
});

module.exports = router;
