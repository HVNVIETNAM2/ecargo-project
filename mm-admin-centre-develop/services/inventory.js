"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var fs=require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var secure = require('./secure.js');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var multer = require('multer');
var multerUpload = multer({dest: __dirname + '/../uploads/sheets',limits:{fileSize:(CONSTANTS.MEGABYTE * 10),files:1}}).single('file');

var qfs=require("qfs-node");
var XlsxTemplate = require('../logic/logicexceltemplate.js');
var li = require('../logic/logicinventory.js');
var lp = require('../logic/logicproduct.js');
var lm = require('../logic/logicmerch.js');
var commonexcel = require('../lib/commonexcel.js');
var throat = require('throat');

router.post('/save',
	secure.applySecurity([
		secure.checkMMUser(),
		secure.checkMerchUser()
	]), function (req, res) {
	var tr= new cm();
	var Bale={};
	var Inventory=null;
	Bale=req.body;
	Bale.UserId = req.AccessUser.UserId;
        
	if (!Bale.JournalEventId)
		Bale.JournalEventId=CONSTANTS.JOURNAL_EVENT_TYPE.MANUAL_EDIT;
		
	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		if (cu.isBlank(Bale.MerchantId) || cu.isBlank(Bale.SkuId) || cu.isBlank(Bale.InventoryLocationId)) {
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
		}
	})
		.then(function(){
			return li.existsInventoryLocation(Bale.InventoryLocationId, Bale.MerchantId, tr);
		})
		.then(function(){
			return lp.existsSku(Bale.SkuId, Bale.MerchantId, tr);
		})
		.then(function(){
			//cannot create inventory when product is not active
			if (Bale.InventoryId==0)
			{
				return tr.queryOne('select SkuId from Sku where SkuId=:SkuId and StatusId=:StatusId',
					{SkuId: Bale.SkuId, StatusId: CONSTANTS.STATUS.ACTIVE}).then(function (data) {
					if (data === null) {
						throw {
							AppCode: 'MSG_ERR_PRODUCT_STATUS_INVALID'
						}
					}
				}).then(function () {
					return lm.isPendingMerchant(tr, Bale.MerchantId, Bale.StatusId);
				})
			}
		})
		.then(function(){
                // Added handling on perpetual allocation no for server side and client side column ordering
                if(Bale.IsPerpetual === "1" || Bale.IsPerpetual === 1){
                    Bale.QtyAllocated = config.inventory.perpetualAllocationNo;
                }
            
		if (Bale.InventoryId==0)
		{
			if (cu.isBlank(Bale.InventoryId) || cu.isBlank(Bale.IsPerpetual) || cu.isBlank(Bale.QtyAllocated))
				throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
			Bale.JournalTypeId=CONSTANTS.JOURNAL_TYPE.INSERT;

			//MM-2209
			return tr.queryOne('SELECT `InventoryId` FROM `Inventory` WHERE `InventoryLocationId` = :InventoryLocationId AND SkuId = :SkuId', Bale).then(function (data) {
					if (data) {
						throw { AppCode: 'MSG_ERR_INVENTORY_EXISTS' };
					} else {
						return tr.queryExecute("insert into Inventory (SkuId,InventoryLocationId,IsPerpetual,QtyAllocated,QtyExported,QtyOrdered,StatusId) values (:SkuId,:InventoryLocationId,:IsPerpetual,:QtyAllocated,0,0,2)",Bale).then(function(data) {
							Bale.InventoryId=data.insertId;
						});
					}
				});
		}
		else
		{
			Bale.JournalTypeId=CONSTANTS.JOURNAL_TYPE.UPDATE;
			if (cu.isBlank(Bale.InventoryId) || cu.isBlank(Bale.IsPerpetual) || cu.isBlank(Bale.QtyAllocated))
				throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
			//cannot edit pending and deleted inventory
			return tr.queryOne('select SkuId from Sku where SkuId=:SkuId and StatusId in (:StatusId)',
				{SkuId: Bale.SkuId, StatusId: [CONSTANTS.STATUS.PENDING, CONSTANTS.STATUS.DELETED]}).then(function (data) {
				if (data !== null) {
					throw {
						AppCode: 'MSG_ERR_PRODUCT_STATUS_INVALID'
					}
				}
			}).then(function () {
				return tr.queryExecute("update Inventory set IsPerpetual=:IsPerpetual, QtyAllocated=:QtyAllocated where InventoryId=:InventoryId",Bale);
			});
		}
	})
	.then(function() {
		//return tr.queryOne("select * from Inventory where InventoryId=:InventoryId",Bale).then(function(data) {
		return tr.queryOne("select inv.*,s.QtySafetyThreshold as QtySafetyThresholdSku, il.QtySafetyThreshold as QtySafetyThresholdLocation, (inv.QtyAllocated - inv.QtyExported - inv.QtyOrdered  - s.QtySafetyThreshold - il.QtySafetyThreshold) as QtyAts from Inventory inv inner join InventoryLocation il on (inv.InventoryLocationId = il.InventoryLocationID) inner join Sku s on (inv.SkuId=s.SkuId) where inv.InventoryId=:InventoryId",Bale).then(function(data) {
			if (data==null) throw {AppCode: 'MSG_ERR_NOT_FOUND'};
			Inventory=data; 
			Inventory.JournalTypeId=Bale.JournalTypeId;
			Inventory.JournalEventId=Bale.JournalEventId;
			Inventory.UserId=Bale.UserId 
		})
	})
	.then(function(){
		return tr.queryExecute("insert into InventoryJournal (LastModified,LastModifiedUserId,JournalTypeId,JournalEventId,InventoryId,SkuId,InventoryLocationId,IsPerpetual,QtyAllocated,QtyExported,QtyOrdered,QtySafetyThresholdSku,QtySafetyThresholdLocation,QtyAts,StatusId) values (UTC_TIMESTAMP(),:UserId,:JournalTypeId,:JournalEventId,:InventoryId,:SkuId,:InventoryLocationId,:IsPerpetual,:QtyAllocated,:QtyExported,:QtyOrdered,:QtySafetyThresholdSku,:QtySafetyThresholdLocation,:QtyAts,:StatusId)",Inventory).then(function(data) {
			Bale.InventoryJournalId=data.insertId;
		}); 
	})
        .then(function(){
                // Get updated inventory status information
                return li.getSkuInventoryInfo(Bale, tr).then(function (data) {
                   Inventory = data;
                });
	})
	.then(function() {
		console.log('super commit');
		return tr.commit();
	})
	.then(function() {
		res.json(Inventory);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_INVENTORY_FAIL"));
		return tr.rollback();
	})
	.done();
});


// List sku inventory
router.get('/list/sku', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
	var tr= new cm();
        
        var Result={};
	Result.Sku=null;
	Result.InventoryList={};
        
	var Bale={};
	Bale.MerchantId=req.query.merchantid;
	Bale.SkuId=req.query.skuid;
	Bale.CultureCode=req.query.cc;

	Q.when()
            .then(function () {
                //at least one criteria must be provided, all three can't be null
                if (cu.isBlank(Bale.SkuId) || cu.isBlank(Bale.MerchantId))
                    throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
                if (cu.isBlank(Bale.CultureCode))
                    throw {AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'};
            })
            .then(function () {
                return tr.begin();
            })
            .then(function () {
                return lp.getSkuInfo(Bale.SkuId, Bale.CultureCode, tr).then(function (data) {
                    Result.Sku = data;
                });
            })
            .then(function () {
                return li.listSku(Bale, tr).then(function (data) {
                    Result.InventoryList = data;
                });
            })
            .then(function () {
                return tr.commit();
            })
            .then(function () {
                return res.json(Result);
            })
            .catch(function (err) {
                res.status(500).json(cu.createErrorResult(err, "MSG_ERR_INVENTORY_LIST_FAIL"));
                return tr.rollback();
            })
            .done();
});

// List inventory location's sku inventories
router.get('/list/location', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
	var tr= new cm();
        
        var Result={};
	Result.InventoryLocation=null;
	Result.SkuList={};
        
	var Bale={};
	Bale.InventoryLocationId=req.query.inventorylocationid;
	Bale.CultureCode=req.query.cc;
	Bale.MerchantId = req.query.merchantid;

	Bale.Size=req.query.size;
	Bale.Page=req.query.page;
	Bale.Predicate=req.query.predicate;
	Bale.Order=req.query.order;
	Bale.HasInventory=req.query.hasInventory;
	Bale.InventoryStatusId=req.query.inventoryStatusId;
	Bale.Search=req.query.search;
        
	Q.when()
            .then(function () {
                //at least one criteria must be provided, all three can't be null
                if (cu.isBlank(Bale.InventoryLocationId) ||cu.isBlank(Bale.Size) ||cu.isBlank(Bale.Page) || cu.isBlank(Bale.MerchantId))
                    throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
                if (cu.isBlank(Bale.CultureCode))
                    throw {AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'};
            })
            .then(function () {
                return tr.begin();
            })
			.then(function(){
				return li.existsInventoryLocation(Bale.InventoryLocationId, Bale.MerchantId, tr);
			})
            .then(function () {
                return li.getInventoryLocation(Bale, tr).then(function (data) {
                    Result.InventoryLocation = data;
                });
            })
            .then(function () {
                return li.listLocation(Bale, tr).then(function (data) {
                    Result.SkuList = data;
                });
            })
            .then(function () {
                return tr.commit();
            })
            .then(function () {
                return res.json(Result);
            })
            .catch(function (err) {
                res.status(500).json(cu.createErrorResult(err, "MSG_ERR_INVENTORY_LIST_FAIL"));
                return tr.rollback();
            })
            .done();
});

router.get('/view', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
	var tr= new cm();
	var Result={};
	var Bale={};
	Bale.InventoryLocationId=req.query.inventorylocationid;
	Bale.SkuId=req.query.skuid;
	Bale.CultureCode=req.query.cc;
	Bale.MerchantId=req.query.merchantid;

	Result.Sku=null;
	Result.InventoryLocation={};
	Result.Inventory=null;
	
	Q.when()
	.then(function(){
		//at least one criteria must be provided, all three can't be null
		if (cu.isBlank(Bale.SkuId) || cu.isBlank(Bale.InventoryLocationId) || cu.isBlank(Bale.MerchantId))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
		if (cu.isBlank(Bale.CultureCode))
			throw {AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return li.existsInventoryLocation(Bale.InventoryLocationId, Bale.MerchantId, tr);
	})
	.then(function(){
		return lp.existsSku(Bale.SkuId, Bale.MerchantId, tr);
	})
	.then(function(){
		return lp.skuList(0,'',0,0,Bale.SkuId,'',Bale.CultureCode, 1, 1,tr).then(function(data) {
			if (data && data.PageData.length>0)
				Result.Sku=data.PageData[0];
		});		
	})
	.then(function(){
		return tr.queryOne("select il.*, ilc.LocationName from InventoryLocation il inner join InventoryLocationCulture ilc on il.InventoryLocationId = ilc.InventoryLocationId and ilc.CultureCode=:CultureCode where il.InventoryLocationId=:InventoryLocationId",Bale).then(function(data) {
			console.dir(data);
			Result.InventoryLocation=data; //store teh returned user
		});
	})
	.then(function(){
//		return tr.queryOne("select inv.*,s.QtySafetyThreshold as QtySafetyThresholdSku, il.QtySafetyThreshold as QtySafetyThresholdLocation, (inv.QtyAllocated - inv.QtyExported - inv.QtyOrdered  - s.QtySafetyThreshold - il.QtySafetyThreshold) as QtyAts from Inventory inv inner join InventoryLocation il on (inv.InventoryLocationId = il.InventoryLocationID) inner join Sku s on (inv.SkuId=s.SkuId) where inv.InventoryLocationId=:InventoryLocationId and inv.SkuId=:SkuId",Bale).then(function(data) {
//			Result.Inventory=data; //store teh returned user
//		});
                return li.getSkuInventoryInfo(Bale, tr).then(function (data) {
                    Result.Inventory = data;
                });
	})
	.then(function(){
		return tr.commit();
	})
	.then(function(){
		return res.json(Result);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_INVENTORY_VIEW_FAIL"));
		return tr.rollback();
	})
	.done();
});


router.get('/list/journal', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]),function (req, res) {
	var tr= new cm();
	var Bale={};
	Bale.InventoryLocationId=req.query.inventorylocationid;
	Bale.SkuId=req.query.skuid;
	Bale.CultureCode=req.query.cc;
	Bale.MerchantId = req.query.merchantid;
	
	var Result={};
	Result.Sku=null;
	Result.InventoryJournalList=null;
	
	
	Q.when()
	.then(function(){
		//at least one criteria must be provided, all three can't be null
		if (cu.isBlank(Bale.SkuId) || cu.isBlank(Bale.InventoryLocationId) || cu.isBlank(Bale.MerchantId))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
		if (cu.isBlank(Bale.CultureCode))
			throw {AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return li.existsInventoryLocation(Bale.InventoryLocationId, Bale.MerchantId, tr);
	})
	.then(function(){
		return lp.existsSku(Bale.SkuId, Bale.MerchantId, tr);
	})
	.then(function(){
		return lp.skuList(0,'',0,0,Bale.SkuId,'',Bale.CultureCode, 1, 1,tr).then(function(data) {
			if (data && data.PageData.length>0)
				Result.Sku=data.PageData[0];
		});
	})
	.then(function(){
		return li.ListJournal(Bale, tr).then(function(data) {
			Result.InventoryJournalList = data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		return res.json(Result);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_LOCATION_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/sheet/upload', [
	multerUpload,
	function (req, res, next) {
		var data = JSON.parse(req.body.data);
		req.body.data = data;
		req.body.MerchantId = data.MerchantId;
		next();
	},
	secure.applySecurity([
		secure.checkMMUser(),
		secure.checkMerchUser()
	])], function (req, res){
	var tr = new cm();
	var data = req.body.data;

	var Bale = {};
	Bale.Overwrite = data.Overwrite ? 1 : 0;
	Bale.MerchantId = data.MerchantId;
	Bale.InventoryLocationId = data.InventoryLocationId;
	Bale.UserId = req.AccessUser.UserId;

	var filePath = null;
	var newPath = null;

	//this should save the file to the file-system and create an entry in the InventoryImport table.
	//All the data passed like the MerchantId, InventoryLocationId, Overwrite etc must be stored in this table. 
	//When the actual import is called it will just pass the GUID and will need all this info to actually process the spreadsheet.

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		if(cu.isBlank(Bale.MerchantId)){
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		}

		if(!Bale.InventoryLocationId){
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		}
	})
	.then(function(){
		if (!req.file) throw { AppCode : "MSG_ERR_DOCUMENT_UPLOAD" };
		Bale.Guid = req.file.filename;
		filePath = __dirname + '/../uploads/sheets/' + req.file.filename;
		filePath = path.resolve(filePath);
		return;
	})
	.then(function(){
		var fileExtension = req.file.originalname;
		fileExtension = fileExtension.split('.');
		fileExtension = fileExtension[ fileExtension.length - 1 ];
		Bale.FileLocation = '/uploads/sheets/' + req.file.filename + "." + fileExtension;
		return newPath = filePath + "." + fileExtension;
	})
	.then(function(){
		// Rename file to add an extension file.
		return qfs.rename(filePath, newPath);
	})
	.then(function(){
		var sqlQuery = "INSERT INTO InventoryImport (Guid,UserId,MerchantId,InventoryLocationId,Overwrite,FileLocation,StatusId) VALUES (:Guid,:UserId,:MerchantId,:InventoryLocationId,:Overwrite,:FileLocation,3)";
		return tr.queryExecute(sqlQuery, Bale);
	})
	.then(function(){
		return tr.commit();
	})
	.then(function(){
		return res.json({ Guid : Bale.Guid });
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_INVENTORY_IMPORT"));
		return tr.rollback();
	})
	.done();
});

router.post('/sheet/import', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res){
	var tr = new cm();

	var Bale = {};
	Bale.Guid = req.body.Guid; //this is the only parameter allowed for this method
	Bale.MerchantId = req.body.MerchantId;

	var filePath = null;
	var ImportData = false;
	var ImportList = false;

	var chunks = { inserted : 0, updated : 0, errors : 0, skipped : 0, total : 0, errorList : [] };
	var tmpErrorPath = null;
	var errorFileName = null;

	//this method will accept only a GUID. using this GUID it will look up the InventoryImport table and use 
	//the meta data stored in there to process the sheet save the data in the sheet to the Inventory table
	//each sku is identified by a SKU Code and MerchantID combination

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		if(!Bale.Guid){
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		}
	})
	.then(function(){
		var sqlQuery = "SELECT * FROM InventoryImport WHERE Guid = :Guid AND MerchantId = :MerchantId";
		return tr.queryOne(sqlQuery, Bale).then(function(data){
			if(!data){
				throw { AppCode: 'MSG_ERR_INVENTORY_IMPORT_NOT_FOUND' };
			}
			filePath = __dirname + '/..' + data.FileLocation;
			ImportData = data;
		});
	})
	.then(function(){
		// checker if status is set to 2.
		if(ImportData.StatusId == CONSTANTS.STATUS.ACTIVE){
			throw { AppCode : "MSG_ERR_INVENTORY_IMPORT_ALREADY_PROCESS" };
		}
	})
	.then(function(){
		return commonexcel.toJson(filePath, "Inventory").then(function(data){
			ImportList = data;
			if(ImportList==null || ImportList.length==0)
				throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		});
	})
	.then(function(){
		if (!ImportList || ImportList.length==0)
			throw { AppCode: '"MSG_ERR_IMPORT_SKU_EMPTY_FAIL"' };

		var promises = ImportList.map(throat(1, function(item, index){
			item.MerchantId = ImportData.MerchantId;
			item.Overwrite = ImportData.Overwrite;
			item.InventoryLocationId = ImportData.InventoryLocationId;
			item.rowNumber = item.rowNumber === undefined ? index : item.rowNumber;
			chunks["total"] += 1;
			console.log("-Import going to save Row=" + item.rowNumber + " SkuCode=" + item.SkuCode);
			item.UserId=ImportData.UserId;
			return li.inventorySaveSheetRow(item, tr).then(function(statusMessage){
				console.log(statusMessage)
				chunks[statusMessage] += 1;
			}, function(err){
                            console.log(err);
				console.log('ERRRRRRRRRRRRRR' + err);
				chunks["errors"] += 1;

				for(var key in item){
					err[key] = item[key];
				}

				chunks.errorList.push(err);
			});
		}));

		return Q.all(promises);
	})
	.then(function(){
		var newFolder = __dirname + '/../uploads/sheeterrors';
		var newFolder = path.resolve(newFolder);

		return qfs.stat(newFolder).then(function(ret){
			return;
		}, function(err){
			return qfs.mkdir(newFolder); 
		});
	})
	.then(function() {
		if (chunks && chunks.errorList && chunks.errorList.length > 0){
			console.log("create excel error")
			return qfs.readFile(path.join(__dirname, '../data/InventoryErrorTemplate.xlsx')).then(function(data){
				var template = new XlsxTemplate(data);
				var sheetNumber = 1;
				template.substitute(sheetNumber, chunks, "inventory");
				var data = template.generate();

				errorFileName = Bale.Guid+"_";
				errorFileName += ImportData.MerchantId +"_";
				errorFileName += new Date().getTime() + ".xlsx";
				tmpErrorPath = '/uploads/sheeterrors/' + errorFileName;
				var errorPath = __dirname + '/../uploads/sheeterrors/' + errorFileName;
                                
				return qfs.writeFile(errorPath, data, 'binary').then(function(){
					console.log('Created /sheeterrors/' + errorFileName);			
				});
			});	
		}
	})
	.then(function(){
		return tr.queryExecute("UPDATE InventoryImport SET ErrorLocation=:ErrorLocation,ErrorCounts=:ErrorCounts,SkippedCounts=:SkippedCounts,UpdateCounts=:UpdateCounts,InsertCounts=:InsertCounts,TotalCounts=:TotalCounts,StatusId=:StatusId WHERE ImportId=:ImportId", {
			ImportId : ImportData.ImportId,
			ErrorLocation : tmpErrorPath,
			ErrorCounts : chunks.errors || 0,
			SkippedCounts : chunks.skipped || 0,
			UpdateCounts : chunks.updated || 0,
			InsertCounts : chunks.inserted || 0,
			TotalCounts : chunks.total || 0,
                        StatusId: CONSTANTS.STATUS.ACTIVE
		});
	})
	.then(function(){
		return tr.commit();
	})
	.then(function(){
		return res.json({ chunks : chunks, file : errorFileName });
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_INVENTORY_IMPORT"));
		return tr.rollback();
	})
	.done();
});

router.get('/sheet/export', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res){
	var tr = new cm();
	// var page = parseInt(req.query['page']) || 1;
	// var size = parseInt(req.query['size']) || 100000000;
	var merchantid = req.query.merchantid;
	var inventorylocationid = req.query.inventorylocationid;
	var fileName;
	var export_data = [];
	var allPromises = [];

	Q.when()
	.then(function(){
		if (cu.isBlank(merchantid) && cu.isBlank(inventorylocationid))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		var sqlQuery = "SELECT * FROM Inventory Where InventoryLocationId= :InventoryLocationId";
		return tr.queryMany(sqlQuery, {InventoryLocationId: inventorylocationid}).then(function(data){
			if(!data){
				throw { AppCode: 'MSG_ERR_INVENTORY_EXPORT_NOT_FOUND' };
			}
			export_data = data;
			//return data;
		});
	})
	.then(function(){
		if(!(export_data.length > 0)){
			throw { AppCode: 'MSG_ERR_INVENTORY_EXPORT_NOT_FOUND' };
		}
	})
	.then(function(data){
		//loop here...
		var promises = export_data.map(throat(1, function(item){
			return Q.when()
			.then(function(){
				//find SkuCode...
				var sqlQuery = "SELECT * FROM Sku WHERE SkuId = :SkuId";
				return tr.queryOne(sqlQuery, { SkuId : item.SkuId }).then(function(data){
					item.SkuCode = (data && data.SkuCode) ? data.SkuCode : "";
//					item.SkuCode = parseInt(item.SkuCode);
					delete item.SkuId;
				});
			})
			.then(function(){
				if(item.IsPerpetual == 1){
					item.IsPerpetual = 'Y'
				}else{
					item.IsPerpetual = 'N';
				}
			})
			.then(function(){
				if (item.IsPerpetual === 'Y') {
					item.QtyAllocated = '';
				} else {
					item.QtyAllocated = item.QtyAllocated.toString();
				}
			})
		}));

		return Q.all(promises);
	})
	.then(function(){
		var newFolder = __dirname + '/../uploads/sheetexports';
		var newFolder = path.resolve(newFolder);

		return qfs.stat(newFolder).then(function(ret){
			console.log("already had a directory skipped creation.");
			return;
		}, function(err){
			console.log("not a directory create one.")
			return qfs.mkdir(newFolder);
		});
	})
	.then(function(){
		console.time("writeerrorfile");
		return qfs.readFile(path.join(__dirname, '../data/InventoryExportTemplate.xlsx')).then(function(data){
			var template = new XlsxTemplate(data);
			var sheetNumber = 1;
			template.substitute(sheetNumber, { InventoryExport : export_data });
			var data = template.generate();

			fileName = merchantid+"_"+inventorylocationid;
			fileName += new Date().getTime() + ".xlsx";

			return qfs.writeFile(path.join(__dirname, '../uploads/sheetexports/' + fileName), data, 'binary').then(function(){
				console.log('Created /sheetexports/' + fileName);
				console.timeEnd("writeerrorfile");			
			});
		});	
	})
	
	.then(function(){
		//res.send(export_data);
		Q.all(allPromises)
		.then(function() {
	      	console.log('Success');
	    	//res.status(200).json({ fileName : fileName });
	  		return tr.commit();
	  	})
	  	.then(function(){
	  		return res.status(200).json({ fileName : fileName });
	  	});
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_EXPORT_INVENTORY_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.get('/sheet/history', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res){
	var MerchantId = req.query.merchantid;
	var importList = null;

	var tr = new cm();

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		var sqlQuery = "SELECT * FROM InventoryImport Where MerchantId=:MerchantId";
		return tr.queryMany(sqlQuery, { MerchantId : MerchantId }).then(function(data){
			importList = data;
		});
	})
	.then(function(){
		return tr.commit();
	})
	.then(function(){
		return res.status(200).json(importList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_MERCHANT_IMPORT"));
		return tr.rollback();
	})
	.done()
});

router.get('/location/list', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
	var tr= new cm();
	var InventoryLocationList=null;
	var InventoryLocation={};
	InventoryLocation.MerchantId=req.query.merchantid;
	InventoryLocation.CultureCode=req.query.cc;
	InventoryLocation.CultureCodeOverload=req.query.cc;
	if (InventoryLocation.CultureCode!='EN' && InventoryLocation.CultureCode!='en')
			InventoryLocation.CultureCodeOverload='ZH'
	Q.when()
	.then(function(){
		if (cu.isBlank(InventoryLocation.MerchantId))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
		if (cu.isBlank(InventoryLocation.CultureCode))
			throw {AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
                return tr.queryMany("select il.*,iltc.InventoryLocationTypeName, s.StatusNameInvariant, ilc.CultureCode, ilc.LocationName, " +
			"gcoC.GeoCountryName, ifnull(gprovC.GeoName,gprov.GeoNameInvariant) as GeoProvinceName, ifnull(gcityC.GeoName,gcity.GeoNameInvariant) as GeoCityName " +
			"from InventoryLocation il " +
			"inner join InventoryLocationType ilt on (il.InventoryLocationTypeId=ilt.InventoryLocationTypeId) " +
			"inner join InventoryLocationTypeCulture iltc on (ilt.InventoryLocationTypeId=iltc.InventoryLocationTypeId and iltc.CultureCode=:CultureCode) " +
			"inner join Status s on (il.StatusId=s.StatusId) " +
			"inner join GeoCountry gco on (il.GeoCountryId=gco.GeoCountryId) " +
			"inner join GeoCountryCulture gcoC on (gco.GeoCountryId=gcoC.GeoCountryId and gcoC.CultureCode=:CultureCode) " +
			"inner join Geo gprov on ( il.GeoIdProvince=gprov.GeoId) inner join Geo gcity on (il.GeoIdCity=gcity.GeoId) " +
			"inner join InventoryLocationCulture ilc on (il.InventoryLocationId=ilc.InventoryLocationId and ilc.CultureCode=:CultureCode) " +
			"left outer join GeoCulture gprovC on (gprov.GeoId=gprovC.GeoId and gprovC.CultureCode=:CultureCodeOverload) " +
			"left outer join GeoCulture gcityC on (gcity.GeoId=gcityC.GeoId and gcityC.CultureCode=:CultureCodeOverload) " +
			"where il.StatusId <> 1 and MerchantId=:MerchantId",InventoryLocation).then(function(data) {
			InventoryLocationList=data; //store teh returned user
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(InventoryLocationList);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_INVENTORY_LOCATION_FAIL"));
		return tr.rollback();
	})
	.done();
});

/**
 * this route is not used in frontend, i think this is used in storefront?
 */
router.get('/location/list/user', function (req, res) {
	var tr= new cm();
	var InventoryLocationList=null;
	var Bale={};
	Bale.UserId=req.query.userid;
	Bale.CultureCode=req.query.cc;
	Bale.CultureCodeOverload=req.query.cc;
	if (Bale.CultureCode!='EN' && Bale.CultureCode!='en')
			Bale.CultureCodeOverload='ZH'
	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserId))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
		if (cu.isBlank(Bale.CultureCode))
			throw {AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
                return tr.queryMany("select il.*,iltc.InventoryLocationTypeName, s.StatusNameInvariant, gcoC.GeoCountryName, gprovC.GeoName as GeoProvinceName, ifnull(gcityC.GeoName,gcity.GeoNameInvariant) as GeoCityName from InventoryLocation il inner join InventoryLocationType ilt on (il.InventoryLocationTypeId=ilt.InventoryLocationTypeId) inner join InventoryLocationTypeCulture iltc on (ilt.InventoryLocationTypeId=iltc.InventoryLocationTypeId and iltc.CultureCode=:CultureCode) inner join Status s on (il.StatusId=s.StatusId) inner join GeoCountry gco on (il.GeoCountryId=gco.GeoCountryId) inner join GeoCountryCulture gcoC on (gco.GeoCountryId=gcoC.GeoCountryId and gcoC.CultureCode=:CultureCode) inner join Geo gprov on ( il.GeoIdProvince=gprov.GeoId) inner join Geo gcity on (il.GeoIdCity=gcity.GeoId) left outer join GeoCulture gprovC on (gprov.GeoId=gprovC.GeoId and gprovC.CultureCode=:CultureCodeOverload) left outer join GeoCulture gcityC on (gcity.GeoId=gcityC.GeoId and gcityC.CultureCode=:CultureCodeOverload) where il.StatusId <> 1 and il.InventoryLocationId in (select InventoryLocationId from UserInventoryLocation where UserId=:UserId)",Bale).then(function(data) {
				InventoryLocationList=data; //store teh returned user
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(InventoryLocationList);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_INVENTORY_LOCATION_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.get('/location/list', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
	var tr= new cm();
	var InventoryLocationList=null;
	var InventoryLocation={};
	InventoryLocation.MerchantId=req.query.merchantid;
	InventoryLocation.CultureCode=req.query.cc;
	InventoryLocation.CultureCodeOverload=req.query.cc;
	if (InventoryLocation.CultureCode!='EN' && InventoryLocation.CultureCode!='en')
			InventoryLocation.CultureCodeOverload='ZH'
	Q.when()
	.then(function(){
		if (cu.isBlank(InventoryLocation.MerchantId))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
		if (cu.isBlank(InventoryLocation.CultureCode))
			throw {AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryMany("select il.*,iltc.InventoryLocationTypeName, s.StatusNameInvariant, gcoC.GeoCountryName, gprovC.GeoName as GeoProvinceName, ifnull(gcityC.GeoName,gcity.GeoNameInvariant) as GeoCityName from InventoryLocation il inner join InventoryLocationType ilt on (il.InventoryLocationTypeId=ilt.InventoryLocationTypeId) inner join InventoryLocationTypeCulture iltc on (ilt.InventoryLocationTypeId=iltc.InventoryLocationTypeId and iltc.CultureCode=:CultureCode) inner join Status s on (il.StatusId=s.StatusId) inner join GeoCountry gco on (il.GeoCountryId=gco.GeoCountryId) inner join GeoCountryCulture gcoC on (gco.GeoCountryId=gcoC.GeoCountryId and gcoC.CultureCode=:CultureCode) inner join Geo gprov on ( il.GeoIdProvince=gprov.GeoId) inner join Geo gcity on (il.GeoIdCity=gcity.GeoId) left outer join GeoCulture gprovC on (gprov.GeoId=gprovC.GeoId and gprovC.CultureCode=:CultureCodeOverload) left outer join GeoCulture gcityC on (gcity.GeoId=gcityC.GeoId and gcityC.CultureCode=:CultureCodeOverload) where il.StatusId <> 1 and MerchantId=:MerchantId",InventoryLocation).then(function(data) {
			if (data==null || data.length==0) throw new Error("User not found");//, ,Geo gcit
				InventoryLocationList=data; //store teh returned user
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(InventoryLocationList);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_INVENTORY_LOCATION_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/location/save', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
	var tr= new cm();
	var InventoryLocation={};
	InventoryLocation=req.body;
	Q.when()
	.then(function() {
		if (cu.isBlank(InventoryLocation.MerchantId)) {
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
		}
		else if (_.any(config.availableCultureCode, function (cultureCode) {
				return !InventoryLocation.Culture || !InventoryLocation.Culture[cultureCode] || _.trim(InventoryLocation.Culture[cultureCode].LocationName) === '';
			})){
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
		}
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		//check location code uniqueness
		return li.checkLocationCodeUniqueness(InventoryLocation.InventoryLocationId, InventoryLocation.MerchantId, InventoryLocation.LocationExternalCode, tr);
	})
	.then(function(){
		//check location name uniqueness.
		return li.checkLocationNameUniqueness(InventoryLocation.InventoryLocationId, InventoryLocation.MerchantId, InventoryLocation.Culture.EN.LocationName, InventoryLocation.Culture.CHS.LocationName, InventoryLocation.Culture.CHT.LocationName, tr);
	})
	.then(function(){

		var params 	= _.reduce(config.availableCultureCode, function (result, lang) {
			result[lang] = [];
			_.each(['InventoryLocationId', 'CultureCode', 'LocationName'], function (field) {
				if (field === 'CultureCode') {
					result[lang][field] = lang;
				} else if (field === 'InventoryLocationId') {
					result[lang][field] = InventoryLocation[field];
				} else {
					result[lang][field] = _.trim(InventoryLocation.Culture[lang][field]);
					if (lang === 'EN') {
						InventoryLocation[field + 'Invariant'] = _.trim(InventoryLocation.Culture[lang][field]);
					}
				}
			});
			return result;
		}, {});

		if (InventoryLocation.InventoryLocationId==0)
		{
            InventoryLocation.StatusId = InventoryLocation.isDraft ? CONSTANTS.STATUS.PENDING : CONSTANTS.STATUS.ACTIVE;
			return tr.queryExecute("insert into InventoryLocation (MerchantId,InventoryLocationTypeId,LocationNameInvariant, LocationExternalCode,GeoCountryId,GeoIdProvince,GeoIdCity,District,PostalCode,Apartment,Floor,BlockNo,Building,StreetNo,Street,QtySafetyThreshold,StatusId,LastStatus) values (:MerchantId,:InventoryLocationTypeId,:LocationNameInvariant, :LocationExternalCode,:GeoCountryId,:GeoIdProvince,:GeoIdCity,:District,:PostalCode,:Apartment,:Floor,:BlockNo,:Building,:StreetNo,:Street,:QtySafetyThreshold,:StatusId,UTC_TIMESTAMP())",InventoryLocation).then(function(data) {
					InventoryLocation.InventoryLocationId=data.insertId;
					return;
				}).then(function () {
				return Q.all(config.availableCultureCode.map(function (lang) {
					params[lang].InventoryLocationId = InventoryLocation.InventoryLocationId;
					return tr.queryExecute(
							'INSERT INTO `InventoryLocationCulture` (`InventoryLocationId`, `CultureCode`, `LocationName`) VALUES (:InventoryLocationId, :CultureCode, :LocationName) ', params[lang]);
				}));
			})
		}
		else
		{
			return tr.queryExecute("update InventoryLocation set MerchantId=:MerchantId,InventoryLocationTypeId=:InventoryLocationTypeId,LocationNameInvariant=:LocationNameInvariant, LocationExternalCode=:LocationExternalCode, GeoCountryId=:GeoCountryId,GeoIdProvince=:GeoIdProvince,GeoIdCity=:GeoIdCity,District=:District,PostalCode=:PostalCode,Apartment=:Apartment,Floor=:Floor,BlockNo=:BlockNo,Building=:Building,StreetNo=:StreetNo,Street=:Street,QtySafetyThreshold=:QtySafetyThreshold where InventoryLocationId=:InventoryLocationId",InventoryLocation).then(function(data) {
				return;
			}).then(function () {
				return Q.all(config.availableCultureCode.map(function (lang) {
					return tr.queryExecute(
						'UPDATE `InventoryLocationCulture` SET LocationName = :LocationName WHERE InventoryLocationId = :InventoryLocationId AND CultureCode = :CultureCode ', params[lang]);
				}));
			});
		}
	})
	.then(function() {
		return tr.queryOne("select il.*,s.StatusNameInvariant from InventoryLocation il, Status s where il.StatusId=s.StatusId and il.InventoryLocationId=:InventoryLocationId",InventoryLocation).then(function(data) {
			if (data==null) throw {AppCode: 'MSG_ERR_NOT_FOUND'};
			InventoryLocation=data; //store teh returned user
		})
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(InventoryLocation);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_INVENTORY_LOCATION_FAIL"));
		return tr.rollback();

	})
	.done();

});

router.get('/location/view', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
	var tr= new cm();
	var InventoryLocation={};
	InventoryLocation.InventoryLocationId=req.query.id;
	InventoryLocation.MerchantId=req.query.merchantid;
	InventoryLocation.CultureCode=req.query.cc;
	InventoryLocation.CultureCodeOverload=req.query.cc;
	if (InventoryLocation.CultureCode!='EN' && InventoryLocation.CultureCode!='en')
			InventoryLocation.CultureCodeOverload='ZH'
	Q.when()
	.then(function(){
                if (cu.isBlank(InventoryLocation.MerchantId))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
		if (cu.isBlank(InventoryLocation.InventoryLocationId))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
		if (cu.isBlank(InventoryLocation.CultureCode))
			throw {AppCode: 'MSG_ERR_CULTURE_CODE_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return tr.queryMany("select il.*,ilc.CultureCode, ilc.LocationName, " +
			"iltc.InventoryLocationTypeName, s.StatusNameInvariant, gcoC.GeoCountryName, " +
			"gprovC.GeoName as GeoProvinceName, ifnull(gcityC.GeoName,gcity.GeoNameInvariant) as GeoCityName " +
			"from InventoryLocation il " +
			"inner join InventoryLocationType ilt on (il.InventoryLocationTypeId=ilt.InventoryLocationTypeId) " +
			"inner join InventoryLocationTypeCulture iltc on (ilt.InventoryLocationTypeId=iltc.InventoryLocationTypeId and iltc.CultureCode=:CultureCode) " +
			"inner join Status s on (il.StatusId=s.StatusId) " +
			"inner join GeoCountry gco on (il.GeoCountryId=gco.GeoCountryId) " +
			"inner join GeoCountryCulture gcoC on (gco.GeoCountryId=gcoC.GeoCountryId and gcoC.CultureCode=:CultureCode) " +
			"inner join Geo gprov on (il.GeoIdProvince=gprov.GeoId) " +
			"inner join Geo gcity on (il.GeoIdCity=gcity.GeoId) " +
			"inner join InventoryLocationCulture ilc on (il.InventoryLocationId=ilc.InventoryLocationId) " +
			"left outer join GeoCulture gprovC on (gprov.GeoId=gprovC.GeoId and gprovC.CultureCode=:CultureCodeOverload) " +
			"left outer join GeoCulture gcityC on (gcity.GeoId=gcityC.GeoId and gcityC.CultureCode=:CultureCodeOverload) " +
			"where il.InventoryLocationId=:InventoryLocationId and il.MerchantId = :MerchantId and il.StatusId<>1",InventoryLocation).then(function(data) {
//			if (data==null) throw new Error("User not found");
                        if (data==null || data.length==0) throw new Error("InventoryLocation not found");

			_.each(data, function (location) {
				if (!InventoryLocation.Culture) {
					InventoryLocation = location;
					InventoryLocation.Culture = {};
				}

				if (!InventoryLocation.Culture[location.CultureCode] && !cu.isBlank(location.CultureCode)) {
					InventoryLocation.Culture[location.CultureCode] = {
						LocationName: location.LocationName
					};
				}
			});
		})
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(InventoryLocation);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_INVENTORY_LOCATION_FAIL"));
		return tr.rollback();

	})
	.done();
});

/**
 * not yet used as inventory status cannot be changed
 */
router.post('/status/change', function (req, res) {
	var tr= new cm();
	var Data={};
	var StatusIdRequested=0;
	Data.InventoryId=req.body.InventoryId;

	Q.when()
	.then(function(){
		switch(req.body.Status)
		{
		case 'Active':
			 StatusIdRequested=CONSTANTS.STATUS.ACTIVE;
			 break;
		case 'Inactive':
			 StatusIdRequested=CONSTANTS.STATUS.INACTIVE;
			 break;
		case 'Deleted':
			 StatusIdRequested=CONSTANTS.STATUS.DELETED;
			 break;
		default:
			throw {AppCode:"MSG_ERR_SKU_STATUS_UNKOWN"};
		};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return li.InventoryStatusChange(Data, StatusIdRequested, tr).then(function(Data){
			return Data;
		}, function(err){
			throw err;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SKU_STATUS_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/location/status/change', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res) {
	var tr= new cm();
	var Data={};
	var StatusIdRequested=0;
	Data.InventoryLocationId=req.body.InventoryLocationId;
	Data.MerchantId=req.body.MerchantId;

	Q.when()
	.then(function(){
		switch(req.body.Status)
		{
		case 'Active':
			 StatusIdRequested=CONSTANTS.STATUS.ACTIVE;
			 break;
		case 'Inactive':
			 StatusIdRequested=CONSTANTS.STATUS.INACTIVE;
			 break;
		case 'Deleted':
			 StatusIdRequested=CONSTANTS.STATUS.DELETED;
			 break;
		default:
			throw {AppCode:"MSG_ERR_STATUS_UNKOWN"};
		}
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return li.existsInventoryLocation(Data.InventoryLocationId, Data.MerchantId, tr);
	})
	.then(function(){
		if (StatusIdRequested===CONSTANTS.STATUS.INACTIVE) {
			//All allocations to this inventory location is N/A or 0
			return tr.queryOne('select InventoryId from Inventory ' +
				'where (QtyAllocated<>0 or IsPerpetual=1) and StatusId<>:StatusId and InventoryLocationId=:InventoryLocationId',
				{InventoryLocationId: Data.InventoryLocationId, StatusId: CONSTANTS.STATUS.DELETED}).then(function (data) {
				if (data !== null) {
					throw {
						AppCode: "MSG_ERR_INVENTORY_EXISTS"
					}
				}
			});
		}
	})
	.then(function () {
		if (StatusIdRequested!==CONSTANTS.STATUS.DELETED) {
			return lm.isPendingMerchant(tr, Data.MerchantId, StatusIdRequested);
		}
	})
	.then(function() {
		return li.InventoryLocationStatusChange(Data, StatusIdRequested, tr).then(function(Data){
			return Data;
		}, function(err){
			throw err;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_STATUS_FAIL"));
		return tr.rollback();
	})
	.done();
});

//handle file too large error or other event related error
router.use('/sheet/upload', function (err, req, res, next) {
	res.status(500).json(cu.createErrorResult(err, "MSG_ERR_FILE_SIZE"));
});

module.exports = router;
