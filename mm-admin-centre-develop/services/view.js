"use strict";
var express = require('express'), router = express.Router();
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');

var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var logicview = require('../logic/logicview.js');
var logicuser = require('../logic/logicuser.js');

router.get('/user', function (req, res) {
	Q.spawn(function*(){
		var tr = new cm();

		try{
			var Bale = req.query || {};

			if(cu.isBlank(Bale.UserKey))
				throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };

			yield tr.begin();

			var userInfo = yield logicview.user(tr, Bale);

			if(userInfo){
				yield tr.commit();
				res.send(userInfo);	
			}else{
				throw { AppCode: 'MSG_ERR_USER_NOT_EXISTS' };
			}	
		}catch(err){			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err, "MSG_ERR_VIEW_USER"));			
		}
	});	
});

router.get('/merchant', function (req, res) {
	Q.spawn(function*(){
		var tr = new cm();

		try{
			var Bale = req.query || {};

			if(cu.isBlank(Bale.MerchantId))
				throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "MerchantId" };
			if(cu.isBlank(Bale.cc) && cu.isBlank(Bale.CultureCode))
				throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "cc/CultureCode" };

			Bale.CultureCode = Bale.CultureCode || Bale.cc;

			yield tr.begin();

			var merchantInfo = yield logicview.merchant(tr, Bale);

			if(merchantInfo){
				yield tr.commit();
				res.send(merchantInfo);	
			}else{
				throw { AppCode: 'MSG_ERR_MERCHANT_NOT_EXISTS' };
			}	
		}catch(err){			
			yield tr.rollback();
			res.status(500).json(cu.createErrorResult(err, "MSG_ERR_VIEW_MERCHANT"));			
		}
	});	
});

module.exports = router;