"use strict";
var express = require('express'), router = express.Router();
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');

var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var logicfriend = require('../logic/logicfriend.js');
var logicfollow = require('../logic/logicfollow.js');
var logicuser = require('../logic/logicuser.js');

router.post('/*', [ secure.checkAuth, secure.checkMmAdminOrSelf ]);

router.get('/find', function(req, res){
	var tr = new cm();
	var Bale = {};
	var UserList = [];

	Bale.limit = parseInt(req.query.limit) || 100;
	Bale.start = parseInt(req.query.start) || 0;
	Bale.s = (req.query.s || "");

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicfriend.getFindUser(tr, Bale).then(function(Users){
			UserList = Users;
		});
	})
	.then(function(){
		UserList.forEach(function(d){
          if(cu.isBlank(d.DisplayName)){
            d.DisplayName = d.FirstName + " " + d.LastName;
          }
          if(cu.isBlank(d.UserName)){
            d.UserName = d.FirstName + d.LastName;
          }

          delete d.FirstName;
          delete d.LastName;
        });
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(UserList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FRIEND_LIST_FAIL"));
		return tr.rollback();
	})
	.done();

});

router.get('/list', function(req, res){
	var tr = new cm();
	var Bale = req.query;
	var UserList = [];

	Bale.limit = parseInt(Bale.limit) || 100;
	Bale.start = parseInt(Bale.start) || 0;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicfriend.getUserFriend(tr, Bale).then(function(User){
			UserList = User;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(UserList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FRIEND_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.get('/request/list', function(req, res){
	var tr = new cm();
	var Bale = req.query;
	var UserList = [];

	Bale.limit = parseInt(Bale.limit) || 100;
	Bale.start = parseInt(Bale.start) || 0;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicfriend.getUserFriendRequest(tr, Bale).then(function(User){
			UserList = User;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(UserList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FRIEND_REQUEST_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.get('/request/receive', function(req, res){
	var tr = new cm();
	var Bale = req.query;
	var UserList = [];

	Bale.limit = parseInt(Bale.limit) || 100;
	Bale.start = parseInt(Bale.start) || 0;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicfriend.getUserFriendReceive(tr, Bale).then(function(User){
			UserList = User;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(UserList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FRIEND_REQUEST_RECIEVE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Save friend request API
// Required: UserKey, ToUserKey
router.post('/request', function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.ToUserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ToUserKey" };

		if(Bale.UserKey == Bale.ToUserKey)
			throw { AppCode: 'MSG_ERR_CA_CANNOT_FRIEND_SELF' };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    	Bale.IsCuratorUser = User.IsCuratorUser;
	    });
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.ToUserKey).then(function(User){
	    	Bale.ToUserId = User.UserId;
	    	Bale.ToIsCuratorUser = User.IsCuratorUser;
	    });
	})
	.then(function(){
		return logicfriend.saveFriend(tr, Bale);
	})
	.then(function(){
		return logicfollow.saveUser(tr, Bale);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.send(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FRIEND_SAVE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Delete friend / friend request API
// Required: UserKey, ToUserKey

router.post('/delete', function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.ToUserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ToUserKey" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.ToUserKey).then(function(User){
	    	Bale.ToUserId = User.UserId;
	    });
	})
	.then(function(){
		return logicfriend.deleteFriend(tr, Bale);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.send(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FRIEND_DELETE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Accept friend request API
// Required: UserKey, ToUserKey
router.post('/request/accept', function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.ToUserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ToUserKey" };

		if(Bale.UserKey == Bale.ToUserKey)
			throw { AppCode: 'MSG_ERR_CA_CANNOT_FRIEND_SELF' };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    	Bale.IsCuratorUser = User.IsCuratorUser;
	    });
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.ToUserKey).then(function(User){
	    	Bale.ToUserId = User.UserId;
	    	Bale.ToIsCuratorUser = User.IsCuratorUser;
	    });
	})
	.then(function(){
		return logicfriend.acceptFriend(tr, Bale);
	})
	.then(function(){
		return logicfollow.saveUser(tr, Bale);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.send(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_FRIEND_ACCEPT_FAIL"));
		return tr.rollback();
	})
	.done();
});

module.exports = router;
