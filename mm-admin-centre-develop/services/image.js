"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var multer = require('multer');
var config=require('../config');
var fs=require('fs');
var path = require('path');
var cu = require('../lib/commonutil.js');
var ci = require('../lib/commonimage.js');
var secure = require('./secure.js');
var Q = require('q');

//1.middleware for check auth for upload
router.use('/upload',secure.checkAuth);

//2.middleware for bucket avalibility checking
router.use('/upload',function(req, res, next){
	var bucket = req.query.b;
	console.log("check bucket", bucket);
	if (/[^a-zA-Z0-9]/.test(bucket) || bucket.indexOf('images') == -1) {
		res.status(500).json(cu.createErrorResult(new Error("bucket is not exited! upload failed"),"MSG_ERR_IMAGE_UPLOAD_BUCKET"));
		return ;
	}
	return next();
});

//3.middleware for image upload storage
var multer = require('multer');//user image upload multer
var mkdirp = require('mkdirp');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
	var bucket = req.query.b;
    var dir = __dirname + '/../uploads/' + bucket;
    cb(null, dir); //specify the folder dynamically by bucket.
  }
});

var multerUpload = multer({storage:storage,limits:{fileSize:config.fileSize.image,files:1}}).single('file');

/**
 * common upload image which is for merchant, brand, user etc.
 * except for the product image due to the logic is different.
 * @param {bucket} this param specify the folder which image will be stored.
 */
router.post('/upload', multerUpload, function (req, res) {
	var bucket = req.query.b;
	if (!req.file)
		res.status(500).json(cu.createErrorResult(new Error("missing file"), "MSG_ERR_IMAGE_UPLOAD"));
	var filePath=__dirname + '/../uploads/' + bucket + '/' + req.file.filename;
	filePath=path.resolve(filePath);
	ci.resizeImage(filePath,200).then(function(){
		var ImageKey=req.file.filename;
		var ImageMimeType=req.file.mimetype;
		res.json({ImageKey:ImageKey,ImageMimeType:ImageMimeType});
	}).catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_IMAGE_RESIZE"));
	})
	.done();
});

//this router will be canceled, please use "/api/resizer/view".
// router.get('/view', function (req, res) {
// 	var key=req.query.key;
// 	var size=req.query.s;
//    	var image = null;
//
//    	Q.when()
// 	.then(function(){
// 		if( /[^a-zA-Z0-9]/.test( key ) ){
// 			throw {AppCode: 'MSG_ERR_IMAGE_INVALID_KEY'}
// 		}
// 	})
// 	.then(function(){
// 		//check old path
// 		var filePath=__dirname + '/../uploads/userimages/' + key
// 		if (size==200)
// 			filePath=filePath + "_200";
// 		filePath=path.resolve(filePath);
//
// 		if (fs.existsSync(filePath)){
// 			image = filePath;
// 		}
// 		return true;
// 	})
// 	.then(function(){
// 		//check on new path
// 		var filePath=__dirname + '/../uploads/userimages/' + key
// 		if (size==200)
// 			filePath=filePath + "_200";
// 		filePath=path.resolve(filePath);
//
// 		if (fs.existsSync(filePath)){
// 			image = filePath;
// 		}
// 		return true;
// 	})
// 	.then(function(){
// 		if( !image ){
// 			return res.status(500).json({ AppCode : "MSD_ERR_IMAGE_NOT_FOUND" });
// 		}else{
// 			res.set('Content-Type', 'image/jpeg');
// 			res.sendFile(image);
// 		}
// 	})
// 	.catch(function(err){
// 		console.log(err);
// 		res.status(500).json(err);
// 	})
// 	.done();
//
// });

router.get('/form', function(req, res){
    res.sendFile(path.resolve(__dirname + '/../views/imageForm.html'));
});


//handle file too large error or other event related error
router.use('/upload', function (err, req, res, next) {
	res.status(500).json(cu.createErrorResult(err, "MSG_ERR_FILE_SIZE"));
});

module.exports = router;
