"use strict";
var express = require('express'), router = express.Router();
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');

var secure = require('./secure.js');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var logicfollow = require('../logic/logicfollow.js');
var logicuser = require('../logic/logicuser.js');

router.post('/*', [ secure.checkAuth, secure.checkMmAdminOrSelf ]);

// List Users Following User API
// Required: UserKey
// Optional: start, limit
router.get('/user/following', function(req, res){
	var tr = new cm();
	var Bale = req.query;
	var UserList = [];

	Bale.limit = parseInt(Bale.limit) || 100;
	Bale.start = parseInt(Bale.start) || 0;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicfollow.getUserFollowing(tr, Bale).then(function(User){
			UserList = User;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(UserList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

// List Users Followed By User API
// Required: UserKey
// Optional: start, limit
router.get('/user/followed', function(req, res){
	var tr = new cm();
	var Bale = req.query;
	var UserList = [];

	Bale.limit = parseInt(Bale.limit) || 100;
	Bale.start = parseInt(Bale.start) || 0;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicfollow.getUserFollowed(tr, Bale).then(function(User){
			UserList = User;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(UserList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

// List Users Following Merchant API
// Required: MerchantId
// Optional: start, limit
router.get('/merchant/following', function(req, res){
	var tr = new cm();
	var Bale = req.query;
	var MerchantList = [];

	Bale.limit = parseInt(Bale.limit) || 100;
	Bale.start = parseInt(Bale.start) || 0;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.MerchantId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "MerchantId" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicfollow.getMerchantFollowing(tr, Bale).then(function(Merchant){
			MerchantList = Merchant;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(MerchantList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_MERCHANT_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

// List Merchants Followed By User API
// Required: UserKey
// Optional: start, limit, cc
router.get('/merchant/followed', function(req, res){
	var tr = new cm();
	var Bale = req.query;
	var MerchantList = [];

	Bale.limit = parseInt(Bale.limit) || 100;
	Bale.start = parseInt(Bale.start) || 0;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.cc)) Bale.cc = "EN";
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicfollow.getMerchantFollowed(tr, Bale).then(function(Merchant){
			MerchantList = Merchant;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(MerchantList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_MERCHANT_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

// List Users Following Brand API
// Required: BrandId
// Optional: start, limit
router.get('/brand/following', function(req, res){
	var tr = new cm();
	var Bale = req.query;
	var BrandList = [];

	Bale.limit = parseInt(Bale.limit) || 100;
	Bale.start = parseInt(Bale.start) || 0;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.BrandId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "BrandId" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicfollow.getBrandFollowing(tr, Bale).then(function(Brand){
			BrandList = Brand;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(BrandList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_BRAND_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

// List Brands Followed by User API
// Required: UserKey
// Optional: start, limit, cc
router.get('/brand/followed', function(req, res){
	var tr = new cm();
	var Bale = req.query;
	var BrandList = [];

	Bale.limit = parseInt(Bale.limit) || 100;
	Bale.start = parseInt(Bale.start) || 0;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.cc)) Bale.cc = "EN";
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicfollow.getBrandFollowed(tr, Bale).then(function(Brand){
			BrandList = Brand;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(BrandList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_BRAND_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Save follow user API
// Required: UserKey, ToUserKey/ToUserKeys
router.post('/user/save', function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.ToUserKey) && cu.isBlank(Bale.ToUserKeys))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ToUserKey" };

		if(Bale.UserKey == Bale.ToUserKey)
			throw { AppCode: 'MSG_ERR_CANNOT_CREATE_RELATIONSHIP_SELF' };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
    var promises = cu.split(Bale.ToUserKey || Bale.ToUserKeys).map(function (ToUserKey) {
      return logicuser.getUser(tr, ToUserKey).then(function (User) {
        return logicfollow.saveUser(tr, {
          UserId: Bale.UserId,
          ToUserId: User.UserId,
          IsCuratorUser : User.IsCuratorUser
        });
      });
    });
    return Q.all(promises);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.send(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_RELATIONSHIP_SAVE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Save follow merchant API
// Required: UserKey, ToMerchantId/ToMerchantIds
router.post('/merchant/save', function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.ToMerchantId) && cu.isBlank(Bale.ToMerchantIds))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ToMerchantId" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function(User){
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
    var promises = cu.split(Bale.ToMerchantId || Bale.ToMerchantIds).map(function (ToMerchantId) {
      return logicfollow.saveMerchant(tr, {
        UserId: Bale.UserId,
        ToMerchantId: ToMerchantId
      });
    })
    return Q.all(promises);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.send(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_MERCHANT_RELATIONSHIP_SAVE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Save follow brand API
// Required: UserKey, ToBrandId/ToBrandIds
router.post('/brand/save', function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.ToBrandId) && cu.isBlank(Bale.ToBrandIds))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ToBrandId" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function(User){
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
    var promises = cu.split(Bale.ToBrandId || Bale.ToBrandIds).map(function (ToBrandId) {
      return logicfollow.saveBrand(tr, {
        UserId: Bale.UserId,
        ToBrandId: ToBrandId
      });
    });
    return Q.all(promises);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.send(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_BRAND_RELATIONSHIP_SAVE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Delete follow user API
// Required: UserKey, ToUserKey
router.post('/user/delete', function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.ToUserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ToUserKey" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function (User) {
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.ToUserKey).then(function(User){
	    	Bale.ToUserId = User.UserId;
	    	Bale.IsCuratorUser = User.IsCuratorUser;
	    });
	})
	.then(function(){
		return logicfollow.deleteUser(tr, Bale);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.send(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_RELATIONSHIP_DELETE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Delete follow merchant API
// Required: UserKey, ToMerchantId
router.post('/merchant/delete', function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.ToMerchantId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ToMerchantId" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function(User){
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicfollow.deleteMerchant(tr, Bale);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.send(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_MERCHANT_RELATIONSHIP_DELETE_FAIL"));
		return tr.rollback();
	})
	.done();
});

// Delete follow brand API
// Required: UserKey, ToBrandId
router.post('/brand/delete', function(req, res){
	var tr = new cm();
	var Bale = req.body;

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.UserKey))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "UserKey" };
		if (cu.isBlank(Bale.ToBrandId))
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ToBrandId" };
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return logicuser.getUser(tr, Bale.UserKey).then(function(User){
	    	Bale.UserId = User.UserId;
	    });
	})
	.then(function(){
		return logicfollow.deleteBrand(tr, Bale);
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.send(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_BRAND_RELATIONSHIP_DELETE_FAIL"));
		return tr.rollback();
	})
	.done();
});

module.exports = router;
