"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();

var fs=require("fs");
var qfs=require("qfs-node");
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var XlsxTemplate = require('../logic/logicexceltemplate.js');
var secure = require('./secure.js');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var commonexcel = require('../lib/commonexcel.js');

var lp = require('../logic/logicproduct.js');
var lm = require('../logic/logicmerch.js');
var cst = require('../logic/constants.js');

var Excel = require('exceljs');
var throat = require('throat');
var multer = require('multer');
var multerUpload = multer({dest: __dirname + '/../uploads/sheets',limits:{fileSize:(CONSTANTS.MEGABYTE * 10),files:1}}).single('file');
//this may need to be auth secured for securoty reason but leaving public for now....

router.get('/style/list', function (req, res){
	var StyleList=null;
	var tr= new cm();
	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.cc))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin(true);
	})
	.then(function(){
		var page = (req.query['page'] || 1);
		var size = (req.query['size'] || 25);
		var missingImgType = req.query.missingimgtype;
		return lp.styleList(req.query.merchantid,req.query.categoryid,req.query.stylecode,req.query.statusid,req.query.s,req.query.cc, page, size, missingImgType, tr).then(function(data) {
			StyleList=data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(StyleList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_PRODUCT_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

/*
router.get('/style/list/old', function (req, res){
	var StyleList=null;
	var tr= new cm();
	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.cc))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin(true);
	})
	.then(function(){
		var page = (req.query['page'] || 1);
		var size = (req.query['size'] || 25);
		var missingImgType = req.query.missingimgtype;
		return lp.styleListOld(req.query.merchantid,req.query.categoryid,'',req.query.statusid,req.query.s,req.query.cc, page, size, missingImgType, tr).then(function(data) {
			StyleList=data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(StyleList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_PRODUCT_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});
*/

router.get('/style/view', function (req, res){
	var Style = null;
	var tr = new cm();
	Q.when()

		.then(function() {
			if (cu.isBlank(req.query.cc) || cu.isBlank(req.query.stylecode) || cu.isBlank(req.query.merchantid))
				throw {
					AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'
				};
		})
		.then(function() {
			return tr.begin();
		})
		.then(function() {
			//1. get style object
			return lp.styleList(req.query.merchantid,0,req.query.stylecode,0,'',req.query.cc, 1, 1, '',tr).then(function(data) {
				if (data)
					if (data.PageData.length > 0)
						Style = data.PageData[0];
			});
		})
		.then(function() {
			console.log("2. get sku culture realted values");
			//set sku culture properties.
			//2.1.get sku culture realted values
			var SkuIdList = Style.SkuList.map(function(n) {
				return n.SkuId;
			});
			var cin = SkuIdList.join();
			var sqlQuery = "SELECT * FROM SkuCulture WHERE SkuId in ({cin})".replace('{cin}', cin);

			return tr.queryMany(sqlQuery);
		})
		.then(function(skuCultureList) {
			//2.2.set culture value to style Object
			if (skuCultureList.length == 0) {
				throw {
					AppCode: 'MSG_ERR_SKU_CULTURE_MISSING'
				};
			}
			Style.SkuList.forEach(function(n) {
				var skuCultures = skuCultureList.filter(function(sc) {
					return n.SkuId == sc.SkuId;
				});
				skuCultures.forEach(function(skuCulture) {
					if (skuCulture.CultureCode === CONSTANTS.CULTURE_CODE.EN) {
						n.SkuColorEN = skuCulture.SkuColor;
						Style.SkuNameEN = skuCulture.SkuName;
						Style.SkuManufacturerNameEN = skuCulture.SkuManufacturerName;
						Style.SkuDescEN = skuCulture.SkuDesc;
						Style.SkuFeatureEN = skuCulture.SkuFeature;
						Style.SkuMaterialEN = skuCulture.SkuMaterial;
						Style.SkuSizeCommentEN = skuCulture.SkuSizeComment;
					} else if (skuCulture.CultureCode === CONSTANTS.CULTURE_CODE.CHS) {
						n.SkuColorCHS = skuCulture.SkuColor;
						Style.SkuNameCHS = skuCulture.SkuName;
						Style.SkuManufacturerNameCHS = skuCulture.SkuManufacturerName;
						Style.SkuDescCHS = skuCulture.SkuDesc;
						Style.SkuFeatureCHS = skuCulture.SkuFeature;
						Style.SkuMaterialCHS = skuCulture.SkuMaterial;
						Style.SkuSizeCommentCHS = skuCulture.SkuSizeComment;
					} else if (skuCulture.CultureCode === CONSTANTS.CULTURE_CODE.CHT) {
						n.SkuColorCHT = skuCulture.SkuColor;
						Style.SkuNameCHT = skuCulture.SkuName;
						Style.SkuManufacturerNameCHT = skuCulture.SkuManufacturerName;
						Style.SkuDescCHT = skuCulture.SkuDesc;
						Style.SkuFeatureCHT = skuCulture.SkuFeature;
						Style.SkuMaterialCHT = skuCulture.SkuMaterial;
						Style.SkuSizeCommentCHT = skuCulture.SkuSizeComment;
					}
				})
			});
		})
		.then(function() {
			return tr.commit();
		})
		.then(function() {
			res.json(Style);
		})
		.catch(function(err) {
			res.status(500).json(cu.createErrorResult(err, "MSG_ERR_PRODUCT_LIST_FAIL"));
			console.log('Rollback');
			console.dir(err);
			return tr.rollback();
		})
		.done();

});

router.get('/sku/list', function (req, res){
	var SkuList=null;
	var tr= new cm();
	Q.when()
	.then(function(){
		if (cu.isBlank(req.query.cc))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		var page = (req.query['page'] || 1);
		var size = (req.query['size'] || 25);
		return lp.skuList(req.query.merchantid,req.query.stylecode,req.query.categoryid,req.query.statusid,0,req.query.s,req.query.cc, page, size,tr).then(function(data) {
			SkuList=data;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(SkuList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_PRODUCT_LIST_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.get('/sku/view', function (req, res){
	var tr= new cm();
	var Sku = null

	Q.when().then(function(){
		if (cu.isBlank(req.query.cc) || cu.isBlank(req.query.skuid) || cu.isBlank(req.query.merchantid))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		var SkuId = req.query['skuid'];
		var MerchantId = req.query['merchantid'];
		return lp.skuList(MerchantId,req.query.stylecode,0,0,SkuId,'',req.query.cc, 1, 1, tr).then(function(data) {
			if (data)
				if (data.PageData.length>0)
					Sku=data.PageData[0];
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(Sku);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_PRODUCT_SKU_VIEW_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/style/status/change', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res){
	var tr= new cm();
	var Bale={};
	Bale.StatusId=0;
	Bale.Status=req.body.Status;
	Bale.MerchantId = req.body.MerchantId;  // this one
	Bale.StyleCode = req.body.StyleCode;  // and this one

	Q.when()
	.then(function(){
		if (cu.isBlank(Bale.MerchantId) || cu.isBlank(Bale.StyleCode) || cu.isBlank(Bale.Status))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		switch(Bale.Status)
		{
		case 'Active':
			 Bale.StatusId=CONSTANTS.STATUS.ACTIVE;
			 break;
		case 'Inactive':
			 Bale.StatusId=CONSTANTS.STATUS.INACTIVE;
			 break;
		case 'Deleted':
			 Bale.StatusId=CONSTANTS.STATUS.DELETED;
			 break;
		default:
			throw {AppCode:"MSG_ERR_SKU_STATUS_UNKOWN"};
		}
	})
	.then(function(){
		return tr.begin();
	})
	.then(function () {
		if (Bale.StatusId===CONSTANTS.STATUS.ACTIVE) {
			//if a merchant is in pending or inactive, users cannot active its product, what if user is mm admin?
			return tr.queryOne('select MerchantId from Merchant where MerchantId=:MerchantId and StatusId in (:Pending, :Inactive)',
				{MerchantId: Bale.MerchantId, Pending: CONSTANTS.STATUS.PENDING, Inactive: CONSTANTS.STATUS.INACTIVE}
			).then(function (data) {
				if (data !== null) {
					throw {
						AppCode: 'MSG_ERR_MERCHANT_DISABLED'
					}
				}
			});
		}
	})
	.then(function () {
		if (Bale.StatusId!==CONSTANTS.STATUS.DELETED) {
			return lm.isPendingMerchant(tr, Bale.MerchantId, Bale.StatusId);
		}
	})
	.then(function() {
		return lp.StyleStatusChange(Bale, tr).then(function(Bale){
			return Bale;
		}, function(err){
			throw err;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_STYLE_STATUS_FAIL"));
		return tr.rollback();
	})
	.done();
});


router.post('/sku/activate/all', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req ,res){
	var tr = new cm();
	var MerchantId = req.body.MerchantId;

	Q.when()
	.then(function(){
		if(!MerchantId){
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		}
	})
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		//if a merchant is in pending or inactive, users cannot active its product
		return tr.queryOne('select MerchantId from Merchant where MerchantId=:MerchantId and StatusId in (:Pending, :Inactive)',
			{MerchantId: MerchantId, Pending: CONSTANTS.STATUS.PENDING, Inactive: CONSTANTS.STATUS.INACTIVE}
		).then(function (data) {
			if (data !== null) {
				throw {
					AppCode: 'MSG_ERR_MERCHANT_DISABLED'
				}
			}
		});
	})
	//insert then from logicproduct here
	.then(function(){
		//update sku of merchant id to 2 "ACTIVE" of all SKU for that mechant id that the statusId is equal to 4 "INACTIVE"
		return lp.ActivalAllPending({ MerchantId : MerchantId }, tr).then(function(Status){
			return Status;
		}, function(err){
			throw err;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		return res.json(true);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SKU_STATUS_FAIL"));
		return tr.rollback();
	})
	.done();

});

/**
 * this route is not used at all
 */
router.post('/sku/status/change', [secure.checkAuth,secure.checkMmAdminOrMerchant], function (req, res){
	var tr= new cm();
	var Sku={};
	var StatusIdRequested=0;
	Sku.SkuId=req.body.SkuId;

	Q.when()
	.then(function(){
		switch(req.body.Status)
		{
		case 'Active':
			 StatusIdRequested=CONSTANTS.STATUS.ACTIVE;
			 break;
		case 'Inactive':
			 StatusIdRequested=CONSTANTS.STATUS.INACTIVE;
			 break;
		case 'Deleted':
			 StatusIdRequested=CONSTANTS.STATUS.DELETED;
			 break;
		default:
			throw {AppCode:"MSG_ERR_SKU_STATUS_UNKOWN"};
		};
	})
	.then(function(){
		return tr.begin();
	})
	.then(function() {
		return lp.SkuStatusChange(Sku, StatusIdRequested, tr).then(function(Sku){
			return Sku;
		}, function(err){
			throw err;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		res.json(true);
		return;
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_SKU_STATUS_FAIL"));
		return tr.rollback();
	})
	.done();
});

router.post('/sku/sheet/upload', [
	multerUpload,
	function (req, res, next) {
		var data = JSON.parse(req.body.data);
		req.body.data = data;
		req.body.MerchantId = data.MerchantId;
		next();
	},
	secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()])], function (req, res){
	var tr = new cm();
	var data = req.body.data;

	var Bale = {};
	Bale.Overwrite = data.Overwrite ? 1 : 0;
	Bale.Guid = null;
	Bale.MerchantId = data.MerchantId;
	Bale.UserId = req.AccessUser.UserId;

	console.log(Bale)

	var filePath = null;
	var newPath = "";

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		if(!Bale.MerchantId){
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		}
	})
	.then(function(){
		if (!req.file) throw { AppCode : "MSG_ERR_DOCUMENT_UPLOAD" };
		Bale.Guid = req.file.filename;
		filePath = __dirname + '/../uploads/sheets/' + req.file.filename;
		filePath = path.resolve(filePath);
		return;
	})
	.then(function(){
		//get file type
		var fileExtension = req.file.originalname;
		fileExtension = fileExtension.split('.');
		fileExtension = fileExtension[ fileExtension.length - 1 ];
		Bale.FileLocation = '/uploads/sheets/' + req.file.filename + "." + fileExtension;
		return newPath = filePath + "." + fileExtension;
	})
	.then(function(){
		return qfs.rename(filePath, newPath);
	})
	.then(function(){
		var sqlQuery = "INSERT INTO SkuImport (Guid,UserId,MerchantId,Overwrite,FileLocation,StatusId) VALUES (:Guid,:UserId,:MerchantId,:Overwrite,:FileLocation,3)";
		return tr.queryExecute(sqlQuery, Bale);
	})
	.then(function(){
		return tr.commit();
	})
	.then(function() {
		return res.json({ Guid : Bale.Guid });
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_PRODUCT_IMPORT"));
		return tr.rollback();
	})
	.done();
});

router.post('/sku/sheet/import', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res){
	var tr = new cm();

	var Bale = {};
	Bale.Guid = req.body.Guid; //this is the only parameter allowed for this method
	Bale.MerchantId = req.body.MerchantId;

	var filePath = null;
	var SkuData = false;
	var SkuList = false;

	var chunks = { inserted : 0, updated : 0, errors : 0, skipped : 0, total : 0, errorList : [] };
	var tmpErrorPath = null;
	var errorFileName = null;

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		if(!Bale.Guid){
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		}
	})
	.then(function(){
		var sqlQuery = "SELECT * FROM SkuImport WHERE Guid = :Guid AND MerchantId = :MerchantId";
		return tr.queryOne(sqlQuery, Bale).then(function(data){
			if(!data){
				throw { AppCode: 'MSG_ERR_SKU_IMPORT_NOT_FOUND' };
			}
			filePath = __dirname + '/..' + data.FileLocation;
			SkuData = data;
		});
	})
	.then(function(){
		// checker if status is set to 2.
		if(SkuData.StatusId == CONSTANTS.STATUS.ACTIVE){
			throw { AppCode : "MSG_ERR_SKU_IMPORT_ALREADY_PROCESS" };
		}
	})
	.then(function(){
		return commonexcel.toJson(filePath, "SKU").then(function(data){
			SkuList = data;
			if(SkuList==null || SkuList.length==0)
				throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		});
	})
	.then(function() {
		if (!SkuList || SkuList.length==0)
			throw { AppCode: '"MSG_ERR_IMPORT_SKU_EMPTY_FAIL"' };
                    
                var rowNumber = 1;
		var promises = SkuList.map(throat(1, function(item){
			item.MerchantId = SkuData.MerchantId;
			item.Overwrite = SkuData.Overwrite;
      item.SkuCode = item.SkuCode || item.SKU;
			chunks["total"] += 1;
			console.log("-Import going to save Row=" + rowNumber++ + " SkuCode=" + item.SkuCode);

			return lp.skusave(item, tr).then(function(statusMessage){
				chunks[statusMessage] += 1;
			}, function(err){
                            // console.log(err);
				chunks["errors"] += 1;

				for(var key in item){
					err[key] = item[key];
				}

				chunks.errorList.push(err);
			});
		}));

		return Q.all(promises).then(function(){
		});
	})
	.then(function(){
		var newFolder = __dirname + '/../uploads/sheeterrors';
		var newFolder = path.resolve(newFolder);

		return qfs.stat(newFolder).then(function(ret){
			return;
		}, function(err){
			return qfs.mkdir(newFolder);
		});
	})
	.then(function() {
		if (chunks && chunks.errorList && chunks.errorList.length > 0){
			return qfs.readFile(path.join(__dirname, '../data/ProductErrorTemplate.xlsx')).then(function(data){
				var template = new XlsxTemplate(data);
				var sheetNumber = 1;
				template.substitute(sheetNumber, chunks, "product");
				var data = template.generate();

				errorFileName = Bale.Guid+"_";
				errorFileName += Bale.MerchantId +"_";
				errorFileName += new Date().getTime() + ".xlsx";
				tmpErrorPath = '/uploads/sheeterrors/' + errorFileName;
				var errorPath = __dirname + '/../uploads/sheeterrors/' + errorFileName;

				return qfs.writeFile(errorPath, data, 'binary').then(function(){
					console.log('Created /sheeterrors/' + errorFileName);
				});
			});
		}
	})
	.then(function(){
		return tr.queryExecute("UPDATE SkuImport SET ErrorLocation=:ErrorLocation,ErrorCounts=:ErrorCounts,SkippedCounts=:SkippedCounts,UpdateCounts=:UpdateCounts,InsertCounts=:InsertCounts,TotalCounts=:TotalCounts,StatusId=:StatusId WHERE ImportId=:ImportId", {
			ImportId : SkuData.ImportId,
			ErrorLocation : tmpErrorPath,
			ErrorCounts : chunks.errors || 0,
			SkippedCounts : chunks.skipped || 0,
			UpdateCounts : chunks.updated || 0,
			InsertCounts : chunks.inserted || 0,
			TotalCounts : chunks.total || 0,
                        StatusId: CONSTANTS.STATUS.ACTIVE
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		return res.json({ chunks : chunks, file : errorFileName });
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_PRODUCT_IMPORT"));
		return tr.rollback();
	})
	.done();
});

///added for 3RD API CALL
router.post('/sku/save', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function (req, res){

	var tr = new cm();
	var chunks = { success : false, message : "Error", errors : 0, errorList : [] };
	var Bale = req.body;

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		if(!Bale.MerchantId || cu.isBlank(Bale.MerchantId)){
			throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' };
		}
	})
	.then(function(){
		return lp.skusave(Bale, tr).then(function(s){
			chunks.message = s;
			chunks.success = true;
		}, function(err){
			//read all error
			chunks["errors"] = err.errors.length;
			chunks.errorList = err.errors;
		});
	})
	.then(function() {
		return tr.commit();
	})
	.then(function() {
		return res.json(chunks);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_PRODUCT_IMPORT"));
		return tr.rollback();
	})
	.done();

});

router.get('/sku/sheet/history', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function(req, res){
	var MerchantId = req.query.merchantid;
	var importList = null;

	var tr = new cm();

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		var sqlQuery = "SELECT * FROM SkuImport Where MerchantId=:MerchantId";
		return tr.queryMany(sqlQuery, { MerchantId : MerchantId }).then(function(data){
			importList = data;
		});
	})
	.then(function(){
		return tr.commit();
	})
	.then(function(){
		return res.status(200).json(importList);
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_MERCHANT_IMPORT"));
		return tr.rollback();
	})
	.done()
});

router.get('/export', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function(req, res){
	console.log("export all sku of merchant");
	var SkuList = [];
	var page = parseInt(req.query['page']) || 1;
	var size = parseInt(req.query['size']) || 100000000;

	var MerchantId = req.query.merchantid;
	var CategoryId = req.query.categoryid;
	var fileName = null;

	var tr = new cm();

	Q.when()
	.then(function(){
		return tr.begin();
	})
	.then(function(){
		return lp.skuList(MerchantId,'', CategoryId, '', 0, '', req.query.cc, page, size, tr).then(function(response){
			if(response.PageData.length <= 0){
				throw { AppCode : "No Product to export" }
			}
                        
			SkuList = response.PageData;
		});
	})
	.then(function(){
		var promises = SkuList.map(throat(1, function(item){
			return Q.when()
			.then(function(){
				// set values to prevent undefined/null value being generated in excel..
				item.PrimaryCategoryCode = ""; item.MerchandisingCategoryCode1 = ""; item.MerchandisingCategoryCode2 = ""; item.MerchandisingCategoryCode3 = ""; item.MerchandisingCategoryCode4 = ""; item.MerchandisingCategoryCode5 = "";
				if(cu.isBlank(item.SaleFrom) || item.SaleFrom == "Invalid Date"){ item.SaleFrom = ""; }
				if(cu.isBlank(item.SaleTo) || item.SaleTo == "Invalid Date"){ item.SaleTo = ""; }
				if(cu.isBlank(item.AvailableFrom) || item.AvailableFrom == "Invalid Date"){ item.AvailableFrom = ""; }
				if(cu.isBlank(item.AvailableTo) || item.AvailableTo == "Invalid Date"){ item.AvailableTo = ""; }
				if(cu.isBlank(item.QtySafetyThreshold)){ item.QtySafetyThreshold = 0; }
				if(cu.isBlank(item.PriceSale)){ item.PriceSale = 0; }
				if(cu.isBlank(item.PriceRetail)){ item.PriceRetail = 0; }
				return;
			})
			.then(function(){
				var sqlQuery = "SELECT * FROM Brand WHERE BrandId = :BrandId";
				return tr.queryOne(sqlQuery, { BrandId : item.BrandId }).then(function(data){
					item.BrandCode = (data && data.BrandCode) ? data.BrandCode : "";
				});
			})
			.then(function(){
				var sqlQuery = "SELECT * FROM Badge WHERE BadgeId = :BadgeId";
				return tr.queryOne(sqlQuery, { BadgeId : item.BadgeId }).then(function(data){
					item.BadgeCode = (data && data.BadgeCode) ? data.BadgeCode : "";
				});
			})
			.then(function(){
				var sqlQuery = "SELECT * FROM Season WHERE SeasonId = :SeasonId";
				return tr.queryOne(sqlQuery, { SeasonId : item.SeasonId }).then(function(data){
					item.SeasonCode = (data && data.SeasonCode) ? data.SeasonCode : "";
				});
			})
			.then(function(){
				var sqlQuery = "SELECT * FROM Size WHERE SizeId = :SizeId";
				return tr.queryOne(sqlQuery, { SizeId : item.SizeId }).then(function(data){
					item.SizeCode = (data && data.SizeCode) ? data.SizeCode : "";
				});
			})
			.then(function(){
				var sqlQuery = "SELECT * FROM Color WHERE ColorId = :ColorId";
				return tr.queryOne(sqlQuery, { ColorId : item.ColorId }).then(function(data){
					item.ColorCode = (data && data.ColorCode) ? data.ColorCode : "";
				});
			})
			.then(function(){
				var sqlQuery = "SELECT * FROM GeoCountry WHERE GeoCountryId = :GeoCountryId";
				return tr.queryOne(sqlQuery, { GeoCountryId : item.GeoCountryId }).then(function(data){
					item.CountryCode = (data && data.CountryCode) ? data.CountryCode : "";
				});
			})
			.then(function(){
				var sqlQuery = "SELECT SkuCategory.Priority, SkuCategory.CategoryId, Category.CategoryId, Category.CategoryCode FROM SkuCategory LEFT JOIN Category ON Category.CategoryId = SkuCategory.CategoryId WHERE SkuId = :SkuId";
				return tr.queryMany(sqlQuery, { SkuId : item.SkuId }).then(function(data){
					for(var i = 0; i < data.length; i++){
						var Priority = parseInt(data[i].Priority);
						if(Priority == 0){
							item.PrimaryCategoryCode = data[i].CategoryCode || "";
						}else{
							item["MerchandisingCategoryCode" + ( Priority) ] = data[i].CategoryCode && data[i].CategoryCode != "CAT0" ? data[i].CategoryCode : "";
						}
					}
				});
			})
			.then(function(){
				if(item.StatusId == CONSTANTS.STATUS.DELETED){
					item.Delete = 1;
				}else{
					item.Delete = "";
				}

//				if(!item.QtySafetyThreshold){ item.QtySafetyThreshold = ""; }
			})
			.then(function(){
				var sqlQuery = "SELECT * FROM SkuCulture WHERE SkuId = :SkuId";
				return tr.queryMany(sqlQuery, { SkuId : item.SkuId }).then(function(data){
					var tmp = {
						SkuName : "SkuName",
						SkuDesc : "Description",
						SkuFeature : "Feature",
						SkuMaterial : "Material",
						SkuSizeComment : "SizeComment",
						SkuColor : "ColorName"
					};

					var tmpCulture = {
						"EN" : "English", "CHT" : "Hant", "CHS" : "Hans"
					};

					for(var i = 0; i < data.length; i++){
						for(var key in tmp){
							item[ tmp[key] + "-" + tmpCulture[data[i].CultureCode] ] = data[i][key];
						}
					}
				});
			})
		}));

		return Q.all(promises);
	})
	.then(function(){
		var newFolder = __dirname + '/../uploads/sheetexports';
		var newFolder = path.resolve(newFolder);

		return qfs.stat(newFolder).then(function(ret){
			console.log("already had a directory skipped creation.");
			return;
		}, function(err){
			console.log("not a directory create one.")
			return qfs.mkdir(newFolder);
		});
	})
	.then(function(){
		console.time("writeerrorfile");
		return qfs.readFile(path.join(__dirname, '../data/ProductExportTemplate.xlsx')).then(function(data){
			var template = new XlsxTemplate(data);
			var sheetNumber = 'SKU';
			template.substitute(sheetNumber, { SkuList : SkuList });
			var data = template.generate();

			fileName = MerchantId +"_";
			fileName += new Date().getTime() + ".xlsx";

			return qfs.writeFile(path.join(__dirname, '../uploads/sheetexports/' + fileName), data, 'binary').then(function(){
				console.log('Created /sheetexports/' + fileName);
				console.timeEnd("writeerrorfile");
			});
		});
	})
	.then(function(){
		tr.commit();
		return res.status(200).json({ fileName : fileName });
	})
	.catch(function(err){
		res.status(500).json(cu.createErrorResult(err,"MSG_ERR_MERCHANT_EXPORT"));
		return tr.rollback();
	})
	.done();
});

router.get('/testImport', function(req, res){
	var file = "Test_Blank_Column.xlsx";
	var method = "xlsxToJson";

	if(req.query.file){
		file = req.query.file;
	}

	if(req.query.method){
		method = req.query.method;
	}

	commonexcel[method]("./data/" + file, "SKU").then(function(data){
		res.send(data);
	}, function(err){
		console.log("error", err);
	});
});

/**
 * @desc create product.
 **/
router.post('/create', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function(req, res) {
  console.log("create product");
  var style = req.body.style;
  style.MerchantId = req.body.MerchantId;
	style.StatusId = (req.body.isDraft==true)?CONSTANTS.STATUS.PENDING:CONSTANTS.STATUS.ACTIVE;

  var tr = new cm();
  Q.when()
		.then(function() {
			return tr.begin();
		})
    .then(function() {
			lp.settingProductDefaultValue(style);
    })
    .then(function() {
			//1.check input and if style code is unique
      console.log("1. checking input and if style code is unique");
			return lp.validateProductInputBeforeSave(tr, style, true, '', true);
    })
	  .then(function () {
		  if (style.StatusId !== CONSTANTS.STATUS.PENDING) {
			  return lm.isPendingMerchant(tr, style.MerchantId, style.StatusId);
		  }
	  })
    .then(function() {
      //2.store sku list;
      console.log("2. store sku list");
      var promises = style.SkuList.map(function(sku) {
        //2.1 set sku other common column value.
        var skuCombin = Object.assign({}, sku, style); //combine style properties and sku properties into one object.
        skuCombin.SkuNameInvariant = skuCombin.SkuNameEN;
        skuCombin.SkuDescInvariant = skuCombin.SkuDescEN;
        skuCombin.SkuFeatureInvariant = skuCombin.SkuFeatureEN;
        skuCombin.SkuMaterialInvariant = skuCombin.SkuMaterialEN;
	    skuCombin.SkuSizeCommentInvariant = skuCombin.SkuSizeCommentEN;
        skuCombin.SkuColorInvariant = skuCombin.SkuColorEN;

        return tr.queryExecute("INSERT INTO `Sku` (`StyleCode`, `SkuCode`, `Bar`, `SkuNameInvariant`, `SkuDescInvariant`, `SkuFeatureInvariant`, `SkuMaterialInvariant`, `SkuSizeCommentInvariant`, `SkuColorInvariant`, `BrandId`, `BadgeId`, `SeasonId`, `SizeId`, `ColorId`, `ColorKey`, `GeoCountryId`, `LaunchYear`, `ManufacturerName`, `PriceRetail`, `PriceSale`, `SaleFrom`, `SaleTo`, `AvailableFrom`, `AvailableTo`, `WeightKg`, `HeightCm`, `WidthCm`, `LengthCm`, `QtySafetyThreshold`, `MerchantId`, `StatusId`, `IsFeatured`) VALUES (:StyleCode, :SkuCode, :Bar, :SkuNameInvariant, :SkuDescInvariant, :SkuFeatureInvariant, :SkuMaterialInvariant, :SkuSizeCommentInvariant, :SkuColorInvariant, :BrandId, :BadgeId, :SeasonId, :SizeId, :ColorId, :ColorKey, :GeoCountryId, :LaunchYear, :ManufacturerName, :PriceRetail, :PriceSale, :SaleFrom, :SaleTo, :AvailableFrom, :AvailableTo, :WeightKg, :HeightCm, :WidthCm, :LengthCm, :QtySafetyThreshold, :MerchantId, :StatusId, :IsFeatured)", skuCombin)
          .then(function(data) {
            sku.SkuId = data.insertId;
          });
      });
      return Q.all(promises);
    })
    .then(function() {
      //3.store sku culture
      console.log("3. store sku culture");
      var skuCultureInsertSQL = 'INSERT INTO `SkuCulture` (`SkuId`, `CultureCode`, `SkuName`, `SkuDesc`, `SkuFeature`, `SkuMaterial`, `SkuSizeComment`, `SkuColor`) VALUES (:SkuId, :CultureCode, :SkuName, :SkuDesc, :SkuFeature, :SkuMaterial, :SkuSizeComment, :SkuColor)';
      var promises = style.SkuList.map(function(sku) {
        //3.1 set sku culture column value;
        var SkuCultureDataArray = [];
        SkuCultureDataArray.push({
          SkuId: sku.SkuId,
          CultureCode: CONSTANTS.CULTURE_CODE.EN,
          SkuName: style.SkuNameEN,
          SkuDesc: style.SkuDescEN,
          SkuFeature: style.SkuFeatureEN,
          SkuMaterial: style.SkuMaterialEN,
		  SkuSizeComment: style.SkuSizeCommentEN,
          SkuColor: sku.SkuColorEN,
        });
        SkuCultureDataArray.push({
          SkuId: sku.SkuId,
          CultureCode: CONSTANTS.CULTURE_CODE.CHS,
          SkuName: style.SkuNameCHS,
          SkuDesc: style.SkuDescCHS,
          SkuFeature: style.SkuFeatureCHS,
          SkuMaterial: style.SkuMaterialCHS,
		  SkuSizeComment: style.SkuSizeCommentCHS,
          SkuColor: sku.SkuColorCHS,
        });
        SkuCultureDataArray.push({
          SkuId: sku.SkuId,
          CultureCode: CONSTANTS.CULTURE_CODE.CHT,
          SkuName: style.SkuNameCHT,
          SkuDesc: style.SkuDescCHT,
          SkuFeature: style.SkuFeatureCHT,
          SkuMaterial: style.SkuMaterialCHT,
		  SkuSizeComment: style.SkuSizeCommentCHT,
          SkuColor: sku.SkuColorCHT,
        });
        var skuCulturePromises = SkuCultureDataArray.map(function(skuCulture) {
          return tr.queryExecute(skuCultureInsertSQL, skuCulture);
        });
        return Q.all(skuCulturePromises);
      });
      return Q.all(promises);
    })
    .then(function() {
      //4.store style images;
      console.log("4. store style images");
      var styleImageInsertSQL = 'INSERT INTO StyleImage (`MerchantId`, `StyleCode`, `ImageTypeCode`, `ImageTypeId`, `ColorKey`, `ProductImage`, `Position`) value (:MerchantId, :StyleCode, :ImageTypeCode, :ImageTypeId, :ColorKey, :ImageKey, :Position)';
      //4.1store feature images
      var featureImagePromises = style.FeaturedImageList.map(function(featuredImage) {
        featuredImage.MerchantId = style.MerchantId;
        featuredImage.StyleCode = style.StyleCode;
        featuredImage.ImageTypeCode = CONSTANTS.PRODUCT_IMG_TYPE_CODE.FEATURE;
        featuredImage.ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.FEATURE;
        return tr.queryExecute(styleImageInsertSQL, featuredImage);
      });
      //4.2store description images
      var descriptionImagePromises = style.DescriptionImageList.map(function(descriptionImage) {
        descriptionImage.MerchantId = style.MerchantId;
        descriptionImage.StyleCode = style.StyleCode;
        descriptionImage.ImageTypeCode = CONSTANTS.PRODUCT_IMG_TYPE_CODE.DESC;
        descriptionImage.ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.DESC;
        return tr.queryExecute(styleImageInsertSQL, descriptionImage);
      });
      //4.3store color images
      var colorList = Object.keys(style.ColorImageListMap);
      var colorListPromises = colorList.map(function(colorKey) {
        var colorImageList = style.ColorImageListMap[colorKey];
        var colorPromises = colorImageList.map(function(colorImage) {
          colorImage.MerchantId = style.MerchantId;
          colorImage.StyleCode = style.StyleCode;
          colorImage.ImageTypeCode = CONSTANTS.PRODUCT_IMG_TYPE_CODE.COLOR;
          colorImage.ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.COLOR;
          colorImage.ColorKey = colorKey;
          return tr.queryExecute(styleImageInsertSQL, colorImage);
        });
        return Q.all(colorPromises);
      });

      return Q.all(colorListPromises, descriptionImagePromises, featureImagePromises);
    })
    .then(function() {
      //5.store sku category
      console.log("5. store sku category");
      var skuCategoryInsertSQL = "INSERT INTO SkuCategory(SkuId, CategoryId, Priority) VALUE(:SkuId, :CategoryId, :Priority)"
      var promises = style.SkuList.map(function(sku) {
        var skuCategoryPromises = style.CategoryPriorityList.map(function(skuCategory) {
          skuCategory.SkuId = sku.SkuId;
          return tr.queryExecute(skuCategoryInsertSQL, skuCategory);
        });
        return Q.all(skuCategoryPromises);
      });
      return Q.all(promises);
    })
    .then(function() {
      console.log("product save successfully!");
      return tr.commit();
    })
    .then(function() {
      res.json();
    })
    .catch(function(err) {
      res.status(500).json(cu.createErrorResult(err, "MSG_ERR_CREATE_PRODUCT"));
      console.log('Rollback');
      console.dir(err);
      return tr.rollback();
    })
    .done();

});

/**
 * @desc update product.
 **/
router.post('/update', secure.applySecurity([secure.checkMMUser(), secure.checkMerchUser()]), function(req, res) {
  console.log("update product");
  var style = req.body.style;
  style.MerchantId = req.body.MerchantId;
  var deleteSkuIdList = req.body.deleteSkuIdList;
	var isSkuListModifed = false;
  var isCategoryChanged = (req.body.isCategoryChanged == true) ? true : false;
  var isImageChanged = (req.body.isImageChanged == true) ? true : false;
	var isStyleCodeChanged = (req.body.isStyleCodeChanged == true) ? true : false;
	var originalStyleCode = req.body.originalStyleCode;

  var tr = new cm();
  Q.when()
		.then(function() {
			return tr.begin();
		})
		.then(function(){
			lp.settingProductDefaultValue(style);
		})
    .then(function() {
			//1.check product input
			console.log("1.1 check product input");
      return	lp.validateProductInputBeforeSave(tr, style, isStyleCodeChanged, originalStyleCode);
    })
	  .then(function () {
		  //for a pending merchant, all its sku should be pending
		  return tr.queryOne("select MerchantId from Merchant where MerchantId=:MerchantId and StatusId=:StatusId",
			  {MerchantId: style.MerchantId, StatusId: CONSTANTS.STATUS.PENDING}).then(function (data) {
			  if (data !== null) {
				  _.each(style.SkuList, function (sku) {
					  if (sku.StatusId === CONSTANTS.STATUS.ACTIVE) {
						  sku.StatusId = CONSTANTS.STATUS.PENDING;
					  }
				  });
			  }
		  });
	  })
	.then(function(){
		if(isStyleCodeChanged){
			console.log("1.2 update stylecode in sku and styleImage");
			var updateSkuPromise = tr.queryExecute("UPDATE `Sku` SET `StyleCode`=:StyleCode WHERE StyleCode=:OriginStyleCode AND MerchantId=:MerchantId", {StyleCode:style.StyleCode, OriginStyleCode:originalStyleCode, MerchantId:style.MerchantId});
			var updateStyleImagePromise = tr.queryExecute("UPDATE `StyleImage` SET `StyleCode`=:StyleCode WHERE StyleCode=:OriginStyleCode AND MerchantId=:MerchantId", {StyleCode:style.StyleCode, OriginStyleCode:originalStyleCode, MerchantId:style.MerchantId});
			return Q.all(updateSkuPromise, updateStyleImagePromise);
		}
	})
    .then(function() {
		//2.update sku list;
		console.log("2.1 check if there is sku need to be deleted.if yes, delete them");
		//2.1 check if there is sku need to be deleted.if yes, soft delete them
		var deleteSkupromises = deleteSkuIdList.map(function(skuId) {
			//soft deleted the sku
			return tr.queryExecute("UPDATE Sku SET StatusId=1 WHERE SkuId = :SkuId", {
				SkuId: skuId
			});
		});
		var deleteSkuCulturePromises = deleteSkuIdList.map(function(skuId) {
			//soft deleted the sku culture
			return tr.queryExecute("UPDATE SkuCulture SET StatusId=1 WHERE SkuId = :SkuId", {
			SkuId: skuId
			});
		});
		var deleteInventoryPromises = deleteSkuIdList.map(function(skuId) {
			//soft delete sku inventory.
			return tr.queryExecute("UPDATE Inventory SET StatusId=1 WHERE SkuId = :SkuId", {
				SkuId: skuId
			});
		});
		return Q.all(deleteSkupromises, deleteSkuCulturePromises, deleteInventoryPromises);
    })
    .then(function() {
      console.log("2.2 check if there is sku need to be added.if yes, add them.");
      //2.2 check if there is sku need to be added.if yes, add them.
      var promises = style.SkuList.map(function(sku) {
        if (sku.IsNewAdd == 1) {
					isSkuListModifed = true;
          var skuCombin = Object.assign({}, sku, style); //combine style properties and sku properties into one object.
          skuCombin.SkuNameInvariant = skuCombin.SkuNameEN;
          skuCombin.SkuDescInvariant = skuCombin.SkuDescEN;
		  skuCombin.SkuSizeCommentInvariant = skuCombin.SkuSizeCommentEN;
          skuCombin.SkuFeatureInvariant = skuCombin.SkuFeatureEN;
          skuCombin.SkuMaterialInvariant = skuCombin.SkuMaterialEN;
          skuCombin.SkuColorInvariant = skuCombin.SkuColorEN;
          return tr.queryExecute("INSERT INTO `Sku` (`StyleCode`, `SkuCode`, `Bar`, `SkuNameInvariant`, `SkuDescInvariant`, `SkuSizeCommentInvariant`, `SkuFeatureInvariant`, `SkuMaterialInvariant`, `SkuColorInvariant`, `BrandId`, `BadgeId`, `SeasonId`, `SizeId`, `ColorId`, `ColorKey`, `GeoCountryId`, `LaunchYear`, `ManufacturerName`, `PriceRetail`, `PriceSale`, `SaleFrom`, `SaleTo`, `AvailableFrom`, `AvailableTo`, `WeightKg`, `HeightCm`, `WidthCm`, `LengthCm`, `QtySafetyThreshold`, `MerchantId`, `StatusId`, `IsFeatured`) VALUES (:StyleCode, :SkuCode, :Bar, :SkuNameInvariant, :SkuDescInvariant, :SkuSizeCommentInvariant, :SkuFeatureInvariant, :SkuMaterialInvariant, :SkuColorInvariant, :BrandId, :BadgeId, :SeasonId, :SizeId, :ColorId, :ColorKey, :GeoCountryId, :LaunchYear, :ManufacturerName, :PriceRetail, :PriceSale, :SaleFrom, :SaleTo, :AvailableFrom, :AvailableTo, :WeightKg, :HeightCm, :WidthCm, :LengthCm, :QtySafetyThreshold, :MerchantId, :StatusId, :IsFeatured)", skuCombin)
            .then(function(data) {
              sku.SkuId = data.insertId;
            });
        }
				return;
      });
      return Q.all(promises);
    })
    .then(function() {
      //2.3 check if there is sku need to be added.if yes, add it skuCulture.
      console.log("2.3 check if there is sku need to be added.if yes, add it skuCulture.");
      var skuCultureInsertSQL = 'INSERT INTO `SkuCulture` (`SkuId`, `CultureCode`, `SkuName`, `SkuDesc`, `SkuSizeComment`, `SkuFeature`, `SkuMaterial`, `SkuColor`) VALUES (:SkuId, :CultureCode, :SkuName, :SkuDesc, :SkuSizeComment, :SkuFeature, :SkuMaterial, :SkuColor)';
      var promises = style.SkuList.map(function(sku) {
        if (sku.IsNewAdd == 1) {
          var SkuCultureDataArray = [];
          SkuCultureDataArray.push({
            SkuId: sku.SkuId,
            CultureCode: CONSTANTS.CULTURE_CODE.EN,
            SkuName: style.SkuNameEN,
            SkuDesc: style.SkuDescEN,
		    SkuSizeComment: style.SkuSizeCommentEN,
            SkuFeature: style.SkuFeatureEN,
            SkuMaterial: style.SkuMaterialEN,
            SkuColor: sku.SkuColorEN,
          });
          SkuCultureDataArray.push({
            SkuId: sku.SkuId,
            CultureCode: CONSTANTS.CULTURE_CODE.CHS,
            SkuName: style.SkuNameCHS,
            SkuDesc: style.SkuDescCHS,
		  	SkuSizeComment: style.SkuSizeCommentCHS,
		  	SkuFeature: style.SkuFeatureCHS,
            SkuMaterial: style.SkuMaterialCHS,
            SkuColor: sku.SkuColorCHS
          });
          SkuCultureDataArray.push({
            SkuId: sku.SkuId,
            CultureCode: CONSTANTS.CULTURE_CODE.CHT,
            SkuName: style.SkuNameCHT,
            SkuDesc: style.SkuDescCHT,
		  	SkuSizeComment: style.SkuSizeCommentCHT,
		    SkuFeature: style.SkuFeatureCHT,
            SkuMaterial: style.SkuMaterialCHT,
            SkuColor: sku.SkuColorCHT
          });
          var skuCulturePromises = SkuCultureDataArray.map(function(skuCulture) {
            return tr.queryExecute(skuCultureInsertSQL, skuCulture);
          });
          return Q.all(skuCulturePromises);
        }
				return;
      });
      return Q.all(promises);
    })
    .then(function() {
      console.log("2.4 update the rest sku.");
      //2.4 update the rest sku
      var promises = style.SkuList.map(function(sku) {
        if (sku.IsNewAdd != 1) {
          var skuCombin = Object.assign({}, sku, style); //combine style properties and sku properties into one object.if sku has the same property as style, style's will replace the sku's
          skuCombin.SkuNameInvariant = skuCombin.SkuNameEN;
          skuCombin.SkuDescInvariant = skuCombin.SkuDescEN;
		  skuCombin.SkuSizeCommentInvariant = skuCombin.SkuSizeCommentEN;
          skuCombin.SkuFeatureInvariant = skuCombin.SkuFeatureEN;
          skuCombin.SkuMaterialInvariant = skuCombin.SkuMaterialEN;
          skuCombin.SkuColorInvariant = skuCombin.SkuColorEN;
          return tr.queryExecute("UPDATE `Sku` SET `SkuCode`=:SkuCode, `Bar`=:Bar, `SkuNameInvariant`=:SkuNameInvariant, `SkuDescInvariant`=:SkuDescInvariant, `SkuSizeCommentInvariant`=:SkuSizeCommentInvariant, `SkuFeatureInvariant`=:SkuFeatureInvariant, `SkuMaterialInvariant`=:SkuMaterialInvariant, `SkuColorInvariant`=:SkuColorInvariant, `BrandId`=:BrandId, `BadgeId`=:BadgeId, `SeasonId`=:SeasonId, `SizeId`=:SizeId, `ColorId`=:ColorId, `ColorKey`=:ColorKey, `GeoCountryId`=:GeoCountryId, `LaunchYear`=:LaunchYear, `ManufacturerName`=:ManufacturerName, `PriceRetail`=:PriceRetail, `PriceSale`=:PriceSale, `SaleFrom`=:SaleFrom, `SaleTo`=:SaleTo, `AvailableFrom`=:AvailableFrom, `AvailableTo`=:AvailableTo, `WeightKg`=:WeightKg, `HeightCm`=:HeightCm, `WidthCm`=:WidthCm, `LengthCm`=:LengthCm, `QtySafetyThreshold`=:QtySafetyThreshold, `IsFeatured`=:IsFeatured WHERE SkuId=:SkuId", skuCombin)
        }
				return;
      });
      return Q.all(promises);
    })
    .then(function() {
      console.log("2.5 update the rest sku culture.");
      //2.5 update the rest sku culture
      console.log("update sku culture");
      var skuCultureUpdateSQL = 'UPDATE `SkuCulture` SET  `SkuName`=:SkuName, `SkuDesc`=:SkuDesc, `SkuSizeComment`=:SkuSizeComment, `SkuFeature`=:SkuFeature, `SkuMaterial`=:SkuMaterial, `SkuColor`=:SkuColor WHERE `SkuId`=:SkuId AND `CultureCode`=:CultureCode';
      var promises = style.SkuList.map(function(sku) {
        if (sku.IsNewAdd != 1) {
          var SkuCultureDataArray = [];
          SkuCultureDataArray.push({
            SkuId: sku.SkuId,
            CultureCode: CONSTANTS.CULTURE_CODE.EN,
            SkuName: style.SkuNameEN,
            SkuDesc: style.SkuDescEN,
		  	SkuSizeComment: style.SkuSizeCommentEN,
            SkuFeature: style.SkuFeatureEN,
            SkuMaterial: style.SkuMaterialEN,
            SkuColor: sku.SkuColorEN,
          });
          SkuCultureDataArray.push({
            SkuId: sku.SkuId,
            CultureCode: CONSTANTS.CULTURE_CODE.CHS,
            SkuName: style.SkuNameCHS,
            SkuDesc: style.SkuDescCHS,
			SkuSizeComment: style.SkuSizeCommentCHS,
            SkuFeature: style.SkuFeatureCHS,
            SkuMaterial: style.SkuMaterialCHS,
            SkuColor: sku.SkuColorCHS,
          });
          SkuCultureDataArray.push({
            SkuId: sku.SkuId,
            CultureCode: CONSTANTS.CULTURE_CODE.CHT,
            SkuName: style.SkuNameCHT,
            SkuDesc: style.SkuDescCHT,
			SkuSizeComment: style.SkuSizeCommentCHT,
            SkuFeature: style.SkuFeatureCHT,
            SkuMaterial: style.SkuMaterialCHT,
            SkuColor: sku.SkuColorCHT,
          });
          var skuCulturePromises = SkuCultureDataArray.map(function(skuCulture) {
            return tr.queryExecute(skuCultureUpdateSQL, skuCulture);
          });
          return Q.all(skuCulturePromises);
        }
				return;
      });
      return Q.all(promises);
    })
    .then(function() {
      if (isImageChanged) {
        console.log("3.1 edit product delete style images first");
        var deletePromies = tr.queryExecute('DELETE FROM StyleImage WHERE StyleCode=:StyleCode AND MerchantId=:MerchantId', style);
        //3.2store style images;
        console.log("3.2 store style images");
        var styleImageInsertSQL = 'INSERT INTO StyleImage (`MerchantId`, `StyleCode`, `ImageTypeCode`, `ImageTypeId`, `ColorKey`, `ProductImage`, `Position`) value (:MerchantId, :StyleCode, :ImageTypeCode, :ImageTypeId, :ColorKey, :ImageKey, :Position)';
        //3.2.1store feature images
        var featureImagePromises = style.FeaturedImageList.map(function(featuredImage) {
          featuredImage.MerchantId = style.MerchantId;
          featuredImage.StyleCode = style.StyleCode;
					featuredImage.ImageTypeCode = CONSTANTS.PRODUCT_IMG_TYPE_CODE.FEATURE;
	        featuredImage.ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.FEATURE;
          return tr.queryExecute(styleImageInsertSQL, featuredImage);
        });
        //3.2.2store description images
        var descriptionImagePromises = style.DescriptionImageList.map(function(descriptionImage) {
          descriptionImage.MerchantId = style.MerchantId;
          descriptionImage.StyleCode = style.StyleCode;
          descriptionImage.ImageTypeCode = CONSTANTS.PRODUCT_IMG_TYPE_CODE.DESC;
          descriptionImage.ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.DESC;
          return tr.queryExecute(styleImageInsertSQL, descriptionImage);
        });
        //3.2.3store color images
        var colorList = Object.keys(style.ColorImageListMap);
        var colorListPromises = colorList.map(function(colorKey) {
          var colorImageList = style.ColorImageListMap[colorKey];
          var colorPromises = colorImageList.map(function(colorImage) {
            colorImage.MerchantId = style.MerchantId;
            colorImage.StyleCode = style.StyleCode;
            colorImage.ImageTypeCode = CONSTANTS.PRODUCT_IMG_TYPE_CODE.COLOR;
            colorImage.ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.COLOR;
            colorImage.ColorKey = colorKey;
            return tr.queryExecute(styleImageInsertSQL, colorImage);
          });
          return Q.all(colorPromises);
        });
        return Q.all(deletePromies, colorListPromises, descriptionImagePromises, featureImagePromises);
      }
    })
    .then(function() {
      if (isCategoryChanged||isSkuListModifed) {
        //4. update sku category
        //4.1 delete sku category
        console.log("4.1 delete sku category");
        var deleteSkuPromises = style.SkuList.map(function(sku) {
          return tr.queryExecute('DELETE FROM SkuCategory WHERE SkuId=:SkuId', {
            SkuId: sku.SkuId
          });
        });
        //4.2 insert sku category
        console.log("4.2 insert sku category");
        var skuCategoryInsertSQL = "INSERT INTO SkuCategory(SkuId, CategoryId, Priority) VALUE(:SkuId, :CategoryId, :Priority)"
        var promises = style.SkuList.map(function(sku) {
          var skuCategoryPromises = style.CategoryPriorityList.map(function(skuCategory) {
            skuCategory.SkuId = sku.SkuId;
            return tr.queryExecute(skuCategoryInsertSQL, skuCategory);
          });
          return Q.all(skuCategoryPromises);
        });
        return Q.all(deleteSkuPromises, promises);
      }
    })
    .then(function() {
      console.log("product edit successfully!");
      return tr.commit();
    })
    .then(function() {
      res.json();
    })
    .catch(function(err) {
      res.status(500).json(cu.createErrorResult(err, "MSG_ERR_EDIT_PRODUCT"));
      console.log('Rollback');
      console.dir(err);
      return tr.rollback();
    })
    .done();

});


//handle file too large error or other event related error
router.use('/sku/sheet/upload', function (err, req, res, next) {
	res.status(500).json(cu.createErrorResult(err, "MSG_ERR_FILE_SIZE"));
});


module.exports = router;
