var Q=require('q');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var config=require('../config');

exports.createSimpleToken=function ()
{
	return crypto.randomBytes(16).toString('hex');
}

exports.createMobileCode=function ()
{
	return Math.floor(1000 + Math.random() * 9000);
}

exports.createSalt=function ()
{
	return crypto.randomBytes(16).toString('base64');
}

exports.createHash=function (clearText,salt)
{
	return crypto.pbkdf2Sync(clearText, salt, 4096, 64).toString('base64');
}

exports.createJwtToken=function(objIn)
{
	return jwt.sign(objIn, config.jwtSecret, { expiresIn: 60*60*24*7 });
}

exports.decodeJwtToken=function(tokIn)
{
	try {
		return jwt.verify(tokIn, config.jwtSecret);
	} catch(err) {
		return null;
	}
}

/**
 * @desc create 4 digits random number
 * @return String
**/
exports.createRandom4digits = function(){
	var randomDigits = Math.floor(Math.random()*10000).toString();
	var len = randomDigits.length;
	for(var i=0;i<4-len;i++){
		randomDigits="0"+randomDigits;
	}
	console.log(randomDigits);
	return randomDigits;
}
