var Q = require('q');
var fs = require('fs');
var yauzl = require('yauzl');
var pump = require('pump');
var throat = require('throat');

exports.unzip = function (filepath, destMapper) {
  var deferred = Q.defer();

  yauzl.open(filepath, { autoClose: false }, function (err, zipfile) {
    if (err) return deferred.reject(err);

    var entries = [];

    function extractEntry (entry) {
      var deferred = Q.defer();
      zipfile.openReadStream(entry, function (err, readStream) {
        if (err) return deferred.reject(err);
        pump(
          readStream,
          fs.createWriteStream(entry.destination),
          function (err) {
            if (err) return deferred.reject(err);
            deferred.resolve(entry);
          }
        );
      });
      return deferred.promise;
    }

    zipfile.on('entry', function (entry) {
      var dest = destMapper(entry);
      if (typeof dest !== 'string') return;

      entry.destination = dest;
      entries.push(entry);
    });

    zipfile.on('end', function () {
      Q.all(
        entries.map(throat(1, extractEntry))
      )
      .then(function () {
        deferred.resolve(entries);
      })
      .catch(function (err) {
        deferred.reject(err);
      })
      .then(function () {
        zipfile.close();
      });
    });

    zipfile.on('error', function (err) {
      deferred.reject(err);
    });
  });

  return deferred.promise;
};
