var Q=require('q');
var im = require("imagemagick");
var config=require('../config');
var fs = require('fs');

exports.resizeImage=function(imagePath, size){
	var deferred = Q.defer();
	
	var options = {
		width: size,
		height: size + "^", 
		customArgs: [
					 "-gravity", "center"
					,"-extent", size + "x" + size
					],
		srcPath: imagePath,
		dstPath: imagePath + "_" + size
	};

	im.resize(options, function(err) {
		if (err)
		{
			deferred.reject( {AppCode:"MSG_ERR_IMAGE_RESIZE"});
		}
		else
		{
			deferred.resolve(true);
		}
	});
	
	return deferred.promise;
	
};

exports.resizeImageSmart=function(imagePath, w, h){
	var deferred = Q.defer();
	
	var options = {
		width: w,
		height : h,
		customArgs: [
					 "-gravity", "center"
					,"-extent", w + "x" + h
					],
		srcPath: imagePath,
		dstPath: imagePath + "_" + w + "_" + h
	};


	var key = options.dstPath.split('/');
		key = key[ key.length - 1 ];

	if (fs.existsSync(options.dstPath)){
		//deferred.resolve({ ImageKey : key });
		deferred.resolve(options.dstPath);
	}else{
		im.resize(options, function(err) {
			if (err)
			{
				deferred.reject( {AppCode:"MSG_ERR_IMAGE_RESIZE"});
			}
			else
			{
				//deferred.resolve({ ImageKey : key });
				deferred.resolve(options.dstPath);
			}
		});		
	}

	return deferred.promise;
};
