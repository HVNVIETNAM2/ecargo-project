var moment = require('moment');
var Logger = {};
Logger.debugLevel = 'info';
Logger.log = function(level, message) {
	var levels = ['error', 'warn', 'info'];
	if (levels.indexOf(Logger.debugLevel) >=  levels.indexOf(level)) {
		if (typeof message !== 'string') {
			message = JSON.stringify(message);
		};
		console.log(moment().format('YYYY-MM-DD HH:mm:ss.SSS') + ' ' + level + ' ' + message + '\n');
	}
}
 
module.exports = Logger;

  