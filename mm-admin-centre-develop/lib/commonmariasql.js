var Client = require('mariasql');
var Q=require('q');



function Connection(configIn) {
  /*
  if (!configIn.charset)
	configIn.charset='utf8';
  configIn.multiStatements=true;
  configIn.compress =true;
  */
  if (!(this instanceof Connection)) {
    return new Connection(configIn);
  }
	
  //this.config = configIn;
  this.client = null;
  
};

var Pool = require('generic-pool').Pool;
var pool = null;

Connection.poolStart = function (config) {

	if (!config.charset)
		config.charset='utf8';
	config.multiStatements=true;
	
	var poolMaxSize=config.poolMaxSize || 10;
	var poolIdleTimeout=config.poolIdleTimeout || 60;
	console.log("Pool Start");
	pool = new Pool({
		name     : 'mariadb',
		create   : function(callback) {
			if (config==null)
				throw Error('config has not been set');
			var c = new Client(config);
			// parameter order: err, resource
			// new in 1.0.6
			//console.log('new conn created');
			callback(null, c);
		},
		destroy  : function(client) { client.end(); },
		max      : poolMaxSize,
		// optional. if you set this, make sure to drain() (see step 3)
		//min      : 2,
		// specifies how long a resource can stay idle in pool before being removed
		idleTimeoutMillis : poolIdleTimeout * 1000,
		 // if true, logs via console.log - can also be a function
		log : false
	});
}

Connection.poolStop = function () {
	pool.drain(function() {
		console.log("Pool Stop");
		pool.destroyAllNow();
	});
}

// Override mariasql Client._format_value for fixing number parameter in prepare the SQL
Client.prototype._format_value = function(v) {
  if (Buffer.isBuffer(v))
    return "'" + Client.escape(v.toString('utf8')) + "'";
  else if (Array.isArray(v)) {
    var r = [];
    for (var i = 0, len = v.length; i < len; ++i)
      r.push(this._format_value(v[i]));
    return r.join(',');
  } else if (v !== null && v !== undefined){
      if(typeof v === 'number'){
          return Client.escape(v + '');
      }
      else{
        return "'" + Client.escape(v + '') + "'";      
      }
  }
  return 'NULL';
};

Connection.prototype.queryMany = function query(sql,paramss) {
	var deferred = Q.defer();
	
	var fn=this.client.prepare(sql);
	var querySql=fn(paramss);
	this.client.query(querySql, function(err, rows){
		if (err)
		{
			err.sql=querySql;
			deferred.reject(err);
		}
		else
		{
			if (rows.info)
			{
				//Single Result Set
				convertRows(rows);
				//delete of info must occur after the convert
				delete rows.info;	
			}
			else
			{
				//Multiple Result Sets
				for(resultSet in rows)
				{
					
					convertRows(rows[resultSet]);
					delete rows.info;	
				}
			}
			
								
			deferred.resolve(rows);	
		}
	});
	return deferred.promise;
}

Connection.prototype.queryOne = function query(sql,paramss) {
	var deferred = Q.defer();
	var fn=this.client.prepare(sql);
	var querySql=fn(paramss);
	this.client.query(querySql, function(err, rows){
		if (err)
		{
			err.sql=querySql;
			deferred.reject(err);
		}
		else
		{
			if (rows.length>0)
			{
				convertRowSingle(rows);
				//delete of info must occur after the convert
				delete rows.info;
				deferred.resolve(rows[0]);
			}
			else
				deferred.resolve(null);							
		}	
	});
	return deferred.promise;
}

Connection.prototype.queryExecute = function query(sql,paramss) {
	var deferred = Q.defer();
	var fn=this.client.prepare(sql);
	var querySql=fn(paramss);
	this.client.query(querySql, function(err, rows){
		if (err)
		{
			err.sql=querySql;
			deferred.reject(err);
		}
		else
			deferred.resolve(rows.info);		
	});
	return deferred.promise;
}

Connection.prototype.begin=function begin(ReadOnly)
{
	var refe=this;
	var deferred = Q.defer();
	if (refe.client!=null)
		deferred.reject("Error: Begin already active.");
		
		
	pool.acquire(function(errPool, client) {
		if (errPool) {
			// handle error - this is generally the err from your
			// factory.create function
			deferred.reject(errPool);			
		}
		else {
			refe.client = client;
			var beginSql="start transaction";
			if (ReadOnly)
				beginSql=beginSql+" READ ONLY; SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;"
			refe.client.query(beginSql, function(err, rows){
				if (err)
					deferred.reject(err);
				else
					deferred.resolve(rows.info);		
			});			
		}
	});
		
	
	return deferred.promise;
}

Connection.prototype.commit=function commit() 
{
	var refe=this;
	var deferred = Q.defer();
	if (refe.client==null)
	{
		deferred.resolve(); //if no existing connection there can be no transaction so skip over this
	}
	else
	{
		
		refe.client.query("commit;", function(err, rows){
			//refe.client.end(); //this will close the connection once the query queue completes
			pool.release(refe.client);
			//refe.client=null;
			if (err)
				deferred.reject(err);
			else
			{			
				deferred.resolve(rows.info);		
			}
		});
		
	}
	return deferred.promise;
}

Connection.prototype.rollback=function commit() 
{
	var refe=this;
	var deferred = Q.defer();
	if (refe.client==null)
	{
		deferred.resolve();  //if no existing connection there can be no transaction so skip over this
	}
	else
	{
		refe.client.query("rollback;", function(err, rows){
			//refe.client.end(); //this will close the connection once the query queue completes
			pool.release(refe.client);
			//refe.client=null;
			if (err)
				deferred.reject(err);
			else
			{			
				deferred.resolve(rows.info);		
			}
		});			
	}
	return deferred.promise;
}

var convert = function(row, metadata) {
	//Convert row elements based on type info.
	var key, t;
	if (row==null)
		return null;
		
	for (key in row) {
		if (metadata[key] && metadata[key]['type']) //only convert if we have meta data
		{
			t = metadata[key]['type'];
			if (t === "DATE" || t === "DATETIME" || t === "TIMESTAMP") {
				if (row[key]!=null)
					row[key] = new Date(row[key]);			  
			}
			else if (t === "DECIMAL" || t === "DOUBLE" || t === "FLOAT") {
			  row[key] = parseFloat(row[key]);
			}
			else if (t === "BIT") {
			  row[key]=(row[key]=="\u0001"); //"\u0001"
			}
			else if (t === "INTEGER" || t === "TINYINT" || t === "SMALLINT" || t === "MEDIUMINT" || t === "BIGINT") {
			  row[key] = parseInt(row[key]);
			} 
		}
	}
	//return _results;
};

var convertRows = function(rows) {
	if (!rows.info)
		throw new Error("Invalid resultset, missing info object");
	for (var i in rows)
		convert(rows[i], rows.info.metadata)
	
};

var convertRowSingle = function(rows) {
	if (!rows.info)
		throw new Error("Invalid resultset, missing info object");
	convert(rows[0], rows.info.metadata)
};

module.exports = Connection;