var Q = require('q');
var _ = require('lodash');
var throat = require('throat');

var Excel = require('exceljs');
var XLSX = require("xlsx");
var pyspreadsheet = require('pyspreadsheet');

var cu = require('./commonutil.js');

var Factory = {};

Factory.toJson=function(fileLocation, sheetName){
	//return Factory.toJsonExcelJs(fileLocation, sheetName);
	//return Factory.toJsonXlsx(fileLocation, sheetName);
	return Factory.toJsonPySpread(fileLocation, sheetName);
	//DO NOT CHANGE EXCEL PARSER WITHOUT SPEAKING WITH ALBERT
}

Factory.toJsonExcelJs = function(fileLocation, sheetName){
	
	var deferred = Q.defer();
	var List = [];
	var workbook = new Excel.Workbook();

	var Header = [];

	Q.when()
	.then(function(){
		return workbook.xlsx.readFile(fileLocation)
		.catch(function(err){ throw { AppCode : 'MSG_ERR_EXCEL_FILE_CORRUPT' } });
	})
	.then(function() {
		var worksheet = workbook.getWorksheet(sheetName);
		if (worksheet == null)
			throw { AppCode : 'MSG_ERR_EXCEL_WORKSHEET_MISSING' };
		
		var row = worksheet.getRow(1);
		var i=1;
		while (i<1000)
		{
			var curCell=row.getCell(i);
			if (curCell)
			{
				var value=curCell.value;
				if (value)
				{
					var celluar= {};
					celluar.index=i;
					celluar.name=value.replace(/ /g, '');
					Header.push(celluar);
				}
			}
			i++;
			curCell=row.getCell(i);
		}
		
		worksheet.eachRow(function(rower, rowNumber){
			if(rowNumber > 1){
				var rowData = { rowNumber : rowNumber };
				for (var j in Header)					
				{
					var HeaderCol=Header[j];
					
					var cell=rower.getCell(HeaderCol.index);
					var value = cell.value;

					if( value && typeof value === 'object' ){
				      	value = value.result;
				    }

					rowData["Delete"] = "";
					if(cu.isBlank(value)){ value = ""; }
					rowData[HeaderCol.name] = value;
				};
				List.push(rowData);
			}
		});
	})
	.then(function() {
		deferred.resolve(List);
	})
	.catch(function(err){
		deferred.reject(err);
	})

	return deferred.promise;		
};

Factory.toJsonXlsx = function(fileLocation, sheetName){
	//if(!startRange){ startRange = 1; }
	startRange = 1;
	var deferred = Q.defer();
	var List = [];

	Q.when()
	.then(function(){
		var wb = XLSX.readFile(fileLocation);
		var ws = wb.Sheets[sheetName];

		function format_column_name(name) { return name.replace(/\s/g, ""); }
		var range = XLSX.utils.decode_range(ws['!ref']);
		var headers = [];
		
		for(var C = range.s.c; C <= range.e.c; ++C) {
		    var addr = XLSX.utils.encode_cell({r:range.s.r, c:C});
		    var cell = ws[addr];
		    if(!cell){ 
		    	headers.push(addr);
		    }else{
		    	headers.push( format_column_name(cell.v));
		    }
		}

		List = XLSX.utils.sheet_to_json(ws, { header : headers, range : startRange });
		return;
	})
	.then(function() {
		deferred.resolve(List);
	})
	.catch(function(err){
		deferred.reject(err);
	})

	return deferred.promise;
};

Factory.toJsonPySpread = function(fileLocation, sheetName){
	var SpreadsheetReader = pyspreadsheet.SpreadsheetReader;
	var deferred = Q.defer();
	var List = [];
	var Header = {};

	Q.when()
	.then(function(){
		return Q.nfapply(SpreadsheetReader.read, [fileLocation, { sheet : sheetName }]).then(function(workbook){
			// Iterate each row.
			workbook.sheets[sheetName].rows.forEach(function(row){
				var tmpData = { rowNumber : (row[0].row || 0) + 1 };
				// Iterate each cell. 
				row.forEach(function(cell){
					// check if current row is 0, then set it as a header. else add it as a content.
					if(cell.row == 0){
						var value = cell.value;

						// Check if value exist, if yes replace all spaces, else use the cell address 
						// as a default holder for blank columns.
						if(value){
							value = value.replace(/ /g,"");
						}else{
							value = cell.address;
						}

						Header[cell.column] = value;
					}else{
						tmpData[Header[cell.column]] = cell.value;
					}
				});

				// Check if rownumber is 1, ignore column header, else push to list container.
				if(tmpData.rowNumber > 1){
					List.push(tmpData);	
				}
			});			
		});
	})
	.then(function(){
		deferred.resolve(List);
	})
	.catch(function(err){
		deferred.reject(err);
	})
	return deferred.promise;
};

module.exports = Factory;
