var libphonenumber = require('google-libphonenumber');
var PNF = libphonenumber.PhoneNumberFormat;
var phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();

exports.parseNumber = function (MobileNumber, CountryCode) {
  try {
    var parsed = phoneUtil.parse(MobileNumber, CountryCode);
    if (phoneUtil.isValidNumberForRegion(parsed, CountryCode)) {
      return parsed.getNationalNumber();
    }
  } catch (e) {}
  // return false for invalid
  return false;
}
