var Q=require('q');
var config=require('../config');

exports.save=function (row,table)
{
	var Id=table + 'Id';
	var isNew=false;
	var sql='';
	
	if (row[Id]==0)
		isNew=true;
	
	if (isNew)
	{
		var sqlCols='';	
		var sqlVals='';	
		for(var prop in row) {
			if (prop!=Id)
			{
				sqlCols = sqlCols + "," + prop;
				sqlVals = sqlVals + ",:" + prop;				
			}
		}
		sqlCols = sqlCols.substr(1);
		sqlVals = sqlVals.substr(1);
		sql="insert into " + table + "(" + sqlCols + ") values (" + sqlVals + ");"
	}
	else
	{
		for(var prop in row) {
		   sql = sql + "," + prop + "=:" + prop;		
		}
		sql = sql.substr(1);
		sql="update " + table + " set " + sql + " where " + Id + "=:" + Id; 
	}
	return sql;
}