"use strict";
var Q=require('q');
var config=require('../config');
var clog=require('./commonlog');

exports.objectEqualByValues = function (obj1, obj2) {
  var leftChain, rightChain;

  function compare2Objects (x, y) {
    var p;

    if (isNaN(x) && isNaN(y) && typeof x === 'number' && typeof y === 'number') {
         return true;
    }

    if (x === y) {
        return true;
    }

    if ((typeof x === 'function' && typeof y === 'function') ||
       (x instanceof Date && y instanceof Date) ||
       (x instanceof RegExp && y instanceof RegExp) ||
       (x instanceof String && y instanceof String) ||
       (x instanceof Number && y instanceof Number)) {
        return x.toString() === y.toString();
    }

    if (!(x instanceof Object && y instanceof Object)) {
        return false;
    }

    if (x.isPrototypeOf(y) || y.isPrototypeOf(x)) {
        return false;
    }

    if (x.constructor !== y.constructor) {
        return false;
    }

    if (x.prototype !== y.prototype) {
        return false;
    }

    if (leftChain.indexOf(x) > -1 || rightChain.indexOf(y) > -1) {
         return false;
    }
    
    for (p in y) {
        if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
            return false;
        }
        else if (typeof y[p] !== typeof x[p]) {
            return false;
        }
    }

    for (p in x) {
        if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
            return false;
        }
        else if (typeof y[p] !== typeof x[p]) {
            return false;
        }

        switch (typeof (x[p])) {
            case 'object':
            case 'function':

                leftChain.push(x);
                rightChain.push(y);

                if (!compare2Objects (x[p], y[p])) {
                    return false;
                }

                leftChain.pop();
                rightChain.pop();
                break;

            default:
                if (x[p] !== y[p]) {
                    return false;
                }
                break;
        }
    }

    return true;
  }

  if (arguments.length < 1) {
    return true; 
  }

  leftChain = []; 
  rightChain = [];

  if (!compare2Objects(obj1, obj2)) {
      return false;
  }

  return true;
};


// exports.objectEqualByValues = function(w,x)
// {
//     for(p in w)
//     {
//     	switch(typeof(w[p]))
//     	{
//     		case 'object':
//     			if (!w[p].equals(x[p])) { return false }; break;
//     		case 'function':
//     			if (typeof(x[p])=='undefined' || (p != 'equals' && w[p].toString() != x[p].toString())) { return false; }; break;
//     		default:
//     			if (w[p] != x[p]) { return false; }
//     	}
//     }

//     for(p in x)
//     {
//     	if(typeof(w[p])=='undefined') {return false;}
//     }

//     return true;
// }

exports.createErrorResult=function (err,defaultMessage)
{
		var resErr={};
		
		if (err.AppCode)
			resErr.AppCode=err.AppCode
		else
			resErr.AppCode=defaultMessage;
		
		resErr.Message=err.message || err.Message;
		
		//special cases
		if (err.sql) //sql error
			resErr.Details=err.sql;
		else if (err.body && err.displayName) //elastic error
			resErr.Details=err.body;
		else if (err.response && err.response.text && err.response.error) //superagent error
		{
			let ej=JSON.parse(err.response.text); 
			if (ej.AppCode)
			{
				//if the error that came back from the http service call is one with an AppCode then just doa striaght pass through
				resErr.AppCode=ej.AppCode;
				resErr.Message=ej.Message;
				resErr.Details=ej.Details;
			}
			else
			{
				resErr.Details=err.response.text;
			}
		}
		else
			resErr.Details=err.stack;
                    
                clog.log('error',resErr);
                    
                // MM-4377--[API] Save Sku - unauthorize calls show too much details in response (security concern)    
                if ('MSG_ERR_USER_UNAUTHORIZED' === resErr.AppCode) {
                    delete resErr.Message;
                    delete resErr.Details;
                }
		return resErr;	
}

exports.createErrorResultSimple=function (defaultMessage)
{
		var resErr={};
		resErr.AppCode=defaultMessage;
		resErr.Message='';
		resErr.Details='';
		clog.log('error',resErr);
		return resErr;	
}

exports.checkIdBlank=function (idValue)
{
	return !idValue || idValue==='' || idValue==='0';
}

exports.isBlank=function (valer)
{
	if (valer===null || valer===undefined || valer==='')
		return true;
	else
		return false;
}

exports.isUUID = function (id) {
  return typeof id === 'string' && id.length === 36;
}

exports.reqUserId = function (req) {
  return req.query.UserId || req.query.userid || req.query.userId ||
    (req.body && req.body.UserId) || req.query.id;
}

exports.reqUserKey = function (req) {
  return req.query.UserKey || req.query.userkey || req.query.userKey || 
    (req.body && req.body.UserKey) || req.query.key;
}
exports.reqUser = function (req) {
  return exports.reqUserKey(req) || exports.reqUserId(req);
}
exports.isReqSelf = function (req) {
  var UserId = exports.reqUserId(req);
  var UserKey = exports.reqUserKey(req);
  return UserId == req.AccessUser.UserId || UserKey == req.AccessUser.UserKey;
}
exports.split = function (data, sep) {
  if (!data) return [];
  if (Array.isArray(data)) return data;
  if (typeof data !== 'string') return [data];
  return data.split(sep || ',').map(function (str) {
    return str.trim();
  });
};
