"use strict";
var CONSTANTS = require('../logic/constants');

var express = require('express'), router = express.Router();
var fs = require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');
var ce = require('../lib/commonemail.js');
var cm = require('../lib/commonmariasql.js');
var throat = require('throat');
var util = require('util');


var Factory = {};

Factory.convertTree = function (adjacencyArray) {
    var thing = {};
    for (var i = 0; i < adjacencyArray.length; i++) {
        var obj = adjacencyArray[i];
        obj.CategoryList = [];
        thing[obj.CategoryId] = obj;
        var parent = obj.ParentCategoryId || '~';
        if (!thing[parent]) {
            thing[parent] = {
                CategoryList: []
            };
        }
        thing[parent].CategoryList.push(obj);
    }
    return thing['~'].CategoryList;
}

Factory.categoryPath = function (adjacencyArray, PrimaryCategoryId) {
    var PrimaryCategoryPathList = [];
    var obj = _.find(adjacencyArray, 'CategoryId', PrimaryCategoryId);
    while (obj && obj.ParentCategoryId != null && obj.ParentCategoryId != obj.CategoryId) {
        PrimaryCategoryPathList.push(obj);
        obj = _.find(adjacencyArray, 'CategoryId', obj.ParentCategoryId);

    }
    PrimaryCategoryPathList = _(PrimaryCategoryPathList).reverse().value();
    for (var ss in PrimaryCategoryPathList)
        PrimaryCategoryPathList[ss].Level = parseInt(ss) + 1;
    return PrimaryCategoryPathList;
}

Factory.subtreeCategoryArray = function (obj) {
    var result = [];

    function go(cur) {
        result.push(cur.CategoryId);
        for (var i = 0, l = cur.CategoryList.length; i < l; i++)
            go(cur.CategoryList[i]);
    }

    go(obj);
    return result;
}

Factory.findSubtree = function (source, id) {
    for (var key in source) {
        var item = source[key];
        if (item.CategoryId == id)
            return item;
        // Item not returned yet. Search its children by recursive call.
        if (item.CategoryList.length > 0) {
            var subresult = Factory.findSubtree(item.CategoryList, id);
            // If the item was found in the subchildren, return it.
            if (subresult)
                return subresult;
        }
    }
    // Nothing found return null.
    return null;
}

var LIST_SQL = [
    "SELECT cat.CategoryId, cat.CategoryCode, cat.ParentCategoryId, cat.IsMerchCanSelect, cat.Priority, catc.CategoryName, cat.CategoryNameInvariant, cat.CategoryImage, ifnull(SCount, 0) as SkuCount, cat.StatusId from Category cat",
    "left join CategoryCulture as catc on cat.CategoryId=catc.CategoryId and catc.CultureCode=:CultureCode",
    "left join (select CategoryId, count(*) as SCount from SkuCategory group by CategoryId) as skuc on skuc.CategoryId = cat.CategoryId",
    "where cat.StatusId <> 1"
].join('\n');

Factory.listAdjacency = function (CultureCode, showAll, tr) {
    var deferred = Q.defer();
    var CategoryList = null;
    Q.when()
        .then(function () {
            if (cu.isBlank(CultureCode))
                throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
        })
        .then(function () {
            var sqlQuery = LIST_SQL;

            if (!showAll) sqlQuery += " and cat.StatusId = 2";
            sqlQuery += " order by cat.ParentCategoryId, cat.Priority";
            return tr.queryMany(sqlQuery, {CultureCode: CultureCode}).then(function (data) {
                CategoryList = data;
            });
        })
        .then(function () {
            deferred.resolve(CategoryList);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();
    return deferred.promise;
};

Factory.getCategoriesUnder = function (CategoryId, tr) {
    var deferred = Q.defer();
    var Result = null;
    Q.when()
        .then(function () {
            if (cu.isBlank(CategoryId))
                throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
        })
        .then(function () {
            var sqlQuery = "SELECT cat.CategoryId, cat.ParentCategoryId, cat.IsMerchCanSelect, cat.Priority, cat.CategoryNameInvariant as CategoryName, cat.CategoryImage from Category cat order by cat.ParentCategoryId, cat.Priority";
            return tr.queryMany(sqlQuery, {}).then(function (data) {
                var tree = Factory.convertTree(data);
                var subtree = Factory.findSubtree(tree, CategoryId);
                Result = Factory.subtreeCategoryArray(subtree);
            });
        })
        .then(function () {
            deferred.resolve(Result);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();
    return deferred.promise;
};

Factory.updatePosition = function (CategoryId, newParent, newPos, tr) {
    var deferred = Q.defer();

    Q.when()
        .then(function () {
            newPos = parseInt(newPos);
            if (!newPos) throw new Error('Invalid Priority');
        })
        .then(function () {
            return tr.queryOne('select * from Category where CategoryId=:CategoryId', {
                CategoryId: CategoryId
            })
        })
        .then(function (Category) {
            if (!Category) throw new Error('CategoryId not exists');

            // same position and parent, no need update
            if (
              Category.Priority == newPos &&
              Category.ParentCategoryId == newParent
            ) return true;
    
            var params = {
                ParentCategoryId: Category.ParentCategoryId,
                Priority: Category.Priority
            };
            
            // move up priority under old parent
            return tr.queryExecute(
              'update Category set Priority = Priority - 1 ' +
              'where ParentCategoryId=:ParentCategoryId and StatusId <> 1 and ' +
              'Priority >= :Priority',
              params
            )
            .then(function () {
              // move down priority under new parent
              
                var params = {
                    Priority: newPos,
                    ParentCategoryId: newParent
                };

              return tr.queryExecute(
                'update Category set Priority = Priority + 1 ' +
                'where ParentCategoryId=:ParentCategoryId and StatusId <> 1 and ' +
                'Priority >= :Priority',
                params
              );
            })
            .then(function () {
              // swap parent and priority
              Category.Priority = newPos;
              Category.ParentCategoryId = newParent;
              return tr.queryExecute(
                'update Category set Priority=:Priority, ParentCategoryId=:ParentCategoryId where CategoryId=:CategoryId',
                Category
              );
            });
        })
        .then(function (result) {
            deferred.resolve(result);
        })
        .catch(function (err) {
            deferred.reject(err);
        });

    return deferred.promise;
};

Factory.getCategory = function (CategoryId, tr) {
    var deferred = Q.defer();
    var Category = null;
    Q.when()
        .then(function () {
            if (cu.isBlank(CategoryId))
                throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
        })
        .then(function () {
            //this sql include the ParentCategoryCode
            //1.get category object
            var sqlQuery = "SELECT c1.*, c2.CategoryCode ParentCategoryCode FROM Category c1 inner join Category c2 on(c1.ParentCategoryId = c2.CategoryId) Where c1.CategoryId=:CategoryId";
            return tr.queryOne(sqlQuery, {CategoryId: CategoryId}).then(function (data) {
                Category = data;
            });
        })
        .then(function () { //1.2.get Category cultrue
            return tr.queryMany("select * from CategoryCulture where CategoryId=:CategoryId", {CategoryId: CategoryId}).then(function (data) {
                var CategoryCultureList = data;
                _.forEach(CategoryCultureList, function (CategoryCulture) {
                    if (CategoryCulture.CultureCode === 'EN') {
                        Category.CategoryNameEN = CategoryCulture.CategoryName;
                        Category.CategoryDescEN = CategoryCulture.CategoryDesc;
                        Category.CategoryMetaKeywordEN = CategoryCulture.CategoryMetaKeyword;
                        Category.CategoryMetaDescEN = CategoryCulture.CategoryMetaDesc;
                        Category.SizeGridImageEN = CategoryCulture.SizeGridImage
                    } else if (CategoryCulture.CultureCode === 'CHS') {
                        Category.CategoryNameCHS = CategoryCulture.CategoryName;
                        Category.CategoryDescCHS = CategoryCulture.CategoryDesc;
                        Category.CategoryMetaKeywordCHS = CategoryCulture.CategoryMetaKeyword;
                        Category.CategoryMetaDescCHS = CategoryCulture.CategoryMetaDesc;
                        Category.SizeGridImageCHS = CategoryCulture.SizeGridImage
                    } else if (CategoryCulture.CultureCode === 'CHT') {
                        Category.CategoryNameCHT = CategoryCulture.CategoryName;
                        Category.CategoryDescCHT = CategoryCulture.CategoryDesc;
                        Category.CategoryMetaKeywordCHT = CategoryCulture.CategoryMetaKeyword;
                        Category.CategoryMetaDescCHT = CategoryCulture.CategoryMetaDesc;
                        Category.SizeGridImageCHT = CategoryCulture.SizeGridImage
                    }
                })
            });
        })
        .then(function () {
            deferred.resolve(Category);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();
    return deferred.promise;

};

Factory.listCategoryBrandMerchants = function (tr) {
    var deferred = Q.defer();
    var CategoryBrandList = null;
    Q.when()
       .then(function () {
            //1.get category brand object
            var sqlQuery = "Select * From CategoryBrandMerchant Order By Priority";
            return tr.queryMany(sqlQuery, {}).then(function (data) {
                CategoryBrandList = data;
            });
        })
        .then(function () {
            deferred.resolve(CategoryBrandList);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();
    return deferred.promise;
};

Factory.getCategoryBrandMerchants = function (CategoryId, tr) {
    var deferred = Q.defer();
    var CategoryBrandList = null;
    Q.when()
        .then(function () {
            if (cu.isBlank(CategoryId))
                throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
        })
        .then(function () {
            //1.get category brand object
            var sqlQuery = "Select * From CategoryBrandMerchant Where CategoryId=:CategoryId Order By Priority";
            return tr.queryMany(sqlQuery, {CategoryId: CategoryId}).then(function (data) {
                CategoryBrandList = data;
            });
        })
        .then(function () {
            deferred.resolve(CategoryBrandList);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();
    return deferred.promise;
};

Factory.checkIfCategoryUsed = function (tr, CategoryId, StatusId) {

    var deferred = Q.defer();
    var flag = true; //false-category is not used; true-category is used.
    Q.when()
        //no need to check product
        /**
        .then(function () {
            if (StatusId === CONSTANTS.STATUS.INACTIVE) {
                //1.check if there are product (exclude deleted) under this category
                return tr.queryOne("select count(*) Count " +
                    "from Category c " +
                    "inner join SkuCategory sc on c.CategoryId = sc.CategoryId " +
                    "inner join Sku s on s.SkuId = c.SkuId " +
                    "where s.StatusId<>:StatusId and c.CategoryId=:CategoryId", {
                    CategoryId: CategoryId,
                    StatusId: CONSTANTS.STATUS.DELETED
                }).then(function (data) {
                    if (data.Count === 0) {
                        flag = false;
                    }
                });
            }
        })
         **/
        .then(function () {
            if (StatusId === CONSTANTS.STATUS.ACTIVE) {
                //All parent categories are "Active
                return tr.queryOne("select count(*) Count " +
                    "from Category " +
                    "where p.StatusId = :StatusId and " +
                    "p.CategoryId in (select CategoryId from Category where ParentCategoryId=:CategoryId) ", {
                    CategoryId: CategoryId,
                    StatusId: CONSTANTS.STATUS.ACTIVE
                }).then(function (data) {
                    if (data.Count === 0) {
                        flag = false;
                    }
                });
            } else {
                //No child category is "Active"
                return tr.queryOne("select count(*) Count " +
                    "from Category " +
                    "where ParentCategoryId=:CategoryId and StatusId = :StatusId", {
                    CategoryId: CategoryId,
                    StatusId: CONSTANTS.STATUS.ACTIVE
                }).then(function (data) {
                    if (data.Count > 0) {
                        flag = false;
                    }
                });
            }
        })
        .then(function () {
            deferred.resolve(flag);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();

    return deferred.promise;
};

// Factory function to create Category object.
Factory.createCategory = function(categoryId, categoryCode, categoryName, defaultRate, newRate) {
    var categoryObject = (function () {
        var my = {};
        my.categoryId = categoryId;
        my.categoryCode = categoryCode;
        my.categoryName = categoryName;
        my.defaultRate = defaultRate;
        my.newRate = newRate;
        my.children = [];

        // Recursive function to generate the category list. "acc" is an accumulator variable to hold intermediate result.
        /**
         my.generateCategoryListOld = function(acc, parentCategoryListObj) {
			var categoryString = '';
			if (parentCategoryListObj) {
				categoryString = parentCategoryListObj.categoryString + ' > ' + this.categoryName;
			} else {
				categoryString = this.categoryName;
			}
			var categoryListObj = null;
			// Only add valid categories (i.e. skip the dummy root (id=0))
			if (this.categoryId > 0) {
				categoryListObj = {
					categoryId: this.categoryId,
					categoryString: categoryString,
					defaultRate: this.defaultRate,
					newRate: this.newRate
				};
				acc.push(categoryListObj);
			}
			_.each(this.children, function(subCat) {
				subCat.generateCategoryList(acc, categoryListObj);
			});
		};
         **/

        my.generateCategoryList = function () {
            var stack = [this];
            var categoryString = '';
            var finalCategoryList = [];

            while (stack.length > 0) {
                var nodeList = stack.splice(0, 1);  // pop
                var node = nodeList[0];

                if (node.parentCategoryListObj) {
                    categoryString = node.parentCategoryListObj.categoryString + ' > ' + node.categoryName;
                } else {
                    categoryString = node.categoryName;
                }

                var categoryListObj = null;

                if (node.categoryId > 0) {
                    categoryListObj = {
                        categoryId: node.categoryId,
                        categoryString: categoryString,
                        defaultRate: node.defaultRate,
                        newRate: node.newRate
                    };
                    finalCategoryList.push(categoryListObj);
                }

                _.eachRight(node.children, function (child) {
                    // Add the parent's categoryListObj to the child
                    child.parentCategoryListObj = categoryListObj;
                    stack.unshift(child);  // push child to the stack
                });
            }

            return finalCategoryList;
        };

        return my;
    }());

    return categoryObject;
};

module.exports = Factory;
