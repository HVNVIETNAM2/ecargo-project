"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');
var li = require('./logicinventory.js');
var lprodimg = require ('./logicproductimage.js');
var lwish = require('./logicwishlist.js');

var crypto = require('crypto');

var Factory = {};

Factory.getCartLogic = Q.async(function * (tr, CultureCode, CartKey, UserKey, CreateNew){
	var cart = null;
	var User =  null;

	if(CartKey){
		cart = yield tr.queryOne('select CartKey, CartId, UserId from Cart where CartKey=:CartKey and StatusId=:StatusId and CartTypeId=:CartTypeId',{CartKey:CartKey,StatusId:2,CartTypeId:1});
	}else if(UserKey){
		User = yield tr.queryOne('select UserId from User where UserKey=:UserKey',{UserKey:UserKey});
		if (!User)
			throw { AppCode: 'MSG_ERR_USER_NOT_FOUND', Message : "User not found." };
		cart = yield tr.queryOne('select CartKey, CartId, UserId from Cart where CartId=(select max(CartId) as CartId from Cart where UserId=:UserId and StatusId=:StatusId and CartTypeId=:CartTypeId)',{UserId:User.UserId,StatusId:2,CartTypeId:1});
	}

	if(!cart){
		if(CreateNew){
			var ret=yield tr.queryExecute('insert into Cart (UserId, CartTypeId, CartKey, StatusId, LastModified, LastCreated) values (:UserId,:CartTypeId,(SELECT uuid()),:StatusId,UTC_TIMESTAMP(),UTC_TIMESTAMP())',{UserId:((User && User.UserId) || 0),CartTypeId:1,StatusId:2});
			cart=yield tr.queryOne('select CartKey, CartId, UserId from Cart where CartId=:CartId',{CartId:ret.insertId});
			return cart;
		}else{
			throw { AppCode: 'MSG_ERR_CART_NOT_FOUND', Message : "Cart not Found." };
		}
	}else{
		return cart;
	}
});

Factory.create = Q.async(function* (tr, CultureCode, UserKey) {
	var User = { UserId : 0 };
	if(!(UserKey == 0 || UserKey == "0")){
		User=yield tr.queryOne('select * from User where UserKey=:UserKey',{UserKey:UserKey});
		if (!User)
			throw { AppCode: 'MSG_ERR_USER_NOT_FOUND', Message : "User not found." };
	}
	//var CartKey = yield crypto.pseudoRandomBytes(16).toString('hex');
	var ret=yield tr.queryExecute('insert into Cart (UserId, CartTypeId, CartKey, StatusId, LastModified, LastCreated) values (:UserId,:CartTypeId,(SELECT uuid()),:StatusId,UTC_TIMESTAMP(),UTC_TIMESTAMP())',{UserId:User.UserId,CartTypeId:1,StatusId:2});
	var cart=yield tr.queryOne('select CartKey, CartId from Cart where CartId=:CartId',{CartId:ret.insertId});
	cart=Factory.viewByCartKey(tr,CultureCode,cart.CartKey);
	return cart;
});

Factory.userUpdate = Q.async(function* (tr, CultureCode, CartKey, UserKey) {
	var User=yield tr.queryOne('select * from User where UserKey=:UserKey',{UserKey:UserKey});
	if (!User)
		throw { AppCode: 'MSG_ERR_USER_NOT_FOUND', Message : "User not found." };
	var ret=yield tr.queryExecute('update Cart set UserId=:UserId where CartKey=:CartKey',{CartKey:CartKey,UserId:User.UserId});
	var cart=Factory.viewByCartKey(tr,CultureCode,CartKey);
	return cart;
});

Factory.itemAdd = Q.async(function* (tr, CultureCode, CartKey, UserKey, SkuId, Qty) {
	var cart = yield Factory.getCartLogic(tr, CultureCode, CartKey, UserKey, true);
	var checkIfExists = yield tr.queryOne('select * from CartItem where SkuId=:SkuId and CartId=:CartId and StatusId=:StatusId', { SkuId : SkuId, CartId : cart.CartId, StyleCode : null, ColorKey : null, StatusId : 2 });
	if(checkIfExists){
		yield tr.queryExecute('update CartItem set Qty = Qty + :Qty where CartItemId=:CartItemId', { Qty : Qty, CartItemId : checkIfExists.CartItemId });
	}else{
		yield tr.queryExecute('insert into CartItem (CartId,SkuId,Qty,LastModified, LastCreated, StyleCode, ColorKey, StatusId) values (:CartId,:SkuId,:Qty,UTC_TIMESTAMP(),UTC_TIMESTAMP(),:StyleCode,:ColorKey,:StatusId)',{CartId:cart.CartId,SkuId:SkuId,Qty:Qty,StyleCode:null,ColorKey:null,StatusId:2});
	}
	cart=Factory.viewByCartKey(tr,CultureCode,cart.CartKey);
	return cart;
});

Factory.itemRemove = Q.async(function* (tr, CultureCode, CartKey, UserKey, CartItemId) {
	var cart = yield Factory.getCartLogic(tr, CultureCode, CartKey, UserKey, false);
	var ret=yield tr.queryExecute('update CartItem set StatusId=:StatusId where CartId=:CartId and CartItemId=:CartItemId',{CartId:cart.CartId,CartItemId:CartItemId,StatusId:1});
	cart=Factory.viewByCartKey(tr,CultureCode,cart.CartKey);
	return cart;
});

Factory.itemUpdate = Q.async(function* (tr, CultureCode, CartKey, UserKey, CartItemId, SkuId, Qty) {
	var cart = yield Factory.getCartLogic(tr, CultureCode, CartKey, UserKey, false);

	var sql='';
	var Bale={};
	Bale.CartId=cart.CartId;
	Bale.CartItemId=CartItemId;
	
	if (!cu.isBlank(SkuId))
	{
		sql+=', SkuId=:SkuId ';
		Bale.SkuId=SkuId;
	}
	
	if (!cu.isBlank(Qty))
	{
		sql+=', Qty=:Qty ';
		Bale.Qty=Qty;
	}
	
	if (cu.isBlank(sql))
		throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "Neither SkuID or Qty were specified, atleast one is required for an update" };
	
	sql=sql.substr(1);//trim leading comma
	
	sql='update CartItem set ' + sql + ' where CartId=:CartId and CartItemId=:CartItemId';
	var ret=yield tr.queryExecute(sql,Bale);
	cart=Factory.viewByCartKey(tr,CultureCode,cart.CartKey);
	return cart;
});

Factory.viewByCartKey = Q.async(function* (tr, CultureCode, CartKey) {
	var cart = yield Factory.getCartLogic(tr, CultureCode, CartKey, null, false);
	cart.UserKey = null;
	var merchantSql="select s.MerchantId, ifnull(mc.MerchantName,m.MerchantNameInvariant) as MerchantName,m.MerchantNameInvariant, m.SmallLogoImage as MerchantImage, MAX(ci.LastModified) as LastModified from CartItem ci inner join  Sku s on (ci.SkuId=s.SkuId) inner join Merchant m on (s.MerchantId=m.MerchantId) left outer join MerchantCulture mc on (m.MerchantId=mc.MerchantId and mc.CultureCode=:CultureCode) where ci.CartId=:CartId and ci.StatusId=:StatusId group by s.MerchantId order by MAX(ci.LastModified) DESC";
	var MerchantList=yield tr.queryMany(merchantSql,{CultureCode:CultureCode,CartId:cart.CartId,StatusId:2});
	var itemSql="select s.SkuId, ci.CartItemId, ci.Qty, ci.LastModified, s.StyleCode, s.SkuCode, s.Bar, s.BrandId,bc.BrandName,b.BrandNameInvariant,b.SmallLogoImage as BrandImage, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, s.GeoCountryId, s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, s.QtySafetyThreshold, s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, scat.CategoryId as PrimaryCategoryId, sz.SizeNameInvariant as SizeName,s.ColorKey, clc.ColorName, scu.SkuColor, scu.SkuName";
	var itemSql=itemSql+" from Sku s inner join CartItem ci on (s.SkuId=ci.SkuId) inner join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) inner join Brand b on (s.BrandId=b.BrandId) inner join BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) inner join Size sz on (s.SizeId=sz.SizeId) inner join Color cl on (s.ColorId=cl.ColorId) inner join ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) inner join SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0) inner join Status st on (s.StatusId=st.StatusId) inner join StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) where s.StatusId <> :DeletedStatusId and ci.CartId=:CartId and ci.StatusId=:StatusId and s.MerchantId=:MerchantId order by ci.LastModified DESC";
	for (var Merchant of MerchantList) {
	  var ItemList=yield tr.queryMany(itemSql,{CultureCode:CultureCode,CartId:cart.CartId,StatusId:2,MerchantId:Merchant.MerchantId,DeletedStatusId: CONSTANTS.STATUS.DELETED});
	  Merchant.ItemList=ItemList;
	  for (var Item of ItemList)
	  {
		var summary=yield li.getSkuInventorySummary(Item.SkuId, CultureCode, tr);
		var img=yield lprodimg.getDefaultImage(Item.SkuId, tr);
		Item.LocationCount = summary.LocationCount;
		Item.QtyAts = summary.QtyAts;
		Item.InventoryStatusId = summary.InventoryStatusId;
		Item.ProductImage=img.ProductImage
	  }
	}
	var cartMerchant=null;
	cart.MerchantList=MerchantList;
	
	if(cart.UserId == 0){
		cart.UserKey = 0;
	}else{
		var User=yield tr.queryOne('select * from User where UserId=:UserId',{UserId:cart.UserId});
		if(User){
			cart.UserKey = User.UserKey;
		}else{
			cart.UserKey = cart.UserId;
		}
	};

	delete cart.UserId; //to hide the userId
	delete cart.CartId; //to hide the cartId
	return cart;
});

Factory.viewByUserKey = Q.async(function* (tr, CultureCode, UserKey) {
	var User=yield tr.queryOne('select * from User where UserKey=:UserKey',{UserKey:UserKey});
	if (!User)
		throw { AppCode: 'MSG_ERR_USER_NOT_FOUND', Message : "User not found." };

	// use CartTypeId = 1 for basket.
	var ret=yield tr.queryOne('select CartKey, CartId from Cart where CartId=(select max(CartId) as CartId from Cart where UserId=:UserId and StatusId=:StatusId and CartTypeId=:CartTypeId)',{UserId:User.UserId,StatusId:2,CartTypeId:1});
	if(!ret || isNaN(ret.CartId) || !ret.CartId){
		throw { AppCode: 'MSG_ERR_CART_NOT_FOUND', Message : "Cart not Found." };
	};
	var cart=Factory.viewByCartKey(tr,CultureCode,ret.CartKey);
	return cart;
});


Factory.wishlistLogic = Q.async(function* (tr, CultureCode, CartKey, UserKey, CartItemId, WishlistKey){
	var cart = yield Factory.getCartLogic(tr, CultureCode, CartKey, UserKey, false);
	var User = null;
	var WishlistObj = null;

	if(WishlistKey){
		WishlistObj = yield tr.queryOne('select CartKey, CartId, UserId from Cart where CartKey=:WishlistKey and StatusId=:StatusId and CartTypeId=:CartTypeId',{WishlistKey:WishlistKey,StatusId:2,CartTypeId:2});
		if(!WishlistObj)
			throw { AppCode : 'MSG_ERR_WISHLIST_NOT_FOUND', Message : "Wishlist not found." };
	}else{
		if(cu.checkIdBlank(cart.UserId)){
			//create new wishlist for userid=0
			WishlistObj = yield lwish.create(tr,CultureCode,0);
		}else{
			User=yield tr.queryOne('select * from User where UserId=:UserId',{UserId:cart.UserId});
			if (!User)
				throw { AppCode: 'MSG_ERR_USER_NOT_FOUND', Message : "User not found." };
		}
	}

	
	var item=yield tr.queryOne('select * from CartItem where CartId=:CartId and CartItemId=:CartItemId and StatusId=:StatusId',{CartId:cart.CartId,StatusId:2,CartItemId:CartItemId});
	if (!item)
		throw { AppCode: 'MSG_ERR_CART_ITEM_NOT_FOUND', Message : "Cart item not found." };

	//get Sku using SkuId from item to validate
	var Sku=yield tr.queryOne('select * from Sku where SkuId=:SkuId',{SkuId : item.SkuId});
	if(!Sku)
		throw { AppCode: 'MSG_ERR_SKU_NOT_FOUND', Message : "Sku not found." };

	var Bale = Sku;
	if (cu.isBlank(Bale.StyleCode))
		throw { AppCode: 'MSG_ERR_STYLECODE_REQUIRED', Message : "StyleCode is required." };

	if (cu.checkIdBlank(Bale.MerchantId))
		throw { AppCode: 'MSG_ERR_MERCHANTID_REQUIRED', Message : "MerchantId is required." };

	if(!WishlistObj){
		var wishlist = yield tr.queryOne('select CartKey, CartId from Cart where CartId=(select max(CartId) as CartId from Cart where UserId=:UserId and StatusId=:StatusId and CartTypeId=:CartTypeId)',{UserId:cart.UserId,StatusId:2,CartTypeId:2});
		WishlistObj = wishlist;

		if(!wishlist || isNaN(wishlist.CartId) || !wishlist.CartId){
			WishlistObj = yield lwish.create(tr,CultureCode,User.UserKey);
		}
	}

	var add = lwish.itemAdd(tr,CultureCode,WishlistObj.CartKey,null,Bale.StyleCode,Bale.SkuId,Bale.ColorKey,Bale.MerchantId);
	return { wishlist : add, item : item, cart : cart, WishlistKey : WishlistObj.CartKey };
});

Factory.addToWishList = Q.async(function * (tr, CultureCode, CartKey, UserKey, CartItemId, WishlistKey){
	var ret = yield Factory.wishlistLogic(tr, CultureCode, CartKey, UserKey, CartItemId, WishlistKey);
	var cart= yield Factory.viewByCartKey(tr,CultureCode,ret.cart.CartKey);
	cart.WishlistKey = ret.WishlistKey;
	return cart;
});

Factory.moveToWishList = Q.async(function * (tr, CultureCode, CartKey, UserKey, CartItemId, WishlistKey){
	var ret = yield Factory.wishlistLogic(tr, CultureCode, CartKey, UserKey, CartItemId, WishlistKey);
	var item = yield tr.queryExecute('update CartItem set StatusId=1 where CartId=:CartId and CartItemId=:CartItemId and StatusId=:StatusId',{CartId:ret.cart.CartId,StatusId:2,CartItemId:CartItemId});
	var cart= yield Factory.viewByCartKey(tr,CultureCode,ret.cart.CartKey);
	cart.WishlistKey = ret.WishlistKey;
	return cart;
});

Factory.mergeCarts = Q.async(function* (tr, CultureCode, CartKey, UserKey, MergeCartKey){
	var cart = yield Factory.getCartLogic(tr, CultureCode, CartKey, UserKey, true);
	var mergeCart=yield tr.queryOne('select * from Cart where CartKey=:MergeCartKey and StatusId=:StatusId',{MergeCartKey:MergeCartKey,StatusId:2,CartTypeId:1});
	if (!mergeCart)
		throw { AppCode: 'MSG_ERR_CART_MERGE_NOT_FOUND', Message : "Cart to merge not found." };

	//var ItemList = yield tr.queryMany("select ci.SkuId,ci.CartItemId,ci.Qty from CartItem ci INNER JOIN CartItem ci2 on (ci.SkuId = ci2.SkuId) WHERE ci2.CartId=:CartId and ci.CartId=:MergeId and ci.StatusId=:StatusId",{MergeId:mergeCart.CartId,CartId:cart.CartId,StatusId:2});
	var ItemList = yield tr.queryMany("SELECT * from CartItem where CartId=:MergeId and StatusId=:StatusId and SkuId IN (SELECT SkuId from CartItem where CartId=:CartId and StatusId=:StatusId);", {MergeId:mergeCart.CartId,CartId:cart.CartId,StatusId:2});
	//merge qtys
	for(var Item of ItemList){
		var ret=yield tr.queryExecute('update CartItem set Qty = Qty + :Qty where SkuId=:SkuId and CartId=:CartId',{Qty:Item.Qty,CartId:cart.CartId,SkuId:Item.SkuId});
		var rem=yield tr.queryExecute('update CartItem set StatusId=:StatusId where CartItemId=:CartItemId',{CartItemId:Item.CartItemId,StatusId:1});
	};
	//move remaining
	var ret=yield tr.queryExecute('update CartItem set CartId=:CartId where CartId=:MergeId and StatusId=:StatusId',{CartId:cart.CartId,MergeId:mergeCart.CartId,StatusId:2});
	var ret=yield tr.queryExecute('update Cart set StatusId=:StatusId where CartId=:MergeId',{MergeId :mergeCart.CartId,StatusId:1});

	cart=Factory.viewByCartKey(tr,CultureCode,cart.CartKey);
	return cart;
});

module.exports = Factory;
