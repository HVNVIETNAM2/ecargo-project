"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');

var Factory={};

Factory.compactFilename = function(str){
	str = str.replace(/ /g, "");
    str = str.replace(/([?<>:*|"\[\]\/\\])/g, "");
    str = str.replace(/<|>/g, "");
    return str;
};

Factory.isValidDate = function(date){
	if( ! (new Date(date) !== "Invalid Date" && !isNaN(new Date(date)) ) ){
		date = date.toString().replace(/-/g,"/");
		if(! (new Date(date) !== "Invalid Date" && !isNaN(new Date(date)) ) ){
			return false;
		}else{
			return true;
		}
	}else{
		return true;
	}
};

/**
 * @returns {*|promise}
 */
Factory.existsInventoryLocation = function(locationId, merchantId, tr){
    var deferred = Q.defer();

    Q.when()
        .then(function() {
            return tr.queryOne("SELECT `InventoryLocationId` " +
                "FROM `InventoryLocation` " +
                "WHERE `InventoryLocationId` = :InventoryLocationId AND MerchantId = :MerchantId",
                {InventoryLocationId: locationId, MerchantId: merchantId});
        })
        .then(function(data) {
            if (data == null)
            {
                throw new Error("no inventory location found for this merchant id "+merchantId);
            } else {
                deferred.resolve(true);
            }
        })
        .catch(function(err){
            deferred.reject(err);
        });

    return deferred.promise;
};

Factory.isBlank = function(string){
	if (string===null || string===undefined || string===''){
		return true;
	}else{
		if(string.toString().indexOf(':MISSING:') > -1 || string.toString().indexOf(':NOTVALID:') > -1 || string.toString().indexOf(':NOTFOUND:') > -1){
			return true;
		}else{
			return false;
		}
	}
};

Factory.isTrue = function(string){
	var truthy = ["1","y"];
	string = _.trim(string).toLowerCase().replace(/ /g, "");
	if(string && _.any(truthy, function (v) {
            return v === string;
        })){
		return true;
	}else{
		return false;
	}
};


Factory.list = function(Bale, tr){
	var deferred = Q.defer();
	var InventoryList = null;

	Q.when()
	.then(function() {
		var sql="select inv.*, s.QtySafetyThreshold as QtySafetyThresholdSku, il.QtySafetyThreshold as QtySafetyThresholdLocation, (inv.QtyAllocated - inv.QtyExported - inv.QtyOrdered  - s.QtySafetyThreshold - il.QtySafetyThreshold) as QtyAts, ilc.LocationName, il.LocationExternalCode, il.InventoryLocationTypeId, s.SkuId, s.StyleCode, s.SkuCode, s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, s.GeoCountryId, s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, s.QtySafetyThreshold, s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, scat.CategoryId as PrimaryCategoryId, sz.SizeNameInvariant as SizeName,s.ColorKey, clc.ColorName, scu.SkuColor, scu.SkuName";
                // Add product default image
                sql += ", (SELECT f.ProductImage " +
                        "FROM StyleImage f " +
                        "INNER JOIN ImageType it ON it.ImageTypeCode = f.ImageTypeCode " +
						"INNER JOIN InventoryLocationCulture ilc ON il.InventoryLocationId = ilc.InventoryLocationId AND ilc.CultureCode = :CultureCode " +
                        "WHERE f.MerchantId = s.MerchantId " +
                        "AND f.StyleCode = s.StyleCode " +
                        "AND f.Position = 1 " +
                        "order by it.Priority, f.ColorKey LIMIT 1 " +
                        ") AS ImageDefault";
		sql+=" from Sku s inner join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) inner join Brand b on (s.BrandId=b.BrandId) inner join BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) inner join Size sz on (s.SizeId=sz.SizeId) inner join Color cl on (s.ColorId=cl.ColorId) inner join ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) inner join SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0) inner join Status st on (s.StatusId=st.StatusId) inner join StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) left join Inventory inv on (s.SkuId=inv.SkuId) left join InventoryLocation il on (inv.InventoryLocationId=il.InventoryLocationId and il.StatusId<>1) where s.StatusId <> 1";
		if (Bale.SkuId)
			sql+=" and s.SkuId=:SkuId";
		if (Bale.InventoryLocationId)
			sql+=" and inv.InventoryLocationId=:InventoryLocationId";
		if (Bale.InventoryId)
			sql+=" and inv.InventoryId=:InventoryId";

		return tr.queryMany(sql,Bale).then(function(data) {
			InventoryList=data; //store teh returned user
		});
	})
	.then(function() {
		deferred.resolve(InventoryList);
	})
	.catch(function(err){
		console.log(err);
		deferred.reject(err);
	});

	return deferred.promise;
};

Factory.getSkuInventorySummary = function(skuId, CultureCode, tr){
    var deferred = Q.defer();
    var summary = {
        LocationCount: 0,
        QtyAts: null,
        InventoryStatusId: null
    };

    var systemSafetyThreshold = config.inventory.systemSafetyThreshold;
    var productLowStockThreshold = config.inventory.productLowStockThreshold;

    var params = {
        SkuId: skuId,
        SystemSafetyThreshold: systemSafetyThreshold,
        ProductLowStockThreshold: productLowStockThreshold
    };

    Q.when()
    .then(function () {
        // Get all Inventory status
        return Factory.setParamStatus(params, tr);
    })
    .then(function () {
        summary.InventoryStatusId = params.NAId;
    })
    .then(function () {
        var sql = "SELECT result.SkuId, result.LocationCount, result.QtyAts, "+
            "IF(result.HasInventory = 0, :NAId, "+
            "IF(result.QtyAts = 'Unlimited', :InStockId, " +
            "IF(result.QtyAts < result.SystemSafetyThreshold, :OutOfStockId, " +
            "IF(result.QtyAts <= result.ProductLowStockThreshold, :LowStockId, :InStockId)))) AS InventoryStatusId " +
            "FROM (SELECT inv.SkuId, IF(SUM(inv.InventoryId),TRUE,FALSE) AS HasInventory, "+
            "SUM(:SystemSafetyThreshold) AS SystemSafetyThreshold, "+
            "SUM(:ProductLowStockThreshold) AS ProductLowStockThreshold, "+
            "COUNT(inv.SkuId) AS LocationCount, " +
            "IF(SUM(inv.IsPerpetual), 'Unlimited', (SUM(inv.QtyAllocated) - SUM(inv.QtyExported) - SUM(inv.QtyOrdered)  - SUM(s.QtySafetyThreshold) - SUM(il.QtySafetyThreshold))) as QtyAts " +
            "FROM Sku s " +
            "INNER JOIN Inventory inv ON s.SkuId = inv.SkuId " +
            "INNER JOIN InventoryLocation il ON il.InventoryLocationId = inv.InventoryLocationId " +
            // If table index cannot resolve perforamce case when query start at large inventory set in execution plan,
            // we may use left join to force the smaller Sku table being executed at the first step.
//            "LEFT JOIN Inventory inv ON s.SkuId = inv.SkuId " +
//            "LEFT JOIN InventoryLocation il ON il.InventoryLocationId = inv.InventoryLocationId " +
            "WHERE s.StatusId <> 1 AND il.StatusId = 2 AND s.SkuId = :SkuId " +
            "GROUP BY inv.SkuId ) AS result";
                console.log(sql);
        return tr.queryOne(sql,params).then(function(data) {
            if (data !== null
                    && typeof data.LocationCount !== 'undefined'
                    && typeof data.QtyAts !== 'undefined'
                    && typeof data.InventoryStatusId !== 'undefined'
                    ) {
                summary.LocationCount = data.LocationCount;
                summary.QtyAts = data.QtyAts;
                summary.InventoryStatusId = data.InventoryStatusId;
            }
        });
    })
    .then(function() {
            deferred.resolve(summary);
    })
    .catch(function(err){
            console.log(err);
            deferred.reject(err);
    })
    .done();

    return deferred.promise;
}

Factory.getSkuInventoryInfo = function (Bale, tr) {
    var deferred = Q.defer();
    var inventoryInfo = null;

    var systemSafetyThreshold = config.inventory.systemSafetyThreshold;
    var productLowStockThreshold = config.inventory.productLowStockThreshold;

    var params = {
        SkuId: Bale.SkuId,
        InventoryLocationId: Bale.InventoryLocationId,
        SystemSafetyThreshold: systemSafetyThreshold,
        ProductLowStockThreshold: productLowStockThreshold
    };
    
    Q.when()
    .then(function() {
        // Get all Inventory status
        return Factory.setParamStatus(params, tr);
    })
    .then(function () {
        var sql = "SELECT result.*, " +
                "IF(result.InventoryId IS NULL, :NAId, "+
                "IF(result.IsPerpetual, :InStockId, " +
                "IF(result.QtyAts < :SystemSafetyThreshold, :OutOfStockId, " +
                "IF(result.QtyAts <= :ProductLowStockThreshold, :LowStockId, :InStockId)))) AS InventoryStatusId " +
                "FROM (SELECT inv.*,s.QtySafetyThreshold as QtySafetyThresholdSku, " +
                "il.QtySafetyThreshold as QtySafetyThresholdLocation, " +
                "(inv.QtyAllocated - inv.QtyExported - inv.QtyOrdered  - s.QtySafetyThreshold - il.QtySafetyThreshold) as QtyAts " +
                "FROM Inventory inv " +
                "INNER JOIN InventoryLocation il ON (inv.InventoryLocationId = il.InventoryLocationID) " +
                "INNER JOIN Sku s ON (inv.SkuId=s.SkuId) " +
                "WHERE inv.InventoryLocationId=:InventoryLocationId AND inv.SkuId=:SkuId) AS result";
        return tr.queryOne(sql, params).then(function (data) {
            inventoryInfo = data;
        });
    })
    .then(function () {
        deferred.resolve(inventoryInfo);
    })
    .catch(function (err) {
        console.log(err);
        deferred.reject(err);
    })
    .done();

    return deferred.promise;
}

/**
 * Private method for setup inventory status id from database.
 *
 * @param {Array} params
 * @param {Connection} tr
 * @returns {promise}
 */
Factory.setParamStatus = function(params, tr) {
    // use EN culture code to get the map
    var cultureCode = CONSTANTS.CULTURE_CODE.EN;
    return Factory.getStatusList(cultureCode, tr).then(function (statusList) {
        for (var index in statusList) {
            var status = statusList[index];
            params[status.InventoryStatusNameInvariant.replace('/', '') + 'Id'] = status.InventoryStatusId;
        }
    });
};

/**
 * Parse the values (QtyAts, QtyAllocated, QtyExported, QtyOrdered) as number for ordering
 * 
 * @param {array} data
 * @return {array} data
 */
Factory.parseInventoryData = function (data) {
    _.forEach(data, function (item) {
        item.QtyAts = parseInt(item.QtyAts);
        item.QtyAllocated = parseInt(item.QtyAllocated);
        item.QtyExported = parseInt(item.QtyExported);
        item.QtyOrdered = parseInt(item.QtyOrdered);
    });
    return data;
}

/**
 * List inventories of a sku
 */
Factory.listSku = function(Bale, tr){
	var deferred = Q.defer();
	var InventoryList = null;

        var systemSafetyThreshold = config.inventory.systemSafetyThreshold;
        var productLowStockThreshold = config.inventory.productLowStockThreshold;
        var naNo = config.inventory.naNo;

        var params = {
            MerchantId: Bale.MerchantId,
            SkuId: Bale.SkuId,
            CultureCode: Bale.CultureCode,
            SystemSafetyThreshold: systemSafetyThreshold,
            ProductLowStockThreshold: productLowStockThreshold,
            NANo:naNo
        };

	Q.when()
        .then(function() {
            // Get all Inventory status
            return Factory.setParamStatus(params, tr);
        })
	.then(function() {

            var sql =  "SELECT result.*, "+
                        // Add stock status handling
                        "IF(result.SkuId IS NULL, :NAId, "+
                        "IF(result.IsPerpetual, :InStockId, "+
                        "IF(result.QtyAts < :SystemSafetyThreshold, :OutOfStockId, "+
                        "IF(result.QtyAts <= :ProductLowStockThreshold, :LowStockId, :InStockId)))) AS InventoryStatusId " +
                        "FROM (SELECT inv.InventoryId, il.InventoryLocationId, inv.IsPerpetual, il.StatusId AS InventoryLocationStatusId, "+
                        "IFNULL(inv.QtyAllocated,:NANo) AS QtyAllocated, " +
                        "skuv.QtySafetyThreshold as QtySafetyThresholdSku, " +
                        "il.QtySafetyThreshold as QtySafetyThresholdLocation, " +
                        "IFNULL((inv.QtyAllocated - inv.QtyExported - inv.QtyOrdered  - skuv.QtySafetyThreshold - il.QtySafetyThreshold),:NANo) as QtyAts, "+
                        "IFNULL(inv.QtyExported,:NANo) AS QtyExported, IFNULL(inv.QtyOrdered,:NANo) AS QtyOrdered, inv.LastModified, il.LocationExternalCode, ilc.LocationName, " +
                        "il.InventoryLocationTypeId, skuv.* "+
                        "FROM InventoryLocation il " +
                        "INNER JOIN InventoryLocationCulture ilc ON il.InventoryLocationId = ilc.InventoryLocationId AND ilc.CultureCode = :CultureCode " +
                        "LEFT JOIN Inventory inv ON il.InventoryLocationId = inv.InventoryLocationId AND inv.Skuid = :SkuId " +
                        "LEFT JOIN " +
                        "(SELECT s.SkuId, s.StyleCode, s.SkuCode, " +
                        "s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, " +
                        "s.GeoCountryId, s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, " +
                        "s.SaleTo, s.AvailableFrom, s.AvailableTo, s.QtySafetyThreshold, " +
                        "s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId AS SkuStatusId, " +
                        "scat.CategoryId as PrimaryCategoryId, sz.SizeNameInvariant as SizeName, " +
                        "s.ColorKey, clc.ColorName, scu.SkuColor, scu.SkuName, " +
                        "(SELECT f.ProductImage " +
                        "FROM StyleImage f " +
                        "INNER JOIN ImageType it ON it.ImageTypeCode = f.ImageTypeCode " +
                        "WHERE f.MerchantId = s.MerchantId " +
                        "AND f.StyleCode = s.StyleCode " +
                        "AND f.Position = 1 " +
                        "ORDER BY it.Priority, f.ColorKey LIMIT 1 ) AS ImageDefault " +
                        "FROM  Sku s " +
                        "INNER JOIN SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) " +
                        "INNER JOIN Brand b on (s.BrandId=b.BrandId) " +
                        "INNER JOIN BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) " +
                        "INNER JOIN Size sz on (s.SizeId=sz.SizeId) " +
                        "INNER JOIN Color cl on (s.ColorId=cl.ColorId) " +
                        "INNER JOIN ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) " +
                        "INNER JOIN SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0) " +
                        "INNER JOIN Status st on (s.StatusId=st.StatusId) " +
                        "INNER JOIN StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) " +
                        ") skuv ON skuv.SkuId = inv.Skuid " +
                        "WHERE il.StatusId = 2 AND il.MerchantId = :MerchantId ) AS result " +
                        "ORDER BY result.LastModified DESC, result.InventoryLocationId ";
		return tr.queryMany(sql,params).then(function(data) {
			InventoryList=Factory.parseInventoryData(data); //store teh returned user
		});
	})
	.then(function() {
		deferred.resolve(InventoryList);
	})
	.catch(function(err){
		deferred.reject(err);
	})
	.done();

	return deferred.promise;
};


// TODO: Define or modifed the CultureCodeOverload Issue.
/**
 * Get Inventory Location
 */
Factory.getInventoryLocation = function(Bale, tr){
	var deferred = Q.defer();
	var Inventory = null;

	Q.when()
	.then(function() {
                var sql =  "SELECT il.*,ilc.LocationName,iltc.InventoryLocationTypeName, s.StatusNameInvariant, "+
                        "gcoC.GeoCountryName, gprovC.GeoName as GeoProvinceName, ifnull(gcityC.GeoName,gcity.GeoNameInvariant) AS GeoCityName " +
                        "FROM InventoryLocation il " +
						"INNER JOIN InventoryLocationCulture ilc ON il.InventoryLocationId = ilc.InventoryLocationId AND ilc.CultureCode = :CultureCode " +
                        "INNER JOIN InventoryLocationType ilt on (il.InventoryLocationTypeId=ilt.InventoryLocationTypeId) " +
                        "INNER JOIN InventoryLocationTypeCulture iltc on (ilt.InventoryLocationTypeId=iltc.InventoryLocationTypeId AND iltc.CultureCode=:CultureCode) " +
                        "INNER JOIN Status s on (il.StatusId=s.StatusId) " +
                        "INNER JOIN GeoCountry gco on (il.GeoCountryId=gco.GeoCountryId) " +
                        "INNER JOIN GeoCountryCulture gcoC on (gco.GeoCountryId=gcoC.GeoCountryId and gcoC.CultureCode=:CultureCode) " +
                        "INNER JOIN Geo gprov on ( il.GeoIdProvince=gprov.GeoId) inner join Geo gcity on (il.GeoIdCity=gcity.GeoId) " +
                        "LEFT JOIN GeoCulture gprovC on (gprov.GeoId=gprovC.GeoId " +
                        "AND gprovC.CultureCode=:CultureCode) " +
                        "LEFT JOIN GeoCulture gcityC on (gcity.GeoId=gcityC.GeoId " +
                        "AND gcityC.CultureCode=:CultureCode) "+
                        "WHERE il.StatusId <> 1 " +
                        "AND il.InventoryLocationId = :InventoryLocationId";

		return tr.queryOne(sql,Bale).then(function(data) {
			Inventory=data; //store teh returned user
		});
	})
	.then(function() {
		deferred.resolve(Inventory);
	})
	.catch(function(err){
		console.log(err);
		deferred.reject(err);
	})
	.done();

	return deferred.promise;
};

/**
 * List products in a inventory location
 */
Factory.listLocation = function(Bale, tr){
	var deferred = Q.defer();

        var systemSafetyThreshold = config.inventory.systemSafetyThreshold;
        var productLowStockThreshold = config.inventory.productLowStockThreshold;
        var naNo = config.inventory.naNo;
        var limit = parseInt(Bale.Size);
        var offset = ((Bale.Page) - 1) * limit;

        var params = {
            InventoryLocationId: Bale.InventoryLocationId,
            CultureCode: Bale.CultureCode,
            SystemSafetyThreshold: systemSafetyThreshold,
            ProductLowStockThreshold: productLowStockThreshold,
            NANo:naNo,
            Limit:limit,
            Offset:offset
        };
       
        var result = {
            HitsTotal: 0,
            PageTotal: 0,
            PageSize: 0,
            PageCurrent: 0,
            PageData: []
        };
        
        var filterInventorySQL = "";
        if(!cu.isBlank(Bale.HasInventory)){
            var hasInventory = parseInt(Bale.HasInventory);
            switch (hasInventory){
                case CONSTANTS.INVENTORY_FILTER.WITH_INVENTORY:
                    filterInventorySQL = "AND (inv.InventoryLocationId=:InventoryLocationId) ";
                    break;
                case CONSTANTS.INVENTORY_FILTER.WITHOUT_INVENTORY:
                    filterInventorySQL = "AND (inv.InventoryId is null) ";
                    break;
                default:
                case CONSTANTS.INVENTORY_FILTER.ALL:
                    filterInventorySQL = "AND (inv.InventoryLocationId=:InventoryLocationId or inv.InventoryId is null) ";
                    break;
            }
        }
        
        var searchSQL= "";
        if(!cu.isBlank(Bale.Search)){
            var search = Bale.Search;
            searchSQL = "AND (SkuName LIKE :Search OR Bar LIKE :Search OR SkuCode LIKE :Search OR SkuColor LIKE :Search OR SizeNameInvariant LIKE :Search)";
            params['Search'] = search+'%';
        }
        
        var filterInventoryStatusIdSQL = "";                
        if(!cu.isBlank(Bale.InventoryStatusId)){
            var inventoryStatusId = parseInt(Bale.InventoryStatusId);
            switch (inventoryStatusId) {
                case CONSTANTS.INVENTORY_STATUS.IN_STOCK:
                case CONSTANTS.INVENTORY_STATUS.LOW_STOCK:
                case CONSTANTS.INVENTORY_STATUS.OUT_OF_STOCK:
                case CONSTANTS.INVENTORY_STATUS.NA:
                    filterInventoryStatusIdSQL = "WHERE InventoryStatusId = :InventoryStatusId ";
                    params['InventoryStatusId'] = inventoryStatusId;
                    break;
                case CONSTANTS.INVENTORY_STATUS.ALL:
                default:
                    break;
            }
        }
        
        var commonSQL = " FROM (SELECT result.*, "+
                        "IF(result.InventoryId IS NULL, :NAId, "+
                        "IF(result.IsPerpetual, :InStockId, "+
                        "IF(result.QtyAts < :SystemSafetyThreshold, :OutOfStockId, "+
                        "IF(result.QtyAts <= :ProductLowStockThreshold, :LowStockId, :InStockId)))) AS InventoryStatusId " +
                        "FROM (SELECT inv.InventoryId, il.InventoryLocationId, inv.IsPerpetual, il.StatusId AS InventoryLocationStatusId, "+
                        "IFNULL(inv.QtyAllocated,:NANo) AS QtyAllocated, " +
                        "s.QtySafetyThreshold as QtySafetyThresholdSku, " +
                        "il.QtySafetyThreshold as QtySafetyThresholdLocation, " +
                        "IFNULL((inv.QtyAllocated - inv.QtyExported - inv.QtyOrdered  - s.QtySafetyThreshold - il.QtySafetyThreshold),:NANo) as QtyAts, " +
                        "IFNULL(inv.QtyExported,:NANo) AS QtyExported, IFNULL(inv.QtyOrdered,:NANo) AS QtyOrdered, inv.LastModified, " +
                        "s.SkuId, s.StyleCode, s.SkuCode, " +
                        "s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, " +
                        "s.GeoCountryId, s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, " +
                        "s.SaleTo, s.AvailableFrom, s.AvailableTo, s.QtySafetyThreshold, " +
                        "s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId, " +
                        "scat.CategoryId as PrimaryCategoryId, sz.SizeNameInvariant as SizeName, " +
                        "s.ColorKey, clc.ColorName, scu.SkuColor, scu.SkuName, s.StatusId AS SkuStatusId " +
                        "FROM Sku s " +
                        "INNER JOIN SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) " +
                        "INNER JOIN Size sz on (s.SizeId=sz.SizeId) " +
                        "INNER JOIN Color cl on (s.ColorId=cl.ColorId) " +
                        "INNER JOIN ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) " +
                        "INNER JOIN SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0) " +
                        "LEFT JOIN Inventory inv on (s.SkuId=inv.SkuId and inv.InventoryLocationId=:InventoryLocationId) " +
                        "LEFT JOIN InventoryLocation il on (inv.InventoryLocationId=il.InventoryLocationId and il.StatusId<>1) " +
                        "WHERE s.StatusId <> 1 " +
                        "AND s.MerchantID = :MerchantID " +
                        filterInventorySQL +
                        searchSQL +
                        ") AS result ) AS final " + filterInventoryStatusIdSQL;
                
        var merchantID = null;
                        
	Q.when()
        .then(function() {
            // Get all Inventory status
            return Factory.setParamStatus(params, tr);
        })
	.then(function() {
                var merchantIdSQL = "SELECT MerchantID FROM InventoryLocation WHERE InventoryLocationId=:InventoryLocationId";
                return tr.queryOne(merchantIdSQL, {InventoryLocationId:Bale.InventoryLocationId}).then(function(data) {
                    merchantID = data.MerchantID;
                    params['MerchantID']=merchantID;
                });
        })
	.then(function() {
                var orderSQL = "ORDER BY LastModified DESC ";

                if (!cu.isBlank(Bale.Order) && !cu.isBlank(Bale.Predicate)) {
                    orderSQL = " ORDER BY " + Bale.Predicate + " " + Bale.Order;
                }
                
                var selectSQL = "SELECT * "+ commonSQL + orderSQL + " LIMIT :Offset, :Limit";
		return tr.queryMany(selectSQL,params).then(function(data) {
                        result.PageData= Factory.parseInventoryData(data);
		});
	})
	.then(function() {
            var skuIds = [];
            // Get all sku default image
            var skuList = result.PageData;
            
                var skuIdList = skuList.map(function (n) {
                    skuIds.push(n.SkuId);
                });
                
                if(skuIds.length){
                    var imageSQL = "SELECT s.StyleCode, IFNULL((SELECT f.ProductImage " +
                            "FROM StyleImage f " +
                            "INNER JOIN ImageType it ON it.ImageTypeCode = f.ImageTypeCode " +
                            "WHERE f.MerchantId = s.MerchantId " +
                            "AND f.StyleCode = s.StyleCode " +
                            "AND f.Position = 1 " +
                            "ORDER BY it.Priority, f.ColorKey LIMIT 1), '00000000000000000000000000000000' ) AS ImageDefault " +
                            "FROM Sku s " +
                            "WHERE s.MerchantId = :MerchantId " +
                            "AND s.SkuId IN (:SkuIds) " +
                            "AND s.StatusId <> 1";

                    return tr.queryMany(imageSQL, {MerchantId: merchantID, SkuIds: skuIds}).then(function (data) {
                        var defaultImages = {};

                        for (let image of data){
                            defaultImages[image.StyleCode] = image.ImageDefault;
                        }

                        for(let sku of skuList){
                            sku.ImageDefault = defaultImages[sku.StyleCode];
                        }
                    });
                }
        })
	.then(function() {
            var countSQL = "SELECT COUNT(*) AS total " + commonSQL;
            return tr.queryOne(countSQL,params).then(function(data) {
                        result.HitsTotal= parseInt(data.total);
                        result.PageSize= parseInt(limit);
                        result.PageTotal= Math.ceil(result.HitsTotal/result.PageSize);
                        result.PageCurrent= parseInt(Bale.Page);
		});
        })
	.then(function() {
		deferred.resolve(result);
	})
	.catch(function(err){
		console.log(err);
		deferred.reject(err);
	})
	.done();

	return deferred.promise;
};

Factory.ListJournal = function(Bale, tr){
	var deferred = Q.defer();
	var ListJournal = null;

	Q.when()
	.then(function() {
		//var sql="select inv.*, il.LocationExternalCode, il.LocationName, il.InventoryLocationTypeId, s.SkuId, s.StyleCode, s.SkuCode, s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, s.GeoCountryId, s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, s.QtySafetyThreshold, s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, scat.CategoryId as PrimaryCategoryId, sz.SizeNameInvariant as SizeName,s.ColorKey, clc.ColorName, scu.SkuColor, scu.SkuName";
		//sql+=" from Sku s inner join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) inner join Brand b on (s.BrandId=b.BrandId) inner join BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) inner join Size sz on (s.SizeId=sz.SizeId) inner join Color cl on (s.ColorId=cl.ColorId) inner join ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) inner join SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0) inner join Status st on (s.StatusId=st.StatusId) inner join StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) inner join InventoryJournal inv on (s.SkuId=inv.SkuId) inner join InventoryLocation il on (inv.InventoryLocationId=il.InventoryLocationId and il.StatusId<>1) where s.StatusId <> 1";
		var sql="select invj.*, jec.JournalEventName, concat(u.FirstName,' ',u.LastName) as UserName, ut.UserTypeId, ut.UserTypeNameInvariant as UserTypeName";
		sql+=" from InventoryJournal invj inner join JournalEvent je on (invj.JournalEventId=je.JournalEventId) inner join JournalEventCulture jec on (je.JournalEventId=jec.JournalEventId and jec.CultureCode=:CultureCode) inner join User u on (invj.LastModifiedUserId=u.UserId) inner join UserType ut on (u.UserTypeId=ut.UserTypeId) ";
		sql+=" where invj.SkuId=:SkuId";
		sql+=" and invj.InventoryLocationId=:InventoryLocationId";
		return tr.queryMany(sql,Bale).then(function(data) {
			ListJournal=data; //store teh returned user
		});
	})
	.then(function() {
		deferred.resolve(ListJournal);
	})
	.catch(function(err){
		console.log(err);
		deferred.reject(err);
	});

	return deferred.promise;
}

Factory.inventorySaveSheetRow = function(item, tr){
	var deferred = Q.defer();
	var statusMessage = "";
	var errorCols = [];
    var isActiveSku = false;
	var Inventory = null;

	Q.when()
	.then(function(){
		if (cu.isBlank(item.SkuCode)){
			item.SkuCode = ':MISSING:' + item.SkuCode || "";
			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', field : "SkuCode" })
		}
	})
	.then(function(){
		if(item.SkuCode.toString().indexOf(':MISSING:') <= -1){
			var sqlQuery = "SELECT * FROM Sku WHERE SkuCode = :SkuCode AND StatusId not in (:StatusIds) AND MerchantId=:MerchantId";
			return tr.queryOne(sqlQuery, { SkuCode : item.SkuCode, MerchantId: item.MerchantId, StatusIds: [CONSTANTS.STATUS.DELETED, CONSTANTS.STATUS.PENDING] }).then(function(data){
				if(data && data.SkuId){
                    isActiveSku = item.StatusId === CONSTANTS.STATUS.ACTIVE;
					return item.SkuId = data.SkuId;
				}else{
					item["SkuCode"] = ':NOTFOUND:' + item["SkuCode"];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_FOUND', field : "SkuCode" })
				}
			});
		}
	})
    .then(function(){
        console.log(item.Perpetual, item.Allocation, item.SkuCode, item.Perpetual);
        switch(item.Perpetual){
            case 'Y':
                if (!cu.isBlank(item.Allocation)){
                    item.Allocation = ':NOTVALID:' + item.Allocation;
                    errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', field : "Allocation" })
                } else {
                    item.Allocation = config.inventory.perpetualAllocationNo;
                }
                item.PerpetualCon = 1;
                break;
            case 'N':
                if(cu.isBlank(item.Allocation)){
                    item.Allocation = ':MISSING:' + item.Allocation;
                    errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', field : "Allocation" })
                }
                else{
                    if ((parseFloat(item.Allocation) < 0 || isNaN(parseFloat(item.Allocation)))){
                        item.Allocation = ':NOTVALID:' + item.Allocation;
                        errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', field : "Allocation" })
                    }
                }
                item.PerpetualCon = 0;
                break;
            default:
                if (!cu.isBlank(item.Perpetual)){
                    item.Perpetual = ':NOTVALID:' + item.Perpetual;
                    errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', field : "Perpetual" });
                }
                else{
                    item.Perpetual = ':MISSING:' + item.Perpetual;
                    errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', field : "Perpetual" })
                }
                break;
        }
        
//        if(Factory.isTrue(item.Perpetual)){
//            if (!cu.isBlank(item.Allocation)){
//                item.Allocation = ':NOTVALID:' + item.Allocation;
//                errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', field : "Perpetual" })
//            } else {
//                item.Allocation = config.inventory.perpetualAllocationNo;
//            }
//            item.PerpetualCon = 1;
//        }else{
//            if(cu.isBlank(item.Allocation)){
//                item.Allocation = ':MISSING:' + item.Allocation;
//                errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', field : "Allocation" })
//            }
//            else{
//                if (!cu.isBlank(item.Allocation) &&
//                    (parseFloat(item.Allocation) < 0 || isNaN(parseFloat(item.Allocation)))){
//                    item.Allocation = ':NOTVALID:' + item.Allocation;
//                    errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', field : "Allocation" })
//                }
//            }
//            item.PerpetualCon = 0;
//        }
    })
/*	.then(function(){
		var checkNull = ["Allocated"];
		var promises = checkNull.map(function(elem){
			if(cu.isBlank(item[elem])){
				item[elem] = 0;
			}else{
				if(parseFloat(item[elem]) < 0 || isNaN(parseFloat(item[elem]))){
					item[elem] = ':NOTVALID:' + item[elem];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', field : elem })
				}
			}
		});

		return Q.all(promises);
	})*/
    .then(function(){
        if(errorCols.length > 0){
            throw { errors : errorCols };
        }else{
            return;
        }
    })
    .then(function(){
        return tr.queryOne("SELECT * FROM Inventory WHERE SkuId = :SkuId AND InventoryLocationId = :InventoryLocationId", item).then(function(data){
            Inventory = data;

            //if inactive sku, it cannot create new inventory
            if (Inventory === null && !isActiveSku) {
                item["SkuCode"] = ':NOTFOUND:' + item["SkuCode"];
                errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_FOUND', field : "SkuCode" });
                throw { errors : errorCols };
            }
        });
    })
	.then(function(){
		if(Inventory == null){
			// do an insert..
			item.StatusId = CONSTANTS.STATUS.ACTIVE;

			if(item.Delete == 1){
				item.StatusId = CONSTANTS.STATUS.DELETED;
			}

			if (item.StatusId != CONSTANTS.STATUS.DELETED){
				var sqlQuery = "INSERT INTO Inventory (SkuId,InventoryLocationId,IsPerpetual,QtyAllocated,StatusId) VALUES (:SkuId,:InventoryLocationId,:PerpetualCon,:Allocation,:StatusId)";
				return tr.queryExecute(sqlQuery, item).then(function(data){
					item.InventoryId = data.insertId;
					item.JournalTypeId=CONSTANTS.JOURNAL_TYPE.INSERT;
					item.JournalEventId=CONSTANTS.JOURNAL_EVENT_TYPE.DATA_IMPORT;
					statusMessage = "inserted";
					console.log("** Inserted =" + item.SkuId);
				});
			}else{
				statusMessage = "skipped";
			}
		}
	})
	.then(function(){
		if(Inventory != null){
			// do an update..
			if(item.Overwrite || item.Overwrite == 'true'  || item.Overwrite == '1'  || item.Overwrite == 1){
				item.SkuId = Inventory.SkuId;
				item.StatusId = Inventory.StatusId;
				item.InventoryId = Inventory.InventoryId;
				item.JournalTypeId=CONSTANTS.JOURNAL_TYPE.UPDATE;
				item.JournalEventId=CONSTANTS.JOURNAL_EVENT_TYPE.DATA_IMPORT;
				if(item.Delete == 1){
					item.StatusId = CONSTANTS.STATUS.DELETED;
				}

				console.log("Start updating product: " + item.SkuId);
				var sqlQuery = "UPDATE Inventory SET QtyAllocated=:Allocation, IsPerpetual=:PerpetualCon, StatusId=:StatusId WHERE SkuId=:SkuId";
				return tr.queryExecute(sqlQuery, item).then(function(data){
					statusMessage = "updated";
					console.log("*** Updated =" + item.SkuId);
				});
			}else{
				statusMessage = "skipped";
			}
		}
	})
	.then(function() {
		console.log(item);
		if (item.JournalEventId)
		{
			console.log('in select');
			return tr.queryOne("select inv.*,s.QtySafetyThreshold as QtySafetyThresholdSku, il.QtySafetyThreshold as QtySafetyThresholdLocation, (inv.QtyAllocated - inv.QtyExported - inv.QtyOrdered  - s.QtySafetyThreshold - il.QtySafetyThreshold) as QtyAts from Inventory inv inner join InventoryLocation il on (inv.InventoryLocationId = il.InventoryLocationID) inner join Sku s on (inv.SkuId=s.SkuId) where inv.InventoryId=:InventoryId",item).then(function(data) {
				console.dir(data);
				if (data==null) throw {AppCode: 'MSG_ERR_NOT_FOUND'};
				Inventory=data;
				Inventory.JournalTypeId=item.JournalTypeId;
				Inventory.JournalEventId=item.JournalEventId;
				Inventory.UserId=item.UserId
			});
		}
	})
	.then(function(){
		if (item.JournalEventId)
		{
			console.log('try insert jour');
			return tr.queryExecute("insert into InventoryJournal (LastModified,LastModifiedUserId,JournalTypeId,JournalEventId,InventoryId,SkuId,InventoryLocationId,IsPerpetual,QtyAllocated,QtyExported,QtyOrdered,QtySafetyThresholdSku,QtySafetyThresholdLocation,QtyAts,StatusId) values (UTC_TIMESTAMP(),:UserId,:JournalTypeId,:JournalEventId,:InventoryId,:SkuId,:InventoryLocationId,:IsPerpetual,:QtyAllocated,:QtyExported,:QtyOrdered,:QtySafetyThresholdSku,:QtySafetyThresholdLocation,:QtyAts,:StatusId)",Inventory).then(function(data) {
				console.log('DONE try insert jour');
				Inventory.InventoryJournalId=data.insertId;
			});
		}
	})
	.then(function() {
		deferred.resolve(statusMessage);
	})
	.catch(function(err){
		deferred.reject(err);
	});

	return deferred.promise;
};

Factory.InventoryStatusChange = function(Data, StatusIdRequested, tr){
	var deferred = Q.defer();
	Q.when()
	.then(function(){
		return tr.queryOne("select d.* from Inventory d where d.InventoryId=:InventoryId", Data).then(function(data) {
			if (data)
			{
				Data=data;
				if (Data.StatusId==CONSTANTS.STATUS.DELETED) //don't perfom if deleted
					throw {AppCode:"MSG_ERR_STATUS_INVALID"};
			}
			else
			{
				throw {AppCode:"MSG_ERR_INVENTORY_NOT_FOUND"};
			}
		})
	})
	.then(function() {
		Data.StatusId=StatusIdRequested;
		return tr.queryExecute("update Inventory set StatusId=:StatusId where InventoryId=:InventoryId;",Data).then(function(data) {
			return;
		})
	})
	.then(function() {
		deferred.resolve(Data);
	})
	.catch(function(err){
		deferred.reject(err);
	});
	return deferred.promise;
};

Factory.InventoryLocationStatusChange = function(Data, StatusIdRequested, tr){
	var deferred = Q.defer();
	Q.when()
    .then(function(){
        if (_.parseInt(StatusIdRequested) === CONSTANTS.STATUS.DELETED) {
            //dont delete if it has inventory
            return tr.queryOne("select InventoryLocationId from Inventory where InventoryLocationId=:InventoryLocationId and StatusId=:StatusId",
                {InventoryLocationId: Data.InventoryLocationId, StatusId: CONSTANTS.STATUS.ACTIVE}).then(function(data) {
                if (data !== null) {
                    throw {AppCode:"MSG_ERR_DEL_LOCATION_FOUND_INVENTORY"};
                }
            });
        }
    })
	.then(function(){
		return tr.queryOne("select d.* from InventoryLocation d where d.InventoryLocationId=:InventoryLocationId", Data).then(function(data) {
			if (data)
			{
				Data=data;
				if (Data.StatusId==CONSTANTS.STATUS.DELETED) //don't perfom if deleted
					throw {AppCode:"MSG_ERR_STATUS_INVALID"};
			}
			else
			{
				throw {AppCode:"MSG_ERR_INVENTORY_LOCATION_NOT_FOUND"};
			}
		})
	})
	.then(function() {
		Data.StatusId=StatusIdRequested;
		return tr.queryExecute("update InventoryLocation set StatusId=:StatusId,LastStatus=UTC_TIMESTAMP() where InventoryLocationId=:InventoryLocationId;",Data).then(function(data) {
			return;
		})
	})
	.then(function() {
		deferred.resolve(Data);
	})
	.catch(function(err){
		deferred.reject(err);
	});
	return deferred.promise;
};

Factory.getStatusList = function(cultureCode, tr){
    var deferred = Q.defer();
	var statusList = null;

	Q.when()
	.then(function() {
                var sql =  "SELECT invs.*, invc.InventoryStatusCultureId, invc.CultureCode, invc.InventoryStatusName " +
                        "FROM InventoryStatus invs " +
                        "INNER JOIN InventoryStatusCulture invc ON invs.InventoryStatusId = invc.InventoryStatusId " +
                        "WHERE invc.CultureCode = :CultureCode";

		return tr.queryMany(sql, {CultureCode:cultureCode}).then(function(data) {
			statusList=data;
		});
	})
	.then(function() {
		deferred.resolve(statusList);
	})
	.catch(function(err){
		console.log(err);
		deferred.reject(err);
	})
	.done();

	return deferred.promise;
}

//check if locationCode is unique within one merchant.
Factory.checkLocationCodeUniqueness = function(inventoryLocationId, merchantId, locationExternalCode, tr){
    var sql="SELECT COUNT(InventoryLocationId) totalRecords FROM InventoryLocation WHERE StatusId <> 1 AND MerchantId=:MerchantId AND LocationExternalCode=:LocationExternalCode";
    sql = (cu.isBlank(inventoryLocationId))?sql:(sql+" AND InventoryLocationId!=:InventoryLocationId");
    return tr.queryOne(sql, {
        MerchantId: merchantId,
        LocationExternalCode: locationExternalCode,
        InventoryLocationId: inventoryLocationId
    }).then(function (data) {
        if (data.totalRecords > 0) {
            throw {
                AppCode: "MSG_ERR_LOCATION_EXTERNAL_CODE_UNIQUE_WITHIN_MERCHANT"
            };
        }
    });
}
//check if locationName is unique within one merchant.
Factory.checkLocationNameUniqueness = function(inventoryLocationId, merchantId, locationNameEN, locationNameCHS, locationNameCHT, tr){
    var sql="SELECT COUNT(i.InventoryLocationId) totalRecords from InventoryLocation i inner join InventoryLocationCulture ic on(i.InventoryLocationId = ic.InventoryLocationId) where i.StatusId <> 1 AND i.MerchantId=:MerchantId and (ic.LocationName=:LocationNameEN OR ic.LocationName=:LocationNameCHS OR ic.LocationName=:LocationNameCHT)";
    sql = (cu.isBlank(inventoryLocationId))?sql:(sql+" AND i.InventoryLocationId!=:InventoryLocationId");
    return tr.queryOne(sql, {
        MerchantId: merchantId,
        LocationNameEN: locationNameEN,
        LocationNameCHS: locationNameCHS,
        LocationNameCHT: locationNameCHT,
        InventoryLocationId: inventoryLocationId
    }).then(function (data) {
        if (data.totalRecords > 0) {
            throw {
                AppCode: "MSG_ERR_LOCATION_NAME_UNIQUE_WITHIN_MERCHANT"
            };
        }
    });
}

module.exports=Factory;