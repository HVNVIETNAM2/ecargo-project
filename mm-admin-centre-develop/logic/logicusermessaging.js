"use strict";
var Q = require('q');
var config = require('../config');
var _ = require('lodash');
var cm = require('../lib/commonmariasql.js');
var ce = require('../lib/commonemail.js');
var lu = require('../logic/logicuser.js');
var request = require('superagent');

exports.messageTemplateCodes = {
  CREATE_USER: 'CREATE_USER', //template code of create user message
  SIGNIN_ATTEMPTS_EXCEEDED: 'SIGNIN_ATTEMPTS_EXCEEDED', //template code of login failed many times and lock the account message
  ACTIVATE_USER: 'ACTIVATE_USER', //template code of activate user message
  INACTIVATE_USER: 'INACTIVATE_USER', //template code of inactivate user message
  CHANGE_EMAIL: 'CHANGE_EMAIL', //template code of change user email message
  RESET_PASSWORD: 'RESET_PASSWORD', //template code of reset password message
  FORGOT_PASSWORD: 'FORGOT_PASSWORD', //template code of forget password message
  CHANGE_MOBILE: 'CHANGE_MOBILE', //template code of change mobile
  BRAND_STATUS_CHANGED: 'BRAND_STATUS_CHANGED' //template code of change brand status
};

/**
 * sending messaging function including email and sms.
 * because every placeholder in template compeletly depends on User object, it is advised that
 * all the template's placeholder must be defined to be related with the User object's property.
 * @param {Object} user. User Object.
 * @param {String} templateCode, which is consistent with MessagingTemplate table's code.
 * @param {Object} tr database transaction object
 * @return {Object} promise object.
 */
exports.sendUserMessage = function(user, templateCode, tr) {
  var templateObj = {};
  var placeHolder = _.clone(user); // placeHolder object, the reason why clone from user object is because placeHolder need to be extended for new properties and avoid pollute the outer object.
  placeHolder.RootURL = lu.getLinkPath(user); //extend RootUrl properties.
  var deferred = Q.defer();

  Q.when()
    .then(function() { //get template data from table MessagingTemplate;
      var attr = {
        MessagingTemplateCode: templateCode,
        CultureCode: user.CultureCode
      };
      return tr.queryOne("select * from MessagingTemplate where MessagingTemplateCode = :MessagingTemplateCode and CultureCode = :CultureCode", attr).then(function(data) {
        templateObj = data;
      })
    })
    .then(function() {
      //sending messaging logic
      var isSendingSMS = lu.isMobileUser(user);
      if (isSendingSMS) {
        //send sms
        console.log('sending sms...');
        //format the message template
        var contentText = messageFormat(templateObj.SmsTemplateContent, placeHolder); //replace the place holder in template.

        var receiver = config.sms.overrideToNumber ? config.sms.overrideToNumber : (user.MobileCode + user.MobileNumber);

        //send sms msg
        return sendSms(contentText, receiver);
      } else {
        //send email;
        console.log('sending email...');
        var subjectText = templateObj.EmailTemplateSubject;
        var contentText = templateObj.EmailTemplateContent;
        //replace the place holder in template.
        subjectText = messageFormat(subjectText, placeHolder);
        contentText = messageFormat(contentText, placeHolder);
        contentText = appendCultureCodeToLink(contentText, user.CultureCode);

        var email = {
          to: user.Email, // list of receivers
          subject: subjectText, // Subject line
          html: contentText,
          text: contentText
        };
        return ce.sendEmail(email).then(function() {
          console.log("sending email success!");
        });
      }
    })
      .then(function () {
        deferred.resolve();
      })
      .catch(function (err) {
        console.log('sending message failed!');
        deferred.reject(err);
      });

  return deferred.promise;
};

function sendSms(contentText, receiver) {
  return sendSmsNexmo(contentText, receiver);
}

/**
 * sending sms handler specific for nexmo provider.
 * @param {String} contentText. sms content.
 * @param {String} receiver phone number.
 * @return {Object} promise object.
 */
function sendSmsNexmo(contentText, receiver) {
  var nexmoParams = {
    api_key: config.sms.key,
    api_secret: config.sms.secret,
    type: 'unicode', //set type unicode to avoid the chinese messy code.
    to: receiver,
    from: 'NEXMO',
    text: contentText
  };
  var smsRestFulURL = config.sms.baseURL;
  //call restful service to send msg
  var deferred = Q.defer();
  request.post(smsRestFulURL)
    .set('Content-Type', 'application/json')
    .send(nexmoParams)
    .end(function(err, res) {
      if (!err) {
        var resTextObj = JSON.parse(res.text);
        var messageObj = resTextObj.messages[0];
        var msgStatus = messageObj.status; //0-Success, others-Failed. ps:for the status code reference: https://docs.nexmo.com/api-ref/sms-api/response/status-codes
        if (msgStatus == 0) {
          console.log("sending sms success!");
          deferred.resolve();
        } else {
          console.log("sending sms fail!");
          console.log("status: " + msgStatus); ////0-Success, others-Failed. ps:for the status code reference: https://docs.nexmo.com/api-ref/sms-api/response/status-codes
          deferred.reject(new Error(JSON.stringify(resTextObj)));
        }
      } else {
        console.log("call sms restful ws failed!");
        deferred.reject(new Error(err));
      }
    });
  return deferred.promise;
}

/**
 * format the message with placeholder;
 * @param str String. eg: i have a {{color}} car
 * @param obj Object. eg: {color: 'red'}
 * @return format String. eg: i have a red car
 **/
function messageFormat(str, obj) {
  var regExpText = "";
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      regExpText += "{{" + prop + "}}|";
    }
  }
  regExpText = regExpText.substring(0, regExpText.length - 1);
  var re = new RegExp(regExpText, "gi");
  var formatedMsg = str.replace(re, function(matched) {
    var prop = matched.replace(/{{|}}/gi, "");
    return obj[prop];
  });
  return formatedMsg;
}

exports.sendVerificationSms = function (data, cc, tr) {
  return tr.queryOne(
    "select * from Translation where TranslationCode = :TranslationCode and CultureCode = :CultureCode",
    {CultureCode: cc || 'EN', TranslationCode: 'SMS_CA_MOBILE_VER_SIGNUP'}
  )
  .then(function (Tran) {
    if (!Tran) throw {AppCode: 'MSG_ERR_TRANSLATION_MISSING'};
    var contentText = messageFormat(Tran.TranslationName, data); 
    //replace the place holder in template.
    var receiver = config.sms.overrideToNumber ? config.sms.overrideToNumber : (data.MobileCode + data.MobileNumber);
    console.log(contentText);

    return sendSms(contentText, receiver);
  });
};

function appendCultureCodeToLink (content, cc) {
  if (!content) {
    return '';
  }

  if (!cc) {
    cc = 'EN';
  }

  return content.replace(/href="([^"]+)"/g, function (link) {
    return link.slice(0, -1) + (link.indexOf('?') === -1 ? '?' : '&') + 'cc='+cc+'"';
  });
}
