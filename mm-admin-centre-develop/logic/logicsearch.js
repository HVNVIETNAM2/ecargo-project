"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');
var li = require('./logicinventory.js');

var Factory = {};

Factory.buildQuery = function(searchField,paramIn)
{
	let qf='';
	if (paramIn)
	{
		let list=paramIn.split(',');
		for (var i in list)
			qf += ' OR ' + searchField + ':"' + list[i] + '"~0';
		qf=qf.replace(/^( OR )/,"");
		if (qf)
			qf=" AND (" + qf + ") ";
		
	}
	return qf;
}

Factory.standardSearch = Q.async(function* (client,IndexType,CultureCode, pagesize,pageno,sort,order,s) {
	pagesize=parseInt(pagesize);
	pageno=parseInt(pageno);
	var from= ((pageno-1) * pagesize);
	var qf=null;
	var list=null;
	var pricefrom=null;
	var priceto=null;
	var queryString=null;
	if (!s)
		s='';
	
	queryString=s + '*';
		
	if (cu.isBlank(CultureCode))
		throw new Error("cc required to define CultureCode");
		
	if (!pagesize)
		pagesize=1000;
	
	if (!pageno)
		pageno=1;
		
	var query={
		"from" : from, "size" : pagesize,
		"query": {
			"query_string": {
				"query": queryString,
				"use_dis_max" : true
			}
		}				
	}
	
	if (sort)
	{
		if (!order)
		{
			order="asc";
		}
		var sortObj={};
		sortObj[sort]={"order" : order}
		query.sort = [];
		query.sort.push(sortObj);
	}
	
	var indexName='mm-' + CultureCode.toLowerCase();
	
	var sr=yield client.search({
	  index: indexName,
	  type : IndexType,
	  body: query
	});
	
	var naturalResult=_.pluck(sr.hits.hits,'_source');
	var doc={ 
		HitsTotal : sr.hits.total,
		PageTotal : naturalResult.length, 
		PageSize : pagesize, 
		PageCurrent : pageno, 
		PageData : naturalResult  
	};			
	return naturalResult;
});

module.exports = Factory;
