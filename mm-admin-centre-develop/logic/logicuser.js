"use strict";
var CONSTANTS = require('../logic/constants');

var Q=require('q');
var _ = require('lodash');
var config=require('../config');
var cu = require('../lib/commonutil.js');
var cc = require('../lib/commoncrypt.js');

var Factory={};

Factory.getLinkPath=function (User)
{
	var result=config.url; 
	if (User==null)
		throw new Error("User is null");
	if (result==null)
		throw new Error("config.url is not set");
	if (User.UserTypeId==null)
	{
		throw new Error("UserTypeId is null for UserId " + User.UserId + "'");
	}
	else if (User.UserTypeId==CONSTANTS.USER_TYPE.MM)
	{
		result= result + "/admin/#";
	}
	else if (User.UserTypeId==CONSTANTS.USER_TYPE.MERCHANT)
	{
		result= result + "/merchant/#";
	}
	else
	{
		throw new Error("Unkown UserTypeId of '" + User.UserTypeId + "' for UserId '" + User.UserId + "'");
	}
	return result;
}

Factory.isMobileUser=function (User)
{
	var isMobile=false;
	if (User==null)
		throw new Error("User is null");
	if (User.UserSecurityGroupArray==null)
		throw new Error("User.UserSecurityGroupArray is null");
	if (User.UserSecurityGroupArray.length==0)
		throw new Error("User.UserSecurityGroupArray is Empty");

	if (_.contains(User.UserSecurityGroupArray,CONSTANTS.USER_SECURITY_GROUP.MERCHANT_ORDER_PROCESSSING) || 
            _.contains(User.UserSecurityGroupArray,CONSTANTS.USER_SECURITY_GROUP.MERCHANT_PRODUCT_RETURN)) //order processing or returns
		isMobile=true;
	
	return isMobile;
}

var USER_SQL = "select u.UserId, u.UserKey, u.UserTypeId, u.MerchantId, u.InventoryLocationId, u.WeChatId, u.UserName, u.FirstName, u.LastName, u.MiddleName, u.DisplayName,u.Email,u.MobileCode, u.MobileNumber, u.CoverImage, u.ProfileImage, u.GeoCountryId, u.GeoProvinceId, u.GeoCityId, u.Gender, u.DateOfBirth, u.IsCuratorUser, u.StatusId, s.StatusNameInvariant, u.TimeZoneId, u.LanguageId, u.FollowerCount, u.FriendCount, u.FollowingUserCount, u.FollowingCuratorCount, u.FollowingMerchantCount, u.FollowingBrandCount, u.LastLogin, u.LastModified, u.ActivationToken, l.LanguageNameInvariant as CultureCode, (u.LastLogin<u.LastStatus) as PushLogout from User u, Status s, Language l where u.LanguageId=l.LanguageId and u.StatusId=s.StatusId and u.StatusId <> 1";

// get User by req object, UserId or UserKey
// reject if user not found
Factory.getUser= function (tr, key, merchantId)
{
  var merchantSQL = cu.isBlank(merchantId) ? '' : ' and u.MerchantId=:MerchantId';
  var User;
	return Q.when().then(function() {
    if (cu.checkIdBlank(key)) {
			throw {AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'};
    }
    if (cu.isUUID(key)) {
      return tr.queryOne(USER_SQL + " and u.UserKey=:UserKey" + merchantSQL, {UserKey: key, MerchantId: merchantId});
    } else {
      return tr.queryOne(USER_SQL + " and u.UserId=:UserId" + merchantSQL, {UserId: key, MerchantId: merchantId});
    }
	})
	.then(function(data) {
    if (!data) throw {AppCode:"MSG_ERR_USER_NOT_FOUND"};
    User = data;

		return tr.queryMany("select SecurityGroupId from UserSecurityGroup where UserId=:UserId",User).then(function(data) {
			User.UserSecurityGroupArray=[];
			for (var i in data)
				User.UserSecurityGroupArray.push(data[i].SecurityGroupId);
		})
	})
	.then(function() {
		if (!cu.checkIdBlank(User.MerchantId))
		{
			return tr.queryMany("select InventoryLocationId from UserInventoryLocation where UserId=:UserId",User).then(function(data) {
				User.UserInventoryLocationArray=[];
				for (var i in data)
					User.UserInventoryLocationArray.push(data[i].InventoryLocationId);
			})
      .then(function() {
        return tr.queryOne("select * from Merchant where MerchantId=:MerchantId",User).then(function(data) {
          User.Merchant=data; //store the returned user
        });
      })
		}
	})
	.then(function() {
    return User;
	});
};

// Factory.getAll = function(tr){

// 	var deferred = Q.defer();
// 	var UserList=null;
// 	Q.when()
// 	.then(function(){
// 		return tr.begin();
// 	})
// 	.then(function() {
// 		return tr.queryMany("select u.UserId, u.UserTypeId, u.MerchantId, u.InventoryLocationId, u.FirstName, u.LastName, u.MiddleName, u.DisplayName,u.Email,u.MobileCode, u.MobileNumber, u.ProfileImage,u.StatusId, s.StatusNameInvariant, u.LastLogin, u.LastModified from User u, Status s where u.StatusId=s.StatusId and u.StatusId <> 1 and u.UserTypeId=1 order by u.LastModified desc").then(function(data) {
// 			UserList=data;
// 		})
// 	})
// 	.then(function() {
// 		var promises = UserList.map(function(iUser){
// 			return tr.queryMany("select SecurityGroupId from UserSecurityGroup where UserId=:UserId",{UserId:iUser.UserId}).then(function(data) {
// 				iUser.UserSecurityGroupArray=[];
// 				for (var i in data)
// 					iUser.UserSecurityGroupArray.push(data[i].SecurityGroupId);
// 			});
// 		});
// 		return Q.all(promises);
// 	})
// 	.then(function() {
// 		for (var i = 0; i < UserList.length; i++) {
// 			UserList[i].isMobileUser = false;
// 		}
// 		// res.json(UserList);
// 		//console.log(UserList);
// 		//return tr.commit();
// 		deferred.resolve(UserList);	
// 	})
// 	.catch(function(err){
// 		// res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_LIST"));
// 		// console.log('Rollback');
// 		// console.dir(err);
// 		//return tr.rollback();
// 		deferred.reject(err);
// 	})
// 	.done(function(){

// 	});
// 	return deferred.promise;
// }


// Factory.listMerchant = function(id, tr){
// 	var deferred = Q.defer();
// 	var UserList=null;
// 	Q.when()
// 	// .then(function(){
// 	// 	if (cu.isBlank(req.query.id))
// 	// 		throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
// 	// })
// 	.then(function(){
// 		return tr.begin();
// 	})
// 	.then(function() {
// 		return tr.queryMany("select u.UserId, u.UserTypeId, u.MerchantId, u.InventoryLocationId, u.FirstName, u.LastName, u.MiddleName, u.DisplayName,u.Email,u.MobileCode, u.MobileNumber, u.ProfileImage,u.StatusId, s.StatusNameInvariant, u.LastLogin, u.LastModified from User u, Status s where u.StatusId=s.StatusId and u.StatusId <> 1 and u.UserTypeId=2 and u.MerchantId=:MerchantId order by u.LastModified desc",{MerchantId:id}).then(function(data) {
// 			UserList=data;
// 		})
// 	})
// 	.then(function() {
// 		var promises = UserList.map(function(iUser){
// 			return tr.queryMany("select SecurityGroupId from UserSecurityGroup where UserId=:UserId",{UserId:iUser.UserId}).then(function(data) {
// 				iUser.UserSecurityGroupArray=[];
// 				for (var i in data)
// 					iUser.UserSecurityGroupArray.push(data[i].SecurityGroupId);
// 			});
// 		});
// 		return Q.all(promises);
// 	})
// 	.then(function() {

// 		//checking if user is mobile or not....
// 		for (var i = 0; i < UserList.length; i++) {
// 			if (UserList[i].UserSecurityGroupArray) {
// 	            if (UserList[i].UserSecurityGroupArray.indexOf(11) > -1 || UserList[i].UserSecurityGroupArray.indexOf(12) > -1) {
// 	             	UserList[i].isMobileUser = true;
// 	            }else{
// 	               UserList[i].isMobileUser = false;
// 	            }
// 	        }
// 		}
// 		console.log(UserList);
// 		// res.json(UserList);
// 		// return tr.commit();
// 		deferred.resolve(UserList);
// 	})
// 	.catch(function(err){
// 		//res.status(500).json(cu.createErrorResult(err,"MSG_ERR_USER_LIST"));
// 		//console.log('Rollback');
// 		console.dir(err);
// 		deferred.reject(err);
// 		//return tr.rollback();

// 	})
// 	.done(function(){

// 	});

// 	return deferred.promise;
// }

/**
 * Get UserType list
 * 
 * @returns {Q@call;defer.promise}
 */
Factory.getUserTypeList = function (tr) {
    var deferred = Q.defer();
    var userTypeList = [];
    Q.when()
    .then(function () {
        var sql = "SELECT UserTypeId, UserTypeNameInvariant from UserType";
        return tr.queryMany(sql).then(function (data) {
            userTypeList = data;
        });
    })
    .then(function () {
        deferred.resolve(userTypeList);
    })
    .catch(function (err) {
        console.dir(err);
        deferred.reject(err);
    })
    .done(function () {

    });
    return deferred.promise;
};

Factory.saveMobileVerification = function (User, tr) {
  var result = {
    MobileCode: User.MobileCode,
    MobileNumber: User.MobileNumber,
    MobileVerificationToken: ('000000' + Math.floor(Math.random() * 999999)).slice(-6)
  };

  return Q.when()
  .then(function(){
    // check if required fields are available.
    if(cu.isBlank(User.MobileCode) || cu.isBlank(User.MobileNumber)){
      throw { AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING' };
    }
  })
  .then(function(){
    // check if MobileVerification > 3
    var sql = "SELECT COUNT(*) Count from MobileVerification WHERE MobileNumber=:MobileNumber AND MobileCode=:MobileCode AND LastCreated >= DATE_SUB(NOW(),INTERVAL 1 HOUR)";
    return tr.queryOne(sql, User).then(function(data){
      if(data.Count >= 3){
        throw { AppCode : 'MSG_ERR_MOBILE_VERIFICATION_COUNT_EXCEED' };
      }
    });
  })
  .then(function(){
    var sql = "INSERT INTO MobileVerification (MobileCode, MobileNumber, MobileVerificationToken) VALUES (:MobileCode, :MobileNumber, :MobileVerificationToken)";
    return tr.queryExecute(sql, result).then(function(data){
      result.MobileVerificationId = data.insertId;
    });
  })
  .then(function(){
    return result; 
  });
};

Factory.checkMobileVerification = function (User, tr){
  return Q.when()
  .then(function(){
    // check if all required fields are specified.
    if(cu.isBlank(User.MobileVerificationId) || cu.isBlank(User.MobileVerificationToken) || cu.isBlank(User.MobileCode) || cu.isBlank(User.MobileNumber)){ 
      throw { AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING' }; 
    }
  })
  .then(function(){
    // check if MobileVerificationAttempt > 3
    var sql = "SELECT COUNT(*) Count from MobileVerificationAttempt WHERE MobileNumber=:MobileNumber AND MobileCode=:MobileCode AND LastCreated >= DATE_SUB(NOW(), INTERVAL 12 HOUR)";
    return tr.queryOne(sql, User).then(function(data){
      if(data.Count > 3){
        throw { AppCode : 'MSG_ERR_MOBILE_VERIFICATION_ATTEMPT_COUNT_EXCEED' };
      }
    });
  })
  .then(function(){
    // check within 10 minutes and if token is valid
    var sql = "SELECT count(*) Count FROM MobileVerification WHERE MobileVerificationId = :MobileVerificationId AND MobileCode = :MobileCode AND MobileNumber = :MobileNumber AND MobileVerificationToken = :MobileVerificationToken AND LastCreated > date_sub(now(), interval 12 HOUR)";
      return tr.queryOne(sql, User).then(function(data){
        if(data.Count !== 1){
            var sqlinsert = "INSERT INTO MobileVerificationAttempt (MobileCode, MobileNumber) VALUES (:MobileCode, :MobileNumber)";
            return tr.queryExecute(sqlinsert, User).then(function(ins){
              User.MobileVerificationAttemptId = ins.insertId;
              throw { AppCode: 'LB_CA_VERCODE_INVALID' };
            });
          }
     })
  })
};


Factory.resetMobileVerification = function (User, tr) {
  var sql= 'DELETE FROM MobileVerification where MobileVerificationId = :MobileVerificationId';
  return tr.queryExecute(sql, User);
}

Factory.checkUserName = function (Bale, tr) {
    if (cu.isBlank(Bale.UserName)) {
        return Q.reject({AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'});
    }
    return tr.queryOne(
        cu.checkIdBlank(Bale.UserId)
            ? "select * from User where UserName=:UserName and StatusId <> 1"
            : "select * from User where UserName=:UserName and UserId<>:UserId and StatusId <> 1",
        Bale
    ).then(function(data) {
        if (data) throw {AppCode: "MSG_ERR_USERNAME_REGISTERED"};
    });
};


Factory.checkUserEmail = function (Bale, tr) {
  if (cu.isBlank(Bale.Email)) {
    return Q.reject({AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'});
  }
  return tr.queryOne(
    cu.checkIdBlank(Bale.UserId)
    ? "select * from User where Email=:Email and StatusId <> 1"
    : "select * from User where Email=:Email and UserId<>:UserId and StatusId <> 1", 
    Bale
  ).then(function(data) {
    if (data) throw {AppCode: "MSG_ERR_EMAIL_REGISTERED"};
  });
};

Factory.checkUserMobile = function (Bale, tr) {
  if (cu.isBlank(Bale.MobileCode) || cu.isBlank(Bale.MobileNumber)) {
    return Q.reject({AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'});
  }
  return tr.queryOne(
    cu.checkIdBlank(Bale.UserId)
    ? "select * from User where MobileNumber=:MobileNumber and MobileCode=:MobileCode and StatusId <> 1"
    : "select * from User where MobileNumber=:MobileNumber and MobileCode=:MobileCode and UserId<>:UserId and StatusId <> 1", 
    Bale
  ).then(function(data) {
    if (data) throw {AppCode: "MSG_ERR_MOBILE_REGISTERED"};
  });
};

var USERNAME_REGEX = /^[a-zA-z][\w-]{0,34}$/;

Factory.checkUserUserName = function (Bale, tr) {
  if (cu.isBlank(Bale.UserName)) {
    return Q.reject({AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'});
  }
  if (!USERNAME_REGEX.test(Bale.UserName)) {
    return Q.reject({AppCode: 'MSG_ERR_CA_USERNAME_PATTERN'});
  }
  return tr.queryOne(
    cu.checkIdBlank(Bale.UserId)
    ? "select * from User where UserName=:UserName and StatusId <> 1"
    : "select * from User where UserName=:UserName and UserId<>:UserId and StatusId <> 1", 
    Bale
  ).then(function(data) {
    if (data) throw {AppCode: 'MSG_ERR_CA_DUP_USERNAME'};
  });
};

Factory.saveUserSecurityGroup = function (Bale, SecurityGroupArray, tr) {
  return tr.queryExecute(
    "delete from UserSecurityGroup where UserId=:UserId;",
    { UserId: Bale.UserId }
  )
  .then(function () {
    return Q.all(
      SecurityGroupArray.map(function(rid){
        return tr.queryExecute(
          "insert into UserSecurityGroup (UserId,SecurityGroupId) values(:UserId,:SecurityGroupId)",
          { UserId: Bale.UserId, SecurityGroupId: rid }
        );
      })
    );
  });
};

Factory.saveUserInventoryLocation = function (Bale, InventoryLocationArray, tr) {
  return tr.queryExecute(
    "delete from UserInventoryLocation where UserId=:UserId;",
    { UserId: Bale.UserId }
  )
  .then(function () {
    return Q.all(
      InventoryLocationArray.map(function(rid){
        return tr.queryExecute(
          "insert into UserInventoryLocation (UserId,InventoryLocationId) values(:UserId,:InventoryLocationId)",
          { UserId: Bale.UserId, InventoryLocationId: rid }
        );
      })
    );
  });
};

Factory.updateUserPassword = function(Bale, tr){
  Bale.Salt = cc.createSalt();
  Bale.Hash = cc.createHash(Bale.Password, Bale.Salt);
  var sql = "UPDATE User SET Salt=:Salt, Hash=:Hash WHERE UserId=:UserId";
  return tr.queryExecute(sql, Bale);
};

Factory.changePassword = function(Bale, tr){
  return Q.when()
  .then(function(){
    if ( cu.isBlank(Bale.CurrentPassword) || cu.isBlank(Bale.Password) ) {
      return Q.reject({AppCode: 'MSG_ERR_USER_REQUIRED_FIELD_MISSING'});
    }
  })
  .then(function(){
    var sql = "SELECT * FROM User where UserKey=:UserKey";
    return tr.queryOne(sql, Bale).then(function(data){
      Bale.Hash = cc.createHash(Bale.CurrentPassword, data.Salt);
      if (!(data.Hash == Bale.Hash)) {
        return Q.reject({AppCode: 'MSG_ERR_USER_NOT_FOUND'});
      }
      return data;
    })
  })
  .then(function(user){
    return Factory.updateUserPassword({ Password : Bale.Password, UserId : user.UserId}, tr);
  });
};

Factory.UpdateMobileCodeandMobileNumber = function(Bale, tr){
  var sql = "Update User set MobileCode=:MobileCode, MobileNumber=:MobileNumber where UserId=:UserId";
  return tr.queryExecute(sql, Bale);
};

Factory.changeUsername = Q.async(function* (tr, Bale){
  var sql = "SELECT * FROM User WHERE UserName=:UserName AND UserKey != :UserKey";
  var check = yield tr.queryOne(sql, Bale);

  if(check){
    throw { AppCode : "MSG_ERR_USERNAME_ALREADY_EXISTS" };
  }
  
  sql = "UPDATE User SET UserName=:UserName WHERE UserKey=:UserKey";
  return yield tr.queryExecute(sql, Bale);
});

module.exports = Factory;
