"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');
var li = require('./logicinventory.js');

var Factory = {};

Factory.standardImport = Q.async(function* (client, indexName, type, list, idName, messageList) {
	let EntityName='';
	for (let row of list)
	{
		if (row.Entity && type=='term')
			EntityName=' (' + row.Entity + ') '; //for term list put the sub type in so we know which set was just imported
		let item={
		  index: indexName,
		  type: type,
		  id: row[idName],
		  body: row
		}; 
		let radd=yield client.index(item);
	}
	messageList.push('Ingested ' + type + EntityName + ' Count:' + list.length);	
});

module.exports = Factory;
