"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');
var lp = require('./logicproduct.js');


var crypto = require('crypto');

var Factory = {};

Factory.getWishlistLogic = Q.async(function * (tr, CultureCode, CartKey, UserKey, CreateNew){
	var cart = null;
	var User =  null;

	if(CartKey){
		cart = yield tr.queryOne('select CartKey, CartId, UserId from Cart where CartKey=:CartKey and StatusId=:StatusId and CartTypeId=:CartTypeId',{CartKey:CartKey,StatusId:2,CartTypeId:2});
	}else if(UserKey){
		User = yield tr.queryOne('select UserId from User where UserKey=:UserKey',{UserKey:UserKey});
		if (!User)
			throw { AppCode: 'MSG_ERR_USER_NOT_FOUND', Message : "User not found." };
		cart = yield tr.queryOne('select CartKey, CartId, UserId from Cart where CartId=(select max(CartId) as CartId from Cart where UserId=:UserId and StatusId=:StatusId and CartTypeId=:CartTypeId)',{UserId:User.UserId,StatusId:2,CartTypeId:2});
	}

	if(!cart){
		if(CreateNew){
			var ret = yield tr.queryExecute('insert into Cart (UserId, CartTypeId, CartKey, StatusId, LastModified, LastCreated) values (:UserId,:CartTypeId,(SELECT uuid()),:StatusId,UTC_TIMESTAMP(),UTC_TIMESTAMP())',{UserId:((User && User.UserId) || 0),CartTypeId:2,StatusId:2});
			cart = yield tr.queryOne('select CartKey, CartId, UserId from Cart where CartId=:CartId',{CartId:ret.insertId});
			return cart;
		}else{
			throw { AppCode: 'MSG_ERR_WISHLIST_NOT_FOUND', Message : "Wishlist not found" };
		}
	}else{
		return cart;
	}
});


//create new wish list for unauthenticated and/or authenticated users.
Factory.create = Q.async(function* (tr, CultureCode, UserKey) {
	var User = { UserId : 0 };
	if(!(UserKey == 0 || UserKey == "0")){
		User=yield tr.queryOne('select * from User where UserKey=:UserKey',{UserKey:UserKey});
		if (!User)
			throw { AppCode: 'MSG_ERR_USER_NOT_FOUND', Message : "User not found." };
	}
	//var CartKey = yield crypto.pseudoRandomBytes(16).toString('hex');
	var ret=yield tr.queryExecute('insert into Cart (UserId, CartTypeId, CartKey, StatusId, LastModified, LastCreated) values (:UserId,:CartTypeId,(SELECT uuid()),:StatusId,UTC_TIMESTAMP(),UTC_TIMESTAMP())',{UserId:User.UserId,CartTypeId:2,StatusId:2});
	var cart=yield tr.queryOne('select CartKey, CartId from Cart where CartId=:CartId',{CartId:ret.insertId});
	cart=Factory.viewByCartKey(tr,CultureCode,cart.CartKey);
	return cart;
});

//assign the unauthenticated wishlist to a specific user (authenticated).
Factory.userUpdate = Q.async(function* (tr, CultureCode, CartKey, UserKey) {
	var User=yield tr.queryOne('select * from User where UserKey=:UserKey',{UserKey:UserKey});
	if (!User)
		throw { AppCode: 'MSG_ERR_USER_NOT_FOUND', Message : "User not found." };
	var ret=yield tr.queryExecute('update Cart set UserId=:UserId where CartKey=:CartKey',{CartKey:CartKey,UserId:User.UserId});
	var cart=Factory.viewByCartKey(tr,CultureCode,CartKey);
	return cart;
});

//add item to wish list, since this a wishlist the default qty is zero. qty will be change if the wishlist item will add to the main basket
Factory.itemAdd = Q.async(function* (tr, CultureCode, CartKey, UserKey, StyleCode, SkuId, ColorKey, MerchantId) {
	var cart = yield Factory.getWishlistLogic(tr, CultureCode, CartKey, UserKey, true);
	var checkIfExists = yield tr.queryOne('select * from CartItem where SkuId=:SkuId and CartId=:CartId and StyleCode=:StyleCode and ColorKey=:ColorKey and MerchantId=:MerchantId and StatusId=:StatusId', {CartId:cart.CartId,SkuId:SkuId,StyleCode:StyleCode,ColorKey:ColorKey,StatusId:2,MerchantId:MerchantId});
	if(!checkIfExists)
		yield tr.queryExecute('insert into CartItem (CartId,SkuId,Qty,LastModified, LastCreated, StyleCode, ColorKey, StatusId, MerchantId) values (:CartId,:SkuId,:Qty,UTC_TIMESTAMP(),UTC_TIMESTAMP(),:StyleCode,:ColorKey,:StatusId, :MerchantId)',{CartId:cart.CartId,SkuId:SkuId,Qty:0,StyleCode:StyleCode,ColorKey:ColorKey,StatusId:2,MerchantId:MerchantId});
	cart = Factory.viewByCartKey(tr,CultureCode,cart.CartKey);
	return cart;
});

//remove item to wish list
Factory.itemRemove = Q.async(function* (tr, CultureCode, CartKey, UserKey, CartItemId) {
	var cart = yield Factory.getWishlistLogic(tr, CultureCode, CartKey, UserKey, false);
	var ret = yield tr.queryExecute('update CartItem set StatusId=:StatusId where CartId=:CartId and CartItemId=:CartItemId',{CartId:cart.CartId,CartItemId:CartItemId,StatusId:1});
	cart = Factory.viewByCartKey(tr,CultureCode,cart.CartKey);
	return cart;
});

Factory.viewByCartKey = Q.async(function* (tr, CultureCode, CartKey) {
	var cart = yield Factory.getWishlistLogic(tr, CultureCode, CartKey, null, false);
	cart.UserKey = null;
	var cartItemSql = "select * from CartItem where CartId=:CartId and StatusId=:StatusId";
	cart.CartItems = yield tr.queryMany(cartItemSql,{CartId:cart.CartId,StatusId:2});

	for (var StyleL of cart.CartItems) {
		var MerchantId = null;

		if (!cu.checkIdBlank(StyleL.MerchantId)){
			MerchantId = StyleL.MerchantId;
		}

		var tmpStyle = yield lp.styleList(MerchantId,null,StyleL.StyleCode,null,null,CultureCode,1,1,null,tr);
		if(tmpStyle.PageData.length > 0){
			StyleL.Style = tmpStyle.PageData[0];
		}else{
			//put empty object or null
			StyleL.Style = {};
		}
	};


	if(cart.UserId == 0){
		cart.UserKey = 0;
	}else{
		var User=yield tr.queryOne('select * from User where UserId=:UserId',{UserId:cart.UserId});
		if(User){
			cart.UserKey = User.UserKey;
		}else{
			cart.UserKey = cart.UserId;
		}
	};
	
	delete cart.UserId; //to hide the userId
	delete cart.CartId; //to hide the cartId
	return cart;
});

Factory.viewByUserKey = Q.async(function* (tr, CultureCode, UserKey) {
	var User=yield tr.queryOne('select * from User where UserKey=:UserKey',{UserKey:UserKey});
	if (!User)
		throw { AppCode: 'MSG_ERR_USER_NOT_FOUND', Message : "User not found." };

	// use CartTypeId = 2 for wishlist.
	var ret=yield tr.queryOne('select CartKey, CartId from Cart where CartId=(select max(CartId) as CartId from Cart where UserId=:UserId and StatusId=:StatusId and CartTypeId=:CartTypeId)',{UserId:User.UserId,StatusId:2,CartTypeId:2});
	if(!ret || isNaN(ret.CartId) || !ret.CartId){
		throw { AppCode: 'MSG_ERR_WISHLIST_NOT_FOUND', Message : "Wishlist not found" };
	};
	var cart=Factory.viewByCartKey(tr,CultureCode,ret.CartKey);
	return cart;
});

Factory.mergeWishlist = Q.async(function* (tr, CultureCode, CartKey, UserKey, MergeCartKey){
	var cart = yield Factory.getWishlistLogic(tr, CultureCode, CartKey, UserKey, true);
	var mergeCart=yield tr.queryOne('select * from Cart where CartKey=:MergeCartKey and StatusId=:StatusId',{MergeCartKey:MergeCartKey,StatusId:2,CartTypeId:2});
	if (!mergeCart)
		throw { AppCode: 'MSG_ERR_WISHLIST_MERGE_NOT_FOUND', Message : "Wishlist to merge not found" };

	//var ItemList = yield tr.queryMany("select ci.StyleCode,ci.CartItemId from CartItem ci INNER JOIN CartItem ci2 on (ci.StyleCode = ci2.StyleCode) WHERE ci2.CartId=:CartId and ci.CartId=:MergeId and ci.StatusId=:StatusId",{MergeId:mergeCart.CartId,CartId:cart.CartId,StatusId:2});
	var ItemList = yield tr.queryMany("SELECT * from CartItem where CartId=:MergeId and StatusId=:StatusId and StyleCode IN (SELECT StyleCode from CartItem where CartId=:CartId and StatusId=:StatusId) and MerchantId IN (SELECT MerchantId from CartItem where CartId=:CartId and StatusId=:StatusId);",{MergeId:mergeCart.CartId,CartId:cart.CartId,StatusId:2});
	//change status of same wishlist item by StyleCode
	for(var Item of ItemList){
		var rem=yield tr.queryExecute('update CartItem set StatusId=:StatusId where CartItemId=:CartItemId',{CartItemId:Item.CartItemId,StatusId:1});
	};
	//move remaining
	var ret=yield tr.queryExecute('update CartItem set CartId=:CartId where CartId=:MergeId and StatusId=:StatusId',{CartId:cart.CartId,MergeId:mergeCart.CartId,StatusId:2});
	var ret=yield tr.queryExecute('update Cart set StatusId=:StatusId where CartId=:MergeId',{MergeId :mergeCart.CartId,StatusId:1});

	cart=Factory.viewByCartKey(tr,CultureCode,cart.CartKey);
	return cart;
});


module.exports = Factory;
