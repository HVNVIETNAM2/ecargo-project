"use strict";

var CONSTANTS = {
    STATUS: {
        DELETED: 1,
        ACTIVE: 2,
        PENDING: 3,
        INACTIVE: 4
    },
    INVENTORY_STATUS: {
        ALL:-1,
        IN_STOCK: 1,
        LOW_STOCK: 2,
        OUT_OF_STOCK: 3,
        NA: 4
    },
    USER_TYPE: {
        MM: 1,
        MERCHANT: 2,
        CONSUMER: 3
    },
    APP: {
        MM: 'MM',
        MERCHANT: 'MERCHANT',
        CONSUMER: 'CONSUMER'
    },
    USER_SECURITY_GROUP: {
        MM_ADMIN: 1,
        MM_CUSTOMER_SERVICE: 2,
        MM_FINANCE: 3,
        MM_TECH_OPS: 4,
        MERCHANT_FINANCE: 5,
        MERCHANT_ADMIN: 6,
        MERCHANT_CUSTOMER_SERVICE: 7,
        MERCHANT_CONTENT_MANAGER: 9,
        MERCHANT_MERCHANDISER: 10,
        MERCHANT_ORDER_PROCESSSING: 11,
        MERCHANT_PRODUCT_RETURN: 12
    },
    EXCEL: {
        PRODUCT: {
            MISSING_ERR_TMP_CODE: "11",
            NOT_FOUND_ERR_TMP_CODE: "12",
            NOT_VALID_ERR_TMP_CODE: "14"
        },
        INVENTORY: {
            MISSING_ERR_TMP_CODE: "12",
            NOT_FOUND_ERR_TMP_CODE: "15",
            NOT_VALID_ERR_TMP_CODE: "17"
        },
    },
    PRODUCT_IMG_TYPE: {
        FEATURE: 1,
        COLOR: 2,
        DESC: 3
    },

    PRODUCT_IMG_TYPE_CODE : {
      FEATURE: 'Feature',
      COLOR: 'Color',
      DESC: 'Desc'
    },

    JOURNAL_EVENT_TYPE: {
        MANUAL_EDIT: 1,
        DATA_IMPORT: 2
    },

    CULTURE_CODE:{
      EN: 'EN',
      CHS: 'CHS',
      CHT: 'CHT'
    },

    JOURNAL_TYPE: {
        INSERT: 1,
        UPDATE: 2
    },
    MEGABYTE: 1048576, //i.e. 1024 * 1024
    NAME_REGX: {
        PROD_IMG: /^([A-Z0-9]+)_?([A-Z0-9-_]+)?_([0-9]+)(.\w+)$/i
    },
    INVENTORY_FILTER :{
        ALL: -1,
        WITH_INVENTORY: 1,
        WITHOUT_INVENTORY: 0
    },
//  TODO: add below as supported case
//  YYYMMDD HH:mm:ss
//  YYYMMDDTHH:mm:ss
    DATE_FORMAT:'YYYY-MM-DD HH:mm:ss'
};

module.exports = CONSTANTS;
