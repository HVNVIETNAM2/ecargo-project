"use strict";
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');

var Factory = {};

Factory.create = Q.async(function* (tr, Bale){
	var sql = "INSERT INTO Ticket (TicketTypeId,TicketSummary,UserId,CultureCode,LastModifiedUserId,TicketStatusId) VALUES (:TicketTypeId,:TicketSummary,:UserId,:CultureCode,:UserId,3)";
	return yield tr.queryExecute(sql, Bale);
});

module.exports = Factory;