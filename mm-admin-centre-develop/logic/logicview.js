"use strict";
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');

var Factory = {};

Factory.user = Q.async(function* (tr, Bale){
  var sql = "SELECT u.UserKey,u.UserName,u.FirstName,u.LastName,u.MiddleName,u.DisplayName,l.LanguageNameInvariant as CultureCode,u.IsCuratorUser,u.ProfileImage,u.CoverImage,u.FollowerCount,u.FriendCount,u.FollowingUserCount,u.FollowingCuratorCount,u.FollowingMerchantCount,u.FollowingBrandCount FROM User u INNER JOIN Language l ON u.LanguageId=l.LanguageId WHERE u.UserKey=:UserKey";
  return yield tr.queryOne(sql, Bale);
});

Factory.merchant = Q.async(function* (tr, Bale){
  var sql = "SELECT m.MerchantId,m.FollowerCount,m.HeaderLogoImage,m.SmallLogoImage,m.LargeLogoImage,m.ProfileBannerImage,mc.MerchantName,mc.MerchantDesc,m.MerchantTypeId,mtc.MerchantTypeName,m.MerchantCompanyName,m.MerchantSubDomain FROM Merchant m INNER JOIN MerchantCulture mc ON m.MerchantId=mc.MerchantId AND mc.CultureCode=:CultureCode INNER JOIN MerchantTypeCulture mtc ON m.MerchantTypeId=mtc.MerchantTypeId AND mtc.CultureCode=:CultureCode WHERE m.MerchantId=:MerchantId AND m.StatusId=2";
  var MerchantInfo = yield tr.queryOne(sql, Bale);

  if (!MerchantInfo) throw { AppCode: 'MSG_ERR_MERCHANT_NOT_EXISTS' };

  sql = "SELECT MerchantImageId,ImageTypeCode,MerchantImage,Position FROM MerchantImage WHERE MerchantId=:MerchantId";
  MerchantInfo.MerchantImages = yield tr.queryMany(sql, Bale);
  return MerchantInfo;
});

module.exports = Factory;
