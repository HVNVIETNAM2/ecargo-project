"use strict";
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');

var Factory = {};

// Tag Type
Factory.insertTagType = function(tr, Bale){
	return Q.when()
	.then(function(){
		var sql = "SELECT * FROM TagType WHERE TagTypeName=:TagTypeName";
		return tr.queryOne(sql, Bale).then(function(data){
			if(data){
				throw { AppCode : 'MSG_ERR_TAG_TYPE_ALREADY_EXIST' };
			}
		});
	})
	.then(function(){
		var sql = "INSERT INTO TagType (TagTypeName) VALUES (:TagTypeName)";
		return tr.queryExecute(sql, Bale).then(function(data){
			Bale = data; 
		});
	})
	.then(function(){
		return Bale;
	});
};

Factory.updateTagType = function(tr, Bale){
	var sql = "UPDATE TagType SET TagTypeName = :TagTypeName WHERE TagTypeId = :TagTypeId";
	return tr.queryExecute(sql, Bale);
};

Factory.deleteTagType = function(tr, Bale){
	var sql = "DELETE FROM TagType WHERE TagTypeId = :TagTypeId";
	return tr.queryExecute(sql, Bale);
};

// Tag
Factory.list = function(tr, Bale){
  Bale.limit = parseInt(Bale.limit) || 100;
  Bale.start = parseInt(Bale.start) || 0;

	var sql = "SELECT Tag.TagId, TagCulture.TagName, Tag.Priority, TagType.TagTypeId, TagType.TagTypeName FROM Tag INNER JOIN TagType ON Tag.TagTypeId = TagType.TagTypeId INNER JOIN TagCulture ON TagCulture.TagId = Tag.TagId AND TagCulture.CultureCode = :CultureCode WHERE Tag.TagTypeId = :TagTypeId ORDER BY Tag.Priority ASC, TagName ASC LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, { TagTypeId : Bale.tagtypeid, CultureCode : Bale.cc });
};

Factory.insertTag = function(tr, Bale){
	if(!Bale.Priority){ Bale.Priority = 0; }

	return Q.when()
	.then(function(){
		var sql = "SELECT * FROM Tag WHERE TagNameInvariant=:TagNameEN AND TagTypeId=:TagTypeId";
		return tr.queryOne(sql, Bale).then(function(data){
			if(data){
				throw { AppCode : 'MSG_ERR_TAG_ALREADY_EXIST' };
			}
		});
	})
	.then(function(){
		var sql = "INSERT INTO Tag (TagNameInvariant,TagTypeId,Priority) VALUES (:TagNameEN,:TagTypeId,:Priority)";
		return tr.queryExecute(sql, Bale).then(function(data){
			Bale.TagId = data.insertId; 
		});
	})
	.then(function(){
		var sql = "INSERT INTO TagCulture (TagId,CultureCode,TagName) VALUES (:TagId,'EN',:TagNameEN)";
		return tr.queryExecute(sql, Bale);
	})
	.then(function(){
		var sql = "INSERT INTO TagCulture (TagId,CultureCode,TagName) VALUES (:TagId,'CHT',:TagNameCHT)";
		return tr.queryExecute(sql, Bale);
	})
	.then(function(){
		var sql = "INSERT INTO TagCulture (TagId,CultureCode,TagName) VALUES (:TagId,'CHS',:TagNameCHS)";
		return tr.queryExecute(sql, Bale);
	})
  .then(function(){
    return Bale;
  });
};

Factory.updateTag = function(tr, Bale){
	if(!Bale.Priority){ Bale.Priority = 0; }

	return Q.when()
	.then(function(){
		var sql = "UPDATE Tag SET TagNameInvariant = :TagNameEN, TagTypeId = :TagTypeId, Priority = :Priority WHERE TagId = :TagId";
		return tr.queryExecute(sql, Bale);
	})
	.then(function(){
		var sql = "UPDATE TagCulture SET TagName = :TagNameEN WHERE TagId = :TagId AND CultureCode='EN'";
		return tr.queryExecute(sql, Bale);
	})
	.then(function(){
		var sql = "UPDATE TagCulture SET TagName = :TagNameCHT WHERE TagId = :TagId AND CultureCode='CHT'";
		return tr.queryExecute(sql, Bale);
	})
	.then(function(){
		var sql = "UPDATE TagCulture SET TagName = :TagNameCHS WHERE TagId = :TagId AND CultureCode='CHS'";
		return tr.queryExecute(sql, Bale);
	})
	.then(function(){
		return Bale;
	});
};

Factory.deleteTag = function(tr, Bale){
	return Q.when()
	.then(function(){
		var sql = "DELETE FROM Tag WHERE TagId = :TagId";
		return tr.queryExecute(sql, Bale);
	})
	.then(function(){
		var sql = "DELETE FROM TagCulture WHERE TagId = :TagId";
		return tr.queryExecute(sql, Bale);
	})
  .then(function(){
    return Bale;
  });
};

// Merchant
Factory.deleteTagMerchant = function(tr, Bale){
	var sql = "DELETE FROM MerchantTag WHERE MerchantId = :MerchantId";
	return tr.queryExecute(sql, Bale);
};

Factory.insertTagMerchant = function(tr, Bale){
	if(!Bale.Priority){ Bale.Priority = 0; }

	return Q.when()
	.then(function(){
		var sql = "SELECT * FROM MerchantTag WHERE MerchantId=:MerchantId AND TagId=:TagId";
		return tr.queryOne(sql, Bale).then(function(data){
			if(data){
				throw { AppCode : 'MSG_ERR_MERCHANT_TAG_ALREADY_EXIST' };
			}
		});
	})
	.then(function(){
		var sql = "INSERT INTO MerchantTag (MerchantId,TagId,Priority) VALUES (:MerchantId,:TagId,:Priority)";
		return tr.queryExecute(sql, Bale).then(function(data){
			Bale = data; 
		});
	})
	.then(function(){
		return Bale;
	});
};

// User
Factory.deleteTagUser = function(tr, Bale){
	var sql = "DELETE FROM UserTag WHERE UserId = :UserId";
	return tr.queryExecute(sql, Bale);
};

Factory.insertTagUser = function(tr, Bale){
	if(!Bale.Priority){ Bale.Priority = 0; }

	return Q.when()
	.then(function(){
		var sql = "SELECT * FROM UserTag WHERE UserId=:UserId AND TagId=:TagId";
		return tr.queryOne(sql, Bale).then(function(data){
			if(data){
				throw { AppCode : 'MSG_ERR_USER_TAG_ALREADY_EXIST' };
			}
		});
	})
	.then(function(){
		var sql = "INSERT INTO UserTag (UserId,TagId,Priority) VALUES (:UserId,:TagId,:Priority)";
		return tr.queryExecute(sql, Bale).then(function(data){
			Bale = data; 
		});
	})
	.then(function(){
		return Bale;
	});
};

// Factory to list all user merchants
Factory.UserMerchants = function(tr, UserId, CultureCode){
	var sql = "SELECT MerchantTag.MerchantId, MerchantCulture.MerchantName, Merchant.MerchantNameInvariant, Merchant.SmallLogoImage, Merchant.LargeLogoImage, Merchant.FollowerCount FROM UserTag INNER JOIN MerchantTag ON UserTag.TagId = MerchantTag.TagId INNER JOIN Merchant ON MerchantTag.MerchantId = Merchant.MerchantId INNER JOIN MerchantCulture ON MerchantTag.MerchantId = MerchantCulture.MerchantId AND MerchantCulture.CultureCode=:CultureCode WHERE UserTag.UserId=:UserId GROUP BY Merchant.MerchantId ORDER BY COUNT(*) DESC, MerchantTag.Priority ASC LIMIT 20";
	return tr.queryMany(sql, { UserId : UserId, CultureCode : CultureCode });
};

Factory.UserCurators = function(tr, UserId){
	var sql = "SELECT User.UserKey, User.UserName, User.FirstName, User.LastName, User.DisplayName, User.ProfileImage, User.FollowerCount FROM UserTag ut1 INNER JOIN UserTag ut2 ON ut1.TagId = ut2.TagId AND ut2.UserId <> :UserId INNER JOIN User on ut2.UserId = User.UserId AND (User.IsCuratorUser = 1 AND User.StatusId = 2) WHERE ut1.UserId=:UserId GROUP BY User.UserId ORDER BY COUNT(*) DESC, ut1.Priority ASC LIMIT 20";
	return tr.queryMany(sql, { UserId : UserId });
};

module.exports = Factory;
