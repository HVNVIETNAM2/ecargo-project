"use strict";
var CONSTANTS = require('../logic/constants');
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');

var Factory = {};

Factory.list = function(tr,CultureCode){

	return Q.when()
	.then(function(){
		var Bale={};
		Bale.CultureCode=CultureCode;
		return tr.queryMany("select m.*,mtc.MerchantTypeName, s.StatusNameInvariant, gcoC.GeoCountryName, mc.MerchantName, mc.MerchantDesc from Merchant m left join MerchantCulture mc on (m.MerchantId=mc.MerchantId and mc.CultureCode=:CultureCode) inner join MerchantType mt on (m.MerchantTypeId=mt.MerchantTypeId) inner join MerchantTypeCulture mtc on (mt.MerchantTypeId=mtc.MerchantTypeId and mtc.CultureCode=:CultureCode) inner join Status s on (m.StatusId=s.StatusId) inner join GeoCountry gco on (m.GeoCountryId=gco.GeoCountryId) inner join GeoCountryCulture gcoC on (gco.GeoCountryId=gcoC.GeoCountryId and gcoC.CultureCode=:CultureCode) where m.StatusId <> 1", Bale).then(function(data) {
			return data;
		});
	});
};

//check if record is the same status as merchant status
Factory.isPendingMerchant = function(tr, MerchantId, StatusId){
	var deferred = Q.defer();

	Q.when()
		.then(function() {
			return tr.queryOne("select MerchantId from Merchant where MerchantId=:MerchantId and StatusId=:StatusId",
				{MerchantId: MerchantId, StatusId: CONSTANTS.STATUS.PENDING}).then(function (data) {
				if (data !== null && StatusId !== CONSTANTS.STATUS.PENDING) {
					throw {
						AppCode: 'ERR_MSG_MERCHANT_PENDING'
					}
				}
			});
		})
		.then(function() {
			deferred.resolve(true);
		})
		.catch(function(err){
			deferred.reject(err);
		});

	return deferred.promise;
};

module.exports = Factory;