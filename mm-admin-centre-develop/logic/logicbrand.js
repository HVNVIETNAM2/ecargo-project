"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var config = require('../config');
var _ = require('lodash');
var cu = require('../lib/commonutil.js');
var lum = require('../logic/logicusermessaging.js');

var Factory = {};

Factory.isSaveMode = function (brandId) {
    return !_.isNaN(brandId) && brandId >= 0;
};


Factory.getBrand = function (data) {
    var Brand = {};
    Brand.BrandId = _.parseInt(data.BrandId);
    Brand.BrandCode = _.trim(data.BrandCode);
    Brand.BrandSubdomain = _.trim(data.BrandSubdomain);
    Brand.IsListedBrand = data.IsListedBrand ? 1 : 0;
    Brand.IsFeaturedBrand = data.IsFeaturedBrand ? 1 : 0;
    Brand.IsRecommendedBrand = data.IsRecommendedBrand ? 1 : 0;
    Brand.IsSearchableBrand = data.IsSearchableBrand ? 1 : 0;
    Brand.HeaderLogoImage = _.trim(data.HeaderLogoImage);
    Brand.SmallLogoImage = _.trim(data.SmallLogoImage);
    Brand.LargeLogoImage = _.trim(data.LargeLogoImage);
    Brand.ProfileBannerImage = _.trim(data.ProfileBannerImage);
    if (!this.isSaveMode(Brand.BrandId)) {
        Brand.StatusId = data.isDraft ? CONSTANTS.STATUS.PENDING : CONSTANTS.STATUS.ACTIVE;
    }

    //may not trim

    Brand.DisplayName = _.reduce(config.availableCultureCode, function (result, lang) {
        result[lang] = _.trim(data.DisplayName[lang]);
        if (lang === 'EN') {
            Brand.BrandNameInvariant = result[lang];
        }
        return result;
    }, {});

    if (data.BrandDesc) {
        Brand.BrandDesc = _.reduce(config.availableCultureCode, function (result, lang) {
            result[lang] = _.trim(data.BrandDesc[lang]);
            if (lang === 'EN') {
                Brand.BrandDescInvariant = result[lang];
            }
            return result;
        }, {});
    } else {
        Brand.BrandDesc = [];
    }

    return Brand;
};

Factory.validate = function (Brand) {
    //display name
    _.each(config.availableCultureCode, function (lang) {
        if (cu.isBlank(Brand.DisplayName[lang])) {
            throw {AppCode: "MSG_ERR_REQUIRED_FIELD_MISSING"};
        }
    });
    _.each(config.availableCultureCode, function (lang) {
        if (Brand.DisplayName[lang].length > 25) {
            throw {AppCode: "MSG_ERR_BRAND_DISPLAY_NAME"};
        }
    });

    //subdomain
    if (cu.isBlank(Brand.BrandSubdomain)) {
        throw {AppCode: "MSG_ERR_REQUIRED_FIELD_MISSING"};
    }
    if (Brand.BrandSubdomain.length > 25) {
        throw {AppCode: "MSG_ERR_SUBDOMAIN"};
    }

    //brandcode
    if (cu.isBlank(Brand.BrandCode)) {
        throw {AppCode: "MSG_ERR_REQUIRED_FIELD_MISSING"};
    }
    if (Brand.BrandCode.length > 25) {
        throw {AppCode: "MSG_ERR_BRAND_CODE_FORMAT"};
    }

};

Factory.save = function (Brand, BrandImageList, IsTouchedBrandImages, tr) {
    var deferred = Q.defer();
    var isSaveMode = Factory.isSaveMode(Brand.BrandId);

    Q.when()
        .then(function () {
            return tr.queryOne(
                'SELECT `BrandCode` FROM `Brand` WHERE `BrandCode` = :BrandCode ' + (isSaveMode ? 'AND `BrandId` <> :BrandId' : ''),
                {BrandCode: Brand.BrandCode, BrandId: Brand.BrandId}
            ).then(function (data) {
                    if (data) throw {AppCode: "MSG_ERR_BRAND_CODE_UNIQUE"};
                });
        }).then(function () {
            return tr.queryOne(
                'SELECT `BrandSubdomain` FROM `Brand` WHERE `BrandSubdomain` = :BrandSubdomain ' + (isSaveMode ? 'AND `BrandId` <> :BrandId' : ''),
                {BrandSubdomain: Brand.BrandSubdomain, BrandId: Brand.BrandId}
            ).then(function (data) {
                    if (data) throw {AppCode: "MSG_ERR_SUBDOMAIN_UNIQUE"};
                });
        }).then(function () {
            return Q.allSettled(_.reduce(config.availableCultureCode, function (result, lang) {
                result.push(tr.queryOne(
                    'SELECT `BrandId`, `CultureCode` FROM `BrandCulture` WHERE `CultureCode` = :CultureCode AND BrandName = :BrandName ' + (isSaveMode ? 'AND BrandId <> :BrandId ' : ''),
                    {CultureCode: lang, BrandName: Brand.DisplayName[lang], BrandId: Brand.BrandId}
                ));
                return result;
            }, []))
                .then(function (results) {
                    results.forEach(function (result) {
                        if (result.state !== 'fulfilled' || (result.value && !cu.isBlank(result.value.BrandId))) {
                            //throw new Error('Display Name "' + Brand.DisplayName[result.value.CultureCode] + '" for language "' + result.value.CultureCode + '" already exists');
                            throw {AppCode: "MSG_ERR_BRAND_DISPLAY_NAME_UNIQUE"};
                        }
                    });
                });
        }).then(function () {
            var sql = isSaveMode ?
            'UPDATE `Brand` SET `BrandCode` = :BrandCode, BrandSubdomain = :BrandSubdomain, IsListedBrand = :IsListedBrand, ' +
            'IsFeaturedBrand = :IsFeaturedBrand, IsRecommendedBrand = :IsRecommendedBrand, IsSearchableBrand = :IsSearchableBrand, ' +
            'HeaderLogoImage = :HeaderLogoImage, SmallLogoImage = :SmallLogoImage, LargeLogoImage = :LargeLogoImage, ProfileBannerImage = :ProfileBannerImage, ' +
            'BrandNameInvariant = :BrandNameInvariant, BrandDescInvariant = :BrandDescInvariant WHERE `BrandId` = :BrandId '
                :
            'INSERT INTO `Brand` ' +
            '(BrandCode, BrandNameInvariant, BrandDescInvariant, BrandSubdomain, IsListedBrand, IsFeaturedBrand, IsRecommendedBrand, IsSearchableBrand, HeaderLogoImage, SmallLogoImage, LargeLogoImage, ProfileBannerImage, StatusId) VALUES ' +
            '(:BrandCode, :BrandNameInvariant, :BrandDescInvariant, :BrandSubdomain, :IsListedBrand, :IsFeaturedBrand, :IsRecommendedBrand, :IsSearchableBrand, :HeaderLogoImage, :SmallLogoImage, :LargeLogoImage, :ProfileBannerImage, :StatusId) ';
            return tr.queryExecute(sql, Brand).then(function (data) {
                if (!isSaveMode) {
                    Brand.BrandId = data.insertId;
                }
            });

        })
        .then(function () {
            /**
             * the folowing commented is okay to go when unique key constraint is set
             */
            /**
             return Q.all(config.availableCultureCode.map(function (lang) {
                return tr.queryExecute('INSERT INTO `BrandCulture` ' +
                    '(`BrandId`, `CultureCode`, `BrandName`) VALUES ' +
                    '(:BrandId, :CultureCode, :BrandName) ' +
                    'ON DUPLICATE KEY UPDATE `BrandName` = VALUES(`BrandName`) ', {
                    BrandId: Brand.BrandId,
                    CultureCode: lang,
                    BrandName: Brand.DisplayName[lang]
                });
            }));
             **/
            return Q.all(config.availableCultureCode.map(function (lang) {
                return tr.queryExecute(
                    (isSaveMode ? 'UPDATE `BrandCulture` SET `BrandName` = :BrandName, `BrandDesc` = :BrandDesc WHERE `BrandId` = :BrandId AND `CultureCode` = :CultureCode '
                        :
                        'INSERT INTO `BrandCulture` (`BrandId`, `CultureCode`, `BrandName`, `BrandDesc`) VALUES (:BrandId, :CultureCode, :BrandName, :BrandDesc) '), {
                        BrandId: Brand.BrandId,
                        CultureCode: lang,
                        BrandName: Brand.DisplayName[lang],
                        BrandDesc: Brand.BrandDesc[lang]
                    });
            }));
        })
        .then(function () {
            //insert or update the brand images
            var promises = [];
            if (!isSaveMode) {
                console.log("insert Brand images");
            } else {
                if (!IsTouchedBrandImages) { // if Brand image did not do any changes, just ignore this part.
                    return;
                }
                console.log("update Brand images");
                //delete Brand images of one Brand
                var deletePromise = tr.queryExecute("Delete From BrandImage Where BrandId=:BrandId", {
                    BrandId: Brand.BrandId,
                });
                promises.push(deletePromise);
            }
            var insertPromises = _.map(BrandImageList, function (BrandImage) {
                return tr.queryExecute("insert into BrandImage (BrandId, ImageTypeCode, BrandImage, Position) values(:BrandId, :ImageTypeCode, :BrandImage, :Position)", {
                    BrandId: Brand.BrandId,
                    ImageTypeCode: BrandImage.ImageTypeCode,
                    BrandImage: BrandImage.BrandImage,
                    Position: BrandImage.Position
                })
            });
            promises = promises.concat(insertPromises);
            return Q.all(promises);

        })
        .then(function () {
            deferred.resolve(Brand);
        })
        .catch(function (err) {
            deferred.reject(err);
        });

    return deferred.promise;
};

Factory.changeStatus = function (Bale, tr) {
    var deferred = Q.defer();
    var Brand = null;
    var StatusCur = Bale.StatusId;
    var StatusPrev = null;

    Q.when()
        .then(function () {
            return tr.queryOne("select `BrandId`, `StatusId` from Brand where BrandId=:BrandId", Bale).then(function (data) {
                Brand = data;
                if (Brand === null)
                    throw {
                        AppCode: "MSG_ERR_BRAND_NOT_FOUND"
                    };
                if (Brand.StatusId == CONSTANTS.STATUS.DELETED) //don't perfom if deleted
                    throw {
                        AppCode: "MSG_ERR_STATUS_INVALID"
                    };

                StatusPrev = Brand.StatusId;
            });
        })
        .then(function () {
            if (Bale.StatusId === CONSTANTS.STATUS.INACTIVE) {
                //check if any product under this brand (exclude deleted)
                return tr.queryOne("select `SkuId` from Sku where BrandId=:BrandId and StatusId<>:StatusId", {
                    BrandId: Bale.BrandId,
                    StatusId: CONSTANTS.STATUS.DELETED
                }).then(function (data) {
                    if (data !== null) {
                        throw {
                            AppCode: "MSG_ERR_PRODUCT_UNDER_BRAND_FOUND"
                        }
                    }
                });
            }
        })
        .then(function () {
            return tr.queryExecute("update Brand set StatusId=:StatusId where BrandId=:BrandId;", Bale);
        })
        .then(function (result) {
            if (result.affectedRows && _.parseInt(result.affectedRows) > 0) {
                return tr.queryMany('SELECT `StatusId`, `StatusName`, `CultureCode` FROM `StatusCulture` ' +
                    'WHERE StatusId = :StatusPrev OR StatusId = :StatusCur ', {
                    StatusPrev: StatusPrev,
                    StatusCur: StatusCur
                }).then(function (StatusName) {
                    return tr.queryMany('SELECT bc.`CultureCode`, bc.`BrandName` FROM `Brand` b INNER JOIN `BrandCulture` bc ON b.`BrandId` = bc.`BrandId` ' +
                        'WHERE b.`BrandId` = :BrandId ', {BrandId: Brand.BrandId}).then(function (BrandName) {
                        var s = {};
                        s[StatusCur] = {};
                        s[StatusPrev] = {};
                        var StatusCulture = _.reduce(StatusName, function (result, name) {
                            result[name.StatusId][name.CultureCode] = name.StatusName;
                            return result;
                        }, s);
                        var BrandCulture = _.reduce(BrandName, function (result, name) {
                            result[name.CultureCode] = name.BrandName;
                            return result;
                        }, {});

                        return tr.queryMany('SELECT u.`Email`, u.`FirstName`, u.`DisplayName`, u.`UserTypeId`, l.`LanguageNameInvariant` ' +
                            'FROM `User` u ' +
                            'INNER JOIN `Language` l ON l.`LanguageId` = u.`LanguageId` ' +
                            'WHERE u.`UserTypeId` = :UserTypeId AND EXISTS ' +
                            '(SELECT m.`MerchantId` FROM `Merchant` m ' +
                            'INNER JOIN `MerchantBrand` mb ON mb.`MerchantId` = m.`MerchantId` ' +
                            'INNER JOIN `Brand` b ON b.`BrandId` = mb.`BrandId` ' +
                            'WHERE b.`BrandId` = :BrandId AND m.`MerchantId` = u.`MerchantId`) ',
                            {UserTypeId: CONSTANTS.USER_TYPE.MERCHANT, BrandId: Brand.BrandId}).then(function (users) {
                                return Q.allSettled(_.reduce(users, function (result, user) {
                                    var cc = user.LanguageNameInvariant;
                                    return result.concat(lum.sendUserMessage({
                                        DisplayName: _.trim(user.DisplayName) ? _.trim(user.DisplayName) : user.FirstName,
                                        BrandName: BrandCulture[cc],
                                        StatusPrev: StatusCulture[StatusPrev][cc],
                                        StatusCur: StatusCulture[StatusCur][cc],
                                        //no mobile sms sending
                                        UserSecurityGroupArray: [-1],
                                        CultureCode: cc,
                                        UserTypeId: user.UserTypeId,
                                        Email: user.Email
                                    }, lum.messageTemplateCodes.BRAND_STATUS_CHANGED, tr));
                                }, []));
                            });
                    });
                });
            } else {
                throw new Error('Brand status update failed');
            }
        })
        .then(function (result) {
            deferred.resolve(result);
        })
        .catch(function (err) {
            deferred.reject(err);
        });

    return deferred.promise;
};

var ONE_SQL = 'SELECT b.`BrandId`, b.`BrandNameInvariant`, b.`BrandCode`, b.`BrandSubdomain`,' +
    'b.`IsListedBrand`, b.`IsFeaturedBrand`, b.`IsRecommendedBrand`, b.`IsSearchableBrand`, b.`HeaderLogoImage`,' +
    'b.`SmallLogoImage`, b.`LargeLogoImage`, b.`ProfileBannerImage`, b.`BrandDescInvariant`, b.`StatusId`,' +
    's.`StatusNameInvariant`, bc.`CultureCode`, bc.`BrandName`, bc.`BrandDesc` ' +
    'FROM `Brand` b ' +
    'INNER JOIN `BrandCulture` bc ON b.`BrandId` = bc.`BrandId` ' +
    'INNER JOIN `Status` s ON s.`StatusId` = b.`StatusId` WHERE b.`BrandId` = :BrandId'; //AND b.`StatusId` = 2

Factory.get = function (brandId, tr) {
    var deferred = Q.defer();
    var brand = {};
    var brandImageList = [];

    Q.when()
        .then(function () {
            return tr.queryMany(ONE_SQL, {BrandId: brandId});
        })
        .then(function (data) {
            brand = _.reduce(data, function (brand, row) {
                brand.BrandId = row.BrandId;
                brand.BrandNameInvariant = row.BrandNameInvariant;
                brand.BrandCode = row.BrandCode;
                brand.BrandSubdomain = row.BrandSubdomain;
                brand.IsListedBrand = row.IsListedBrand ? true : false;
                brand.IsFeaturedBrand = row.IsFeaturedBrand ? true : false;
                brand.IsRecommendedBrand = row.IsRecommendedBrand ? true : false;
                brand.IsSearchableBrand = row.IsSearchableBrand ? true : false;
                brand.HeaderLogoImage = row.HeaderLogoImage;
                brand.SmallLogoImage = row.SmallLogoImage;
                brand.LargeLogoImage = row.LargeLogoImage;
                brand.ProfileBannerImage = row.ProfileBannerImage;
                brand.BrandDescInvariant = row.BrandDescInvariant;
                brand.StatusId = row.StatusId;
                brand.StatusNameInvariant = row.StatusNameInvariant;
                if (row.CultureCode) {
                    brand.DisplayName[row.CultureCode] = row.BrandName;
                    brand.BrandDesc[row.CultureCode] = row.BrandDesc;
                }
                return brand;
            }, {DisplayName: {}, BrandDesc: {}});
        })
        .then(function () {
            return tr.queryMany("SELECT * FROM BrandImage WHERE BrandId=:BrandId", {BrandId: brandId}).then(function (data) {
                brandImageList = data;
            });
        })
        .then(function () {
            deferred.resolve({
                Brand: brand,
                BrandImageList: brandImageList
            });
        })
        .catch(function (err) {
            deferred.reject(err);
        });

    return deferred.promise;
};

var LIST_SQL = [
    "select Brand.*, BrandName, ifnull(SCount, 0) as StyleCount, ifnull(MCount, 0) as MerchantCount from Brand",
    "left join BrandCulture as BC on Brand.BrandId = BC.BrandId",
    "left join (select distinct count(*) as SCount, BrandId from Sku group by BrandId) Sku on Brand.BrandId = Sku.BrandId",
    "left join (SELECT BrandId, count(*) as MCount FROM MerchantBrand group by BrandId) MB on Brand.BrandId = MB.BrandId",
    "where CultureCode = :CC and Brand.BrandId<>0"
].join('\n');

var LIST_BY_MERCHANT_SQL = [
    'select Brand.*, BrandName, ifnull(SCount, 0) as StyleCount, 0 as MerchantCount from Brand ',
    'inner join MerchantBrand MB on MB.BrandId=Brand.BrandId',
    'inner join BrandCulture as BC on (Brand.BrandId = BC.BrandId and CultureCode = :CC)',
    'left join (select distinct count(*) as SCount, BrandId from Sku group by BrandId) Sku on Brand.BrandId = Sku.BrandId',
    'where Brand.BrandId<>0  and MB.MerchantId=:MerchantId'
].join('\n');

Factory.list = function (CC, merchantId, tr) {
    var deferred = Q.defer();

    Q.when().then(function () {
        if (cu.isBlank(CC)) {
            throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
        }
    })
        .then(function () {
            if(cu.isBlank(merchantId)) {
                return tr.queryMany(LIST_SQL, {CC: CC});
            }else{
                return tr.queryMany(LIST_BY_MERCHANT_SQL, {CC: CC, MerchantId: merchantId});
            }
        })
        .then(function (result) {
            deferred.resolve(result);
        })
        .catch(function (err) {
            deferred.reject(err);
        });

    return deferred.promise;
};

var LIST_COMBINED_ALL_SQL = [
    "SELECT b.BrandId AS EntityId, b.BrandNameInvariant AS NameInvariant, 'Brand' AS Entity,",
    "bc.BrandName AS Name, bc.BrandDesc AS Description, IsSearchableBrand AS IsSearchable,",
    "IsListedBrand AS IsListed, IsFeaturedBrand AS IsFeatured, IsRecommendedBrand AS IsRecommended,",
    "StatusId,",
    "HeaderLogoImage, SmallLogoImage, LargeLogoImage, ProfileBannerImage",
    "FROM Brand b",
    "INNER JOIN BrandCulture bc on b.BrandId = bc.BrandId",
    "WHERE ",
    "bc.CultureCode = :CC",
    "UNION",
    "SELECT m.MerchantId AS EntityId, m.MerchantNameInvariant AS NameInvariant, 'Merchant' AS Entity,",
    "mc.MerchantName AS Name, mc.MerchantDesc AS Description, IsSearchableMerchant AS IsSearchable,",
    "IsListedMerchant AS IsListed, IsFeaturedMerchant AS IsFeatured, IsRecommendedMerchant AS IsRecommended,",
    "StatusId,",
    "HeaderLogoImage, SmallLogoImage, LargeLogoImage, ProfileBannerImage",
    "FROM Merchant m",
    "INNER JOIN MerchantCulture mc on m.MerchantId = mc.MerchantId",
    "WHERE ",
    "mc.CultureCode = :CC",
    "ORDER BY Name"
].join('\n');

/**
 * @desc return list of brand and merchants
 * @param CC cultrue Code
 * @param tr transaction object
 **/
Factory.listCombined = function (CC, tr) {
    var deferred = Q.defer();

    Q.when().then(function () {
        if (cu.isBlank(CC)) {
            throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
        }
    })
        .then(function () {
            return tr.queryMany(LIST_COMBINED_ALL_SQL, {CC: CC});
        })
        .then(function (result) {
            deferred.resolve(result);
        })
        .catch(function (err) {
            deferred.reject(err);
        });

    return deferred.promise;
};

module.exports = Factory;
