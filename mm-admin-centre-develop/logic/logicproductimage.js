"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var config = require('../config');
var _ = require('lodash');
var cu = require('../lib/commonutil.js');

var FILENAME_REGEX = /^([A-Z0-9]+)_?([A-Z0-9-_]+)?_([0-9]+)(.\w+)$/i;

exports.add = function (MerchantId, filename, ImageKey, isIgnore, tr) {
    var deferred = Q.defer();
    var StyleImage, result;

    Q.when().then(function () {
        var matches = filename.match(FILENAME_REGEX);
        if (!matches) throw new Error('Invalid file name format.');

        var StyleCode = matches[1];
        var ColorKey = matches[2];
        var ImageTypeCode = 'Color';
        var ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.COLOR;
        if (!ColorKey) {
            ImageTypeCode = 'Feature';
            ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.FEATURE;
        } else if (ColorKey.toUpperCase() === 'DESC') {
            ImageTypeCode = 'Desc';
            ColorKey = null;
            ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.DESC;
        }
        var Position = parseInt(matches[3], 10);

        StyleImage = {
            MerchantId: MerchantId,
            ProductImage: ImageKey,
            StyleCode: StyleCode,
            Position: Position,
            ColorKey: ColorKey,
            ImageTypeCode: ImageTypeCode,
            ImageTypeId: ImageTypeId
        };
        result = {
            StyleImage: StyleImage,
            inserted: false,
            updated: false
        };
    })
        .then(function () {
            if (StyleImage.ColorKey) {
                return tr.queryOne(
                    'select * from Sku where MerchantId=:MerchantId and StyleCode=:StyleCode and ColorKey=:ColorKey limit 1',
                    StyleImage
                );
            } else {
                return tr.queryOne(
                    'select * from Sku where MerchantId=:MerchantId and StyleCode=:StyleCode limit 1',
                    StyleImage
                );
            }
        })
        .then(function (data) {
            if (!data) throw {
                AppCode: 'MSG_ERR_PROD_STYLE_OR_COLOR_NOT_EXIST'
            };
            result.Sku = data;

            if (StyleImage.ColorKey) {
                StyleImage.ColorKey = data.ColorKey; // use ColorKey from Sku
                return tr.queryOne(
                    'select * from StyleImage where MerchantId=:MerchantId and StyleCode=:StyleCode and ColorKey=:ColorKey and ImageTypeCode=:ImageTypeCode and Position=:Position limit 1',
                    StyleImage
                );
            } else {
                return tr.queryOne(
                    'select * from StyleImage where MerchantId=:MerchantId and StyleCode=:StyleCode and ImageTypeCode=:ImageTypeCode and Position=:Position limit 1',
                    StyleImage
                );
            }
        })
        .then(function (data) {
            if (data) {
                StyleImage.StyleImageId = data.StyleImageId;
                // ignore existing: do nothing
                if (isIgnore) return;
                // replace existing: update
                return tr.queryExecute(
                    'update StyleImage set MerchantId=:MerchantId, ProductImage=:ProductImage where StyleImageId=:StyleImageId;',
                    StyleImage
                ).then(function () {
                        result.updated = true;
                    });
            } else {
                // no existing: insert
                return tr.queryExecute(
                    'insert into StyleImage (MerchantId, StyleCode, ColorKey, ProductImage, Position, ImageTypeCode, ImageTypeId) values (:MerchantId, :StyleCode, :ColorKey, :ProductImage, :Position, :ImageTypeCode, :ImageTypeId)',
                    StyleImage
                ).then(function (data) {
                        StyleImage.StyleImageId = data.insertId;
                        result.inserted = true;
                    });
            }
        })
        .then(function () {
            // get product names
            return tr.queryMany(
                'SELECT * FROM SkuCulture where SkuId=:SkuId', result.Sku
            ).then(function (data) {
                    result.SkuCulture = {};
                    data.forEach(function (cult) {
                        result.SkuCulture[cult.CultureCode] = cult;
                    });
                });
        })
        .then(function () {
            deferred.resolve(result);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();

    return deferred.promise;
};

exports.remove = function (StyleImageId, MerchantId, tr) {
    var deferred = Q.defer();

    Q.when()
        .then(function () {
            return tr.queryExecute(
                'delete from StyleImage where StyleImageId=:StyleImageId and MerchantId=:MerchantId',
                {StyleImageId: StyleImageId, MerchantId: MerchantId}
            );
        })
        .then(function () {
            deferred.resolve(true);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();

    return deferred.promise;
};

exports.getStyleImagelist = function (StyleImageIdList, tr) {
    var deferred = Q.defer();
    var styleImageList;

    Q.when()
        .then(function () {
            return tr.queryMany(
                'SELECT * FROM StyleImage FORCE INDEX(PRIMARY) WHERE StyleImageId IN (:StyleImageIdList)', {
                    StyleImageIdList: StyleImageIdList
                }
            ).then(function (data) {
                    styleImageList = data;
                });
        })
        .then(function () {
            deferred.resolve(styleImageList);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();

    return deferred.promise;
};
exports.updatePosition = function (imagePositionList, styleImageList, tr) {
    var deferred = Q.defer();
    var transactionPromise = Q.when();
    // 1.{styleImageId: position}, this is to decrease the time complexity and improve performance.
    var imagePositionObject = {};
    _.forEach(imagePositionList, function (n) {
        imagePositionObject[n.StyleImageId] = n.Position;
    });
    // 2.check and update position to db
    _.forEach(styleImageList, function (n) {
        var styleImageId = n.StyleImageId;
        var newPosition = imagePositionObject[styleImageId];
        var oldPosition = n.Position;
        if (newPosition !== oldPosition) { //update the postion to db.
            transactionPromise
                .then(function () {
                    return tr.queryExecute(
                        'update StyleImage set Position = :Position where StyleImageId=:StyleImageId', {
                            StyleImageId: styleImageId,
                            Position: newPosition
                        }
                    );
                })
        }
    });
    //3.if success done and return promise object
    transactionPromise
        .then(function () {
            console.log("update success");
            deferred.resolve(true);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();

    return deferred.promise;
}


/**
 * @desc add image
 * @param StyleCode
 * @param ImageKey
 * @param ColorKey
 * @param Position
 * @return object{StyleImage:StyleImage, Sku: Sku, SkuCulture: SkuCulture}
 **/
exports.addIndividually = function (MerchantId, ImageKey, StyleCode, ImageTypeCode, ColorKey, Position, tr) {
    var StyleImage;
    var deferred = Q.defer();

    if (ImageTypeCode === 'Feature' || ImageTypeCode === 'Desc') {
        ColorKey = null;
    }

    var ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.COLOR;
    if (ImageTypeCode === 'Feature') {
        ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.FEATURE;
    } else if (ImageTypeCode === 'Desc') {
        ImageTypeId = CONSTANTS.PRODUCT_IMG_TYPE.DESC;
    }


    var StyleImage = {
        ProductImage: ImageKey,
        StyleCode: StyleCode,
        ImageTypeCode: ImageTypeCode,
        ImageTypeId: ImageTypeId,
        Position: Position,
        ColorKey: ColorKey
    };
    var result = {
        StyleImage: StyleImage
    };

    Q.when()
        .then(function () {
            if (ColorKey) {
                return tr.queryOne(
                    'select * from Sku where MerchantId=:MerchantId and StyleCode=:StyleCode and ColorKey=:ColorKey limit 1',
                    StyleImage
                );
            } else {
                return tr.queryOne(
                    'select * from Sku where MerchantId=:MerchantId and StyleCode=:StyleCode limit 1',
                    StyleImage
                );
            }
        })
        .then(function (data) {
            if (!data) throw new Error('StyleCode or ColorKey not exists.');
            StyleImage.MerchantId = MerchantId;
            result.Sku = data;
            if (ColorKey) {
                return tr.queryOne(
                    'select * from StyleImage where MerchantId=:MerchantId and StyleCode=:StyleCode and ColorKey=:ColorKey and Position=:Position limit 1',
                    StyleImage
                );
            } else {
                return tr.queryOne(
                    'select * from StyleImage where MerchantId=:MerchantId and StyleCode=:StyleCode and Position=:Position limit 1',
                    StyleImage
                );
            }
        })
        .then(function (data) {
            return tr.queryExecute(
                'insert into StyleImage (MerchantId, StyleCode, ImageTypeCode, ImageTypeId, ColorKey, ProductImage, Position) values (:MerchantId, :StyleCode, :ImageTypeCode, :ImageTypeId, :ColorKey, :ProductImage, :Position)',
                StyleImage
            ).then(function (data) {
                    StyleImage.StyleImageId = data.insertId;
                });
        })
        .then(function () {
            // get product names
            return tr.queryMany(
                'SELECT * FROM SkuCulture where SkuId=:SkuId', result.Sku
            ).then(function (data) {
                    result.SkuCulture = {};
                    data.forEach(function (cult) {
                        result.SkuCulture[cult.CultureCode] = cult;
                    });
                });
        })
        .then(function () {
            deferred.resolve(result);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();

    return deferred.promise;
};


exports.getMissingImgReportParams = function (req) {
    //validate file filters
    var fileFilter = req.query.filefilter;

    //single filter for files filter
    if (!_.isArray(fileFilter) && !_.isNaN(_.parseInt(fileFilter))) {
        fileFilter = [_.parseInt(fileFilter)];
    }

    //multiple file filters
    if (!_.isArray(fileFilter) || !_.all(fileFilter, function (type) {
            var t = _.parseInt(type);
            return !_.isNaN(t) && t >= 1 && t <= 4;
        })) {
        throw new Error('Invalid files filter');
    }

    fileFilter = _.uniq(_.reduce(fileFilter, function (result, type) {
        return result.concat(_.parseInt(type));
    }, []));

    if (!fileFilter.length) {
        throw new Error('Invalid files filter');
    }

    var statusFilter = _.parseInt(req.query.status);
    if (_.isNaN(statusFilter)) {
        throw new Error('Invalid status filter');
    }

    var cultureCode = _.trim(req.query.cc);
    if (cu.isBlank(cultureCode))
        throw new Error('Language Code is required');

    var merchantId = _.parseInt(req.query.merchantid);
    if (_.isNaN(merchantId)) {
        throw new Error('Valid merchant ID is required');
    }

    //Query params
    var params = {
        MerchantId: merchantId,
        CultureCode: cultureCode,
        StatusId: statusFilter
    };
    //always apply feature filter
    var applyFeatureFilter = true; //_.indexOf(fileFilter, 1) !== -1;
    var applyDescFilter = _.indexOf(fileFilter, CONSTANTS.PRODUCT_IMG_TYPE.DESC) !== -1;
    var applyColorFilter = _.indexOf(fileFilter, CONSTANTS.PRODUCT_IMG_TYPE.COLOR) !== -1;

    return {
        params: params,
        applyFeatureFilter: applyFeatureFilter,
        applyDescFilter: applyDescFilter,
        applyColorFilter: applyColorFilter,
        statusFilter: statusFilter
    };
};

/**
 * get the product missing image report
 * @param {object} params the parameters for the product image report
 * @param {boolean} applyFeatureFilter
 * @param {boolean} applyDescFilter
 * @param {boolean} applyColorFilter
 * @param {number} statusFilter
 * @param {object} tr
 */
//SELECT MAX(`SkuId`) AS `SkuId`, `ColorKey`, MAX(`ColorId`) AS `ColorId`, MAX(`StatusId`) AS `StatusId`, `StyleCode` FROM `Sku` WHERE MerchantId = 1 and StyleCode = 'MD00068' GROUP BY `StyleCode`, HEX(`ColorKey`)
exports.getMissingImgReport = function (params, applyFeatureFilter, applyDescFilter, applyColorFilter, statusFilter, tr) {
    var deferred = Q.defer();
    var result = [];
    var notEnglish = params.CultureCode.toUpperCase() !== 'EN';

    var sql = [
        'SELECT s.`SkuId`, s.`ColorKey`, s.`StyleCode`, ' + (applyColorFilter ? 's.`HasColor`, ssc.`SkuColor`, scr.`ColorName`,' : '') + ' s.`TotalFeature`, s.`TotalDesc` ' +
        'FROM ( ' +
        '   SELECT s.`SkuId`, s.`ColorKey`, ' + (applyColorFilter ? 'CASE WHEN c.`ColorKey` is null THEN 0 ELSE 1 END AS `HasColor`,' : '') + ' s.`ColorId`, s.`StatusId`, s.`StyleCode`, IFNULL(fd.`TotalFeature`, 0) AS `TotalFeature`, IFNULL(fd.`TotalDesc`, 0) AS `TotalDesc` ' +
        '   FROM (SELECT MAX(`SkuId`) AS `SkuId`, `ColorKey`, MAX(`ColorId`) AS `ColorId`, MAX(`StatusId`) AS `StatusId`, `StyleCode` FROM `Sku` WHERE MerchantId = :MerchantId AND (ColorKey is not null AND ColorKey <> \'\') GROUP BY `StyleCode`, HEX(`ColorKey`)) s ' +
        '   LEFT JOIN ( ' +
        '     SELECT `StyleCode`, ' +
        '     SUM(IF(`ImageTypeCode` = \'Feature\', 1, 0)) AS `TotalFeature`, ' +
        '     SUM(IF(`ImageTypeCode` = \'Desc\', 1, 0)) AS `TotalDesc` ' +
        '     FROM `StyleImage` WHERE MerchantId = :MerchantId AND `ImageTypeCode` = \'Feature\' OR `ImageTypeCode` = \'Desc\' GROUP BY `StyleCode` ' +
        '   ) fd ON fd.`StyleCode` = s.`StyleCode` ' +
        '   ' + ( applyColorFilter ? 'LEFT JOIN ( ' +
        '     SELECT `StyleCode`, `ColorKey` ' +
        '     FROM `StyleImage` WHERE MerchantId = :MerchantId AND `ImageTypeCode` = \'Color\' GROUP BY `StyleCode`, `ColorKey` ' +
        '   ) c ON c.`StyleCode` = s.`StyleCode` AND c.`ColorKey` = s.`ColorKey` ' : '' ) +
        '   WHERE (' +
        '   (fd.`TotalFeature` is null OR fd.`TotalFeature` = 0) ' +
        '   ' + (applyDescFilter ? 'OR (fd.`TotalDesc` is null OR fd.`TotalDesc` = 0)' : '') + ' ' +
        '   ' + (applyColorFilter ? 'OR (c.`StyleCode` is null)' : '') + ') ' +
        ') s ' +
        (applyColorFilter ?
        'LEFT JOIN (SELECT `SkuId`, `CultureCode`, ' + (notEnglish ? 'GROUP_CONCAT(`SkuColor` SEPARATOR \' \') AS `SkuColor`' : '`SkuColor`') + ' FROM `SkuCulture` WHERE `CultureCode` = :CultureCode ' + (notEnglish ? ' OR `CultureCode` = \'EN\' GROUP BY `SkuId`' : '') + ') ssc ON s.`SkuId` = ssc.`SkuId` ' +
        'LEFT JOIN (SELECT c.`ColorId`, cc.`ColorName` FROM `Color` c LEFT JOIN `ColorCulture` cc ON c.`ColorId` = cc.`ColorId` AND cc.CultureCode = :CultureCode) scr ON s.`ColorId` = scr.`ColorId` ' : '') +
        'INNER JOIN `Status` st ON s.`StatusId` = st.`StatusId` ' +
        'WHERE ' + (statusFilter === -1 ? '(st.`StatusId` = 2 OR st.`StatusId` = 3 OR st.`StatusId` = 4)' : 'st.`StatusId` = :StatusId') + ' ORDER BY s.`StyleCode`, s.`ColorKey`'
    ].join(' \n');

    Q.when()
        .then(function () {
            //and merchant id
            return tr.queryMany(sql, params).then(function (data) {
                var pid = null;
                var index = 1;
                //var numOfRows = data.length;

                result = _.reduce(data, function (result, row) {
                    var product = null;
                    if (pid === null || pid !== row.StyleCode) {
                        product = {
                            SkuId: row.SkuId,
                            StyleCode: row.StyleCode,
                            MissingColors: [],
                            TotalFeature: row.TotalFeature,
                            TotalDesc: row.TotalDesc
                        };
                        result.push(product);
                    } else {
                        product = result[result.length - 1];
                    }

                    if (applyColorFilter && !row.HasColor && !cu.isBlank(row.ColorName) && !cu.isBlank(row.SkuColor)) {
                        product.MissingColors.push({
                            SystemColorName: row.ColorName,
                            UserColorName: row.SkuColor,
                            ColorKey: row.ColorKey
                        });
                    }

                    pid = row.StyleCode;
                    index++;

                    return result;
                }, []);
            })
        })
        .then(function () {
            deferred.resolve(result);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();

    return deferred.promise;
};

/**
 * @param {number} skuId
 * @param {object} tr
 * @returns {*|promise}
 */
exports.getDefaultImage = function(skuId, tr){
    var deferred = Q.defer();

    Q.when()
        .then(function() {
            //try to find the skuid specify color
            return tr.queryOne("select si.ProductImage " +
                "from Sku s " +
                "inner join StyleImage si on s.StyleCode = si.StyleCode and s.ColorKey = si.ColorKey and si.ImageTypeId = 2 " +
                "where s.SkuId = :SkuId " +
                "order by si.Position asc limit 1", {SkuId: skuId});
        })
        .then(function (data) {
            if (data == null) {
                //if no default color image found, use feature or desc
                return tr.queryOne("select si.ProductImage " +
                    "from Sku s " +
                    "inner join StyleImage si on s.StyleCode = si.StyleCode and si.ImageTypeId in (1,3) " +
                    "inner join ImageType it on si.ImageTypeId = it.ImageTypeId " +
                    "where s.SkuId = :SkuId " +
                    "order by it.Priority asc, si.Position asc limit 1", {SkuId: skuId});
            } else {
                return data;
            }
        })
        .then(function(data) {
            //if no image was found it would return a null object which is not ideal, so now it returns and object with a null ProductImage field
            if (data == null)
            {
                data={};
                data.ProductImage=null;
            }

            deferred.resolve(data);
        })
        .catch(function(err){
            deferred.reject(err);
        });

    return deferred.promise;
};


exports.imageList = function(StyleCode, MerchantId, tr){
    var deferred = Q.defer();
    var ImageList = null;

    var imageSql="select StyleImageId, ProductImage, ImageTypeCode, ColorKey, Position from StyleImage where MerchantId = :MerchantId and StyleCode = :StyleCode order by ColorKey, Position";
    var iProd = {};

    Q.when()
    .then(function() {
        return tr.queryMany(imageSql, {StyleCode : StyleCode, MerchantId : MerchantId}).then(function(data) {
            ImageList=data;
        })
    })
    .then(function(){
        //start image
        iProd.FeaturedImageList = [];
        iProd.DescriptionImageList = [];
        iProd.ColorImageList = [];
        for (let row of ImageList){
            if (row.ImageTypeCode === 'Feature') {
              iProd.FeaturedImageList.push({
                StyleImageId: row.StyleImageId,
                ImageKey: row.ProductImage,
                Position: row.Position
              });
            } else if (row.ImageTypeCode === 'Desc') {
              iProd.DescriptionImageList.push({
                StyleImageId: row.StyleImageId,
                ImageKey: row.ProductImage,
                Position: row.Position
              });
            } else if (row.ImageTypeCode === 'Color'){
              iProd.ColorImageList.push({
                StyleImageId: row.StyleImageId,
                ColorKey: row.ColorKey,
                ImageKey: row.ProductImage,
                Position: row.Position
              });
            }
        };
        
        if (iProd.FeaturedImageList.length > 0) // Set the first feature image (which has the lowest position) as default. The "order by" in SQL is important.                  
            iProd.ImageDefault = iProd.FeaturedImageList[0].ImageKey;
        else if (iProd.ColorImageList.length > 0) // Use the first color image as default if there is no feature image.                 
            iProd.ImageDefault = iProd.ColorImageList[0].ImageKey;
        else if (iProd.DescriptionImageList.length > 0)  // Use the first description image as default if there is no feature or color image.
            iProd.ImageDefault = iProd.DescriptionImageList[0].ImageKey;
        //end image
    })
    .then(function() {
        deferred.resolve(iProd);
    })
    .catch(function(err){
        deferred.reject(err);
    });
    return deferred.promise;
};
