"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var _ = require('lodash');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var lu = require('../logic/logicuser.js');
var moment = require('moment');

var logicaddress = require('../logic/logicaddress.js');
var logicorder = require('../logic/logicorder.js');
var logicprouct = require('../logic/logicproduct.js');

exports.checkOrder = function (tr, Bale) {
  var User, Address;
  var SkuIds;
  var SkuMap = {};
  var OrderMerchantMap = {};
  var OrderNetTotal = 0;
  var OrderGrossTotal = 0;
  var IsCrossBorder = false;
  var SkuErrors = [];

  return Q.when()
  .then(function () {
    return tr.queryOne('select UserId, UserIdentificationKey from User where UserKey=:UserKey', Bale)
    .then(function (data) {
      if (!data) throw { AppCode: 'MSG_ERR_USER_NOT_FOUND' };
      User = data;
      Bale.UserId = User.UserId;
    });
  })
  .then(function () {
    // get user address
    return logicaddress.view(tr, User.UserId, Bale.UserAddressKey).then(function (data) {
      Address = _.pick(data, [
        'RecipientName', 'PhoneCode', 'PhoneNumber',
        'GeoCountryId', 'GeoProvinceId', 'GeoCityId',
        'Country', 'Province', 'City', 'District',
        'PostalCode', 'Address', 'CultureCode'
      ]);
    });
  })
  .then(function () {
    // get sku list from items
    SkuIds = Bale.Skus.map(function (data) {
      if (cu.checkIdBlank(data.SkuId)) throw { AppCode: 'MSG_ERR_SKUID_INVALID' };

      data.SkuId = data.SkuId.toString();
      data.Qty = parseInt(data.Qty);

      if (!(data.Qty && data.Qty > 0)) throw { AppCode: 'MSG_ERR_QTY_INVALID' };

      SkuMap[data.SkuId] = data;
      return data.SkuId;
    });

    return tr.queryMany(
      "SELECT s.*, COUNT(inv.SkuId) AS LocationCount, SUM(inv.IsPerpetual) > 0 AS IsPerpetual, " +
      "(SUM(inv.QtyAllocated) - SUM(inv.QtyExported) - SUM(inv.QtyOrdered) - SUM(s.QtySafetyThreshold) - SUM(il.QtySafetyThreshold)) as QtyAts " +
      "FROM Sku s " +
      "LEFT JOIN Inventory inv ON s.SkuId = inv.SkuId AND inv.StatusId = 2 " +
      "LEFT JOIN InventoryLocation il ON il.InventoryLocationId = inv.InventoryLocationId AND il.StatusId = 2 " +
      "WHERE s.SkuId in (:SkuIds) AND s.StatusId = 2 GROUP BY s.SkuId;",
      { SkuIds: SkuIds }
    )
    .then(function (List) {
      List.forEach(function (Sku) {
        SkuMap[Sku.SkuId.toString()].Sku = Sku;
      });

      Bale.Skus.forEach(function (data) {
        if (!data.Sku) {
          SkuErrors.push({
            AppCode: 'MSG_ERR_SKU_NOT_EXISTS',
            SkuId: data.SkuId
          });
        }
      });
    });
  })
  .then(function () {
    if (SkuErrors.length > 0) throw { AppCode: 'MSG_ERR_SKU_FAIL', Errors: SkuErrors }

    Bale.Skus.forEach(function (data) {
      var QtyAts = parseInt(data.Sku.QtyAts);
      if (!data.Sku.IsPerpetual && (!QtyAts || data.Qty > QtyAts)) {
        SkuErrors.push({
          AppCode: 'MSG_ERR_SKU_ATS',
          SkuId: data.SkuId,
          QtyAts: QtyAts || 0
        })
      }

      if (data.Sku.StatusId !== CONSTANTS.STATUS.ACTIVE) {
        SkuErrors.push({
          AppCode: 'MSG_ERR_SKU_NOT_ACTIVE',
          SkuId: data.SkuId
        });
      }

      // get Sku data
      // within available time and status active
      // if within sales time and SalePrice > 0, UnitPrice = SalePrice
      // otherwise UnitPrice = RetailPrice
      if (
        (data.Sku.AvailableFrom && moment(data.Sku.AvailableFrom).isAfter(moment())) || 
        (data.Sku.AvailableTo && moment(data.Sku.AvailableTo).isBefore(moment()))
      ) {
        SkuErrors.push({
          AppCode: 'MSG_ERR_SKU_NOT_AVAILABLE',
          SkuId: data.SkuId
        });
      }

      data.UnitPrice = data.Sku.PriceRetail;

      if (data.Sku.PriceSale && data.Sku.PriceSale > 0 && 
        (!data.Sku.SaleFrom || !moment(data.Sku.SaleFrom).isAfter(moment())) &&
        (!data.Sku.SaleTo || !moment(data.Sku.SaleTo).isBefore(moment()))
      ) {
        data.UnitPrice = data.Sku.PriceSale;
      }

      data.MerchantId = data.Sku.MerchantId.toString();
      data.TaxInvoiceName = null;

      // todo implement this. Hardcoded crossborder flag for Alan testing
      if (/^(1|16)$/.test(data.MerchantId)) IsCrossBorder = true; 

      // todo net and gross will be different?
      data.NetTotal = data.UnitPrice * data.Qty;
      data.GrossTotal = data.UnitPrice * data.Qty;

      // order total
      OrderNetTotal += data.NetTotal;
      OrderGrossTotal += data.GrossTotal;

      var OrderMerchant = OrderMerchantMap[data.MerchantId] = OrderMerchantMap[data.MerchantId] || {
        MerchantId: data.MerchantId,
        OrderMerchantStatusId: 3, // created
        NetTotal: 0,
        GrossTotal: 0,
        TaxInvoiceName: null,
        UserIdentificationKey: User.UserIdentificationKey,
        IsUserIdentificationExists: !cu.checkIdBlank(User.UserIdentificationKey),
        OrderMerchantItems: []
      };
      // Merge address to merchant Order
      _.extend(OrderMerchant, Address);
      // merchant order total
      OrderMerchant.NetTotal += data.NetTotal;
      OrderMerchant.GrossTotal += data.GrossTotal;

      OrderMerchant.OrderMerchantItems.push(
        _.extend({
          MerchantId: data.MerchantId,
          QtyOrdered: data.Qty,
          QtyAllocated: 0,
          QtyShipped: 0,
          NetTotal: data.NetTotal,
          GrossTotal: data.GrossTotal,
          UnitPrice: data.UnitPrice,
        }, _.pick(data.Sku, [
          'SkuId', 'SkuCode', 'StyleCode', 'Bar', 'BrandId', 'SizeId',
          'ColorId', 'ColorKey', 'PriceRetail', 'PriceSale',
          'WeightKg', 'HeightCm', 'WidthCm', 'LengthCm'
        ]))
      );
    });

    if (Bale.Invoices) {
      Bale.Invoices.forEach(function (data) {
        if (
          !data.MerchantId || 
          !OrderMerchantMap.hasOwnProperty(data.MerchantId.toString())
        ) throw { AppCode: 'MSG_ERR_INVOICE_INVALID' };
        
        if (data.TaxInvoiceName) {
          OrderMerchantMap[data.MerchantId.toString()].TaxInvoiceName = data.TaxInvoiceName;
        }
      });
    }
  })
  .then(function () {
    if (SkuErrors.length > 0) throw { AppCode: 'MSG_ERR_SKU_FAIL', Errors: SkuErrors }

    // Order daily limit
    return tr.queryOne(
      "SELECT SUM(GrossTotal) Total FROM `Order` WHERE Userid = :UserId AND LastModified >= DATE_SUB(NOW(),INTERVAL 1 DAY);", Bale
    ).then(function (data) {
      var Total = parseFloat(data.Total);
      // todo move daily limit to constant
      if (Total > 100000) throw { AppCode: 'MSG_ERR_ORDER_LIMIT_EXCEEDED' };
    })
  })
  .then(function () {
    if (SkuErrors.length > 0) throw { AppCode: 'MSG_ERR_SKU_FAIL', Errors: SkuErrors }

    return {
      UserId: User.UserId,
      OrderStatusId: 3, // initiated
      CultureCode: Bale.CultureCode,
      NetTotal: OrderNetTotal,
      GrossTotal: OrderGrossTotal,
      UserIdentificationKey: User.UserIdentificationKey,
      IsCrossBorder: IsCrossBorder,
      IsUserIdentificationExists: !cu.checkIdBlank(User.UserIdentificationKey),
      Comments: Bale.Comments,
      OrderMerchants: _.values(OrderMerchantMap)
    };
  })
};

exports.createOrder = function (tr, Bale) {
  var Order;
  return exports.checkOrder(tr, Bale)
  .then(function (data) {
    Order = data;
    // insert Order
    return tr.queryExecute(
      "INSERT INTO `Order` (OrderKey, CultureCode, UserId, OrderStatusId, NetTotal, GrossTotal, UserIdentificationKey, Comments) VALUES (uuid(), :CultureCode, :UserId, :OrderStatusId, :NetTotal, :GrossTotal, :UserIdentificationKey, :Comments)", 
      Order
    ).then(function (data) {
      Order.OrderId = data.insertId;
    });
  })
  .then(function () {
    var promises = Order.OrderMerchants.map(function (OrderMerchant) {
      // insert Order Merchants
      OrderMerchant.OrderId = Order.OrderId;

      return tr.queryExecute(
        "INSERT INTO OrderMerchant (OrderMerchantKey, OrderId, MerchantId, OrderMerchantStatusId, NetTotal, GrossTotal, TaxInvoiceName, UserIdentificationKey, RecipientName, PhoneCode, PhoneNumber, GeoCountryId, GeoProvinceId, GeoCityId, Country, Province, City, District, PostalCode, Address, CultureCode) VALUES (uuid(), :OrderId, :MerchantId, :OrderMerchantStatusId, :NetTotal, :GrossTotal, :TaxInvoiceName, :UserIdentificationKey, :RecipientName, :PhoneCode, :PhoneNumber, :GeoCountryId, :GeoProvinceId, :GeoCityId, :Country, :Province, :City, :District, :PostalCode, :Address, :CultureCode)", 
        OrderMerchant
      )
      .then(function (data) {
        var OrderMerchantId = data.insertId;
        var promises = OrderMerchant.OrderMerchantItems.map(function (OrderMerchantItem) {
          OrderMerchantItem.OrderId = Order.OrderId;
          OrderMerchantItem.OrderMerchantId = OrderMerchantId;

          // insert Order Merchant Items
          return tr.queryExecute(
            "INSERT INTO OrderMerchantItem (OrderMerchantId, OrderId, MerchantId, QtyOrdered, QtyAllocated, QtyShipped, SkuId, SkuCode, StyleCode, Barcode, BrandId, SizeId, ColorId, ColorKey, PriceRetail, PriceSale, WeightKg, HeightCm, WidthCm, LengthCm, UnitPrice, NetTotal, GrossTotal) VALUES (:OrderMerchantId, :OrderId, :MerchantId, :QtyOrdered, :QtyAllocated, :QtyShipped, :SkuId, :SkuCode, :StyleCode, :Bar, :BrandId, :SizeId, :ColorId, :ColorKey, :PriceRetail, :PriceSale, :WeightKg, :HeightCm, :WidthCm, :LengthCm, :UnitPrice, :NetTotal, :GrossTotal)",
            OrderMerchantItem
          );
        });
        return Q.all(promises);
      });
    })
  })
  .then(function () {
    return Order;
  })
};

exports.viewOrder = function (tr, Bale, where) {
  var Order;
  var OrderMerchantMap = {};
  var IsCrossBorder = false;

  where = where || "UserId = :UserId AND OrderKey = :OrderKey";
  return Q.when()
  .then(function () {
    return tr.queryOne("SELECT * FROM `Order` WHERE " + where, Bale)
    .then(function (data) {
      if (!data) throw { AppCode: "MSG_ERR_ORDER_NOT_EXISTS" };
      Order = data;
      Order.IsUserIdentificationExists = !cu.checkIdBlank(Order.UserIdentificationKey);
    })
  })
  .then(function () {
    return tr.queryMany(
      "SELECT * FROM `OrderMerchant` WHERE OrderId = :OrderId", Order
    ).then(function (Merchants) {
      Merchants.forEach(function (Merchant) {
        // todo implement this. Hardcoded crossborder flag for Alan testing
        if (/^(1|16)$/.test(Merchant.MerchantId)) IsCrossBorder = true; 

        Merchant.IsUserIdentificationExists = !cu.checkIdBlank(Merchant.UserIdentificationKey);
        OrderMerchantMap[Merchant.MerchantId] = Merchant;
        Merchant.OrderMerchantItems = [];
      });
      Order.IsCrossBorder = IsCrossBorder;
      Order.OrderMerchants = Merchants;
    });
  })
  .then(function () {
    return tr.queryMany(
      "SELECT * FROM `OrderMerchantItem` WHERE OrderId = :OrderId", Order
    ).then(function (Items) {
      Items.forEach(function (Item) {
        OrderMerchantMap[Item.MerchantId].OrderMerchantItems.push(Item);
      });
    });
  })
  .then(function () {
    return Order;
  });
}

exports.hideIds = function (Obj) {
  delete Obj.UserId;
  delete Obj.OrderId;
  delete Obj.OrderMerchantId;
  delete Obj.OrderMerchantItemId;
  delete Obj.UserIdentificationKey;

  if (Array.isArray(Obj.OrderMerchants)) {
    Obj.OrderMerchants.forEach(exports.hideIds);
  }
  if (Array.isArray(Obj.OrderMerchantItems)) {
    Obj.OrderMerchantItems.forEach(exports.hideIds);
  }
  return Obj;
}
