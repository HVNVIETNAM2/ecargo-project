"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var moment = require("moment");
var _ = require('lodash');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');

var Factory = {};

Factory.validateUserActivationTokenCode = function (User, ActivationToken, tr) {
    var deferred = Q.defer();
    var self = this;

    Q.when()
        .then(function () {
            if (User.ActivationToken !== ActivationToken)
                throw {AppCode: "MSG_ERR_USER_ACTIVATION_TOKEN_INVALID"};
            if (User.TokenAttempts >= config.tokenAttempts)
                throw {AppCode: "MSG_ERR_USER_ACTIVATION_TOKEN_DONE"};
            self.validateUserActivationToken(User);
        })
        .then(function () {
            return tr.queryExecute("update User set TokenAttempts=TokenAttempts + 1 where UserId=:UserId;", User);
        })
        .then(function () {
            deferred.resolve(true);
        })
        .catch(function (err) {
            deferred.reject(err);
        });

    return deferred.promise;
};

Factory.getUserByToken = function (token, tr) {
    var deferred = Q.defer();
    var self = this;

    Q.when()
        .then(function () {
            return tr.queryOne("select * from User where ActivationToken=:ActivationToken", {ActivationToken: token}).then(function (data) {
                if (data === null) {
                    throw {AppCode: "MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND"};
                } else {
                    self.validateUserActivationToken(data); //checks the token stored against the user for expiry etc
                    return data;
                }
            });
        })
        .then(function (data) {
            deferred.resolve(data);
        })
        .catch(function (err) {
            deferred.reject(err);
        });

    return deferred.promise;
};

Factory.validateUserActivationToken = function (User) {
    if (User.StatusId !== CONSTANTS.STATUS.PENDING)
        throw {AppCode: "MSG_ERR_USER_STATUS_INVALID"};
    var mLastStatus = moment(User.LastStatus);
    var mDiff = moment().diff(mLastStatus, 'h', true);
    var expiryHours = config.activationTimeoutOther;
    
    if (cu.isBlank(User.Hash) && User.StatusReasonCode !== 'RC_RESET_PASSWORD'){
        expiryHours = config.activationTimeoutRegistration; //if no password then this is aregistration link so use registration setting expiry
    }
//    if (cu.isBlank(expiryHours)){
//        expiryHours = 24; //defensive catch for blank config entries
//    }
    if (mDiff >= expiryHours){
        throw {AppCode: "MSG_ERR_USER_ACTIVATION_TOKEN_EXPIRED"};
    }
};

module.exports = Factory;