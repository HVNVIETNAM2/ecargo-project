"use strict";
var CONSTANTS = require('../logic/constants');

var Q=require('q');
var _ = require('lodash');
var throat = require('throat');
var moment = require('moment');
var config=require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');
var ce = require('../lib/commonemail.js');

var lcat = require('./logiccategory.js');
var li = require('./logicinventory.js');

var Excel = require('exceljs');
var xlsx = require('node-xlsx');
var path = require('path');


var SkuListQuery = function(){
    var fields = "s.SkuId, s.StyleCode, s.SkuCode, s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, s.GeoCountryId, "+
                    "s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, s.LastCreated, " +
                    "s.QtySafetyThreshold, s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, szc.SizeName, " +
                    "s.ColorKey, cl.ColorCode, cl.ColorImage, clc.ColorName, scu.SkuColor, scu.SkuName, s.ManufacturerName, s.IsFeatured, " +
                    "COUNT(inv.SkuId) AS LocationCount, " +
                    "IF(SUM(inv.IsPerpetual), :PerpetualAllocationNo, (SUM(inv.QtyAllocated) - SUM(inv.QtyExported) - SUM(inv.QtyOrdered)  - SUM(s.QtySafetyThreshold) - SUM(il.QtySafetyThreshold))) as QtyAts ";

    var sql = "SELECT {fields} FROM Sku s " +
                "INNER join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) " +
                "INNER join Size sz on (s.SizeId=sz.SizeId) " +
				"INNER join SizeCulture szc on (sz.SizeId=szc.SizeId and szc.CultureCode=:CultureCode) " +
                "INNER join Color cl on (s.ColorId=cl.ColorId) " +
                "INNER join ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) " +
                "LEFT JOIN Inventory inv ON s.SkuId = inv.SkuId AND inv.StatusId = 2 " +
                "LEFT JOIN InventoryLocation il ON il.InventoryLocationId = inv.InventoryLocationId AND il.StatusId = 2 " +
                "WHERE {where}";
        
    //var countClause = " COUNT(*) as Total ";
    
    this.getDataQuery = function(where, offset, limit){
        var dataQuery = sql.replace('{fields}',fields).replace('{where}',where)+ " GROUP BY s.SkuId ";
        if(typeof offset !== 'undefined' && typeof limit !== 'undefined'){
           dataQuery += ("LIMIT "+ offset +"," + limit);
        }
        return dataQuery + ";";
    }
    
    this.getCountQuery = function(where){
        return 'select count(*) as Total from (' + sql.replace('{fields}', 's.SkuId').replace('{where}',where) + " GROUP BY s.SkuId) a ";
    }
};

var skuListQueryBuilder = new SkuListQuery();

var Factory={};

Factory.compactFilename = function(str){
	str = str.replace(/ /g, "");
    str = str.replace(/([?<>:*|"\[\]\/\\])/g, "");
    str = str.replace(/<|>/g, "");
    return str;
};

Factory.isValidDate = function(date){
        return moment(date,CONSTANTS.DATE_FORMAT, true).isValid();
//	if( ! (new Date(date) !== "Invalid Date" && !isNaN(new Date(date)) ) ){
//		date = date.toString().replace(/-/g,"/");
//		if(! (new Date(date) !== "Invalid Date" && !isNaN(new Date(date)) ) ){
//			return false;
//		}else{
//			return true;
//		}
//	}else{
//		return true;
//	}
};

Factory.isBlank = function(string){
	if (string===null || string===undefined || string===''){
		return true;
	}else{
		if(string.toString().indexOf(':MISSING:') > -1 || string.toString().indexOf(':NOTVALID:') > -1 || string.toString().indexOf(':NOTFOUND:') > -1){
			return true;
		}else{
			return false;
		}
	}
}

//Factory.isDate = function(date) {
//	var d = Date.parse(date);
//	if(isNaN(d)){
//	 return false;
//	}else{
//	 return true;
//	}
//}

/**
 * @returns {*|promise}
 */
Factory.existsSku = function(skuId, merchantId, tr){
	var deferred = Q.defer();

	Q.when()
		.then(function() {
			return tr.queryOne("SELECT `SkuId` FROM `Sku` WHERE SkuId = :SkuId AND MerchantId = :MerchantId", {SkuId: skuId, MerchantId: merchantId});
		})
		.then(function(data) {
			if (data == null)
			{
				throw new Error("no sku id found for this merchant id "+merchantId);
			} else {
				deferred.resolve(true);
			}
		})
		.catch(function(err){
			deferred.reject(err);
		});

	return deferred.promise;
};

//do not change this method without talking to Albert.
Factory.styleList = function(MerchantId,CategoryId,StyleCode,StatusId,Search,CultureCode, page, size, MissingImgType, tr){
	var deferred = Q.defer();
	var CategoryListAdjacency=null;
	var ProductList = null;
	var ProductSize = 0;
	var ProductTotalSize = 0;
	var PageCount=0;
	
	if(page){
		page = parseInt(page);
	}
	if(isNaN(page)){
		page = 1;
	}

	//decrease page number for offset
	page -= 1;

	var limit = parseInt(size);
	var offset = (page * limit);

	var CategoryArray=null;
	//var fields="s.StyleCode, s.MerchantId, '00000000000000000000000000000000' as ImageDefault, min(s.PriceRetail) as PriceRetail, min(s.PriceSale) as PriceSale, min(s.SaleFrom) as SaleFrom, max(s.SaleTo) as SaleTo, max(s.LastCreated) as LastCreated, s.AvailableFrom, s.AvailableTo, ManufacturerName, LaunchYear, b.BrandId, bc.BrandName, b.BrandNameInvariant, b.SmallLogoImage as BrandImage, scu.SkuName, s.SkuNameInvariant, scu.SkuDesc, s.SkuDescInvariant, scu.SkuFeature, s.SkuFeatureInvariant, s.StatusId, stc.StatusName, st.StatusNameInvariant, s.SeasonId, seac.SeasonName, sea.SeasonNameInvariant, bad.BadgeId, badc.BadgeName, bad.BadgeNameInvariant, s.GeoCountryId, gcoc.GeoCountryName, gco.GeoCountryNameInvariant, s.SkuId as PrimarySkuId";  // max(LastCreated)>=DATE_SUB(NOW(), INTERVAL 1 MONTH) as IsNew, (now() BETWEEN min(SaleFrom) and max(SaleTo)) as IsSale"; //scat.CategoryId as PrimaryCategoryId,
	//var sql="select {fields} from Sku s inner join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) inner join Brand b on (s.BrandId=b.BrandId) inner join BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) inner join Status st on (s.StatusId=st.StatusId) inner join StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) inner join Season sea on (s.SeasonId=sea.SeasonId) inner join SeasonCulture seac on (sea.SeasonId=seac.SeasonId and seac.CultureCode=:CultureCode) inner join Badge bad on (s.BadgeId=bad.BadgeId) inner join BadgeCulture badc on (bad.BadgeId=badc.BadgeId and badc.CultureCode=:CultureCode) inner join GeoCountry gco on (s.GeoCountryId=gco.GeoCountryId) inner join GeoCountryCulture gcoc on (gco.GeoCountryId=gcoc.GeoCountryId and gcoc.CultureCode=:CultureCode) where s.StatusId <> 1 {where} group by StyleCode "; // inner join SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0)
	
	var fields="s.StyleCode, s.MerchantId, '00000000000000000000000000000000' as ImageDefault, s.WeightKg, s.HeightCm, s.WidthCm, s.LengthCm, style.PriceRetail, style.PriceSale, style.SaleFrom, style.SaleTo, style.LastCreated, s.AvailableFrom, s.AvailableTo, ManufacturerName, LaunchYear, b.BrandId, bc.BrandName, b.BrandNameInvariant, b.HeaderLogoImage as BrandHeaderLogoImage, b.SmallLogoImage as BrandSmallLogoImage, scu.SkuName, s.SkuNameInvariant, scu.SkuDesc, s.SkuDescInvariant, scu.SkuFeature, s.SkuFeatureInvariant, s.StatusId, stc.StatusName, st.StatusNameInvariant, s.SeasonId, seac.SeasonName, sea.SeasonNameInvariant, bad.BadgeId, badc.BadgeName, bad.BadgeNameInvariant, s.GeoCountryId, gcoc.GeoCountryName, gco.GeoCountryNameInvariant, s.SkuId as PrimarySkuId, s.IsFeatured";  // max(LastCreated)>=DATE_SUB(NOW(), INTERVAL 1 MONTH) as IsNew, (now() BETWEEN min(SaleFrom) and max(SaleTo)) as IsSale"; //scat.CategoryId as PrimaryCategoryId,
	var sql="select {fields} from (select StyleCode,MerchantId, SkuId,  min(PriceRetail) as PriceRetail, min(PriceSale) as PriceSale, min(SaleFrom) as SaleFrom, max(SaleTo) as SaleTo, max(LastCreated) as LastCreated from Sku where StatusID<>1 group by StyleCode, MerchantId) style inner join Sku s on (style.SkuId=s.SkuId and style.MerchantId=s.MerchantId) inner join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) inner join Brand b on (s.BrandId=b.BrandId) inner join BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) inner join Status st on (s.StatusId=st.StatusId) inner join StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) inner join Season sea on (s.SeasonId=sea.SeasonId) inner join SeasonCulture seac on (sea.SeasonId=seac.SeasonId and seac.CultureCode=:CultureCode) inner join Badge bad on (s.BadgeId=bad.BadgeId) inner join BadgeCulture badc on (bad.BadgeId=badc.BadgeId and badc.CultureCode=:CultureCode) inner join GeoCountry gco on (s.GeoCountryId=gco.GeoCountryId) inner join GeoCountryCulture gcoc on (gco.GeoCountryId=gcoc.GeoCountryId and gcoc.CultureCode=:CultureCode) where 1=1 {where}  "; // inner join SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0)
	
	//var fields="s.StyleCode, s.MerchantId, '00000000000000000000000000000000' as ImageDefault, s.AvailableFrom, s.AvailableTo, ManufacturerName, LaunchYear, b.BrandId, bc.BrandName, b.BrandNameInvariant, b.SmallLogoImage as BrandImage, scu.SkuName, s.SkuNameInvariant, scu.SkuDesc, s.SkuDescInvariant, scu.SkuFeature, s.SkuFeatureInvariant, s.StatusId, stc.StatusName, st.StatusNameInvariant, s.SeasonId, seac.SeasonName, sea.SeasonNameInvariant, bad.BadgeId, badc.BadgeName, bad.BadgeNameInvariant, s.GeoCountryId, gcoc.GeoCountryName, gco.GeoCountryNameInvariant, s.SkuId as PrimarySkuId";  // max(LastCreated)>=DATE_SUB(NOW(), INTERVAL 1 MONTH) as IsNew, (now() BETWEEN min(SaleFrom) and max(SaleTo)) as IsSale"; //scat.CategoryId as PrimaryCategoryId,	
	//var sql="select {fields} from (select StyleCode, SkuId from Sku where StatusID<>1 group by StyleCode) style inner join Sku s on (style.SkuId=s.SkuId) inner join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) inner join Brand b on (s.BrandId=b.BrandId) inner join BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) inner join Status st on (s.StatusId=st.StatusId) inner join StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) inner join Season sea on (s.SeasonId=sea.SeasonId) inner join SeasonCulture seac on (sea.SeasonId=seac.SeasonId and seac.CultureCode=:CultureCode) inner join Badge bad on (s.BadgeId=bad.BadgeId) inner join BadgeCulture badc on (bad.BadgeId=badc.BadgeId and badc.CultureCode=:CultureCode) inner join GeoCountry gco on (s.GeoCountryId=gco.GeoCountryId) inner join GeoCountryCulture gcoc on (gco.GeoCountryId=gcoc.GeoCountryId and gcoc.CultureCode=:CultureCode) where 1=1 {where}  "; // inner join SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0)
	//fields=" StyleCode, MerchantId, SkuId as PrimarySkuId ";
	//sql="select {fields} from Sku where StatusID<>1 group by StyleCode";
	var where='';
	var sq='';
        var params = {};

	Q.when()
	.then(function(){
		if (cu.isBlank(CultureCode))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return lcat.listAdjacency(CultureCode, false, tr).then(function(data){
			CategoryListAdjacency=data;
		});
	})
	.then(function(){
		if (!cu.checkIdBlank(MerchantId))
			where=where + " and s.MerchantId=:MerchantId ";
	})
	.then(function(){
		if (!cu.checkIdBlank(StatusId))
			where=where + " and s.StatusId=:StatusId ";
	})
	.then(function(){
		if (!cu.checkIdBlank(Search))
		{
			Search=Search + '%';
			where=where + " and (scu.SkuName like :Search or bc.BrandName like :Search or s.StyleCode like :Search) ";
		}
	})
	.then(function () {
		if (!cu.isBlank(MissingImgType))
		{
			var desc = '';
			var s = _.parseInt(MissingImgType);
			switch(s) {
				case CONSTANTS.PRODUCT_IMG_TYPE.FEATURE:
				case CONSTANTS.PRODUCT_IMG_TYPE.DESC:
					desc = s === 1 ? 'Feature' : 'Desc';
					where=where + " and exists (select StyleCode from " +
						"(select ss.StyleCode " +
						"	from Sku ss " +
						"	left join (select StyleCode, SUM(IF(ImageTypeCode = \'"+ desc +"\', 1, 0)) AS Total " +
						"			from StyleImage where MerchantId = :MerchantId and ImageTypeCode = \'"+ desc +"\' group by StyleCode) si on ss.StyleCode = si.StyleCode  " +
						"	where si.Total is null OR si.Total = 0) a where s.StyleCode = a.StyleCode) ";
					break;
				case CONSTANTS.PRODUCT_IMG_TYPE.COLOR:
					where=where + " and exists (select StyleCode from " +
						"(select ss.StyleCode " +
						"	from Sku ss " +
						"	left join (select `StyleCode`, `ColorKey` " +
						"			from `StyleImage` where MerchantId = :MerchantId and `ImageTypeCode` = \'Color\' group by `StyleCode`, `ColorKey`) si on ss.StyleCode = si.StyleCode and ss.ColorKey = si.ColorKey  " +
						"	where si.StyleCode is null) a where s.StyleCode = a.StyleCode) ";
					break;
			}
		}
	})
	.then(function(){
		if (!cu.isBlank(StyleCode))
			where=where + " and s.StyleCode=:StyleCode ";
	})
	.then(function(){
		if (!cu.checkIdBlank(CategoryId))
		{
			var tree=lcat.convertTree(CategoryListAdjacency);
			var subtree=lcat.findSubtree(tree,CategoryId);
			var catarray=lcat.subtreeCategoryArray(subtree);
			var cin=catarray.join();
			cin = cin.replace(/[;\']/g,'###'); //don't allow literal quote, or semi colons put triple hash in to show the error
			//where=where + " and scat.CategoryId in ({cin})";
			where=where + " and s.SkuId in (select SkuId from SkuCategory where CategoryId in ({cin}))";
			where=where.replace('{cin}',cin);
		}
	})
	.then(function() {
		sq=sql;
		sq=sq.replace('{fields}',fields);
		sq=sq.replace('{where}',where);
		sq=sq + " LIMIT "+ offset +"," + limit;
		return tr.queryMany(sq,{MerchantId:MerchantId,CultureCode:CultureCode,StatusId:StatusId,StyleCode:StyleCode,Search:Search}).then(function(data) {
			ProductList=data;
		})
	})
	.then(function(){
		sq=sql;
		sq=sq.replace('{fields}',' COUNT(*) as Total ');
		sq=sq.replace('{where}',where);
		//use this is the style query does end with a group by
		/*
		return tr.queryMany(sq, {MerchantId:MerchantId,CultureCode:CultureCode,StatusId:StatusId,StyleCode:StyleCode,Search:Search}).then(function(data){
			ProductTotalSize=data.length;
			var PageCountDec = (ProductTotalSize / size);
			PageCount = parseInt(PageCountDec);
			if (PageCountDec>PageCount)
				PageCount=PageCount + 1;
		});
		*/
		//use this is teh style query does NOT end with a group by
		return tr.queryOne(sq, {MerchantId:MerchantId,CultureCode:CultureCode,StatusId:StatusId,StyleCode:StyleCode,Search:Search}).then(function(data){
			ProductTotalSize=data.Total;
			var PageCountDec = (ProductTotalSize / size);
			PageCount = parseInt(PageCountDec);
			if (PageCountDec>PageCount)
				PageCount=PageCount + 1;
		});
	})
	.then(function() {
            return li.setParamStatus(params, tr);
        })
	.then(function() {
		var promises = ProductList.map(function(iProd){
			//var sizeSql="select distinct s.SizeId, sz.SizeNameInvariant as SizeName from Sku s inner join Size sz on s.SizeId=sz.SizeId where sz.SizeId<>0 and s.MerchantId = :MerchantId and s.StyleCode = :StyleCode order by sz.SizeId;";
			//var colorSql="select distinct s.ColorId,  colc.ColorName, s.ColorKey, col.ColorCode, sc.SkuColor from Sku s inner join Color col on (s.ColorId=col.ColorId) inner join ColorCulture colc on (col.ColorId=colc.ColorId and colc.CultureCode=:CultureCode) inner join SkuCulture sc on (s.SkuId=sc.SkuId and sc.CultureCode=:CultureCode) where col.ColorId<>0 and s.MerchantId = :MerchantId and s.StyleCode = :StyleCode order by s.ColorKey;";
			//var skuSql="select s.SkuId, s.StyleCode, s.SkuCode, s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, s.GeoCountryId, s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, s.QtySafetyThreshold, s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, sz.SizeNameInvariant as SizeName,s.ColorKey, clc.ColorName, scu.SkuColor, scu.SkuName";
			//skuSql=skuSql+" from Sku s inner join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) inner join Size sz on (s.SizeId=sz.SizeId) inner join Color cl on (s.ColorId=cl.ColorId) inner join ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode)";
			//skuSql=skuSql+" where s.StatusId <> :DeletedStatusId and s.StyleCode=:StyleCode;";
			var catgeorySql="select distinct c.CategoryId, sc.Priority, cc.CategoryName, c.CategoryNameInvariant from Category c inner join CategoryCulture cc on (c.CategoryId=cc.CategoryId and cc.CultureCode=:CultureCode) inner join SkuCategory sc on (sc.CategoryId=c.CategoryId) where sc.CategoryId<>0 and sc.SkuId=:SkuId;";
			var imageSql="select StyleImageId, ProductImage, ImageTypeCode, ColorKey, Position from StyleImage where MerchantId = :MerchantId and StyleCode = :StyleCode order by ColorKey, Position";			
//			var skuSql="select s.SkuId, s.StyleCode, s.SkuCode, s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, s.GeoCountryId, s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, s.LastCreated, s.QtySafetyThreshold, s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, sz.SizeNameInvariant as SizeName,s.ColorKey, cl.ColorCode, cl.ColorImage, clc.ColorName, scu.SkuColor, scu.SkuName," +
//				//"LastCreated>=DATE_SUB(NOW(), INTERVAL 1 MONTH) as IsNew, (now() BETWEEN SaleFrom and SaleTo) as IsSale," + 
//				"Summary.LocationCount,Summary.QtyAts,Summary.InventoryStatusId "+
//				"from Sku s "+
//                "INNER join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) "+
//                //"INNER join Brand b on (s.BrandId=b.BrandId) "+
//                //"INNER join BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) "+
//                "INNER join Size sz on (s.SizeId=sz.SizeId) " +
//				"INNER join Color cl on (s.ColorId=cl.ColorId) "+
//                "INNER join ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) "+
//                //"INNER join SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0) "+
//                //"INNER join Status st on (s.StatusId=st.StatusId) "+
//                //"INNER join StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) "+
//                "LEFT JOIN ("+
//                "SELECT result.SkuId, result.LocationCount, result.QtyAts, " +
//                "IF(result.HasInventory = 0, :NAId, " +
//                "IF(result.QtyAts = 'Unlimited', :InStockId, " +
//                "IF(result.QtyAts < result.SystemSafetyThreshold, :OutOfStockId, " +
//                "IF(result.QtyAts <= result.ProductLowStockThreshold, :LowStockId, :InStockId)))) AS InventoryStatusId "+
//                "FROM (SELECT inv.SkuId, " +
//                "IF(SUM(inv.InventoryId),TRUE,FALSE) AS HasInventory, " +
//                "SUM(:SystemSafetyThreshold) AS SystemSafetyThreshold, " +
//                "SUM(:ProductLowStockThreshold) AS ProductLowStockThreshold, " +
//                "COUNT(inv.SkuId) AS LocationCount, " +
//                "IF(SUM(inv.IsPerpetual), 'Unlimited', (SUM(inv.QtyAllocated) - SUM(inv.QtyExported) - SUM(inv.QtyOrdered)  - SUM(ss.QtySafetyThreshold) - SUM(il.QtySafetyThreshold))) as QtyAts " +
//                "FROM Sku ss " +
//                "INNER JOIN Inventory inv ON ss.SkuId = inv.SkuId " +
//                "INNER JOIN InventoryLocation il ON il.InventoryLocationId = inv.InventoryLocationId " +
//                "WHERE ss.StatusId <> 1 AND il.StatusId = 2 " +
//                "GROUP BY inv.SkuId ) AS result " +
//                ") AS Summary ON Summary.SkuId = s.SkuId " +
//                "where s.StatusId <> 1 and s.MerchantId = :MerchantId and s.StyleCode=:StyleCode;";
        
//                    var skuSql = "select s.SkuId, s.StyleCode, s.SkuCode, s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, s.GeoCountryId, "+
//                    "s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, s.LastCreated, " +
//                    "s.QtySafetyThreshold, s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, sz.SizeNameInvariant as SizeName, " +
//                    "s.ColorKey, cl.ColorCode, cl.ColorImage, clc.ColorName, scu.SkuColor, scu.SkuName, " +
//                    "COUNT(inv.SkuId) AS LocationCount, " +
//                    "IF(SUM(inv.IsPerpetual), :PerpetualAllocationNo, (SUM(inv.QtyAllocated) - SUM(inv.QtyExported) - SUM(inv.QtyOrdered)  - SUM(s.QtySafetyThreshold) - SUM(il.QtySafetyThreshold))) as QtyAts " +
//                    "FROM Sku s " +
//                    "INNER join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) " +
//                    "INNER join Size sz on (s.SizeId=sz.SizeId) " +
//                    "INNER join Color cl on (s.ColorId=cl.ColorId) " +
//                    "INNER join ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) " +
//                    "LEFT JOIN Inventory inv ON s.SkuId = inv.SkuId AND inv.StatusId = 2 " +
//                    "LEFT JOIN InventoryLocation il ON il.InventoryLocationId = inv.InventoryLocationId AND il.StatusId = 2 " +
//                    "WHERE " +
//                    "s.MerchantId = :MerchantId AND " +
//                    "s.StyleCode=:StyleCode AND " +
//                    "s.StatusId <> 1  " +
//                    "GROUP BY s.SkuId;";
            
                    var skuSql = skuListQueryBuilder.getDataQuery("s.MerchantId = :MerchantId AND s.StyleCode=:StyleCode AND s.StatusId <> 1 ");
        
			var systemSafetyThreshold = config.inventory.systemSafetyThreshold;
			var productLowStockThreshold = config.inventory.productLowStockThreshold;
                        var perpetualAllocationNo = config.inventory.perpetualAllocationNo;
			return tr.queryMany(skuSql + catgeorySql + imageSql,
                                {StyleCode:iProd.StyleCode,SkuId:iProd.PrimarySkuId,MerchantId:iProd.MerchantId,CultureCode:CultureCode, PerpetualAllocationNo: perpetualAllocationNo}).then(function(data) {			
				/*
				iProd.LastCreated=null;
				iProd.PriceRetail=null;
				iProd.PriceSale=null;
				iProd.SaleFrom=null;
				iProd.SaleTo=null;
				*/
				let rd=moment();
				iProd.IsNew=~~(moment(iProd.LastCreated)>=rd.add(-1, 'months'));
				iProd.IsSale=~~(moment(iProd.SaleFrom)<=rd && moment(iProd.SaleTo)>=rd);
				iProd.ColorList=[];
				iProd.SizeList=[];
				iProd.SkuList=data[0];
				iProd.TotalLocationCount = 0;
                                
				for (let iSku of iProd.SkuList)
				{
					//calculate the total location count of all skus within one style.
					if(!isNaN(iSku.LocationCount)){
						iProd.TotalLocationCount += iSku.LocationCount;
					}
                                        
                                        //Update inventory status
                                        iSku.InventoryStatusId = params.NAId;
                                        
                                        if (iSku.LocationCount){
                                            if (iSku.QtyAts === perpetualAllocationNo){
                                                iSku.InventoryStatusId = params.InStockId;
                                            } else if (iSku.QtyAts < systemSafetyThreshold * iSku.LocationCount){
                                                iSku.InventoryStatusId = params.OutOfStockId;
                                            } else if (iSku.QtyAts <= productLowStockThreshold * iSku.LocationCount){
                                                iSku.InventoryStatusId = params.LowStockId;
                                            } else{
                                                iSku.InventoryStatusId = params.InStockId;
                                            }
                                        }
                                        
                                        if(cu.isBlank(iSku.QtyAts)){
                                            iSku.QtyAts = 0;
                                        }
                                        else{
                                            iSku.QtyAts = parseInt(iSku.QtyAts);
                                        }
				/*
					if (iProd.LastCreated==null || iProd.LastCreated<iSku.LastCreated)
						iProd.LastCreated=iSku.LastCreated;
					if (iProd.PriceRetail==null || iProd.PriceRetail>iSku.PriceRetail)
						iProd.PriceRetail=iSku.PriceRetail;
					if (iProd.PriceSale==null || iProd.PriceSale>iSku.PriceSale)
						iProd.PriceSale=iSku.PriceSale;
					if (iProd.SaleFrom==null || iProd.SaleFrom>iSku.SaleFrom)
						iProd.SaleFrom=iSku.SaleFrom;
					if (iProd.SaleTo==null || iProd.SaleTo<iSku.SaleTo)
						iProd.SaleTo=iSku.SaleTo;
					*/
					iSku.IsNew=~~(moment(iSku.LastCreated)>=rd.add(-1, 'months'));
					iSku.IsSale=~~(moment(iSku.SaleFrom)<=rd && moment(iSku.SaleTo)>=rd);
					
					//derive the distinct size and color lists form the sku list
					let found=null;
					if (iSku.ColorId!=0)
					{
						let Color={ColorId:iSku.ColorId,ColorName:iSku.ColorName, ColorKey:iSku.ColorKey, ColorCode:iSku.ColorCode,ColorImage:iSku.ColorImage, SkuColor:iSku.SkuColor};
						found=false;
						for (let C of iProd.ColorList)
							if (cu.objectEqualByValues(Color,C))
								found=true;
						if (!found)
							iProd.ColorList.push(Color);
					}
					if (iSku.SizeId!=0)
					{
						let Size={SizeId:iSku.SizeId,SizeName:iSku.SizeName};
						found=false;
						for (let S of iProd.SizeList)
							if (cu.objectEqualByValues(Size,S))
								found=true;
						if (!found)
							iProd.SizeList.push(Size);
					}
				}
				
				//start category
				iProd.CategoryPriorityList=[];
				for (let iCat of data[1]){
					let cattle=lcat.categoryPath(CategoryListAdjacency,iCat.CategoryId);
					for (let iPror of cattle){
						if(iProd.CategoryPriorityList.indexOf(iPror)===-1) {
							iPror.Priority = iCat.Priority;
							iProd.CategoryPriorityList.push(iPror);
						}
					};
				};
				//end category 
				
				//start image
				iProd.FeaturedImageList = [];
				iProd.DescriptionImageList = [];
				iProd.ColorImageList = [];
				for (let row of data[2]){
					if (row.ImageTypeCode === 'Feature') {
					  iProd.FeaturedImageList.push({
						StyleImageId: row.StyleImageId,
						ImageKey: row.ProductImage,
						Position: row.Position
					  });
					} else if (row.ImageTypeCode === 'Desc') {
					  iProd.DescriptionImageList.push({
						StyleImageId: row.StyleImageId,
						ImageKey: row.ProductImage,
						Position: row.Position
					  });
					} else if (row.ImageTypeCode === 'Color'){
					  iProd.ColorImageList.push({
						StyleImageId: row.StyleImageId,
						ColorKey: row.ColorKey,
						ImageKey: row.ProductImage,
						Position: row.Position
					  });
					}
				};
				
				if (iProd.FeaturedImageList.length > 0) // Set the first feature image (which has the lowest position) as default. The "order by" in SQL is important.					
					iProd.ImageDefault = iProd.FeaturedImageList[0].ImageKey;
				else if (iProd.ColorImageList.length > 0) // Use the first color image as default if there is no feature image.					
					iProd.ImageDefault = iProd.ColorImageList[0].ImageKey;
				else if (iProd.DescriptionImageList.length > 0)  // Use the first description image as default if there is no feature or color image.
					iProd.ImageDefault = iProd.DescriptionImageList[0].ImageKey;
				//end image
			});
		});
		return Q.all(promises);
	})
	.then(function() {
		deferred.resolve({ HitsTotal : ProductTotalSize,PageTotal : parseInt(PageCount), PageSize : parseInt(size), PageCurrent : page + 1, PageData : ProductList  } );
	})
	.catch(function(err){
		deferred.reject(err);
	})
	.done();
	return deferred.promise;
};

/*
//do not change this method without talking to Albert.
Factory.styleListOld = function(MerchantId,CategoryId,StyleCode,StatusId,Search,CultureCode, page, size, MissingImgType, tr){
	var deferred = Q.defer();
	var CategoryListAdjacency=null;
	var ProductList = null;
	var ProductSize = 0;
	var ProductTotalSize = 0;
	var PageCount=0;

	if(page){
		page = parseInt(page);
	}
	if(isNaN(page)){
		page = 1;
	}

	//decrease page number for offset
	page -= 1;

	var limit = parseInt(size);
	var offset = (page * limit);

	var CategoryArray=null;
	var fields="s.StyleCode, s.MerchantId, '00000000000000000000000000000000' as ImageDefault, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, ManufacturerName, LaunchYear, b.BrandId, bc.BrandName, b.BrandNameInvariant, scu.SkuName, s.SkuNameInvariant, scu.SkuDesc, s.SkuDescInvariant, scu.SkuFeature, s.SkuFeatureInvariant, scat.CategoryId as PrimaryCategoryId, s.StatusId, stc.StatusName, st.StatusNameInvariant, s.SeasonId, seac.SeasonName, sea.SeasonNameInvariant, bad.BadgeId, badc.BadgeName, bad.BadgeNameInvariant, s.GeoCountryId, gcoc.GeoCountryName, gco.GeoCountryNameInvariant, s.SkuId as PrimarySkuId";
	var sql="select {fields} from Sku s inner join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) inner join Brand b on (s.BrandId=b.BrandId) inner join BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) inner join SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0) inner join Status st on (s.StatusId=st.StatusId) inner join StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) inner join Season sea on (s.SeasonId=sea.SeasonId) inner join SeasonCulture seac on (sea.SeasonId=seac.SeasonId and seac.CultureCode=:CultureCode) inner join Badge bad on (s.BadgeId=bad.BadgeId) inner join BadgeCulture badc on (bad.BadgeId=badc.BadgeId and badc.CultureCode=:CultureCode) inner join GeoCountry gco on (s.GeoCountryId=gco.GeoCountryId) inner join GeoCountryCulture gcoc on (gco.GeoCountryId=gcoc.GeoCountryId and gcoc.CultureCode=:CultureCode) where s.StatusId <> 1 {where} group by StyleCode ";
	var where='';
	var sq='';

	Q.when()
	.then(function(){
		if (cu.isBlank(CultureCode))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return lcat.listAdjacency(CultureCode, false, tr).then(function(data){
			CategoryListAdjacency=data;
		});
	})
	.then(function(){
		if (!cu.checkIdBlank(MerchantId))
			where=where + " and s.MerchantId=:MerchantId ";
	})
	.then(function(){
		if (!cu.checkIdBlank(StatusId))
			where=where + " and s.StatusId=:StatusId ";
	})
	.then(function(){
		if (!cu.checkIdBlank(Search))
		{
			Search=Search + '%';
			where=where + " and (scu.SkuName like :Search or bc.BrandName like :Search or s.StyleCode like :Search) ";
		}
	})
	.then(function () {
		if (!cu.isBlank(MissingImgType))
		{
			var desc = '';
			var s = _.parseInt(MissingImgType);
			switch(s) {
				case CONSTANTS.PRODUCT_IMG_TYPE.FEATURE:
				case CONSTANTS.PRODUCT_IMG_TYPE.DESC:
					desc = s === 1 ? 'Feature' : 'Desc';
					where=where + " and exists (select StyleCode from " +
						"(select ss.StyleCode " +
						"	from Sku ss " +
						"	left join (select StyleCode, SUM(IF(ImageTypeCode = \'"+ desc +"\', 1, 0)) AS Total " +
						"			from StyleImage where MerchantId = :MerchantId and ImageTypeCode = \'"+ desc +"\' group by StyleCode) si on ss.StyleCode = si.StyleCode  " +
						"	where si.Total is null OR si.Total = 0) a where s.StyleCode = a.StyleCode) ";
					break;
				case CONSTANTS.PRODUCT_IMG_TYPE.COLOR:
					where=where + " and exists (select StyleCode from " +
						"(select ss.StyleCode " +
						"	from Sku ss " +
						"	left join (select `StyleCode`, `ColorKey` " +
						"			from `StyleImage` where MerchantId = :MerchantId and `ImageTypeCode` = \'Color\' group by `StyleCode`, `ColorKey`) si on ss.StyleCode = si.StyleCode and ss.ColorKey = si.ColorKey  " +
						"	where si.StyleCode is null) a where s.StyleCode = a.StyleCode) ";
					break;
			}
		}
	})
	.then(function(){
		if (!cu.isBlank(StyleCode))
			where=where + " and s.StyleCode=:StyleCode ";
	})
	.then(function(){
		if (!cu.checkIdBlank(CategoryId))
		{
			var tree=lcat.convertTree(CategoryListAdjacency);
			var subtree=lcat.findSubtree(tree,CategoryId);
			var catarray=lcat.subtreeCategoryArray(subtree);
			var cin=catarray.join();
			cin = cin.replace(/[;\']/g,'###'); //don't allow literal quote, or semi colons put triple hash in to show the error
			where=where + " and scat.CategoryId in ({cin})";
			where=where.replace('{cin}',cin);
		}
	})
	.then(function() {
		sq=sql;
		sq=sq.replace('{fields}',fields);
		sq=sq.replace('{where}',where);
		sq=sq + " LIMIT "+ offset +"," + limit;
		return tr.queryMany(sq,{MerchantId:MerchantId,CultureCode:CultureCode,StatusId:StatusId,StyleCode:StyleCode,Search:Search}).then(function(data) {
			ProductList=data;
		})
	})
	.then(function(){
		sq=sql;
		sq=sq.replace('{fields}',' COUNT(*) as Total ');
		sq=sq.replace('{where}',where);
		return tr.queryMany(sq, {MerchantId:MerchantId,CultureCode:CultureCode,StatusId:StatusId,StyleCode:StyleCode,Search:Search}).then(function(data){
			ProductTotalSize=data.length;
			var PageCountDec = (ProductTotalSize / size);
			PageCount = parseInt(PageCountDec);
			if (PageCountDec>PageCount)
				PageCount=PageCount + 1;
		});
	})
	.then(function() {
		ProductList.map(function(iProd){
			iProd.PrimaryCategoryPathList=lcat.categoryPath(CategoryListAdjacency,iProd.PrimaryCategoryId);
		});
	})
	.then(function() {
		var promises = ProductList.map(function(iProd){
			var params={SkuId:iProd.PrimarySkuId,CultureCode:CultureCode};
			return tr.queryMany("select distinct c.CategoryId, sc.Priority, cc.CategoryName, c.CategoryNameInvariant from Category c inner join CategoryCulture cc on (c.CategoryId=cc.CategoryId and cc.CultureCode=:CultureCode) inner join SkuCategory sc on (sc.CategoryId=c.CategoryId) where sc.CategoryId<>0 and sc.SkuId=:SkuId",params).then(function(data) {
				iProd.CategoryPriorityList=[];
				data.map(function(iCat){
					var cattle=lcat.categoryPath(CategoryListAdjacency,iCat.CategoryId);
					//var cattle = _.merge(cattle, {Priority:iCat.Priority});
					cattle.map(function(iPror){
						iPror.Priority=iCat.Priority;
						iProd.CategoryPriorityList.push(iPror);
					});
				});
			});
		});
		return Q.all(promises);
	})
	.then(function() {
		var promises = ProductList.map(function(iProd){
			return tr.queryMany("select distinct s.SizeId, sz.SizeNameInvariant as SizeName from Sku s inner join Size sz on s.SizeId=sz.SizeId where sz.SizeId<>0 and s.StyleCode = :StyleCode order by sz.SizeId",{StyleCode:iProd.StyleCode,CultureCode:CultureCode}).then(function(data) {
				iProd.SizeList=data;
			});
		});
		return Q.all(promises);
	})
	.then(function() {
		var promises = ProductList.map(function(iProd){
			return tr.queryMany("select distinct s.ColorId,  colc.ColorName, s.ColorKey, sc.SkuColor from Sku s inner join Color col on (s.ColorId=col.ColorId) inner join ColorCulture colc on (col.ColorId=colc.ColorId and colc.CultureCode=:CultureCode) inner join SkuCulture sc on (s.SkuId=sc.SkuId and sc.CultureCode=:CultureCode) where col.ColorId<>0 and s.StyleCode = :StyleCode order by s.ColorKey",{StyleCode:iProd.StyleCode,CultureCode:CultureCode}).then(function(data) {
				iProd.ColorList=data;
			});
		});
		return Q.all(promises);
	})
	.then(function() {
		var promises = ProductList.map(function(iProd) {
			iProd.FeaturedImageList = [];
			iProd.DescriptionImageList = [];
			iProd.ColorImageList = [];

			return tr.queryMany("select StyleImageId, ProductImage, ImageTypeCode, ColorKey, Position from StyleImage where MerchantId = :MerchantId and StyleCode = :StyleCode order by ColorKey, Position", {
	    MerchantId: iProd.MerchantId,
	    StyleCode: iProd.StyleCode
	  }).then(function(rows) {
	      _.each(rows, function(row) {
	        if (row.ImageTypeCode === 'Feature') {
	          // Featured image
	          iProd.FeaturedImageList.push({
	            StyleImageId: row.StyleImageId,
	            ImageKey: row.ProductImage,
	            Position: row.Position
	          });
	        } else if (row.ImageTypeCode === 'Desc') {
	          iProd.DescriptionImageList.push({
	            StyleImageId: row.StyleImageId,
	            ImageKey: row.ProductImage,
	            Position: row.Position
	          });
	        } else if (row.ImageTypeCode === 'Color'){
	          iProd.ColorImageList.push({
	            StyleImageId: row.StyleImageId,
	            ColorKey: row.ColorKey,
	            ImageKey: row.ProductImage,
	            Position: row.Position
	          });
	        }
	      });

				// Override the default "image not found" image 00000000000000000000000000000000
				if (iProd.FeaturedImageList.length > 0) {
					// Set the first feature image (which has the lowest position) as default. The "order by" in SQL is important.
					iProd.ImageDefault = iProd.FeaturedImageList[0].ImageKey;
				}
				else if (iProd.ColorImageList.length > 0) {
					// Use the first color image as default if there is no feature image.
					iProd.ImageDefault = iProd.ColorImageList[0].ImageKey;
				}
				else if (iProd.DescriptionImageList.length > 0) {
					// Use the first description image as default if there is no feature or color image.
					iProd.ImageDefault = iProd.DescriptionImageList[0].ImageKey;
				}
			});
		});
		return Q.all(promises);
	})
	.then(function() {
		var promises = ProductList.map(function(iProd){
			return Factory.skuList(iProd.MerchantId,iProd.StyleCode,0,0,0,'',CultureCode, 1, 100000,tr).then(function(data) {
				iProd.SkuList=data.PageData;
			});
//			return Factory.skuListWithInventorySummary(iProd.MerchantId,iProd.StyleCode,0,0,0,'',CultureCode, 1, 100000,tr).then(function(data) {
//				iProd.SkuList=data;
//			});
		});
		return Q.all(promises);
	})
	.then(function() {
		deferred.resolve({ HitsTotal : ProductTotalSize,PageTotal : parseInt(PageCount), PageSize : parseInt(size), PageCurrent : page + 1, PageData : ProductList  } );
	})
	.catch(function(err){
		deferred.reject(err);
	})
	.done();
	return deferred.promise;
};
*/ 

// Keep skuListWithInventorySummary for case need iteration SQL call to resolving unresolved INDEXES in Factory.skuList.
//Factory.skuListWithInventorySummary = function (MerchantId, StyleCode, CategoryId, StatusId, SkuId, Search, CultureCode, page, size, tr) {
//    var deferred = Q.defer();
//    var skuList = null;
//    Q.when()
//        .then(function () {
//            return Factory.skuList(MerchantId, StyleCode, CategoryId, StatusId, SkuId, Search, CultureCode, page, size, tr).then(function (data) {
//                if (typeof data.PageData !== 'undefined' && data.PageData.length > 0) {
//                    skuList = data.PageData;
//                }
//            });
//        })
//        .then(function () {
//                var promises = skuList.map(function (sku) {
//                    if (typeof sku.SkuId !== 'undefined' && sku.SkuId > 0) {
//                        return li.getSkuInventorySummary(sku.SkuId, tr).then(function (summary) {
//                            sku.LocationCount = summary.LocationCount;
//                            sku.QtyAts = summary.QtyAts;
//                            sku.InventoryStatusId = summary.InventoryStatusId;
//                        });
//                    }
//                });
//                return Q.all(promises);
//        })
//        .then(function () {
//            deferred.resolve(skuList);
//        })
//        .catch(function (err) {
//            deferred.reject(err);
//        })
//        .done();
//
//    return deferred.promise;
//};


Factory.skuList = function(MerchantId,StyleCode,CategoryId,StatusId, SkuId, Search, CultureCode, page, size,tr){
	var deferred = Q.defer();
	var CategoryListAdjacency=null;
	var ProductList = null;
	var ProductSize = 0;
	var ProductTotalSize = 0;
	var PageCount=0;

	if(page){
		page = parseInt(page);
	}
	if(isNaN(page)){
		page = 1;
	}
	//decrease page number for offset
	page -= 1;

	var limit = parseInt(size);
	var offset = (page * limit);

	var CategoryArray=null;
	//, s.*, bc.BrandName, scu.SkuName, scu.SkuDesc, scu.SkuMaterial, scu.SkuFeature, stc.StatusName
//	var fields="s.SkuId, s.StyleCode, s.SkuCode, s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, s.GeoCountryId, s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, s.QtySafetyThreshold, s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, scat.CategoryId as PrimaryCategoryId, sz.SizeNameInvariant as SizeName,s.ColorKey, clc.ColorName, scu.SkuColor, scu.SkuName";
//	var sql="select {fields} from Sku s "+
//                "INNER join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) "+
//                "INNER join Brand b on (s.BrandId=b.BrandId) "+
//                "INNER join BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) "+
//                "INNER join Size sz on (s.SizeId=sz.SizeId) inner join Color cl on (s.ColorId=cl.ColorId) "+
//                "INNER join ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) "+
//                "INNER join SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0) "+
//                "INNER join Status st on (s.StatusId=st.StatusId) "+
//                "INNER join StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) "+
//                "where s.StatusId <> :DeletedStatusId {where}";
//	var fields="s.SkuId, s.StyleCode, s.SkuCode, s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, s.GeoCountryId, s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, s.QtySafetyThreshold, s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, scat.CategoryId as PrimaryCategoryId, sz.SizeNameInvariant as SizeName,s.ColorKey, clc.ColorName, scu.SkuColor, scu.SkuName";
//        fields+=",Summary.LocationCount,Summary.QtyAts,Summary.InventoryStatusId ";
//        // If table index cannot resolve perforamce case when query start at larger table set(i.e. not the Sku table) in execution plan,
//        // we may use LEFT JOIN to force the smaller Sku table being executed at the first step.
//	var sql="select {fields} from Sku s "+
//                "INNER join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) "+
//                "INNER join Brand b on (s.BrandId=b.BrandId) "+
//                "INNER join BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) "+
//                "INNER join Size sz on (s.SizeId=sz.SizeId) inner join Color cl on (s.ColorId=cl.ColorId) "+
//                "INNER join ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) "+
//                "INNER join SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0) "+
//                "INNER join Status st on (s.StatusId=st.StatusId) "+
//                "INNER join StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) "+
//                "LEFT JOIN ("+
//                "SELECT result.SkuId, result.LocationCount, result.QtyAts, " +
//                "IF(result.HasInventory = 0, :NAId, " +
//                "IF(result.QtyAts = 'Unlimited', :InStockId, " +
//                "IF(result.QtyAts < result.SystemSafetyThreshold, :OutOfStockId, " +
//                "IF(result.QtyAts <= result.ProductLowStockThreshold, :LowStockId, :InStockId)))) AS InventoryStatusId "+
//                "FROM (SELECT inv.SkuId, " +
//                "IF(SUM(inv.InventoryId),TRUE,FALSE) AS HasInventory, " +
//                "SUM(:SystemSafetyThreshold) AS SystemSafetyThreshold, " +
//                "SUM(:ProductLowStockThreshold) AS ProductLowStockThreshold, " +
//                "COUNT(inv.SkuId) AS LocationCount, " +
//                "IF(SUM(inv.IsPerpetual), 'Unlimited', (SUM(inv.QtyAllocated) - SUM(inv.QtyExported) - SUM(inv.QtyOrdered)  - SUM(ss.QtySafetyThreshold) - SUM(il.QtySafetyThreshold))) as QtyAts " +
//                "FROM Sku ss " +
//                "INNER JOIN Inventory inv ON ss.SkuId = inv.SkuId " +
//                "INNER JOIN InventoryLocation il ON il.InventoryLocationId = inv.InventoryLocationId " +
//                "WHERE ss.StatusId <> 1 AND il.StatusId = 2 " +
//                "GROUP BY inv.SkuId ) AS result " +
//                ") AS Summary ON Summary.SkuId = s.SkuId " +
//                "where s.StatusId <> 1 {where}";


        //overwrite the original sql and fields to backup also the original sql
        var perpetualAllocationNo = config.inventory.perpetualAllocationNo;
//        var fields = "s.SkuId, s.StyleCode, s.SkuCode, s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, s.GeoCountryId, "+
//                    "s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, s.SaleTo, s.AvailableFrom, s.AvailableTo, s.LastCreated, " +
//                    "s.QtySafetyThreshold, s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, sz.SizeNameInvariant as SizeName, " +
//                    "s.ColorKey, cl.ColorCode, cl.ColorImage, clc.ColorName, scu.SkuColor, scu.SkuName, s.ManufacturerName, " +
//                    "COUNT(inv.SkuId) AS LocationCount, " +
//                    "IF(SUM(inv.IsPerpetual), :PerpetualAllocationNo, (SUM(inv.QtyAllocated) - SUM(inv.QtyExported) - SUM(inv.QtyOrdered)  - SUM(s.QtySafetyThreshold) - SUM(il.QtySafetyThreshold))) as QtyAts ";
//
//        var sql = "select {fields} FROM Sku s " +
//                    "INNER join SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) " +
//                    "INNER join Size sz on (s.SizeId=sz.SizeId) " +
//                    "INNER join Color cl on (s.ColorId=cl.ColorId) " +
//                    "INNER join ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) " +
//                    "LEFT JOIN Inventory inv ON s.SkuId = inv.SkuId AND inv.StatusId = 2 " +
//                    "LEFT JOIN InventoryLocation il ON il.InventoryLocationId = inv.InventoryLocationId AND il.StatusId = 2 " +
//                    "WHERE " +
//                    "s.StatusId <> 1 {where}";

        var systemSafetyThreshold = config.inventory.systemSafetyThreshold;
        var productLowStockThreshold = config.inventory.productLowStockThreshold;

        var params = {
            SystemSafetyThreshold: systemSafetyThreshold,
            ProductLowStockThreshold: productLowStockThreshold,
            PerpetualAllocationNo : perpetualAllocationNo //added derived from stylelist sql
        };
        
	var where='1 = 1 ';
	var sq='';

	Q.when()
	.then(function(){
		if (cu.isBlank(CultureCode))
			throw {AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING'};
	})
	.then(function(){
		return lcat.listAdjacency(CultureCode, false ,tr).then(function(data){
			CategoryListAdjacency=data;
		});
	})
	.then(function(){
		if (!cu.checkIdBlank(MerchantId))
			where=where + " and s.MerchantId=:MerchantId ";
	})
	.then(function(){
		if (!cu.isBlank(StyleCode))
			where=where + " and s.StyleCode=:StyleCode ";
	})
	.then(function(){
		if (!cu.checkIdBlank(SkuId))
			where=where + " and s.SkuId=:SkuId ";
	})
	.then(function(){
		if (!cu.checkIdBlank(StatusId))
			where=where + " and s.StatusId=:StatusId ";
		else
			where=where + " and s.StatusId <> 1 "
	})
	.then(function(){
		if (!cu.checkIdBlank(Search))
		{
			Search=Search + '%';
			where=where + " and (scu.SkuName like :Search or s.StyleCode like :Search or s.SkuCode like :Search) ";
		}
	})
	.then(function(){
		if (!cu.checkIdBlank(CategoryId))
		{
			var tree=lcat.convertTree(CategoryListAdjacency);
			var subtree=lcat.findSubtree(tree,CategoryId);
			var catarray=lcat.subtreeCategoryArray(subtree);
			var cin=catarray.join();
			cin = cin.replace(/[;\']/g,'###'); //don't allow literal quote, or semi colons put triple hash in to show the error
			where=where + " and scat.CategoryId in ({cin})";
			where=where.replace('{cin}',cin);
		}
	})
	.then(function() {
        return li.setParamStatus(params, tr);
    })
	.then(function() {

                sq = skuListQueryBuilder.getDataQuery(where, offset, limit);
//		sq=sql;
//		sq=sq.replace('{fields}',fields);
//		sq=sq.replace('{where}',where);
//		sq=sq  + " GROUP BY s.SkuId"; //derived from style list skusql
//		sq=sq + " LIMIT "+ offset +"," + limit;

                params.MerchantId = MerchantId;
                params.CultureCode = CultureCode;
                params.StatusId = StatusId;
                params.SkuId = SkuId;
                params.StyleCode = StyleCode;
                params.Search = Search;

		return tr.queryMany(sq,params).then(function(data) {
			ProductList=data;
		});
	})
	.then(function(){ //sku logic from stylist sku logic remove others because its for iProd = Stylelist
		let rd=moment();
		for (let iSku of ProductList)
		{

            //Update inventory status
            iSku.InventoryStatusId = params.NAId;

            if (iSku.LocationCount){
                if (iSku.QtyAts === perpetualAllocationNo){
                    iSku.InventoryStatusId = params.InStockId;
                } else if (iSku.QtyAts < systemSafetyThreshold * iSku.LocationCount){
                    iSku.InventoryStatusId = params.OutOfStockId;
                } else if (iSku.QtyAts <= productLowStockThreshold * iSku.LocationCount){
                    iSku.InventoryStatusId = params.LowStockId;
                } else{
                    iSku.InventoryStatusId = params.InStockId;
                }
            }

            if(cu.isBlank(iSku.QtyAts)){
                iSku.QtyAts = 0;
            }
            else{
                iSku.QtyAts = parseInt(iSku.QtyAts);
            }

			iSku.IsNew=~~(moment(iSku.LastCreated)>=rd.add(-1, 'months'));
			iSku.IsSale=~~(moment(iSku.SaleFrom)<=rd && moment(iSku.SaleTo)>=rd);

		}
	})
	.then(function(){
                sq = skuListQueryBuilder.getCountQuery(where);
//		sq=sql;
//		sq=sq.replace('{fields}',' COUNT(*) as Total ');
//		sq=sq.replace('{where}',where);

                params.MerchantId = MerchantId;
                params.CultureCode = CultureCode;
                params.StatusId = StatusId;
                params.SkuId = SkuId;
                params.StyleCode = StyleCode;
                params.Search = Search;

		return tr.queryOne(sq, params).then(function(data){
			ProductTotalSize=data.Total;
			var PageCountDec = (ProductTotalSize / size);
			PageCount = parseInt(PageCountDec);
			if (PageCountDec>PageCount)
				PageCount=PageCount + 1;
		});
	})
	.then(function() {
		deferred.resolve({ HitsTotal : ProductTotalSize,PageTotal : parseInt(PageCount), PageSize : parseInt(size), PageCurrent : page + 1, PageData : ProductList  } );
	})
	.catch(function(err){
		deferred.reject(err);
	})
	.done();
	return deferred.promise;
};


/*
// Factory.ReadXlsx=function(filepath){
Factory.readExcelJS=function(filepath){
	var deferred = Q.defer();
	var skus = [];
	var row1 = [];
	var workbook = new Excel.Workbook();
	var Header=[];
	Q.when()
	.then(function(){
		return workbook.xlsx.readFile(filepath)
		.catch(function(erre){throw {AppCode: 'MSG_ERR_IMPORT_SKU_XLXS_CORRUPT'} });
	})
	.then(function() {
		var worksheet = workbook.getWorksheet("SKU");
		if (worksheet==null)
			throw {AppCode: 'MSG_ERR_IMPORT_SKU_WORKSHEET_MISSING'};
		
		
		var row = worksheet.getRow(1);
		var i=1;
		while (i<1000)
		{
			console.log(i);
			var curCell=row.getCell(i);
			if (curCell)
			{
				var value=curCell.value;
				if (value)
				{
					var celluar= {};
					celluar.index=i;
					celluar.name=value.replace(/ /g, '');
					Header.push(celluar);
				}
			}
			i++;
			curCell=row.getCell(i);
		}
		console.log(Header);
		
		worksheet.eachRow(function(rower, rowNumber){
			if(rowNumber > 1){
				var rowData = { rowNumber : rowNumber };
				for (var j in Header)					
				{
					var HeaderCol=Header[j];
					
					var cell=rower.getCell(HeaderCol.index);
					var value = cell.value;

					if( value && typeof value === 'object' ){
				      	value = value.result;
				    }

					rowData["Delete"] = "";
					if(cu.isBlank(value)){ value = ""; }
					rowData[HeaderCol.name] = value;
				};
				skus.push(rowData);
			}
		});
		
		console.dir(skus);
		
				
		// var row1 = [];

		// console.time("eachrow");
		// worksheet.eachRow(function(row, rowNumber){
			// if(rowNumber > 1){
				// var rowData = { rowNumber : rowNumber };
				// row.eachCell({ includeEmpty: true }, function(cell, colNumber){
					// var value = cell.value;

					// if( value && typeof value === 'object' ){
				      	// value = value.result;
				    // }

					// rowData["Delete"] = "";
					// if(cu.isBlank(value)){ value = ""; }
					// rowData[row1[colNumber - 1]] = value;
				// });
				// skus.push(rowData);
			// }else{
				// row.eachCell({ includeEmpty: true }, function(cell, colNumber){
					// var value = cell.value;
					// if( value && typeof value === 'object' ){
				      	// value = value.result;
				    // }

					// if(colNumber == 2){
						// value = "SkuCode";
					// }

					// value = value.replace(/ /g, '');
					// row1.push(value);
				// });
			// }

			// if (row==worksheet.lastRow){
				// console.log("----LastRow");
				// console.timeEnd("eachrow");
				// console.log(row1);
			// }
		// });
		
	})
	.then(function() {
		deferred.resolve({ skus : skus, rowDetails : Header });
	})
	.then(function() {
		res.json(Reference);
		return;
	})
	.catch(function(err){
		deferred.reject(err);
	})
	return deferred.promise;
}
*/
		
/*
Factory.ReadXlsx=function(filepath){
	var deferred = Q.defer();
	var SkuList = [];
	var row = [];
	var ctr = 0;
	var obj = null;
	
	Q.when()
	.then(function() {

		obj = xlsx.parse(filepath);
		obj.forEach(throat(1,function(item){
			if(item.name == "SKU"){
				item.data.forEach(throat(1,function(elem, ind){
					if(ind == 0){
						elem.forEach(function(cell, ind){
							cell = cell.replace(/ /g, '');
							row.push(cell);
						});

						ctr++;
					}else{
						var rowData = { rowNumber : ind + 1 };
						rowData["Delete"] = "";
						for(var i = 0; i < elem.length; i++){
							var value = elem[i];
							if(cu.isBlank(value)){ value = ""; }
							rowData[row[i]] = value;
						}

						SkuList.push(rowData);
						ctr++;
					}

					if(item.data.length == ctr){
						console.log("done reading.. return.");
						return SkuList;
					}
				}));
			}
		}));
	})
	.then(function() {
		deferred.resolve({ skus : SkuList, rowDetails : row });
	})
	.catch(function(err){
		deferred.reject(err);
	})
	return deferred.promise;
}
*/

Factory.ActivalAllPending = function(Status, tr){
    var deferred = Q.defer();
    Q.when()
        .then(function() {
            return tr.queryExecute("update Sku set StatusId=2 where MerchantId=:MerchantId and StatusId=3 " +
                "and exists (select MerchantId from Merchant where MerchantId=:MerchantId and StatusId in (:StatusActive)) " +
                "and BrandId in (select BrandId from Brand where StatusId in (:StatusActive))",
                _.extend(Status, {StatusActive: CONSTANTS.STATUS.ACTIVE}));
        })
        .then(function() {
            deferred.resolve(Status);
        })
        .catch(function(err){
            deferred.reject(err);
        })
        .done();
    return deferred.promise;
};

Factory.StyleStatusChange = function(Bale, tr){
	var deferred = Q.defer();
	Q.when()
	.then(function(){
		if (_.parseInt(Bale.StatusId) === CONSTANTS.STATUS.DELETED) {
			//dont delete if it has inventory
			return tr.queryOne("select InventoryLocationId from Inventory i inner join Sku s on i.SkuId=s.SkuId " +
				"where s.StyleCode=:StyleCode and i.MerchantId=:MerchantId and s.MerchantId=:MerchantId, and i.StatusId=:StatusId",
				{StyleCode: Bale.StyleCode, MerchantId: Bale.MerchantId, StatusId: CONSTANTS.STATUS.ACTIVE}).then(function(data) {
				if (data !== null) {
					throw {AppCode:"MSG_ERR_DEL_SKU_FOUND_INVENTORY"};
				}
			});
		}
	})
	.then(function() {
		return tr.queryExecute("update Sku set StatusId=:StatusId where MerchantId=:MerchantId and StyleCode=:StyleCode and StatusId<>1",Bale).then(function(data) {
			return;
		})
	})
	.then(function() {
		deferred.resolve(Bale);
	})
	.catch(function(err){
		deferred.reject(err);
	})
	.done();
	return deferred.promise;
};

Factory.SkuStatusChange = function(tr, Sku, StatusIdRequested, isActivate){
	var deferred = Q.defer();
	Q.when()
	.then(function(){
		return tr.queryOne("select s.* from Sku s where s.SkuId=:SkuId " +
            (isActivate ?
            "and exists (select MerchantId from Merchant where MerchantId=s.MerchantId and StatusId=:StatusActive) " +
            "and exists (select BrandId from Brand where BrandId=s.BrandId and StatusId=:StatusActive)" : ""),
            _.extend(Sku, {StatusActive: CONSTANTS.STATUS.ACTIVE})).then(function(data) {
			if (data)
			{
				Sku=data;
				if (Sku.StatusId==CONSTANTS.STATUS.DELETED) //don't perfom if deleted
					throw {AppCode:"MSG_ERR_SKU_STATUS_INVALID"};
			}
			else
			{
				throw {AppCode:"MSG_ERR_SKU_NOT_FOUND"};
			}
		})
	})
	.then(function() {
		Sku.StatusId=StatusIdRequested;
		return tr.queryExecute("update Sku set StatusId=:StatusId where SkuId=:SkuId;",Sku).then(function(data) {
			return;
		})
	})
	.then(function() {
		deferred.resolve(Sku);
	})
	.catch(function(err){
		deferred.reject(err);
	})
	.done();
	return deferred.promise;
};

Factory.skusave = function(ProductRow, tr){
	var deferred = Q.defer();
	var product = null;
	var statusMessage = "";
	var errorCols = [];

	Q.when()
	.then(function(){

		///addded for CHS CHT HANS AND HANT compatibility
		var CHS = ['ColorName-CHS', 'SkuName-CHS', 'Description-CHS', 'SizeComment-CHS', 'Material-CHS', 'Feature-CHS'];
		var CHT = ['ColorName-CHT', 'SkuName-CHT', 'Description-CHT', 'SizeComment-CHT', 'Material-CHT', 'Feature-CHT'];
		var fieldNames = [];
		for(var i = 0; i < CHS.length; i++){
			var fieldName = CHS[i].split('-')[0];
			fieldNames.push(fieldName);
			if(ProductRow[CHS[i]]){
				ProductRow[fieldName + "-Hans"] = ProductRow[CHS[i]];
			}
			if(ProductRow[CHT[i]]){
				ProductRow[fieldName + "-Hant"] = ProductRow[CHT[i]];
			}
		}

		//copy Hans to Hant or English if empty
		var otherLanguages = ['Hant', 'English'];
		_.each(otherLanguages, function (language) {
			_.each(fieldNames, function (fieldName) {
				if (!cu.isBlank(ProductRow[fieldName + "-Hans"]) &&
					cu.isBlank(ProductRow[fieldName + "-" + language])) {
					ProductRow[fieldName + "-" + language] = ProductRow[fieldName + "-Hans"];
				}
			});
		});


		if (cu.isBlank(ProductRow.StyleCode)){
			ProductRow.StyleCode = ':MISSING:' + ProductRow.StyleCode || "";
			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "StyleCode" })
		}

		if (cu.isBlank(ProductRow.SkuCode)){
			ProductRow.SkuCode = ':MISSING:' + ProductRow.SkuCode || "";
			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "SkuCode" })
		}

		if (cu.isBlank(ProductRow.BrandCode) && cu.isBlank(ProductRow.BrandId)){
			ProductRow.BrandCode = ':MISSING:' + ProductRow.BrandCode || "";
			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "BrandCode/BrandId" })
		}

		if (cu.isBlank(ProductRow.RetailPrice)){
			ProductRow.RetailPrice = ':MISSING:' + ProductRow.RetailPrice || "";
			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "RetailPrice" })
		}else if(parseFloat(ProductRow.RetailPrice) <= 0 || isNaN(parseFloat(ProductRow.RetailPrice))){
			ProductRow.RetailPrice = ':NOTVALID:' + ProductRow.RetailPrice;
			errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', Message : "RetailPrice" })
		}
                
		if (cu.isBlank(ProductRow.PrimaryCategoryCode) && cu.isBlank(ProductRow.CategoryId)){
			ProductRow.PrimaryCategoryCode = ':MISSING:' + ProductRow.PrimaryCategoryCode || "";
			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "PrimaryCategoryCode/PrimaryCategoryId" })
		}

		//check merchandise category; merchantdise category must start from 1, otherwise validate fail. this checking is raising by QA team.
		var mustNull = false;
		[1,2,3,4,5].map(function(index){
			var tmpMerchCategory = ProductRow['MerchandisingCategoryCode' + index];
			if(cu.isBlank(tmpMerchCategory)){
				mustNull = true;
			}else{
				if(mustNull){
					ProductRow['MerchandisingCategoryCode' + index] = ':MISSING:' + ProductRow['MerchandisingCategoryCode' + index] || "";
					errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "MerchandisingCategory" })
				}
			}
		});


		/** should be optional as same in product edit page **/
		//if (cu.isBlank(ProductRow.ShippingWeightKg)){
		//	ProductRow.ShippingWeightKg = ':MISSING:' + ProductRow.ShippingWeightKg || "";
		//	errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ShippingWeightKg" })
		//}
		if (cu.isBlank(ProductRow["SkuName-English"])){
			ProductRow["SkuName-English"] = ':MISSING:' + ProductRow["SkuName-English"];
			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "SkuName-English" })
		}
		if (cu.isBlank(ProductRow["SkuName-Hans"])){
			ProductRow["SkuName-Hans"] = ':MISSING:' + ProductRow["SkuName-Hans"];
			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "SkuName-Hans" })
		}
		if (cu.isBlank(ProductRow["SkuName-Hant"])){
			ProductRow["SkuName-Hant"] = ':MISSING:' + ProductRow["SkuName-Hant"];
			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "SkuName-Hant" })
		}

		// Start Color Checker
		if( !cu.isBlank(ProductRow["ColorName-Hant"]) || !cu.isBlank(ProductRow["ColorName-English"]) ){
			if(Factory.isBlank(ProductRow["ColorCode"])){
				ProductRow["ColorCode"] = ':MISSING:' + ProductRow["ColorCode"];
				errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ColorCode" })
			}

			if (Factory.isBlank(ProductRow["ColorName-Hans"])){
				ProductRow["ColorName-Hans"] = ':MISSING:' + ProductRow["ColorName-Hans"];
				errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ColorName-Hans" })
			}else{
				ProductRow.ColorKey = Factory.compactFilename(ProductRow["ColorName-English"].toString());
			}
		}else if( cu.isBlank(ProductRow["ColorName-Hant"]) || cu.isBlank(ProductRow["ColorName-English"]) ){
			if(!Factory.isBlank(ProductRow["ColorCode"]) && !Factory.isBlank(ProductRow["ColorName-Hans"])){

			}else{
				if( !Factory.isBlank(ProductRow["ColorCode"]) && ProductRow["ColorCode"] != '0'){
                                        if(cu.isBlank(ProductRow["ColorName-Hans"])){
                                            ProductRow["ColorName-Hans"] = '';
                                        }
					ProductRow["ColorName-Hans"] = ':MISSING:' + ProductRow["ColorName-Hans"];
					errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ColorName-Hans" })
				}else if(!Factory.isBlank(ProductRow["ColorName-Hans"])){
					ProductRow["ColorCode"] = ':MISSING:' + ProductRow["ColorCode"];
					errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "ColorCode" })
				}
			}

		}
		// End Color Checker
                
	    if(cu.isBlank(ProductRow.PriceSale)){
	        ProductRow.PriceSale = 0;
	    }
		if(cu.isBlank(ProductRow.IsFeatured)){
			ProductRow.IsFeatured = 0;
		}

		if(!cu.isBlank(ProductRow["SalePriceFrom"]) && !Factory.isValidDate(ProductRow["SalePriceFrom"])){
			ProductRow["SalePriceFrom"] = ':NOTVALID:' + ProductRow["SalePriceFrom"];
			errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', Message : "SalePriceFrom" })
		}

		if(!cu.isBlank(ProductRow["SalePriceTo"]) && !Factory.isValidDate(ProductRow["SalePriceTo"])){
			ProductRow["SalePriceTo"] = ':NOTVALID:' + ProductRow["SalePriceTo"];
			errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', Message : "SalePriceTo" })

			// if ( !(ProductRow["SalePriceFrom"] > ProductRow["SalePriceTo"]) ) {
 			// 	ProductRow["SalePriceTo"] = ':NOTVALID:' + ProductRow["SalePriceTo"];
			// 	errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', field : "SalePriceTo" })
			// }else{
			// 	ProductRow["SalePriceTo"] = ':NOTVALID:' + ProductRow["SalePriceTo"];
			// 	errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', field : "SalePriceTo" })
			// }

		}

		// if ( !(ProductRow["SalePriceFrom"] > ProductRow['SalePriceTo']) ) {
		// 	ProductRow["SalePriceTo"] = ':NOTVALID:' + ProductRow["SalePriceTo"];
		// 	errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', field : "SalePriceTo" })
		// }


		if( !Factory.isBlank(ProductRow.PriceSale) || !Factory.isBlank(ProductRow.SalePriceFrom) || !Factory.isBlank(ProductRow.SalePriceTo) ){
			
			if(Factory.isBlank(ProductRow.PriceSale)){
				ProductRow["PriceSale"] = ':MISSING:' + ProductRow["PriceSale"];
				errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "PriceSale" })
			}

			//SalePriceFrom is null means start from now,
			//SalePriceTo is null means to the end of world
			/**
			if(ProductRow.PriceSale > 0 && Factory.isBlank(ProductRow.SalePriceFrom)){
				ProductRow["SalePriceFrom"] = ':MISSING:' + ProductRow["SalePriceFrom"];
				errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "SalePriceFrom" })
			}

			if(ProductRow.PriceSale > 0 && Factory.isBlank(ProductRow.SalePriceTo)){
				ProductRow["SalePriceTo"] = ':MISSING:' + ProductRow["SalePriceTo"];
				errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', Message : "SalePriceTo" })
			}
			 **/

			// If PriceSale is greater than 0, check if SalePriceFrom is greater than SalePriceTo, if yes return an error
			// February 18, 2016 -> Alvin
			if(ProductRow.PriceSale > 0){
				if (parseInt(moment(ProductRow["SalePriceFrom"]).format('X')) > parseInt(moment(ProductRow["SalePriceTo"]).format('X'))) {
					ProductRow["SalePriceFrom"] = ':NOTVALID:' + ProductRow["SalePriceFrom"];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', Message : "SalePriceFrom" });
				} else if (parseInt(moment(ProductRow["SalePriceFrom"]).format('X')) < parseInt(moment(ProductRow["AvailableFrom"]).format('X'))) {
					ProductRow["SalePriceFrom"] = ':NOTVALID:' + ProductRow["SalePriceFrom"];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', Message : "SalePriceFrom" });
				}

				if (parseInt(moment(ProductRow["SalePriceTo"]).format('X')) > parseInt(moment(ProductRow["AvailableTo"]).format('X'))) {
					ProductRow["SalePriceTo"] = ':NOTVALID:' + ProductRow["SalePriceTo"];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', Message : "SalePriceTo" });
				}
			}

		}

		//console.log({to: Factory.isDate(ProductRow["AvailableFrom"]), from: Factory.isDate(ProductRow["AvailableTo"])});
		//		if(!Factory.isDate(ProductRow["AvailableFrom"])){
		//			ProductRow["AvailableFrom"] = ':NOTVALID:' + ProductRow["AvailableFrom"];
		//			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', field : "AvailableFrom" })
		//		}
		//
		//		if(!Factory.isDate(ProductRow["AvailableTo"])){
		//			ProductRow["AvailableTo"] = ':NOTVALID:' + ProductRow["AvailableTo"];
		//			errorCols.push({ AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING', field : "AvailableTo" })
		//		}

		if(!cu.isBlank(ProductRow["AvailableFrom"]) && !Factory.isValidDate(ProductRow["AvailableFrom"])){
			ProductRow["AvailableFrom"] = ':NOTVALID:' + ProductRow["AvailableFrom"];
			errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', Message : "AvailableFrom" });
		}

		if(!cu.isBlank(ProductRow["AvailableTo"]) && !Factory.isValidDate(ProductRow["AvailableTo"])){
			ProductRow["AvailableTo"] = ':NOTVALID:' + ProductRow["AvailableTo"];
			errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', Message : "AvailableTo" });
		}

		// if AvailableFrom and AvailableTo is not blank, convert both date to timestamp then compare
		// AvailableTo should be greater that AvailableFrom --> February 18, 2016 -> Alvin
		if(!cu.isBlank(ProductRow["AvailableFrom"]) && !cu.isBlank(ProductRow["AvailableTo"])){
			if(parseInt(moment(ProductRow["AvailableFrom"]).format('X')) > parseInt(moment(ProductRow["AvailableTo"]).format('X'))){
				ProductRow["AvailableFrom"] = ':NOTVALID:' + ProductRow["AvailableFrom"];
				errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', Message : "AvailableFrom" })
			}
		}

	})
	.then(function(){
		if(cu.isBlank(ProductRow.BrandId) && ProductRow.BrandCode.indexOf(':MISSING:') <= -1){
			var sqlQuery = "SELECT * FROM Brand WHERE BrandCode = :BrandCode and StatusId<>:StatusId";
			return tr.queryOne(sqlQuery, { BrandCode : ProductRow.BrandCode, StatusId: CONSTANTS.STATUS.PENDING }).then(function(data){
				if(data && !cu.isBlank(data.BrandId)){
					return ProductRow.BrandId = data.BrandId;
				}else{
					ProductRow["BrandCode"] = ':NOTFOUND:' + ProductRow["BrandCode"];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_FOUND', Message : "BrandCode" })
				}
			});
		}
	})
	.then(function(){
		if(cu.isBlank(ProductRow.BadgeId) && !cu.isBlank(ProductRow.BadgeCode) && ProductRow.BadgeCode.indexOf(':MISSING:') <= -1){
			var sqlQuery = "SELECT * FROM Badge WHERE BadgeCode = :BadgeCode";
			return tr.queryOne(sqlQuery, { BadgeCode : ProductRow.BadgeCode }).then(function(data){
				if(data && !cu.isBlank(data.BadgeId)){
					return ProductRow.BadgeId = data.BadgeId;
				}else{
					ProductRow["BadgeCode"] = ':NOTFOUND:' + ProductRow["BadgeCode"];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_FOUND', Message : "BadgeCode" })
				}
			});
		}
	})
	.then(function(){
		if(cu.isBlank(ProductRow.SeasonId) && !cu.isBlank(ProductRow.SeasonCode) && ProductRow.SeasonCode.indexOf(':MISSING:') <= -1){
			var sqlQuery = "SELECT * FROM Season WHERE SeasonCode = :SeasonCode";
			return tr.queryOne(sqlQuery, { SeasonCode : ProductRow.SeasonCode }).then(function(data){
				if(data && !cu.isBlank(data.SeasonId)){
					return ProductRow.SeasonId = data.SeasonId;
				}else{
					ProductRow["SeasonCode"] = ':NOTFOUND:' + ProductRow["SeasonCode"];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_FOUND', Message : "SeasonCode" })
				}
			});
		}
	})
	.then(function(){
		if(cu.isBlank(ProductRow.SizeId) && !cu.isBlank(ProductRow.SizeCode) && ProductRow.SizeCode.indexOf(':MISSING:') <= -1){
			var sqlQuery = "SELECT * FROM Size WHERE SizeCode = :SizeCode";
			return tr.queryOne(sqlQuery, { SizeCode : ProductRow.SizeCode }).then(function(data){
				if(data && !cu.isBlank(data.SizeId)){
					return ProductRow.SizeId = data.SizeId;
				}else{
					ProductRow["SizeCode"] = ':NOTFOUND:' + ProductRow["SizeCode"];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_FOUND', Message : "SizeCode" })
				}
			});
		}
	})
	.then(function(){
		if(!cu.isBlank(ProductRow.ColorCode) && ProductRow.ColorCode.indexOf(':MISSING:') <= -1){
			var sqlQuery = "SELECT * FROM Color WHERE ColorCode = :ColorCode";
			return tr.queryOne(sqlQuery, { ColorCode : ProductRow.ColorCode }).then(function(data){
				if(data && !cu.isBlank(data.ColorId)){
					return ProductRow.ColorId = data.ColorId;
				}else{
					ProductRow["ColorCode"] = ':NOTFOUND:' + ProductRow["ColorCode"];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_FOUND', Message : "ColorCode" })
				}
			});
		}
	})
	.then(function(){
		if(cu.isBlank(ProductRow.GeoCountryId) && !cu.isBlank(ProductRow.CountryOriginCode) && ProductRow.CountryOriginCode.indexOf(':MISSING:') <= -1){
			var sqlQuery = "SELECT * FROM GeoCountry WHERE CountryCode = :CountryCode";
			return tr.queryOne(sqlQuery, { CountryCode : ProductRow.CountryOriginCode }).then(function(data){
				if(data && data.GeoCountryId){
					return ProductRow.GeoCountryId = data.GeoCountryId;
				}else{
					ProductRow["CountryOriginCode"] = ':NOTFOUND:' + ProductRow["CountryOriginCode"];
					errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_FOUND', Message : "CountryOriginCode" })
				}
			});
		}
	})
	// .then(function(){
	// 	// SkuCode should be unique per MerchantID --> February 18, 2016 -> Alvin
	// 	var sqlQuery = "SELECT * FROM Sku WHERE SkuCode = :SkuCode and MerchantId= :MerchantId";
	// 	return tr.queryOne(sqlQuery, { SkuCode : ProductRow.SkuCode, MerchantId: ProductRow.MerchantId }).then(function(data){
	// 		if(data){
	// 			ProductRow["SkuCode"] = ':NOTVALID:' + ProductRow["SkuCode"];
	// 			errorCols.push({ AppCode: 'MSG_ERR_FIELD_NOT_VALID', Message : "SkuCode" })
	// 		}
	// 	});
	// })
	.then(function(){
		var checkNull = ["BrandId","BadgeId","SeasonId","SizeId","ColorId","GeoCountryId","LaunchYear","ShippingWeightKg","HeightCm","WidthCm","LengthCm","MerchantId","StatusId","QtySafetyThreshold"];
		var promises = checkNull.map(function(item){
			if(cu.isBlank(ProductRow[item])){
				ProductRow[item] = 0;
			}
		});

		return Q.all(promises)
	})
	.then(function(){
		// Count errors..
		console.log("ERROR COUNT", errorCols.length);
		if(errorCols.length > 0){
			throw { errors : errorCols };
		}else{
			return;
		}
	})
	.then(function(){
		var cats=[0,1,2,3,4,5];

		var promises = cats.map(function(cInd){
			var catCode=null;
			if (cInd==0)
				catCode=ProductRow['PrimaryCategoryCode'];
			else
				catCode=ProductRow['MerchandisingCategoryCode' + cInd];
			var sqlQuery = "SELECT * FROM Category WHERE CategoryCode = :CategoryCode";
			return tr.queryOne(sqlQuery, { CategoryCode : catCode }).then(function(data){
				ProductRow['CategoryId' + cInd] = (data && data.CategoryId) ? data.CategoryId : "";
			});
		});
		return Q.all(promises);
	})
	.then(function(){
		var sqlQuery = "SELECT * FROM Sku WHERE SkuCode = :SkuCode AND MerchantId = :MerchantId AND StatusId <> 1";
		return tr.queryOne(sqlQuery, { SkuCode : ProductRow.SkuCode, MerchantId : ProductRow.MerchantId }).then(function(data){
			product = data;
			console.log("Found SkuCode=" + ProductRow.SkuCode + " " + (product!=null));
		});
	})
	.then(function(){
		ProductRow.SkuNameEnglish = ProductRow["SkuName-English"];
		ProductRow.DescriptionEnglish = ProductRow["Description-English"];
		ProductRow.SizeCommentEnglish = ProductRow["SizeComment-English"];
		ProductRow.FeatureEnglish = ProductRow["Feature-English"];
		ProductRow.MaterialEnglish = ProductRow["Material-English"];
		ProductRow.ColorNameEnglish = ProductRow["ColorName-English"];
		ProductRow.QtySafetyThreshold = ProductRow['QtySafetyThreshold'];
		return;
	})
	.then(function(){
		// Insert Product Info to table.
		if(product == null){
			ProductRow.StatusId = CONSTANTS.STATUS.PENDING;

			if(ProductRow.Delete == 1){
				ProductRow.StatusId = CONSTANTS.STATUS.DELETED;
			}

			if (ProductRow.StatusId !=CONSTANTS.STATUS.DELETED)
			{
				var sqlQuery = "INSERT INTO Sku (StyleCode,SkuCode,Bar,BrandId,BadgeId,SeasonId,SizeId,ColorId,GeoCountryId,LaunchYear,ManufacturerName,PriceRetail,PriceSale,"+
                                        "SaleFrom,SaleTo,AvailableFrom,AvailableTo,WeightKg,HeightCm,WidthCm,LengthCm,StatusId,MerchantId,ColorKey, SkuNameInvariant, SkuDescInvariant, SkuSizeCommentInvariant, "+
                                        "SkuFeatureInvariant, SkuMaterialInvariant, SkuColorInvariant, QtySafetyThreshold, IsFeatured) "+
                                        "VALUES (:StyleCode,:SkuCode,:Barcode,:BrandId,:BadgeId,:SeasonId,:SizeId,:ColorId,:GeoCountryId,:LaunchYear,:ManufacturerName,:RetailPrice,:PriceSale,"+
                                        ":SalePriceFrom,:SalePriceTo,:AvailableFrom,:AvailableTo,:ShippingWeightKg,:HeightCm,:WidthCm,:LengthCm,:StatusId,:MerchantId,:ColorKey,:SkuNameEnglish, :DescriptionEnglish, :SizeCommentEnglish, "+
                                        ":FeatureEnglish, :MaterialEnglish, :ColorNameEnglish, :QtySafetyThreshold, :IsFeatured)";
                                
				return tr.queryExecute(sqlQuery, ProductRow).then(function(data){
					ProductRow.SkuId=data.insertId;
					statusMessage = "inserted";
					console.log("Inserted =" + ProductRow.SkuId);
				});
			}
			else
			{
				//This woudl have been an insert of a record marked as delete so instead skipp
				statusMessage = "skipped";
				//deferred.resolve(statusMessage);
			}
		}
	})
	.then(function(){
		// Update Product Info to table.
		if(product != null){
			console.log("has product.", product.SkuId, ProductRow.Overwrite);
			if(ProductRow.Overwrite || ProductRow.Overwrite == 'true'  || ProductRow.Overwrite == '1'  || ProductRow.Overwrite == 1){
				ProductRow.SkuId=product.SkuId;
				ProductRow.StatusId = product.StatusId;
				if(ProductRow.Delete == 1){
					ProductRow.StatusId = CONSTANTS.STATUS.DELETED;
				}
				//todo: to fix? we may need to update skunameinvariant, ... etc?
				console.log("Start updating product: " + ProductRow.SkuId);
				var sqlQuery = "UPDATE Sku SET "+
                                        "StyleCode=:StyleCode,SkuCode=:SkuCode,Bar=:Barcode,BrandId=:BrandId,BadgeId=:BadgeId,"+
                                        "SeasonId=:SeasonId,SizeId=:SizeId,ColorId=:ColorId,GeoCountryId=:GeoCountryId,LaunchYear=:LaunchYear,"+
                                        "ManufacturerName=:ManufacturerName,PriceRetail=:RetailPrice,PriceSale=:PriceSale,SaleFrom=:SalePriceFrom,"+
                                        "SaleTo=:SalePriceTo,AvailableFrom=:AvailableFrom,AvailableTo=:AvailableTo,WeightKg=:ShippingWeightKg,"+
                                        "HeightCm=:HeightCm,WidthCm=:WidthCm,LengthCm=:LengthCm, MerchantId=:MerchantId, ColorKey=:ColorKey, "+
                                        "StatusId=:StatusId, QtySafetyThreshold=:QtySafetyThreshold, IsFeatured=:IsFeatured "+
                                        "WHERE SkuId=:SkuId AND MerchantId=:MerchantId";
                                
				return tr.queryExecute(sqlQuery, ProductRow).then(function(data){
					statusMessage = "updated";
					console.log("Updated =" + ProductRow.SkuId);
				});
			}else{
				// Overwrite set to false, nothing else to do. resolve promise chain.
				statusMessage = "skipped";
				//deferred.resolve(statusMessage);
			}
		}
	})
	.then(function(){
		if (ProductRow.SkuId)
		{
			return Q.when()
			.then(function(){
				return tr.queryExecute("delete from SkuCategory where SkuId=:SkuId;",{SkuId:ProductRow.SkuId}).then(function(data) {
					return;
				})
			})
			.then(function() {
				var cats=[0,1,2,3,4,5];
				var promises = cats.map(function(cInd){
					var catId=ProductRow['CategoryId' + cInd];
					return tr.queryExecute("insert into SkuCategory (SkuId,CategoryId,Priority) values(:SkuId,:CategoryId,:Priority)",{SkuId:ProductRow.SkuId,CategoryId:catId,Priority:cInd})
				});
				return Q.all(promises);
			});
		}
	})
	.then(function(){
                // TODO: use UPDATE statement if possible to reduce transaction log in DELETE statement
		if (ProductRow.SkuId)
		{
			return Q.when()
			.then(function(){
				return tr.queryExecute("delete from SkuCulture where SkuId=:SkuId;",{SkuId:ProductRow.SkuId}).then(function(data) {
					return;
				})
			})
			.then(function() {
				var ccs=['English','Hans','Hant'];
				var promises = ccs.map(function(cci){
					var CultureCode='EN';
					if (cci==='Hans')
						CultureCode='CHS';
					if (cci==='Hant')
						CultureCode='CHT';
					var SkuName=ProductRow['SkuName-' + cci];
					var SkuColor=ProductRow['ColorName-' + cci];
					var SkuDesc=ProductRow['Description-' + cci];
					var SkuSizeComment=ProductRow['SizeComment-' + cci];
					var SkuFeature=ProductRow['Feature-' + cci];
					var SkuMaterial=ProductRow['Material-' + cci];
					return tr.queryExecute("insert into SkuCulture (SkuId,CultureCode,SkuName,SkuDesc,SkuSizeComment,SkuFeature,SkuMaterial,SkuColor) values(:SkuId,:CultureCode,:SkuName,:SkuDesc,:SkuSizeComment,:SkuFeature,:SkuMaterial,:SkuColor)",{SkuId:ProductRow.SkuId,CultureCode:CultureCode,SkuName:SkuName,SkuDesc:SkuDesc,SkuSizeComment:SkuSizeComment,SkuFeature:SkuFeature,SkuMaterial:SkuMaterial,SkuColor:SkuColor});
				});
				return Q.all(promises);
			});
		}
	})
	.then(function() {
		deferred.resolve(statusMessage);
	})
	.catch(function(err){
		deferred.reject(err);
		// console.dir(err);
	})
	.done();
	return deferred.promise;
};

Factory.getSkuInfo = function (skuid, cultureCode, tr) {

    var deferred = Q.defer();
    var SkuInfo = null;

    Q.when()
        .then(function () {
            var sql = "SELECT s.SkuId, s.StyleCode, s.SkuCode, " +
                    "s.Bar, s.BrandId, s.BadgeId, s.SeasonId, s.SizeId, s.ColorId, " +
                    "s.GeoCountryId, s.LaunchYear, s.PriceRetail, s.PriceSale, s.SaleFrom, " +
                    "s.SaleTo, s.AvailableFrom, s.AvailableTo, s.QtySafetyThreshold, " +
                    "s.WeightKg, s.HeightCm,s.WidthCm,s.LengthCm,s.MerchantId,s.StatusId, " +
                    "scat.CategoryId as PrimaryCategoryId, sz.SizeNameInvariant as SizeName, " +
                    "s.ColorKey, clc.ColorName, scu.SkuColor, scu.SkuName, " +
                    "(SELECT f.ProductImage " +
                    "FROM StyleImage f " +
                    "INNER JOIN ImageType it ON it.ImageTypeCode = f.ImageTypeCode " +
                    "WHERE f.MerchantId = s.MerchantId " +
                    "AND f.StyleCode = s.StyleCode " +
                    "AND f.Position = 1 " +
                    "ORDER BY it.Priority, f.ColorKey LIMIT 1 ) AS ImageDefault " +
                    "FROM Sku s " +
                    "INNER JOIN SkuCulture scu on (s.SkuId=scu.SkuId and scu.CultureCode=:CultureCode) " +
                    "INNER JOIN Brand b on (s.BrandId=b.BrandId) " +
                    "INNER JOIN BrandCulture bc on (b.BrandId=bc.BrandId and bc.CultureCode=:CultureCode) " +
                    "INNER JOIN Size sz on (s.SizeId=sz.SizeId) " +
                    "INNER JOIN Color cl on (s.ColorId=cl.ColorId) " +
                    "INNER JOIN ColorCulture clc on (cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode) " +
                    "INNER JOIN SkuCategory scat on (s.SkuId=scat.SkuId and scat.Priority=0) " +
                    "INNER JOIN Status st on (s.StatusId=st.StatusId) " +
                    "INNER JOIN StatusCulture stc on (st.StatusId=stc.StatusId and stc.CultureCode=:CultureCode) " +
                    "WHERE s.Skuid = :Skuid ";

            return tr.queryOne(sql, {Skuid: skuid, CultureCode:cultureCode}).then(function (data) {
                SkuInfo = data; //store teh returned user
            });
        })
        .then(function () {
            deferred.resolve(SkuInfo);
        })
        .catch(function (err) {
            deferred.reject(err);
        })
        .done();

    return deferred.promise;
};


Factory.badgeList = function(CultureCode, tr){
	var deferred = Q.defer();
	var BadgeList = null;

	Q.when()
	.then(function() {
		return tr.queryMany("select * from Badge bd , BadgeCulture bdc where bd.BadgeId=bdc.BadgeId and bdc.CultureCode=:CultureCode", {CultureCode:CultureCode}).then(function(data) {
			BadgeList=data;
		})
	})
	.then(function() {
		deferred.resolve(BadgeList);
	})
	.catch(function(err){
		deferred.reject(err);
	});
	return deferred.promise;
};


Factory.sizeList = function(CultureCode, tr){
	var deferred = Q.defer();
	var SizeList = null;

	Q.when()
	.then(function() {
		//return tr.queryMany("select * from Size sz , SizeCulture szc where sz.SizeId=szc.SizeId and szc.CultureCode=:CultureCode", {CultureCode:CultureCode}).then(function(data) {
		let sizeSql="select sz.SizeId, sz.SizeCode, szc.SizeName, szc.SizeCultureId, sz.SizeNameInvariant, sg.SizeGroupId, sg.SizeGroupCode, sg.SizeGroupNameInvariant, sgc.SizeGroupName, sgc.CultureCode "
		+ "from Size sz "
		+ "inner join SizeCulture szc on (sz.SizeId=szc.SizeId and szc.CultureCode=:CultureCode) "
		+ "inner join SizeGroup sg on (sz.SizeGroupId=sg.SizeGroupId) "
		+ "inner join SizeGroupCulture sgc on (sg.SizeGroupId=sgc.SizeGroupId and sgc.CultureCode=:CultureCode) ORDER BY sz.Position ASC";
		return tr.queryMany(sizeSql, {CultureCode:CultureCode}).then(function(data) {
			SizeList=data;
		})
	})
	.then(function() {
		deferred.resolve(SizeList);
	})
	.catch(function(err){
		deferred.reject(err);
	});
	return deferred.promise;
};

Factory.sizeGroupList = function(CultureCode, tr){
	var deferred = Q.defer();
	var SizeGroupList = null;

	Q.when()
	.then(function() {
		//return tr.queryMany("select * from Size sz , SizeCulture szc where sz.SizeId=szc.SizeId and szc.CultureCode=:CultureCode", {CultureCode:CultureCode}).then(function(data) {
		let sizeGroupSql="select sg.*, sgc.SizeGroupName from SizeGroup sg  inner join SizeGroupCulture sgc on (sg.SizeGroupId=sgc.SizeGroupId and sgc.CultureCode=:CultureCode)";
		return tr.queryMany(sizeGroupSql, {CultureCode:CultureCode}).then(function(data) {
			SizeGroupList=data;
		})
	})
	.then(function() {
		var promises = SizeGroupList.map(function(iRow){
			return tr.queryMany("select sz.*, szc.SizeName from Size sz inner join SizeCulture szc on (sz.SizeId=szc.SizeId and szc.CultureCode=:CultureCode) and sz.SizeGroupId=:SizeGroupId ORDER BY sz.Position ASC",{SizeGroupId:iRow.SizeGroupId,CultureCode:CultureCode}).then(function(data) {
				iRow.SizeList=data;
			});
		});
		return Q.all(promises);
	})
	.then(function() {
		deferred.resolve(SizeGroupList);
	})
	.catch(function(err){
		deferred.reject(err);
	});
	return deferred.promise;
};



Factory.colorList = function(CultureCode, tr){
	var deferred = Q.defer();
	var ColorList = null;

	Q.when()
	.then(function() {
		return tr.queryMany("select * from Color cl , ColorCulture clc where cl.ColorId=clc.ColorId and clc.CultureCode=:CultureCode ORDER BY cl.Position ASC", {CultureCode:CultureCode}).then(function(data) {
			ColorList=data;
		})
	})
	.then(function() {
		deferred.resolve(ColorList);
	})
	.catch(function(err){
		deferred.reject(err);
	});
	return deferred.promise;
};

//validate product input before create or update product
Factory.validateProductInputBeforeSave = function(tr, style, isStyleCodeChanged, originalStyleCode, isNew){
	//1.1check input
	if(cu.isBlank(style.StyleCode)){
		throw {
			AppCode: 'MSG_ERR_COMPULSORY_FIELD_MISSING'
		};
	}
	if(cu.isBlank(style.SkuNameEN)||cu.isBlank(style.SkuNameCHS)||cu.isBlank(style.SkuNameCHT)){
		throw {
			AppCode: 'MSG_ERR_COMPULSORY_FIELD_MISSING'
		};
	}
	if(cu.isBlank(style.BrandId)){
		throw {
			AppCode: 'MSG_ERR_COMPULSORY_FIELD_MISSING'
		};
	}
	if(cu.isBlank(style.PriceRetail)){
		throw {
			AppCode: 'MSG_ERR_COMPULSORY_FIELD_MISSING'
		};
	}
	if(cu.isBlank(style.PriceRetail)){
		throw {
			AppCode: 'MSG_ERR_COMPULSORY_FIELD_MISSING'
		};
	}
	if(cu.isBlank(style.CategoryPriorityList[0].CategoryId)){//Primary category is mandatory
		throw {
			AppCode: 'MSG_ERR_COMPULSORY_FIELD_MISSING'
		};
	}
	if(style.SkuList.length===0){//At least there should be one sku
		throw {
			AppCode: 'MSG_ERR_SKU_NIL'
		};
	}

	var statusCheckPromise = tr.queryOne('select SkuId from Sku where StyleCode=:StyleCode and StatusId=:StatusId',
		{StyleCode: originalStyleCode || style.StyleCode, StatusId: CONSTANTS.STATUS.DELETED}).then(function (data) {
		if (data !== null) {
			throw {
				AppCode: 'MSG_ERR_PRODUCT_NOT_FOUND'
			};
		}
	});

	var brandCheckPromise = isNew ? tr.queryOne('select BrandId from Brand where BrandId=:BrandId and StatusId<>:StatusId',
		{BrandId: style.BrandId, StatusId: CONSTANTS.STATUS.PENDING}).then(function (data) {
		if (data === null) {
			throw {
				AppCode: 'MSG_ERR_BRAND_STATUS_PENDING'
			};
		}
	}) : Q.resolve(true);

	var tmpSkuCodeArray = [];
	style.SkuList.forEach(function(sku){//Sku code is mandatory
		if(cu.isBlank(sku.SkuCode)){
			throw {
				AppCode: 'MSG_ERR_SKU_SKUCODE_NIL'
			};
		}
		if(tmpSkuCodeArray.indexOf(sku.SkuCode)==-1){
			tmpSkuCodeArray.push(sku.SkuCode);
		}else{
			throw {
				AppCode: 'MSG_ERR_PRODUCT_SKUCODE_UNIQUE_WITHIN_MERCHANT'
			};
		}
	});
	var tmpBarCodeArray = [];
	style.SkuList.forEach(function(sku){
		if(cu.isBlank(sku.Bar)){
			return;
		}
		if(tmpBarCodeArray.indexOf(sku.Bar)==-1){
			tmpBarCodeArray.push(sku.Bar);
		}else{
			throw {
				AppCode: 'MSG_ERR_PRODUCT_BARCODE_UNIQUE_WITHIN_MERCHANT'
			};
		}
	});

	//check if style code is unique.
	var styleCodeCheckPromise;
	if(isStyleCodeChanged){
		//1.2checking if style code is unique;
		var sqlQuery = "SELECT COUNT(SkuId) totalAmount FROM Sku WHERE StyleCode=:StyleCode AND MerchantId=:MerchantId";
		styleCodeCheckPromise = tr.queryMany(sqlQuery, {
                StyleCode: style.StyleCode,
                MerchantId: style.MerchantId
		}).then(function(data) {
                if (data[0].totalAmount > 0) {
                        throw {
                                AppCode: 'MSG_ERR_PRODUCT_STYLECODE_UNIQUE'
                        };
                }
        });
	}
        
	//check if product name is unique.
	var productNamePromise;
	var productNameUniquenessSqlQuery = "select count(*) totalRecord from Sku s "+
		"inner join SkuCulture sc on(s.SkuId=sc.SkuId AND s.MerchantId=:MerchantId) "+
		"where StyleCode<>:StyleCode "+
		"and ((sc.CultureCode='EN' and sc.SkuName=:SkuNameEN) "+
		"or (sc.CultureCode='CHS' and sc.SkuName=:SkuNameCHS) "+
		"or (sc.CultureCode='CHT' and sc.SkuName=:SkuNameCHT))";
	productNamePromise = tr.queryMany(productNameUniquenessSqlQuery, {
		StyleCode: originalStyleCode || style.StyleCode,
		SkuNameEN: style.SkuNameEN,
		SkuNameCHS: style.SkuNameCHS,
		SkuNameCHT: style.SkuNameCHT,
		MerchantId: style.MerchantId
	}).then(function(data) {
		if (data[0].totalRecord > 0) {
			throw {
				AppCode: 'MSG_ERR_PROD_NAME_DUPLICATE'
			};
		}
	});


	//check if skucode is unique within merchant.
	var skuCodeCheckPromises = style.SkuList.map(function(sku){//Sku code is mandatory
		var sqlQuery = "SELECT COUNT(SkuId) totalAmount FROM Sku WHERE SkuCode=:SkuCode and MerchantId=:MerchantId and SkuId!=:SkuId";
		if(cu.isBlank(sku.SkuId)){
			sqlQuery = "SELECT COUNT(SkuId) totalAmount FROM Sku WHERE SkuCode=:SkuCode and MerchantId=:MerchantId";
		}
		return tr.queryMany(sqlQuery, {
			SkuCode: sku.SkuCode,
			MerchantId: style.MerchantId,
			SkuId: sku.SkuId
		}).then(function(data){
			if (data[0].totalAmount > 0) {
				throw {
					AppCode: 'MSG_ERR_PRODUCT_SKUCODE_UNIQUE_WITHIN_MERCHANT'
				};
			}
		});
	});

	//check if barcode is unique within merchant.
	var barCodeCheckPromises = style.SkuList.map(function(sku){//bar code is mandatory
		if(cu.isBlank(sku.Bar)){
			return;
		}
		var sqlQuery = "SELECT COUNT(SkuId) totalAmount FROM Sku WHERE Bar=:Bar and MerchantId=:MerchantId and SkuId!=:SkuId";
		if(cu.isBlank(sku.SkuId)){
			sqlQuery = "SELECT COUNT(SkuId) totalAmount FROM Sku WHERE Bar=:Bar and MerchantId=:MerchantId";
		}
		return tr.queryMany(sqlQuery, {
			Bar: sku.Bar,
			MerchantId: style.MerchantId,
			SkuId: sku.SkuId
		}).then(function(data){
			console.log(data[0].totalAmount > 0);
			if (data[0].totalAmount > 0) {
				throw {
					AppCode: 'MSG_ERR_PRODUCT_BARCODE_UNIQUE_WITHIN_MERCHANT'
				};
			}
		});
	});

	var promises = [];
	promises = promises.concat(statusCheckPromise, brandCheckPromise, styleCodeCheckPromise, productNamePromise, skuCodeCheckPromises, barCodeCheckPromises);
	console.log(promises);
	return Q.all(promises);
};

//setting default value for creating or update product
Factory.settingProductDefaultValue = function(style){
	//check and set default value for SeasonId
	if(cu.isBlank(style.SeasonId)){
		style.SeasonId=0
	}
	//check and set default value for BadgeId
	if(cu.isBlank(style.BadgeId)){
		style.BadgeId=0
	}
	//check and set default value for GeoCountry
	if(cu.isBlank(style.GeoCountryId)){
		style.GeoCountryId=0
	}
	//check and set default value for LaunchYear
	if(cu.isBlank(style.LaunchYear)){
		style.LaunchYear=0
	}
	//check and set default value for PriceSale
	if(cu.isBlank(style.PriceSale)){
		style.PriceSale=0
	}
	//check and set default value for WeightKg
	if(cu.isBlank(style.WeightKg)){
		style.WeightKg=0
	}
	//check and set default value for WeightKg
	if(cu.isBlank(style.HeightCm)){
		style.HeightCm=0
	}
	//check and set default value for WeightKg
	if(cu.isBlank(style.WidthCm)){
		style.WidthCm=0
	}
	//check and set default value for WeightKg
	if(cu.isBlank(style.LengthCm)){
		style.LengthCm=0
	}
	//check and set default value for IsFeatured
	if(cu.isBlank(style.IsFeatured)){
		style.IsFeatured=0
	}
};

module.exports=Factory;
