"use strict";
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');

var Factory = {};

// Factory to return a list of Users who are following a specific User, by UserKey 
Factory.getUserFollowing = function(tr, Bale){
	var sql = "SELECT User.FollowerCount,User.IsCuratorUser,User.UserKey,User.FirstName,User.LastName,User.UserId,User.UserName,User.ProfileImage,User.DisplayName FROM UserUserRelationship INNER JOIN User ON User.UserId = UserUserRelationship.FromUserId AND User.StatusId=2 WHERE UserUserRelationship.ToUserId=:UserId AND UserUserRelationship.StatusId = 2 AND UserUserRelationship.RelationshipTypeId = 1" 

	if(Bale.hasOwnProperty('IsCuratorUser')){
		sql += " AND IsCuratorUser = :IsCuratorUser ";
	}

	sql += " ORDER BY UserUserRelationship.LastModified DESC LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, Bale);
};

// Factory to return a list of Users who a specific User is following.
Factory.getUserFollowed = function(tr, Bale){
	var sql = "SELECT User.FollowerCount,User.IsCuratorUser,User.UserKey,User.FirstName,User.LastName,User.UserId,User.UserName,User.ProfileImage,User.DisplayName FROM UserUserRelationship INNER JOIN User ON User.UserId = UserUserRelationship.ToUserId AND User.StatusId=2 WHERE UserUserRelationship.FromUserId=:UserId AND UserUserRelationship.StatusId = 2 AND UserUserRelationship.RelationshipTypeId = 1 "

	if(Bale.hasOwnProperty('IsCuratorUser')){
		sql += " AND IsCuratorUser = :IsCuratorUser ";
	}
	
	sql += " ORDER BY UserUserRelationship.LastModified DESC LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, Bale);
};

Factory.getMerchantFollowing = function(tr, Bale){
	var sql = "SELECT User.FollowerCount,User.IsCuratorUser,User.UserKey,User.FirstName,User.LastName,User.UserId,User.UserName,User.ProfileImage,User.DisplayName FROM UserMerchantRelationship INNER JOIN User ON User.UserId = UserMerchantRelationship.FromUserId AND User.StatusId=2 WHERE UserMerchantRelationship.ToMerchantId=:MerchantId ORDER BY UserMerchantRelationship.LastModified DESC LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, Bale);
};

Factory.getMerchantFollowed = function(tr, Bale){
	var sql = "SELECT Merchant.FollowerCount,Merchant.MerchantId,Merchant.MerchantNameInvariant,Merchant.SmallLogoImage,Merchant.LargeLogoImage,MerchantCulture.MerchantName FROM UserMerchantRelationship LEFT JOIN MerchantCulture ON MerchantCulture.MerchantId=UserMerchantRelationship.ToMerchantId AND MerchantCulture.CultureCode=:cc INNER JOIN Merchant ON Merchant.MerchantId=UserMerchantRelationship.ToMerchantId AND Merchant.StatusId=2 WHERE UserMerchantRelationship.FromUserId=:UserId ORDER BY UserMerchantRelationship.LastModified DESC LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, Bale);
};

Factory.getBrandFollowing = function(tr, Bale){
	var sql = "SELECT User.FollowerCount,User.IsCuratorUser,User.UserKey,User.FirstName,User.LastName,User.UserId,User.UserName,User.ProfileImage,User.DisplayName FROM UserBrandRelationship INNER JOIN User ON User.UserId = UserBrandRelationship.FromUserId AND User.StatusId=2 WHERE UserBrandRelationship.ToBrandId=:BrandId ORDER BY UserBrandRelationship.LastModified DESC LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, Bale);
};

Factory.getBrandFollowed = function(tr, Bale){
	var sql = "SELECT Brand.FollowerCount,Brand.BrandId,Brand.BrandNameInvariant,Brand.SmallLogoImage,Brand.LargeLogoImage,BrandCulture.BrandName FROM UserBrandRelationship LEFT JOIN BrandCulture ON BrandCulture.BrandId=UserBrandRelationship.ToBrandId AND BrandCulture.CultureCode=:cc INNER JOIN Brand ON Brand.BrandId=UserBrandRelationship.ToBrandId AND Brand.StatusId=2 WHERE UserBrandRelationship.FromUserId=:UserId ORDER BY UserBrandRelationship.LastModified DESC LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, Bale);
};

// Factory to Follow User Api
Factory.saveUser = function(tr, Bale){
	var Relationship = null;
	var affectedRows = 0;

	return Q.when()
	.then(function(){
		var sql = "SELECT * FROM UserUserRelationship WHERE FromUserId=:UserId AND ToUserId=:ToUserId AND RelationshipTypeId=1";
	  return tr.queryOne(sql, Bale).then(function(data){
	  	Relationship = data;
	  });
	})
	.then(function(){
		// insert if not exists
		if(!Relationship){
      var sql = 'INSERT INTO UserUserRelationship (FromUserId,ToUserId,RelationshipTypeId,StatusId) VALUES (:UserId,:ToUserId,1,2)';
      return tr.queryExecute(sql, Bale).then(function(data){
      	affectedRows = parseInt(data.affectedRows) || 0;
				console.log("affectedRows saveUser: ", affectedRows);
      });
    }
	})
	.then(function(){
		// increment the FollowingUserCount of the current User who issued the saveUser logic.
		if(affectedRows > 0 && Bale.IsCuratorUser == 0){
			var sql = "UPDATE User SET FollowingUserCount = FollowingUserCount + 1 WHERE UserId=:UserId";
			return tr.queryExecute(sql, Bale);
		}
	})
	.then(function(){
		// increment the FollowingCuratorCount of the current User who issued the saveUser logic.
		if(affectedRows > 0 && Bale.IsCuratorUser == 1){
			var sql = "UPDATE User SET FollowingCuratorCount = FollowingCuratorCount + 1 WHERE UserId=:UserId";
			return tr.queryExecute(sql, Bale);
		}
	})
	.then(function(){
		// increment the FollowerCount of the current User being followed.
		if(affectedRows > 0){
			var sql = "UPDATE User SET FollowerCount = FollowerCount + 1 WHERE UserId=:ToUserId";
			return tr.queryExecute(sql, Bale);
		}
	})
};

// Factory to Follow Merchant Api
Factory.saveMerchant = function(tr, Bale){
	var Relationship = null;
	var affectedRows = 0;

	return Q.when()
	.then(function(){
		// Check if Merchant Exist.
		var sql = "SELECT * FROM Merchant WHERE MerchantId=:ToMerchantId";
		return tr.queryOne(sql, Bale).then(function(data){
			if(!data){
				throw { AppCode : 'MSG_ERR_MERCHANT_NOT_FOUND' };
			}
		});
	})
	.then(function(){
		var sql = "SELECT * FROM UserMerchantRelationship WHERE FromUserId=:UserId AND ToMerchantId=:ToMerchantId";
		return tr.queryOne(sql, Bale).then(function(data){
			Relationship = data;
		});
	})
	.then(function(){
		// insert if not exists
    if(!Relationship){
      var sql = 'INSERT INTO UserMerchantRelationship (FromUserId,ToMerchantId) VALUES (:UserId,:ToMerchantId)';
      return tr.queryExecute(sql, Bale).then(function(data){
      	affectedRows = parseInt(data.affectedRows) || 0;
				console.log("affectedRows saveMerchant: ", affectedRows);
      });
    }
	})
	.then(function(){
		// increment the FollowingMerchantCount of the current User who issued the saveMerchant logic.
		if(affectedRows > 0){
			var sql = "UPDATE User SET FollowingMerchantCount = FollowingMerchantCount + 1 WHERE UserId=:UserId";
			return tr.queryExecute(sql, Bale);
		}
	})
	.then(function(){
		// increment the FollowerCount of the current Merchant being followed.
		if(affectedRows > 0){
			var sql = "UPDATE Merchant SET FollowerCount = FollowerCount + 1 WHERE MerchantId=:ToMerchantId";
			return tr.queryExecute(sql, Bale);
		}
	})
};

// Factory to Follow Brand Api
Factory.saveBrand = function(tr, Bale){
	var Relationship = null;
	var affectedRows = 0;

	return Q.when()
	.then(function(){
		// Check if Brand Exist.
		var sql = "SELECT * FROM Brand WHERE BrandId=:ToBrandId";
		return tr.queryOne(sql, Bale).then(function(data){
			if(!data || cu.checkIdBlank(data.BrandId)){
				throw { AppCode : 'MSG_ERR_BRAND_NOT_FOUND' };
			}
		});
	})
	.then(function(){
		var sql = "SELECT * FROM UserBrandRelationship WHERE FromUserId=:UserId AND ToBrandId=:ToBrandId";
		return tr.queryOne(sql, Bale).then(function(data){
			Relationship = data;
		});
	})
	.then(function(){
		// insert if not exists
		if(!Relationship){
      var sql = 'INSERT INTO UserBrandRelationship (FromUserId,ToBrandId) VALUES (:UserId,:ToBrandId)';
      return tr.queryExecute(sql, Bale).then(function(data){
      	affectedRows = parseInt(data.affectedRows) || 0;
				console.log("affectedRows saveBrand: ", affectedRows);
      });
		}
	})
	.then(function(){
		// increment the FollowingBrandCount of the current User who issued the saveBrand logic.
		if(affectedRows > 0){
			var sql = "UPDATE User SET FollowingBrandCount = FollowingBrandCount + 1 WHERE UserId=:UserId";
			return tr.queryExecute(sql, Bale);
		}
	})
	.then(function(){
		// increment the FollowerCount of the current Brand being followed.
		if(affectedRows > 0){
			var sql = "UPDATE Brand SET FollowerCount = FollowerCount + 1 WHERE BrandId=:ToBrandId";
			return tr.queryExecute(sql, Bale);
		}
	})
};

Factory.deleteUser = function(tr, Bale){
	var affectedRows = 0;
	return Q.when()
	.then(function(){
		var sql = 'DELETE FROM UserUserRelationship WHERE FromUserId=:UserId AND ToUserId=:ToUserId AND RelationshipTypeId=1';
		return tr.queryExecute(sql, Bale).then(function(data){
			affectedRows = parseInt(data.affectedRows) || 0;
			console.log("affectedRows deleteUser: ", affectedRows);
		});
	})
	.then(function(){
		// decrement the FollowingUserCount of the current User who issued the deleteUser logic.
		if(affectedRows > 0 && Bale.IsCuratorUser == 0){
			var sql = "UPDATE User SET FollowingUserCount = FollowingUserCount - 1 WHERE UserId=:UserId AND FollowingUserCount > 0";
			return tr.queryExecute(sql, Bale);
		}
	})
	.then(function(){
		// decrement the FollowingCuratorCount of the current User who issued the deleteUser logic.
		if(affectedRows > 0 && Bale.IsCuratorUser == 1){
			var sql = "UPDATE User SET FollowingCuratorCount = FollowingCuratorCount - 1 WHERE UserId=:UserId AND FollowingCuratorCount > 0";
			return tr.queryExecute(sql, Bale);
		}
	})
	.then(function(){
		// decrement the FollowerCount of the current User being followed.
		if(affectedRows > 0){
			var sql = "UPDATE User SET FollowerCount = FollowerCount - 1 WHERE UserId=:ToUserId AND FollowerCount > 0";
			return tr.queryExecute(sql, Bale);
		}
	})
};

Factory.deleteMerchant = function(tr, Bale){
	var affectedRows = 0;
	return Q.when()
	.then(function(){
		var sql = 'DELETE FROM UserMerchantRelationship WHERE FromUserId=:UserId AND ToMerchantId=:ToMerchantId';
		return tr.queryExecute(sql, Bale).then(function(data){
			affectedRows = parseInt(data.affectedRows) || 0;
			console.log("affectedRows deleteMerchant: ", affectedRows);
		});
	})
	.then(function(){
		// decrement the FollowingMerchantCount of the current User who issued the deleteMerchant logic.
		if(affectedRows > 0){
			var sql = "UPDATE User SET FollowingMerchantCount = FollowingMerchantCount - 1 WHERE UserId=:UserId AND FollowingMerchantCount > 0";
			return tr.queryExecute(sql, Bale);
		}
	})
	.then(function(){
		// decrement the FollowerCount of the current Merchant being followed.
		if(affectedRows > 0){
			var sql = "UPDATE Merchant SET FollowerCount = FollowerCount - 1 WHERE MerchantId=:ToMerchantId AND FollowerCount > 0";
			return tr.queryExecute(sql, Bale);
		}
	})
};

Factory.deleteBrand = function(tr, Bale){
	var affectedRows = 0;
	return Q.when()
	.then(function(){
		var sql = 'DELETE FROM UserBrandRelationship WHERE FromUserId=:UserId AND ToBrandId=:ToBrandId';
		return tr.queryExecute(sql, Bale).then(function(data){
			affectedRows = parseInt(data.affectedRows) || 0;
			console.log("affectedRows deleteBrand: ", affectedRows);
		});
	})
	.then(function(){
		// decrement the FollowingBrandCount of the current User who issued the deleteBrand logic.
		if(affectedRows > 0){
			var sql = "UPDATE User SET FollowingBrandCount = FollowingBrandCount - 1 WHERE UserId=:UserId AND FollowingBrandCount > 0";
			return tr.queryExecute(sql, Bale);
		}
	})
	.then(function(){
		// decrement the FollowerCount of the current Brand being followed.
		if(affectedRows > 0){
			var sql = "UPDATE Brand SET FollowerCount = FollowerCount - 1 WHERE BrandId=:ToBrandId AND FollowerCount > 0";
			return tr.queryExecute(sql, Bale);
		}
	})
};


module.exports = Factory;
