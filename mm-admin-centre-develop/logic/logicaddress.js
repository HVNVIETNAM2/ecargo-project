"use strict";
var CONSTANTS = require('../logic/constants');

var Q = require('q');
var _ = require('lodash');
var config = require('../config');
var cu = require('../lib/commonutil.js');
var cm = require('../lib/commonmariasql.js');
var lu = require('../logic/logicuser.js');

var ADDRESS_BASE_SQL = "SELECT UserAddress.*, (User.DefaultUserAddressId = UserAddressId) IsDefault FROM UserAddress INNER JOIN User ON User.UserId = UserAddress.UserId WHERE UserAddress.UserId = :UserId";

exports.view = function (tr, UserId, Key, KeyCol) {
  return tr.queryOne(ADDRESS_BASE_SQL+" AND "+(KeyCol || "UserAddressKey")+" = :Key", {
    UserId: UserId,
    Key: Key
  }).then(function (data) {
    if (!data) throw { AppCode: "MSG_ERR_USER_ADDRESS_NOT_EXISTS" };
    // hide ids
    delete data.UserId;
    delete data.UserAddressId;
    return data;
  });
};

exports.viewDefault = function (tr, UserId) {
  return tr.queryOne(ADDRESS_BASE_SQL + " ORDER BY IsDefault DESC, LastCreated DESC", {
    UserId: UserId
  }).then(function (data) {
    if (!data) throw { AppCode: "MSG_ERR_USER_ADDRESS_EMPTY" };
    // hide ids
    delete data.UserId;
    delete data.UserAddressId;
    return data;
  });
}

exports.list = function (tr, UserId) {
  return tr.queryMany(ADDRESS_BASE_SQL, { UserId: UserId });
};

exports.delete = function (tr, UserId, Key) {
  return tr.queryExecute("DELETE FROM UserAddress WHERE UserId=:UserId AND UserAddressKey = :Key", {
    UserId: UserId,
    Key: Key
  }).then(function (data) {
    if (parseInt(data.affectedRows) !== 1) throw { AppCode: "MSG_ERR_USER_ADDRESS_NOT_EXISTS" };
    return true;
  })
};

exports.save = function (tr, Bale) {
  var isInsert = cu.checkIdBlank(Bale.UserAddressKey);
  var UserAddressId;

  return Q.when()
  .then(function () {
    // get Country name
    return tr.queryOne("SELECT GeoCountry.*, IFNULL(GeoCountryName, GeoCountryNameInvariant) AS GeoCountryName FROM GeoCountry LEFT JOIN GeoCountryCulture ON GeoCountry.GeoCountryId = GeoCountryCulture.GeoCountryId AND CultureCode = :CultureCode WHERE GeoCountry.GeoCountryId = :GeoCountryId", Bale).then(function (data) {
      if (!data) throw { AppCode: "MSG_ERR_GEO_NOT_EXISTS" };
      Bale.Country = data.GeoCountryName;
    })
  })
  .then(function () {
    // get Province, City name
    // todo CultureCode = zh?
    return tr.queryMany("SELECT Geo.*, IFNULL(GeoName, GeoNameInvariant) AS GeoName FROM Geo LEFT JOIN GeoCulture ON Geo.GeoId = GeoCulture.GeoId AND CultureCode = 'zh' WHERE Geo.GeoId = :GeoProvinceId OR Geo.GeoId = :GeoCityId", Bale).then(function (list) {
      if (list.length !== 2) throw { AppCode: "MSG_ERR_GEO_NOT_EXISTS" };

      list.forEach(function (data) {
        if (data.GeoId == Bale.GeoProvinceId) {
          Bale.Province = data.GeoName;
        }
        if (data.GeoId == Bale.GeoCityId) {
          Bale.City = data.GeoName;
        }
      });
    })
  })
  .then(function () {
    if (isInsert) {
      return tr.queryExecute("INSERT INTO UserAddress (UserAddressKey, UserId, RecipientName, PhoneCode, PhoneNumber, GeoCountryId, GeoProvinceId, GeoCityId, Country, Province, City, District, PostalCode, Address, CultureCode) VALUES (uuid(), :UserId, :RecipientName, :PhoneCode, :PhoneNumber, :GeoCountryId, :GeoProvinceId, :GeoCityId, :Country, :Province, :City, :District, :PostalCode, :Address, :CultureCode)", Bale).then(function (data) {
        UserAddressId = data.insertId;
        if (Bale.IsDefault) {
          // update default
          return tr.queryExecute("update User set DefaultUserAddressId = :UserAddressId where UserId = :UserId", {
            UserId: Bale.UserId,
            UserAddressId: UserAddressId
          });
        }
      });
    } else {
      // check existing UserAddressKey then update
      return exports.view(tr, Bale.UserId, Bale.UserAddressKey)
      .then(function (data) {
        Bale.UserAddressId = data.UserAddressId;
        return tr.queryExecute(
          "UPDATE UserAddress SET RecipientName=:RecipientName, PhoneCode=:PhoneCode, PhoneNumber=:PhoneNumber, GeoCountryId=:GeoCountryId, GeoProvinceId=:GeoProvinceId, GeoCityId=:GeoCityId, Country=:Country, Province=:Province, City=:City, District=:District, PostalCode=:PostalCode, Address=:Address, CultureCode=:CultureCode WHERE UserId=:UserId AND UserAddressId=:UserAddressId", 
          Bale
        );
      });
    }
  })
  .then(function () {
    // get UserAddress object
    if (UserAddressId) {
      return exports.view(tr, Bale.UserId, UserAddressId, 'UserAddressId'); // by id
    } else {
      return exports.view(tr, Bale.UserId, Bale.UserAddressKey); // by key uuid
    }
  });
};

exports.saveDefault = function (tr, UserId, Key) {
  return tr.queryExecute("UPDATE User INNER JOIN UserAddress ON User.UserId = UserAddress.UserId SET DefaultUserAddressId = UserAddressId WHERE User.UserId = :UserId AND UserAddressKey = :Key", {
    UserId: UserId,
    Key: Key
  });
}
