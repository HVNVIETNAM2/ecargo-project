"use strict";
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');

var Factory = {};

Factory.getFindUser = function(tr, Bale){
	var sql = "SELECT DisplayName, UserName, UserKey, FirstName, LastName, IsCuratorUser, ProfileImage from User WHERE LOWER(FirstName) LIKE LOWER(CONCAT('%',:s,'%')) OR LOWER(LastName) LIKE LOWER(CONCAT('%',:s,'%')) OR LOWER(UserName) LIKE LOWER(CONCAT('%',:s,'%')) OR LOWER(DisplayName) LIKE LOWER(CONCAT('%',:s,'%')) LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, Bale);
};

Factory.getUserFriend = function(tr, Bale){
	var sql = "SELECT User.FriendCount,User.UserKey,User.FirstName,User.LastName,User.UserId,User.UserName,User.ProfileImage,User.DisplayName,User.IsCuratorUser FROM UserUserRelationship INNER JOIN User ON User.UserId = UserUserRelationship.ToUserId AND User.StatusId=2 WHERE UserUserRelationship.FromUserId=:UserId AND UserUserRelationship.RelationshipTypeId=2 AND UserUserRelationship.StatusId=2 ORDER BY UserUserRelationship.LastModified DESC LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, Bale);
};

Factory.getUserFriendRequest = function(tr, Bale){
	var sql = "SELECT User.FriendCount,User.UserKey,User.FirstName,User.LastName,User.UserId,User.UserName,User.ProfileImage,User.DisplayName,User.IsCuratorUser FROM UserUserRelationship INNER JOIN User ON User.UserId = UserUserRelationship.FromUserId AND User.StatusId=2 WHERE UserUserRelationship.ToUserId=:UserId AND UserUserRelationship.RelationshipTypeId=2 AND UserUserRelationship.StatusId=3 ORDER BY UserUserRelationship.LastModified DESC LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, Bale);
};

Factory.getUserFriendReceive = function(tr, Bale){
	var sql = "SELECT User.FriendCount,User.UserKey,User.FirstName,User.LastName,User.UserId,User.UserName,User.ProfileImage,User.DisplayName,User.IsCuratorUser FROM UserUserRelationship INNER JOIN User ON User.UserId = UserUserRelationship.FromUserId AND User.StatusId=2 WHERE UserUserRelationship.ToUserId=:UserId AND UserUserRelationship.RelationshipTypeId=2 AND UserUserRelationship.StatusId=3 ORDER BY UserUserRelationship.LastModified DESC LIMIT "+Bale.start+","+Bale.limit;
	return tr.queryMany(sql, Bale);
};

Factory.saveFriend = function(tr, Bale){
	return Q.when()
	.then(function(){
		var sql = "SELECT * FROM UserUserRelationship WHERE ((FromUserId=:UserId AND ToUserId=:ToUserId) OR (FromUserId=:ToUserId AND ToUserId=:UserId)) AND RelationshipTypeId=2";
		return tr.queryOne(sql, Bale).then(function(data){
			if(data){
				throw { AppCode : 'MSG_ERR_CA_FRIEND_REQUEST_ALREADY_EXIST' };
			}
		});
	})
	.then(function(){
		var sql = 'INSERT INTO UserUserRelationship (FromUserId,ToUserId,RelationshipTypeId,StatusId) VALUES (:UserId,:ToUserId,2,3)';
		return tr.queryExecute(sql, Bale);
	})
};

Factory.deleteFriend = function(tr, Bale){
	var sql = 'DELETE FROM UserUserRelationship WHERE ((FromUserId=:UserId AND ToUserId=:ToUserId) OR (FromUserId=:ToUserId AND ToUserId=:UserId)) AND RelationshipTypeId=2';
	return tr.queryExecute(sql, Bale).then(function(data){
		if(parseInt(data.affectedRows) === 2){
			var sql = "UPDATE User SET FriendCount = FriendCount - 1 WHERE ( UserId=:UserId AND FriendCount > 0 ) OR ( UserId=:ToUserId AND FriendCount > 0 )";
			return tr.queryExecute(sql, Bale);
		}
	});
};

Factory.acceptFriend = function(tr, Bale){
	var FriendRequest = [];

	return Q.when()
	.then(function(){
		var sql = "SELECT * FROM UserUserRelationship WHERE ((FromUserId=:UserId AND ToUserId=:ToUserId) OR (FromUserId=:ToUserId AND ToUserId=:UserId)) AND RelationshipTypeId=2 AND StatusId=3";
		return tr.queryOne(sql, Bale).then(function(data){
			if(data && data.StatusId == 3){
				FriendRequest = data;
			}else{
				throw { AppCode : 'MSG_ERR_FRIEND_REQUEST_NOT_FOUND' };
			}
		});
	})
	.then(function(){
		var sql = "UPDATE UserUserRelationship SET StatusId=2 WHERE UserUserRelationshipId=:UserUserRelationshipId";
		return tr.queryExecute(sql, FriendRequest).then(function(data){
			if(parseInt(data.affectedRows) > 0){
				var sql = "UPDATE User SET FriendCount = FriendCount + 1 WHERE UserId=:UserId";
				return tr.queryExecute(sql, Bale);
			}
		});
	})
	.then(function(){
		var sql = "INSERT INTO UserUserRelationship (FromUserId,ToUserId,RelationshipTypeId,StatusId) VALUES (:ToUserId,:FromUserId,2,2)";
		return tr.queryExecute(sql, FriendRequest).then(function(data){
			if(parseInt(data.affectedRows) > 0){
				var sql = "UPDATE User SET FriendCount = FriendCount + 1 WHERE UserId=:ToUserId";
				return tr.queryExecute(sql, Bale);
			}
		});
	})
};

module.exports = Factory;
