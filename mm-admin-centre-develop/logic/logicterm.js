"use strict";
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var config = require('../config');
var cm = require('../lib/commonmariasql.js');
var cu = require('../lib/commonutil.js');

var Factory = {};

Factory.listAll = function(tr){
	var deferred = Q.defer();
	var SearchList = null;

	Q.when()
	.then(function(){
		var sql = "SELECT * FROM SearchTerm ORDER BY SearchTermTypeCode,Priority DESC";

		return tr.queryMany(sql, { }).then(function(data) {
			SearchList = data; 
		});
	})
	.then(function(){
		deferred.resolve(SearchList);
	})
	.catch(function(err){
		deferred.reject(err);
	})

	return deferred.promise;
};

Factory.list = function(tr, SearchTermTypeCode){
	var deferred = Q.defer();
	var SearchList = null;

	Q.when()
	.then(function(){
		var sql = "SELECT * FROM SearchTerm WHERE SearchTermTypeCode = :SearchTermTypeCode ORDER BY Priority DESC";

		return tr.queryMany(sql, { SearchTermTypeCode : SearchTermTypeCode }).then(function(data) {
			SearchList = data; 
		});
	})
	.then(function(){
		deferred.resolve(SearchList);
	})
	.catch(function(err){
		deferred.reject(err);
	})

	return deferred.promise;
};

Factory.insert = function(tr, Bale){
	var deferred = Q.defer();

	Q.when()
	.then(function(){
		var sql = "INSERT INTO SearchTerm (SearchTermTypeCode,SearchTerm,SearchTermIn,Priority) VALUES (:SearchTermTypeCode,:SearchTerm,:SearchTermIn,:Priority)";

		return tr.queryExecute(sql, Bale).then(function(data){
			Bale = data; 
		});
	})
	.then(function(){
		deferred.resolve(Bale);
	})
	.catch(function(err){
		deferred.reject(err);
	})

	return deferred.promise;
};

Factory.update = function(tr, Bale){
	var deferred = Q.defer();

	Q.when()
	.then(function(){
		var sql = "UPDATE SearchTerm SET Priority = :Priority, SearchTerm = :SearchTerm, SearchTermIn = :SearchTermIn WHERE SearchTermId = :SearchTermId";

		return tr.queryExecute(sql, Bale).then(function(data){
			Bale = data; 
		});
	})
	.then(function(){
		deferred.resolve(Bale);
	})
	.catch(function(err){
		deferred.reject(err);
	})

	return deferred.promise;
};

Factory.delete = function(tr, Bale){
	var deferred = Q.defer();

	Q.when()
	.then(function(){
		var sql = "DELETE FROM SearchTerm WHERE SearchTermId = :SearchTermId";

		return tr.queryExecute(sql, Bale).then(function(data){
			Bale = data; 
		});
	})
	.then(function(){
		deferred.resolve(Bale);
	})
	.catch(function(err){
		deferred.reject(err);
	})

	return deferred.promise;
};

module.exports = Factory;