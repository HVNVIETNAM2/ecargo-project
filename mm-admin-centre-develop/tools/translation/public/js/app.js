"use strict";
var app = angular.module('mmtranslation', ['ngFileUpload']);

app.config(['$httpProvider', function($httpProvider){
	$httpProvider.interceptors.push('httpInterceptor');
}]);

app.run(['$rootScope', '$window', function($rootScope, $window){
	$rootScope.messages = [];

	$rootScope.removeMessage = function(msg){
		_.pull($rootScope.messages, msg);
	};
}]);

app.factory('translationsFactory', [ '$http', '$q', '$rootScope',
	function($http, $q, $rootScope){
		var tmpArr = [];
		var o = { data : [] };

		o.getData = function(reload){
			if(o.data.length > 0 && !reload){
				var deferred = $q.defer();
	            deferred.resolve(o.data);
	            return deferred.promise;
			}else{
				return $http.get('/translations/list').then(function(response){
					_.map(response.data, function(item){
						if(tmpArr.indexOf(item.TranslationCode) > -1){
							o.data[tmpArr.indexOf(item.TranslationCode)][item.CultureCode] = item.TranslationName;
						}else{
							tmpArr.push(item.TranslationCode)
							var tmpData = { TranslationCode : item.TranslationCode };
							tmpData[item.CultureCode] = item.TranslationName;
							o.data.push(tmpData);
						}
					})

					return o.data;
				});
			}
		};

		o.remove = function(item){
			return $http.post('/translations/remove', item).then(function(response){
				if(response.status == 200){
					$rootScope.messages.push({ type : "success", message : "Successfully deleted translation.", data : item });
					_.pull(o.data, item);
				}
			}, function(err){
				$rootScope.messages.push({ type : "danger", message : "Error while deleting translation from database.", data : item });
			});
		};

		o.save = function(item){
			return $http.post('/translations/save', item).then(function(response){
				if(response.status == 200){
					$rootScope.messages.push({ type : "success", message : "Successfully save translation.", data : item });
					var selected = _.find(o.data, { TranslationCode : item.TranslationCode });
					if(!selected){
						o.data.push(item);
					}else{
						_.pull(o.data, selected);
						o.data.push(item);
					}
				}
			}, function(err){
				$rootScope.messages.push({ type : "danger", message : "Error while saving translation to database.", data : item });
			});
		};

		o.export = function(codes){
			return $http.post('/translations/export', { code : codes }).then(function(response){
				if(response.status == 200){
					var file = response.data.file;
					window.location = file;
				}
			}, function(err){
				$rootScope.messages.push({ type : "danger", message : "Error while exporting data." });
			})
		};

		o.exportXLSX = function(){
			return $http.post('/translations/exportXLSX', { }).then(function(response){
				if(response.status == 200){
					var fileLocation = "/exports/" + response.data.fileName;
					window.location = fileLocation;
				}
			}, function(err){
				$rootScope.messages.push({ type : "danger", message : "Error while exporting excel data." });
			})
		};

		return o;
	}
]);

app.filter('offset', function() {
  	return function(input, start) {
    	start = parseInt(start, 10);
    	return input.slice(start);
  	};
});

app.factory('httpInterceptor', [
	'$rootScope', '$q', '$window',
	function($rootScope, $q, $window){
		return {
			request : function(config){
				var proxyDir = $('[data-proxy]').data('proxy');

				if(proxyDir != ""){
					config.url = proxyDir + config.url;
				}
				config.headers = config.headers || {};
				if($window.localStorage['mm-translation-key']){
					config.headers.Authorization = 'Bearer ' + $window.localStorage['mm-translation-key'];
				}

				return config;
			},
			responseError : function(error){
				if(error.status >= 400){
			    	$rootScope.messages.push({ type : "danger", message : error.data });
				}

				return error;
		    }
		}
	}
]);

app.controller('MainCtrl', [ '$http', '$scope', 'translationsFactory', '$window', '$rootScope', 'Upload',
	function($http, $scope, translationsFactory, $window, $rootScope, Upload){
		$scope.loginUser = $window.localStorage['mm-translation-key'];
		// For Login
		$scope.user = {};
		$scope.login = function(){
			$http.post('/login', $scope.user).then(function(response){
				if(response.status == 200){
					$window.localStorage['mm-translation-key'] = response.data.token;
					$window.location.reload();
				}
			});
		};

		$scope.logout = function(){
			$scope.loginUser = false;
			$window.localStorage.removeItem('mm-translation-key');
		};

		// Start Translation
		$scope.allData = [];
		$scope.itemsPerPage = 25;
		$scope.currentPage = 0;
		$scope.sortBy = 'TranslationCode';
		$scope.reverse = false;
		$scope.item = null;
		$scope.update = false;
		$scope.checkData = [];
		$scope.exportAll = true;
		$scope.tableMessage = "Fetching translation data.";
		$scope.isLoading = true;

		if($scope.loginUser){
			translationsFactory.getData().then(function(data){
				$scope.allData = data;
				$scope.tableMessage = "No translation available.";
				$scope.isLoading = false;
			});

			$scope.addTranslation = function(){
				$scope.item = null;
				$scope.update = false;
				$('#translation').modal('show');
			};

			$scope.reloadData = function(){
				console.log("Reloading data..");
				$scope.isLoading = true;
				$scope.allData = [];
				$scope.tableMessage = "Fetching translation data.";
				translationsFactory.getData(true).then(function(data){
					$scope.allData = data;
					$scope.tableMessage = "No translation available.";
					$scope.isLoading = false;
				});
			};

			$scope.editTranslation = function(item){
				$scope.item = item;
				$scope.update = true;
				$scope.item.edit = "true";
				$('#translation').modal('show');
			};

			$scope.deleteTranslation = function(item){
				$scope.item = item;
				$('#confirm').modal('show');
			};

			$scope.confirmDelete = function(){
				$scope.isLoading = true;
				translationsFactory.remove($scope.item).then(function(){
					$scope.isLoading = false;
					$('#confirm').modal('hide');
				});
			};

			$scope.saveChanges = function(){
				$scope.isLoading = true;
				translationsFactory.save($scope.item).then(function(){
					$('#translation').modal('hide');
					$scope.isLoading = false;
				});
			};

			$scope.export = function(){
				$scope.isLoading = true;
				if($scope.checkData.length <= 0){ 
					$scope.allData.forEach(function(item){
						$scope.checkData.push(item.TranslationCode);
					});
				}

				translationsFactory.export($scope.checkData).then(function(){
					$scope.checkData = [];
					$scope.isLoading = false;
				});
			};

			$scope.exportXLSX = function(){
				$scope.isLoading = true;
				translationsFactory.exportXLSX($scope.checkData).then(function(){
					$scope.isLoading = false;
				});
			};

			$scope.toggleCheck = function(toggle, item){
				$scope.exportAll = false;
				if(toggle){
					$scope.checkData.push(item.TranslationCode);	
				}else{
					_.pull($scope.checkData, item.TranslationCode);
				}

				if($scope.checkData.length <= 0){
					$scope.exportAll = true;
				}
			};

			$scope.clickToggleAll = function(toggle){
				if(toggle){
					$scope.toggle = true;
					$scope.exportAll = false;
					$scope.allData.forEach(function(item){
						$scope.checkData.push(item.TranslationCode);
					});
				}else{
					$scope.toggle = false;
					$scope.exportAll = true;
					$scope.checkData = [];
				}
			};

			$scope.uploadXLSX = function(file){
				if(file.type.indexOf('sheet') > -1 || file.type.indexOf('spreadsheet') > -1 || file.type.indexOf('xls') > -1){
			        $scope.filetoupload = file.name;
			        $scope.uploading = true;

			        $scope.isLoading = true;

			        Upload.upload({
			            url : '/translations/import',
			            file : file
			        }).then(function(resp){
			        	$scope.isLoading = false;
			        	if(resp.status == 200){
			        		$rootScope.messages.push({ type : "success", message : "Successfully imported excel file." });
			        		$scope.reloadData();
			        	}
			        }, function(resp){
			            $scope.uploading = false;
			            $scope.isLoading = false;
			            $rootScope.messages.push({ type : "danger", message : resp.data });
			        }, function(evt){
			          	$scope.progressUpload = parseInt(100.0 * evt.loaded / evt.total);
			        });
			    }else{
			        $rootScope.messages.push({ type : "danger", message : "Please select a valid excel file." });
			    }
			};
		}else{
			$scope.isLoading = false;
		};

		$scope.filterBySearch = function(search){
		    $scope.currentPage = 0;
		    var lowercaseTerm = search.toLowerCase();
		    if(lowercaseTerm){
		    	$scope.allData = _.filter($scope.allData, function(item){
		    		for(var k in item){
		    			if(item[k] && item[k].toLowerCase().indexOf(lowercaseTerm) > -1){
		    				return true;
		    			}
		    		}
				});
		    }else{
		    	$scope.allData = translationsFactory.data;
		    }
		};

		$scope.filterCheck = function(check){
			$scope.currentPage = 0;
			if(check){
				$scope.allData = _.filter($scope.allData, function(item){
					if(item.EN == "" || item.CHT == "" || item.CHS== "" || item.EN == null || item.CHT == null || item.CHS == null || item.CHT.indexOf("-CHT") > -1 || item.CHS.indexOf("-CHS") > -1){
						return true;
					}
					if(item.CHT == item.EN){ return true; }
					if(item.CHS == item.EN){ return true; }
				});
			}else{
				$scope.allData = translationsFactory.data;
			}
		};

		$scope.sortTable = function(sortBy){
		    if($scope.sortBy == sortBy){
		      	$scope.reverse = $scope.reverse === false ? true: false;
		    }else{
		      	$scope.reverse = false;
		    }

		    $scope.sortBy = sortBy;
		};

		$scope.setItemsPerPage = function(itemsPerPage){
		    $scope.itemsPerPage = itemsPerPage;
		    $scope.currentPage = 0;
		};

		$scope.range = function() {
		    var rangeSize = 5;
		    var ret = [];
		    var start;

		    if($scope.pageCount() < rangeSize){
		      	rangeSize = $scope.pageCount() + 1;
		    }

		    start = $scope.currentPage;
		    if ( start > $scope.pageCount()-rangeSize ) {
		      	start = $scope.pageCount()-rangeSize+1;
		    }

		    for (var i=start; i<start+rangeSize; i++) {
		      	ret.push(i);
		    }
		    return ret;
		};

		$scope.prevPage = function() {
		    if ($scope.currentPage > 0) {
		      	$scope.currentPage--;
		    }
		};

		$scope.prevPageDisabled = function() {
		    return $scope.currentPage === 0 ? "disabled" : "";
		};

		$scope.pageCount = function() {
		    return Math.ceil($scope.allData.length/$scope.itemsPerPage)-1;
		};

		$scope.nextPage = function() {
		    if ($scope.currentPage < $scope.pageCount()) {
		      	$scope.currentPage++;
		    }
		};

		$scope.nextPageDisabled = function() {
		    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
		};

		$scope.setPage = function(n) {
		    $scope.currentPage = n;
		};
	}
]);
