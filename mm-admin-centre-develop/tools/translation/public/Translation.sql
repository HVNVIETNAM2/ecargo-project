INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("AAA", "CHS", "A\"A\'-C\'HT\""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("AAA", "CHT", "A\"A\'-C\'HS\""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("AAA", "EN", "A\"A\' Raitho"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT", "EN", "Missing Image Report"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_PRINT", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_PRINT", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_PRINT", "EN", "Print"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_RUN", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_RUN", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_RUN", "EN", "Run"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_ADD_IMAGES", "CHS", "添加图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_ADD_IMAGES", "CHT", "添加圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_ADD_IMAGES", "EN", "Add images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_IMG_DEL", "CHS", "删除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_IMG_DEL", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_IMG_DEL", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCOUNT", "CHS", "账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCOUNT", "CHT", "帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCOUNT", "EN", "Account"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCT_LOCKED", "CHS", "你的账户已被锁定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCT_LOCKED", "CHT", "你的帳戶已被鎖定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCT_LOCKED", "EN", "Your account has been locked."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIONS", "CHS", "操作"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIONS", "CHT", "操作"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIONS", "EN", "Action"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_CREATE", "CHS", "成功激活新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_CREATE", "CHT", "成功激活新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_CREATE", "EN", "Total no. of new product(s) activated: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_NOTE", "CHS", "确定激活所有待处理的商品么？激活后商品将出现在用户应用端，用户可以浏览购买该商品。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_NOTE", "CHT", "確定現在激活所有待處理的商品嗎？激活後商品將出現在用戶應用端，用戶可以瀏覽購買該商品。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_NOTE", "EN", "Are you sure to activate all pending products to the storefront now?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_PRODUCTS", "CHS", "激活所有待处理的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_PRODUCTS", "CHT", "激活所有待處理的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_PRODUCTS", "EN", "Activate Pending Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_SUCCEED", "CHS", "商品激活成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_SUCCEED", "CHT", "商品激活成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_SUCCEED", "EN", "Activated Products Successful"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS", "CHS", "Activate Products-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS", "CHT", "Activate Products-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS", "EN", "Activate Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS_NOTE", "CHS", "Please click confirm to activate products-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS_NOTE", "CHT", "Please click confirm to activate products-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS_NOTE", "EN", "Please click confirm to activate products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_USER", "CHS", "激活用户账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_USER", "CHT", "激活用戶帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_USER", "EN", "Activate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE", "CHS", "验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE", "CHT", "驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE", "EN", "Activation Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE_3TIMES", "CHS", "(验证码只可发送3次)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE_3TIMES", "CHT", "(驗證碼只可發送3次)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE_3TIMES", "EN", "(Activation code can only be sent 3 times)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVE", "CHS", "激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVE", "CHT", "激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVE", "EN", "Active"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTIVE_BRAND_NOTE", "CHS", "确认激活这个品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTIVE_BRAND_NOTE", "CHT", "確認激活這個品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTIVE_BRAND_NOTE", "EN", "Confirm to activate this Brand?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ALL_BRANDS", "CHS", "所有品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ALL_BRANDS", "CHT", "所有品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ALL_BRANDS", "EN", "All Brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND", "CHS", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND", "CHT", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND", "EN", "Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_MERCHANT", "CHS", "品牌商家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_MERCHANT", "CHT", "品牌商家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_MERCHANT", "EN", "Brand Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_NUM_PROD", "CHS", "商品数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_NUM_PROD", "CHT", "商品數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_NUM_PROD", "EN", "No. of Product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_SEARCH", "CHS", "搜索品牌名称或代号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_SEARCH", "CHT", "搜索品牌名稱或代號"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_SEARCH", "EN", "Search for Brand Name or Brand Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_HEADER", "CHS", "公司商标
(标题栏)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_HEADER", "CHT", "公司商標
(標題欄)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_HEADER", "EN", "Company Logo
(Header)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_LARGE", "CHS", "公司商标
(大尺寸)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_LARGE", "CHT", "公司商標
(大尺寸)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_LARGE", "EN", "Company Logo
 (Large size)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_SMALL", "CHS", "公司商标
(小尺寸)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_SMALL", "CHT", "公司商標
(小尺寸)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_SMALL", "EN", "Company Logo
(Small size)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CREATE_BRAND", "CHS", "创建新品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CREATE_BRAND", "CHT", "創建新品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CREATE_BRAND", "EN", "Create New Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DOMAIN_MM", "CHS", ".maymay.com"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DOMAIN_MM", "CHT", ".maymay.com"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DOMAIN_MM", "EN", ".maymay.com"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_EDIT_BRAND", "CHS", "编辑品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_EDIT_BRAND", "CHT", "編輯品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_EDIT_BRAND", "EN", "Edit Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FEATURED", "CHS", "显示为精选品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FEATURED", "CHT", "顯示為精選品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FEATURED", "EN", "Show as Featured Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_FEATURED", "CHS", "精选品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_FEATURED", "CHT", "精選品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_FEATURED", "EN", "Featured Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_LISTED", "CHS", "上架品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_LISTED", "CHT", "上架品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_LISTED", "EN", "Listed Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_RECOMMEND", "CHS", "推介品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_RECOMMEND", "CHT", "推介品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_RECOMMEND", "EN", "Recommended Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_HTTP", "CHS", "http://"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_HTTP", "CHT", "http://"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_HTTP", "EN", "http://"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INACTIVE_BRAND_NOTE", "CHS", "确认停用这个品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INACTIVE_BRAND_NOTE", "CHT", "確認停用這個品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INACTIVE_BRAND_NOTE", "EN", "Confirm to inactivate this Brand?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LISTED", "CHS", "顯示為上架品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LISTED", "CHT", "显示为上架品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LISTED", "EN", "Show in Listed Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LIST_RESULT", "CHS", "找到 {0} 条有关「{1}」的纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LIST_RESULT", "CHT", "找到 {0} 條有關「{1}」的紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LIST_RESULT", "EN", "{0} search result(s) for ‘’{1}”"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_IMG_GUIDE", "CHS", "如何准备图片：请参阅指引里的文件格式和说明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_IMG_GUIDE", "CHT", "如何準備圖片：請參閱指引裡的文件格式和說明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_IMG_GUIDE", "EN", "How to prepare images: Please refer to the guideline for the file format and instructions"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_LIST_SEARCH_PLACEHOLDER", "CHS", "请输入公司显示名称或注册名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_LIST_SEARCH_PLACEHOLDER", "CHT", "請輸入公司顯示名稱或註冊名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_LIST_SEARCH_PLACEHOLDER", "EN", "Please enter display name or company name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MONO_BRAND_AGENT", "CHS", "单品牌代理商"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MONO_BRAND_AGENT", "CHT", "單品牌代理商"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MONO_BRAND_AGENT", "EN", "Mono-brand Agent"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MULTI_BRAND_AGENT", "CHS", "多品牌代理商"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MULTI_BRAND_AGENT", "CHT", "多品牌代理商"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MULTI_BRAND_AGENT", "EN", "Multi-brand Agent"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_MERCHANT", "CHS", "商户数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_MERCHANT", "CHT", "商戶數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_MERCHANT", "EN", "No. of Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_STYLE", "CHS", "样式数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_STYLE", "CHT", "樣式數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_STYLE", "EN", "No. of Style"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT", "CHS", "商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT", "CHT", "商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT", "EN", "Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT_NOTE", "CHS", "查看属于此商家的产品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT_NOTE", "CHT", "查看属于此商家的產品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT_NOTE", "EN", "To view the products under this merchant."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT", "EN", "Product Missing Images Report"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT_NO_RESULT", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT_NO_RESULT", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT_NO_RESULT", "EN", "No information to be displayed!"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER", "CHS", "个人信息封面图"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER", "CHT", "個人信息封面圖"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER", "EN", "Profile Banner Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER_NOTE", "CHS", "文件大小上限 <CONST_BG_IMG_FILE_SIZE>，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER_NOTE", "CHT", "文件大小上限 <CONST_BG_IMG_FILE_SIZE>，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER_NOTE", "EN", "Max. file size <CONST_BG_IMG_FILE_SIZE>,  JPG/PNG format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_RECOMMEND", "CHS", "显示为推介品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_RECOMMEND", "CHT", "顯示為推介品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_RECOMMEND", "EN", "Show as Recommended Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_SUGGEST", "CHS", "提供搜索建议"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_SUGGEST", "CHT", "提供搜索建議"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_SUGGEST", "EN", "Suggested in Search Bar"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_LIST_SEARCH_PLACEHOLDER", "CHS", "搜索姓名或手机号码..."); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_LIST_SEARCH_PLACEHOLDER", "CHT", "搜索姓名或手機號碼..."); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_LIST_SEARCH_PLACEHOLDER", "EN", "Search for Name or Mobile..."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_FILE", "CHS", "添加文档"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_FILE", "CHT", "添加文檔"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_FILE", "EN", "Add File"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_IMAGES", "CHS", "添加图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_IMAGES", "CHT", "添加圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_IMAGES", "EN", "Add Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_STAFF", "CHS", "分配员工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_STAFF", "CHT", "分配员工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_STAFF", "EN", "Add Staff"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADMIN_CENTER", "CHS", "用户中心"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADMIN_CENTER", "CHT", "用戶中心"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADMIN_CENTER", "EN", "Admin Center"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ALLOC", "CHS", "所有配额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ALLOC", "CHT", "所有配額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ALLOC", "EN", "All Allocations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_CATEGORY", "CHS", "所有类目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_CATEGORY", "CHT", "所有類目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_CATEGORY", "EN", "All Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_COUNTRIES", "CHS", "全部国家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_COUNTRIES", "CHT", "全部國家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_COUNTRIES", "EN", "All Countries"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_LOC_TYPE", "CHS", "全部类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_LOC_TYPE", "CHT", "全部類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_LOC_TYPE", "EN", "All Types"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_TYPES", "CHS", "全部商户类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_TYPES", "CHT", "全部商戶類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_TYPES", "EN", "All Merchant Types"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PRODUCTS", "CHS", "所有商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PRODUCTS", "CHT", "所有商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PRODUCTS", "EN", "All Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PROVINCE", "CHS", "全部省/州/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PROVINCE", "CHT", "全部省/州/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PROVINCE", "EN", "All Province"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ROLES", "CHS", "所有角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ROLES", "CHT", "所有角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ROLES", "EN", "All Roles"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_STATUS", "CHS", "所有状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_STATUS", "CHT", "所有狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_STATUS", "EN", "All Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ASSIGN_MM_STAFF_TO_MERCHANT", "CHS", "为商户分配美美员工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ASSIGN_MM_STAFF_TO_MERCHANT", "CHT", "為商戶分配美美員工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ASSIGN_MM_STAFF_TO_MERCHANT", "EN", "Assign MayMay Staff to this Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK", "CHS", "返回"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK", "CHT", "返回"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK", "EN", "Back"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_INFO", "CHS", "商户注册信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_INFO", "CHT", "商戶註冊信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_INFO", "EN", "Basic Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_PROFILE", "CHS", "用户基本信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_PROFILE", "CHT", "用戶基本信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_PROFILE", "EN", "Basic Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BG_IMG", "CHS", "背景大图"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BG_IMG", "CHT", "背景大圖"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BG_IMG", "EN", "Background Image (big)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM", "CHS", "街区号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM", "CHT", "街區號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM", "EN", "Block Number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM_OPT", "CHS", "街区号码（选填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM_OPT", "CHT", "街區號碼（選填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM_OPT", "EN", "Block Number (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_CODE", "CHS", "品牌代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_CODE", "CHT", "品牌代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_CODE", "EN", "Brand Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_S", "CHS", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_S", "CHT", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_S", "EN", "Brand(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BR_NUM", "CHS", "商业登记号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BR_NUM", "CHT", "商業登記號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BR_NUM", "EN", "Business Registration no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME", "CHS", "楼宇名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME", "CHT", "樓宇名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME", "EN", "Buillding Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME_OPT", "CHS", "楼宇名称（选填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME_OPT", "CHT", "樓宇名稱（選填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME_OPT", "EN", "Building Name (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CANCEL", "CHS", "取消"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CANCEL", "CHT", "取消"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CANCEL", "EN", "Cancel"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY", "CHS", "类目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY", "CHT", "類目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY", "EN", "Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL", "CHS", "全部"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL", "CHT", "全部"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL", "EN", "All"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_COMMENTS", "CHS", "全部评价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_COMMENTS", "CHT", "全部評價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_COMMENTS", "EN", "All Comments"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_REVIEW", "CHS", "全部评价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_REVIEW", "CHT", "全部評價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_REVIEW", "EN", "All reviews"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRAND", "CHS", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRAND", "CHT", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRAND", "EN", "Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BROWSE", "CHS", "发现"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BROWSE", "CHT", "發現"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BROWSE", "EN", "Browse"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CANCEL", "CHS", "取消"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CANCEL", "CHT", "取消"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CANCEL", "EN", "Cancel"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY", "CHS", "类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY", "CHT", "類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY", "EN", "Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY_BRAND", "CHS", "品类"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY_BRAND", "CHT", "品類"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY_BRAND", "EN", "Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COLOUR", "CHS", "颜色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COLOUR", "CHT", "顏色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COLOUR", "EN", "Color"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COMMENT", "CHS", "评论"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COMMENT", "CHT", "評論"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COMMENT", "EN", "Comment"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CUST_SERVICE", "CHS", "卖家服务"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CUST_SERVICE", "CHT", "賣家服務"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CUST_SERVICE", "EN", "Customer service"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOUNT", "CHS", "特价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOUNT", "CHT", "特價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOUNT", "EN", "Discount"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOVER", "CHS", "发现"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOVER", "CHT", "發現"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOVER", "EN", "Discover"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER", "CHS", "筛选"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER", "CHT", "篩選"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER", "EN", "Filter"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_LIKE", "CHS", "喜欢"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_LIKE", "CHT", "喜歡"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_LIKE", "EN", "Like"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ME", "CHS", "我的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ME", "CHT", "我的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ME", "EN", "Me"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MESSENGER", "CHS", "聊聊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MESSENGER", "CHT", "聊聊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MESSENGER", "EN", "Messenger"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MY_BRAND", "CHS", "我的品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MY_BRAND", "CHT", "我的品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MY_BRAND", "EN", "My brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEWSFEED", "CHS", "动态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEWSFEED", "CHT", "動態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEWSFEED", "EN", "Newsfeed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT", "CHS", "亲，没有找到相关商品。
您可以换个词再试试，以看看我们的推介。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT", "CHT", "親，沒有找到相關商品。
您可以換個詞再試試，以看看我們的推介。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT", "EN", "Sorry, your search did not match any products. Please try again or take a look at our recommendations below."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_COMMENT", "CHS", "条评论"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_COMMENT", "CHT", "條評論"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_COMMENT", "EN", "Comment(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PPL_RCMD_ITEM", "CHS", "人推荐了这件单件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PPL_RCMD_ITEM", "CHT", "人推薦了這件單件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PPL_RCMD_ITEM", "EN", "consumers recommend this item"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_1", "CHS", "有"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_1", "CHT", "有"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_1", "EN", "Showing"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_2", "CHS", "件商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_2", "CHT", "件商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_2", "EN", "product(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OUTFIT", "CHS", "配搭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OUTFIT", "CHT", "配搭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OUTFIT", "EN", "Outfit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OVERALL", "CHS", "综合"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OVERALL", "CHT", "綜合"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OVERALL", "EN", "Overall"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PDP_NUM_LIKE", "CHS", "人喜欢这件单品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PDP_NUM_LIKE", "CHT", "人喜歡這件單品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PDP_NUM_LIKE", "EN", "people like this product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE", "CHS", "价格"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE", "CHT", "價格"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE", "EN", "Price"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE_RANGE", "CHS", "价格区间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE_RANGE", "CHT", "價格區間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE_RANGE", "EN", "Price range"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DESC", "CHS", "商品描述"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DESC", "CHT", "商品描述"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DESC", "EN", "Product Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DETAILS", "CHS", "图文详情"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DETAILS", "CHT", "圖文詳情"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DETAILS", "EN", "Product Details"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_REVIEW", "CHS", "商品评价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_REVIEW", "CHT", "商品評價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_REVIEW", "EN", "Product Review"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_SPEC", "CHS", "产品参数"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_SPEC", "CHT", "產品參數"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_SPEC", "EN", "Product Specification"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_QUANTITY", "CHS", "数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_QUANTITY", "CHT", "數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_QUANTITY", "EN", "Quantity"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RCMD_TO_CONSUMER", "CHS", "猜您喜欢"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RCMD_TO_CONSUMER", "CHT", "猜您喜歡"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RCMD_TO_CONSUMER", "EN", "Recommended for you"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECENT_SEARCH", "CHS", "最近搜寻"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECENT_SEARCH", "CHT", "最近搜尋"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECENT_SEARCH", "EN", "Recent searches"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED", "CHS", "推介"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED", "CHT", "推介"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED", "EN", "Recommended"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET", "CHS", "重设"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET", "CHT", "重設"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET", "EN", "Reset"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SALES", "CHS", "销量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SALES", "CHT", "銷量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SALES", "EN", "Sales"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_BRAND_PLACEHOLDER", "CHS", "要找什么品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_BRAND_PLACEHOLDER", "CHT", "要找什麼品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_BRAND_PLACEHOLDER", "EN", "Search brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_FILTER_PLACEHOLDER", "CHS", "搜寻品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_FILTER_PLACEHOLDER", "CHT", "搜尋品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_FILTER_PLACEHOLDER", "EN", "Search for brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_PLACEHOLDER", "CHS", "要买什么"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_PLACEHOLDER", "CHT", "要買什麼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_PLACEHOLDER", "EN", "Search products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SELECT_SIZE", "CHS", "选择尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SELECT_SIZE", "CHT", "選擇尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SELECT_SIZE", "EN", "Select Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPMENT_RATING", "CHS", "物流服务"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPMENT_RATING", "CHT", "物流服務"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPMENT_RATING", "EN", "Shipment rating"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_ADDR", "CHS", "收货地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_ADDR", "CHT", "收貨地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_ADDR", "EN", "Shipping Address"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_CONTACT", "CHS", "收货人"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_CONTACT", "CHT", "收貨人"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_CONTACT", "EN", "Shipping Contact"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_METHOD", "CHS", "配送方式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_METHOD", "CHT", "配送方式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_METHOD", "EN", "Shipping Method"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SINGLE_PRODUCT", "CHS", "单品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SINGLE_PRODUCT", "CHT", "單品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SINGLE_PRODUCT", "EN", "Single Product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SIZE", "CHS", "尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SIZE", "CHT", "尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SIZE", "EN", "Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT", "CHS", "排序"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT", "CHT", "排序"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT", "EN", "Sort"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RATING", "CHS", "店铺评分"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RATING", "CHT", "店舖評分"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RATING", "EN", "Store rating"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RCMD", "CHS", "时尚穿搭贴心建议"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RCMD", "CHT", "時尚穿搭貼心建議"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RCMD", "EN", "trending pick"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STYLEFEED", "CHS", "美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STYLEFEED", "CHT", "美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STYLEFEED", "EN", "Stylefeed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SUBMIT", "CHS", "确认"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SUBMIT", "CHT", "確認"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SUBMIT", "EN", "Submit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SWIPE_TO_BUY", "CHS", "一扫即买"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SWIPE_TO_BUY", "CHT", "一掃即買"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SWIPE_TO_BUY", "EN", "Swipe to buy"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL", "CHS", "合计"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL", "CHT", "合計"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL", "EN", "Total"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TRENDING_SEARCH", "CHS", "热门搜寻"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TRENDING_SEARCH", "CHT", "熱門搜尋"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TRENDING_SEARCH", "EN", "Trending search"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE", "CHS", "更改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE", "CHT", "更改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE", "EN", "Change"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_EMAIL", "CHS", "修改邮箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_EMAIL", "CHT", "修改郵箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_EMAIL", "EN", "Change Email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_MOBILE", "CHS", "修改手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_MOBILE", "CHT", "修改手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_MOBILE", "EN", "Change Mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_PASSWORD", "CHS", "修改密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_PASSWORD", "CHT", "修改密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_PASSWORD", "EN", "Change Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_STATUS", "CHS", "更改状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_STATUS", "CHT", "更改狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_STATUS", "EN", "Change Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_USER_MOBILE", "CHS", "修改用户手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_USER_MOBILE", "CHT", "修改用戶手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_USER_MOBILE", "EN", "Change User Mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHOOSE_MERCHANT_TYPE", "CHS", "选择商户类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHOOSE_MERCHANT_TYPE", "CHT", "選擇商戶類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHOOSE_MERCHANT_TYPE", "EN", "Choose Merchant Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY", "CHS", "城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY", "CHT", "城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY", "EN", "City"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY_REGION", "CHS", "城市/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY_REGION", "CHT", "城市/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY_REGION", "EN", "City/Region"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CLOSE", "CHS", "关闭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CLOSE", "CHT", "關閉"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CLOSE", "EN", "Close"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOR_IMAGE", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOR_IMAGE", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOR_IMAGE", "EN", "Color Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOUR", "CHS", "颜色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOUR", "CHT", "顏色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOUR", "EN", "Color"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO", "CHS", "公司商标"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO", "CHT", "公司商標"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO", "EN", "Company Logo"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_HEADER_NOTE", "CHS", "文件大小上限 < CONST_LOGO_FILE_SIZE>，(高)72px X (阔)236px，PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_HEADER_NOTE", "CHT", "文件大小上限 < CONST_LOGO_FILE_SIZE>，(高)72px X (闊)236px，PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_HEADER_NOTE", "EN", "Max. file size < CONST_LOGO_FILE_SIZE>, 72(h)px X 236(w)px, PNG format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_LARGE_NOTE", "CHS", "文件大小上限 < CONST_LOGO_FILE_SIZE>，(高)360px X (阔)360px，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_LARGE_NOTE", "CHT", "文件大小上限 < CONST_LOGO_FILE_SIZE>，(高)360px X (闊)360px，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_LARGE_NOTE", "EN", "Max. file size < CONST_LOGO_FILE_SIZE>, 360(h)px X 360(w)px, JPG/PNG format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_SMALL_NOTE", "CHS", "文件大小上限 < CONST_LOGO_FILE_SIZE>，(高)108px X (阔)108px，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_SMALL_NOTE", "CHT", "文件大小上限 < CONST_LOGO_FILE_SIZE>，(高)108px X (闊)108px，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_SMALL_NOTE", "EN", "Max. file size < CONST_LOGO_FILE_SIZE>, 108(h)px X 108(w)px, JPG/PNG format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_NAME", "CHS", "公司注册名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_NAME", "CHT", "公司註冊名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_NAME", "EN", "Company Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION", "CHS", "完成注册"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION", "CHT", "完成註冊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION", "EN", "Complete New User Registration"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION_NOTE", "CHS", "请输入短信中的验证码已创建密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION_NOTE", "CHT", "請輸入短信中的驗證碼以創建密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION_NOTE", "EN", "Please enter activation code from SMS
and create your password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMP_DESC", "CHS", "公司简介"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMP_DESC", "CHT", "公司簡介"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMP_DESC", "EN", "Company Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRM", "CHS", "确认"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRM", "CHT", "確認"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRM", "EN", "Confirm"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRMATION_IMPORT", "CHS", "确认汇入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRMATION_IMPORT", "CHT", "確認匯入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRMATION_IMPORT", "EN", "Confirm to import"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_EMAIL", "CHS", "确认新邮箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_EMAIL", "CHT", "確認新郵箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_EMAIL", "EN", "Confirm New Email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_MOBILE", "CHS", "确认手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_MOBILE", "CHT", "確認手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_MOBILE", "EN", "Confirm Mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_PASSWORD", "CHS", "确认新密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_PASSWORD", "CHT", "確認新密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_PASSWORD", "EN", "Confirm New Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONGRATULATIONS", "CHS", "成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONGRATULATIONS", "CHT", "成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONGRATULATIONS", "EN", "Congratulations!"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONTENT", "CHS", "內容"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONTENT", "CHT", "內容"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONTENT", "EN", "Content"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY", "CHS", "国家/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY", "CHT", "國家/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY", "EN", "Country/Region"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY_PICK", "CHS", "请选择国家/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY_PICK", "CHT", "请选择國家/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY_PICK", "EN", "Please select country/region"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE", "CHS", "创建"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE", "CHT", "創建"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE", "EN", "Create"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER", "CHS", "創建另一個"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER", "CHT", "创建另一个"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER", "EN", "Create Another"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER_USER", "CHS", "建立下一个库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER_USER", "CHT", "創建下一個用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER_USER", "EN", "Create Another"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MERCHANT", "CHS", "创建新商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MERCHANT", "CHT", "創建新商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MERCHANT", "EN", "Create New Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MER_PROF_FILL_DETAILS", "CHS", "请填写下列信息以创建商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MER_PROF_FILL_DETAILS", "CHT", "請填寫下列信息以創建商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MER_PROF_FILL_DETAILS", "EN", "Please fill in the following information to create the merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_USER", "CHS", "创建新用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_USER", "CHT", "創建新用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_USER", "EN", "Create New User"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_EMAIL", "CHS", "现在的邮箱地址:"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_EMAIL", "CHT", "現在的郵箱地址:"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_EMAIL", "EN", "Current email:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_MOBILE", "CHS", "现在的手机号码:"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_MOBILE", "CHT", "現在的手機號碼:"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_MOBILE", "EN", "Current mobile no.:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE", "CHS", "Date-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE", "CHT", "Date-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE", "EN", "Date"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TIME", "CHS", "日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TIME", "CHT", "日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TIME", "EN", "Date & Time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DEFAULT_RATE", "CHS", "默认佣金费率"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DEFAULT_RATE", "CHT", "默認佣金費率"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DEFAULT_RATE", "EN", "Default Rate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE", "CHS", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE_USER", "CHS", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE_USER", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE_USER", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DESCRIPTION_IMAGE", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DESCRIPTION_IMAGE", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DESCRIPTION_IMAGE", "EN", "Description Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DETAILS", "CHS", "详细信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DETAILS", "CHT", "詳細信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DETAILS", "EN", "Details"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISP_NAME", "CHS", "显示名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISP_NAME", "CHT", "顯示名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISP_NAME", "EN", "Display Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT", "CHS", "区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT", "CHT", "區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT", "EN", "District"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT_POSTCODE", "CHS", "区域/邮编"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT_POSTCODE", "CHT", "區域/郵編"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT_POSTCODE", "EN", "District/Postcode"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DONE_BACK_TO_LISTING", "CHS", "完成并返回商品列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DONE_BACK_TO_LISTING", "CHT", "完成並返回商品列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DONE_BACK_TO_LISTING", "EN", "Done and back to listing"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DRAFTED", "CHS", "草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DRAFTED", "CHT", "草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DRAFTED", "EN", "Draft"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT", "CHS", "修改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT", "CHT", "修改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT", "EN", "Edit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_MERCHANT", "CHS", "编辑商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_MERCHANT", "CHT", "編輯商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_MERCHANT", "EN", "Edit Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_HEADER", "CHS", "查看商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_HEADER", "CHT", "查看商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_HEADER", "EN", "View Product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY", "CHS", "编辑库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY", "CHT", "編輯庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY", "EN", "Edit Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY_SINGLE", "CHS", "编辑库存 - {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY_SINGLE", "CHT", "編輯庫存 - {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY_SINGLE", "EN", "Edit Inventory - {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PROFILE", "CHS", "编辑个人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PROFILE", "CHT", "編輯個人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PROFILE", "EN", "Edit Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_USER", "CHS", "编辑"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_USER", "CHT", "編輯"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_USER", "EN", "Edit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL", "CHS", "邮箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL", "CHT", "郵箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL", "EN", "Email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL_OR_MOBILE", "CHS", "邮箱地址或手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL_OR_MOBILE", "CHT", "郵箱地址或手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL_OR_MOBILE", "EN", "Email/Mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_ACTIVATION_CODE", "CHS", "请输入短信中的验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_ACTIVATION_CODE", "CHT", "請輸入短信中的驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_ACTIVATION_CODE", "EN", "Please enter activation code from SMS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_PASSWORD", "CHS", "输入密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_PASSWORD", "CHT", "輸入密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_PASSWORD", "EN", "Enter password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR", "CHS", "出现错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR", "CHT", "出現錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR", "EN", "Error Occurs"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERRORS", "CHS", "Errors-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERRORS", "CHT", "Errors-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERRORS", "EN", "Errors"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_DETAILS", "CHS", "错误信息:"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_DETAILS", "CHT", "錯誤信息:"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_DETAILS", "EN", "Error details:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_FILE", "CHS", "Error File-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_FILE", "CHT", "Error File-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_FILE", "EN", "Error File"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EVENT_LOG", "CHS", "操作记录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EVENT_LOG", "CHT", "操作記錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EVENT_LOG", "EN", "Event/Log"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT", "CHS", "下载"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT", "CHT", "下載"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT", "EN", "Export"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY", "CHS", "汇出库存纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY", "CHT", "匯出庫存紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY", "EN", "Export Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY_NOTE", "CHS", "下载所有库存信息，共 {0} 条"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY_NOTE", "CHT", "下載所有庫存信息，共 {0} 條"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY_NOTE", "EN", "You are going to export {0} inventory record(s) in a xlsx file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS", "CHS", "下载商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS", "CHT", "下載商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS", "EN", "Export Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS_NOTE", "CHS", "下载所有商品信息，共 {0} 件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS_NOTE", "CHT", "下載所有商品信息，共 {0} 件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS_NOTE", "EN", "You are going to export {0} product(s) in a xlsx file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURED_BRAND", "CHS", "精选品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURED_BRAND", "CHT", "精選品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURED_BRAND", "EN", "Featured Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURE_IMAGE", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURE_IMAGE", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURE_IMAGE", "EN", "Featured Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILES", "CHS", "文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILES", "CHT", "文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILES", "EN", "Files"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_BR", "CHS", "请填写您的营业执照资料。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_BR", "CHT", "請填寫您的營業執照資料。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_BR", "EN", "Please fill in your business registration (BR) information."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_EMAIL_RETRIEVE_PASSWORD", "CHS", "填写邮箱地址以获取您的密码。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_EMAIL_RETRIEVE_PASSWORD", "CHT", "填寫郵箱地址以獲取您的密碼。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_EMAIL_RETRIEVE_PASSWORD", "EN", "Fill in Email to retrieve your password."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_PASSWORD_COMPLETE", "CHS", "请输入密码以完成注册。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_PASSWORD_COMPLETE", "CHT", "請輸入密碼以完成註冊。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_PASSWORD_COMPLETE", "EN", "Please fill in the password in order to complete your registration."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILTER_ALL_CITY", "CHS", "全部城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILTER_ALL_CITY", "CHT", "全部城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILTER_ALL_CITY", "EN", "All City"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FIRSTNAME", "CHS", "名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FIRSTNAME", "CHT", "名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FIRSTNAME", "EN", "First Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT", "CHS", "单位/公寓/住宅号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT", "CHT", "單位/公寓/住宅號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT", "EN", "Flat / Apartment / House"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT_OPT", "CHS", "单位/公寓/住宅号码（选填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT_OPT", "CHT", "單位/公寓/住宅號碼（選填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT_OPT", "EN", "Flat / Apartment / House (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR", "CHS", "楼层"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR", "CHT", "樓層"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR", "EN", "Floor"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR_OPT", "CHS", "楼层（选填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR_OPT", "CHT", "樓層（選填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR_OPT", "EN", "Floor (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FORGOT_PASSWORD", "CHS", "忘记密码?"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FORGOT_PASSWORD", "CHT", "忘記密碼?"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FORGOT_PASSWORD", "EN", "Forgot Password?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUID", "CHS", "GUID-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUID", "CHT", "GUID-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUID", "EN", "GUID"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE", "CHS", "如何准备图片：请参照图像的命名规则和上传限制的说明。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE", "CHT", "如何準備圖片：請參照圖像的命名規則和上傳限制的說明。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE", "EN", "How to prepare images: Please refer to the instructions for the image naming conventions and uploading limitations."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE_DL", "CHS", "下载"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE_DL", "CHT", "下載"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE_DL", "EN", "Download"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HIDDEN", "CHS", "己隐藏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HIDDEN", "CHT", "己隱藏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HIDDEN", "EN", "Hidden"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HOME", "CHS", "主页"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HOME", "CHT", "主頁"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HOME", "EN", "Home"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_ALTTEXT", "CHS", "请输入图片替换文字 (1-100字符)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_ALTTEXT", "CHT", "請輸入圖片替換文字 (1-100字符)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_ALTTEXT", "EN", "Please enter alt text for image, max. 100 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_UPLOAD", "CHS", "上传图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_UPLOAD", "CHT", "上傳圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_UPLOAD", "EN", "Upload Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT", "CHS", "导入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT", "CHT", "導入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT", "EN", "Import"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_ANOTHER_FILE", "CHS", "汇入另一个文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_ANOTHER_FILE", "CHT", "匯入另一個文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_ANOTHER_FILE", "EN", "Import another file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_FILE", "CHS", "Import File-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_FILE", "CHT", "Import File-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_FILE", "EN", "Import File"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_HISTORY", "CHS", "Import History-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_HISTORY", "CHT", "Import History-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_HISTORY", "EN", "Import History"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_INVENTORY", "CHS", "更新现有商品库存信息 (当通用商品代码 SKU 相同时)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_INVENTORY", "CHT", "更新現有商品庫存信息 (當通用商品代碼 SKU 相同時)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_INVENTORY", "EN", "Update the items in existing inventory list with the same product SKU"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_PRODUCT", "CHS", "更新现有商品信息 (当商品编码相同时)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_PRODUCT", "CHT", "更新現有商品信息 (當商品編碼相同時)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_PRODUCT", "EN", "Overwrite the items in existing product list with the same product code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS", "CHS", "导入商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS", "CHT", "導入商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS", "EN", "Import Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_CREATE", "CHS", "成功创建商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_CREATE", "CHT", "成功創建商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_CREATE", "EN", "Total no. of product(s) created: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_DOWNLOAD_ERROR_FILE", "CHS", "Download Error File-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_DOWNLOAD_ERROR_FILE", "CHT", "Download Error File-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_DOWNLOAD_ERROR_FILE", "EN", "Download Error File"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FAILED", "CHS", "上传失败纪录：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FAILED", "CHT", "上傳失敗紀錄：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FAILED", "EN", "Total no. of record(s) failed to upload: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FILE_ERROR_CELL", "CHS", "第 {0} 行   第 {1} 列："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FILE_ERROR_CELL", "CHT", "第 {0} 行   第 {1} 列："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FILE_ERROR_CELL", "EN", "Row {0}, column {1}:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_FAILED", "CHS", "上传失败库存纪录：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_FAILED", "CHT", "上傳失敗庫存紀錄：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_FAILED", "EN", "Total no. of inventory record(s) failed to upload: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_OVERWRITE", "CHS", "成功更新库存纪录: {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_OVERWRITE", "CHT", "成功更新庫存紀錄: {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_OVERWRITE", "EN", "Total no. of inventory record(s) updated: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_OVERWRITE", "CHS", "成功更新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_OVERWRITE", "CHT", "成功更新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_OVERWRITE", "EN", "Total no. of products updated: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SKIPPED", "CHS", "被略过商品: {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SKIPPED", "CHT", "被略過商品: {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SKIPPED", "EN", "Total no. of products skipped: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SUCCEED", "CHS", "导入商品成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SUCCEED", "CHT", "導入商品成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SUCCEED", "EN", "Import Products Succeeded"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY", "CHS", "汇入库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY", "CHT", "匯入庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY", "EN", "Import Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY_CREATE", "CHS", "成功创建库存纪录：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY_CREATE", "CHT", "成功創建庫存紀錄：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY_CREATE", "EN", "Total no. of inventory record(s) created: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_USER", "CHS", "停用库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_USER", "CHT", "暫停用戶帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_USER", "EN", "Inactivate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVE", "CHS", "暂停"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVE", "CHT", "暫停"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVE", "EN", "Inactive"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INPUT_MERCHANT_NAME", "CHS", "请输入商户名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INPUT_MERCHANT_NAME", "CHT", "請輸入商戶名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INPUT_MERCHANT_NAME", "EN", "Please input the merchant name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INSERTED", "CHS", "Inserted-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INSERTED", "CHT", "Inserted-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INSERTED", "EN", "Inserted"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION", "CHS", "存货配额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION", "CHT", "存貨配額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION", "EN", "Allocation"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION_NOTE", "CHS", "输入数字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION_NOTE", "CHT", "輸入數字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION_NOTE", "EN", "Enter a number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOC_FILTER", "CHS", "存货配额量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOC_FILTER", "CHT", "存貨配額量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOC_FILTER", "EN", "No. of Allocation"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS", "CHS", "可出售量 (ATS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS", "CHT", "可出售量 (ATS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS", "EN", "ATS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS_RESULT", "CHS", "可出售量 (ATS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS_RESULT", "CHT", "可出售量 (ATS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS_RESULT", "EN", "Available to Sell (ATS)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LIST", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LIST", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LIST", "EN", "Inventory List"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LISTS", "CHS", "库存列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LISTS", "CHT", "庫存列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LISTS", "EN", "Inventory Lists"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION", "CHS", "库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION", "CHT", "庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION", "EN", "Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_CHANGE", "CHS", "修改库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_CHANGE", "CHT", "修改庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_CHANGE", "EN", "Change Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_EDIT", "CHS", "编辑库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_EDIT", "CHT", "編輯庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_EDIT", "EN", "Edit Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ONORDER", "CHS", "准备出货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ONORDER", "CHT", "準備出貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ONORDER", "EN", "On Order"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_PERPETUAL", "CHS", "无限量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_PERPETUAL", "CHT", "無限量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_PERPETUAL", "EN", "Perpetual"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_EXPORTED", "CHS", "已出货量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_EXPORTED", "CHT", "已出貨量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_EXPORTED", "EN", "Quantity Exported"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_ORDERED", "CHS", "准备出货量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_ORDERED", "CHT", "準備出貨量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_ORDERED", "EN", "Quantity Ordered"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_RESETTIME", "CHS", "库存重置时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_RESETTIME", "CHT", "庫存重置日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_RESETTIME", "EN", "Reset Date & Time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_SEARCH", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_SEARCH", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_SEARCH", "EN", "Search Inventories"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_INSTOCK", "CHS", "有现货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_INSTOCK", "CHT", "有現貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_INSTOCK", "EN", "In Stock"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_LOWSTOCK", "CHS", "存货紧张"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_LOWSTOCK", "CHT", "存貨緊張"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_LOWSTOCK", "EN", "Low in Stock"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_OUTOFSTOCK", "CHS", "缺货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_OUTOFSTOCK", "CHT", "缺貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_OUTOFSTOCK", "EN", "Out of Stock"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_TOTAL_ATS", "CHS", "可出售总量 (ATS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_TOTAL_ATS", "CHT", "可出售總量 (ATS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_TOTAL_ATS", "EN", "Total ATS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_TURNOVER", "CHS", "已出货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_TURNOVER", "CHT", "已出貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_TURNOVER", "EN", "Turnover"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_UPDATE_HISTORY", "CHS", "库存纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_UPDATE_HISTORY", "CHT", "庫存紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_UPDATE_HISTORY", "EN", "Inventory History"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_CREATE", "CHS", "创建配额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_CREATE", "CHT", "創建配額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_CREATE", "EN", "Allocation Created"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_DELETE", "CHS", "删除配额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_DELETE", "CHT", "刪除配額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_DELETE", "EN", "Allocation Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_EDIT", "CHS", "编辑配额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_EDIT", "CHT", "編輯配額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_EDIT", "EN", "Allocation Edited"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_HIST_NOTE", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_HIST_NOTE", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_HIST_NOTE", "EN", "Note: Please refer to the previous figure of each record for former data"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE", "CHS", "语言"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE", "CHT", "語言"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE", "EN", "Language"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE_SWITCH", "CHS", "转换语言至"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE_SWITCH", "CHT", "轉換語言至"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE_SWITCH", "EN", "Switch language to"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LASTNAME", "CHS", "姓"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LASTNAME", "CHT", "姓"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LASTNAME", "EN", "Last Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LLD", "CHS", "最后登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LLD", "CHT", "最後登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LLD", "EN", "Last Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LMD", "CHS", "最后更改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LMD", "CHT", "最後更改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LMD", "EN", "Last Modified"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING", "CHS", "页面载入中，请稍候⋯"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING", "CHT", "頁面載入中，請稍候⋯"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING", "EN", "Page loading, please wait..."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING_DATA", "CHS", "Loading Data.-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING_DATA", "CHT", "Loading Data.-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING_DATA", "EN", "Loading Data."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION", "CHS", "动作"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION", "CHT", "動作"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION", "EN", "Action"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_DELETE", "CHS", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_DELETE", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_DELETE", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_EDIT", "CHS", "编辑"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_EDIT", "CHT", "編輯"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_EDIT", "EN", "Edit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_INACTIVE", "CHS", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_INACTIVE", "CHT", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_INACTIVE", "EN", "Inactive"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CLOSE", "CHS", "关闭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CLOSE", "CHT", "關閉"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CLOSE", "EN", "Close"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CREATE", "CHS", "创建新位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CREATE", "CHT", "創建新位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CREATE", "EN", "Create New Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_DELETE_LABEL", "CHS", "删除位置状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_DELETE_LABEL", "CHT", "刪除位置狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_DELETE_LABEL", "EN", "Delete Location Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_EXTERNAL_CODE", "CHS", "位置外部代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_EXTERNAL_CODE", "CHT", "位置外部代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_EXTERNAL_CODE", "EN", "Location External Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ID_CREATE", "CHS", "位置編碼是唯一的 (选填项)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ID_CREATE", "CHT", "位置編碼是唯一的 (選填項)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ID_CREATE", "EN", "LocationID is unique (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_NO_DATA", "CHS", "没有可显示的信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_NO_DATA", "CHT", "沒有可顯示的信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_NO_DATA", "EN", "No Information to be displayed."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK", "CHS", "最少库存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK", "CHT", "最少庫存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK", "EN", "Location Safety Threshold"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK_NOTE", "CHS", "输入数字 (可选)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK_NOTE", "CHT", "輸入數字 (可選)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK_NOTE", "EN", "Enter a number (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SEARCH", "CHS", "搜索库存位置及代号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SEARCH", "CHT", "搜索庫存位置及代號"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SEARCH", "EN", "Search for Inventory Location and Location ID"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SHOWING_PAGE", "CHS", "页面顯示中"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SHOWING_PAGE", "CHT", "页面显示中"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SHOWING_PAGE", "EN", "Showing Page"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS", "CHS", "状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS", "CHT", "狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS", "EN", "Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_ACTIVE", "CHS", "激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_ACTIVE", "CHT", "激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_ACTIVE", "EN", "Active"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_INACTIVE", "CHS", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_INACTIVE", "CHT", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_INACTIVE", "EN", "Inactive"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE", "CHS", "更新库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE", "CHT", "更新庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE", "EN", "Update Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE_LABEL", "CHS", "更新位置狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE_LABEL", "CHT", "更新位置状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE_LABEL", "EN", "Update Location Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITHOUT_ALLOC", "CHS", "无配額庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITHOUT_ALLOC", "CHT", "無配额库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITHOUT_ALLOC", "EN", "Location without Allocations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITH_ALLOC", "CHS", "有配额库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITH_ALLOC", "CHT", "有配額庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITH_ALLOC", "EN", "Location with Allocations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_ID", "CHS", "位置代号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_ID", "CHT", "位置代號"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_ID", "EN", "Location ID"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_NAME", "CHS", "位置名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_NAME", "CHT", "位置名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_NAME", "EN", "Location Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_SELECT", "CHS", "库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_SELECT", "CHT", "庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_SELECT", "EN", "Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE", "CHS", "位置类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE", "CHT", "位置類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE", "EN", "Location Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE_SELECT", "CHS", "选择位置类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE_SELECT", "CHT", "選擇位置類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE_SELECT", "EN", "Select Location Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGIN", "CHS", "登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGIN", "CHT", "登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGIN", "EN", "Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGOUT", "CHS", "登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGOUT", "CHT", "登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGOUT", "EN", "Logout"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LTB_LOADING", "CHS", "载入中"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LTB_LOADING", "CHT", "載入中"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LTB_LOADING", "EN", "Loading"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT", "CHS", "商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT", "CHT", "商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT", "EN", "Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANTS", "CHS", "所有商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANTS", "CHT", "所有商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANTS", "EN", "Merchants"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP", "CHS", "商户应用端"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP", "CHT", "商戶應用端"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP", "EN", "Merchant App"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP_NAME", "CHS", "美美﹣商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP_NAME", "CHT", "美美﹣商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP_NAME", "EN", "MM-Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_TYPE", "CHS", "商户类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_TYPE", "CHT", "商戶類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_TYPE", "EN", "Merchant Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MIDDLE_NAME", "CHS", "中间名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MIDDLE_NAME", "CHT", "中間名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MIDDLE_NAME", "EN", "Middle Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_GUIDELINE", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_GUIDELINE", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_GUIDELINE", "EN", "See the missing images: Open a list to see the total number of missing images and which one they are."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_VIEW", "CHS", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_VIEW", "CHT", ""); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_VIEW", "EN", "See Missing Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM", "CHS", "美美"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM", "CHT", "美美"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM", "EN", "MM"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM_LOGIN", "CHS", "登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM_LOGIN", "CHT", "登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM_LOGIN", "EN", "Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE", "CHS", "手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE", "CHT", "手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE", "EN", "Mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE_NOTE", "CHS", "请在手机号码前加国家代码和地区代码
如：+86-12345678"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE_NOTE", "CHT", "請在手機號碼前加國家代碼和地區代碼
如：+86-12345678"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE_NOTE", "EN", "Please specify country and area code in front of the mobile no.
e.g.: +86-12345678"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MSG_CHANGE_PRODUCT_STATUS", "CHS", "您要更改商品状态为 {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MSG_CHANGE_PRODUCT_STATUS", "CHT", "您要更改商品狀態為 {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MSG_CHANGE_PRODUCT_STATUS", "EN", "You are going to change the status of this product to  {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NA", "CHS", "不适用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NA", "CHT", "不適用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NA", "EN", "N/A"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NAME", "CHS", "名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NAME", "CHT", "名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NAME", "EN", "Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEWRATE", "CHS", "实际佣金费率"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEWRATE", "CHT", "實際佣金費率"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEWRATE", "EN", "New Rate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_EMAIL", "CHS", "新的电邮地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_EMAIL", "CHT", "新的電郵地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_EMAIL", "EN", "New Email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_MOBILE", "CHS", "新的手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_MOBILE", "CHT", "新的手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_MOBILE", "EN", "New Mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_PASSWORD", "CHS", "新密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_PASSWORD", "CHT", "新密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_PASSWORD", "EN", "New Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEXT", "CHS", "下一步"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEXT", "CHT", "下一步"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEXT", "EN", "Next"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO", "CHS", "否"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO", "CHT", "否"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO", "EN", "No"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_AVAILABLE_DATA", "CHS", "No available data.-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_AVAILABLE_DATA", "CHT", "No available data.-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_AVAILABLE_DATA", "EN", "No available data."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_FILE_CHOSEN", "CHS", "请选择文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_FILE_CHOSEN", "CHT", "請選擇文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_FILE_CHOSEN", "EN", "Please select file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_UPDATE_HIST", "CHS", "没有更新历史记录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_UPDATE_HIST", "CHT", "沒有更新歷史記錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_UPDATE_HIST", "EN", "No update history"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NUMBER_OF_INVENTORY", "CHS", "库存数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NUMBER_OF_INVENTORY", "CHT", "庫存數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NUMBER_OF_INVENTORY", "EN", "No. of Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OK", "CHS", "确定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OK", "CHT", "確定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OK", "EN", "OK"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OLD_PASSWORD", "CHS", "旧密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OLD_PASSWORD", "CHT", "舊密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OLD_PASSWORD", "EN", "Old Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OOPS", "CHS", "出错了"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OOPS", "CHT", "發現錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OOPS", "EN", "Oops..."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OPTIONAL", "CHS", "(选填项)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OPTIONAL", "CHT", "(選塡項)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OPTIONAL", "EN", "(Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGINATION", "CHS", "共 {2} 个结果，显示 {0} - {1} 个"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGINATION", "CHT", "共 {2} 個結果，顯示 {0} - {1} 個"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGINATION", "EN", "Showing {0} - {1} of {2} results"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD", "CHS", "密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD", "CHT", "密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD", "EN", "Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD_CHARACTER", "CHS", "密码必须至少为8个字符，其中包括1个大写字符，特殊字符和数字字符。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD_CHARACTER", "CHT", "密碼必須至少為8個字符，其中包括1個大寫字符，特殊字符和數字字符。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD_CHARACTER", "EN", "Password must be at least 8 characters including 1 uppercase letter, 1 special character and alphanumeric character."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PENDING", "CHS", "待处理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PENDING", "CHT", "待處理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PENDING", "EN", "Pending"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PERSONAL_INFO", "CHS", "个人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PERSONAL_INFO", "CHT", "個人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PERSONAL_INFO", "EN", "Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PGN", "CHS", "第 {0} 条，共 {1} 条纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PGN", "CHT", "第 {0} 條，共 {1} 條紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PGN", "EN", "Showing {0} of {1} Results"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PHOTO_LIBRARY", "CHS", "从手机相册选择"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PHOTO_LIBRARY", "CHT", "從手機相册選擇"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PHOTO_LIBRARY", "EN", "Select from Photo Library"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PLATFORM", "CHS", "平台"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PLATFORM", "CHT", "平台"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PLATFORM", "EN", "Platform"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP", "CHS", "邮政编码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP", "CHT", "郵政編碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP", "EN", "Postal Code/Zip Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP_OPT", "CHS", "邮政编码（选填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP_OPT", "CHT", "郵政編碼（選填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP_OPT", "EN", "Postal Code / ZIP Code (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVATION", "CHS", "激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVATION", "CHT", "激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVATION", "EN", "Activate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVE", "CHS", "已激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVE", "CHT", "已激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVE", "EN", "Activated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_ID", "CHS", "属性编码代号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_ID", "CHT", "屬性代號"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_ID", "EN", "Attribute ID"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_NAME", "CHS", "属性名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_NAME", "CHT", "屬性名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_NAME", "EN", "Attribute Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_FROM", "CHS", "上架日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_FROM", "CHT", "上架日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_FROM", "EN", "Available From (Date)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_TO", "CHS", "下架日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_TO", "CHT", "下架日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_TO", "EN", "Available To (Date)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_FROM", "CHS", "上架日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_FROM", "CHT", "上架日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_FROM", "EN", "Available From (Date & Time)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_FROM", "CHS", "上架时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_FROM", "CHT", "上架時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_FROM", "EN", "Available From (Time)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_TO", "CHS", "下架时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_TO", "CHT", "下架時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_TO", "EN", "Available To (Time)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TO", "CHS", "下架日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TO", "CHT", "下架日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TO", "EN", "Available To (Date & Time)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BARCODE", "CHS", "条形码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BARCODE", "CHT", "條形碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BARCODE", "EN", "Barcode"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BASIC_INFO", "CHS", "商品信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BASIC_INFO", "CHT", "商品信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BASIC_INFO", "EN", "Product Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BRAND", "CHS", "品牌名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BRAND", "CHT", "品牌名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BRAND", "EN", "Brand Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_CATEGORY_INFO", "CHS", "商品类目信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_CATEGORY_INFO", "CHT", "商品類目資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_CATEGORY_INFO", "EN", "Product Category Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_CODE", "CHS", "颜色编码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_CODE", "CHT", "顏色編碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_CODE", "EN", "Color Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_ID", "CHS", "颜色编码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_ID", "CHT", "顏色編碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_ID", "EN", "Colour Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES", "CHS", "商品颜色图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES", "CHT", "商品顏色圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES", "EN", "Color Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES_NOTE", "CHS", "最多可以添加<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 张图片；已经添加 {0} 张图片；更新<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 张图片 (每张图片不能超过<CONST_PRODUCT_IMG_FILE_SIZE> MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES_NOTE", "CHT", "最多可以添加<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 張圖片；已經添加 {0} 張圖片；更新<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 張圖片(每張圖片不能超過<CONST_PRODUCT_IMG_FILE_SIZE> MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES_NOTE", "EN", "Max. <CONST_PRODUCT_COLOUR_IMG_FILE_NLIMIT> images allowed, {0}/<CONST_PRODUCT_COLOUR_IMG_FILE_NLIMIT> has been uploaded. (Each image with max. <CONST_PRODUCT_IMG_FILE_SIZE> allowed)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_NAME", "CHS", "颜色名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_NAME", "CHT", "顏色名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_NAME", "EN", "Color Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COO", "CHS", "原产地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COO", "CHT", "原產地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COO", "EN", "Country of Origin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION", "CHS", "介绍"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION", "CHT", "介紹"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION", "EN", "Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_AREA", "CHS", "商品说明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_AREA", "CHT", "商品說明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_AREA", "EN", "Product Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES", "CHS", "商品说明图"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES", "CHT", "商品說明圖"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES", "EN", "Description Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES_NOTE", "CHS", "最多可以添加<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 张图片；已经添加 {0} 张图片；更新<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 张图片 (每张图片不能超过<CONST_PRODUCT_IMG_FILE_SIZE> MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES_NOTE", "CHT", "最多可以添加<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 張圖片；已經添加 {0} 張圖片；更新<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 張圖片(每張圖片不能超過<CONST_PRODUCT_IMG_FILE_SIZE> MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES_NOTE", "EN", "Max. <CONST_PRODUCT_DESCRIPTION_IMG_FILE_NLIMIT> images allowed, {0}/<CONST_PRODUCT_DESCRIPTION_IMG_FILE_NLIMIT> has been uploaded. (Each image with max. <CONST_PRODUCT_IMG_FILE_SIZE> allowed)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURE", "CHS", "特征"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURE", "CHT", "特徵"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURE", "EN", "Feature"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES", "CHS", "商品主图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES", "CHT", "商品主圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES", "EN", "Featured Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES_NOTE", "CHS", "最多可以添加<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 张图片；已经添加 {0} 张图片；更新<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 张图片 (每张图片不能超过<CONST_PRODUCT_IMG_FILE_SIZE> MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES_NOTE", "CHT", "最多可以添加<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 張圖片；已經添加 {0} 張圖片；更新<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> 張圖片(每張圖片不能超過<CONST_PRODUCT_IMG_FILE_SIZE> MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES_NOTE", "EN", "Max. <CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> images allowed, {0}/<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT> has been uploaded. (Each image with max. <CONST_PRODUCT_IMG_FILE_SIZE> allowed)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_HEIGHT", "CHS", "高度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_HEIGHT", "CHT", "高度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_HEIGHT", "EN", "Height (cm)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMAGES", "CHS", "商品图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMAGES", "CHT", "商品圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMAGES", "EN", "Product Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMPORT_SAMPLE_TEMPLATE", "CHS", "批量上传 XLSX 模版"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMPORT_SAMPLE_TEMPLATE", "CHT", "批量上傳 XLSX 模版"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMPORT_SAMPLE_TEMPLATE", "EN", "Sample XLSX Template"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVATION", "CHS", "暂停"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVATION", "CHT", "暫停"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVATION", "EN", "Inactivate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVE", "CHS", "已停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVE", "CHT", "已停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVE", "EN", "Inactive"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_DELETE", "CHS", "确认删除此库存纪录？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_DELETE", "CHT", "確認刪除此庫存紀錄？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_DELETE", "EN", "Confirm to delete this inventory record?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_INFORMATION", "CHS", "库存信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_INFORMATION", "CHT", "庫存資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_INFORMATION", "EN", "Inventory Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LENGTH", "CHS", "长度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LENGTH", "CHT", "長度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LENGTH", "EN", "Length (cm)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST", "CHS", "商品列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST", "CHT", "商品列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST", "EN", "Product List"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST_SEARCH_RESULT", "CHS", "找到{0}条有关「{1}」的纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST_SEARCH_RESULT", "CHT", "找到{0}條有關「{1}」的紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST_SEARCH_RESULT", "EN", "{0} search result(s) for ‘’{1}”"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MANFACTURER", "CHS", "生产厂家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MANFACTURER", "CHT", "生產廠家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MANFACTURER", "EN", "Manufacturer Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MATERIAL", "CHS", "材质"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MATERIAL", "CHT", "材質"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MATERIAL", "EN", "Product Material"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MERCHANDISE_CATEGORY", "CHS", "专题类目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MERCHANDISE_CATEGORY", "CHT", "专题类目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MERCHANDISE_CATEGORY", "EN", "Merchandise Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NAME", "CHS", "商品名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NAME", "CHT", "商品名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NAME", "EN", "Product Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NEW_INVENTRY", "CHS", "创建新库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NEW_INVENTRY", "CHT", "創建新庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NEW_INVENTRY", "EN", "Create New Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PENDING", "CHS", "待处理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PENDING", "CHT", "待處理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PENDING", "EN", "Pending"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PRIMARY_CATEGORY", "CHS", "基本类目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PRIMARY_CATEGORY", "CHT", "基本類目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PRIMARY_CATEGORY", "EN", "Primary Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_READY_TO_ACTIVATE", "CHS", "待激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_READY_TO_ACTIVATE", "CHT", "待激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_READY_TO_ACTIVATE", "EN", "Ready to activate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_RETAIL_PRICE", "CHS", "零售价
(人民币)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_RETAIL_PRICE", "CHT", "零售價
(人民幣)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_RETAIL_PRICE", "EN", "Retail Price
(CNY)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SALE_PRICE", "CHS", "销售价
(人民币)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SALE_PRICE", "CHT", "銷售價
(人民幣)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SALE_PRICE", "EN", "Sale Price 
(CNY)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SHIPPING_INFO", "CHS", "运费信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SHIPPING_INFO", "CHT", "運費資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SHIPPING_INFO", "EN", "Shipping Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE", "CHS", "尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE", "CHT", "尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE", "EN", "Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE_CODE", "CHS", "尺码编号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE_CODE", "CHT", "尺码编号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE_CODE", "EN", "Size Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SKU", "CHS", "通用商品代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SKU", "CHT", "通用商品代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SKU", "EN", "SKU"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_ACTIVE_TO_INACTIVE", "CHS", "下架后商品将不会显示在店面。确认将此商品下架？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_ACTIVE_TO_INACTIVE", "CHT", "下架後商品將不會顯示在店面。確認將此商品下架？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_ACTIVE_TO_INACTIVE", "EN", "The product will not be displayed in the storefront once it is inactivated. Confirm to inactivate this product?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_PENDING_TO_ACTIVE", "CHS", "激活后商品将显示在店面。确认激活此商品？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_PENDING_TO_ACTIVE", "CHT", "激活後商品將顯示在店面。確認激活此商品？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_PENDING_TO_ACTIVE", "EN", "The product will be displayed in storefront once it is activated. Confirm to activate this product?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLECODE", "CHS", "产品类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLECODE", "CHT", "產品類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLECODE", "EN", "StyleCode"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLE_CODE", "CHS", "样式代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLE_CODE", "CHT", "樣式代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLE_CODE", "EN", "Style Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TAG_CODE", "CHS", "标签代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TAG_CODE", "CHT", "標籤代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TAG_CODE", "EN", "Tag Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TARGET_CONSUMER_GROUP", "CHS", "目标消费群"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TARGET_CONSUMER_GROUP", "CHT", "目標消費群"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TARGET_CONSUMER_GROUP", "EN", "Target Consumer Group"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_UPC", "CHS", "通用商品代码/条形码 (UPC)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_UPC", "CHT", "通用商品代碼/條形碼 (UPC)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_UPC", "EN", "UPC"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_VARIATIONS", "CHS", "子商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_VARIATIONS", "CHT", "子商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_VARIATIONS", "EN", "Product Variations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WEIGHT", "CHS", "重量 (公斤)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WEIGHT", "CHT", "重量 (公斤)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WEIGHT", "EN", "Weight (kg)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WIDTH", "CHS", "宽度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WIDTH", "CHT", "寬度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WIDTH", "EN", "Width (cm)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_YEAR_LAUNCHED", "CHS", "推出年份"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_YEAR_LAUNCHED", "CHT", "推出年份"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_YEAR_LAUNCHED", "EN", "Year Launched"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_FROM", "CHS", "销售价由"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_FROM", "CHT", "銷售價由"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_FROM", "EN", "Sales Price Available From"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_TO", "CHS", "销售价至"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_TO", "CHT", "銷售價至"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_TO", "EN", "Sales Price Available To"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_TOTAL_INVEN", "CHS", "库存总量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_TOTAL_INVEN", "CHT", "庫存總量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_TOTAL_INVEN", "EN", "Total No. of Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITHOUT_INVENTORY", "CHS", "无库存商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITHOUT_INVENTORY", "CHT", "無庫存商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITHOUT_INVENTORY", "EN", "Products without Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITH_INVENTORY", "CHS", "有库存商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITH_INVENTORY", "CHT", "有庫存商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITH_INVENTORY", "EN", "Products with Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_INFO", "CHS", "商户基本信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_INFO", "CHT", "商戶基本信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_INFO", "EN", "Profile Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_PIC", "CHS", "头像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_PIC", "CHT", "頭像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_PIC", "EN", "Profile Picture"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROVINCE", "CHS", "省"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROVINCE", "CHT", "省"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROVINCE", "EN", "Province"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH", "CHS", "发布"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH", "CHT", "發佈"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH", "EN", "Publish"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISHED", "CHS", "己发布"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISHED", "CHT", "己發佈"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISHED", "EN", "Published"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_CREATE", "CHS", "成功发布新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_CREATE", "CHT", "成功發佈新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_CREATE", "EN", "Total no. of new product(s) published: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_LOADING", "CHS", "正在准备发布，请稍候..."); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_LOADING", "CHT", "正在準備發佈，請稍候..."); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_LOADING", "EN", "Checking the draft products for publish, please wait..."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_NOTE", "CHS", "确定发布所有尚未发布的商品么？发布后商品将出现在用户应用端，用户可以浏览购买该商品。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_NOTE", "CHT", "確定現在發佈尚未所有的商品嗎？發布後商品將出現在用戶應用端，用戶可以瀏覽購買該商品。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_NOTE", "EN", "Are you sure to publish all draft products to the storefront now?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_PRODUCTS", "CHS", "发布所有草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_PRODUCTS", "CHT", "發佈所有草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_PRODUCTS", "EN", "Publish all draft"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_FAILED", "CHS", "发布失败纪录：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_FAILED", "CHT", "發佈失敗紀錄：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_FAILED", "EN", "Total no. of record(s) failed to publish: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_SUCCEED", "CHS", "商品发布成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_SUCCEED", "CHT", "商品發佈成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_SUCCEED", "EN", "Publish Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PULL_DOWN_REFRESH", "CHS", "拉下即可刷新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PULL_DOWN_REFRESH", "CHT", "拉下即可刷新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PULL_DOWN_REFRESH", "EN", "Pull down to refresh page"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REENTER_PASSWORD", "CHS", "再次输入密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REENTER_PASSWORD", "CHT", "再次輸入密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REENTER_PASSWORD", "EN", "Re-enter password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REGISTER", "CHS", "注册"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REGISTER", "CHT", "註冊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REGISTER", "EN", "Register"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REPORT_PROBLEM", "CHS", "举报问题"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REPORT_PROBLEM", "CHT", "舉報問題"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REPORT_PROBLEM", "EN", "Report a Problem"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_ACTIVATION_CODE", "CHS", "重新发送验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_ACTIVATION_CODE", "CHT", "重新發送驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_ACTIVATION_CODE", "EN", "Resend activation code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_CHANGE_EMAIL_LINK", "CHS", "重新发送重置邮箱地址邮件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_CHANGE_EMAIL_LINK", "CHT", "重新發送重置郵箱地址郵件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_CHANGE_EMAIL_LINK", "EN", "Resend Change Email Link"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_REG_LINK", "CHS", "重新发送完成注册邮件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_REG_LINK", "CHT", "重新發送完成註冊郵件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_REG_LINK", "EN", "Resend Registration Link"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET", "CHS", "重置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET", "CHT", "重置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET", "EN", "Reset"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PASSWORD_SMS", "CHS", "重新发送密码重置验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PASSWORD_SMS", "CHT", "重新發送密碼重置驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PASSWORD_SMS", "EN", "Resend Reset Password Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PW_USER", "CHS", "重置密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PW_USER", "CHT", "重置密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PW_USER", "EN", "Reset Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ROLE", "CHS", "角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ROLE", "CHT", "角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ROLE", "EN", "Role"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SAVE", "CHS", "保存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SAVE", "CHT", "保存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SAVE", "EN", "Save"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH", "CHS", "搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH", "CHT", "搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH", "EN", "Search"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PRODUCTS", "CHS", "按名称搜寻商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PRODUCTS", "CHT", "按名稱搜尋商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PRODUCTS", "EN", "Search for Product Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PROD_SKU", "CHS", "按SKU搜寻商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PROD_SKU", "CHT", "按SKU搜尋商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PROD_SKU", "EN", "Search for Product SKU"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEASON_LAUNCHED", "CHS", "推出季节"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEASON_LAUNCHED", "CHT", "推出季節"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEASON_LAUNCHED", "EN", "Season Launched"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEE", "CHS", "查看库存历史纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEE", "CHT", "查看庫存歷史紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEE", "EN", "See the previous activities for this inventory in"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_BRANDS", "CHS", "选择品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_BRANDS", "CHT", "選擇品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_BRANDS", "EN", "Select Brand(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_CITY", "CHS", "选择城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_CITY", "CHT", "選擇城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_CITY", "EN", "Select City"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_COUNTRY", "CHS", "选择国家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_COUNTRY", "CHT", "選擇國家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_COUNTRY", "EN", "Select country"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_MERCHANT_TYPE", "CHS", "选择商户类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_MERCHANT_TYPE", "CHT", "選擇商戶類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_MERCHANT_TYPE", "EN", "Select Merchant Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_OPTION", "CHS", "请选择下列之一："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_OPTION", "CHT", "請選擇下列之一："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_OPTION", "EN", "Please select one of the following:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PRODUCT", "CHS", "选择商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PRODUCT", "CHT", "選擇商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PRODUCT", "EN", "Select Product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PROVINCE", "CHS", "选择省/州/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PROVINCE", "CHT", "選擇省/州/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PROVINCE", "EN", "Select province"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_ROLE", "CHS", "选择角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_ROLE", "CHT", "選擇角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_ROLE", "EN", "Select Role"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_STAFF", "CHS", "选择美美员工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_STAFF", "CHT", "選擇美美員工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_STAFF", "EN", "Select MayMay Staff(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEND", "CHS", "发送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEND", "CHT", "發送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEND", "EN", "Send"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SETTING", "CHS", "设置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SETTING", "CHT", "設置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SETTING", "EN", "Setting"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SIZE", "CHS", "尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SIZE", "CHT", "尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SIZE", "EN", "Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKIPPED", "CHS", "Skipped-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKIPPED", "CHT", "Skipped-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKIPPED", "EN", "Skipped"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_INVENTORY_SINGLE", "CHS", "管理库存 - {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_INVENTORY_SINGLE", "CHT", "管理庫存 - {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_INVENTORY_SINGLE", "EN", "Manage Inventory - {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_EXPORT", "CHS", "已出货订单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_EXPORT", "CHT", "已出貨訂單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_EXPORT", "EN", "Orders Exported"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_PLACE", "CHS", "已下单订单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_PLACE", "CHT", "已下單訂單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_PLACE", "EN", "Order Placed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_SAFETY_STOCK", "CHS", "最少库存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_SAFETY_STOCK", "CHT", "最少庫存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_SAFETY_STOCK", "EN", "Safety Threshold"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_TOTAL_LOCAT", "CHS", "库存位置总数"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_TOTAL_LOCAT", "CHT", "庫存位置總數"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_TOTAL_LOCAT", "EN", "Total no. of Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS", "CHS", "状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS", "CHT", "狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS", "EN", "Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS_UNLIMITED", "CHS", "无限量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS_UNLIMITED", "CHT", "無限量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS_UNLIMITED", "EN", "Unlimited"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP", "CHS", "用户应用端"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP", "CHT", "用戶應用端"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP", "EN", "Storefront App"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP_NAME", "CHS", "美美"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP_NAME", "CHT", "美美"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP_NAME", "EN", "MM"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NAME", "CHS", "街道名称/中央邮政信箱/公司名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NAME", "CHT", "街道名稱/中央郵政信箱/公司名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NAME", "EN", "Street Name / P.O Box / Company Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NUM", "CHS", "街道号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NUM", "CHT", "街道號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NUM", "EN", "Street Number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBDOMAIN", "CHS", "子域名名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBDOMAIN", "CHT", "子域名名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBDOMAIN", "EN", "Subdomain Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBJECT", "CHS", "主题"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBJECT", "CHT", "主題"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBJECT", "EN", "Subject"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUCCESS_UPLOAD_FILE", "CHS", "Your file has been uploaded, to process it please click the import button below.-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUCCESS_UPLOAD_FILE", "CHT", "Your file has been uploaded, to process it please click the import button below.-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUCCESS_UPLOAD_FILE", "EN", "Your file has been uploaded, to process it please click the import button below."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC", "CHS", "附加文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC", "CHT", "附加文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC", "EN", "Supporting Documentations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC_MSG", "CHS", "请在这里上传相关文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC_MSG", "CHT", "請在這裡上傳相關文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC_MSG", "EN", "Please upload the supporting documents here"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SYSTEM", "CHS", "系统"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SYSTEM", "CHT", "系統"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SYSTEM", "EN", "System"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TAKE_PHOTO", "CHS", "拍照"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TAKE_PHOTO", "CHT", "拍照"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TAKE_PHOTO", "EN", "Take Photo"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TIME_ZONE", "CHS", "时区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TIME_ZONE", "CHT", "時區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TIME_ZONE", "EN", "Time zone"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_ALL_AC_USERS", "CHS", "所有用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_ALL_AC_USERS", "CHT", "所有用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_ALL_AC_USERS", "EN", "All Admin Centre Users"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_MERCHANT_STATUS", "CHS", "更改商户状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_MERCHANT_STATUS", "CHT", "更改商戶狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_MERCHANT_STATUS", "EN", "Change Merchant Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_PRODUCT_STATUS", "CHS", "更改商品状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_PRODUCT_STATUS", "CHT", "更改商品狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_PRODUCT_STATUS", "EN", "Change Product Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_USER_STATUS", "CHS", "修改用户账户状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_USER_STATUS", "CHT", "修改用戶帳戶狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_USER_STATUS", "EN", "Change User Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONF_DEL_USER", "CHS", "确认册除用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONF_DEL_USER", "CHT", "確認冊除用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONF_DEL_USER", "EN", "Confirm Delete User"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CREATE_USER", "CHS", "创建新用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CREATE_USER", "CHT", "創建新用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CREATE_USER", "EN", "Create User"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_EMAIL_SEND", "CHS", "邮件已发送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_EMAIL_SEND", "CHT", "郵件已發送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_EMAIL_SEND", "EN", "An Email is Sent"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_FORGOT_PASSWORD", "CHS", "忘记密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_FORGOT_PASSWORD", "CHT", "忘記密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_FORGOT_PASSWORD", "EN", "Forgot Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_LOGOUT", "CHS", "登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_LOGOUT", "CHT", "登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_LOGOUT", "EN", "Logout"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_RESET_PASSWORD", "CHS", "重置密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_RESET_PASSWORD", "CHT", "重置密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_RESET_PASSWORD", "EN", "Reset Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_SEND_ACTIVATION_CODE", "CHS", "请输入您的邮箱地址或手机号码以发送验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_SEND_ACTIVATION_CODE", "CHT", "請輸入您的郵箱地址或手機號碼以發送驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_SEND_ACTIVATION_CODE", "EN", "Please enter your email or mobile no., an activation code will be sent to you"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TOBE_PUBLISHED", "CHS", "即将发布"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TOBE_PUBLISHED", "CHT", "即將發佈"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TOBE_PUBLISHED", "EN", "To be Published"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPDATED", "CHS", "Updated-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPDATED", "CHT", "Updated-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPDATED", "EN", "Updated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD", "CHS", "上传"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD", "CHT", "上傳"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD", "EN", "Upload"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE", "CHS", "上传图像文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE", "CHT", "上傳圖像文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE", "EN", "Upload Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE_FAILED", "CHS", "上传失败图片总数：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE_FAILED", "CHT", "上傳失敗圖片總數：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE_FAILED", "EN", "Total no. of images not uploaded: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMG_DRAG", "CHS", "拖动图片到这里上传"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMG_DRAG", "CHT", "拖動圖片到這裡上傳"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMG_DRAG", "EN", "Drag image(s) here to upload"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_FILE_STATUS", "CHS", "成功上传包含 {0} 个库存纪录的文件 {1}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_FILE_STATUS", "CHT", "成功上傳包含 {0} 個庫存紀錄的文件 {1}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_FILE_STATUS", "EN", "You have just uploaded file: {1} with {0} inventory record(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_TEMPLATE_NOTE", "CHS", "请使用批量上传 xlsx 模版来创建和管理商品库存信息，你可以在这里下载"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_TEMPLATE_NOTE", "CHT", "請使用批量上傳 xlsx 模板來創建和管理商品庫存信息，你可以在這裏下載"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_TEMPLATE_NOTE", "EN", "Please use the xlsx template to create and manage your inventory, you can download a"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_FILE_STATUS", "CHS", "成功上传包含 {0} 件商品的 {1}文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_FILE_STATUS", "CHT", "成功上傳包含 {0} 件商品的 {1} 文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_FILE_STATUS", "EN", "You have just uploaded file: {1} with {0} products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_TEMPLATE_NOTE", "CHS", "请使用批量上传 xlsx 模版来创建和管理商品信息，你可以在这里下载"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_TEMPLATE_NOTE", "CHT", "請使用批量上傳 xlsx 模板來創建和管理商品信息，你可以在這裏下載"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_TEMPLATE_NOTE", "EN", "Please use the xlsx template to create and manage your products, you can download a"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_RESULT", "CHS", "上传结果"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_RESULT", "CHT", "上傳結果"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_RESULT", "EN", "Upload Result"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_SUCCESS", "CHS", "上传的图片总数：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_SUCCESS", "CHT", "上傳的圖片總數：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_SUCCESS", "EN", "Total no. of images uploaded: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_COUNT", "CHS", "{0} 张图片上传失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_COUNT", "CHT", "{0} 張圖片上傳失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_COUNT", "EN", "{0} images upload failed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_IMAGE_NAME", "CHS", "图片名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_IMAGE_NAME", "CHT", "圖片名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_IMAGE_NAME", "EN", "Image Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_REASON", "CHS", "原因"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_REASON", "CHT", "原因"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_REASON", "EN", "Reason"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_COL_TYPE", "CHS", "颜色类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_COL_TYPE", "CHT", "顏色類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_COL_TYPE", "EN", "Color Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_IMG_RANK", "CHS", "图片排名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_IMG_RANK", "CHT", "圖片排名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_IMG_RANK", "EN", "Image Rank"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_SELECT_OPTION", "CHS", "当商品图片已经存在时："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_SELECT_OPTION", "CHT", "當商品圖片已經存在時："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_SELECT_OPTION", "EN", "Please select when image already exist:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_LIMIT", "CHS", "最大上传每次<CONST_PRODUCT_IMG_MAX_NUM> 张图片，每个图片最大<CONST_PRODUCT_IMG_FILE_SIZE>MB"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_LIMIT", "CHT", "最大上傳每次<CONST_PRODUCT_IMG_MAX_NUM> 張圖片，每個圖片最大<CONST_PRODUCT_IMG_FILE_SIZE>MB"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_LIMIT", "EN", "Max. upload <CONST_PRODUCT_IMG_MAX_NUM> images each time, with max size of each image <CONST_PRODUCT_IMG_FILE_SIZE>MB"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_COUNT", "CHS", "{0} 张图片上传成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_COUNT", "CHT", "{0} 張圖片上傳成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_COUNT", "EN", "{0} images upload success"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMAGE_NAME", "CHS", "图片名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMAGE_NAME", "CHT", "圖片名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMAGE_NAME", "EN", "Image Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMG_TYPE", "CHS", "图像类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMG_TYPE", "CHT", "圖像類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMG_TYPE", "EN", "Image Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_PROD_NAME", "CHS", "商品名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_PROD_NAME", "CHT", "商品名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_PROD_NAME", "EN", "Product Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER", "CHS", "用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER", "CHT", "用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER", "EN", "User"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_COMPLETE_REGISTRATION_TITLE", "CHS", "重新发送验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_COMPLETE_REGISTRATION_TITLE", "CHT", "重新發送驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_COMPLETE_REGISTRATION_TITLE", "EN", "Resend Registration Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_MY_PROFILE", "CHS", "编辑个人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_MY_PROFILE", "CHT", "編輯個人資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_MY_PROFILE", "EN", "Edit My Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_PROFILE", "CHS", "编辑{0}的个人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_PROFILE", "CHT", "編輯{0}的個人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_PROFILE", "EN", "Edit {0}\'s Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_VIEW", "CHS", "查看"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_VIEW", "CHT", "查看"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_VIEW", "EN", "View"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_YES", "CHS", "是"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_YES", "CHT", "是"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_YES", "EN", "Yes"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "CHS", "您将向{0}再次发送验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "CHT", "您將向{0}再次發送驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "EN", "You are going to resend registration code by SMS to {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ACTIVE_ACCOUNT_TIPS", "CHS", "賬戶創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ACTIVE_ACCOUNT_TIPS", "CHT", "賬戶創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ACTIVE_ACCOUNT_TIPS", "EN", "You have successfully created a new account."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_COMPLETE_REGISTRATION", "CHS", "请输入密码以完成注册。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_COMPLETE_REGISTRATION", "CHT", "請輸入密碼以完成註冊。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_COMPLETE_REGISTRATION", "EN", "Please enter your password in order to complete your registration."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_ACTIVATE", "CHS", "帐户{0}将被激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_ACTIVATE", "CHT", "帳戶{0}將被激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_ACTIVATE", "EN", "You are going to activate {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_USER", "CHS", "将删除{0}的帐户，请注意，这个操作将不能撤销"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_USER", "CHT", "將刪除{0}的帳戶，請注意，這個操作將不能撤銷"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_USER", "EN", "You are going to delete the account of {0}. This action cannot be reversed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_INACTIVATE", "CHS", "帐户{0}将被停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_INACTIVATE", "CHT", "帳戶{0}將被停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_INACTIVATE", "EN", "You are going to inactivate the account of {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_USER_LOGOUT", "CHS", "您确定要登出？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_USER_LOGOUT", "CHT", "您確定要登出？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_USER_LOGOUT", "EN", "Are you sure you want to logout?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING", "CHS", "您的账户处于待处理状态。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING", "CHT", "您的賬戶處理待處理狀態。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING", "EN", "Your account is in pending status."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING_TIPS", "CHS", "已经发送验证邮件给您，请查看邮箱并确认链接。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING_TIPS", "CHT", "已經發送驗證郵件給您，請查看郵箱并確認鏈接。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING_TIPS", "EN", "An email with a verification link has been sent to you. Please check your email and click the link to confirm your identity."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATE_PENDING_FAIL", "CHS", "未能激活商品，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATE_PENDING_FAIL", "CHT", "未能激活商品，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATE_PENDING_FAIL", "EN", "Fail to activate pending products. Please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATION_CODE_NIL", "CHS", "请输入短信中的验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATION_CODE_NIL", "CHT", "請輸入短信中的驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATION_CODE_NIL", "EN", "Please enter activation code from SMS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLOC_NIL", "CHS", "此项为必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLOC_NIL", "CHT", "此項為必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLOC_NIL", "EN", "This is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLO_FORMAT", "CHS", "库存配额必需为一个正整数"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLO_FORMAT", "CHT", "存貨配額必需為一個正整數"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLO_FORMAT", "EN", "Allocation must be a positive integer"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FORMAT", "CHS", "上架日期时间格式错误，请使用格式：YYYY/MM/DD HH:MM:SS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FORMAT", "CHT", "上架日期時間格式錯誤，请使用格式：YYYY/MM/DD HH:MM:SS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FORMAT", "EN", "Available From (Date & Time) needs to be in format: YYYY/MM/DD HH:MM:SS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FUTURE", "CHS", "上架日期时间不能为过去的时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FUTURE", "CHT", "上架日期時間不能為過去的時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FUTURE", "EN", "Available From (Date & Time) cannot be in the past"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_NIL", "CHS", "上架日期时间是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_NIL", "CHT", "上架日期時間是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_NIL", "EN", "Available From (Date & Time) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FORMAT", "CHS", "下架日期时间格式错误，请使用格式：YYYY/MM/DD HH:MM:SS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FORMAT", "CHT", "下架日期時間格式錯誤，请使用格式：YYYY/MM/DD HH:MM:SS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FORMAT", "EN", "Available To (Date & Time) needs to be in format: YYYY/MM/DD HH:MM:SS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE", "CHS", "下架日期时间不能为过去的时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE", "CHT", "下架日期時間不能為過去的時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE", "EN", "Available To (Date & Time) cannot be in the past"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE_FROM", "CHS", "下架日期时间必须早于上架日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE_FROM", "CHT", "下架日期時間必須早於上架日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE_FROM", "EN", "Available To (Date & Time) cannot be before Available From (Date & Time)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_NIL", "CHS", "下架日期时间是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_NIL", "CHT", "下架日期時間是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_NIL", "EN", "Available To (Date & Time) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_FORMAT", "CHS", "上架时间格式错误，请使用时间格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_FORMAT", "CHT", "上架時間格式錯誤，請使用時間格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_FORMAT", "EN", "Available From (Time) needs to be in time format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_NIL", "CHS", "上架时间是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_NIL", "CHT", "上架時間是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_NIL", "EN", "Available From (Time) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_FORMAT", "CHS", "上架时间格式错误，請使用時間格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_FORMAT", "CHT", "上架時間格式錯誤，請使用時間格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_FORMAT", "EN", "Available From (Time) needs to be in time format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_NIL", "CHS", "上架时间是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_NIL", "CHT", "上架時間是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_NIL", "EN", "Available From (Time) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_DUPLICATED", "CHS", "此条形码已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_DUPLICATED", "CHT", "此條形碼已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_DUPLICATED", "EN", "This barcode already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_NIL", "CHS", "条形码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_NIL", "CHT", "條形碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_NIL", "EN", "Barcode is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_UNIQUE", "CHS", "每件商品只有一个条形码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_UNIQUE", "CHT", "每件商品只有一個條形碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_UNIQUE", "EN", "A product can only have one barcode"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_DUPLICATED", "CHS", "此品牌代码已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_DUPLICATED", "CHT", "此品牌代碼已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_DUPLICATED", "EN", "This Brand Code already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_FORMAT", "CHS", "品牌代码必须在1 - 25个字符之间，应用全大写字母而且没有间隔"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_FORMAT", "CHT", "品牌代碼必須在1 - 25個字符之間，應用全大寫字母而且沒有間隔"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_FORMAT", "EN", "Brand Code must be between 1-25 character, all in uppercase and no spacing allowed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_INVALID", "CHS", "在品牌列表中没有找到你填写的品牌代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_INVALID", "CHT", "在品牌列表中沒有找到你填寫的品牌代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_INVALID", "EN", "Brand Code cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_MONO_BRAND", "CHS", "单品牌代理商只能销售一个品牌的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_MONO_BRAND", "CHT", "單品牌代理商只能銷售一個品牌的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_MONO_BRAND", "EN", "Mono-brand agent can only sell products of 1 brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_NIL", "CHS", "品牌代码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_NIL", "CHT", "品牌代碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_NIL", "EN", "Brand Code is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_SAME", "CHS", "同一件商品只能屬於一個品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_SAME", "CHT", "同一件商品只能屬於一個品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_SAME", "EN", "Products with have same style code belong to one brand."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_UNIQUE", "CHS", "品牌代码已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_UNIQUE", "CHT", "品牌代碼已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_UNIQUE", "EN", "A product can only have one Brand Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME", "CHS", "显示名必须在1 - 25个字符之间。字符可用英文及数字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME", "CHT", "顯示名必須在1 - 25個字符之間。字符可用英文及數字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME", "EN", "Display name must be between 1 - 25 characters. Characters allowed: alphabets and digits"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME_UNIQUE", "CHS", "品牌名称已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME_UNIQUE", "CHT", "品牌名稱已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME_UNIQUE", "EN", "Brand Display Name already exists"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BUSINESS_REG_NUM", "CHS", "请填写营业执照号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BUSINESS_REG_NUM", "CHT", "請填寫營業執照號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BUSINESS_REG_NUM", "EN", "Please fill in your business registration no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CHANGE_STATUS_FAIL", "CHS", "无法更改用户状态。请重试。错误码："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CHANGE_STATUS_FAIL", "CHT", "無法更改用戶狀態，請重試。錯誤碼："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CHANGE_STATUS_FAIL", "EN", "Failed to change status. Please try again later. Error code:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_LOGO_NIL", "CHS", "请根据要求的全部尺寸上传公司商标图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_LOGO_NIL", "CHT", "請根據要求的全部尺寸上傳公司商標圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_LOGO_NIL", "EN", "Please upload company logo image in required sizes"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_NAME", "CHS", "请填写公司名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_NAME", "CHT", "請填寫公司名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_NAME", "EN", "Please fill in your company name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMP_DESC_FORMAT", "CHS", "公司简介必须在1 - 50个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMP_DESC_FORMAT", "CHT", "公司簡介必須在1 - 50個字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMP_DESC_FORMAT", "EN", "Company Description must be between 1 - 50 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CONTACT_ADMIN", "CHS", "请联系管理员解锁账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CONTACT_ADMIN", "CHT", "請聯繫管理員解鎖賬戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CONTACT_ADMIN", "EN", "Please contact your admin to unlock your account."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_INVALID", "CHS", "在原产地列表中没有找到你填写的原产地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_INVALID", "CHT", "在原產地列表中沒有找到你填寫的原產地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_INVALID", "EN", "Country of Origin cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_NIL", "CHS", "原产地是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_NIL", "CHT", "原產地是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_NIL", "EN", "Country of Origin is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_UNIQUE", "CHS", "每件商品只有一个原产地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_UNIQUE", "CHT", "每件商品只有一個原產地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_UNIQUE", "EN", "A product can only have one Country of Origin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CREATE_BRAND_FAIL", "CHS", "未能创建新品牌，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CREATE_BRAND_FAIL", "CHT", "未能創建新品牌，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CREATE_BRAND_FAIL", "EN", "Fail to create brand, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CULTURE_CODE_MISSING", "CHS", "必填项不能为空"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CULTURE_CODE_MISSING", "CHT", "必填項不能為空"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CULTURE_CODE_MISSING", "EN", "Sorry, something is not quite right. Please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EDIT_BRAND_FAIL", "CHS", "未能编辑品牌，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EDIT_BRAND_FAIL", "CHT", "未能編輯品牌，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EDIT_BRAND_FAIL", "EN", "Fail to edit brand, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_CHANGE_FAIL", "CHS", "更改邮箱失敗，请重试。错误码："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_CHANGE_FAIL", "CHT", "更改郵箱失敗，請重試。錯誤碼："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_CHANGE_FAIL", "EN", "Failed to change email! Please try again. Error code:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NIL", "CHS", "请输入您的邮箱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NIL", "CHT", "請輸入您的郵箱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NIL", "EN", "Please enter your Email."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_MATCH", "CHS", "两次输入邮箱不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_MATCH", "CHT", "兩次輸入郵箱不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_MATCH", "EN", "Emails do not match."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_UNIQUE", "CHS", "邮箱地址已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_UNIQUE", "CHT", "郵箱地址已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_UNIQUE", "EN", "Email address already registered"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NO_CHANGE", "CHS", "您输入的电邮地址和现有的一样。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NO_CHANGE", "CHT", "您輸入的電郵地址和現有的一樣。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NO_CHANGE", "EN", "The email you entered is the same as the existing one."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_OR_MOBILE_NIL", "CHS", "请输入邮箱地址或手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_OR_MOBILE_NIL", "CHT", "請輸入郵箱地址或手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_OR_MOBILE_NIL", "EN", "Please enter your email or mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_PATTERN", "CHS", "邮箱格式不正确"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_PATTERN", "CHT", "郵箱格式不正確"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_PATTERN", "EN", "Invalid mail format."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REENTER_NIL", "CHS", "请再次输入您的邮箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REENTER_NIL", "CHT", "請再次輸入您的郵箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REENTER_NIL", "EN", "Please enter your email again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REGISTERED", "CHS", "邮箱地址已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REGISTERED", "CHT", "郵箱地址已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REGISTERED", "EN", "Email address already registered."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ERROR_OCCURS", "CHS", "很抱歉，您的访问出错了，请稍后再试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ERROR_OCCURS", "CHT", "很抱歉，您的訪問出錯了，請稍後再試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ERROR_OCCURS", "EN", "Sorry, something is not quite right, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_INVENTORY_FAIL", "CHS", "下载库存纪录文件失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_INVENTORY_FAIL", "CHT", "下載庫存紀錄文件失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_INVENTORY_FAIL", "EN", "Failed to export the inventory record list, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_PRODUCT_FAIL", "CHS", "下载商品文件失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_PRODUCT_FAIL", "CHT", "下載商品文件失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_PRODUCT_FAIL", "EN", "Failed to export the product list, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIELDNAME_PATTERN", "CHS", "{0}格式不正确"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIELDNAME_PATTERN", "CHT", "{0}格式不正確"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIELDNAME_PATTERN", "EN", "Invalid {0} format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_SIZE", "CHS", "文件大小上限: <CONST_DOC_FILE_SIZE>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_SIZE", "CHT", "文件大小上限: <CONST_DOC_FILE_SIZE>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_SIZE", "EN", "Max. file size: <CONST_DOC_FILE_SIZE>"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_FAIL", "CHS", "上传失败，请检查您的文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_FAIL", "CHT", "上傳失敗，請檢查您的文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_FAIL", "EN", "Failed to upload your file, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_NIL", "CHS", "请选择要上传的文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_NIL", "CHT", "請選擇要上傳的文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_NIL", "EN", "Please select a file to upload"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIRSTNAME_NIL", "CHS", "请输入您的名字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIRSTNAME_NIL", "CHT", "請輸入您的名字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIRSTNAME_NIL", "EN", "Please enter your first name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_NIL", "CHS", "高度(厘米)是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_NIL", "CHT", "高度(厘米)是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_NIL", "EN", "Height (cm) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_POSITIVE", "CHS", "高度(厘米)必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_POSITIVE", "CHT", "高度(厘米)必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_POSITIVE", "EN", "Height (cm) is greater than zero"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_INVALID_KEY", "CHS", "图片文件不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_INVALID_KEY", "CHT", "圖片文件不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_INVALID_KEY", "EN", "Image file does not exist."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_NOT_FOUND", "CHS", "图片文件没有找到"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_NOT_FOUND", "CHT", "圖片文件沒有找到"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_NOT_FOUND", "EN", "Image not found."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_RESIZE", "CHS", "头像大小上限: <CONST_PROFILE_PIC_FILE_SIZE>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_RESIZE", "CHT", "頭像大小上限: <CONST_PROFILE_PIC_FILE_SIZE>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_RESIZE", "EN", "Max. profile image size: <CONST_PROFILE_PIC_FILE_SIZE>"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_UPLOAD", "CHS", "图片文件上传失败。请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_UPLOAD", "CHT", "圖片文件上傳失敗。請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_UPLOAD", "EN", "Failed to upload the image. Please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMPORT_SKU_WORKSHEET_MISSING", "CHS", "Sku worksheet is missing.-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMPORT_SKU_WORKSHEET_MISSING", "CHT", "Sku worksheet is missing.-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMPORT_SKU_WORKSHEET_MISSING", "EN", "Sku worksheet is missing."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INSUFFICIENT_PERMISSION_MERCHANT_CTR", "CHS", "您的账户没有商戶中心的登录权限"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INSUFFICIENT_PERMISSION_MERCHANT_CTR", "CHT", "您的帳戶沒有商戶中心的登錄權限"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INSUFFICIENT_PERMISSION_MERCHANT_CTR", "EN", "You do not have permission to access the Merchant Centre"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_CREDENTIALS", "CHS", "您的用户名或密码不正确，您还有 {0} 重试机会"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_CREDENTIALS", "CHT", "您的用戶名或密碼不正確，尚餘 {0} 次重試機會"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_CREDENTIALS", "EN", "Invalid email/mobile or password combination. {0} more attempts are allowed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_EMAIL_OR_MOBILE", "CHS", "邮箱地址或手机号码不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_EMAIL_OR_MOBILE", "CHT", "郵箱地址或手機號碼不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_EMAIL_OR_MOBILE", "EN", "Email or mobile no. does not exist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO", "CHS", "检测到陌生的地理位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO", "CHT", "檢測到陌生的地理位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO", "EN", "It seems you are trying to login from another location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO_DETAIL", "CHS", "确认地理位置邮件已发送到 {0}，请点击邮件内的确认链接以重新登陆"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO_DETAIL", "CHT", "確認地理位置郵件已發送到 {0}， 請點擊郵件內的確認連結以重新登錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO_DETAIL", "EN", "An email with a verification link has been sent to {0}. Please check your email and click the link to confirm your identity and new location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_MOBILE", "CHS", "手机号码不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_MOBILE", "CHT", "手機號碼不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_MOBILE", "EN", "Mobile no. does not exist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_FAIL", "CHS", "库存地点不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_FAIL", "CHT", "庫存地點不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_FAIL", "EN", "Inventory location does not exist."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_NOT_FOUND", "CHS", "库存地点不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_NOT_FOUND", "CHT", "庫存地點不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_NOT_FOUND", "EN", "Inventory location does not exist."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LASTNAME_NIL", "CHS", "请输入您的姓氏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LASTNAME_NIL", "CHT", "請輸入您的姓氏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LASTNAME_NIL", "EN", "Please enter your last name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_NIL", "CHS", "长度(厘米)是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_NIL", "CHT", "長度(厘米)是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_NIL", "EN", "Length (cm) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_POSITIVE", "CHS", "长度(厘米)必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_POSITIVE", "CHT", "長度(厘米)必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_POSITIVE", "EN", "Length (cm) is greater than zero"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATIONID_NIL", "CHS", "位置代号是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATIONID_NIL", "CHT", "位置代號是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATIONID_NIL", "EN", "Location ID is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_CITY", "CHS", "请选择城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_CITY", "CHT", "請選擇城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_CITY", "EN", "Please select city"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_COUNTRY", "CHS", "国家是必选项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_COUNTRY", "CHT", "國家是必選項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_COUNTRY", "EN", "Country is a mandatory field."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_DISTRICT", "CHS", "请选择区域"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_DISTRICT", "CHT", "請選擇區域"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_DISTRICT", "EN", "Please select district"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_EXTERNAL_CODE", "CHS", "請設置位置外部代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_EXTERNAL_CODE", "CHT", "请设置位置外部代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_EXTERNAL_CODE", "EN", "Please set location external code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_NAME", "CHS", "請設置位置名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_NAME", "CHT", "请设置位置名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_NAME", "EN", "Please set location name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_PROVINCE", "CHS", "请选择省/州/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_PROVINCE", "CHT", "請選擇省/州/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_PROVINCE", "EN", "Please select province"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_SAFETY_STOCK", "CHS", "只输入数字于最少库存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_SAFETY_STOCK", "CHT", "只輸入數字於最少庫存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_SAFETY_STOCK", "EN", "Please only enter digit for Location Safety Threshold"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NAME", "CHS", "请填写街道名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NAME", "CHT", "請填寫街道名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NAME", "EN", "Please fill in a street name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NUMBER", "CHS", "请填写街道号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NUMBER", "CHT", "請填寫街道號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NUMBER", "EN", "Please fill in a street number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_TYPE", "CHS", "请选择位置类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_TYPE", "CHT", "請選擇位置類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_TYPE", "EN", "Please select location type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOC_NIL", "CHS", "请选择库存地点 (至少一个)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOC_NIL", "CHT", "請選擇庫存地點 (至少一個)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOC_NIL", "EN", "Please choose inventory location (at least one)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_LONG", "CHS", "生产厂家不能超过100个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_LONG", "CHT", "生產廠家不能超過100個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_LONG", "EN", "Manufacturer Name cannot be more than 100 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_NIL", "CHS", "生产厂家是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_NIL", "CHT", "生產廠家是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_NIL", "EN", "Manufacturer Name is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_NIL", "CHS", "<LB_PRODUCT_MERCHANDISE_CATEGORY> 是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_NIL", "CHT", "<LB_PRODUCT_MERCHANDISE_CATEGORY> 是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_NIL", "EN", "<LB_PRODUCT_MERCHANDISE_CATEGORY> is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_NOTFOUND", "CHS", "在类目列表中没有找到你填写的专题类目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_NOTFOUND", "CHT", "在類目列表中沒有找到你填寫的專題類目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_NOTFOUND", "EN", "Merchandise Category cannot be found, please check the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_UNIQUE", "CHS", "在每个单元中只能填写一个专题类目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_UNIQUE", "CHT", "在每個單元中只能填寫一個專題類目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_UNIQUE", "EN", "Please put only one Merchandise Category in one cell"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAYNAME_NIL", "CHS", "请输入您的显示名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAYNAME_NIL", "CHT", "請輸入您的顯示名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAYNAME_NIL", "EN", "Please enter a display name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME", "CHS", "显示名必须在1 - 25个字符之间。字符可用英文及数字。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME", "CHT", "顯示名必須介乎於1 - 25個字符。字符可用英文字母及數字。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME", "EN", "Display name must be between 1 - 25 characters. Characters allowed: alphabets and digits."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME_UNIQUE", "CHS", "商户名称已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME_UNIQUE", "CHT", "商戶名稱已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME_UNIQUE", "EN", "Merchant Display Name already exists."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISP_NAME", "CHS", "显示名必须在3 - 25个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISP_NAME", "CHT", "顯示名必須介乎於3 - 25個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISP_NAME", "EN", "Display name must be between 3 - 25 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME", "CHS", "名字必须介乎于1 - 50个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME", "CHT", "名字必須介乎於1 - 50個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME", "EN", "First name must be between 1 - 50 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME_NIL", "CHS", "请输入您的名字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME_NIL", "CHT", "請輸入您的名字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME_NIL", "EN", "Please enter your first name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME", "CHS", "姓氏必须在1 - 50个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME", "CHT", "姓氏必須介乎於1 - 50個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME", "EN", "Last name must be between 1 - 50 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME_NIL", "CHS", "请输入您的姓氏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME_NIL", "CHT", "請輸入您的姓氏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME_NIL", "EN", "Please enter your last name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LIST_FAIL", "CHS", "读取商户列表失败。请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LIST_FAIL", "CHT", "讀取商戶列表失敗。請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LIST_FAIL", "EN", "Failed to load merchant list, Please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_HEADER", "CHS", "文件大小上限: < CONST_LOGO_FILE_SIZE >"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_HEADER", "CHT", "文件大小上限: < CONST_LOGO_FILE_SIZE >"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_HEADER", "EN", "Max. file size: < CONST_LOGO_FILE_SIZE >"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_LARGE", "CHS", "文件大小上限: <CONST_LOGO_FILE_SIZE>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_LARGE", "CHT", "文件大小上限: <CONST_LOGO_FILE_SIZE>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_LARGE", "EN", "Max. file size: <CONST_LOGO_FILE_SIZE>"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_SMALL", "CHS", "文件大小上限: < CONST_LOGO_FILE_SIZE >"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_SMALL", "CHT", "文件大小上限: < CONST_LOGO_FILE_SIZE >"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_SMALL", "EN", "Max. file size: < CONST_LOGO_FILE_SIZE >"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_MIDDLENAME", "CHS", "中间名必须在1 - 50个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_MIDDLENAME", "CHT", "中間名必須介乎於1 - 50個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_MIDDLENAME", "EN", "Middle name must be between 1 - 50 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_NOT_FOUND", "CHS", "商户不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_NOT_FOUND", "CHT", "商戶不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_NOT_FOUND", "EN", "Merchant does not exist."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_STATUS_FAIL", "CHS", "商户状态更新失败。请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_STATUS_FAIL", "CHT", "商戶狀態更新失敗。請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_STATUS_FAIL", "EN", "Upload merchant status error. Please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_TYPE", "CHS", "请选择商户类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_TYPE", "CHT", "請選擇商戶類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_TYPE", "EN", "Please Choose Merchant Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_INVALID", "CHS", "在颜色列表中没有找到你填写的颜色编码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_INVALID", "CHT", "在顏色列表中沒有找到你填寫的顏色編碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_INVALID", "EN", "Color Code cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_UNIQUE", "CHS", "请把具有多个颜色编码的商品的每个代码在列表中分格填写"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_UNIQUE", "CHT", "請把具有多個顏色編碼的商品的每個代碼在列表中分格填寫"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_UNIQUE", "EN", "Please put each code in separated cell for products with multiple Color Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_LONG", "CHS", "颜色名称不能超过25个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_LONG", "CHT", "顏色名稱不能超過25個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_LONG", "EN", "Color Name cannot exceed 25 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_UNIQUE", "CHS", "拥有相同样式代码的商品应该有不同的颜色名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_UNIQUE", "CHT", "擁有相同樣式代碼的商品應該有不同的顏色名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_UNIQUE", "EN", "Color Name is unique for all products with the same Style Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_INVALID", "CHS", "在尺码列表中没有找到你填写的尺码编号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_INVALID", "CHT", "在尺碼列表中沒有找到你填寫的尺碼編號"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_INVALID", "EN", "Size Code cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_UNIQUE", "CHS", "把具有多个尺码编号的商品的每个代码在列表中分格填写"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_UNIQUE", "CHT", "請把具有多個尺碼編號的商品的每個代碼在列表中分格填寫"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_UNIQUE", "EN", "Please put each code in separated cell for products with multiple Size Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_ACTIVATION_CODE_NOT_MATCH", "CHS", "手机号码与验证码不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_ACTIVATION_CODE_NOT_MATCH", "CHT", "手機號碼與驗證碼不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_ACTIVATION_CODE_NOT_MATCH", "EN", "The mobile no. and activation code do not match"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_MATCH", "CHS", "两次输入的手机号码不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_MATCH", "CHT", "兩次輸入的手機號碼不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_MATCH", "EN", "Mobile no. doesn\'t match"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NIL", "CHS", "请输入您的手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NIL", "CHT", "請輸入您的手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NIL", "EN", "Please enter your mobile number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NO_CHANGE", "CHS", "你所输入的手机号码与现有号码一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NO_CHANGE", "CHT", "你所輸入的手機號碼與現有號碼一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NO_CHANGE", "EN", "The mobile no. you entered is the same as the existing one"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_PATTERN", "CHS", "手机号码格式不正确"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_PATTERN", "CHT", "手機號碼格式不正確"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_PATTERN", "EN", "Please enter a valid mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_REGISTERED", "CHS", "手机号码已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_REGISTERED", "CHT", "手機號碼已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_REGISTERED", "EN", "Mobile no. is already registered"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NETWORK_FAIL", "CHS", "网络断掉了，请再试一次吧"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NETWORK_FAIL", "CHT", "網絡斷掉了，請再試一次吧"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NETWORK_FAIL", "EN", "Network is lost. Please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NIL", "CHS", "此项为必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NIL", "CHT", "此項為必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NIL", "EN", "This is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NOT_FOUND", "CHS", "纪录不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NOT_FOUND", "CHT", "紀錄不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NOT_FOUND", "EN", "The record does not exist."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NIL", "CHS", "请输入您的密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NIL", "CHT", "請輸入您的密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NIL", "EN", "Please enter your password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NOT_MATCH", "CHS", "两次密码输入不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NOT_MATCH", "CHT", "兩次密碼輸入不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NOT_MATCH", "EN", "Passwords do not match."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_REENTER_NOT_MATCH", "CHS", "两次输入的密码不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_REENTER_NOT_MATCH", "CHT", "兩次輸入的密碼不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_REENTER_NOT_MATCH", "EN", "Passwords do not match"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_RESET_CODE_EXCEEDED", "CHS", "发送验证码次数超出限制，请联系管理员以重置密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_RESET_CODE_EXCEEDED", "CHT", "發送驗證碼次數超出限制，請聯繫管理員以重置密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_RESET_CODE_EXCEEDED", "EN", "You have reached the max. attempts to send activation code, please contract admin to reset password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_WRONG", "CHS", "密码多次输入错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_WRONG", "CHT", "密碼多次輸入錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_WRONG", "EN", "You have attempted too many times to login."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NIL", "CHS", "基本类目是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NIL", "CHT", "基本類目是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NIL", "EN", "Primary Category is a mandatory field."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NOTFOUND", "CHS", "在分类列表中没有找到你填写的基本类目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NOTFOUND", "CHT", "在分類列表中沒有找到你填寫的基本類目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NOTFOUND", "EN", "Primary Category cannot be found, please check the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_UNIQUE", "CHS", "商品应只有一个基本类目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_UNIQUE", "CHT", "商品應只有一個基本類目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_UNIQUE", "EN", "A product can only have one Primary Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_FORMAT", "CHS", "请上传<CONST_PRODUCT_IMPORT_FILE_FORMAT>文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_FORMAT", "CHT", "請上傳<CONST_PRODUCT_IMPORT_FILE_FORMAT>文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_FORMAT", "EN", "Please upload a < CONST_PRODUCT_IMPORT_FILE_FORMAT> file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_SIZE", "CHS", "文件大小上限: <CONST_PRODUCT_IMPORT_FILE_SIZE>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_SIZE", "CHT", "文件大小上限: <CONST_PRODUCT_IMPORT_FILE_SIZE>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_SIZE", "EN", "Max. file size: <CONST_PRODUCT_IMPORT_FILE_SIZE>"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_COL_LIMIT", "CHS", "颜色图片上限：<CONST_PRODUCT_COLOUR_IMG_FILE_NLIMIT>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_COL_LIMIT", "CHT", "顏色圖片上限：<CONST_PRODUCT_COLOUR_IMG_FILE_NLIMIT>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_COL_LIMIT", "EN", "Max no. of color image: <CONST_PRODUCT_COLOUR_IMG_FILE_NLIMIT>"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_DESC_LIMIT", "CHS", "描述图片上限：<CONST_PRODUCT_DESCRIPTION_IMG_FILE_NLIMIT>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_DESC_LIMIT", "CHT", "描述圖片上限：<CONST_PRODUCT_DESCRIPTION_IMG_FILE_NLIMIT>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_DESC_LIMIT", "EN", "Max no. of description image: <CONST_PRODUCT_DESCRIPTION_IMG_FILE_NLIMIT>"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FEAT_LIMIT", "CHS", "特色图片上限：<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FEAT_LIMIT", "CHT", "特色圖片上限：<CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FEAT_LIMIT", "EN", "Max no. of featured image: <CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT>"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FORMAT", "CHS", "图像文件必须为<CONST_PRODUCT_IMG_FILE_FORMAT>格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FORMAT", "CHT", "圖像文件必須為 <CONST_PRODUCT_IMG_FILE_FORMAT>格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FORMAT", "EN", "Image format must be <CONST_PRODUCT_IMG_FILE_FORMAT>"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_IGN_EXISTING", "CHS", "忽略现有的产品图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_IGN_EXISTING", "CHT", "忽略現有的產品圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_IGN_EXISTING", "EN", "Ignore already existing product images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NIL", "CHS", "请上传至少1张主图片或颜色图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NIL", "CHT", "請上傳至少1張主圖片或顏色圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NIL", "EN", "Please upload at least 1 featured image or color image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NLIMIT", "CHS", "已经达到图像文件上传上限"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NLIMIT", "CHT", "已經達到圖像文件上傳上限"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NLIMIT", "EN", "You have reached the max no. of uploaded image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_COL", "CHS", "找不到颜色，请输入简体中文颜色名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_COL", "CHT", "找不到顏色，請輸入簡體中文顏色名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_COL", "EN", "Cannot map Color. Please input color codes in Simplified Chinese"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_DESC", "CHS", "商品说明配对失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_DESC", "CHT", "商品說明配對失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_DESC", "EN", "Product description mapping failure"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_FEAT", "CHS", "商品特征配对失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_FEAT", "CHT", "商品特徵配對失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_FEAT", "EN", "Product feature mapping failure"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_STYLE", "CHS", "找不到此样式代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_STYLE", "CHT", "找不到此樣式代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_STYLE", "EN", "Cannot map this style code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_SIZE", "CHS", "图片文件大小不能超过<CONST_PRODUCT_IMG_FILE_SIZE>MB"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_SIZE", "CHT", "圖片文件大小不能超過<CONST_PRODUCT_IMG_FILE_SIZE>MB"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_SIZE", "EN", "Image file size cannot exceed <CONST_PRODUCT_IMG_FILE_SIZE>MB"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMG_ALTTEXT_CLIMIT", "CHS", "图像替换文件必须在1-100字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMG_ALTTEXT_CLIMIT", "CHT", "圖像替換文件必須在1-100字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMG_ALTTEXT_CLIMIT", "EN", "Max. no. of characters for image alt text: 100 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMPORT", "CHS", "商品信息上传失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMPORT", "CHT", "商品信息上傳失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMPORT", "EN", "Product import failed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_PUBLISH", "CHS", "当商品至少有一张主图像或与颜色相关的图像并且商品介绍及价格不为零时，商品才能被发布"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_PUBLISH", "CHT", "當商品至少有一張主圖像或與顏色相關的圖像並且商品介紹及價格不為零時，商品才能被發布"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_PUBLISH", "EN", "A product cannot be published if: there is at least one featured or color image, and description and price cannot be null"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_LONG", "CHS", "商品介绍不能超过1000个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_LONG", "CHT", "商品介紹不能超過1000個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_LONG", "EN", "Product description cannot exceed 1,000 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_NIL", "CHS", "商品介绍是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_NIL", "CHT", "商品介紹是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_NIL", "EN", "Product description is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_FEATURE_LONG", "CHS", "商品特徵不能超过1000个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_FEATURE_LONG", "CHT", "商品特徵不能超過1000個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_FEATURE_LONG", "EN", "Product feature cannot exceed 1,000 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_MATERIAL_LONG", "CHS", "商品材质不能超过1000个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_MATERIAL_LONG", "CHT", "商品材質不能超過1000個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_MATERIAL_LONG", "EN", "Product material cannot exceed 1,000 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_DUPLICATE", "CHS", "商品名称已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_DUPLICATE", "CHT", "商品名稱已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_DUPLICATE", "EN", "Product Name already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_LONG", "CHS", "商品名称不能超过50个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_LONG", "CHT", "商品名稱不能超過50個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_LONG", "EN", "Product Name cannot exceed 50 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_NIL", "CHS", "商品名称是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_NIL", "CHT", "商品名稱是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_NIL", "EN", "Product Name is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_NIL", "CHS", "请上传个人信息封面图"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_NIL", "CHT", "請上傳個人信息封面圖"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_NIL", "EN", "Please upload profile banner image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_SIZE", "CHS", "文件大小上限: < CONST_LOGO_FILE_SIZE >"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_SIZE", "CHT", "文件大小上限: < CONST_LOGO_FILE_SIZE >"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_SIZE", "EN", "Max. file size: < CONST_LOGO_FILE_SIZE >"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PUBLISH_DRAFT_FAIL", "CHS", "未能发布商品，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PUBLISH_DRAFT_FAIL", "CHT", "未能發佈商品，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PUBLISH_DRAFT_FAIL", "EN", "Fail to publish the draft products. Please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REFERENCE_FAIL", "CHS", "上传文件失败。请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REFERENCE_FAIL", "CHT", "上傳文件失敗。請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REFERENCE_FAIL", "EN", "Failed to upload the file. Please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REQUIRED_FIELD_MISSING", "CHS", "必填项不能为空"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REQUIRED_FIELD_MISSING", "CHT", "必填項不能為空"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REQUIRED_FIELD_MISSING", "EN", "Please fill in all the required fields."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_OLD_PASSWORD_WRONG", "CHS", "您输入的旧密码不正确"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_OLD_PASSWORD_WRONG", "CHT", "您輸入的舊密碼不正確"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_OLD_PASSWORD_WRONG", "EN", "The old password is incorrect"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_FAIL", "CHS", "无法重置密码。请重试。错误码："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_FAIL", "CHT", "無法重置密碼。請重試。錯誤碼："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_FAIL", "EN", "Failed to reset password. Please try again later. Error code:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_NIL", "CHS", "请输入新密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_NIL", "CHT", "請輸入新密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_NIL", "EN", "Please enter your new password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_REENTER_NIL", "CHS", "请再次输入新密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_REENTER_NIL", "CHT", "請再次輸入新密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_REENTER_NIL", "EN", "Please re-enter your new password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_DECIMAL", "CHS", "<LB_PRODUCT_RETAIL_PRICE> 最多支持小数点后两位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_DECIMAL", "CHT", "<LB_PRODUCT_RETAIL_PRICE> 最多支持小數點後兩位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_DECIMAL", "EN", "<LB_PRODUCT_RETAIL_PRICE> needs to be rounded to 2 digits after the decimal"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_POSITIVE", "CHS", "<LB_PRODUCT_RETAIL_PRICE> 必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_POSITIVE", "CHT", "<LB_PRODUCT_RETAIL_PRICE> 必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_POSITIVE", "EN", "<LB_PRODUCT_RETAIL_PRICE> is a positive number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_NIL", "CHS", "商品零售价是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_NIL", "CHT", "商品零售價是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_NIL", "EN", "Product retail price is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_\bPOSITIVE", "CHS", "商品零售价必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_\bPOSITIVE", "CHT", "商品零售價必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_\bPOSITIVE", "EN", "Product retail price is a positive number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_\b\bDECIMAL", "CHS", "商品零售价最多支持小数点后两位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_\b\bDECIMAL", "CHT", "商品零售價最多支持小數點後兩位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_\b\bDECIMAL", "EN", "Product retail price needs to be rounded to 2 digits after the decimal"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ROLE_NIL", "CHS", "请选择至少一种角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ROLE_NIL", "CHT", "請選擇至少一種角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ROLE_NIL", "EN", "Please choose at least one role"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_DECIMAL", "CHS", "<LB_PRODUCT_SALE_PRICE> 最多支持小数点后两位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_DECIMAL", "CHT", "<LB_PRODUCT_SALE_PRICE> 最多支持小數點後兩位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_DECIMAL", "EN", "<LB_PRODUCT_SALE_PRICE> needs to be rounded to 2 digits after the decimal"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_POSITIVE", "CHS", "<LB_PRODUCT_SALE_PRICE> 必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_POSITIVE", "CHT", "<LB_PRODUCT_SALE_PRICE> 必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_POSITIVE", "EN", "<LB_PRODUCT_SALE_PRICE> is a positive number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_INVALID", "CHS", "商品销售价不可以小于商品零售价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_INVALID", "CHT", "商品銷售價不可以小於商品零售價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_INVALID", "EN", "Product sale price cannot be higher than Product retail price"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_NIL", "CHS", "商品销售价是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_NIL", "CHT", "商品銷售價是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_NIL", "EN", "Product sale price is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_\bPOSITIVE", "CHS", "<LB_PRODUCT_SALE_PRICE> 必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_\bPOSITIVE", "CHT", "<LB_PRODUCT_SALE_PRICE> 必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_\bPOSITIVE", "EN", "<LB_PRODUCT_SALE_PRICE> is a positive number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_\b\bDECIMAL", "CHS", "商品销售价最多支持小数点后两位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_\b\bDECIMAL", "CHT", "商品銷售價最多支持小數點後兩位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_\b\bDECIMAL", "EN", "Product sale price needs to be rounded to 2 digits after the decimal"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEASON_CODE_INVALID", "CHS", "在季节列表中没有找到你填写的商品推出季节"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEASON_CODE_INVALID", "CHT", "在季節列表中沒有找到你填寫的商品推出季節"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEASON_CODE_INVALID", "EN", "Product season launched cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEASON_CODE_NIL", "CHS", "推出季节是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEASON_CODE_NIL", "CHT", "商品推出季節是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEASON_CODE_NIL", "EN", "Product season launched  is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SENDING_MESSAGING", "CHS", "信息发送失败，请稍后再试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SENDING_MESSAGING", "CHT", "信息發送失敗，請稍後再試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SENDING_MESSAGING", "EN", "The message is failed to send, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_DUPLICATED", "CHS", "此通用商品代码已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_DUPLICATED", "CHT", "此通用商品代碼已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_DUPLICATED", "EN", "This SKU code already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_INVALID", "CHS", "通用商品代码不正确"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_INVALID", "CHT", "通用商品代碼不正確"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_INVALID", "EN", "SKU code is incorrect"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_NIL", "CHS", "通用商品代码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_NIL", "CHT", "通用商品代碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_NIL", "EN", "SKU is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_NOT_FOUND", "CHS", "找不到通用商品代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_NOT_FOUND", "CHT", "找不到通用商品代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_NOT_FOUND", "EN", "Sku not found"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_STATUS_FAIL", "CHS", "商品代码错误状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_STATUS_FAIL", "CHT", "商品代碼錯誤狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_STATUS_FAIL", "EN", "Sku Status Fail"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_STATUS_INVALID", "CHS", "无效状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_STATUS_INVALID", "CHT", "無效狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_STATUS_INVALID", "EN", "Status Invalid"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_STATUS_UNKOWN", "CHS", "未知状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_STATUS_UNKOWN", "CHT", "未知状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_STATUS_UNKOWN", "EN", "Unknow Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_UNIQUE", "CHS", "每件商品只有一个通用商品代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_UNIQUE", "CHT", "每件商品只有一個通用商品代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_UNIQUE", "EN", "A product can only have one SKU code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STATUS_INVALID", "CHS", "无效状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STATUS_INVALID", "CHT", "無效狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STATUS_INVALID", "EN", "Status invalid"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STATUS_UNKOWN", "CHS", "未知状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STATUS_UNKOWN", "CHT", "未知狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STATUS_UNKOWN", "EN", "Status unknown"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_DUPLICATED", "CHS", "此样式代码已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_DUPLICATED", "CHT", "此樣式代碼已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_DUPLICATED", "EN", "This style code already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_NIL", "CHS", "样式代码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_NIL", "CHT", "樣式代碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_NIL", "EN", "Style code is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_UNIQUE", "CHS", "在每个单元中只能填写一个样式代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_UNIQUE", "CHT", "在每個單元中只能填寫一個樣式代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_UNIQUE", "EN", "A product can only have one style code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SUBDOMAIN", "CHS", "子域名必须在1 - 25个字符之间。字符可用英文及数字。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SUBDOMAIN", "CHT", "子域名必須介乎於1 - 25個字符。字符可用英文字母及數字。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SUBDOMAIN", "EN", "Subdomain must be between 1 - 25 characters. Characters allowed: alphabets and digits."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TAG_INVALID", "CHS", "在标签列表中没有找到你填写的标签代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TAG_INVALID", "CHT", "在標籤列表中沒有找到你填寫的標籤代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TAG_INVALID", "EN", "This tag code cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_EXTRA", "CHS", "{0}不属于上传模版中定义的字段名，无法创建/更新记录。请使用上传模版中所定义的字段名。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_EXTRA", "CHT", "{0}不屬於上傳模版中定義的字段名，無法創建/更新紀錄。請使用上傳模版中所定義的字段名。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_EXTRA", "EN", "{0} does not belong to the \"Template\" sheet. Please use the correct attribute names."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_MISSING", "CHS", "没有找到{0}，无法创建/更新纪录。请使用上传模版中所定义的字段名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_MISSING", "CHT", "沒有找到{0}，無法創建/更新紀錄。請使用上傳模版中所定義的字段名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_MISSING", "EN", "{0} is missing in the \"Template\" sheet. Please use the correct attribute names"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_WRONG_ORDER", "CHS", "上传的文件中的字段名顺序错误，无法创建/更新纪录。请使用与商品上传模版中相同的表格结构"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_WRONG_ORDER", "CHT", "上傳的文件中的字段名順序錯誤,無法創建/更新紀錄。請使用與商品上傳模版中相同的表格結構"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_WRONG_ORDER", "EN", "The attributes in the \"Template\" sheet in the file you just uploaded are not in the correct order. Please make sure you use the same order in the xlsx template"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_EMPTY", "CHS", "名为 “Template” 的表格里面没有任何纪录，无法创建/更新纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_EMPTY", "CHT", "名為 “Template” 的表格里面沒有任何紀錄，無法創建/更新紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_EMPTY", "EN", "The \"Template\" sheet in the file you just uploaded is empty"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_MISSING", "CHS", "上传的文件中没有名为 “Template” 的表格，无法创建/更新纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_MISSING", "CHT", "上傳的文件中沒有名為 “Template” 的表格，無法創建/更新紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_MISSING", "EN", "We cannot find \"Template\" sheet in the fie you just uploaded"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_WRONG_FORMAT", "CHS", "上传的文件不是 XLSX 格式，无法完成纪录创建/更新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_WRONG_FORMAT", "CHT", "上傳的文件不是 XLSX 格式，無法完成紀錄創建/更新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_WRONG_FORMAT", "EN", "The file you just uploaded is not a xlsx file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_DUPLICATED", "CHS", "<LB_PRODUCT_UPC> 已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_DUPLICATED", "CHT", "<LB_PRODUCT_UPC> 已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_DUPLICATED", "EN", "<LB_PRODUCT_UPC> code already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_INVALID", "CHS", "<LB_PRODUCT_UPC> 不正确"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_INVALID", "CHT", "<LB_PRODUCT_UPC> 不正確"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_INVALID", "EN", "<LB_PRODUCT_UPC> code is not correct"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_NIL", "CHS", "<LB_PRODUCT_UPC> 是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_NIL", "CHT", "<LB_PRODUCT_UPC> 是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_NIL", "EN", "<LB_PRODUCT_UPC> is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_UNIQUE", "CHS", "每件商品只有一个<LB_PRODUCT_UPC>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_UNIQUE", "CHT", "每件商品只有一個<LB_PRODUCT_UPC>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_UNIQUE", "EN", "A product can only have one <LB_PRODUCT_UPC>"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_FAIL", "CHS", "无法激活此用户，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_FAIL", "CHT", "無法激活此用戶，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_FAIL", "EN", "Failed to activate user, please contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_DONE", "CHS", "用户验证无法完成，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_DONE", "CHT", "用戶驗證無法完成，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_DONE", "EN", "User activation token cannot be completed, please contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_EXPIRED", "CHS", "用户验证码己过期。请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_EXPIRED", "CHT", "用戶驗證碼己過期。請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_EXPIRED", "EN", "User activation token has expired. Please contact administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_INVALID", "CHS", "用户验证码無效，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_INVALID", "CHT", "用戶驗證碼無效，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_INVALID", "EN", "User activation token invalid, please contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND", "CHS", "无法找到此用户验证码，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND", "CHT", "無法找到此用戶驗證碼，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND", "EN", "User activation token cannot be found, please contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ATTEMPTS_EXCEEDED", "CHS", "错误登录次数超出限制，请点击邮箱内的确认连结以重置密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ATTEMPTS_EXCEEDED", "CHT", "錯誤登錄次數超出限制，請點擊郵箱內的確認連結以重置密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ATTEMPTS_EXCEEDED", "EN", "You have reached the max. attempts to login, please check your email for activation link"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_EMPTY", "CHS", "请输入您的验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_EMPTY", "CHT", "請輸入您的驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_EMPTY", "EN", "Please enter your user authentication"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_FAIL", "CHS", "认证用户失败。请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_FAIL", "CHT", "認證用戶失敗。請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_FAIL", "EN", "Failed to authenticate the user. Please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_INVALID", "CHS", "密码输入错误，还可以尝试 {0} 次"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_INVALID", "CHT", "密碼輸入錯誤，還可以嘗試 {0} 次"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_INVALID", "EN", "Invalid login credentials. {0} more attempts are allowed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_REACTIVATE_FAIL", "CHS", "无法重新激活用户验证码，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_REACTIVATE_FAIL", "CHT", "無法重新激活用戶驗證碼，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_REACTIVATE_FAIL", "EN", "Failed to reactivate user activation code, please contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_RESEND_FAIL", "CHS", "发送用户验证码失败，请稍后重试或联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_RESEND_FAIL", "CHT", "發送用戶驗證碼失敗，請稍後重試或聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_RESEND_FAIL", "EN", "Failed to send user activation code, please try again later or contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_VALIDATE_FAIL", "CHS", "认证用户验证码失败，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_VALIDATE_FAIL", "CHT", "認證用戶驗證碼失敗，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_VALIDATE_FAIL", "EN", "Failed to validate the activation code. Please contact the administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CORRUPT_DATA", "CHS", "用户信息受损。请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CORRUPT_DATA", "CHT", "用户數據受損。請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CORRUPT_DATA", "EN", "User information is damaged. Please contact the administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CREATION_FAIL", "CHS", "创建用户账户失败，请重试。错误码："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CREATION_FAIL", "CHT", "创建用戶帳戶失敗，請重試。錯誤碼："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CREATION_FAIL", "EN", "Failed to create new user! Please try again. Error code:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_EDITED_FAIL", "CHS", "修改用户信息失败，请重试。错误码："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_EDITED_FAIL", "CHT", "修改用戶信息失敗，請重試。錯誤碼："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_EDITED_FAIL", "EN", "Failed to update user information. Please try again. Error code:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ERROR", "CHS", "用户错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ERROR", "CHT", "用戶錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ERROR", "EN", "User error."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_IL_FAIL", "CHS", "更新用户库存地点失败。请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_IL_FAIL", "CHT", "更新用戶庫存地點失敗。請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_IL_FAIL", "EN", "Fail to update user\'s inventory location, please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LINK_UNKOWN", "CHS", "链接不正确"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LINK_UNKOWN", "CHT", "鏈接不正確"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LINK_UNKOWN", "EN", "The link is not valid."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LIST", "CHS", "读取用户列表失败。请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LIST", "CHT", "讀取用戶列表失敗。請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LIST", "EN", "Failed to load user list. Please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_NOT_FOUND", "CHS", "找不到这用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_NOT_FOUND", "CHT", "找不到這用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_NOT_FOUND", "EN", "The user you are looking for cannot be found"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REACTIVATION_FAIL", "CHS", "重新激活用户失败。请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REACTIVATION_FAIL", "CHT", "重新激活用戶失敗。請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REACTIVATION_FAIL", "EN", "Failed to reactivate user. Please contact administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REQUIRED_FIELD_MISSING", "CHS", "很抱歉，您的访问出错了，请稍后再试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REQUIRED_FIELD_MISSING", "CHT", "很抱歉，您的訪問出錯了，請稍後再試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REQUIRED_FIELD_MISSING", "EN", "Sorry, something is not quite right, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_SAVE_FAIL", "CHS", "更新用户信息失败。请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_SAVE_FAIL", "CHT", "更新用戶信息失敗。請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_SAVE_FAIL", "EN", "Failed to update the user information. Please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_FAIL", "CHS", "用户状态不正确。请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_FAIL", "CHT", "用戶狀態不正確。請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_FAIL", "EN", "User status error. Please contact the administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INACTIVE", "CHS", "你的账户已被暂停使用, 请联系管理员已激活账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INACTIVE", "CHT", "您的帳戶已被暫停使用，請聯繫管理員已激活帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INACTIVE", "EN", "Your account status is inactive. Please contact admin to activate your account"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INVALID", "CHS", "您的帐户无效。请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INVALID", "CHT", "您的帳戶無效。請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INVALID", "EN", "Your account is invalid. Please contact administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_PENDING", "CHS", "你的账户正在等待处理, 请查阅邮件内的激活链接"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_PENDING", "CHT", "您的帳戶正在等待處理，請查閱郵件内的激活鏈接"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_PENDING", "EN", "Your account status is pending. Please check your email for activation link"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_UNKOWN", "CHS", "未知用户状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_UNKOWN", "CHT", "未知用戶狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_UNKOWN", "EN", "User status unknown."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_UNAUTHORIZED", "CHS", "用户权限错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_UNAUTHORIZED", "CHT", "用戶權限錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_UNAUTHORIZED", "EN", "The user is not authorized"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_VALUE_ABSENT", "CHS", "没有找到相应的值"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_VALUE_ABSENT", "CHT", "沒有找到相應的值"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_VALUE_ABSENT", "EN", "Value cannot be found in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_NIL", "CHS", "<LB_PRODUCT_WEIGHT> 是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_NIL", "CHT", "<LB_PRODUCT_WEIGHT> 是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_NIL", "EN", "<LB_PRODUCT_WEIGHT> is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_POSITIVE", "CHS", "<LB_PRODUCT_WEIGHT> 必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_POSITIVE", "CHT", "<LB_PRODUCT_WEIGHT> 必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_POSITIVE", "EN", "<LB_PRODUCT_WEIGHT> is greater than zero"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_NIL", "CHS", "<LB_PRODUCT_WIDTH> 是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_NIL", "CHT", "<LB_PRODUCT_WIDTH> 是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_NIL", "EN", "<LB_PRODUCT_WIDTH> is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_POSITIVE", "CHS", "<LB_PRODUCT_WIDTH> 必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_POSITIVE", "CHT", "<LB_PRODUCT_WIDTH> 必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_POSITIVE", "EN", "<LB_PRODUCT_WIDTH> is greater than zero"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_INVALID", "CHS", "<LB_PRODUCT_YEAR_LAUNCHED> 是一个4位数字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_INVALID", "CHT", "<LB_PRODUCT_YEAR_LAUNCHED> 是一個4位數字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_INVALID", "EN", "<LB_PRODUCT_YEAR_LAUNCHED> is a 4 digit number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_NIL", "CHS", "<LB_PRODUCT_YEAR_LAUNCHED> 是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_NIL", "CHT", "<LB_PRODUCT_YEAR_LAUNCHED> 是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_NIL", "EN", "<LB_PRODUCT_YEAR_LAUNCHED> is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_CREATED", "CHS", "商户新增成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_CREATED", "CHT", "商戶新增成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_CREATED", "EN", "Merchant Create Successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_EDITED", "CHS", "商户修改成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_EDITED", "CHT", "商戶修改成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_EDITED", "EN", "Merchant Edited successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_CHANGE_EMAIL_LINK", "CHS", "重新发送用户登陆链接邮件，用户可以点击邮件中链接使用新的邮箱进行登陆。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_CHANGE_EMAIL_LINK", "CHT", "重新發送用戶登陸鏈接郵件，用戶可以點擊郵件中鏈接使用新的郵箱進行登陸。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_CHANGE_EMAIL_LINK", "EN", "You are going to resend an email with the unique link to the user for login. User can use that link to login with the new email."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_REG_LINK", "CHS", "重新发送完成注册邮件，用户可以点击邮件中链接完成注册。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_REG_LINK", "CHT", "重新發送完成註冊郵件，用戶可以點擊郵件中鏈接完成註冊。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_REG_LINK", "EN", "You are going to resend an email with a verification link. User can use that link for registration."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL", "CHS", "请为{0}输入新的电邮地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL", "CHT", "請為{0}輸入新的電郵地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL", "EN", "Please enter new email address for {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL_LINK", "CHS", "用户将收到验证电子邮件。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL_LINK", "CHT", "用戶將收到驗證電子郵件。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL_LINK", "EN", "An email with verification link will be sent to the user."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_MOBILE", "CHS", "请输入新的手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_MOBILE", "CHT", "請輸入新的手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_MOBILE", "EN", "Type the new mobile no. for {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_PASSWORD", "CHS", "您将为用户{0}重设密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_PASSWORD", "CHT", "您將為用戶{0}重設密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_PASSWORD", "EN", "You are going to reset the password for {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_CHANGE_SUCCESS", "CHS", "手机号码更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_CHANGE_SUCCESS", "CHT", "手機號碼更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_CHANGE_SUCCESS", "EN", "Mobile no. has been updated successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_SENT_SMS", "CHS", "验证码已发送到用户新的手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_SENT_SMS", "CHT", "驗證碼已發送到用戶新的手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_SENT_SMS", "EN", "A SMS is sent to the user\'s new mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PASSIVE_LOGOUT", "CHS", "您的帐户已被登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PASSIVE_LOGOUT", "CHT", "您的帳戶已被登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PASSIVE_LOGOUT", "EN", "Your account has been logged out."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_RESET_PASSWORD_EMAIL_SENT", "CHS", "重置密码邮件已发送到您的邮箱，请通过邮件中的链接重设密码。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_RESET_PASSWORD_EMAIL_SENT", "CHT", "重置密碼郵件已發送到您的郵箱，請通過邮件中的鏈接重設密碼。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_RESET_PASSWORD_EMAIL_SENT", "EN", "An email has been sent to your inbox. Please check your inbox and reset your password by the attached link."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_CREATE", "CHS", "品牌创建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_CREATE", "CHT", "品牌創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_CREATE", "EN", "Create Brand Successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_EDIT", "CHS", "品牌编辑成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_EDIT", "CHT", "品牌編輯成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_EDIT", "EN", "Update Brand Successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_DISP_NAME_CHANGE", "CHS", "<LB_DISP_NAME>更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_DISP_NAME_CHANGE", "CHT", "<LB_DISP_NAME>更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_DISP_NAME_CHANGE", "EN", "<LB_DISP_NAME> updated successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_EMAIL_CHANGE", "CHS", "<LB_EMAIL>更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_EMAIL_CHANGE", "CHT", "<LB_EMAIL>更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_EMAIL_CHANGE", "EN", "<LB_EMAIL> has been updated successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_IMPORT_INVENTORY", "CHS", "成功汇入库存纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_IMPORT_INVENTORY", "CHT", "成功匯入庫存紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_IMPORT_INVENTORY", "EN", "Import Inventory Succeeded"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_EXPORT", "CHS", "库存纪录下载成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_EXPORT", "CHT", "庫存紀錄下載成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_EXPORT", "EN", "Export Inventory Succeeded"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PASSWORD_CHANGE", "CHS", "<LB_PASSWORD>更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PASSWORD_CHANGE", "CHT", "<LB_PASSWORD>更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PASSWORD_CHANGE", "EN", "<LB_PASSWORD> updated successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_ACTIVATE", "CHS", "商品激活成功：新发布 {0} 个商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_ACTIVATE", "CHT", "商品激活成功：新發佈 {0} 個商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_ACTIVATE", "EN", "Product activate successfully: {0} new product(s) activated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_EXPORT", "CHS", "商品下載成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_EXPORT", "CHT", "商品下載成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_EXPORT", "EN", "Export Product Succeeded"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_IMPORT", "CHS", "商品导入成功
新增{0}件商品；更新{1}件商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_IMPORT", "CHT", "商品導入成功
新增{0}件商品；更新{1}件商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_IMPORT", "EN", "Product import succeeded
{0} new product(s) created;  {1} existing product(s) updated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_PUBLISH", "CHS", "商品发布成功：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_PUBLISH", "CHT", "商品發佈成功：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_PUBLISH", "EN", "Product publish succeeded: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_RESEND_ACTIVATION_CODE", "CHS", "重新发送验证码成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_RESEND_ACTIVATION_CODE", "CHT", "重新發送驗證碼成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_RESEND_ACTIVATION_CODE", "EN", "Resend activation code succeeded"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_CREATED", "CHS", "用户创建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_CREATED", "CHT", "用戶創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_CREATED", "EN", "User is successfully created."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_EDITED", "CHS", "用戶信息修改成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_EDITED", "CHT", "用戶信息修改成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_EDITED", "EN", "User is successfully updated."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_IGNORE", "CHS", "忽略"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_IGNORE", "CHT", "忽略"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_IGNORE", "EN", "Ignore"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_OVERWRITE", "CHS", "取代"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_OVERWRITE", "CHT", "取代"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_OVERWRITE", "EN", "Overwrite"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("TEST", "CHS", "TEST1-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("TEST", "CHT", "TEST1-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("TEST", "EN", "TEST1"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("TEST2", "CHS", "TEST2-CHS"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("TEST2", "CHT", "TEST2-CHT"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("TEST2", "EN", "TEST2"); 

