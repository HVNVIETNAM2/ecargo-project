var Q=require('q');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var config=require('../config');

// create reusable transporter object using SMTP transport


// NB! No need to recreate the transporter object. You can use
// the same transporter object for all e-mails

// setup sample e-mail data with unicode symbols
/*
var email = {
    from: 'Albert <adallow@hotmail.com>', // sender address
    to: 'albertdallow@ecargo.com', // list of receivers
    subject: 'May May Test email', // Subject line
    text: 'May May Test email', // plaintext body
    html: '<b>Hi all how is the development going</b>' // html body
};
*/

exports.sendEmail=function(email)
{	
	var deferred = Q.defer();

	var transporter = nodemailer.createTransport(smtpTransport(config.email));
	
	if (config.email.overrideToUser!=null && config.email.overrideToUser!='')
		email.to=config.email.overrideToUser;
	
	if (config.email.overrideFromUser!=null && config.email.overrideFromUser!='')
		email.from=config.email.overrideFromUser;
	
	transporter.sendMail(email, function(error, info){
		if(error){
			deferred.reject(error);
		}		
		else if (info==null)
		{
			deferred.reject("email info object is null");
		}
		else
		{
			//console.log(email.html);
			deferred.resolve(info.response);
		}		
	});
		
	return deferred.promise;
}