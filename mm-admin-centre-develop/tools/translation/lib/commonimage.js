var Q=require('q');
var im = require("imagemagick");
var config=require('../config');

exports.resizeImage=function(imagePath, size){
	var deferred = Q.defer();
	
	var options = {
		width: size,
		height: size + "^",
		customArgs: [
					 "-gravity", "center"
					,"-extent", size + "x" + size
					],
		srcPath: imagePath,
		dstPath: imagePath + "_" + size
	};

	im.resize(options, function(err) {
		if (err)
		{
			deferred.reject( {AppCode:"MSG_ERR_IMAGE_RESIZE"});
		}
		else
		{
			deferred.resolve(true);
		}
	});
	
	return deferred.promise;
	
};
