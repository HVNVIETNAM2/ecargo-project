var Q=require('q');
var config=require('../config');

exports.createErrorResult=function (err,defaultMessage)
{
		var resErr={};
		if (err.AppCode)
		{
			resErr.AppCode=err.AppCode;
			resErr.Exception='';
			resErr.Message='';
			resErr.Stack='';
		}
		else
		{
			resErr.AppCode=defaultMessage;
			resErr.Message=err.message;
			resErr.Exception=err;
			resErr.Stack=err.stack;
		}
		return resErr;	
}

exports.createErrorResultSimple=function (defaultMessage)
{
		var resErr={};
		resErr.AppCode=defaultMessage;
		resErr.Exception='';
		resErr.Message='';
		resErr.Stack='';
		return resErr;	
}

exports.checkIdBlank=function (idValue)
{
	if (idValue===null || idValue===undefined || idValue===0 || idValue==='' || idValue==='0')
		return true;
	else
		return false;
}

exports.isBlank=function (val)
{
	if (val===null || val===undefined || val==='')
		return true;
	else
		return false;
}
