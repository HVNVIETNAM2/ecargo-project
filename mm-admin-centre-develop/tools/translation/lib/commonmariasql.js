var Client = require('mariasql');
var Q=require('q');

function Connection(configIn) {

  if (!configIn.charset)
	configIn.charset='utf8';
  
  if (!(this instanceof Connection)) {
    return new Connection(configIn);
  }

  this.config = configIn;
  this.client = null;
};

Connection.prototype.queryMany = function query(sql,paramss) {
	var deferred = Q.defer();
	this.client.query(sql,paramss, function(err, rows){
		if (err)
			deferred.reject(err);
		else
		{
			convertRows(rows);
			//delte of info must occur after the convert
			delete rows.info;						
			deferred.resolve(rows);	
		}
	});
	return deferred.promise;
}

Connection.prototype.queryOne = function query(sql,paramss) {
	var deferred = Q.defer();
	this.client.query(sql,paramss, function(err, rows){
		if (err)
		{
			deferred.reject(err);
		}
		else
		{
			if (rows.length>0)
			{
				convertRowSingle(rows);
				//delte of info must occur after the convert
				delete rows.info;
				deferred.resolve(rows[0]);
			}
			else
				deferred.resolve(null);							
		}	
	});
	return deferred.promise;
}

Connection.prototype.queryExecute = function query(sql,paramss) {
	var deferred = Q.defer();
	this.client.query(sql,paramss, function(err, rows){
		if (err)
			deferred.reject(err);
		else
			deferred.resolve(rows.info);		
	});
	return deferred.promise;
}

Connection.prototype.begin=function begin()
{
	var refe=this;
	var deferred = Q.defer();
	if (refe.client!=null)
		deferred.reject("Error: Begin already active.");
	refe.client = new Client(refe.config);
	refe.client.query("start transaction;", function(err, rows){
		if (err)
			deferred.reject(err);
		else
			deferred.resolve(rows.info);		
	});
	return deferred.promise;
}

Connection.prototype.commit=function commit() 
{
	var refe=this;
	var deferred = Q.defer();
	if (refe.client==null)
	{
		deferred.resolve(); //if no existing connection there can be no transtion so skip over this
	}
	else
	{
		
		refe.client.query("commit;", function(err, rows){
			refe.client.end(); //this will close the connection once the query queue completes
			refe.client=null;
			if (err)
				deferred.reject(err);
			else
			{			
				deferred.resolve(rows.info);		
			}
		});
		
	}
	return deferred.promise;
}

Connection.prototype.rollback=function commit() 
{
	var refe=this;
	var deferred = Q.defer();
	if (refe.client==null)
	{
		deferred.resolve();  //if no existing connection there can be no transtion so skip over this
	}
	else
	{
		refe.client.query("rollback;", function(err, rows){
			refe.client.end(); //this will close the connection once the query queue completes
			refe.client=null;
			if (err)
				deferred.reject(err);
			else
			{			
				deferred.resolve(rows.info);		
			}
		});			
	}
	return deferred.promise;
}

var convert = function(row, metadata) {
	//Convert row elements based on type info.
	var key, t, _results;
	_results = [];
	if (row==null)
		return null;
		
	for (key in row) {
		if (metadata[key] && metadata[key]['type']) //only convvert if we have meta data
		{
			t = metadata[key]['type'];
			if (t === "DATE" || t === "DATETIME" || t === "TIMESTAMP") {
			  row[key] = new Date(row[key]);
			}
			if (t === "DECIMAL" || t === "DOUBLE" || t === "FLOAT") {
			  row[key] = parseFloat(row[key]);
			}
			if (t === "INTEGER" || t === "TINYINT" || t === "SMALLINT" || t === "MEDIUMINT" || t === "BIGINT") {
			  _results.push(row[key] = parseInt(row[key]));
			} else {
			  _results.push(void 0);
			}
		}
	}
	return _results;
};

var convertRows = function(rows) {
	for (var i in rows)
		convert(rows[i], rows.info.metadata)
	
};

var convertRowSingle = function(rows) {
	convert(rows[0], rows.info.metadata)
};

module.exports = Connection;