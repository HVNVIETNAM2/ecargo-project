var express = require('express');
var router = express.Router();
var async = require('async');
var webToken = require('jsonwebtoken');
var jwt = require('express-jwt');
var endOfLine = require('os').EOL;

var fs = require('fs');

var _ = require('lodash');

var Client = require('mariasql');

var c = new Client({
	host: 'localhost',
	user: 'root',
	password: '3nations',
	db : "maymay",
	metadata : true,
	poolsize : 1,
	charset: "utf8"
});

var auth = jwt({ secret : 'OURDIRTYLILSECRET', userProperty : 'payload' });

/* GET home page. */
router.get('/', function(req, res, next){
	res.render('index');
});

var users = [{
	id : 1,
	username : "raitho",
	password : "123456"
}];

router.post('/login', function(req, res, next){
	var data = req.body;

	var user = _.find(users, { username : data.username, password : data.password });

	if(user){
		var today = new Date();
		var exp = new Date(today);
		exp.setDate(today.getDate() + 60);

		var token = webToken.sign({
			_id : this.id,
			username : this.username,
			exp : parseInt(exp.getTime() / 1000)
		}, 'OURDIRTYLILSECRET');

		res.status(200).json({ token : token });
	}else{
		res.status(400).send("Invalid login information.")
	}
});		

router.get('/translations/list', auth, function(req, res, next){
	c.query('SELECT DISTINCT TranslationCode FROM Translation', function(err, data){
		if (err) throw err;
		async.forEach(data, function(item, done){
			c.query('SELECT CultureCode,TranslationName FROM Translation WHERE TranslationCode = ? ', [ item.TranslationCode ], function(err, rows){
				_.forEach(rows, function(n){
					item[n.CultureCode] = n.TranslationName;
				});
				done();
				// process.nextTick(function() { done(); });
			});
		}, function(err){
			c.end();
			res.status(200).send(data);
		});
	});
});

router.post('/translations/save', auth, function(req, res, next){
	var data = req.body;
	c.query('SELECT * FROM Translation WHERE TranslationCode = ?', [ data.TranslationCode ], function(err, rows){
		if(err) throw err;
		if(rows && rows.length > 0){
			// Already exist, do an update.
			if(data.edit == "true"){
				async.parallel([
					function(done){ 
						c.query('UPDATE Translation SET TranslationName = ? WHERE TranslationCode = ? AND CultureCode = ?', [ data.EN, data.TranslationCode, "EN" ], done);
					},
					function(done){ 
						c.query('UPDATE Translation SET TranslationName = ? WHERE TranslationCode = ? AND CultureCode = ?', [ data.CHT, data.TranslationCode, "CHT" ], done);
					},
					function(done){ 
						c.query('UPDATE Translation SET TranslationName = ? WHERE TranslationCode = ? AND CultureCode = ?', [ data.CHS, data.TranslationCode, "CHS" ], done);
					}
				], function(err){
					if(err) throw err;
					console.log("Successfully updated translation.");
					c.end();
					res.status(200).send("Successfully updated translation.");
				});
			}else{
				c.end();
				res.status(400).send("Translation already exists.");
			}
		}else{
			// Not yet in database. insert.
			async.parallel([
				function(done){ 
					c.query('INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES(?, ?, ?) ', [ data.TranslationCode, "EN", data.EN ], done);
				},
				function(done){ 
					c.query('INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES(?, ?, ?) ', [ data.TranslationCode, "CHS", data.CHS ], done);
				},
				function(done){ 
					c.query('INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES(?, ?, ?) ', [ data.TranslationCode, "CHT", data.CHT ], done);
				}
			], function(err){
				if(err) throw err;
				console.log("Successfully inserted translation.");
				c.end();
				res.status(200).send("Successfully inserted translation.");
			});
		}
	});
});	

router.post('/translations/remove', auth, function(req, res, next){
	var data = req.body;
	c.query('DELETE FROM Translation WHERE TranslationCode = ? ', [ data.TranslationCode ], function(err, remove){
		if(err) throw err;
		console.log("Successfully remove translation.");
		c.end();
		res.status(200).send("Successfully remove translation.");
	});
});

router.post('/translations/export', function(req, res, next){
	try{
		var codes = req.body.code;
		var tmpString = "";
		async.forEach(codes, function(code, done){
			c.query('SELECT * FROM Translation WHERE TranslationCode = ? ', [ code ], function(err, rows){
				_.forEach(rows, function(n){
					tmpString += 'INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("'+n.TranslationCode+'", "'+n.CultureCode+'", "'+n.TranslationName+'"); \n';
				});
				tmpString += "\n";
				done();
			});
		}, function(err){
			if(err){ throw err; }
			console.log("Start translation export.");
			c.end();

			fs.writeFile('public/Translation.sql', tmpString, function (err){
			  	if(err){ throw err; }
			  	res.status(200).send({ file : "Translation.sql" });
			});
			
			// var filename = new Date().getTime() + "-Translation.sql"
			// res.setHeader('Content-Type', 'application/octet-stream');
			// res.setHeader('Content-Disposition', 'attachment; filename=' + filename);
			// res.write(tmpString, 'utf-8');
			// res.end();
		});
	}catch(err){
		throw err;
	}
});

module.exports = router;