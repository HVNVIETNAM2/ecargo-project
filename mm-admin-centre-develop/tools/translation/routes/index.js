var express = require('express');
var router = express.Router();
var webToken = require('jsonwebtoken');
var jwt = require('express-jwt');

var fs = require("fs");
var qfs = require("qfs-node");
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var throat = require('throat');
var moment = require('moment');

var promisePipe = require("promisepipe");

var XlsxTemplate = require('xlsx-template');
var Excel = require('exceljs');

var cm = require('../lib/commonmariasql.js');

var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();

var dbConfig = {
	host: 'localhost',
	user: 'root',
	password: '3nations',
	db : "mm",
	metadata : true,
	poolsize : 1,
	charset: "utf8"
};

var configs = {
	throat: 1
};

var auth = jwt({ secret : 'OURDIRTYLILSECRET', userProperty : 'payload' });

/* GET home page. */
router.get('/', function(req, res, next){
	res.render('index');
});

var users = [{
	id : 1,
	username : "raitho",
	password : "123456"
}];

router.post('/login', function(req, res, next){
	var data = req.body;
	var user = _.find(users, { username : data.username, password : data.password });

	if(user){
		var today = new Date();
		var exp = new Date(today);
		exp.setDate(today.getDate() + 60);

		var token = webToken.sign({
			_id : this.id,
			username : this.username,
			exp : parseInt(exp.getTime() / 1000)
		}, 'OURDIRTYLILSECRET');

		res.status(200).json({ token : token });
	}else{
		res.status(400).send("Invalid login information.")
	}
});		

router.get('/translations/list', auth, function(req, res, next){
	var tr = new cm(dbConfig);
	var TranslationList = null;

	Q.when().then(function(){
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationList Starting'+ ', Line Number:78');
		return tr.begin();
	}).then(function(){
		return tr.queryMany("SELECT * FROM Translation").then(function(data){
			TranslationList = data;
		});
	}).then(function(){
		// console.timeEnd("translationlist");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY h:mm:ss a')+', Message:TranslationList Finished'+ ', Line Number:99');
		res.status(200).json(TranslationList);
		return tr.commit();
	}).catch(function(err){
		res.status(400).json("Error fetching translation listing");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY h:mm:ss a')+', Message:TranslationList Error('+err+')'+ ',Line Number:104');
		console.log('Rollback');
		console.dir(err);
		return tr.rollback();
	}).done();
});

// router.get('/translations/list', auth, function(req, res, next){
// 	var tr = new cm(dbConfig);
// 	var TranslationList = null;

// 	Q.when().then(function(){
// 		// console.time("translationlist");
// 		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationList Starting'+ ', Line Number:78');
// 		return tr.begin();
// 	}).then(function(){
// 		return tr.queryMany("SELECT DISTINCT TranslationCode FROM Translation").then(function(data){
// 			// console.log(data);
// 			// console.log('-----');
// 			TranslationList = data;
// 		});
// 	}).then(function(){
// 		var promises = TranslationList.map(throat(configs.throat, function(item){
// 			return tr.queryMany("SELECT CultureCode, TranslationName FROM Translation WHERE TranslationCode = :TranslationCode", { TranslationCode: item.TranslationCode }).then(function(data){
// 				// console.log(data);
// 				// console.log('----');
// 				for(var i in data){
// 					item[data[i].CultureCode] = data[i].TranslationName;
// 				}
// 			});
// 		}));
// 		return Q.all(promises);
// 	}).then(function(){
// 		// console.timeEnd("translationlist");
// 		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY h:mm:ss a')+', Message:TranslationList Finished'+ ', Line Number:99');
// 		res.status(200).json(TranslationList);
// 		return tr.commit();
// 	}).catch(function(err){
// 		res.status(400).json("Error fetching translation listing");
// 		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY h:mm:ss a')+', Message:TranslationList Error('+err+')'+ ',Line Number:104');
// 		console.log('Rollback');
// 		console.dir(err);
// 		return tr.rollback();
// 	}).done();
// });

router.post('/translations/save', auth, function(req, res, next){
	var body = req.body;
	var tr = new cm(dbConfig);
	var Translation = null;

	var sqlQuery = null;

	Q.when()
	.then(function(){
		// console.time("translationsave");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY h:mm:ss a')+', Message:Translation Save Starting'+ ', Line Number:121');
		return tr.begin();
	}).then(function(){
		return tr.queryOne("SELECT * FROM Translation WHERE TranslationCode = :TranslationCode", { TranslationCode : body.TranslationCode }).then(function(data){
			// console.log(data);
			// console.log('----');
			Translation = data;
		});
	}).then(function(){
		if(Translation == null){
			return sqlQuery = 'INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES(:TranslationCode, :CultureCode, :TranslationName) ';
		}else{
			if(Translation != null && body.edit == "true"){
				return sqlQuery = 'UPDATE Translation SET TranslationName = :TranslationName WHERE TranslationCode = :TranslationCode AND CultureCode = :CultureCode';
			}else{
				throw "Translation already exists.";
			}
		}
	}).then(function(){
		if(sqlQuery){
			return Q.when()
			.then(function(){
				var languages = ["EN", "CHT", "CHS"];

				var promises = languages.map(throat(configs.throat, function(item){
					return tr.queryExecute(sqlQuery, {
						TranslationName : body[item],
						TranslationCode : body.TranslationCode,
						CultureCode : item
					});
				}));	

 				return Q.all(promises);	
			}).then(function(){
				console.log("Successfully save translation.");
				return "Successfully save translation.";
			});
		}else{
			throw "Error saving translation";
		}
	}).then(function(){
		return tr.commit();
	}).then(function(message){
		// console.timeEnd("translationsave");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY h:mm:ss a')+', Message: TranslationSave Finished'+', Line Number:165');
		console.log(message);
		res.status(200).send("Successfully save translation");
		console.log("commiting changes");
		return;
	}).catch(function(err){
		res.status(400).json(err || "Error saving translation listing");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY h:mm:ss a')+', Message:Translation Save Error('+err+')'+ ', Line Number:172');
		console.log('Rollback');
		console.dir(err);
		return tr.rollback();
	}).done();
});	

router.post('/translations/remove', auth, function(req, res, next){
	var body = req.body;
	var tr = new cm(dbConfig);

	Q.when().then(function(){
		// console.time("translationremove")
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:Translation Remove Starting'+ ', Line Number:185');
		return tr.begin();
	}).then(function(){
		return tr.queryExecute("DELETE FROM Translation WHERE TranslationCode = :TranslationCode", { TranslationCode : body.TranslationCode }).then(function(data){
			// console.log(data);
			// console.log('----');
			console.log("Successfully remove translation.");
		});
	}).then(function(){
		// console.timeEnd("translationremove");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationRemoved Finished'+ ', Line Number:195');

		res.status(200).send("Successfully remove translation.");
		return tr.commit();
	}).catch(function(err){
		res.status(400).json("Error removing translation.");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationRemoved Error('+err+')'+ ', Line Number:201');
		console.log('Rollback');
		console.dir(err);
		return tr.rollback();
	}).done();
});

router.post('/translations/export', function(req, res, next){
	try{
		var codes = req.body.code;
		var tmpString = "";
		var tr = new cm(dbConfig);

		Q.when().then(function(){
			// console.time("translationexportsql")
			console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationExportSQL Starting'+ ', Line Number:216');
			return tr.begin();
		}).then(function(){
			var promises = codes.map(throat(configs.throat, function(item){
				// console.log(item);
				return tr.queryMany("SELECT * FROM Translation WHERE TranslationCode = :TranslationCode", { TranslationCode: item }).then(function(data){
					// console.log(data);
					// console.log('---');
					for(var i in data){
						var TranslationName = data[i].TranslationName;
						if(TranslationName){
							TranslationName = TranslationName.replace(/"/g, '\\"');
							TranslationName = TranslationName.replace(/'/g, "\\'");
							TranslationName = TranslationName.replace(/\\\\"/g, '\\"');
							TranslationName = TranslationName.replace(/\\\\'/g, "\\'");
						}
						
						tmpString += 'INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("'+data[i].TranslationCode+'", "'+data[i].CultureCode+'", "'+TranslationName+'"); \n';
					}

					tmpString += "\n";
				});
			}));

			return Q.all(promises);
		}).then(function(){
			return qfs.writeFile('public/Translation.sql', tmpString);
		}).then(function(){
			// console.timeEnd("translationexportsql");
			console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message: TranslationExportSQL Finished'+ ', Line Number:245');
			res.status(200).send({ file : "Translation.sql" });
			return tr.commit();
		}).catch(function(err){
			res.status(400).json("Error exporting translation.");
			console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationExportSQL Error('+err+')'+ ', Line Number:250');
			console.log('Rollback');
			console.dir(err);
			return tr.rollback();
		}).done();
	}catch(err){
		throw err;
	}
});

router.post("/translations/exportXLSX", function(req, res){
	var tr = new cm(dbConfig);
	var TranslationList = null;
	var fileName = null;
	var fileLocation = null;

	Q.when().then(function(){
		// console.time("translationexportexcel");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationExportXLSX Starting'+ ', Line Number:268');
		return tr.begin();
	}).then(function(){
		return tr.queryMany("SELECT DISTINCT TranslationCode FROM Translation").then(function(data){
			//console.log(data);
			TranslationList = data;
		});
	}).then(function(){
		var promises = TranslationList.map(throat(configs.throat, function(item){
			// console.log(item);
			// console.log('---');
			return tr.queryMany("SELECT CultureCode, TranslationName FROM Translation WHERE TranslationCode = :TranslationCode", { TranslationCode: item.TranslationCode }).then(function(data){
				var TranslationCode = JSON.stringify(item.TranslationCode).replace(/"/g, "");
				item.TranslationCode = TranslationCode;
				for(var i in data){
					var text = '';
					if(data[i].TranslationName){
						text = data[i].TranslationName.toString();
					}
					
					item[data[i].CultureCode] = text;
				}
			});
		}));
		return Q.all(promises);
	}).then(function(){
		var newFolder = __dirname + '/../public/exports';
		var newFolder = path.resolve(newFolder);

		return qfs.stat(newFolder).then(function(ret){
			console.log("already had a directory skipped creation.");
			return;
		}, function(err){
			console.log("not a directory create one.")
			return qfs.mkdir(newFolder);
		});
	}).then(function(){
		return qfs.readFile(path.join(__dirname, '../public/translation-template.xlsx')).then(function(data){
			var template = new XlsxTemplate(data);
			var sheetNumber = 1;
			var dateToday = new Date().toString();
			template.substitute(sheetNumber, { extractDate : dateToday, translation : TranslationList });
			var data = template.generate();

			fileName = "Translations-" + new Date().getTime() + ".xlsx";
			fileLocation = path.join(__dirname, '../public/exports/' + fileName);

			return qfs.writeFile(fileLocation, data, 'binary').then(function(){
				console.log('Created translation xlsx' + fileName);	
			});
		});	
		
	}).then(function(){
		// console.timeEnd("translationexportexcel");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationExportXLSX Finished'+ ', Line Number:322');
		res.status(200).json({ fileName : fileName, fileLocation : fileLocation });
		return tr.commit();
	}).catch(function(err){
		res.status(400).json("Error fetching translation listing");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationExportXLSX Error('+err+')'+ ', Line Number:327');
		console.log('Rollback');
		console.dir(err);
		return tr.rollback();
	}).done();
});

router.post('/translations/import', [ auth, multipartyMiddleware ], function(req, res, next){
	//console.log(req.files);
	var fileName = new Date().getTime() + ".xlsx";
	var uploadPath = "public/imports/" + fileName;

	var tr = new cm(dbConfig);
	var TranslationList = [];
	var row1 = [];

	var workbook = new Excel.Workbook();

	Q.when().then(function(){
		// console.time("translationimport");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationImport Starting'+ ', Line Number:347');
		return tr.begin();
	}).then(function(){
		var newFolder = __dirname + '/../public/imports';
		var newFolder = path.resolve(newFolder);

		return qfs.stat(newFolder).then(function(ret){
			console.log("already had a directory skipped creation.");
			return;
		}, function(err){
			console.log("not a directory create one.")
			return qfs.mkdir(newFolder);
		});
	}).then(function(){
		console.log("Start uploading imported file.");
		return promisePipe(
		    fs.createReadStream(req.files.file.path),
		    fs.createWriteStream(uploadPath)
		);
	})
	.then(function(){
		console.log("Start reading imported file.");
		return workbook.xlsx.readFile(uploadPath);
	}).then(function(){
		var worksheet = workbook.getWorksheet("Sheet1");
		if (worksheet == null) throw { error : "Worksheet not found." }
		worksheet.eachRow(function(row, rowNumber){
			console.log("Start reading rowNumber: ", rowNumber);
			if(rowNumber == 4){
				row.eachCell(function(cell, colNumber){
					var value = cell.value.replace(/ /g, '');
					row1.push(value);
				});
			}

			if(rowNumber > 4){
				var rowData = { rowNumber : rowNumber };
				row.eachCell({ includeEmpty: true }, function(cell, colNumber){
					var value = cell.value;
					rowData[row1[colNumber - 1]] = value;
				});

				TranslationList.push(rowData);
			}

			if(row==worksheet.lastRow){
				// console.log("----LastRow");
				// console.timeEnd("eachrow");
			}
		});
	}).then(function(){
		console.log("process all data in translation list.");
		var promises = TranslationList.map(throat(configs.throat, function(item){
			var tmpData = null;
			console.log("start updating item: ", item.TranslationCode);
			return Q.when()
			.then(function(){
				var sqlQuery = "SELECT * FROM Translation WHERE TranslationCode = :TranslationCode";
				return tr.queryOne(sqlQuery, item).then(function(data){
					tmpData = data;
					return;
				});
			})
			// Insert Data..
			.then(function(){
				if(tmpData == null){
					var sqlQuery = "INSERT INTO Translation (TranslationCode,CultureCode,TranslationName) VALUES (:TranslationCode,'EN',:English)";
					return tr.queryExecute(sqlQuery, item);
				}
			})
			.then(function(){
				if(tmpData == null){
					var sqlQuery = "INSERT INTO Translation (TranslationCode,CultureCode,TranslationName) VALUES (:TranslationCode,'CHS',:ChineseSimplified)";
					return tr.queryExecute(sqlQuery, item);
				}
			})
			.then(function(){
				if(tmpData == null){
					var sqlQuery = " INSERT INTO Translation (TranslationCode,CultureCode,TranslationName)VALUES (:TranslationCode,'CHT',:ChineseTraditional)";
					return tr.queryExecute(sqlQuery, item);
				}
			})
			// Update Data...
			.then(function(){
				if(tmpData != null){
					var sqlQuery = "UPDATE Translation SET TranslationName = :English WHERE TranslationCode = :TranslationCode AND CultureCode='EN'";
					return tr.queryExecute(sqlQuery, item);
				}
			})
			.then(function(){
				if(tmpData != null){
					var sqlQuery = "UPDATE Translation SET TranslationName = :ChineseSimplified WHERE TranslationCode = :TranslationCode AND CultureCode='CHS'";
					return tr.queryExecute(sqlQuery, item);
				}
			})
			.then(function(){
				if(tmpData != null){
					var sqlQuery = " UPDATE Translation SET TranslationName = :ChineseTraditional WHERE TranslationCode = :TranslationCode AND CultureCode='CHT'";
					return tr.queryExecute(sqlQuery, item);
				}
			})
		}));

		return Q.all(promises);
	}).then(function(){
		// console.timeEnd("translationimport");
		// console.log("done updating.");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationImport Finished'+ ', Line Number:454');
		res.status(200).send("Successfully imported excel file.");
		return tr.commit();
	}).catch(function(err){
		res.status(400).json("Error uploading excel file.");
		console.log('Date:'+ moment(new Date()).format('MMMM Do YYYY, h:mm:ss a')+', Message:TranslationImport Error('+err+')'+ ', Line Number:459');
		console.log('Rollback');
		console.dir(err);
		return tr.rollback();
	}).done();
});

module.exports = router;
