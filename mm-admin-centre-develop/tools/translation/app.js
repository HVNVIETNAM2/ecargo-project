
var express = require('express');
var path = require('path');
var http = require('http');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');
var async = require('async');


// Start Express Application...
var app = express();

var port = process.env.PORT || '3001';
app.set('port', port);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

// app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));
app.use(logger('dev'));
app.use(bodyParser.json({ limit : '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit : '50mb' }));
app.use(cookieParser());

var server = http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));

    app.get('*', function(req, res, next){
        // Attach locals on all get request...
        res.locals.title = "MM Translation";
        res.locals.proxyDir = process.env.PROXY_DIR || "";
        next();
    });

    app.use('/', require('./routes/index.js'));

    // no stacktraces leaked to user
    app.use(function(err, req, res, next){
        console.log(err); // For development only comment out for production
        res.status(401).json(err);
    });

    // For AngularJS if no route was found redirect to home page.
    app.get('*', function(req, res, next){
        res.redirect('/');
    });
});

module.exports = app;

console.log("Test Jenkins: Automatic Restart.");
