var fs = require('fs');
var watch = require('watch');
var Datastore = require('nedb');
var path = require('path');
var async = require('async');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
var exec = require('child_process').exec;

var db = {
    files : new Datastore({ filename : path.join(process.cwd(), 'data', 'files.db'), autoload : true })
};

console.log("DB File Path", path.join(process.cwd(), 'data', 'files.db'));

var configs = {
    // "path" : "/mmservice/sql",
    "path" : "/home/ubuntu/mmservice/sql",
    "receiver": ["raithorivera@gmail.com","paz.alvin22@gmail.com","albertdallow@wweco.com", "michaelyu@wweco.com", "kyochan@wweco.com", "joehu@wweco.com", "adrianshum@wweco.com", "AndrewChan@wweco.com", "AlanYu@wweco.com", "vincentyip@wweco.com"],
    // "receiver": ["raithorivera@gmail.com","paz.alvin22@gmail.com"],
    "email": {
        "host": "email-smtp.us-west-2.amazonaws.com",
        "port": 587,
        "auth": {
            "user": "AKIAIFJLQHWV4CUIV7SA",
            "pass": "AgY/AL4JHgGNNtpEOSyD2pNysLQDJUNz/s1QYNixPkO9"
        }
    },
    "mysql" : {
        "importquery" : "mysql -u root -p3nations mm < "
    },
    "sendEmail" : true
};

var transporter = nodemailer.createTransport(smtpTransport(configs.email));

var tmpFiles = {
    newfile : [],
    change : [],
    remove : []
};

watch.watchTree(configs.path, function (f, curr, prev){
    async.eachLimit(Object.keys(f), 1, function(item, callback){
        db.files.findOne({ name : item }, function(err, file){
            if(!file){
                console.log(item);
                db.files.insert({ name : item, timestamp : new Date().getTime(), stats : f[item] }, function(){
                    console.log("insert database", item);
                    if(item.indexOf('.sql') > -1){
                        tmpFiles.newfile.push(item);

                        exec(configs.mysql.importquery + item, function(err, data){
                            console.log("error", err);
                            callback();
                        });
                    }
                });
            }else{
                console.log(item);
                if(file.stats.mtime.toString() == f[item].mtime.toString()){
                    callback();
                }else{
                    console.log("item change", item);
                    if(item.indexOf('.sql') > -1){
                        tmpFiles.change.push(item); 
                    }
                    
                    db.files.update({ name : item },{ $set : { stats : f[item] }}, function(){
                        // exec(configs.mysql.importquery + item, function(err, data){
                            // console.log("error", err);
                            callback();
                        // });
                    });
                }
            }
        });
    }, function(err){
        console.log(err);
        console.log("done reading directories, start sending out emails.", tmpFiles);

        if(configs.sendEmail && (tmpFiles.newfile.length > 0 || tmpFiles.change.length > 0 || tmpFiles.remove.length > 0)){
            var html = '<b>Maydev SQL Changes</b> <br />';
            html += "<b>Inserted File</b><br />";

            html += "<ol>";
            tmpFiles.newfile.forEach(function(elem){
                html += "<li>" + elem + "</li>";
            });
            html += "</ol>";

            html += "<b>Change File</b><br />";
            html += "<ol>";
            tmpFiles.change.forEach(function(elem){
                html += "<li>" + elem + "</li>";
            });
            html += "</ol>";

            html += "<b>Remove File</b><br />";
            html += "<ol>";
            tmpFiles.remove.forEach(function(elem){
                html += "<li>" + elem + "</li>";
            });
            html += "</ol>";

            configs.receiver.forEach(function(elem){
                var email = {
                    from: 'Maydev FileWatcher <raithorivera@gmail.com>',
                    to: elem, 
                    subject: 'Maydev SQL Changes',
                    html: html
                };

                transporter.sendMail(email, function(error, info){
                    console.log(new Date() + " Sending email to: " + elem);
                    console.log(error);
                    console.log("===================");
                });
            });
        }
    });
});

console.log("Start watching log files.");
