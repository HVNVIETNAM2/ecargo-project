
// CUSTOM DATA LOADER SCRIPT FOR BRAND, COLOR, BADGE, SEASON reference tables.
// WRITTEN BY ALVIN PASCASIO

var Excel = require('exceljs');
var Path = require('path');
var _ = require('lodash');
var Q = require('q');
var cc = require('../../lib/commoncrypt.js');
var cm = require('../../lib/commonmariasql.js');


var connectionInfo = {
  "user": "root",
  "password": "3nations",
  "host": "localhost",
  "db": "maymay",
  "metadata": true,
  "poolsize" :1,
  "charset":"utf8"
};

if (process.argv.length < 4) {
  console.log("Usage: node " + Path.basename(process.argv[1]) + " [file.xls] [sheet name]");
  process.exit(1);
}

var filename = process.argv[2];
var sheetName = process.argv[3];
var table_name = process.argv[4];

var tableDefinitions = {
	Brand : {
		Base : ['BrandNameInvariant', 'BrandCode'],
		Culture : ["BrandId", "CultureCode", "BrandName"]
	},
	Color : {
		Base : ['ColorNameInvariant', 'ColorCode'],
		Culture : ["ColorId", "CultureCode", "ColorName"]
	},
	Badge : {
		Base : ['BadgeNameInvariant', 'BadgeCode'],
		Culture : ["BadgeId", "CultureCode", "BadgeName"]
	},
	Season : {
		Base : ['SeasonNameInvariant', 'SeasonCode'],
		Culture : ["SeasonId", "CultureCode", "SeasonName"]
	},
  Size: {
    Base : ['SizeNameInvariant', 'SizeCode', 'SizeGroup'],
    Culture: ['SizeId', 'CultureCode', 'SizeName']
  },
  GeoCountry: {
    Base: ['GeoCountryNameInvariant', 'CountryCode'],
    Culture: ['GeoCountryId', 'CultureCode', 'GeoCountryName']
  }
}

var workbook = new Excel.Workbook();


console.log('READING EXCEL FILE: ' + filename);
workbook.xlsx.readFile(filename).then(function() {
  var worksheet = workbook.getWorksheet(sheetName);

  if (!worksheet) {
    console.error("Failed to read worksheet " + sheetName);
    process.exit(2);
  }

  var tr = new cm(connectionInfo);
  var allPromises = [];
  var GroupName = null;

  Q.when()
  .then(function() {
   	if( typeof table_name === 'undefined' || !table_name ){
   		throw {'Error' : "Table name is missing"};
   	}
  })
  .then(function() {
   	if( !tableDefinitions[table_name] ){
   		throw {'Error' : "Table name is invalid. Please user other load script."};
   	}
  })
  .then(function() {
    return tr.begin();
  })
  .then(function() {
  	console.log("BASE TABLE EMPTY: " + table_name);
    return tr.queryExecute("DELETE FROM " + table_name);
  })
  .then(function() {
  	console.log("CHANGING SQL_MODE");
   	return tr.queryExecute("SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO'");
  })
  .then(function() {
  	console.log("INSERTING ZERO VALUE FOR: " + table_name);
   	return tr.queryExecute("INSERT INTO "+ table_name +" ("+ table_name +"Id, "+ tableDefinitions[table_name].Base[0] +") VALUES (0, :NameInvariant)", { NameInvariant : "---" });
  })
  .then(function() {
  	console.log("REVERTING SQL_MODE");
   	return tr.queryExecute("SET sql_mode = ''");
  })
  .then(function() {
  	console.log("CULTURE TABLE EMPTY: " + table_name);
    return tr.queryExecute("DELETE FROM "+ table_name +"Culture");
  })
  .then(function() {
  	console.log("INSERTING ZERO VALUE FOR CULTURE EN: " + table_name);
   	return tr.queryExecute("INSERT INTO "+ table_name +"Culture ("+ tableDefinitions[table_name].Culture.join(', ') +") VALUES (0, :CultureCode, :NameInvariant)", { CultureCode : "EN" , NameInvariant : "---"});
  })
  .then(function() {
  	console.log("INSERTING ZERO VALUE FOR CULTURE CHS: " + table_name);
   	return tr.queryExecute("INSERT INTO "+ table_name +"Culture ("+ tableDefinitions[table_name].Culture.join(', ') +") VALUES (0, :CultureCode, :NameInvariant)", { CultureCode : "CHS", NameInvariant : "---" });
  })
  .then(function() {
  	console.log("INSERTING ZERO VALUE FOR CULTURE CHT: " + table_name);
   	return tr.queryExecute("INSERT INTO "+ table_name +"Culture ("+ tableDefinitions[table_name].Culture.join(', ') +") VALUES (0, :CultureCode, :NameInvariant)", { CultureCode : "CHT", NameInvariant : "---" });
  })
  .then(function() {
    worksheet.eachRow(function(row, rowNumber) {
      // Skip the header row
     if (rowNumber <= 1) return;

     var insertId = 0;
     var Code = row.getCell(1).value;
     var Name_en = row.getCell(2).value;
     var Name_Hans = row.getCell(3).value;
     var Name_Hant = row.getCell(4).value;

     //sanitize each rowcell
     if( Code && typeof Code === 'object' ){
      Code = Code.result;
     }
     if( Name_en && typeof Name_en === 'object' ){
      Name_en = Name_en.result;
     }
     if( Name_Hans && typeof Name_Hans === 'object' ){
      Name_Hans = Name_Hans.result;
     }
     if( Name_Hant && typeof Name_Hant === 'object' ){
      Name_Hant = Name_Hant.result;
     }

      var rowPromise = Q.when()
      .then(function() {
      	console.log('PROCESSING ROW: ' + rowNumber);

        if( table_name == 'Size' ){
          
          if( Name_en == null || Name_Hans == null || Name_Hant == null ){
            GroupName = Code;
            sizeBreaker = false;
          }else{
            if(GroupName){
                return tr.queryExecute("INSERT INTO "+ table_name +" ("+ tableDefinitions[table_name].Base.join(', ') +") VALUES (:NameInvariant, :Code, :Group)", {
                  NameInvariant:Name_en,
                  Code: Code,
                  Group : GroupName
                }).then(function(result){
                    GroupName = null;
                    insertId = result.insertId;
                });
            }
          }

        }else{
          return tr.queryExecute("INSERT INTO "+ table_name +" ("+ tableDefinitions[table_name].Base.join(', ') +") VALUES (:NameInvariant, :Code)", {
            NameInvariant:Name_en,
            Code: Code
          }).then(function(result){
              insertId = result.insertId;
          });
        }
        
      })
      .then(function() {
          if(!(insertId > 0)){
            return true;
          }
          return tr.queryExecute("INSERT INTO "+ table_name +"Culture ("+ tableDefinitions[table_name].Culture.join(', ') +") VALUES (:Id, :CultureCode, :NameInvariant)", {Id: insertId, CultureCode: 'EN', NameInvariant: Name_en}).then(function(res){
              console.log("EN INSERTED FOR ROW: " + rowNumber + " WITH BASE TABLE ID: " + insertId);
          });  
      })
      .then(function() {
          if(!(insertId > 0)){
            return true;
          }
          return tr.queryExecute("INSERT INTO "+ table_name +"Culture ("+ tableDefinitions[table_name].Culture.join(', ') +") VALUES (:Id, :CultureCode, :NameInvariant)", {Id: insertId, CultureCode: 'CHS', NameInvariant: Name_Hans}).then(function(res){
              console.log("CHS INSERTED FOR ROW: " + rowNumber + " WITH BASE TABLE ID: " + insertId);
          });  
      })
      .then(function() {
          if(!(insertId > 0)){
            return true;
          }
          return tr.queryExecute("INSERT INTO "+ table_name +"Culture ("+ tableDefinitions[table_name].Culture.join(', ') +") VALUES (:Id, :CultureCode, :NameInvariant)", {Id: insertId, CultureCode: 'CHT', NameInvariant: Name_Hant}).then(function(res){
              console.log("CHT INSERTED FOR ROW: " + rowNumber + " WITH BASE TABLE ID: " + insertId);
          });  
      });
      allPromises.push(rowPromise);
    });
  })
  .then(function() {
    Q.all(allPromises)
    .then(function() {
      console.log("Commit changes for sheet " + sheetName);
      return tr.commit();
    })
    .catch(function(err) {
      console.error(err);
      console.error("Rollback changes for sheet " + sheetName);
      return tr.rollback();
    })
  })
  .catch(function(err) {
    console.error(err);
    return tr.rollback();
  })
  .done();
});



