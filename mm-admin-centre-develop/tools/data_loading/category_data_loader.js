var Excel = require('exceljs');
var Path = require('path');
var _ = require('lodash');
var Q = require('q');
var cm = require('../../lib/commonmariasql.js');

var connectionInfo = {
  "user": "root",
  "password": "3nations",
  "host": "localhost",
  "db": "maymay",
  "metadata": true,
  "poolsize" :1,
  "charset":"utf8"
};

if (process.argv.length < 4) {
  console.log("Usage: node " + Path.basename(process.argv[1]) + " [file.xls] [sheet name]");
  process.exit(1);
}

var filename = process.argv[2];
var sheetName = process.argv[3];

var workbook = new Excel.Workbook();

workbook.xlsx.readFile(filename).then(function() {
  var worksheet = workbook.getWorksheet(sheetName);

  if (!worksheet) {
    console.error("Failed to read worksheet " + sheetName);
    process.exit(2);
  }

  var tr = new cm(connectionInfo);
  var allPromises = [];

  Q.when()
  .then(function() {
    return tr.begin();
  })
  .then(function() {
    worksheet.eachRow(function(row, rowNumber) {
      // Skip the header row
      if (rowNumber <= 1) return;

      var parentCategoryCode = row.getCell(1).value;
      var categoryNameEn = row.getCell(2).value;
      var categoryNameCht = row.getCell(3).value;
      var categoryNameChs = row.getCell(4).value;
      var categoryCode = row.getCell(5).value;

      var rowPromise = Q.when()
      .then(function() {
        return tr.queryOne("SELECT CategoryId FROM Category WHERE CategoryCode = :parentCategoryCode", {parentCategoryCode: parentCategoryCode}).then(function(result) {
          return result.CategoryId;
        });
      })
      .then(function(parentCategoryId) {
        return tr.queryExecute("INSERT INTO Category (ParentCategoryId, CategoryCode, CategoryNameInvariant) VALUES (:parentCategoryId, :categoryCode, :categoryNameInvariant)", {
          parentCategoryId: parentCategoryId,
          categoryCode: categoryCode,
          categoryNameInvariant: categoryNameEn
        }).then(function(result) {
          return result.insertId;
        });
      })
      .then(function(newCategoryId) {
        return tr.queryExecute("INSERT INTO CategoryCulture (CategoryId, CultureCode, CategoryName) VALUES (:categoryId, 'EN', :categoryName)", {categoryId: newCategoryId, categoryName: categoryNameEn}).then(function() {
          return newCategoryId;
        });
      })
      .then(function(newCategoryId) {
        return tr.queryExecute("INSERT INTO CategoryCulture (CategoryId, CultureCode, CategoryName) VALUES (:categoryId, 'CHT', :categoryName)", {categoryId: newCategoryId, categoryName: categoryNameCht}).then(function() {
          return newCategoryId;
        });
      })
      .then(function(newCategoryId) {
        return tr.queryExecute("INSERT INTO CategoryCulture (CategoryId, CultureCode, CategoryName) VALUES (:categoryId, 'CHS', :categoryName)", {categoryId: newCategoryId, categoryName: categoryNameChs});
      });

      allPromises.push(rowPromise);
    });
  })
  .then(function() {
    Q.all(allPromises)
    .then(function() {
      console.log("Commit changes for sheet " + sheetName);
  		return tr.commit();
  	})
    .catch(function(err) {
      console.error(err);
      console.error("Rollback changes for sheet " + sheetName);
      return tr.rollback();
    })
  })
  .catch(function(err) {
    console.error(err);
    return tr.rollback();
  })
  .done();
});
