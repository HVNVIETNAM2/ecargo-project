var Excel = require('exceljs');
var Path = require('path');
var _ = require('lodash');
var Q = require('q');
var cc = require('../../lib/commoncrypt.js');
var cm = require('../../lib/commonmariasql.js');

var connectionInfo = {
  "user": "root",
  "password": "3nations",
  "host": "localhost",
  "db": "maymay",
  "metadata": true,
  "poolsize" :1,
  "charset":"utf8"
};

if (process.argv.length < 3) {
  console.log("Usage: node " + Path.basename(process.argv[1]) + " [file.xls]");
  process.exit(1);
}

var filename = process.argv[2];

var tr = new cm(connectionInfo);

var workbook = new Excel.Workbook();
workbook.xlsx.readFile(filename).then(function() {
  var worksheet = workbook.getWorksheet(1);  // worksheet index starts from 1

  var allPromises = [];

  Q.when()
  .then(function(){
    return tr.begin();
  })
  .then(function() {
    worksheet.eachRow(function(row, rowNumber) {
      // Skip the header row
      if (rowNumber <= 1) return;

      var firstName = row.getCell(1).value;
      var lastName = row.getCell(2).value;
      var middleName = row.getCell(3).value;
      var displayName = row.getCell(4).value;
      var rolesString = row.getCell(5).value;
      var email = row.getCell(6).value;
      var password = row.getCell(7).value;
      var countryCode = row.getCell(8).value;
      var mobileNumber = row.getCell(9).value;
      var languageId = getLanguageId(row.getCell(10).value);
      // var timeZone = row.getCell(11).value;
      var timeZoneId = 1;
      var statusId = getStatusId(row.getCell(13).value);
      var reason = row.getCell(14).value;
      var userTypeId = 1;
      var activationToken = cc.createSimpleToken();
      var salt = password ? cc.createSalt() : null;
      var hash = password ? cc.createHash(password, salt) : null;

      var promise = Q.when()
      .then(function() {
        return tr.queryOne("SELECT * FROM User WHERE Email=:Email AND StatusId <> 1", {Email: email}).then(function(data) {
          if (data) {
            throw {msg: "MSG_ERR_EMAIL_REGISTERED"};
          }
        });
      })
      .then(function() {
        return tr.queryOne("SELECT * FROM User WHERE MobileNumber = :MobileNumber AND MobileCode = :MobileCode AND StatusId <> 1", {MobileNumber: mobileNumber, MobileCode: countryCode}).then(function(data) {
          if (data)
            throw {msg: "MSG_ERR_MOBILE_REGISTERED"};
        });
      })
      .then(function() {
        var data = {
          userTypeId: userTypeId,
          firstName: firstName,
          lastName: lastName,
          middleName: middleName,
          displayName: displayName,
          email: email,
          mobileCode: countryCode,
          mobileNumber: mobileNumber,
          languageId: languageId,
          timeZoneId: timeZoneId,
          activationToken: statusId === 3 ? activationToken : null,
          salt: statusId !== 3 ? salt : null,
          hash: statusId !== 3 ? hash : null,
          statusId: statusId,
          statusReasonCode: reason
        };

        return tr.queryExecute("INSERT INTO User (UserTypeId, FirstName, LastName, MiddleName, DisplayName, Email, MobileCode, MobileNumber, LanguageId, TimeZoneId, ActivationToken, Hash, Salt, StatusId, StatusReasonCode) VALUES (:userTypeId, :firstName, :lastName, :middleName, :displayName, :email, :mobileCode, :mobileNumber, :languageId, :timeZoneId, :activationToken, :hash, :salt, :statusId, :statusReasonCode);", data).then(function(result) {
          console.log("Created user " + data.firstName + " " + data.lastName + ", ID = " + result.insertId);
          return result.insertId;
        });
      })
      .then(function(newUserId) {
        var roles = rolesString.split(',');
        var insertPromises = roles.map(function(role) {
          return tr.queryExecute("INSERT INTO UserSecurityGroup (UserId, SecurityGroupId) VALUES (:UserId, :SecurityGroupId)", {UserId: newUserId, SecurityGroupId: getSecurityGroupId(role)}).then(function(result) {
            console.log("Assigned role " + role + " to user ID " + newUserId);
          });
        });
        return Q.all(insertPromises);
      });

      allPromises.push(promise);
    });
  })
  .then(function() {
    Q.all(allPromises)
    .then(function() {
      console.log("Commit changes.")
  		return tr.commit();
  	})
    .catch(function(err) {
      console.error(err);
      console.error("Rollback changes.");
      return tr.rollback();
    })
  })
  .catch(function(err) {
    console.error(err);
    console.error("Rollback changes.");
    return tr.rollback();
  })
  .done();
});

var getLanguageId = function(languageName) {
  if (!languageName) {
    throw {msg: "Language cannot be null"};
  }

  languageName = languageName.trim().toLowerCase();
  if (languageName === 'en') {
    return 1;
  }
  else if (languageName === 'chs') {
    return 2;
  }
  else if (languageName === 'cht') {
    return 3;
  }
  else {
    throw {msg: "Invalid language: "  + languageName};
  }
};

var getStatusId = function(statusName) {
  if (!statusName) {
    throw {msg: "Status cannot be null"};
  }

  statusName = statusName.trim().toLowerCase();
  if (statusName === 'deleted') {
    return 1;
  }
  else if (statusName === 'active') {
    return 2;
  }
  else if (statusName === 'pending') {
    return 3;
  }
  else if (statusName === 'inactive') {
    return 4;
  }
  else {
    throw {msg: "Invalid status: " + statusName};
  }
}

var getSecurityGroupId = function(roleName) {
  if (!roleName) {
    throw {msg: "Role cannot be null"};
  }

  roleName = roleName.trim().toLowerCase();

  if (roleName === 'mm admin') {
    return 1;
  }
  else if (roleName === 'mm customer service') {
    return 2;
  }
  else if (roleName === 'mm finance') {
    return 3;
  }
  else if (roleName === 'mm tech ops') {
    return 4;
  }
  else if (roleName === 'merchant finance') {
    return 5;
  }
  else if (roleName === 'merchant admin') {
    return 6;
  }
  else {
    throw {msg: "Invalid role " + roleName};
  }
}
