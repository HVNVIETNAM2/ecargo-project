DROP TABLE IF EXISTS `UserIdentification`;
CREATE TABLE IF NOT EXISTS `UserIdentification` (
  `UserIdentificationId` int(11) NOT NULL AUTO_INCREMENT,
  `UserIdentificationKey` varchar(36) NOT NULL,
  `UserId` int(11) DEFAULT '0',
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `IdentificationNumber` varchar(50) DEFAULT NULL,
  `FrontImage` varchar(50) DEFAULT NULL,
  `BackImage` varchar(50) DEFAULT NULL,
  `LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserIdentificationId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

ALTER TABLE `User`
ADD COLUMN `UserIdentificationKey` varchar(50) DEFAULT NULL AFTER `DefaultUserAddressId`;
