DROP TABLE IF EXISTS `Ticket`;
CREATE TABLE `Ticket` (
  `TicketId` int(11) NOT NULL AUTO_INCREMENT,
  `TicketTypeId` INT(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `TicketSummary` varchar(255) NOT NULL,
  `TicketStatusId` INT(11) NOT NULL,
  `CultureCode` varchar(50) DEFAULT NULL,
  `LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModifiedUserId` int(11) NOT NULL,
  PRIMARY KEY (`TicketId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `TicketType`;
CREATE TABLE `TicketType` (
  `TicketTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `TicketTypeName` INT(11) NOT NULL,
  PRIMARY KEY (`TicketTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


INSERT INTO `TicketType` (`TicketTypeId`,`TicketTypeName`) VALUES (1,'Feedback');