ALTER TABLE `Color` 
ADD COLUMN `Position` INT(11) NOT NULL DEFAULT '0';

update Color set Position=ColorId;

ALTER TABLE `Size` 
ADD COLUMN `Position` INT(11) NOT NULL DEFAULT '0';

update Size set Position=SizeId;