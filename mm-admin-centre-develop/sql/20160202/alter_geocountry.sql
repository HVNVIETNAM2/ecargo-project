ALTER TABLE `GeoCountry` ADD `MobileCode` VARCHAR(20) NOT NULL DEFAULT 0;
ALTER TABLE `GeoCountry` ADD `IsStoreFrontCountry` TINYINT(1) NOT NULL DEFAULT 0;

UPDATE GeoCountry SET `IsStoreFrontCountry` = 1 WHERE `CountryCode` = 'CN' OR `CountryCode` = 'HK';