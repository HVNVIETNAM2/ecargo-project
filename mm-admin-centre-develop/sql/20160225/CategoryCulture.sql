ALTER TABLE CategoryCulture
ADD COLUMN SizeGridImage VARCHAR(255) AFTER CategoryMetaDesc;

ALTER TABLE Category
ADD COLUMN SizeGridImageInvariant VARCHAR(255) AFTER CategoryImage;