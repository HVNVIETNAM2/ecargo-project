INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT", "CHS", "缺少图片报告"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT", "CHT", "缺少圖片報告"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT", "EN", "Missing Image Report"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_PRINT", "CHS", "列印"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_PRINT", "CHT", "列印"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_PRINT", "EN", "Print"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_RUN", "CHS", "运行"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_RUN", "CHT", "運行"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_PROD_MISSING_IMG_REPORT_RUN", "EN", "Run"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_ADD_IMAGES", "CHS", "添加图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_ADD_IMAGES", "CHT", "添加圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_ADD_IMAGES", "EN", "Add images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_IMG_DEL", "CHS", "删除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_IMG_DEL", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_IMG_DEL", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCOUNT", "CHS", "账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCOUNT", "CHT", "帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCOUNT", "EN", "Account"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCT_LOCKED", "CHS", "你的账户已被锁定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCT_LOCKED", "CHT", "你的帳戶已被鎖定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACCT_LOCKED", "EN", "Your account has been locked"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIONS", "CHS", "操作"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIONS", "CHT", "操作"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIONS", "EN", "Action"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_CREATE", "CHS", "成功激活新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_CREATE", "CHT", "成功激活新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_CREATE", "EN", "New product activated: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_NOTE", "CHS", "确定激活所有待处理的商品么？激活后商品将出现在用户应用端，用户可以浏览购买该商品。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_NOTE", "CHT", "確定現在激活所有待處理的商品嗎？激活後商品將出現在用戶應用端，用戶可以瀏覽購買該商品。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_NOTE", "EN", "Are you sure to activate all pending products to the storefront now?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_PRODUCTS", "CHS", "激活所有待处理的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_PRODUCTS", "CHT", "激活所有待處理的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_PENDING_PRODUCTS", "EN", "Activate Pending Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_SUCCEED", "CHS", "激活商品成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_SUCCEED", "CHT", "激活商品成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_ALL_SUCCEED", "EN", "Activated products successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS", "CHS", "激活商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS", "CHT", "激活商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS", "EN", "Activate Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS_NOTE", "CHS", "确认激活商品？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS_NOTE", "CHT", "確認激活商品？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_PRODUCTS_NOTE", "EN", "Confirm to activate products?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_USER", "CHS", "激活用户账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_USER", "CHT", "激活用戶帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_USER", "EN", "Activate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE", "CHS", "验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE", "CHT", "驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE", "EN", "Activation Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE_3TIMES", "CHS", "(验证码只可发送3次)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE_3TIMES", "CHT", "(驗證碼只可發送3次)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATION_CODE_3TIMES", "EN", "(Activation code can only be sent 3 times)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVE", "CHS", "正常"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVE", "CHT", "正常"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVE", "EN", "Active"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTIVE_BRAND_NOTE", "CHS", "确认激活这个品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTIVE_BRAND_NOTE", "CHT", "確認激活這個品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTIVE_BRAND_NOTE", "EN", "Confirm to activate this brand?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ALL_BRANDS", "CHS", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ALL_BRANDS", "CHT", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ALL_BRANDS", "EN", "Brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND", "CHS", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND", "CHT", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND", "EN", "Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_MERCHANT", "CHS", "品牌商家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_MERCHANT", "CHT", "品牌商家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_MERCHANT", "EN", "Brand Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_NUM_PROD", "CHS", "商品数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_NUM_PROD", "CHT", "商品數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_NUM_PROD", "EN", "Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_SEARCH", "CHS", "搜索品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_SEARCH", "CHT", "搜索品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_SEARCH", "EN", "Search for Brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_HEADER", "CHS", "公司商标
(标题栏)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_HEADER", "CHT", "公司商標
(標題欄)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_HEADER", "EN", "Company Logo
(Header)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_LARGE", "CHS", "公司商标
(大尺寸)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_LARGE", "CHT", "公司商標
(大尺寸)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_LARGE", "EN", "Company Logo
 (Large size)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_SMALL", "CHS", "公司商标
(小尺寸)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_SMALL", "CHT", "公司商標
(小尺寸)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_COMPANY_LOGO_SMALL", "EN", "Company Logo
(Small size)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CREATE_BRAND", "CHS", "创建品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CREATE_BRAND", "CHT", "創建品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CREATE_BRAND", "EN", "Create Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FEATURED", "CHS", "显示为精选品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FEATURED", "CHT", "顯示為精選品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FEATURED", "EN", "Show as Featured Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_FEATURED", "CHS", "精选品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_FEATURED", "CHT", "精選品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_FEATURED", "EN", "Featured Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_LISTED", "CHS", "上架品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_LISTED", "CHT", "上架品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_LISTED", "EN", "Listed Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_RECOMMEND", "CHS", "推介品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_RECOMMEND", "CHT", "推介品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_FILTER_RECOMMEND", "EN", "Recommended Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_HTTP", "CHS", "http://"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_HTTP", "CHT", "http://"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_HTTP", "EN", "http://"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INACTIVE_BRAND_NOTE", "CHS", "确认停用这个品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INACTIVE_BRAND_NOTE", "CHT", "確認停用這個品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INACTIVE_BRAND_NOTE", "EN", "Confirm to inactivate this brand?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LISTED", "CHS", "显示为上架品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LISTED", "CHT", "顯示為上架品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LISTED", "EN", "Show in Listed Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LIST_RESULT", "CHS", "找到 {0} 条有关「{1}」的纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LIST_RESULT", "CHT", "找到 {0} 條有關「{1}」的紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LIST_RESULT", "EN", "{0} search result(s) for ‘’{1}”"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_IMG_GUIDE", "CHS", "如何准备图片：请参阅指引里的文件格式和说明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_IMG_GUIDE", "CHT", "如何準備圖片：請參閱指引裡的文件格式和說明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_IMG_GUIDE", "EN", "How to prepare images: Please refer to the guideline for the file format and instructions"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_LIST_SEARCH_PLACEHOLDER", "CHS", "搜索商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_LIST_SEARCH_PLACEHOLDER", "CHT", "搜索商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_LIST_SEARCH_PLACEHOLDER", "EN", "Search for Merchants"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MONO_BRAND_AGENT", "CHS", "单品牌代理商"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MONO_BRAND_AGENT", "CHT", "單品牌代理商"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MONO_BRAND_AGENT", "EN", "Mono-brand Agent"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MULTI_BRAND_AGENT", "CHS", "多品牌代理商"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MULTI_BRAND_AGENT", "CHT", "多品牌代理商"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MULTI_BRAND_AGENT", "EN", "Multi-brand Agent"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_MERCHANT", "CHS", "商户数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_MERCHANT", "CHT", "商戶數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_MERCHANT", "EN", "Merchants"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_STYLE", "CHS", "样式数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_STYLE", "CHT", "樣式數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_STYLE", "EN", "Styles"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT", "CHS", "商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT", "CHT", "商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT", "EN", "Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT_NOTE", "CHS", "查看属于此商家的产品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT_NOTE", "CHT", "查看属于此商家的產品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PRODUCT_NOTE", "EN", "To view the products under this merchant."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT", "CHS", "缺少商品图片报告"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT", "CHT", "缺少商品圖片報告"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT", "EN", "Missing Product Images Report"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT_NO_RESULT", "CHS", "没有要显示的信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT_NO_RESULT", "CHT", "沒有要顯示的信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROD_MISSING_IMG_REPORT_NO_RESULT", "EN", "No information to display"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER", "CHS", "个人信息封面图"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER", "CHT", "個人信息封面圖"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER", "EN", "Profile Banner Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER_NOTE", "CHS", "文件大小上限 {CONST_BG_IMG_FILE_SIZE}，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER_NOTE", "CHT", "文件大小上限 {CONST_BG_IMG_FILE_SIZE}，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PROFILE_BANNER_NOTE", "EN", "Max. file size {CONST_BG_IMG_FILE_SIZE},  JPG/PNG format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_RECOMMEND", "CHS", "显示为推介品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_RECOMMEND", "CHT", "顯示為推介品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_RECOMMEND", "EN", "Show as Recommended Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_SUGGEST", "CHS", "可搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_SUGGEST", "CHT", "可搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_SUGGEST", "EN", "Is searchable in the Search Bar"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_LIST_SEARCH_PLACEHOLDER", "CHS", "搜索用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_LIST_SEARCH_PLACEHOLDER", "CHT", "搜索用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_LIST_SEARCH_PLACEHOLDER", "EN", "Search for Users"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_FILE", "CHS", "添加文档"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_FILE", "CHT", "添加文檔"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_FILE", "EN", "Add File"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_IMAGES", "CHS", "添加图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_IMAGES", "CHT", "添加圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_IMAGES", "EN", "Add Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_STAFF", "CHS", "分配员工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_STAFF", "CHT", "分配员工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD_STAFF", "EN", "Add Staff"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADMIN_CENTER", "CHS", "用户中心"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADMIN_CENTER", "CHT", "用戶中心"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADMIN_CENTER", "EN", "Admin Center"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ALLOC", "CHS", "所有配额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ALLOC", "CHT", "所有配額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ALLOC", "EN", "All Allocations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_CATEGORY", "CHS", "所有类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_CATEGORY", "CHT", "所有類别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_CATEGORY", "EN", "All Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_COUNTRIES", "CHS", "全部国家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_COUNTRIES", "CHT", "全部國家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_COUNTRIES", "EN", "All Countries"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_LOC_TYPE", "CHS", "全部类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_LOC_TYPE", "CHT", "全部類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_LOC_TYPE", "EN", "All Types"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_TYPES", "CHS", "全部商户类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_TYPES", "CHT", "全部商戶類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_TYPES", "EN", "All Merchant Types"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PRODUCTS", "CHS", "所有商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PRODUCTS", "CHT", "所有商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PRODUCTS", "EN", "All Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PROVINCE", "CHS", "全部省/州/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PROVINCE", "CHT", "全部省/州/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_PROVINCE", "EN", "All Province"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ROLES", "CHS", "所有角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ROLES", "CHT", "所有角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_ROLES", "EN", "All Roles"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_STATUS", "CHS", "所有状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_STATUS", "CHT", "所有狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_STATUS", "EN", "All Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ASSIGN_MM_STAFF_TO_MERCHANT", "CHS", "为商户分配美美员工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ASSIGN_MM_STAFF_TO_MERCHANT", "CHT", "為商戶分配美美員工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ASSIGN_MM_STAFF_TO_MERCHANT", "EN", "Assign MayMay Staff to this Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK", "CHS", "返回"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK", "CHT", "返回"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK", "EN", "Back"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_INFO", "CHS", "商户注册信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_INFO", "CHT", "商戶註冊信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_INFO", "EN", "Basic Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_PROFILE", "CHS", "用户基本信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_PROFILE", "CHT", "用戶基本信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_PROFILE", "EN", "Basic Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BG_IMG", "CHS", "背景图片 (大尺寸)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BG_IMG", "CHT", "背景圖片 (大尺寸)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BG_IMG", "EN", "Background Image (big)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM", "CHS", "街区号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM", "CHT", "街區號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM", "EN", "Block Number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM_OPT", "CHS", "街区号码（选填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM_OPT", "CHT", "街區號碼（選填）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BLOCK_NUM_OPT", "EN", "Block Number (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_CODE", "CHS", "品牌代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_CODE", "CHT", "品牌代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_CODE", "EN", "Brand Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_S", "CHS", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_S", "CHT", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_S", "EN", "Brand(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BR_NUM", "CHS", "商业登记号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BR_NUM", "CHT", "商業登記號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BR_NUM", "EN", "Business Registration no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME", "CHS", "楼宇名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME", "CHT", "樓宇名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME", "EN", "Buillding Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME_OPT", "CHS", "楼宇名称 (选填)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME_OPT", "CHT", "樓宇名稱 (選填)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BUILDING_NAME_OPT", "EN", "Building Name (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CANCEL", "CHS", "取消"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CANCEL", "CHT", "取消"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CANCEL", "EN", "Cancel"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY", "CHS", "类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY", "CHT", "類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY", "EN", "Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL", "CHS", "全部"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL", "CHT", "全部"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL", "EN", "All"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_COMMENTS", "CHS", "全部评价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_COMMENTS", "CHT", "全部評價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_COMMENTS", "EN", "All Comments"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_REVIEW", "CHS", "全部评价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_REVIEW", "CHT", "全部評價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_REVIEW", "EN", "All Reviews"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRAND", "CHS", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRAND", "CHT", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRAND", "EN", "Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BROWSE", "CHS", "发现"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BROWSE", "CHT", "發現"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BROWSE", "EN", "Browse"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CANCEL", "CHS", "取消"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CANCEL", "CHT", "取消"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CANCEL", "EN", "Cancel"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY", "CHS", "类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY", "CHT", "類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY", "EN", "Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY_BRAND", "CHS", "类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY_BRAND", "CHT", "類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CATEGORY_BRAND", "EN", "Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COLOUR", "CHS", "颜色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COLOUR", "CHT", "顏色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COLOUR", "EN", "Color"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COMMENT", "CHS", "评论"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COMMENT", "CHT", "評論"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_COMMENT", "EN", "Comment"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CUST_SERVICE", "CHS", "卖家服务"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CUST_SERVICE", "CHT", "賣家服務"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CUST_SERVICE", "EN", "Customer Service"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOUNT", "CHS", "特价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOUNT", "CHT", "特價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOUNT", "EN", "Discount"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOVER", "CHS", "发现"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOVER", "CHT", "發現"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISCOVER", "EN", "Discover"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER", "CHS", "筛选"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER", "CHT", "篩選"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER", "EN", "Filter"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_LIKE", "CHS", "喜欢"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_LIKE", "CHT", "喜歡"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_LIKE", "EN", "Like"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ME", "CHS", "我的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ME", "CHT", "我的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ME", "EN", "Me"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MESSENGER", "CHS", "聊聊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MESSENGER", "CHT", "聊聊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MESSENGER", "EN", "Messenger"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MY_BRAND", "CHS", "我的品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MY_BRAND", "CHT", "我的品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MY_BRAND", "EN", "My Brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEWSFEED", "CHS", "动态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEWSFEED", "CHT", "動態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEWSFEED", "EN", "Newsfeed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT", "CHS", "亲，没有找到相关商品。
您可以换个词再试试，或者看看我们的热门搜寻。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT", "CHT", "親，沒有找到相關商品。
您可以換個詞再試試，或者看看我們的热门搜寻。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT", "EN", "Sorry, your search did not match any products. Please try again or take a look at our recommendations below."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_COMMENT", "CHS", "条评论"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_COMMENT", "CHT", "條評論"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_COMMENT", "EN", "Comment(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PPL_RCMD_ITEM", "CHS", "人推荐了这件单品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PPL_RCMD_ITEM", "CHT", "人推薦了這件單品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PPL_RCMD_ITEM", "EN", "consumers recommend this item"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_1", "CHS", "有"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_1", "CHT", "有"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_1", "EN", "Showing"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_2", "CHS", "件商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_2", "CHT", "件商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NUM_PRODUCTS_2", "EN", "product(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OUTFIT", "CHS", "配搭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OUTFIT", "CHT", "配搭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OUTFIT", "EN", "Outfit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OVERALL", "CHS", "综合"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OVERALL", "CHT", "綜合"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_OVERALL", "EN", "Overall"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PDP_NUM_LIKE", "CHS", "人喜欢这件单品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PDP_NUM_LIKE", "CHT", "人喜歡這件單品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PDP_NUM_LIKE", "EN", "people like this product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE", "CHS", "价格"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE", "CHT", "價格"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE", "EN", "Price"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE_RANGE", "CHS", "价格区间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE_RANGE", "CHT", "價格區間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PRICE_RANGE", "EN", "Price Range"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DESC", "CHS", "商品描述"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DESC", "CHT", "商品描述"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DESC", "EN", "Product Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DETAILS", "CHS", "图文详情"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DETAILS", "CHT", "圖文詳情"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_DETAILS", "EN", "Product Details"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_REVIEW", "CHS", "商品评价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_REVIEW", "CHT", "商品評價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_REVIEW", "EN", "Product Review"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_SPEC", "CHS", "产品参数"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_SPEC", "CHT", "產品參數"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROD_SPEC", "EN", "Product Specifications"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_QUANTITY", "CHS", "数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_QUANTITY", "CHT", "數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_QUANTITY", "EN", "Quantity"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RCMD_TO_CONSUMER", "CHS", "猜您喜欢"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RCMD_TO_CONSUMER", "CHT", "猜您喜歡"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RCMD_TO_CONSUMER", "EN", "Recommended for You"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECENT_SEARCH", "CHS", "最近搜寻"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECENT_SEARCH", "CHT", "最近搜尋"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECENT_SEARCH", "EN", "Recent Searches"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED", "CHS", "推介"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED", "CHT", "推介"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED", "EN", "Recommended"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET", "CHS", "重设"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET", "CHT", "重設"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET", "EN", "Reset"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SALES", "CHS", "销量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SALES", "CHT", "銷量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SALES", "EN", "Sales"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_BRAND_PLACEHOLDER", "CHS", "要找什么品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_BRAND_PLACEHOLDER", "CHT", "要找什麼品牌？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_BRAND_PLACEHOLDER", "EN", "Search for Brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_FILTER_PLACEHOLDER", "CHS", "搜寻品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_FILTER_PLACEHOLDER", "CHT", "搜尋品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_FILTER_PLACEHOLDER", "EN", "Search for Brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_PLACEHOLDER", "CHS", "要买什么"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_PLACEHOLDER", "CHT", "要買什麼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH_PLACEHOLDER", "EN", "Search for Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SELECT_SIZE", "CHS", "选择尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SELECT_SIZE", "CHT", "選擇尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SELECT_SIZE", "EN", "Select Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPMENT_RATING", "CHS", "物流服务"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPMENT_RATING", "CHT", "物流服務"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPMENT_RATING", "EN", "Shipment Rating"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_ADDR", "CHS", "收货地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_ADDR", "CHT", "收貨地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_ADDR", "EN", "Shipping Address"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_CONTACT", "CHS", "收货人"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_CONTACT", "CHT", "收貨人"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_CONTACT", "EN", "Shipping Contact"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_METHOD", "CHS", "配送方式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_METHOD", "CHT", "配送方式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHIPPING_METHOD", "EN", "Shipping Method"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SINGLE_PRODUCT", "CHS", "单品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SINGLE_PRODUCT", "CHT", "單品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SINGLE_PRODUCT", "EN", "Single Product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SIZE", "CHS", "尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SIZE", "CHT", "尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SIZE", "EN", "Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT", "CHS", "排序"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT", "CHT", "排序"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT", "EN", "Sort"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RATING", "CHS", "店铺评分"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RATING", "CHT", "店舖評分"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RATING", "EN", "Store Rating"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RCMD", "CHS", "时尚穿搭贴心建议"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RCMD", "CHT", "時尚穿搭貼心建議"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STORE_RCMD", "EN", "Trendy Pick"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STYLEFEED", "CHS", "美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STYLEFEED", "CHT", "美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STYLEFEED", "EN", "Stylefeed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SUBMIT", "CHS", "确认"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SUBMIT", "CHT", "確認"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SUBMIT", "EN", "Submit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SWIPE_TO_BUY", "CHS", "一扫即买"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SWIPE_TO_BUY", "CHT", "一掃即買"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SWIPE_TO_BUY", "EN", "Swipe to Buy"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL", "CHS", "合计"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL", "CHT", "合計"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL", "EN", "Total"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TRENDING_SEARCH", "CHS", "热门搜寻"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TRENDING_SEARCH", "CHT", "熱門搜尋"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TRENDING_SEARCH", "EN", "Trending Search"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE", "CHS", "更改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE", "CHT", "更改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE", "EN", "Change"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_EMAIL", "CHS", "更改邮箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_EMAIL", "CHT", "更改郵箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_EMAIL", "EN", "Change Email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_MOBILE", "CHS", "更改手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_MOBILE", "CHT", "更改手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_MOBILE", "EN", "Change Mobile No."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_PASSWORD", "CHS", "更改密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_PASSWORD", "CHT", "更改密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_PASSWORD", "EN", "Change Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_STATUS", "CHS", "更改状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_STATUS", "CHT", "更改狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_STATUS", "EN", "Change Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_USER_MOBILE", "CHS", "更改用户手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_USER_MOBILE", "CHT", "更改用戶手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_USER_MOBILE", "EN", "Change User Mobile No."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHOOSE_MERCHANT_TYPE", "CHS", "选择商户类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHOOSE_MERCHANT_TYPE", "CHT", "選擇商戶類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHOOSE_MERCHANT_TYPE", "EN", "Choose Merchant Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY", "CHS", "城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY", "CHT", "城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY", "EN", "City"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY_REGION", "CHS", "城市/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY_REGION", "CHT", "城市/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CITY_REGION", "EN", "City/Region"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CLOSE", "CHS", "关闭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CLOSE", "CHT", "關閉"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CLOSE", "EN", "Close"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOR_IMAGE", "CHS", "商品颜色图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOR_IMAGE", "CHT", "商品顏色圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOR_IMAGE", "EN", "Color Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOUR", "CHS", "颜色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOUR", "CHT", "顏色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COLOUR", "EN", "Color"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO", "CHS", "公司商标"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO", "CHT", "公司商標"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO", "EN", "Company Logo"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_HEADER_NOTE", "CHS", "文件大小上限 {CONST_LOGO_FILE_SIZE}，(高)72px X (阔)236px，PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_HEADER_NOTE", "CHT", "文件大小上限 {CONST_LOGO_FILE_SIZE}，(高)72px X (闊)236px，PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_HEADER_NOTE", "EN", "Max. file size {CONST_LOGO_FILE_SIZE}, 72(h)px X 236(w)px, PNG format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_LARGE_NOTE", "CHS", "文件大小上限 {CONST_LOGO_FILE_SIZE}，(高)360px X (阔)360px，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_LARGE_NOTE", "CHT", "文件大小上限 {CONST_LOGO_FILE_SIZE}，(高)360px X (闊)360px，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_LARGE_NOTE", "EN", "Max. file size {CONST_LOGO_FILE_SIZE}, 360(h)px X 360(w)px, JPG/PNG format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_SMALL_NOTE", "CHS", "文件大小上限 {CONST_LOGO_FILE_SIZE}，(高)108px X (阔)108px，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_SMALL_NOTE", "CHT", "文件大小上限 {CONST_LOGO_FILE_SIZE}，(高)108px X (闊)108px，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_LOGO_SMALL_NOTE", "EN", "Max. file size {CONST_LOGO_FILE_SIZE}, 108(h)px X 108(w)px, JPG/PNG format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_NAME", "CHS", "公司注册名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_NAME", "CHT", "公司註冊名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_NAME", "EN", "Company Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION", "CHS", "完成注册"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION", "CHT", "完成註冊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION", "EN", "Complete New User Registration"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION_NOTE", "CHS", "请输入短信中的验证码已创建密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION_NOTE", "CHT", "請輸入短信中的驗證碼以創建密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPLETE_REGISTRATION_NOTE", "EN", "Please enter activation code from SMS
and create your password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMP_DESC", "CHS", "简介"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMP_DESC", "CHT", "簡介"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMP_DESC", "EN", "Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRM", "CHS", "确认"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRM", "CHT", "確認"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRM", "EN", "Confirm"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRMATION_IMPORT", "CHS", "确认上传"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRMATION_IMPORT", "CHT", "確認上傳"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRMATION_IMPORT", "EN", "Confirm to import"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_EMAIL", "CHS", "确认新邮箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_EMAIL", "CHT", "確認新郵箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_EMAIL", "EN", "Confirm New Email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_MOBILE", "CHS", "确认手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_MOBILE", "CHT", "確認手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_MOBILE", "EN", "Confirm Mobile No."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_PASSWORD", "CHS", "确认新密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_PASSWORD", "CHT", "確認新密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_PASSWORD", "EN", "Confirm New Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONGRATULATIONS", "CHS", "成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONGRATULATIONS", "CHT", "成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONGRATULATIONS", "EN", "Congratulations!"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONTENT", "CHS", "內容"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONTENT", "CHT", "內容"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONTENT", "EN", "Content"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY", "CHS", "国家/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY", "CHT", "國家/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY", "EN", "Country / Region"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY_PICK", "CHS", "请选择国家/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY_PICK", "CHT", "請選擇國家/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COUNTRY_PICK", "EN", "Please Select Country / Region"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE", "CHS", "创建"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE", "CHT", "創建"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE", "EN", "Create"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER", "CHS", "建立下一个"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER", "CHT", "創建下一個"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER", "EN", "Create Another"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER_USER", "CHS", "建立下一个"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER_USER", "CHT", "創建下一個"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_ANOTHER_USER", "EN", "Create Another"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MERCHANT", "CHS", "创建商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MERCHANT", "CHT", "創建商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MERCHANT", "EN", "Create Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MER_PROF_FILL_DETAILS", "CHS", "请填写下列信息以创建商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MER_PROF_FILL_DETAILS", "CHT", "請填寫下列信息以創建商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MER_PROF_FILL_DETAILS", "EN", "Please fill in the following information to create the merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_USER", "CHS", "创建用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_USER", "CHT", "創建用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_USER", "EN", "Create User"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_EMAIL", "CHS", "现在的邮箱地址："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_EMAIL", "CHT", "現在的郵箱地址："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_EMAIL", "EN", "Current email:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_MOBILE", "CHS", "现在的手机号码："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_MOBILE", "CHT", "現在的手機號碼："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CURRENT_MOBILE", "EN", "Current Mobile No.:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE", "CHS", "日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE", "CHT", "日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE", "EN", "Date"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TIME", "CHS", "日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TIME", "CHT", "日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TIME", "EN", "Date & Time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DEFAULT_RATE", "CHS", "默认佣金费率"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DEFAULT_RATE", "CHT", "默認佣金費率"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DEFAULT_RATE", "EN", "Default Rate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE", "CHS", "删除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE_USER", "CHS", "删除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE_USER", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE_USER", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DESCRIPTION_IMAGE", "CHS", "说明图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DESCRIPTION_IMAGE", "CHT", "說明圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DESCRIPTION_IMAGE", "EN", "Description Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DETAILS", "CHS", "详细信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DETAILS", "CHT", "詳細信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DETAILS", "EN", "Details"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISP_NAME", "CHS", "显示名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISP_NAME", "CHT", "顯示名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISP_NAME", "EN", "Display Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT", "CHS", "区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT", "CHT", "區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT", "EN", "District"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT_POSTCODE", "CHS", "区域 / 邮编"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT_POSTCODE", "CHT", "區域 / 郵編"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DISTRICT_POSTCODE", "EN", "District / Postcode"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DONE_BACK_TO_LISTING", "CHS", "完成并返回商品列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DONE_BACK_TO_LISTING", "CHT", "完成並返回商品列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DONE_BACK_TO_LISTING", "EN", "Done and back to listing"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DRAFTED", "CHS", "草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DRAFTED", "CHT", "草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DRAFTED", "EN", "Draft"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT", "CHS", "编辑"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT", "CHT", "編輯"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT", "EN", "Edit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_MERCHANT", "CHS", "编辑商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_MERCHANT", "CHT", "編輯商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_MERCHANT", "EN", "Edit Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_HEADER", "CHS", "查看商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_HEADER", "CHT", "查看商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_HEADER", "EN", "View Product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY", "CHS", "编辑库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY", "CHT", "編輯庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY", "EN", "Edit Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY_SINGLE", "CHS", "编辑库存 - {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY_SINGLE", "CHT", "編輯庫存 - {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PRODUCT_INVENTORY_SINGLE", "EN", "Edit Inventory - {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PROFILE", "CHS", "编辑个人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PROFILE", "CHT", "編輯個人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_PROFILE", "EN", "Edit Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_USER", "CHS", "编辑"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_USER", "CHT", "編輯"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_USER", "EN", "Edit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL", "CHS", "邮箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL", "CHT", "郵箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL", "EN", "Email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL_OR_MOBILE", "CHS", "邮箱地址或手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL_OR_MOBILE", "CHT", "郵箱地址或手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EMAIL_OR_MOBILE", "EN", "Email/Mobile No."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_ACTIVATION_CODE", "CHS", "请输入短信中的验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_ACTIVATION_CODE", "CHT", "請輸入短信中的驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_ACTIVATION_CODE", "EN", "Please enter activation code from SMS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_PASSWORD", "CHS", "输入密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_PASSWORD", "CHT", "輸入密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_PASSWORD", "EN", "Enter password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR", "CHS", "出现错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR", "CHT", "出現錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR", "EN", "Error Occurs"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERRORS", "CHS", "错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERRORS", "CHT", "錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERRORS", "EN", "Errors"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_DETAILS", "CHS", "错误信息："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_DETAILS", "CHT", "錯誤信息："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_DETAILS", "EN", "Error details:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EVENT_LOG", "CHS", "操作记录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EVENT_LOG", "CHT", "操作記錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EVENT_LOG", "EN", "Event/Log"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT", "CHS", "下载"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT", "CHT", "下載"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT", "EN", "Export"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY", "CHS", "下载库存纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY", "CHT", "下載庫存紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY", "EN", "Export Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY_NOTE", "CHS", "下载此位置下所有库存信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY_NOTE", "CHT", "下載此位置下所有庫存信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_INVENTORY_NOTE", "EN", "You are going to export inventory record in a xlsx file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS", "CHS", "下载商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS", "CHT", "下載商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS", "EN", "Export Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS_NOTE", "CHS", "下载所有商品信息於xlsx文件中"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS_NOTE", "CHT", "下載所有商品信息於xlsx文件中"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPORT_PRODUCTS_NOTE", "EN", "You are going to export all products in a xlsx file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURED_BRAND", "CHS", "精选品牌及商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURED_BRAND", "CHT", "精選品牌及商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURED_BRAND", "EN", "Featured Brand & Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURE_IMAGE", "CHS", "商品精选图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURE_IMAGE", "CHT", "商品精選圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FEATURE_IMAGE", "EN", "Featured Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILES", "CHS", "文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILES", "CHT", "文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILES", "EN", "Files"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_BR", "CHS", "营业执照资料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_BR", "CHT", "營業執照資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_BR", "EN", "Business Registration (BR) Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_EMAIL_RETRIEVE_PASSWORD", "CHS", "填写邮箱地址以获取您的密码。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_EMAIL_RETRIEVE_PASSWORD", "CHT", "填寫郵箱地址以獲取您的密碼。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_EMAIL_RETRIEVE_PASSWORD", "EN", "Fill in Email to retrieve your password."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_PASSWORD_COMPLETE", "CHS", "请输入密码以完成注册"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_PASSWORD_COMPLETE", "CHT", "請輸入密碼以完成註冊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_PASSWORD_COMPLETE", "EN", "Please fill in the password in order to complete your registration"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILTER_ALL_CITY", "CHS", "全部城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILTER_ALL_CITY", "CHT", "全部城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILTER_ALL_CITY", "EN", "All City"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FIRSTNAME", "CHS", "名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FIRSTNAME", "CHT", "名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FIRSTNAME", "EN", "First Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT", "CHS", "单位/公寓/住宅号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT", "CHT", "單位/公寓/住宅號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT", "EN", "Flat / Apartment / House"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT_OPT", "CHS", "单位/公寓/住宅号码 (选填)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT_OPT", "CHT", "單位/公寓/住宅號碼 (選填)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLAT_OPT", "EN", "Flat / Apartment / House (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR", "CHS", "楼层"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR", "CHT", "樓層"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR", "EN", "Floor"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR_OPT", "CHS", "楼层 (选填)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR_OPT", "CHT", "樓層 (選填)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FLOOR_OPT", "EN", "Floor (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FORGOT_PASSWORD", "CHS", "忘记密码?"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FORGOT_PASSWORD", "CHT", "忘記密碼?"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FORGOT_PASSWORD", "EN", "Forgot Password?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUID", "CHS", "GUID"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUID", "CHT", "GUID"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUID", "EN", "GUID"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE", "CHS", "如何准备图片：请参照图片的命名规则和上传限制说明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE", "CHT", "如何準備圖片：請參照圖片的命名規則和上傳限制說明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE", "EN", "How to prepare images: Please refer to the instructions for the image naming conventions and uploading limitations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE_DL", "CHS", "下载"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE_DL", "CHT", "下載"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GUIDELINE_DL", "EN", "Download"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HIDDEN", "CHS", "己隐藏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HIDDEN", "CHT", "己隱藏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HIDDEN", "EN", "Hidden"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HOME", "CHS", "主页"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HOME", "CHT", "主頁"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HOME", "EN", "Home"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_ALTTEXT", "CHS", "请输入图片替换文字 (1-100字符)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_ALTTEXT", "CHT", "請輸入圖片替換文字 (1-100字符)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_ALTTEXT", "EN", "Please enter alt text for image, max. 100 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_UPLOAD", "CHS", "上传图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_UPLOAD", "CHT", "上傳圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMAGE_UPLOAD", "EN", "Upload Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT", "CHS", "上传"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT", "CHT", "上傳"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT", "EN", "Import"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_ANOTHER_FILE", "CHS", "上传另一个文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_ANOTHER_FILE", "CHT", "上傳另一個文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_ANOTHER_FILE", "EN", "Import another file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_FILE", "CHS", "上传文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_FILE", "CHT", "上傳文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_FILE", "EN", "Import File"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_HISTORY", "CHS", "上传文件历史纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_HISTORY", "CHT", "上傳文件歷史紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_HISTORY", "EN", "Import History"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_INVENTORY", "CHS", "更新现有商品库存信息 (当通用商品代码 SKU 相同时)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_INVENTORY", "CHT", "更新現有商品庫存信息 (當通用商品代碼 SKU 相同時)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_INVENTORY", "EN", "Update the items in existing inventory list with the same product SKU"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_PRODUCT", "CHS", "更新现有商品信息 (当商品编码相同时)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_PRODUCT", "CHT", "更新現有商品信息 (當商品編碼相同時)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_OVERWRITE_EXIST_PRODUCT", "EN", "Overwrite the items in existing product list with the same product code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS", "CHS", "上传商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS", "CHT", "上傳商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS", "EN", "Import Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_CREATE", "CHS", "成功创建商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_CREATE", "CHT", "成功創建商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_CREATE", "EN", "Product created: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_DOWNLOAD_ERROR_FILE", "CHS", "下载有错误的文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_DOWNLOAD_ERROR_FILE", "CHT", "下載有錯誤的文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_DOWNLOAD_ERROR_FILE", "EN", "Download error file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FAILED", "CHS", "上传失败纪录：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FAILED", "CHT", "上傳失敗紀錄：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FAILED", "EN", "Record failed to upload: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FILE_ERROR_CELL", "CHS", "第 {0} 行   第 {1} 列："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FILE_ERROR_CELL", "CHT", "第 {0} 行   第 {1} 列："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_FILE_ERROR_CELL", "EN", "Row {0}, column {1}:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_FAILED", "CHS", "上传失败库存纪录：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_FAILED", "CHT", "上傳失敗庫存紀錄：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_FAILED", "EN", "Inventory record failed to upload: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_OVERWRITE", "CHS", "成功更新库存纪录: {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_OVERWRITE", "CHT", "成功更新庫存紀錄: {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_INVENTORY_OVERWRITE", "EN", "Inventory record updated: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_OVERWRITE", "CHS", "成功更新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_OVERWRITE", "CHT", "成功更新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_OVERWRITE", "EN", "Products updated: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SKIPPED", "CHS", "被略过商品: {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SKIPPED", "CHT", "被略過商品: {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SKIPPED", "EN", "Products skipped: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SUCCEED", "CHS", "上传商品成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SUCCEED", "CHT", "上傳商品成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCTS_SUCCEED", "EN", "Imported products successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY", "CHS", "上传库存纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY", "CHT", "上傳庫存紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY", "EN", "Import Inventory Records"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY_CREATE", "CHS", "成功创建库存纪录：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY_CREATE", "CHT", "成功創建庫存紀錄：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_PRODUCT_INVENTORY_CREATE", "EN", "Inventory record created: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_USER", "CHS", "暂停用户帐户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_USER", "CHT", "暫停用戶帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_USER", "EN", "Inactivate User"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVE", "CHS", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVE", "CHT", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVE", "EN", "Inactive"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INPUT_MERCHANT_NAME", "CHS", "请输入商户名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INPUT_MERCHANT_NAME", "CHT", "請輸入商戶名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INPUT_MERCHANT_NAME", "EN", "Please input the merchant name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INSERTED", "CHS", "添加"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INSERTED", "CHT", "添加"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INSERTED", "EN", "Inserted"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION", "CHS", "存货配额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION", "CHT", "存貨配額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION", "EN", "Allocation"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION_NOTE", "CHS", "输入数字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION_NOTE", "CHT", "輸入數字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOCATION_NOTE", "EN", "Enter a number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOC_FILTER", "CHS", "存货配额量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOC_FILTER", "CHT", "存貨配額量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ALLOC_FILTER", "EN", "No. of Allocation"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS", "CHS", "可出售量 (ATS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS", "CHT", "可出售量 (ATS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS", "EN", "ATS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS_RESULT", "CHS", "可出售量 (ATS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS_RESULT", "CHT", "可出售量 (ATS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ATS_RESULT", "EN", "Available to Sell (ATS)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LIST", "CHS", "库存列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LIST", "CHT", "庫存列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LIST", "EN", "Inventory List"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LISTS", "CHS", "库存列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LISTS", "CHT", "庫存列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LISTS", "EN", "Inventory Lists"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION", "CHS", "库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION", "CHT", "庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION", "EN", "Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_CHANGE", "CHS", "更改库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_CHANGE", "CHT", "更改庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_CHANGE", "EN", "Change Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_EDIT", "CHS", "编辑库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_EDIT", "CHT", "編輯庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATION_EDIT", "EN", "Edit Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ONORDER", "CHS", "准备出货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ONORDER", "CHT", "準備出貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_ONORDER", "EN", "On Order"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_EXPORTED", "CHS", "已出货量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_EXPORTED", "CHT", "已出貨量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_EXPORTED", "EN", "Quantity Exported"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_ORDERED", "CHS", "准备出货量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_ORDERED", "CHT", "準備出貨量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_QUANTITY_ORDERED", "EN", "Quantity Ordered"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_RESETTIME", "CHS", "库存重置时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_RESETTIME", "CHT", "庫存重置日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_RESETTIME", "EN", "Reset Date & Time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_INSTOCK", "CHS", "有现货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_INSTOCK", "CHT", "有現貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_INSTOCK", "EN", "In Stock"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_LOWSTOCK", "CHS", "存货紧张"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_LOWSTOCK", "CHT", "存貨緊張"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_LOWSTOCK", "EN", "Low in Stock"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_OUTOFSTOCK", "CHS", "缺货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_OUTOFSTOCK", "CHT", "缺貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_STATUS_OUTOFSTOCK", "EN", "Out of Stock"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_TURNOVER", "CHS", "已出货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_TURNOVER", "CHT", "已出貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_TURNOVER", "EN", "Turnover"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_UPDATE_HISTORY", "CHS", "库存纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_UPDATE_HISTORY", "CHT", "庫存紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_UPDATE_HISTORY", "EN", "Inventory History"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_CREATE", "CHS", "创建配额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_CREATE", "CHT", "創建配額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_CREATE", "EN", "Allocation Created"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_DELETE", "CHS", "删除配额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_DELETE", "CHT", "刪除配額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_DELETE", "EN", "Allocation Deleted"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_EDIT", "CHS", "编辑配额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_EDIT", "CHT", "編輯配額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_ALLOC_EDIT", "EN", "Allocation Edited"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_HIST_NOTE", "CHS", "您可以在这里看到SKU的库存更新历史记录，操作者和目前的可出售量 (ATS) 水平。请以前一个记录的数据作参照"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_HIST_NOTE", "CHT", "您可以在這裡看到SKU的庫存更新歷史記錄，操作者和目前的可出售量 (ATS) 水平。請以前一個記錄的數據作參照"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENT_HIST_NOTE", "EN", "You can see the Inventory update history of a SKU, who is responsible for the update and what is the current ATS level. Please refer to the previous figure of each record for former data"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE", "CHS", "语言"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE", "CHT", "語言"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE", "EN", "Language"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE_SWITCH", "CHS", "转换语言至"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE_SWITCH", "CHT", "轉換語言至"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANGUAGE_SWITCH", "EN", "Switch language to"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LASTNAME", "CHS", "姓"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LASTNAME", "CHT", "姓"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LASTNAME", "EN", "Last Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LLD", "CHS", "最后登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LLD", "CHT", "最後登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LLD", "EN", "Last Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LMD", "CHS", "最后更改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LMD", "CHT", "最後更改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LMD", "EN", "Last Modified"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING", "CHS", "页面载入中，请稍候⋯"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING", "CHT", "頁面載入中，請稍候⋯"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING", "EN", "Page loading, please wait..."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING_DATA", "CHS", "加载信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING_DATA", "CHT", "加載信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOADING_DATA", "EN", "Loading Data."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION", "CHS", "动作"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION", "CHT", "動作"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION", "EN", "Action"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_DELETE", "CHS", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_DELETE", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_DELETE", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_EDIT", "CHS", "编辑"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_EDIT", "CHT", "編輯"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_EDIT", "EN", "Edit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_INACTIVE", "CHS", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_INACTIVE", "CHT", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ACTION_INACTIVE", "EN", "Inactive"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CLOSE", "CHS", "关闭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CLOSE", "CHT", "關閉"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CLOSE", "EN", "Close"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CREATE", "CHS", "创建位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CREATE", "CHT", "創建位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_CREATE", "EN", "Create Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_DELETE_LABEL", "CHS", "删除位置状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_DELETE_LABEL", "CHT", "刪除位置狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_DELETE_LABEL", "EN", "Delete Location Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_EXTERNAL_CODE", "CHS", "位置代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_EXTERNAL_CODE", "CHT", "位置代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_EXTERNAL_CODE", "EN", "Location ID"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ID_CREATE", "CHS", "位置編碼是唯一的 (选填项)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ID_CREATE", "CHT", "位置編碼是唯一的 (選填項)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_ID_CREATE", "EN", "Location ID is unique (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_NO_DATA", "CHS", "没有可显示的信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_NO_DATA", "CHT", "沒有可顯示的信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_NO_DATA", "EN", "No Information to be displayed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK", "CHS", "位置安全库存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK", "CHT", "位置安全庫存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK", "EN", "Location Safety Threshold"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK_NOTE", "CHS", "输入数字 (可选)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK_NOTE", "CHT", "輸入數字 (可選)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SAFETY_STOCK_NOTE", "EN", "Enter a number (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SEARCH", "CHS", "搜索库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SEARCH", "CHT", "搜索庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SEARCH", "EN", "Search for Inventory Locations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SHOWING_PAGE", "CHS", "显示页"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SHOWING_PAGE", "CHT", "顯示頁"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_SHOWING_PAGE", "EN", "Showing Page"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS", "CHS", "状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS", "CHT", "狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS", "EN", "Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_ACTIVE", "CHS", "激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_ACTIVE", "CHT", "激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_ACTIVE", "EN", "Active"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_INACTIVE", "CHS", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_INACTIVE", "CHT", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_STATUS_INACTIVE", "EN", "Inactive"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE", "CHS", "更新库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE", "CHT", "更新庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE", "EN", "Update Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE_LABEL", "CHS", "更新位置状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE_LABEL", "CHT", "更新位置狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_UPDATE_LABEL", "EN", "Update Location Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITHOUT_ALLOC", "CHS", "无配額庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITHOUT_ALLOC", "CHT", "無配額庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITHOUT_ALLOC", "EN", "Location without Allocations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITH_ALLOC", "CHS", "有配额库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITH_ALLOC", "CHT", "有配額庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOCATION_WITH_ALLOC", "EN", "Location with Allocations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_ID", "CHS", "位置代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_ID", "CHT", "位置代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_ID", "EN", "Location ID"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_NAME", "CHS", "位置名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_NAME", "CHT", "位置名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_NAME", "EN", "Location Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_SELECT", "CHS", "库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_SELECT", "CHT", "庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_SELECT", "EN", "Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE", "CHS", "位置类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE", "CHT", "位置類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE", "EN", "Location Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE_SELECT", "CHS", "选择位置类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE_SELECT", "CHT", "選擇位置類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOC_TYPE_SELECT", "EN", "Select Location Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGIN", "CHS", "登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGIN", "CHT", "登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGIN", "EN", "Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGOUT", "CHS", "登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGOUT", "CHT", "登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGOUT", "EN", "Logout"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LTB_LOADING", "CHS", "载入中"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LTB_LOADING", "CHT", "載入中"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LTB_LOADING", "EN", "Loading"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT", "CHS", "商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT", "CHT", "商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT", "EN", "Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANTS", "CHS", "所有商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANTS", "CHT", "所有商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANTS", "EN", "Merchants"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP", "CHS", "商户应用端"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP", "CHT", "商戶應用端"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP", "EN", "Merchant App"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP_NAME", "CHS", "美美 - 商户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP_NAME", "CHT", "美美 - 商戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_APP_NAME", "EN", "MM - Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_TYPE", "CHS", "商户类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_TYPE", "CHT", "商戶類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_TYPE", "EN", "Merchant Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MIDDLE_NAME", "CHS", "中间名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MIDDLE_NAME", "CHT", "中間名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MIDDLE_NAME", "EN", "Middle Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_GUIDELINE", "CHS", "查看缺失的图片：打开列表查看有哪些缺失的图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_GUIDELINE", "CHT", "查看缺失的圖片：打開列表查看有哪些缺失的圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_GUIDELINE", "EN", "See the missing images: Open a list to see the total number of missing images and which one they are"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_VIEW", "CHS", "查看缺失的图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_VIEW", "CHT", "查看缺失的圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_REPORT_VIEW", "EN", "See Missing Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM", "CHS", "美美"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM", "CHT", "美美"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM", "EN", "MM"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM_LOGIN", "CHS", "登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM_LOGIN", "CHT", "登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM_LOGIN", "EN", "Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE", "CHS", "手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE", "CHT", "手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE", "EN", "Mobile No."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE_NOTE", "CHS", "请在手机号码前加国家代码和地区代码
如：+86-12345678"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE_NOTE", "CHT", "請在手機號碼前加國家代碼和地區代碼
如：+86-12345678"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MOBILE_NOTE", "EN", "Please specify country and area code in front of the mobile no.
e.g.: +86-12345678"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MSG_CHANGE_PRODUCT_STATUS", "CHS", "您要更改商品状态为 {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MSG_CHANGE_PRODUCT_STATUS", "CHT", "您要更改商品狀態為 {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MSG_CHANGE_PRODUCT_STATUS", "EN", "You are going to change the status of this product to  {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NA", "CHS", "不适用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NA", "CHT", "不適用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NA", "EN", "N/A"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NAME", "CHS", "名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NAME", "CHT", "名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NAME", "EN", "Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEWRATE", "CHS", "实际佣金费率"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEWRATE", "CHT", "實際佣金費率"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEWRATE", "EN", "New Rate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_EMAIL", "CHS", "新的电邮地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_EMAIL", "CHT", "新的電郵地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_EMAIL", "EN", "New Email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_MOBILE", "CHS", "新的手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_MOBILE", "CHT", "新的手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_MOBILE", "EN", "New Mobile No."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_PASSWORD", "CHS", "新密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_PASSWORD", "CHT", "新密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_PASSWORD", "EN", "New Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEXT", "CHS", "下一步"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEXT", "CHT", "下一步"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEXT", "EN", "Next"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO", "CHS", "否"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO", "CHT", "否"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO", "EN", "No"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_AVAILABLE_DATA", "CHS", "信息不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_AVAILABLE_DATA", "CHT", "信息不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_AVAILABLE_DATA", "EN", "No available data."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_FILE_CHOSEN", "CHS", "请选择文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_FILE_CHOSEN", "CHT", "請選擇文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_FILE_CHOSEN", "EN", "Please select file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_UPDATE_HIST", "CHS", "没有更新历史记录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_UPDATE_HIST", "CHT", "沒有更新歷史記錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NO_UPDATE_HIST", "EN", "No update history"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NUMBER_OF_INVENTORY", "CHS", "库存数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NUMBER_OF_INVENTORY", "CHT", "庫存數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NUMBER_OF_INVENTORY", "EN", "No. of Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OK", "CHS", "确定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OK", "CHT", "確定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OK", "EN", "OK"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OLD_PASSWORD", "CHS", "旧密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OLD_PASSWORD", "CHT", "舊密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OLD_PASSWORD", "EN", "Old Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OOPS", "CHS", "出错了"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OOPS", "CHT", "發現錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OOPS", "EN", "Oops... Error occurs"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OPTIONAL", "CHS", "(选填项)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OPTIONAL", "CHT", "(選塡項)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_OPTIONAL", "EN", "(Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGINATION", "CHS", "共 {2} 个结果，显示 {0} - {1} 个"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGINATION", "CHT", "共 {2} 個結果，顯示 {0} - {1} 個"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGINATION", "EN", "Showing {0} - {1} of {2} results"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD", "CHS", "密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD", "CHT", "密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD", "EN", "Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD_CHARACTER", "CHS", "密码必须至少为8个字符，其中包括1个大写字符，1个小写字符，1个特殊字符和1个数字字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD_CHARACTER", "CHT", "密碼必須至少為8個字符，其中包括1個大寫字符，1個小寫字符，1個特殊字符和1個數字字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PASSWORD_CHARACTER", "EN", "Password must be at least 8 characters including 1 uppercase letter, 1 lowercase letter, 1 special character and 1 alphanumeric character"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PENDING", "CHS", "待处理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PENDING", "CHT", "待處理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PENDING", "EN", "Pending"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PERSONAL_INFO", "CHS", "个人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PERSONAL_INFO", "CHT", "個人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PERSONAL_INFO", "EN", "Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PGN", "CHS", "第 {0} 条，共 {1} 条纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PGN", "CHT", "第 {0} 條，共 {1} 條紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PGN", "EN", "Showing {0} of {1} Results"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PHOTO_LIBRARY", "CHS", "从手机相册选择"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PHOTO_LIBRARY", "CHT", "從手機相册選擇"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PHOTO_LIBRARY", "EN", "Select from Photo Library"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PLATFORM", "CHS", "平台"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PLATFORM", "CHT", "平台"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PLATFORM", "EN", "Platform"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP", "CHS", "邮政编码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP", "CHT", "郵政編碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP", "EN", "Postal Code / Zip Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP_OPT", "CHS", "邮政编码 (选填)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP_OPT", "CHT", "郵政編碼 (選填)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_POSTAL_OR_ZIP_OPT", "EN", "Postal Code / ZIP Code (Optional)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVATION", "CHS", "激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVATION", "CHT", "激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVATION", "EN", "Activate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVE", "CHS", "已激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVE", "CHT", "已激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ACTIVE", "EN", "Activated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_ID", "CHS", "属性代号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_ID", "CHT", "屬性代號"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_ID", "EN", "Attribute ID"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_NAME", "CHS", "属性名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_NAME", "CHT", "屬性名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_ATTRIBUTE_NAME", "EN", "Attribute Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_FROM", "CHS", "上架日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_FROM", "CHT", "上架日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_FROM", "EN", "Available From (Date)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_TO", "CHS", "下架日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_TO", "CHT", "下架日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_DATE_TO", "EN", "Available To (Date)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_FROM", "CHS", "上架日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_FROM", "CHT", "上架日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_FROM", "EN", "Available Date Time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_FROM", "CHS", "上架时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_FROM", "CHT", "上架時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_FROM", "EN", "Available From (Time)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_TO", "CHS", "下架时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_TO", "CHT", "下架時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TIME_TO", "EN", "Available To (Time)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TO", "CHS", "下架日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TO", "CHT", "下架日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_AVAILABLE_TO", "EN", "Available To (Date & Time)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BARCODE", "CHS", "条形码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BARCODE", "CHT", "條形碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BARCODE", "EN", "Bar Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BASIC_INFO", "CHS", "商品信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BASIC_INFO", "CHT", "商品信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BASIC_INFO", "EN", "Product Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BRAND", "CHS", "品牌名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BRAND", "CHT", "品牌名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_BRAND", "EN", "Brand Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_CATEGORY_INFO", "CHS", "商品类别信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_CATEGORY_INFO", "CHT", "商品類目資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_CATEGORY_INFO", "EN", "Product Category Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_CODE", "CHS", "颜色编码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_CODE", "CHT", "顏色編碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_CODE", "EN", "Color Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_ID", "CHS", "颜色编码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_ID", "CHT", "顏色編碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_ID", "EN", "Colour Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES", "CHS", "商品颜色图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES", "CHT", "商品顏色圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES", "EN", "Color Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES_NOTE", "CHS", "最多可以添加{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT} 张图片；已经添加 {0} 张图片；更新{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT} 张图片 (每张图片不能超过{CONST_PRODUCT_IMG_FILE_SIZE}MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES_NOTE", "CHT", "最多可以添加{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT} 張圖片；已經添加 {0} 張圖片；更新{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT} 張圖片(每張圖片不能超過{CONST_PRODUCT_IMG_FILE_SIZE}MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_IMAGES_NOTE", "EN", "Max. {CONST_PRODUCT_COLOUR_IMG_FILE_NLIMIT} images allowed, {0}/{CONST_PRODUCT_COLOUR_IMG_FILE_NLIMIT} has been uploaded. (Each image with max. {CONST_PRODUCT_IMG_FILE_SIZE} allowed)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_NAME", "CHS", "颜色名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_NAME", "CHT", "顏色名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COLOUR_NAME", "EN", "Color Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COO", "CHS", "原产地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COO", "CHT", "原產地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_COO", "EN", "Country of Origin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION", "CHS", "介绍"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION", "CHT", "介紹"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION", "EN", "Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_AREA", "CHS", "商品说明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_AREA", "CHT", "商品說明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_AREA", "EN", "Product Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES", "CHS", "商品说明 (图片)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES", "CHT", "商品說明 (圖片)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES", "EN", "Description Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES_NOTE", "CHS", "最多可以添加{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}张图片；已经添加 {0} 张图片；更新{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}张图片 (每张图片不能超过{CONST_PRODUCT_IMG_FILE_SIZE}MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES_NOTE", "CHT", "最多可以添加{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}張圖片；已經添加 {0} 張圖片；更新{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}張圖片(每張圖片不能超過{CONST_PRODUCT_IMG_FILE_SIZE}MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_DESCRIPTION_IMAGES_NOTE", "EN", "Max. {CONST_PRODUCT_DESCRIPTION_IMG_FILE_NLIMIT} images allowed, {0}/{CONST_PRODUCT_DESCRIPTION_IMG_FILE_NLIMIT} has been uploaded. (Each image with max. {CONST_PRODUCT_IMG_FILE_SIZE} allowed)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURE", "CHS", "特征"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURE", "CHT", "特徵"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURE", "EN", "Feature"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES", "CHS", "商品精选图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES", "CHT", "商品精選圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES", "EN", "Featured Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES_NOTE", "CHS", "最多可以添加{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}张图片；已经添加 {0} 张图片；更新{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}张图片 (每张图片不能超过{CONST_PRODUCT_IMG_FILE_SIZE}MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES_NOTE", "CHT", "最多可以添加{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}張圖片；已經添加 {0} 張圖片；更新{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}張圖片(每張圖片不能超過{CONST_PRODUCT_IMG_FILE_SIZE}MB)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_FEATURED_IMAGES_NOTE", "EN", "Max. {CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT} images allowed, {0}/{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT} has been uploaded. (Each image with max. {CONST_PRODUCT_IMG_FILE_SIZE} allowed)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_HEIGHT", "CHS", "高度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_HEIGHT", "CHT", "高度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_HEIGHT", "EN", "Height (cm)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMAGES", "CHS", "商品图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMAGES", "CHT", "商品圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMAGES", "EN", "Product Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMPORT_SAMPLE_TEMPLATE", "CHS", "批量上传模版 (xlsx)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMPORT_SAMPLE_TEMPLATE", "CHT", "批量上傳模版 (xlsx)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_IMPORT_SAMPLE_TEMPLATE", "EN", "Batch Upload Template (xlsx)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVATION", "CHS", "下架"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVATION", "CHT", "下架"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVATION", "EN", "Inactivate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVE", "CHS", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVE", "CHT", "停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INACTIVE", "EN", "Inactive"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_DELETE", "CHS", "确认删除此库存纪录？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_DELETE", "CHT", "確認刪除此庫存紀錄？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_DELETE", "EN", "Confirm to delete this inventory record?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_INFORMATION", "CHS", "库存信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_INFORMATION", "CHT", "庫存資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_INVENTORY_INFORMATION", "EN", "Inventory Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LENGTH", "CHS", "长度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LENGTH", "CHT", "長度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LENGTH", "EN", "Length (cm)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST", "CHS", "商品列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST", "CHT", "商品列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST", "EN", "Product List"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST_SEARCH_RESULT", "CHS", "找到 {0} 条有关「{1}」的纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST_SEARCH_RESULT", "CHT", "找到 {0} 條有關「{1}」的紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_LIST_SEARCH_RESULT", "EN", "{0} search result(s) for ‘’{1}”"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MANFACTURER", "CHS", "生产厂家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MANFACTURER", "CHT", "生產廠家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MANFACTURER", "EN", "Manufacturer Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MATERIAL", "CHS", "材质"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MATERIAL", "CHT", "材質"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MATERIAL", "EN", "Product Material"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MERCHANDISE_CATEGORY", "CHS", "专题类别 {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MERCHANDISE_CATEGORY", "CHT", "專題類別 {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_MERCHANDISE_CATEGORY", "EN", "Merchandize Category {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NAME", "CHS", "商品名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NAME", "CHT", "商品名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NAME", "EN", "Product Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NEW_INVENTRY", "CHS", "创建库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NEW_INVENTRY", "CHT", "創建庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_NEW_INVENTRY", "EN", "Create Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PENDING", "CHS", "待处理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PENDING", "CHT", "待處理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PENDING", "EN", "Pending"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PRIMARY_CATEGORY", "CHS", "基本类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PRIMARY_CATEGORY", "CHT", "基本類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_PRIMARY_CATEGORY", "EN", "Primary Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_READY_TO_ACTIVATE", "CHS", "待激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_READY_TO_ACTIVATE", "CHT", "待激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_READY_TO_ACTIVATE", "EN", "Ready to activate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_RETAIL_PRICE", "CHS", "零售价
(人民币)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_RETAIL_PRICE", "CHT", "零售價
(人民幣)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_RETAIL_PRICE", "EN", "Retail Price
(CNY)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SALE_PRICE", "CHS", "销售价
(人民币)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SALE_PRICE", "CHT", "銷售價
(人民幣)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SALE_PRICE", "EN", "Sale Price 
(CNY)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SHIPPING_INFO", "CHS", "运费信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SHIPPING_INFO", "CHT", "運費資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SHIPPING_INFO", "EN", "Shipping Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE", "CHS", "尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE", "CHT", "尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE", "EN", "Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE_CODE", "CHS", "尺码编号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE_CODE", "CHT", "尺碼編號"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SIZE_CODE", "EN", "Size Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SKU", "CHS", "SKU代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SKU", "CHT", "SKU代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_SKU", "EN", "SKU Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_ACTIVE_TO_INACTIVE", "CHS", "下架后商品将不会显示在店面。确认将此商品下架？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_ACTIVE_TO_INACTIVE", "CHT", "下架後商品將不會顯示在店面。確認將此商品下架？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_ACTIVE_TO_INACTIVE", "EN", "The product will not be displayed in the storefront once it is inactivated. Confirm to inactivate this product?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_PENDING_TO_ACTIVE", "CHS", "激活后商品将显示在店面。确认激活此商品？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_PENDING_TO_ACTIVE", "CHT", "激活後商品將顯示在店面。確認激活此商品？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STATUS_PENDING_TO_ACTIVE", "EN", "The product will be displayed in storefront once it is activated. Confirm to activate this product?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLECODE", "CHS", "样式代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLECODE", "CHT", "樣式代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLECODE", "EN", "Style Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLE_CODE", "CHS", "样式代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLE_CODE", "CHT", "樣式代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_STYLE_CODE", "EN", "Style Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TAG_CODE", "CHS", "标签代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TAG_CODE", "CHT", "標籤代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TAG_CODE", "EN", "Badge Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TARGET_CONSUMER_GROUP", "CHS", "目标消费群"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TARGET_CONSUMER_GROUP", "CHT", "目標消費群"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_TARGET_CONSUMER_GROUP", "EN", "Target Consumer Group"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_UPC", "CHS", "条形码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_UPC", "CHT", "條形碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_UPC", "EN", "UPC"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_VARIATIONS", "CHS", "子商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_VARIATIONS", "CHT", "子商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_VARIATIONS", "EN", "Product Variations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WEIGHT", "CHS", "重量 (公斤)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WEIGHT", "CHT", "重量 (公斤)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WEIGHT", "EN", "Weight (kg)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WIDTH", "CHS", "宽度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WIDTH", "CHT", "寬度 (厘米)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_WIDTH", "EN", "Width (cm)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_YEAR_LAUNCHED", "CHS", "推出年份"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_YEAR_LAUNCHED", "CHT", "推出年份"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCT_YEAR_LAUNCHED", "EN", "Year Launched"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_FROM", "CHS", "销售价时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_FROM", "CHT", "銷售價時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_FROM", "EN", "Sales Price Available Date Time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_TO", "CHS", "销售价至"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_TO", "CHT", "銷售價至"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SALE_AVAIL_TO", "EN", "Sales Price Available To"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_TOTAL_INVEN", "CHS", "库存总量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_TOTAL_INVEN", "CHT", "庫存總量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_TOTAL_INVEN", "EN", "Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITHOUT_INVENTORY", "CHS", "无库存商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITHOUT_INVENTORY", "CHT", "無庫存商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITHOUT_INVENTORY", "EN", "Products without Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITH_INVENTORY", "CHS", "有库存商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITH_INVENTORY", "CHT", "有庫存商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_WITH_INVENTORY", "EN", "Products with Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_INFO", "CHS", "商户基本信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_INFO", "CHT", "商戶基本信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_INFO", "EN", "Profile Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_PIC", "CHS", "头像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_PIC", "CHT", "頭像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROFILE_PIC", "EN", "Profile Picture"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROVINCE", "CHS", "省"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROVINCE", "CHT", "省"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROVINCE", "EN", "Province"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH", "CHS", "发布日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH", "CHT", "發佈日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH", "EN", "Publish Time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISHED", "CHS", "己发布"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISHED", "CHT", "己發佈"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISHED", "EN", "Published"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_CREATE", "CHS", "成功发布新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_CREATE", "CHT", "成功發佈新商品：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_CREATE", "EN", "New product published: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_LOADING", "CHS", "正在准备发布，请稍候..."); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_LOADING", "CHT", "正在準備發佈，請稍候..."); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_LOADING", "EN", "Checking the draft products for publish, please wait..."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_NOTE", "CHS", "确定发布所有尚未发布的商品么？发布后商品将出现在用户应用端，用户可以浏览购买该商品。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_NOTE", "CHT", "確定現在發佈尚未所有的商品嗎？發布後商品將出現在用戶應用端，用戶可以瀏覽購買該商品。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_NOTE", "EN", "Are you sure to publish all draft products to the storefront now?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_PRODUCTS", "CHS", "发布所有草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_PRODUCTS", "CHT", "發佈所有草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_DRAFT_PRODUCTS", "EN", "Publish all draft"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_FAILED", "CHS", "发布失败纪录：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_FAILED", "CHT", "發佈失敗紀錄：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_FAILED", "EN", "Record failed to publish: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_SUCCEED", "CHS", "商品发布成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_SUCCEED", "CHT", "商品發佈成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PUBLISH_ALL_SUCCEED", "EN", "Published products successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PULL_DOWN_REFRESH", "CHS", "拉下即可刷新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PULL_DOWN_REFRESH", "CHT", "拉下即可刷新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PULL_DOWN_REFRESH", "EN", "Pull down to refresh page"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REENTER_PASSWORD", "CHS", "再次输入密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REENTER_PASSWORD", "CHT", "再次輸入密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REENTER_PASSWORD", "EN", "Re-enter password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REGISTER", "CHS", "注册"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REGISTER", "CHT", "註冊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REGISTER", "EN", "Register"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REPORT_PROBLEM", "CHS", "举报问题"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REPORT_PROBLEM", "CHT", "舉報問題"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REPORT_PROBLEM", "EN", "Report a Problem"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_ACTIVATION_CODE", "CHS", "重新发送验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_ACTIVATION_CODE", "CHT", "重新發送驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_ACTIVATION_CODE", "EN", "Resend activation code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_CHANGE_EMAIL_LINK", "CHS", "重新发送更改邮箱地址邮件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_CHANGE_EMAIL_LINK", "CHT", "重新發送更改郵箱地址郵件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_CHANGE_EMAIL_LINK", "EN", "Resend Change Email Link"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_REG_LINK", "CHS", "重新发送完成注册邮件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_REG_LINK", "CHT", "重新發送完成註冊郵件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESEND_REG_LINK", "EN", "Resend Registration Link"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET", "CHS", "重置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET", "CHT", "重置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET", "EN", "Reset"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PASSWORD_SMS", "CHS", "重新发送密码重置验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PASSWORD_SMS", "CHT", "重新發送密碼重置驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PASSWORD_SMS", "EN", "Resend Reset Password Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PW_USER", "CHS", "重置密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PW_USER", "CHT", "重置密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PW_USER", "EN", "Reset Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ROLE", "CHS", "角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ROLE", "CHT", "角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ROLE", "EN", "Role"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SAVE", "CHS", "保存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SAVE", "CHT", "保存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SAVE", "EN", "Save"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH", "CHS", "搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH", "CHT", "搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH", "EN", "Search"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PRODUCTS", "CHS", "搜寻商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PRODUCTS", "CHT", "搜尋商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PRODUCTS", "EN", "Search for Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PROD_SKU", "CHS", "按SKU搜寻商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PROD_SKU", "CHT", "按SKU搜尋商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_PROD_SKU", "EN", "Search for Product SKU"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEASON_LAUNCHED", "CHS", "推出季节"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEASON_LAUNCHED", "CHT", "推出季節"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEASON_LAUNCHED", "EN", "Season Launched"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEE", "CHS", "查看库存历史纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEE", "CHT", "查看庫存歷史紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEE", "EN", "See the previous activities for this inventory in"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_BRANDS", "CHS", "选择品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_BRANDS", "CHT", "選擇品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_BRANDS", "EN", "Select Brand(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_CITY", "CHS", "选择城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_CITY", "CHT", "選擇城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_CITY", "EN", "Select City"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_COUNTRY", "CHS", "选择国家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_COUNTRY", "CHT", "選擇國家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_COUNTRY", "EN", "Select Country"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_MERCHANT_TYPE", "CHS", "选择商户类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_MERCHANT_TYPE", "CHT", "選擇商戶類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_MERCHANT_TYPE", "EN", "Select Merchant Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_OPTION", "CHS", "请选择下列之一："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_OPTION", "CHT", "請選擇下列之一："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_OPTION", "EN", "Please select one of the following:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PRODUCT", "CHS", "选择商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PRODUCT", "CHT", "選擇商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PRODUCT", "EN", "Select Product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PROVINCE", "CHS", "选择省/州/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PROVINCE", "CHT", "選擇省/州/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_PROVINCE", "EN", "Select Province"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_ROLE", "CHS", "选择角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_ROLE", "CHT", "選擇角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_ROLE", "EN", "Select Role"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_STAFF", "CHS", "选择美美员工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_STAFF", "CHT", "選擇美美員工"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT_STAFF", "EN", "Select MayMay Staff(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEND", "CHS", "发送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEND", "CHT", "發送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEND", "EN", "Send"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SETTING", "CHS", "设置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SETTING", "CHT", "設置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SETTING", "EN", "Setting"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SIZE", "CHS", "尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SIZE", "CHT", "尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SIZE", "EN", "Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKIPPED", "CHS", "跳过"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKIPPED", "CHT", "跳過"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKIPPED", "EN", "Skipped"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_INVENTORY_SINGLE", "CHS", "管理库存 - {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_INVENTORY_SINGLE", "CHT", "管理庫存 - {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_INVENTORY_SINGLE", "EN", "Manage Inventory - {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_EXPORT", "CHS", "已出货订单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_EXPORT", "CHT", "已出貨訂單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_EXPORT", "EN", "Orders Exported"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_PLACE", "CHS", "已下单订单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_PLACE", "CHT", "已下單訂單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_ORDER_PLACE", "EN", "Order Placed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_SAFETY_STOCK", "CHS", "SKU安全库存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_SAFETY_STOCK", "CHT", "SKU安全庫存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_SAFETY_STOCK", "EN", "SKU Safety Threshold"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_TOTAL_LOCAT", "CHS", "库存位置总数"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_TOTAL_LOCAT", "CHT", "庫存位置總數"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_TOTAL_LOCAT", "EN", "Locations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS", "CHS", "状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS", "CHT", "狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS", "EN", "Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS_UNLIMITED", "CHS", "无限量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS_UNLIMITED", "CHT", "無限量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STATUS_UNLIMITED", "EN", "Unlimited"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP", "CHS", "手机应用端"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP", "CHT", "手机應用端"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP", "EN", "Storefront App"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP_NAME", "CHS", "美美"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP_NAME", "CHT", "美美"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STOREFRONT_APP_NAME", "EN", "MM"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NAME", "CHS", "请填写街道名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NAME", "CHT", "請填寫街道名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NAME", "EN", "Please fill in Street Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NUM", "CHS", "街道号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NUM", "CHT", "街道號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_STREET_NUM", "EN", "Street Number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBDOMAIN", "CHS", "子域名名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBDOMAIN", "CHT", "子域名名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBDOMAIN", "EN", "Subdomain Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBJECT", "CHS", "主题"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBJECT", "CHT", "主題"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUBJECT", "EN", "Subject"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUCCESS_UPLOAD_FILE", "CHS", "你的文件已成功上传，请点击「上傳 」按钮继续"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUCCESS_UPLOAD_FILE", "CHT", "你的文件已成功上傳，請點擊「上傳」按鈕繼續"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUCCESS_UPLOAD_FILE", "EN", "Your file is uploaded successfully, please click \"Import\" button to proceed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC", "CHS", "附加文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC", "CHT", "附加文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC", "EN", "Supporting Documentations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC_MSG", "CHS", "请在这里上传相关文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC_MSG", "CHT", "請在這裡上傳相關文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SUPPORTING_DOC_MSG", "EN", "Please upload the supporting documents here"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SYSTEM", "CHS", "系统"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SYSTEM", "CHT", "系統"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SYSTEM", "EN", "System"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TAKE_PHOTO", "CHS", "拍照"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TAKE_PHOTO", "CHT", "拍照"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TAKE_PHOTO", "EN", "Take Photo"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TIME_ZONE", "CHS", "时区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TIME_ZONE", "CHT", "時區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TIME_ZONE", "EN", "Time Zone"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_ALL_AC_USERS", "CHS", "所有用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_ALL_AC_USERS", "CHT", "所有用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_ALL_AC_USERS", "EN", "All Admin Centre Users"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_MERCHANT_STATUS", "CHS", "更改商户状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_MERCHANT_STATUS", "CHT", "更改商戶狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_MERCHANT_STATUS", "EN", "Change Merchant Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_PRODUCT_STATUS", "CHS", "更改商品状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_PRODUCT_STATUS", "CHT", "更改商品狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_PRODUCT_STATUS", "EN", "Change Product Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_USER_STATUS", "CHS", "更改用户账户状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_USER_STATUS", "CHT", "更改用戶帳戶狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_USER_STATUS", "EN", "Change User Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONF_DEL_USER", "CHS", "确认删除用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONF_DEL_USER", "CHT", "確認刪除用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONF_DEL_USER", "EN", "Confirm Delete User"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CREATE_USER", "CHS", "创建用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CREATE_USER", "CHT", "創建用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CREATE_USER", "EN", "Create User"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_EMAIL_SEND", "CHS", "邮件已发送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_EMAIL_SEND", "CHT", "郵件已發送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_EMAIL_SEND", "EN", "An Email is Sent"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_FORGOT_PASSWORD", "CHS", "忘记密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_FORGOT_PASSWORD", "CHT", "忘記密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_FORGOT_PASSWORD", "EN", "Forgot Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_LOGOUT", "CHS", "登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_LOGOUT", "CHT", "登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_LOGOUT", "EN", "Logout"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_RESET_PASSWORD", "CHS", "重置密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_RESET_PASSWORD", "CHT", "重置密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_RESET_PASSWORD", "EN", "Reset Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_SEND_ACTIVATION_CODE", "CHS", "请输入您的邮箱地址或手机号码以发送验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_SEND_ACTIVATION_CODE", "CHT", "請輸入您的郵箱地址或手機號碼以發送驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_SEND_ACTIVATION_CODE", "EN", "Please enter your email or mobile no., an activation code will be sent to you"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TOBE_PUBLISHED", "CHS", "即将发布"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TOBE_PUBLISHED", "CHT", "即將發佈"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TOBE_PUBLISHED", "EN", "To be Published"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPDATED", "CHS", "已更新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPDATED", "CHT", "已更新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPDATED", "EN", "Updated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD", "CHS", "上传"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD", "CHT", "上傳"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD", "EN", "Upload"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE", "CHS", "上传图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE", "CHT", "上傳圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE", "EN", "Upload Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE_FAILED", "CHS", "上传失败图片总数：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE_FAILED", "CHT", "上傳失敗圖片總數：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMAGE_FAILED", "EN", "Image failed to upload: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMG_DRAG", "CHS", "拖动图片到这里上传"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMG_DRAG", "CHT", "拖動圖片到這裡上傳"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_IMG_DRAG", "EN", "Drag images here to upload"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_FILE_STATUS", "CHS", "成功上传包含 {0} 件库存纪录的 {1}文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_FILE_STATUS", "CHT", "成功上傳包含 {0} 件庫存紀錄的 {1} 文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_FILE_STATUS", "EN", "You have just uploaded file: {1} with {0} inventory records"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_TEMPLATE_NOTE", "CHS", "请使用批量上传 xlsx 模版来创建和管理商品库存信息，你可以在这里下载"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_TEMPLATE_NOTE", "CHT", "請使用批量上傳 xlsx 模板來創建和管理商品庫存信息，你可以在這裏下載"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_INVENTORY_TEMPLATE_NOTE", "EN", "Please use the xlsx template to create and manage your inventory, you can download a"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_FILE_STATUS", "CHS", "成功上传包含 {0} 件商品的 {1}文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_FILE_STATUS", "CHT", "成功上傳包含 {0} 件商品的 {1} 文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_FILE_STATUS", "EN", "You have just uploaded file: {1} with {0} products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_TEMPLATE_NOTE", "CHS", "请使用批量上传 xlsx 模版来创建和管理商品信息，你可以在这里下载"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_TEMPLATE_NOTE", "CHT", "請使用批量上傳 xlsx 模板來創建和管理商品信息，你可以在這裏下載"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_PRODUCT_TEMPLATE_NOTE", "EN", "Please use the xlsx template to create and manage your products, you can download a"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_RESULT", "CHS", "上传结果"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_RESULT", "CHT", "上傳結果"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_RESULT", "EN", "Upload Result"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_SUCCESS", "CHS", "上传的图片总数：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_SUCCESS", "CHT", "上傳的圖片總數：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPLOAD_SUCCESS", "EN", "Images uploaded: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_COUNT", "CHS", "{0} 张图片上传失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_COUNT", "CHT", "{0} 張圖片上傳失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_COUNT", "EN", "{0} images upload failed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_IMAGE_NAME", "CHS", "图片名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_IMAGE_NAME", "CHT", "圖片名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_IMAGE_NAME", "EN", "Image Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_REASON", "CHS", "原因"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_REASON", "CHT", "原因"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_FAIL_REASON", "EN", "Reason"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_COL_TYPE", "CHS", "颜色类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_COL_TYPE", "CHT", "顏色類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_COL_TYPE", "EN", "Color Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_IMG_RANK", "CHS", "图片排名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_IMG_RANK", "CHT", "圖片排名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_IMG_RANK", "EN", "Image Rank"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_SELECT_OPTION", "CHS", "当商品图片已经存在时："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_SELECT_OPTION", "CHT", "當商品圖片已經存在時："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_IMG_SELECT_OPTION", "EN", "Please select when image already exist:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_LIMIT", "CHS", "最大上传每次{CONST_PRODUCT_IMG_MAX_NUM}张图片，每个图片最大{CONST_PRODUCT_IMG_FILE_SIZE}MB"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_LIMIT", "CHT", "最大上傳每次{CONST_PRODUCT_IMG_MAX_NUM}張圖片，每個圖片最大{CONST_PRODUCT_IMG_FILE_SIZE}MB"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_LIMIT", "EN", "Max. upload {CONST_PRODUCT_IMG_MAX_NUM}images each time, with max size of each image {CONST_PRODUCT_IMG_FILE_SIZE}MB"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_COUNT", "CHS", "{0} 张图片上传成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_COUNT", "CHT", "{0} 張圖片上傳成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_COUNT", "EN", "{0} images upload successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMAGE_NAME", "CHS", "图片名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMAGE_NAME", "CHT", "圖片名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMAGE_NAME", "EN", "Image Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMG_TYPE", "CHS", "图片类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMG_TYPE", "CHT", "圖片類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_IMG_TYPE", "EN", "Image Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_PROD_NAME", "CHS", "商品名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_PROD_NAME", "CHT", "商品名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UPL_SUC_PROD_NAME", "EN", "Product Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER", "CHS", "用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER", "CHT", "用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER", "EN", "User"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_COMPLETE_REGISTRATION_TITLE", "CHS", "重新发送验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_COMPLETE_REGISTRATION_TITLE", "CHT", "重新發送驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_COMPLETE_REGISTRATION_TITLE", "EN", "Resend Registration Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_MY_PROFILE", "CHS", "编辑个人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_MY_PROFILE", "CHT", "編輯個人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_MY_PROFILE", "EN", "Edit My Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_PROFILE", "CHS", "编辑{0}的个人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_PROFILE", "CHT", "編輯{0}的個人信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_EDIT_PROFILE", "EN", "Edit {0}\'s Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_VIEW", "CHS", "查看"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_VIEW", "CHT", "查看"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_VIEW", "EN", "View"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_YES", "CHS", "是"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_YES", "CHT", "是"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_YES", "EN", "Yes"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "CHS", "您将向{0}再次发送验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "CHT", "您將向{0}再次發送驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "EN", "You are going to resend registration code by SMS to {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ACTIVE_ACCOUNT_TIPS", "CHS", "账户创建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ACTIVE_ACCOUNT_TIPS", "CHT", "賬戶創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ACTIVE_ACCOUNT_TIPS", "EN", "Created new account successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_COMPLETE_REGISTRATION", "CHS", "请输入密码以完成注册"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_COMPLETE_REGISTRATION", "CHT", "請輸入密碼以完成註冊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_COMPLETE_REGISTRATION", "EN", "Please enter your password in order to complete your registration"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_ACTIVATE", "CHS", "帐户「{0}」将被激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_ACTIVATE", "CHT", "帳戶「{0}」將被激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_ACTIVATE", "EN", "Account \"{0}\" will be activated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_USER", "CHS", "将删除帐户「{0}」。这个动作无法复原"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_USER", "CHT", "將刪除帳戶「{0}」。這個動作無法復原"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_USER", "EN", "Account \"{0}\" will be deleted. This action cannot be undone"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_INACTIVATE", "CHS", "帐户「{0}」将被停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_INACTIVATE", "CHT", "帳戶「{0}」將被停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_INACTIVATE", "EN", "Account \"{0}\" will be inactivated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_USER_LOGOUT", "CHS", "确认登出？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_USER_LOGOUT", "CHT", "確認登出？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_USER_LOGOUT", "EN", "Confirm to logout?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING", "CHS", "您的账户处于待处理状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING", "CHT", "您的賬戶處理待處理狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING", "EN", "Your account is in pending status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING_TIPS", "CHS", "已经发送验证邮件给您，请查看邮箱并确认链接"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING_TIPS", "CHT", "已經發送驗證郵件給您，請查看郵箱并確認鏈接"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACCOUNT_PENDING_TIPS", "EN", "An email with a verification link has been sent to you, please check your email and click the link to confirm your identity"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATE_PENDING_FAIL", "CHS", "未能激活商品，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATE_PENDING_FAIL", "CHT", "未能激活商品，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATE_PENDING_FAIL", "EN", "Failed to activate pending products, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATION_CODE_NIL", "CHS", "请输入短信中的验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATION_CODE_NIL", "CHT", "請輸入短信中的驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ACTIVATION_CODE_NIL", "EN", "Please enter activation code from SMS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLOC_NIL", "CHS", "此项为必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLOC_NIL", "CHT", "此項為必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLOC_NIL", "EN", "This is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLO_FORMAT", "CHS", "库存配额必需为一个正整数"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLO_FORMAT", "CHT", "存貨配額必需為一個正整數"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ALLO_FORMAT", "EN", "Allocation must be a positive integer"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FORMAT", "CHS", "上架日期时间格式错误，请使用格式：「YYYY/MM/DD HH:MM:SS」"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FORMAT", "CHT", "上架日期時間格式錯誤，请使用格式：「YYYY/MM/DD HH:MM:SS」"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FORMAT", "EN", "Available From (Date & Time) needs to be in format: \"YYYY/MM/DD HH:MM:SS\""); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FUTURE", "CHS", "上架日期时间不能为过去的时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FUTURE", "CHT", "上架日期時間不能為過去的時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_FUTURE", "EN", "Available From (Date & Time) cannot be in the past"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_NIL", "CHS", "上架日期时间是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_NIL", "CHT", "上架日期時間是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_FROM_NIL", "EN", "Available From (Date & Time) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FORMAT", "CHS", "下架日期时间格式错误，请使用格式：「YYYY/MM/DD HH:MM:SS」"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FORMAT", "CHT", "下架日期時間格式錯誤，请使用格式：「YYYY/MM/DD HH:MM:SS」"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FORMAT", "EN", "Available To (Date & Time) needs to be in format: \"YYYY/MM/DD HH:MM:SS\""); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE", "CHS", "下架日期时间不能为过去的时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE", "CHT", "下架日期時間不能為過去的時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE", "EN", "Available To (Date & Time) cannot be in the past"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE_FROM", "CHS", "下架日期时间必须早于上架日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE_FROM", "CHT", "下架日期時間必須早於上架日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_FUTURE_FROM", "EN", "Available To (Date & Time) cannot be before Available From (Date & Time)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_NIL", "CHS", "下架日期时间是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_NIL", "CHT", "下架日期時間是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_DATE_TO_NIL", "EN", "Available To (Date & Time) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_FORMAT", "CHS", "上架时间格式错误，请使用时间格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_FORMAT", "CHT", "上架時間格式錯誤，請使用時間格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_FORMAT", "EN", "Available From (Time) needs to be in time format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_NIL", "CHS", "上架时间是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_NIL", "CHT", "上架時間是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_FROM_NIL", "EN", "Available From (Time) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_FORMAT", "CHS", "上架时间格式错误，請使用時間格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_FORMAT", "CHT", "上架時間格式錯誤，請使用時間格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_FORMAT", "EN", "Available From (Time) needs to be in time format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_NIL", "CHS", "上架时间是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_NIL", "CHT", "上架時間是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_AVAIL_TIME_TO_NIL", "EN", "Available From (Time) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_DUPLICATED", "CHS", "此条形码已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_DUPLICATED", "CHT", "此條形碼已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_DUPLICATED", "EN", "This Barcode already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_NIL", "CHS", "条形码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_NIL", "CHT", "條形碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_NIL", "EN", "Barcode is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_UNIQUE", "CHS", "每件商品只有一个条形码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_UNIQUE", "CHT", "每件商品只有一個條形碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BARCODE_UNIQUE", "EN", "A product can only have one Barcode"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_DUPLICATED", "CHS", "此品牌代码已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_DUPLICATED", "CHT", "此品牌代碼已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_DUPLICATED", "EN", "This Brand Code already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_FORMAT", "CHS", "品牌代码必须在1 - 25个字符之间，应用[a-z], [A-Z], [0-9] 或 \"-\"而且没有间隔"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_FORMAT", "CHT", "品牌代碼必須在1 - 25個字符之間，應用[a-z], [A-Z], [0-9] 或 \"-\"而且沒有間隔"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_FORMAT", "EN", "Brand Code must be between 1-25 character, all in [a-z], [A-Z], [0-9] or \"-\" and no space"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_INVALID", "CHS", "在品牌列表中没有找到您填写的品牌代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_INVALID", "CHT", "在品牌列表中沒有找到您填寫的品牌代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_INVALID", "EN", "Brand Code cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_MONO_BRAND", "CHS", "单品牌代理商只能销售一个品牌的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_MONO_BRAND", "CHT", "單品牌代理商只能銷售一個品牌的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_MONO_BRAND", "EN", "Mono-brand agent can only sell products of 1 brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_NIL", "CHS", "品牌代码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_NIL", "CHT", "品牌代碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_NIL", "EN", "Brand Code is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_SAME", "CHS", "同一件商品只能屬於一個品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_SAME", "CHT", "同一件商品只能屬於一個品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_SAME", "EN", "Products with have same Style Code belong to one brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_UNIQUE", "CHS", "每件商品只有一个品牌代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_UNIQUE", "CHT", "每件商品只有一個品牌代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_CODE_UNIQUE", "EN", "A product can only have one Brand Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME", "CHS", "显示名必须在1 - 25个字符之间。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME", "CHT", "顯示名必須在1 - 25個字符之間。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME", "EN", "Display name must be between 1 - 25 characters."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME_UNIQUE", "CHS", "品牌名称已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME_UNIQUE", "CHT", "品牌名稱已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_DISPLAY_NAME_UNIQUE", "EN", "Brand Display Name already exists"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BUSINESS_REG_NUM", "CHS", "请填写营业执照号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BUSINESS_REG_NUM", "CHT", "請填寫營業執照號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BUSINESS_REG_NUM", "EN", "Please fill in business registration no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CHANGE_STATUS_FAIL", "CHS", "无法更改用户状态。请重试。错误码："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CHANGE_STATUS_FAIL", "CHT", "無法更改用戶狀態，請重試。錯誤碼："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CHANGE_STATUS_FAIL", "EN", "Failed to change status. Please try again later. Error code:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_LOGO_NIL", "CHS", "请根据要求的全部尺寸上传公司商标图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_LOGO_NIL", "CHT", "請根據要求的全部尺寸上傳公司商標圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_LOGO_NIL", "EN", "Please upload company logo image in required sizes"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_NAME", "CHS", "请填写公司名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_NAME", "CHT", "請填寫公司名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMPANY_NAME", "EN", "Please fill in your Company Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CONTACT_ADMIN", "CHS", "请联系管理员解锁账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CONTACT_ADMIN", "CHT", "請聯繫管理員解鎖賬戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CONTACT_ADMIN", "EN", "Please contact your admin to unlock your account."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_INVALID", "CHS", "在原产地列表中没有找到你填写的原产地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_INVALID", "CHT", "在原產地列表中沒有找到你填寫的原產地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_INVALID", "EN", "Country of Origin cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_NIL", "CHS", "原产地是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_NIL", "CHT", "原產地是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_NIL", "EN", "Country of Origin is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_UNIQUE", "CHS", "每件商品只有一个原产地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_UNIQUE", "CHT", "每件商品只有一個原產地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COO_UNIQUE", "EN", "A product can only have one Country of Origin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CULTURE_CODE_MISSING", "CHS", "必填项不能为空"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CULTURE_CODE_MISSING", "CHT", "必填項不能為空"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CULTURE_CODE_MISSING", "EN", "This is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_CHANGE_FAIL", "CHS", "更改邮箱失敗，请重试。错误码："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_CHANGE_FAIL", "CHT", "更改郵箱失敗，請重試。錯誤碼："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_CHANGE_FAIL", "EN", "Failed to change email, please try again. Error code:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NIL", "CHS", "请输入您的邮箱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NIL", "CHT", "請輸入您的郵箱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NIL", "EN", "Please enter your Email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_MATCH", "CHS", "两次输入邮箱不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_MATCH", "CHT", "兩次輸入郵箱不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_MATCH", "EN", "Emails do not match"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_UNIQUE", "CHS", "邮箱地址已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_UNIQUE", "CHT", "郵箱地址已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NOT_UNIQUE", "EN", "Email address already registered"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NO_CHANGE", "CHS", "您输入的电邮地址和现有的一样"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NO_CHANGE", "CHT", "您輸入的電郵地址和現有的一樣"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_NO_CHANGE", "EN", "The email you entered is the same as the existing one"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_OR_MOBILE_NIL", "CHS", "请输入邮箱地址或手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_OR_MOBILE_NIL", "CHT", "請輸入郵箱地址或手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_OR_MOBILE_NIL", "EN", "Please enter your email or mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_PATTERN", "CHS", "邮箱格式错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_PATTERN", "CHT", "郵箱格式錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_PATTERN", "EN", "Invalid mail format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REENTER_NIL", "CHS", "请再次输入您的邮箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REENTER_NIL", "CHT", "請再次輸入您的郵箱地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REENTER_NIL", "EN", "Please enter your email again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REGISTERED", "CHS", "邮箱地址已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REGISTERED", "CHT", "郵箱地址已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_REGISTERED", "EN", "Email address already registered"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ERROR_OCCURS", "CHS", "很抱歉，您的访问出错了，请稍后再试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ERROR_OCCURS", "CHT", "很抱歉，您的訪問出錯了，請稍後再試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ERROR_OCCURS", "EN", "Sorry, something is not quite right, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_INVENTORY_FAIL", "CHS", "下载库存纪录文件失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_INVENTORY_FAIL", "CHT", "下載庫存紀錄文件失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_INVENTORY_FAIL", "EN", "Failed to export the inventory record list, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_PRODUCT_FAIL", "CHS", "下载商品文件失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_PRODUCT_FAIL", "CHT", "下載商品文件失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EXPORT_PRODUCT_FAIL", "EN", "Failed to export the product list, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIELDNAME_PATTERN", "CHS", "{0}格式错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIELDNAME_PATTERN", "CHT", "{0}格式錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIELDNAME_PATTERN", "EN", "Invalid {0} format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_SIZE", "CHS", "文件大小上限: {CONST_DOC_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_SIZE", "CHT", "文件大小上限: {CONST_DOC_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_SIZE", "EN", "Max. file size: {CONST_DOC_FILE_SIZE}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_FAIL", "CHS", "上传失败，请检查您的文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_FAIL", "CHT", "上傳失敗，請檢查您的文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_FAIL", "EN", "Failed to upload your file, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_NIL", "CHS", "请选择要上传的文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_NIL", "CHT", "請選擇要上傳的文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FILE_UPLOAD_NIL", "EN", "Please select a file to upload"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIRSTNAME_NIL", "CHS", "请输入您的名字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIRSTNAME_NIL", "CHT", "請輸入您的名字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FIRSTNAME_NIL", "EN", "Please enter your first name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_NIL", "CHS", "高度(厘米)是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_NIL", "CHT", "高度(厘米)是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_NIL", "EN", "Height (cm) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_POSITIVE", "CHS", "高度(厘米)必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_POSITIVE", "CHT", "高度(厘米)必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_HEIGHT_POSITIVE", "EN", "Height (cm) is greater than zero"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_INVALID_KEY", "CHS", "图像文件错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_INVALID_KEY", "CHT", "圖像文件錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_INVALID_KEY", "EN", "Image file does not exist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_NOT_FOUND", "CHS", "没有找到图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_NOT_FOUND", "CHT", "沒有找到圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_NOT_FOUND", "EN", "Image not found"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_RESIZE", "CHS", "头像大小上限: {CONST_PROFILE_PIC_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_RESIZE", "CHT", "頭像大小上限: {CONST_PROFILE_PIC_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_RESIZE", "EN", "Max. profile image size: {CONST_PROFILE_PIC_FILE_SIZE}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_UPLOAD", "CHS", "图片文件上传失败。请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_UPLOAD", "CHT", "圖片文件上傳失敗。請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_IMAGE_UPLOAD", "EN", "Failed to upload the image. Please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INSUFFICIENT_PERMISSION_MERCHANT_CTR", "CHS", "您的账户在商戶中心没有登录权限"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INSUFFICIENT_PERMISSION_MERCHANT_CTR", "CHT", "您的帳戶在商戶中心沒有登錄權限"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INSUFFICIENT_PERMISSION_MERCHANT_CTR", "EN", "You do not have permission to access the merchant centre"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_CREDENTIALS", "CHS", "您的用户名或密码不正确，您还有 {0} 重试机会"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_CREDENTIALS", "CHT", "您的用戶名或密碼不正確，尚餘 {0} 次重試機會"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_CREDENTIALS", "EN", "Invalid email/mobile or password combination, {0} more attempts are allowed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_EMAIL_OR_MOBILE", "CHS", "邮箱地址或手机号码不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_EMAIL_OR_MOBILE", "CHT", "郵箱地址或手機號碼不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_EMAIL_OR_MOBILE", "EN", "Email or mobile no. does not exist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO", "CHS", "检测到陌生的地理位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO", "CHT", "檢測到陌生的地理位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO", "EN", "It seems you are trying to login from another location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO_DETAIL", "CHS", "确认地理位置邮件已发送到 {0}，请点击邮件内的确认链接以重新登陆"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO_DETAIL", "CHT", "確認地理位置郵件已發送到 {0}， 請點擊郵件內的確認連結以重新登錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_GEO_DETAIL", "EN", "An email with a verification link has been sent to {0}. Please check your email and click the link to confirm your identity and new location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_MOBILE", "CHS", "手机号码不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_MOBILE", "CHT", "手機號碼不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_MOBILE", "EN", "Mobile no. does not exist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_FAIL", "CHS", "库存地点不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_FAIL", "CHT", "庫存地點不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_FAIL", "EN", "Inventory location does not exist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_NOT_FOUND", "CHS", "库存地点不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_NOT_FOUND", "CHT", "庫存地點不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_LOCATION_NOT_FOUND", "EN", "Inventory location does not exist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LASTNAME_NIL", "CHS", "请输入您的姓氏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LASTNAME_NIL", "CHT", "請輸入您的姓氏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LASTNAME_NIL", "EN", "Please enter your last name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_NIL", "CHS", "长度(厘米)是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_NIL", "CHT", "長度(厘米)是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_NIL", "EN", "Length (cm) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_POSITIVE", "CHS", "长度(厘米)必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_POSITIVE", "CHT", "長度(厘米)必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LENGTH_POSITIVE", "EN", "Length (cm) is greater than zero"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATIONID_NIL", "CHS", "位置代码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATIONID_NIL", "CHT", "位置代碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATIONID_NIL", "EN", "Location ID is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_CITY", "CHS", "请选择城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_CITY", "CHT", "請選擇城市"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_CITY", "EN", "Please select City"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_COUNTRY", "CHS", "请选择国家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_COUNTRY", "CHT", "請選擇國家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_COUNTRY", "EN", "Please select Country"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_DISTRICT", "CHS", "请填写区域"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_DISTRICT", "CHT", "請填寫區域"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_DISTRICT", "EN", "Please fill in District"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_EXTERNAL_CODE", "CHS", "位置代碼必须在1-25位之间，可以使用a-z, A-Z, 0-9, 以及 -"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_EXTERNAL_CODE", "CHT", "位置代碼必須在1-25位之間，可以使用a-z, A-Z, 0-9, 以及 -"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_EXTERNAL_CODE", "EN", "Location ID must be between 1 - 25 characters. Characters allowed: a-z, A-Z, 0-9, and -"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_NAME", "CHS", "請設置位置名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_NAME", "CHT", "请设置位置名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_NAME", "EN", "Please set Location Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_PROVINCE", "CHS", "请选择省/州/地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_PROVINCE", "CHT", "請選擇省/州/地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_PROVINCE", "EN", "Please select Province"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_SAFETY_STOCK", "CHS", "请输入正整数于安全库存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_SAFETY_STOCK", "CHT", "請輸入正整數於安全庫存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_SAFETY_STOCK", "EN", "Please enter positive integer for Location Safety Threshold"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NAME", "CHS", "请填写街道名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NAME", "CHT", "請填寫街道名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NAME", "EN", "Please fill in a Street Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NUMBER", "CHS", "请填写街道号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NUMBER", "CHT", "請填寫街道號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_STREET_NUMBER", "EN", "Please fill in a Street Number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_TYPE", "CHS", "请选择位置类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_TYPE", "CHT", "請選擇位置類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_TYPE", "EN", "Please select Location Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOC_NIL", "CHS", "请选择库存地点 (至少一个)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOC_NIL", "CHT", "請選擇庫存地點 (至少一個)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOC_NIL", "EN", "Please choose inventory location (at least one)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_LONG", "CHS", "生产厂家不能超过100个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_LONG", "CHT", "生產廠家不能超過100個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_LONG", "EN", "Manufacturer Name cannot be more than 100 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_NIL", "CHS", "生产厂家是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_NIL", "CHT", "生產廠家是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MANUFACTURER_NIL", "EN", "Manufacturer Name is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_NOTFOUND", "CHS", "在类别列表中没有找到你填写的专题类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_NOTFOUND", "CHT", "在類別列表中沒有找到你填寫的專題類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_NOTFOUND", "EN", "Merchandize Category cannot be found, please check the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_UNIQUE", "CHS", "在每个单元中只能填写一个专题类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_UNIQUE", "CHT", "在每個單元中只能填寫一個專題類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_UNIQUE", "EN", "Please put only one Merchandize Category in one cell"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAYNAME_NIL", "CHS", "请输入您的显示名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAYNAME_NIL", "CHT", "請輸入您的顯示名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAYNAME_NIL", "EN", "Please enter a Display Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME", "CHS", "显示名必须在1 - 25个字符之间。字符可用英文及数字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME", "CHT", "顯示名必須在1 - 25個字符之間。字符可用英文及數字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME", "EN", "Display Name must be between 1 - 25 characters. Characters allowed: alphabets and digits"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME_UNIQUE", "CHS", "商户名称已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME_UNIQUE", "CHT", "商戶名稱已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISPLAY_NAME_UNIQUE", "EN", "Merchant Display Name already exists."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISP_NAME", "CHS", "显示名必须在3 - 25个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISP_NAME", "CHT", "顯示名必須在3 - 25個字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_DISP_NAME", "EN", "Display Name must be between 3 - 25 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME", "CHS", "名字必须在1 - 50个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME", "CHT", "名字必須在1 - 50個字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME", "EN", "First Name must be between 1 - 50 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME_NIL", "CHS", "请输入您的名字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME_NIL", "CHT", "請輸入您的名字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_FIRSTNAME_NIL", "EN", "Please enter your First Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME", "CHS", "姓氏必须在1 - 50个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME", "CHT", "姓氏必須在1 - 50個字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME", "EN", "Last Name must be between 1 - 50 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME_NIL", "CHS", "请输入您的姓氏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME_NIL", "CHT", "請輸入您的姓氏"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LASTNAME_NIL", "EN", "Please enter your Last Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LIST_FAIL", "CHS", "读取商户列表失，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LIST_FAIL", "CHT", "讀取商戶列表失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LIST_FAIL", "EN", "Failed to load merchant list, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_HEADER", "CHS", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_HEADER", "CHT", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_HEADER", "EN", "Max. file size: {CONST_LOGO_FILE_SIZE}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_LARGE", "CHS", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_LARGE", "CHT", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_LARGE", "EN", "Max. file size: {CONST_LOGO_FILE_SIZE}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_SMALL", "CHS", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_SMALL", "CHT", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_LOGO_SMALL", "EN", "Max. file size: {CONST_LOGO_FILE_SIZE}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_MIDDLENAME", "CHS", "中间名必须在1 - 50个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_MIDDLENAME", "CHT", "中間名必須在1 - 50個字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_MIDDLENAME", "EN", "Middle name must be between 1 - 50 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_NOT_FOUND", "CHS", "商户不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_NOT_FOUND", "CHT", "商戶不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_NOT_FOUND", "EN", "Merchant does not exist."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_STATUS_FAIL", "CHS", "商户状态更新失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_STATUS_FAIL", "CHT", "商戶狀態更新失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_STATUS_FAIL", "EN", "Upload merchant status error, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_TYPE", "CHS", "请选择商户类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_TYPE", "CHT", "請選擇商戶類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANT_TYPE", "EN", "Please choose merchant type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_INVALID", "CHS", "在颜色列表中没有找到你填写的颜色编码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_INVALID", "CHT", "在顏色列表中沒有找到你填寫的顏色編碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_INVALID", "EN", "Color Code cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_UNIQUE", "CHS", "请把具有多个颜色编码的商品的每个代码在列表中分格填写"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_UNIQUE", "CHT", "請把具有多個顏色編碼的商品的每個代碼在列表中分格填寫"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_CODE_UNIQUE", "EN", "Please put each code in separated cell for products with multiple Color Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_LONG", "CHS", "颜色名称不能超过25个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_LONG", "CHT", "顏色名稱不能超過25個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_LONG", "EN", "Color name cannot exceed 25 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_UNIQUE", "CHS", "拥有相同样式代码的商品应该有不同的颜色名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_UNIQUE", "CHT", "擁有相同樣式代碼的商品應該有不同的顏色名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_COLOUR_NAME_UNIQUE", "EN", "Color name is unique for all products with the same Style Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_INVALID", "CHS", "在尺码列表中没有找到你填写的尺码编号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_INVALID", "CHT", "在尺碼列表中沒有找到你填寫的尺碼編號"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_INVALID", "EN", "Size Code cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_UNIQUE", "CHS", "把具有多个尺码编号的商品的每个代码在列表中分格填写"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_UNIQUE", "CHT", "請把具有多個尺碼編號的商品的每個代碼在列表中分格填寫"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MM_SIZE_CODE_UNIQUE", "EN", "Please put each code in separated cell for products with multiple Size Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_ACTIVATION_CODE_NOT_MATCH", "CHS", "手机号码与验证码不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_ACTIVATION_CODE_NOT_MATCH", "CHT", "手機號碼與驗證碼不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_ACTIVATION_CODE_NOT_MATCH", "EN", "The mobile no. and activation code do not match"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_MATCH", "CHS", "两次输入的手机号码不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_MATCH", "CHT", "兩次輸入的手機號碼不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_MATCH", "EN", "Mobile no. doesn\'t match"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NIL", "CHS", "请输入您的手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NIL", "CHT", "請輸入您的手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NIL", "EN", "Please enter your mobile number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NO_CHANGE", "CHS", "你所输入的手机号码与现有号码一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NO_CHANGE", "CHT", "你所輸入的手機號碼與現有號碼一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NO_CHANGE", "EN", "The mobile no. you entered is the same as the existing one"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_PATTERN", "CHS", "请填入与所选国家一致的手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_PATTERN", "CHT", "請填入與所選國家一致的手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_PATTERN", "EN", "Please fill in the mobile no. from the country you have selected"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_REGISTERED", "CHS", "手机号码已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_REGISTERED", "CHT", "手機號碼已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_REGISTERED", "EN", "Mobile no. is already registered"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NETWORK_FAIL", "CHS", "网络断掉了，请再试一次吧"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NETWORK_FAIL", "CHT", "網絡斷掉了，請再試一次吧"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NETWORK_FAIL", "EN", "Network is lost, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NIL", "CHS", "此项为必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NIL", "CHT", "此項為必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NIL", "EN", "This is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NOT_FOUND", "CHS", "纪录不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NOT_FOUND", "CHT", "紀錄不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NOT_FOUND", "EN", "Record does not exist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NIL", "CHS", "请输入您的密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NIL", "CHT", "請輸入您的密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NIL", "EN", "Please enter your password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NOT_MATCH", "CHS", "两次密码输入不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NOT_MATCH", "CHT", "兩次密碼輸入不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_NOT_MATCH", "EN", "Passwords do not match"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_REENTER_NOT_MATCH", "CHS", "两次输入的密码不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_REENTER_NOT_MATCH", "CHT", "兩次輸入的密碼不一致"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_REENTER_NOT_MATCH", "EN", "Passwords do not match"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_RESET_CODE_EXCEEDED", "CHS", "发送验证码次数超出限制，请联系管理员以重置密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_RESET_CODE_EXCEEDED", "CHT", "發送驗證碼次數超出限制，請聯繫管理員以重置密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_RESET_CODE_EXCEEDED", "EN", "You have reached the max. attempts to send activation code, please contract admin to reset password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_WRONG", "CHS", "密码多次输入错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_WRONG", "CHT", "密碼多次輸入錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PASSWORD_WRONG", "EN", "You have attempted too many times to login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NIL", "CHS", "基本类别是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NIL", "CHT", "基本類別是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NIL", "EN", "Primary Category is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NOTFOUND", "CHS", "在分类列表中没有找到你填写的基本类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NOTFOUND", "CHT", "在分類列表中沒有找到你填寫的基本類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_NOTFOUND", "EN", "Primary Category cannot be found, please check the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_UNIQUE", "CHS", "商品应只有一个基本类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_UNIQUE", "CHT", "商品應只有一個基本類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRIMARY_CATEGORY_UNIQUE", "EN", "A product can only have one Primary Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_FORMAT", "CHS", "请上传{0}文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_FORMAT", "CHT", "請上傳{0}文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_FORMAT", "EN", "Please upload a {0} file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_SIZE", "CHS", "文件大小上限: {CONST_PRODUCT_IMPORT_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_SIZE", "CHT", "文件大小上限: {CONST_PRODUCT_IMPORT_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODCUT_IMPORT_FILE_SIZE", "EN", "Max. file size: {CONST_PRODUCT_IMPORT_FILE_SIZE}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_COL_LIMIT", "CHS", "商品颜色图片上限：{CONST_PRODUCT_COLOUR_IMG_FILE_NLIMIT}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_COL_LIMIT", "CHT", "商品顏色圖片上限：{CONST_PRODUCT_COLOUR_IMG_FILE_NLIMIT}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_COL_LIMIT", "EN", "Max no. of color image: {CONST_PRODUCT_COLOUR_IMG_FILE_NLIMIT}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_DESC_LIMIT", "CHS", "描述图片上限：{CONST_PRODUCT_DESCRIPTION_IMG_FILE_NLIMIT}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_DESC_LIMIT", "CHT", "描述圖片上限：{CONST_PRODUCT_DESCRIPTION_IMG_FILE_NLIMIT}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_DESC_LIMIT", "EN", "Max no. of description image: {CONST_PRODUCT_DESCRIPTION_IMG_FILE_NLIMIT}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FEAT_LIMIT", "CHS", "商品精选图片上限：{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FEAT_LIMIT", "CHT", "商品精選圖片上限：{CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FEAT_LIMIT", "EN", "Max no. of featured image: {CONST_PRODUCT_FEATURED_IMG_FILE_NLIMIT}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FORMAT", "CHS", "图片必须为{CONST_PRODUCT_IMG_FILE_FORMAT}格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FORMAT", "CHT", "圖片必須為{CONST_PRODUCT_IMG_FILE_FORMAT}格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_FORMAT", "EN", "Image format must be {CONST_PRODUCT_IMG_FILE_FORMAT}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_IGN_EXISTING", "CHS", "忽略现有的产品图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_IGN_EXISTING", "CHT", "忽略現有的產品圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_IGN_EXISTING", "EN", "Ignore already existing product images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NIL", "CHS", "请上传至少1张商品精选图片或与颜色相关的图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NIL", "CHT", "請上傳至少1張商品精選圖片或與顏色相關的圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NIL", "EN", "Please upload at least 1 Featured Image or Color Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NLIMIT", "CHS", "已经达到图片上传上限"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NLIMIT", "CHT", "已經達到圖片上傳上限"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NLIMIT", "EN", "You have reached the max no. of uploaded image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_COL", "CHS", "找不到颜色，请输入简体中文颜色名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_COL", "CHT", "找不到顏色，請輸入簡體中文顏色名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_COL", "EN", "Cannot map color. Please input Color Name in Simplified Chinese"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_DESC", "CHS", "商品说明配对失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_DESC", "CHT", "商品說明配對失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_DESC", "EN", "Failed to map Product Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_FEAT", "CHS", "商品特征配对失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_FEAT", "CHT", "商品特徵配對失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_FEAT", "EN", "Failed to map Product Feature"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_STYLE", "CHS", "找不到此样式代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_STYLE", "CHT", "找不到此樣式代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_NO_STYLE", "EN", "Cannot map this Style Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_SIZE", "CHS", "图像文件大小不能超过{CONST_PRODUCT_IMG_FILE_SIZE}MB"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_SIZE", "CHT", "圖像文件大小不能超過{CONST_PRODUCT_IMG_FILE_SIZE}MB"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMAGE_SIZE", "EN", "Image file size cannot exceed {CONST_PRODUCT_IMG_FILE_SIZE}MB"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMG_ALTTEXT_CLIMIT", "CHS", "图片替换文字必须在1 - 100字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMG_ALTTEXT_CLIMIT", "CHT", "圖片替換文字必須在1 - 100字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMG_ALTTEXT_CLIMIT", "EN", "Image alt text must be between 1 - 100 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMPORT", "CHS", "商品信息上传失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMPORT", "CHT", "商品信息上傳失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_IMPORT", "EN", "Failed to import product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_PUBLISH", "CHS", "当商品至少有一张商品精选图片或与颜色相关的图片并且商品介绍及价格不为零时，商品才能被发布"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_PUBLISH", "CHT", "當商品至少有一張商品精選圖片或與顏色相關的圖片並且商品介紹及價格不為零時，商品才能被發佈"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_PUBLISH", "EN", "A product cannot be published if: there is at least one Featured or Color Image, and Description and Price cannot be null"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_LONG", "CHS", "商品介绍不能超过1000个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_LONG", "CHT", "商品介紹不能超過1000個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_LONG", "EN", "Product Description cannot exceed 1,000 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_NIL", "CHS", "商品介绍是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_NIL", "CHT", "商品介紹是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_DESC_NIL", "EN", "Product Description is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_FEATURE_LONG", "CHS", "商品特征不能超过1000个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_FEATURE_LONG", "CHT", "商品特徵不能超過1000個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_FEATURE_LONG", "EN", "Product Feature cannot exceed 1,000 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_MATERIAL_LONG", "CHS", "商品材质不能超过1000个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_MATERIAL_LONG", "CHT", "商品材質不能超過1000個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_MATERIAL_LONG", "EN", "Product Material cannot exceed 1,000 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_DUPLICATE", "CHS", "商品名称已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_DUPLICATE", "CHT", "商品名稱已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_DUPLICATE", "EN", "Product Name already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_LONG", "CHS", "商品名称不能超过100个字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_LONG", "CHT", "商品名稱不能超過100個字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_LONG", "EN", "Product Name cannot exceed 100 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_NIL", "CHS", "商品名称是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_NIL", "CHT", "商品名稱是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROD_NAME_NIL", "EN", "Product Name is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_NIL", "CHS", "请上传个人信息封面图"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_NIL", "CHT", "請上傳個人信息封面圖"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_NIL", "EN", "Please upload profile banner image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_SIZE", "CHS", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_SIZE", "CHT", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PROFILE_BANNER_SIZE", "EN", "Max. file size: {CONST_LOGO_FILE_SIZE}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PUBLISH_DRAFT_FAIL", "CHS", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PUBLISH_DRAFT_FAIL", "CHT", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PUBLISH_DRAFT_FAIL", "EN", "Max. file size: {CONST_LOGO_FILE_SIZE}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REFERENCE_FAIL", "CHS", "上传文件失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REFERENCE_FAIL", "CHT", "上傳文件失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REFERENCE_FAIL", "EN", "Failed to upload the file, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REQUIRED_FIELD_MISSING", "CHS", "必填项不能为空"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REQUIRED_FIELD_MISSING", "CHT", "必填項不能為空"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_REQUIRED_FIELD_MISSING", "EN", "Please fill in all the required fields"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_OLD_PASSWORD_WRONG", "CHS", "您输入的旧密码不正确"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_OLD_PASSWORD_WRONG", "CHT", "您輸入的舊密碼不正確"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_OLD_PASSWORD_WRONG", "EN", "The old password is incorrect"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_FAIL", "CHS", "无法重置密码，请重试。错误码：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_FAIL", "CHT", "無法重置密碼，請重試。錯誤碼：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_FAIL", "EN", "Failed to reset password, please try again later. Error code: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_NIL", "CHS", "请输入新密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_NIL", "CHT", "請輸入新密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_NIL", "EN", "Please enter your new password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_REENTER_NIL", "CHS", "请再次输入新密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_REENTER_NIL", "CHT", "請再次輸入新密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESET_PASSWORD_REENTER_NIL", "EN", "Please re-enter your new password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_NIL", "CHS", "商品零售价是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_NIL", "CHT", "商品零售價是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_NIL", "EN", "Product Retail Price is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_POSITIVE", "CHS", "商品零售价必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_POSITIVE", "CHT", "商品零售價必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_POSITIVE", "EN", "Product Retail Price is a positive number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_DECIMAL", "CHS", "商品零售价最多支持小数点后两位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_DECIMAL", "CHT", "商品零售價最多支持小數點後兩位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_DECIMAL", "EN", "Product Retail Price needs to be rounded to 2 digits after the decimal"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ROLE_NIL", "CHS", "请选择至少一种角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ROLE_NIL", "CHT", "請選擇至少一種角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ROLE_NIL", "EN", "Please choose at least one role"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_DECIMAL", "CHS", "商品销售价最多支持小数点后两位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_DECIMAL", "CHT", "商品銷售價最多支持小數點後兩位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_DECIMAL", "EN", "Product Sale Price needs to be rounded to 2 digits after the decimal"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_POSITIVE", "CHS", "商品销售价必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_POSITIVE", "CHT", "商品銷售價必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_POSITIVE", "EN", "Product Sale Price is a positive number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_INVALID", "CHS", "销售价格式错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_INVALID", "CHT", "銷售價格式錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_INVALID", "EN", "Sale price is invalid"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_NIL", "CHS", "商品销售价是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_NIL", "CHT", "商品銷售價是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_NIL", "EN", "Product sale price is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMP_DESC_FORMAT", "CHS", "公司简介必须在1 - 50个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMP_DESC_FORMAT", "CHT", "公司簡介必須在1 - 50個字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMP_DESC_FORMAT", "EN", "Company Description must be between 1 - 50 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SUBDOMAIN", "CHS", "子域名必须在1 - 25个字符之间。字符可用: a-z, A-Z, 0-9"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SUBDOMAIN", "CHT", "子域名必須在1 - 25個字符之間。字符可用：a-z, A-Z, 0-9"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SUBDOMAIN", "EN", "Subdomain must be between 1 - 25 characters. Characters allowed: a-z, A-Z, 0-9"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TAG_INVALID", "CHS", "在标签列表中没有找到你填写的标签代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TAG_INVALID", "CHT", "在標籤列表中沒有找到你填寫的標籤代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TAG_INVALID", "EN", "This Badge Code cannot be found in the Master Set"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_EXTRA", "CHS", "{0}不属于上传模版中定义的字段名，无法创建/更新记录。请使用上传模版中所定义的字段名。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_EXTRA", "CHT", "{0}不屬於上傳模版中定義的字段名，無法創建/更新紀錄。請使用上傳模版中所定義的字段名。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_EXTRA", "EN", "{0} does not belong to the \"Template\" sheet. Please use the correct attribute names."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_MISSING", "CHS", "没有找到{0}，无法创建/更新纪录。请使用上传模版中所定义的字段名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_MISSING", "CHT", "沒有找到{0}，無法創建/更新紀錄。請使用上傳模版中所定義的字段名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_MISSING", "EN", "{0} is missing in the \"Template\" sheet. Please use the correct attribute names"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_WRONG_ORDER", "CHS", "上传的文件中的字段名顺序错误，无法创建/更新纪录。请使用与商品上传模版中相同的表格结构"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_WRONG_ORDER", "CHT", "上傳的文件中的字段名順序錯誤,無法創建/更新紀錄。請使用與商品上傳模版中相同的表格結構"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_COLUMN_WRONG_ORDER", "EN", "The attributes in the \"Template\" sheet in the file you just uploaded are not in the correct order. Please make sure you use the same order in the xlsx template"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_EMPTY", "CHS", "名为「Template」 的表格里面没有任何纪录，无法创建/更新纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_EMPTY", "CHT", "名為「Template」的表格里面沒有任何紀錄，無法創建/更新紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_EMPTY", "EN", "The \"Template\" sheet in the file you just uploaded is empty"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_MISSING", "CHS", "上传的文件中没有名为「Template」的表格，无法创建/更新纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_MISSING", "CHT", "上傳的文件中沒有名為「Template」的表格，無法創建/更新紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_MISSING", "EN", "We cannot find \"Template\" sheet in the fie you just uploaded"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_WRONG_FORMAT", "CHS", "上传的文件不是 XLSX 格式，无法完成纪录创建/更新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_WRONG_FORMAT", "CHT", "上傳的文件不是 XLSX 格式，無法完成紀錄創建/更新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TEMPLATE_WRONG_FORMAT", "EN", "The file you just uploaded is not a xlsx file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_DUPLICATED", "CHS", "此条形码已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_DUPLICATED", "CHT", "此條形碼已存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_DUPLICATED", "EN", "UPC code already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_INVALID", "CHS", "条形码不正确"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_INVALID", "CHT", "條形碼不正確"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_INVALID", "EN", "UPC code is incorrect"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_NIL", "CHS", "条形码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_NIL", "CHT", "條形碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_NIL", "EN", "UPC is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_UNIQUE", "CHS", "每件商品只有一个条形码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_UNIQUE", "CHT", "每件商品只有一個條形碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_UPC_UNIQUE", "EN", "A product can only have one UPC"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_FAIL", "CHS", "无法激活此用户，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_FAIL", "CHT", "無法激活此用戶，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_FAIL", "EN", "Failed to activate user, please contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_DONE", "CHS", "用户验证无法完成，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_DONE", "CHT", "用戶驗證無法完成，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_DONE", "EN", "User activation token cannot be completed, please contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_EXPIRED", "CHS", "用户验证码己过期，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_EXPIRED", "CHT", "用戶驗證碼己過期，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_EXPIRED", "EN", "User activation token has expired, please contact administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_INVALID", "CHS", "用户验证码無效，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_INVALID", "CHT", "用戶驗證碼無效，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_INVALID", "EN", "User activation token invalid, please contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND", "CHS", "无法找到此用户验证码，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND", "CHT", "無法找到此用戶驗證碼，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ACTIVATION_TOKEN_NOT_FOUND", "EN", "User activation token cannot be found, please contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ATTEMPTS_EXCEEDED", "CHS", "错误登录次数超出限制，请点击邮箱内的确认连结以重置密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ATTEMPTS_EXCEEDED", "CHT", "錯誤登錄次數超出限制，請點擊郵箱內的確認連結以重置密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ATTEMPTS_EXCEEDED", "EN", "You have reached the max. attempts to login, please check your email for activation link"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_EMPTY", "CHS", "请输入您的验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_EMPTY", "CHT", "請輸入您的驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_EMPTY", "EN", "Please enter your user authentication"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_FAIL", "CHS", "认证用户失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_FAIL", "CHT", "認證用戶失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_FAIL", "EN", "Failed to authenticate the user, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_INVALID", "CHS", "密码输入错误."); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_INVALID", "CHT", "密碼輸入錯誤."); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_AUTHENTICATION_INVALID", "EN", "Invalid login credentials."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_REACTIVATE_FAIL", "CHS", "无法重新激活用户验证码，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_REACTIVATE_FAIL", "CHT", "無法重新激活用戶驗證碼，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_REACTIVATE_FAIL", "EN", "Failed to reactivate user activation code, please contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_RESEND_FAIL", "CHS", "发送用户验证码失败，请稍后重试或联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_RESEND_FAIL", "CHT", "發送用戶驗證碼失敗，請稍後重試或聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_RESEND_FAIL", "EN", "Failed to send user activation code, please try again later or contact admin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_VALIDATE_FAIL", "CHS", "认证用户验证码失败，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_VALIDATE_FAIL", "CHT", "認證用戶驗證碼失敗，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CODE_VALIDATE_FAIL", "EN", "Failed to validate the activation code. Please contact the administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CORRUPT_DATA", "CHS", "用户信息受损，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CORRUPT_DATA", "CHT", "用户數據受損，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CORRUPT_DATA", "EN", "User information is damaged, please contact the administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CREATION_FAIL", "CHS", "创建用户账户失败，请重试。错误码："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CREATION_FAIL", "CHT", "创建用戶帳戶失敗，請重試。錯誤碼："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_CREATION_FAIL", "EN", "Failed to create new user! Please try again. Error code:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_EDITED_FAIL", "CHS", "編輯用户信息失败，请重试。错误码：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_EDITED_FAIL", "CHT", "修改用戶信息失敗，請重試。錯誤碼：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_EDITED_FAIL", "EN", "Failed to update user information, please try again. Error code: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ERROR", "CHS", "用户错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ERROR", "CHT", "用戶錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_ERROR", "EN", "User error."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_IL_FAIL", "CHS", "更新用户库存地点失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_IL_FAIL", "CHT", "更新用戶庫存地點失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_IL_FAIL", "EN", "Failed to update user\'s inventory location, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LINK_UNKOWN", "CHS", "链接不正确"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LINK_UNKOWN", "CHT", "鏈接不正確"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LINK_UNKOWN", "EN", "The link is not valid."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LIST", "CHS", "读取用户列表失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LIST", "CHT", "讀取用戶列表失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_LIST", "EN", "Failed to load user list, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_NOT_FOUND", "CHS", "找不到这位用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_NOT_FOUND", "CHT", "找不到這位用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_NOT_FOUND", "EN", "User cannot be found"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REACTIVATION_FAIL", "CHS", "重新激活用户失败，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REACTIVATION_FAIL", "CHT", "重新激活用戶失敗，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REACTIVATION_FAIL", "EN", "Failed to reactivate user, please contact administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REQUIRED_FIELD_MISSING", "CHS", "很抱歉，您的访问出错了，请稍后再试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REQUIRED_FIELD_MISSING", "CHT", "很抱歉，您的訪問出錯了，請稍後再試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_REQUIRED_FIELD_MISSING", "EN", "Sorry, something is not quite right, please try again later"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_SAVE_FAIL", "CHS", "更新用户信息失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_SAVE_FAIL", "CHT", "更新用戶信息失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_SAVE_FAIL", "EN", "Failed to update the user information, please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_FAIL", "CHS", "用户状态不正确。请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_FAIL", "CHT", "用戶狀態不正確。請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_FAIL", "EN", "User status error. Please contact the administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INACTIVE", "CHS", "你的账户已被暂停使用, 请联系管理员已激活账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INACTIVE", "CHT", "您的帳戶已被暫停使用，請聯繫管理員已激活帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INACTIVE", "EN", "Your account status is inactive. Please contact admin to activate your account"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INVALID", "CHS", "您的帐户无效。请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INVALID", "CHT", "您的帳戶無效。請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_INVALID", "EN", "Your account is invalid. Please contact administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_PENDING", "CHS", "你的账户正在等待处理, 请查阅邮件内的激活链接"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_PENDING", "CHT", "您的帳戶正在等待處理，請查閱郵件内的激活鏈接"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_PENDING", "EN", "Your account status is pending. Please check your email for activation link"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_UNKOWN", "CHS", "未知用户状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_UNKOWN", "CHT", "未知用戶狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_STATUS_UNKOWN", "EN", "User status unknown."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_UNAUTHORIZED", "CHS", "用户权限错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_UNAUTHORIZED", "CHT", "用戶權限錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_USER_UNAUTHORIZED", "EN", "The user is not authorized"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_VALUE_ABSENT", "CHS", "没有找到相应的值"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_VALUE_ABSENT", "CHT", "沒有找到相應的值"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_VALUE_ABSENT", "EN", "Value cannot be found in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_NIL", "CHS", "商品重量是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_NIL", "CHT", "商品重量是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_NIL", "EN", "Product Weight is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_POSITIVE", "CHS", "商品重量必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_POSITIVE", "CHT", "商品重量必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WEIGHT_POSITIVE", "EN", "Product Weight is greater than zero"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_NIL", "CHS", "商品宽度是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_NIL", "CHT", "商品寬度是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_NIL", "EN", "Product Width is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_POSITIVE", "CHS", "商品宽度必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_POSITIVE", "CHT", "商品寬度必須大於0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_WIDTH_POSITIVE", "EN", "Product Width is greater than zero"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_INVALID", "CHS", "商品推出年份是一个4位数字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_INVALID", "CHT", "商品推出年份是一個4位數字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_INVALID", "EN", "Product Launch Year is a 4 digit number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_NIL", "CHS", "商品推出年份是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_NIL", "CHT", "商品推出年份是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_YEAR_LAUNCH_NIL", "EN", "Product Launch Year is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_CREATED", "CHS", "商户新增成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_CREATED", "CHT", "商戶新增成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_CREATED", "EN", "Created merchant successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_EDITED", "CHS", "商户编辑成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_EDITED", "CHT", "商戶編輯成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MERCHANT_EDITED", "EN", "Edited merchant successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_CHANGE_EMAIL_LINK", "CHS", "重新发送用户登陆链接邮件，用户{0}可以点击邮件中链接使用新的邮箱进行登陆"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_CHANGE_EMAIL_LINK", "CHT", "重新發送用戶登陸鏈接郵件，用戶{0}可以點擊郵件中鏈接使用新的郵箱進行登陸"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_CHANGE_EMAIL_LINK", "EN", "You are going to resend an email with the unique link to user {0} for login. User can use that link to login with the new email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_REG_LINK", "CHS", "重新发送完成注册邮件，用户{0}可以点击邮件中链接完成注册"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_REG_LINK", "CHT", "重新發送完成註冊郵件，用戶{0}可以點擊郵件中鏈接完成註冊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESEND_REG_LINK", "EN", "You are going to resend an email with a verification link. User {0} can use that link for registration"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL", "CHS", "请为{0}输入新的电邮地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL", "CHT", "請為{0}輸入新的電郵地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL", "EN", "Please enter new email address for {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL_LINK", "CHS", "用户将收到验证电子邮件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL_LINK", "CHT", "用戶將收到驗證電子郵件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_EMAIL_LINK", "EN", "An email with verification link will be sent to the user"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_MOBILE", "CHS", "请为用户{0}输入新的手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_MOBILE", "CHT", "請為用戶{0}輸入新的手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_MOBILE", "EN", "Please fill in the new Mobile No. for {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_PASSWORD", "CHS", "您将为用户{0}重设密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_PASSWORD", "CHT", "您將為用戶{0}重設密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_PASSWORD", "EN", "You are going to reset the password for {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_CHANGE_SUCCESS", "CHS", "手机号码更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_CHANGE_SUCCESS", "CHT", "手機號碼更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_CHANGE_SUCCESS", "EN", "Updated mobile no. successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_SENT_SMS", "CHS", "验证码已发送到用户新的手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_SENT_SMS", "CHT", "驗證碼已發送到用戶新的手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_SENT_SMS", "EN", "A SMS is sent to the user\'s new mobile no."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PASSIVE_LOGOUT", "CHS", "您的帐户已被登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PASSIVE_LOGOUT", "CHT", "您的帳戶已被登出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PASSIVE_LOGOUT", "EN", "Your account has been logged out"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_RESET_PASSWORD_EMAIL_SENT", "CHS", "重置密码邮件已发送到您的邮箱，请通过邮件中的链接重设密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_RESET_PASSWORD_EMAIL_SENT", "CHT", "重置密碼郵件已發送到您的郵箱，請通過邮件中的鏈接重設密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_RESET_PASSWORD_EMAIL_SENT", "EN", "An email has been sent to your inbox. Please check your inbox and reset your password by the attached link"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_CREATE", "CHS", "品牌创建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_CREATE", "CHT", "品牌創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_CREATE", "EN", "Created brand successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_EDIT", "CHS", "品牌编辑成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_EDIT", "CHT", "品牌編輯成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_BRAND_EDIT", "EN", "Updated brand successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_DISP_NAME_CHANGE", "CHS", "显示名更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_DISP_NAME_CHANGE", "CHT", "顯示名更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_DISP_NAME_CHANGE", "EN", "Updated display name successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_EMAIL_CHANGE", "CHS", "邮箱地址更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_EMAIL_CHANGE", "CHT", "郵箱地址更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_EMAIL_CHANGE", "EN", "Updated email successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_IMPORT_INVENTORY", "CHS", "成功上传库存纪录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_IMPORT_INVENTORY", "CHT", "成功上傳庫存紀錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_IMPORT_INVENTORY", "EN", "Imported inventory successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_EXPORT", "CHS", "库存纪录下载成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_EXPORT", "CHT", "庫存紀錄下載成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_EXPORT", "EN", "Exported inventory successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PASSWORD_CHANGE", "CHS", "密码更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PASSWORD_CHANGE", "CHT", "密碼更新成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PASSWORD_CHANGE", "EN", "Updated password successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_ACTIVATE", "CHS", "商品激活成功：新发布 {0} 个商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_ACTIVATE", "CHT", "商品激活成功：新發佈 {0} 個商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_ACTIVATE", "EN", "Activated product successfully: {0} new product(s) activated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_EXPORT", "CHS", "商品下载成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_EXPORT", "CHT", "商品下載成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_EXPORT", "EN", "Exported product successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_IMPORT", "CHS", "商品导入成功
新增{0}件商品；更新{1}件商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_IMPORT", "CHT", "商品導入成功
新增{0}件商品；更新{1}件商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_IMPORT", "EN", "Imported product successfully
{0} new product(s) created;  {1} existing product(s) updated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_PUBLISH", "CHS", "商品发布成功：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_PUBLISH", "CHT", "商品發佈成功：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PRODUCT_PUBLISH", "EN", "Published product successfully: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_RESEND_ACTIVATION_CODE", "CHS", "重新发送验证码成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_RESEND_ACTIVATION_CODE", "CHT", "重新發送驗證碼成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_RESEND_ACTIVATION_CODE", "EN", "Resend activation code successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_CREATED", "CHS", "用户创建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_CREATED", "CHT", "用戶創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_CREATED", "EN", "Created user successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_EDITED", "CHS", "用戶信息编辑成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_EDITED", "CHT", "用戶信息編輯成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_USER_EDITED", "EN", "Updated user successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_IGNORE", "CHS", "忽略"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_IGNORE", "CHT", "忽略"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_IGNORE", "EN", "Ignore"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_OVERWRITE", "CHS", "取代"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_OVERWRITE", "CHT", "取代"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_IMG_OVERWRITE", "EN", "Overwrite"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CREATE_BRAND_FAIL", "CHS", "未能创建新品牌，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CREATE_BRAND_FAIL", "CHT", "未能創建新品牌，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CREATE_BRAND_FAIL", "EN", "Failed to create brand, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EDIT_BRAND_FAIL", "CHS", "未能编辑品牌，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EDIT_BRAND_FAIL", "CHT", "未能編輯品牌，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EDIT_BRAND_FAIL", "EN", "Failed to edit brand, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_EDIT_BRAND", "CHS", "编辑品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_EDIT_BRAND", "CHT", "編輯品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_EDIT_BRAND", "EN", "Edit Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_FILE", "CHS", "错误记录文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_FILE", "CHT", "錯誤記錄文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ERROR_FILE", "EN", "Error File"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DOMAIN_MM", "CHS", ".maymay.com"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DOMAIN_MM", "CHT", ".maymay.com"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DOMAIN_MM", "EN", ".maymay.com"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_UNLIMITED", "CHS", "无限量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_UNLIMITED", "CHT", "無限量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_UNLIMITED", "EN", "Unlimited"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_SKU_ATS", "CHS", "SKU可出售总量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_SKU_ATS", "CHT", "SKU可出售總量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_SKU_ATS", "EN", "SKU ATS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_TEMP_FIELDS", "CHS", "每个SKU只可以选填其中一项：存货配额，无限量配额或删除SKU库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_TEMP_FIELDS", "CHT", "每個SKU只可以選填其中一項：存貨配額，無限量配額或刪除SKU庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_TEMP_FIELDS", "EN", "Please only fill in 1 of the following 3 items for each SKU: no. of  allocation, unlimited stock or delete the SKU inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_DEL_FORMAT", "CHS", "如果此库存需要被删除，请填写「Y」"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_DEL_FORMAT", "CHT", "如果此庫存需要被刪除，請填寫「Y」"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_DEL_FORMAT", "EN", "Please fill in \"Y\" if this inventory needs to be delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDERNO", "CHS", "订单号 {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDERNO", "CHT", "訂單號 {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDERNO", "EN", "Order no. {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE", "CHS", "创建商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE", "CHT", "創建商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE", "EN", "Create Product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_INVENTORY", "CHS", "库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_INVENTORY", "CHT", "庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SKU_INVENTORY", "EN", "Inventory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD", "CHS", "添加"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD", "CHT", "添加"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ADD", "EN", "Add"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TRENDING_SEARCH_TERM_NIL", "CHS", "请输入热门搜寻关键字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TRENDING_SEARCH_TERM_NIL", "CHT", "請輸入熱門搜尋關鍵字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_TRENDING_SEARCH_TERM_NIL", "EN", "Please enter a Trending Search Term"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_MANAGEMENT", "CHS", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_MANAGEMENT", "CHT", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BRAND_MANAGEMENT", "EN", "Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TRENDING_SEARCH_TERMS", "CHS", "热门搜寻关键字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TRENDING_SEARCH_TERMS", "CHT", "熱門搜尋關鍵字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TRENDING_SEARCH_TERMS", "EN", "Trending Search Terms"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH", "CHS", "搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH", "CHT", "搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH", "EN", "Search"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_COLORS_SIZE_TITLE", "CHS", "请选择颜色和尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_COLORS_SIZE_TITLE", "CHT", "請選擇顏色和尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_COLORS_SIZE_TITLE", "EN", "Select Color and Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ADD_SEARCH_TERM", "CHS", "添加热门搜寻关键字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ADD_SEARCH_TERM", "CHT", "添加熱門搜尋關鍵字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ADD_SEARCH_TERM", "EN", "Add Trending Search Term"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LOCALE", "CHS", "地区语言"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LOCALE", "CHT", "地區語言"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LOCALE", "EN", "Locale"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_COLORS_SIZE_NOTE", "CHS", "请选择产品的颜色和尺码，你可以重复使用颜色尺码选择框来将同一个系统颜色与多个产品颜色进行匹配"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_COLORS_SIZE_NOTE", "CHT", "請選擇產品的顏色和尺碼，你可以重複使用顏色尺碼選擇框來將同一個系統顏色與多個產品顏色進行匹配"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_COLORS_SIZE_NOTE", "EN", "Please select the color and size of the product. You can reuse the color and size wizard  to map more than one color to the same color code."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_COLOR_CODE", "CHS", "系统颜色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_COLOR_CODE", "CHT", "系統顏色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_COLOR_CODE", "EN", "Color Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_SIZE_SELECTOR", "CHS", "请选择尺码类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_SIZE_SELECTOR", "CHT", "請選擇尺碼類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_SIZE_SELECTOR", "EN", "Select Size Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_BRAND", "CHS", "选择品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_BRAND", "CHT", "選擇品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_BRAND", "EN", "Select a Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_COO", "CHS", "选择原产地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_COO", "CHT", "選擇原產地"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_COO", "EN", "Select a Country of Origin"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ENGLISH", "CHS", "英文"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ENGLISH", "CHT", "英文"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ENGLISH", "EN", "English"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SIMPLIFIED_CHINESE", "CHS", "简体中文"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SIMPLIFIED_CHINESE", "CHT", "簡體中文"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SIMPLIFIED_CHINESE", "EN", "Simplified Chinese"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TRADITIONAL_CHINESE", "CHS", "繁体中文"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TRADITIONAL_CHINESE", "CHT", "繁體中文"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TRADITIONAL_CHINESE", "EN", "Traditional Chinese"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_YEAR", "CHS", "选择推出年份"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_YEAR", "CHT", "選擇推出年份"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_YEAR", "EN", "Select a Launch Year"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TRENDING_SEARCH_TERM", "CHS", "热门搜寻关键字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TRENDING_SEARCH_TERM", "CHT", "熱門搜尋關鍵字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TRENDING_SEARCH_TERM", "EN", "Trending Search Term"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_SEASON", "CHS", "选择推出季节"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_SEASON", "CHT", "選擇推出季節"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_SEASON", "EN", "Select a Launch Season"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTION", "CHS", "操作"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTION", "CHT", "操作"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTION", "EN", "Action"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GENERATE", "CHS", "生成"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GENERATE", "CHT", "生成"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GENERATE", "EN", "Generate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RED", "CHS", "红色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RED", "CHT", "紅色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RED", "EN", "Reds"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_EDIT", "CHS", "修改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_EDIT", "CHT", "修改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_EDIT", "EN", "Edit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DELETE", "CHS", "删除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DELETE", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DELETE", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_TRENDING_SRCH_TERM", "CHS", "您确定要删除这个热门搜寻关键字吗？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_TRENDING_SRCH_TERM", "CHT", "您確定要刪除這個熱門搜尋關鍵字嗎？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_TRENDING_SRCH_TERM", "EN", "Are you sure you want to delete this trending search term?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SYNONYMS", "CHS", "同义词"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SYNONYMS", "CHT", "同義詞"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SYNONYMS", "EN", "Synonyms"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CAT_TYPE", "CHS", "类别类型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CAT_TYPE", "CHT", "類別類型"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CAT_TYPE", "EN", "Category Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECTED_CAT", "CHS", "选定类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECTED_CAT", "CHT", "選定類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECTED_CAT", "EN", "Selected Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANG_ENG", "CHS", "英"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANG_ENG", "CHT", "英"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANG_ENG", "EN", "EN"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANG_HANS", "CHS", "简"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANG_HANS", "CHT", "簡"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANG_HANS", "EN", "CHS"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANG_HANT", "CHS", "繁"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANG_HANT", "CHT", "繁"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LANG_HANT", "EN", "CHT"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DEL_SKU_NOTE", "CHS", "如果删除SKU，该SKU的所有库存也将被删除。确认删除此SKU？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DEL_SKU_NOTE", "CHT", "如果刪除SKU，該SKU的所有庫存也將被刪除。確認刪除此SKU？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DEL_SKU_NOTE", "EN", "If this SKU is deleted, all inventory underneath will also be deleted. Confirm to delete this SKU?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_ADD_SKU", "CHS", "创建SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_ADD_SKU", "CHT", "創建SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_ADD_SKU", "EN", "Create SKU"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_EDIT", "CHS", "编辑商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_EDIT", "CHT", "編輯商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_EDIT", "EN", "Edit Product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DELETE_NOTE", "CHS", "如果删除商品，该商品的SKU与所有库存也将被删除。确认删除此商品？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DELETE_NOTE", "CHT", "如果刪除商品，該商品的SKU與所有庫存也將被刪除。確認刪除此商品？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DELETE_NOTE", "EN", "If this product is deleted, all SKU and inventory underneath will also be deleted. Confirm to delete this product?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_PRIM_CAT", "CHS", "选择基本类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_PRIM_CAT", "CHT", "選擇基本類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_PRIM_CAT", "EN", "Select Primary Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_MER_CAT", "CHS", "选择专题类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_MER_CAT", "CHT", "選擇專題類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_MER_CAT", "EN", "Select Merchandize Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_SYNONYM", "CHS", "搜索同义词"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_SYNONYM", "CHT", "搜索同義詞"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_SYNONYM", "EN", "Search for Synonyms"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEARCH_SYNONYM_NIL", "CHS", "请输入同义词"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEARCH_SYNONYM_NIL", "CHT", "請輸入同義詞"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEARCH_SYNONYM_NIL", "EN", "Please enter a Synonym"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SYNONYM_GROUP", "CHS", "同义词组"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SYNONYM_GROUP", "CHT", "同義詞組"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SYNONYM_GROUP", "EN", "Synonym Group"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_SYNONYM_GROUP", "CHS", "您确定要删除这个同义词组吗？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_SYNONYM_GROUP", "CHT", "您確定要刪除這個同義詞組嗎？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_SYNONYM_GROUP", "EN", "Are you sure you want to delete this synonym group?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_CUSTOM_AUTO_COMP", "CHS", "搜索自定义自动填写字串"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_CUSTOM_AUTO_COMP", "CHT", "搜索自定義自動填寫字串"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_CUSTOM_AUTO_COMP", "EN", "Search for Custom Auto Complete String"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CUSTOM_AUTO_COMP_NIL", "CHS", "请输入自定义自动填写字串"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CUSTOM_AUTO_COMP_NIL", "CHT", "請輸入自定義自動填寫字串"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CUSTOM_AUTO_COMP_NIL", "EN", "Please enter a custom auto complete string."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CUSTOM_AUTO_COMPLETION", "CHS", "自定义自动填写"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CUSTOM_AUTO_COMPLETION", "CHT", "自定義自動填寫"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CUSTOM_AUTO_COMPLETION", "EN", "Custom Auto Completion"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CUSTOM_AUTO_COMPLETION_STR", "CHS", "自定义自动填写字串"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CUSTOM_AUTO_COMPLETION_STR", "CHT", "自定義自動填寫字串"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CUSTOM_AUTO_COMPLETION_STR", "EN", "Custom Auto Complete String"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_CUSTOM_AUTO_COMPLETE_STR", "CHS", "您确定要删除这个自定义自动填写字串吗？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_CUSTOM_AUTO_COMPLETE_STR", "CHT", "您確定要刪除這個自定義自動填寫字串嗎？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_CUSTOM_AUTO_COMPLETE_STR", "EN", "Are you sure you want to delete this custom auto complete string?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PINYIN_AUTO_COMP_NIL", "CHS", "请输入拼音自动填写字串"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PINYIN_AUTO_COMP_NIL", "CHT", "請輸入拼音自動填寫字串"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PINYIN_AUTO_COMP_NIL", "EN", "Please enter a pinyin auto complete string"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PINYIN_INPUT", "CHS", "拼音输入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PINYIN_INPUT", "CHT", "拼音輸入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PINYIN_INPUT", "EN", "Pinyin Input"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PINYIN_OUTPUT", "CHS", "拼音输出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PINYIN_OUTPUT", "CHT", "拼音輸出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PINYIN_OUTPUT", "EN", "Pinyin Output"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PINYIN_INPUT_NIL", "CHS", "请提供拼音输入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PINYIN_INPUT_NIL", "CHT", "請提供拼音輸入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PINYIN_INPUT_NIL", "EN", "Please enter a pinyin input"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PINYIN_OUTPUT_NIL", "CHS", "请提供拼音输出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PINYIN_OUTPUT_NIL", "CHT", "請提供拼音輸出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PINYIN_OUTPUT_NIL", "EN", "Please enter a pinyin output"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PROD_CREATE", "CHS", "创建商品成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PROD_CREATE", "CHT", "創建商品成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PROD_CREATE", "EN", "Created product successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PROD_EDIT", "CHS", "编辑商品成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PROD_EDIT", "CHT", "編輯商品成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_PROD_EDIT", "EN", "Updated product successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_PINYIN_AUTO_COMPLETE_STR", "CHS", "您确定要删除这对拼音自动填写字串吗？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_PINYIN_AUTO_COMPLETE_STR", "CHT", "您確定要刪除這對拼音自動填寫字串嗎？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CONF_DELETE_PINYIN_AUTO_COMPLETE_STR", "EN", "Are you sure you want to delete this pinyin auto complete string pair?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_INDEXES", "CHS", "搜索索引"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_INDEXES", "CHT", "搜索索引"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_INDEXES", "EN", "Search Indexes"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PINYIN_AUTO_COMPLETION", "CHS", "拼音自动填写"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PINYIN_AUTO_COMPLETION", "CHT", "拼音自動填寫"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_PINYIN_AUTO_COMPLETION", "EN", "Pinyin Auto Completion"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INDEX_TYPE", "CHS", "索引类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INDEX_TYPE", "CHT", "索引類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INDEX_TYPE", "EN", "Index Type"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_STATUS", "CHS", "状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_STATUS", "CHT", "狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_STATUS", "EN", "Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DOCUMENTS", "CHS", "文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DOCUMENTS", "CHT", "文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DOCUMENTS", "EN", "Documents"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INDEX_SIZE", "CHS", "索引大小"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INDEX_SIZE", "CHT", "索引大小"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INDEX_SIZE", "EN", "Index Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LAST_UPDATE", "CHS", "最后更新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LAST_UPDATE", "CHT", "最後更新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_LAST_UPDATE", "EN", "Last Update"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SCHEDULED_REBUILD", "CHS", "定期重建"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SCHEDULED_REBUILD", "CHT", "定期重建"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SCHEDULED_REBUILD", "EN", "Scheduled Rebuild"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INCREMENTAL_UPDATE", "CHS", "增量更新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INCREMENTAL_UPDATE", "CHT", "增量更新"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INCREMENTAL_UPDATE", "EN", "Incremental Update"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ENABLED", "CHS", "启用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ENABLED", "CHT", "啟用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ENABLED", "EN", "Enabled"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DISABLED", "CHS", "禁用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DISABLED", "CHT", "禁用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DISABLED", "EN", "Disabled"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ONLINE", "CHS", "线上"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ONLINE", "CHT", "線上"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ONLINE", "EN", "Online"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INACTIVE_CATEGORY_CONFIRM", "CHS", "确认停用这个类别？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INACTIVE_CATEGORY_CONFIRM", "CHT", "確認停用這個類別？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_INACTIVE_CATEGORY_CONFIRM", "EN", "Confirm to inactivate this category?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_OFFLINE", "CHS", "离线"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_OFFLINE", "CHT", "離線"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_OFFLINE", "EN", "Offline"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTIVE_CATEGORY_CONFIRM", "CHS", "确认激活这个类别？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTIVE_CATEGORY_CONFIRM", "CHT", "確認激活這個類別？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ACTIVE_CATEGORY_CONFIRM", "EN", "Confirm to activate this category?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_DELETE_CATEGORY_SUCCESS", "CHS", "类别删除成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_DELETE_CATEGORY_SUCCESS", "CHT", "類別刪除成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_DELETE_CATEGORY_SUCCESS", "EN", "Deleted category successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_DELETE_CATEGORY_FAIL", "CHS", "删除类别失败，类别下否存在商品。请在删除类别之前清除它的所有商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_DELETE_CATEGORY_FAIL", "CHT", "刪除類別失敗，類別下否存在商品。請在刪除類別之前清除它的所有商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_DELETE_CATEGORY_FAIL", "EN", "Failed to delete this category as it contains product(s). Please remove all its product(s) before deleting a category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_ACTIVATE_DRAFT_CATEGORY", "CHS", "激活待处理类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_ACTIVATE_DRAFT_CATEGORY", "CHT", "激活待處理類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_ACTIVATE_DRAFT_CATEGORY", "EN", "Activate Pending Categories"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_CREATE_CATEGORY", "CHS", "创建类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_CREATE_CATEGORY", "CHT", "創建類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_AC_CREATE_CATEGORY", "EN", "Create Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_ACTIVATE_CATEGORY_SUCCESS", "CHS", "激活待处理类别成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_ACTIVATE_CATEGORY_SUCCESS", "CHT", "激活待處理類別成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_ACTIVATE_CATEGORY_SUCCESS", "EN", "Activated pending categories successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_ACTIVATE_CATEGORY_FAIL", "CHS", "激活待处理类别失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_ACTIVATE_CATEGORY_FAIL", "CHT", "激活待處理類別失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_ACTIVATE_CATEGORY_FAIL", "EN", "Failed to activate pending categories"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CATEGORY_SEARCH", "CHS", "搜索类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CATEGORY_SEARCH", "CHT", "搜索類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CATEGORY_SEARCH", "EN", "Search for Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY_NAME", "CHS", "类别名称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY_NAME", "CHT", "類別名稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY_NAME", "EN", "Category Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_PRODUCTS", "CHS", "产品数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_PRODUCTS", "CHT", "產品數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_NUM_PRODUCTS", "EN", "No. of products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_DRAFT_CATEGORY", "CHS", "激活待处理类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_DRAFT_CATEGORY", "CHT", "激活待處理類别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_DRAFT_CATEGORY", "EN", "Activate Pending Categories"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_CATEGORY_MESSAGE", "CHS", "所有待处理的类别会被激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_CATEGORY_MESSAGE", "CHT", "所有待處理的類別會被激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_CATEGORY_MESSAGE", "EN", "All pending categories will be activated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CHANGE_BRAND_STATUS", "CHS", "改变品牌状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CHANGE_BRAND_STATUS", "CHT", "改變品牌狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CHANGE_BRAND_STATUS", "EN", "Change Brand Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_BRAND_ACTIVATE", "CHS", "品牌「{0}」将被激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_BRAND_ACTIVATE", "CHT", "品牌「{0}」將被激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_BRAND_ACTIVATE", "EN", "Brand \"{0}\" will be activated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_BRAND_INACTIVATE", "CHS", "品牌「{0}」将被停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_BRAND_INACTIVATE", "CHT", "品牌「{0}」將被停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_BRAND_INACTIVATE", "EN", "Brand \"{0}\" will be inactivated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_NAME_CHS", "CHS", "商品名称	(简体中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_NAME_CHS", "CHT", "商品名稱 (簡體中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_NAME_CHS", "EN", "Product Name (Simplified Chinese)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_NAME_CHT", "CHS", "商品名称	(繁体中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_NAME_CHT", "CHT", "商品名稱 (繁體中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_NAME_CHT", "EN", "Product Name (Traditional Chinese)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_NAME_EN", "CHS", "商品名称	(英文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_NAME_EN", "CHT", "商品名稱 (英文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_NAME_EN", "EN", "Product Name (English)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TO", "CHS", "至"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TO", "CHT", "至"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TO", "EN", "To"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MANU_HANS", "CHS", "生产厂家 (简体中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MANU_HANS", "CHT", "生產廠家 (簡體中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MANU_HANS", "EN", "Manufacturer Name (Simplified Chinese)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MANU_HANT", "CHS", "生产厂家 (繁体中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MANU_HANT", "CHT", "生產廠家 (繁體中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MANU_HANT", "EN", "Manufacturer Name (Traditional Chinese)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MANU_EN", "CHS", "生产厂家 (英文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MANU_EN", "CHT", "生產廠家 (英文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MANU_EN", "EN", "Manufacturer Name (English)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_BADGE", "CHS", "标签"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_BADGE", "CHT", "標籤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_BADGE", "EN", "Badge"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_BADGE", "CHS", "选择标签代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_BADGE", "CHT", "選擇標籤代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_SELECT_BADGE", "EN", "Select a Badge"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DIMENSION", "CHS", "尺寸"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DIMENSION", "CHT", "尺寸"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DIMENSION", "EN", "Dimension"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT", "CHS", "选择"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT", "CHT", "選擇"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECT", "EN", "Select"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE_ANOTHER", "CHS", "建立下一个"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE_ANOTHER", "CHT", "創建下一個"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE_ANOTHER", "EN", "Create Another"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_PUBLISH", "CHS", "创建和发布"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_PUBLISH", "CHT", "創建和發佈"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_PUBLISH", "EN", "Create and Publish"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE_DRAFT", "CHS", "创建为草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE_DRAFT", "CHT", "創建為草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE_DRAFT", "EN", "Save as Draft"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DESC_HANS", "CHS", "介绍	(简体中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DESC_HANS", "CHT", "介紹 (簡體中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DESC_HANS", "EN", "Description (Simplified Chinese)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DESC_HANT", "CHS", "介绍	(繁体中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DESC_HANT", "CHT", "介紹 (繁體中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DESC_HANT", "EN", "Description (Traditional Chinese)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DESC_EN", "CHS", "介绍	(英文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DESC_EN", "CHT", "介紹	(英文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_DESC_EN", "EN", "Description (English)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_FEATURE_HANS", "CHS", "特征	(简体中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_FEATURE_HANS", "CHT", "特徵 (簡體中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_FEATURE_HANS", "EN", "Feature (Simplified Chinese)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_FEATURE_HANT", "CHS", "特征 (繁体中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_FEATURE_HANT", "CHT", "特徵 (繁體中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_FEATURE_HANT", "EN", "Feature (Traditional Chinese)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_FEATURE_EN", "CHS", "特征 (英文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_FEATURE_EN", "CHT", "特徵 (英文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_FEATURE_EN", "EN", "Feature (English)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MAT_HANS", "CHS", "材质	 (简体中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MAT_HANS", "CHT", "材質 (簡體中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MAT_HANS", "EN", "Material (Simplified Chinese)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MAT_EN", "CHS", "材质 (英文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MAT_EN", "CHT", "材質 (英文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MAT_EN", "EN", "Material (English)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MAT_HANT", "CHS", "材质	(繁体中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MAT_HANT", "CHT", "材質 (繁體中文)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_MAT_HANT", "EN", "Material (Traditional Chinese)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CATEGORY", "CHS", "类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CATEGORY", "CHT", "類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CATEGORY", "EN", "Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_CREATE_CATEGORY_SUCCESS", "CHS", "类别创建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_CREATE_CATEGORY_SUCCESS", "CHT", "類別創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_CREATE_CATEGORY_SUCCESS", "EN", "Created category successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DELETE_CATEGORY_CONFIRM", "CHS", "确认删除这个类别？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DELETE_CATEGORY_CONFIRM", "CHT", "確認刪除這個類別？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DELETE_CATEGORY_CONFIRM", "EN", "Confirm to delete this category?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_IMG_GUIDE", "CHS", "如何准备图片：请参阅指引里的文件格式和说明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_IMG_GUIDE", "CHT", "如何準備圖片：請參閱指引裡的文件格式和說明"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_IMG_GUIDE", "EN", "How to prepare images: Please refer to the guideline for the file format and instructions"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_PERPETUAL", "CHS", "永久"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_PERPETUAL", "CHT", "永久"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_PERPETUAL", "EN", "Perpetual"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_COLOR_NAME_CHS", "CHS", "颜色名称(CHS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_COLOR_NAME_CHS", "CHT", "顏色名稱(CHS)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_COLOR_NAME_CHS", "EN", "Color Name(CHS)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_COLOR_NAME_CHT", "CHS", "颜色名称(CHT)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_COLOR_NAME_CHT", "CHT", "顏色名稱(CHT)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_COLOR_NAME_CHT", "EN", "Color Name(CHT)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_COLOR_NAME_EN", "CHS", "颜色名称(EN)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_COLOR_NAME_EN", "CHT", "顏色名稱(EN)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_COLOR_NAME_EN", "EN", "Color Name(EN)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_COMP_DESC_FORMAT", "CHS", "公司简介必须在1000个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_COMP_DESC_FORMAT", "CHT", "公司簡介必須在1000個字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BRAND_COMP_DESC_FORMAT", "EN", "Company Description must not more than 1000 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_DUP", "CHS", "您选择了重复的专题类别，商品里的每个专题类别应该是独一无二的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_DUP", "CHT", "您選擇了重複的專題類別，商品裡的每個專題類別應該是獨一無二的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MERCHANDISING_CATEGORY_DUP", "EN", "The Merchandize Categories you selected are duplicated, please only select a Merchandize Category once for a product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COPIED", "CHS", "已经复制"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COPIED", "CHT", "已經複製"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COPIED", "EN", "Copied!"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRESS_TO_COPY", "CHS", "按{0}复制"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRESS_TO_COPY", "CHT", "按{0}複製"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRESS_TO_COPY", "EN", "Press {0} to copy"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_NEW_CATEGORY", "CHS", "创建类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_NEW_CATEGORY", "CHT", "創建類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_NEW_CATEGORY", "EN", "Create Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CATEGORY_CODE", "CHS", "类别编号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CATEGORY_CODE", "CHT", "類別編號"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CATEGORY_CODE", "EN", "Category Code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BANNER", "CHS", "广告"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BANNER", "CHT", "廣告"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BANNER", "EN", "Banner"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_SELECT", "CHS", "商户选择"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_SELECT", "CHT", "商戶選擇"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_SELECT", "EN", "Merchant Select"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY_DESC", "CHS", "类别介绍"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY_DESC", "CHT", "類別介紹"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY_DESC", "EN", "Category Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_META_KEYWORD", "CHS", "自定义关键字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_META_KEYWORD", "CHT", "自定義關鍵字"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_META_KEYWORD", "EN", "Meta Keyword"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_META_DESC", "CHS", "搜索字段描述"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_META_DESC", "CHT", "搜索字段描述"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_META_DESC", "EN", "Meta Description"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SAVE_DRAFT", "CHS", "保存草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SAVE_DRAFT", "CHT", "保存草稿"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SAVE_DRAFT", "EN", "Save Draft"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IS_INCLUDE_AUTOCOMPLETE", "CHS", "可搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IS_INCLUDE_AUTOCOMPLETE", "CHT", "可搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IS_INCLUDE_AUTOCOMPLETE", "EN", "Is Searchable in Search Bar"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_FOREVER", "CHS", "永久"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_FOREVER", "CHT", "永久"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("RBTN_FOREVER", "EN", "Forever"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_NO_INVENTORY_HISTORY", "CHS", "沒有庫存記錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_NO_INVENTORY_HISTORY", "CHT", "沒有庫存記錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_NO_INVENTORY_HISTORY", "EN", "No inventory history is available"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTION_BY", "CHS", "账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTION_BY", "CHT", "賬戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTION_BY", "EN", "Action By"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMMISSION_RATE", "CHS", "佣金比例"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMMISSION_RATE", "CHT", "佣金比例"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMMISSION_RATE", "EN", "Commission Rate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_PINYIN_AUTO_COMP", "CHS", "查询拼音"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_PINYIN_AUTO_COMP", "CHT", "查詢拼音"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SEARCH_PINYIN_AUTO_COMP", "EN", "Search for Pinyin Input"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SAVE", "CHS", "保存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SAVE", "CHT", "保存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_SAVE", "EN", "Save"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOW", "CHS", "关注"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOW", "CHT", "關注"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOW", "EN", "Follow"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWED", "CHS", "已关注"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWED", "CHT", "已關注"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWED", "EN", "Followed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_CATEGORY", "CHS", "编辑类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_CATEGORY", "CHT", "編輯類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_CATEGORY", "EN", "Edit Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ADD_SYNONYMS", "CHS", "添加热门搜寻关键字进同义词组"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ADD_SYNONYMS", "CHT", "添加熱門搜尋關鍵字進同義詞組"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ADD_SYNONYMS", "EN", "Add Synonyms in Synonym Group"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SYNONYMS_NIL", "CHS", "请输入同义词"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SYNONYMS_NIL", "CHT", "請輸入同義詞"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SYNONYMS_NIL", "EN", "Please enter a synonym"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROFILE_ABOUT", "CHS", "关於"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROFILE_ABOUT", "CHT", "關於"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROFILE_ABOUT", "EN", "About"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_SC_NIL", "CHS", "类别名称（简体中文）是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_SC_NIL", "CHT", "類別名稱（簡體中文）是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_SC_NIL", "EN", "Category Name (Simplified Chinese) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_CODE_FORMAT", "CHS", "类别代码必须在1 - 25个字符之间，应用全大写字母而且没有间隔"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_CODE_FORMAT", "CHT", "類別代碼必須在1 - 25個字符之間，應用全大寫字母而且沒有間隔"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_CODE_FORMAT", "EN", "Category Code must be between 1-25 character, all in uppercase and no spacing allowed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_CODE_NIL", "CHS", "基本类别是必须选择"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_CODE_NIL", "CHT", "基本類別是必需選擇"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_CODE_NIL", "EN", "Primary Category is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_CODE_DUPLICATED", "CHS", "此类别代码已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_CODE_DUPLICATED", "CHT", "此類別代碼已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_CODE_DUPLICATED", "EN", "This Category Code already exist in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ADD_CUSTOM_AUTO_COMP", "CHS", "添加自定义自动填写字串"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ADD_CUSTOM_AUTO_COMP", "CHT", "添加自定義自動填寫字串"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_ADD_CUSTOM_AUTO_COMP", "EN", "Add Custom Auto Complete String"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REINDEX", "CHS", "重建索引"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REINDEX", "CHT", "重建索引"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REINDEX", "EN", "Reindex"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_SEARCH_REINDEX_SUCCESS", "CHS", "索引重建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_SEARCH_REINDEX_SUCCESS", "CHT", "索引重建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_SEARCH_REINDEX_SUCCESS", "EN", "Reindexed search index successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEARCH_REINDEX_FAIL", "CHS", "索引重建失败。请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEARCH_REINDEX_FAIL", "CHT", "索引重建失敗。請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEARCH_REINDEX_FAIL", "EN", "Failed to reindex. Please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMMISSION", "CHS", "佣金率"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMMISSION", "CHT", "佣金率"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMMISSION", "EN", "Commission Rate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMM_RATE_POSITIVE", "CHS", "佣金必须是正数"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMM_RATE_POSITIVE", "CHT", "佣金必須是正數"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMM_RATE_POSITIVE", "EN", "Commission must be positive"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FEATURED_BRAND_MAX", "CHS", "精选品牌数上限 = 5"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FEATURED_BRAND_MAX", "CHT", "精選品牌數上限 = 5"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_FEATURED_BRAND_MAX", "EN", "Max. no. of featured brands = 5"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_CAT", "CHS", "全部分类"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_CAT", "CHT", "全部分類"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_CAT", "EN", "All Categories"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_SELECTABLE", "CHS", "商家可选择"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_SELECTABLE", "CHT", "商家可選擇"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_SELECTABLE", "EN", "Merchant selectable"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_DELETE_LABEL", "CHS", "删除搜索条件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_DELETE_LABEL", "CHT", "刪除搜索條件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_DELETE_LABEL", "EN", "Delete Search Term"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_SEARCH", "CHS", "确认删除搜索条件「{0}」。 这个动作无法复原"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_SEARCH", "CHT", "確認刪除搜索條件「{0}」。 這個動作無法復原"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_SEARCH", "EN", "Search term \"{0}\" will be deleted. This action cannot be undone"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_SC_FORMAT", "CHS", "类别名称（简体中文）必须在1 – 25个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_SC_FORMAT", "CHT", "類別名稱（簡體中文）必須在1 – 25個字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_SC_FORMAT", "EN", "Category Name (Simplified Chinese) must be between 1-25 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_LOGO_BANNER", "CHS", "文件大小上限: <CONST_LOGO_FILE_SIZE>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_LOGO_BANNER", "CHT", "文件大小上限: <CONST_LOGO_FILE_SIZE>"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_LOGO_BANNER", "EN", "Max. file size: <CONST_LOGO_FILE_SIZE>"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY_LOGO_BANNER_NOTE", "CHS", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY_LOGO_BANNER_NOTE", "CHT", "文件大小上限: {CONST_LOGO_FILE_SIZE}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CATEGORY_LOGO_BANNER_NOTE", "EN", "Max. file size: {CONST_LOGO_FILE_SIZE}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_SEARCH_TERM", "CHS", "文件大小上限{CONST_LOGO_FILE_SIZE}，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_SEARCH_TERM", "CHT", "文件大小上限{CONST_LOGO_FILE_SIZE}，JPG或PNG格式"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_SEARCH_TERM", "EN", "Max. file size {CONST_LOGO_FILE_SIZE}, JPG/PNG format"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_SEARCH_TERM", "CHS", "新建搜索条件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_SEARCH_TERM", "CHT", "新建搜索條件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_SEARCH_TERM", "EN", "New Search Term"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEARCH_TERM_NIL", "CHS", "请填写搜索条件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEARCH_TERM_NIL", "CHT", "請填寫搜索條件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SEARCH_TERM_NIL", "EN", "Please enter new Search Term"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD_FRIEND", "CHS", "成为朋友"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD_FRIEND", "CHT", "成為朋友"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD_FRIEND", "EN", "Add Friend"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BEFRIENDED", "CHS", "已成为朋友"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BEFRIENDED", "CHT", "已成為朋友"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BEFRIENDED", "EN", "Friends"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_CONFIRM_LABEL", "CHS", "确认更改搜索条件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_CONFIRM_LABEL", "CHT", "確認更改搜索條件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_CONFIRM_LABEL", "EN", "Confirm Changes for Search Term"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_UPDATE_SEARCH", "CHS", "搜索条件「{0}」 将被更改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_UPDATE_SEARCH", "CHT", "搜索條件「{0}」將被更改"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_UPDATE_SEARCH", "EN", "Search term \"{0}\" will be updated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWER", "CHS", "粉丝"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWER", "CHT", "粉絲"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWER", "EN", "Follower"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_CREATE_CATEGORY_FAIL", "CHS", "创建新类别失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_CREATE_CATEGORY_FAIL", "CHT", "創建新類別失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_CREATE_CATEGORY_FAIL", "EN", "Failed to create category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REPORT_POST", "CHS", "举报帖子"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REPORT_POST", "CHT", "舉報帖子"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REPORT_POST", "EN", "Report Post"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_HIDE_POST", "CHS", "隐藏帖子"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_HIDE_POST", "CHT", "隱藏帖子"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_HIDE_POST", "EN", "Hide Post"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_EDIT", "CHS", "库存编辑成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_EDIT", "CHT", "庫存編輯成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_EDIT", "EN", "Updated inventory successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMMISSION_RATE_NIL", "CHS", "佣金比例是必填字段"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMMISSION_RATE_NIL", "CHT", "佣金比例是必填字段"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_COMMISSION_RATE_NIL", "EN", "Commission Rate is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_CREATE", "CHS", "库存创建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_CREATE", "CHT", "庫存創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_CREATE", "EN", "Created inventory successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CATEGORY_CREATED", "CHS", "类别创建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CATEGORY_CREATED", "CHT", "類別創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CATEGORY_CREATED", "EN", "Created category successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CATEGORY_EDITED", "CHS", "类别编辑成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CATEGORY_EDITED", "CHT", "類別編輯成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CATEGORY_EDITED", "EN", "Edited category successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_BRAND", "CHS", "关注品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_BRAND", "CHT", "關注品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_BRAND", "EN", "Following Brand(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_CHANGE_STATUS_CATEGORY_FAIL", "CHS", "修改状态失败，类别下仍存在商品。请在删除类别之前清除它的所有商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_CHANGE_STATUS_CATEGORY_FAIL", "CHT", "修改狀態失敗，類別下仍存在商品。請在刪除類別之前清除它的所有商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_CHANGE_STATUS_CATEGORY_FAIL", "EN", "Failed to update the status of this category as it contains product(s). Please remove all its product(s) before deleting a category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONF_DEL_CATEGORY", "CHS", "删除类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONF_DEL_CATEGORY", "CHT", "刪除類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONF_DEL_CATEGORY", "EN", "Delete the category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL_FOLLOWING", "CHS", "总共关注"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL_FOLLOWING", "CHT", "總共關注"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL_FOLLOWING", "EN", "Following in Total"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH", "CHS", "搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH", "CHT", "搜索"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SEARCH", "EN", "Search"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORYCODE_UNIQUE", "CHS", "类别代码必须唯一"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORYCODE_UNIQUE", "CHT", "類別代碼必須唯一"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORYCODE_UNIQUE", "EN", "Category Code must be unique"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_OF_FOLLOWER", "CHS", "人已关注"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_OF_FOLLOWER", "CHT", "人已關注"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_OF_FOLLOWER", "EN", "follower(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_CURATOR", "CHS", "关注美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_CURATOR", "CHT", "關注美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_CURATOR", "EN", "Following Curator(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_USER", "CHS", "关注用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_USER", "CHT", "關注用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_USER", "EN", "Following User(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATOR", "CHS", "美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATOR", "CHT", "美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATOR", "EN", "Curator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_USER", "CHS", "用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_USER", "CHT", "用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_USER", "EN", "User"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL_FRIEND", "CHS", "总共朋友"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL_FRIEND", "CHT", "總共朋友"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL_FRIEND", "EN", "Friends in Total"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRIENDS_REQUEST", "CHS", "加朋友"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRIENDS_REQUEST", "CHT", "加朋友"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRIENDS_REQUEST", "EN", "Friend Requests"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONVERSATION", "CHS", "对话"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONVERSATION", "CHT", "對話"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONVERSATION", "EN", "Conversation"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL_CONVERSATION", "CHS", "总共对话"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL_CONVERSATION", "CHT", "總共對話"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TOTAL_CONVERSATION", "EN", "Total Conversation(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PUBLISH_DATE", "CHS", "开始日期必须早于结束日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PUBLISH_DATE", "CHT", "開始日期必須早於結束日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PUBLISH_DATE", "EN", "Start Date must be more early than End Date"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_USER_PROF_CREATE", "CHS", "商户用户可以管理品牌以下的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_USER_PROF_CREATE", "CHT", "商戶用戶可以管理品牌以下的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_USER_PROF_CREATE", "EN", "A merchant user can manage products of assigned brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_USER_PROF_EDIT", "CHS", "商户用户可以管理品牌以下的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_USER_PROF_EDIT", "CHT", "商戶用戶可以管理品牌以下的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_USER_PROF_EDIT", "EN", "A merchant user can manage products of assigned brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_INVENTORY_LOC_CREATE", "CHS", "库存位置记录了仓库和商店的资料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_INVENTORY_LOC_CREATE", "CHT", "庫存位置記錄了倉庫和商店的資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_INVENTORY_LOC_CREATE", "EN", "An inventory location will be a record of warehouses and stores"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_INVENTORY_LOC_EDIT", "CHS", "库存位置记录了仓库和商店的资料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_INVENTORY_LOC_EDIT", "CHT", "庫存位置記錄了倉庫和商店的資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_INVENTORY_LOC_EDIT", "EN", "An inventory location will be a record of warehouses and stores"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_CREATE", "CHS", "商品能创建为草稿或当所有必填项目都填写後可直接发布。创建新品牌时请填写表单的每个项目，请输入所有语言信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_CREATE", "CHT", "商品能創建為草稿或當所有必填項目都填寫後可直接發佈。創建新品牌時請填寫表單的每個項目，請輸入所有語言信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_CREATE", "EN", "A product can be saved as draft or published directly given all the mandatory items are fielded. Please fill in all fields to create new product, you can enter information in other languages if required"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_EDIT", "CHS", "商品的状态可由待处理更变为激活 (在店面发布) 或从使用中改为停用 (隐藏於店面)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_EDIT", "CHT", "商品的狀態可由待處理更變為激活 (在店面發佈) 或從使用中改為停用 (隱藏於店面)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_EDIT", "EN", "The status of a product can be changed from pending to activate (publish in storefront) or activate to inactivate (hidden from storefront)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_INFO", "CHS", "商品名称所有的语言栏都应填写，必须选择品牌名称和样式代码，因为它们将用作商品识别。零售价和销售价 (人民币) 代表建议原价和商品在MayMay上标示出的售价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_INFO", "CHT", "商品名稱所有的語言欄都應填寫，必須選擇品牌名稱和樣式代碼，因為它們將用作商品識別。零售價和銷售價 (人民幣) 代表建議原價和商品在MayMay上標示出的售價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_INFO", "EN", "Product name should be have all required languages filled in, Brand name and Style Code must be selected as they identify the product directly, so as Retail Price and Sales Price which represent the amount of Renminbi for which the company that produces a product recommends that it be sold in stores and the original price of the product sold on MayMay"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_CAT", "CHS", "商品的基本类别代表了商品的基本功能类别，所以每个商品只能有1个基本类别；专题类别则代表了商品的特徵，而每个商品可选最多5个专题类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_CAT", "CHT", "商品的基本類別代表了商品的基本功能類別，所以每個商品只能有1個基本類別；專題類別則代表了商品的特徵，而每個商品可選最多5個專題類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_CAT", "EN", "A Primary Category is the functional category of the product, a product can only have one primary category; a Merchandize Category is the feature category of a product, a product can have up to 5 merchandize categories"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_SHIP", "CHS", "包装後商品的重量，将用於计算运费"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_SHIP", "CHT", "包裝後商品的重量，將用於計算運費"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_SHIP", "EN", "The Weight of the product when packaged to ship is displayed and used to calculate shipping costs"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_SKU", "CHS", "每个商品的SKU是一个独特的数字，产品供应商会分配给零售商品作识别商品。一个商品可以有多个条形码。 商品必须设置SKU库存来分配存货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_SKU", "CHT", "每個商品的SKU是一個獨特的數字，產品供應商會分配給零售商品作識別商品。一個商品可以有多個條形碼。 商品必須設置SKU庫存來分配存貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_SKU", "EN", "Unique SKU is a number assigned to retail merchandise that identifies both the product and the vendor that sells the product. A product can have several Barcodes. SKU Inventory must be set to distribute stock allocation"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_IMG", "CHS", "每个商品必须至少上传1张精选图片为店面默认列表图像，或1张颜色图片为不同颜色的商品图像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_IMG", "CHT", "每個商品必須至少上傳1張精選圖片為店面默認列表圖像，或1張顏色圖片為不同顏色的商品圖像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_IMG", "EN", "A product must have at least 1 Featured Image as default listing image or Colour Image display when user picks an available color"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_PRIM_CAT_CREATE", "CHS", "商品的基本类别代表了商品的基本功能类别，所以每个商品只能有1个基本类别。请选择商品的基本类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_PRIM_CAT_CREATE", "CHT", "商品的基本類別代表了商品的基本功能類別，所以每個商品只能有1個基本類別。請選擇商品的基本類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_PRIM_CAT_CREATE", "EN", "A Primary Category is the functional category of the product, a product can only have one primary category. Please select a primary category for this product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_MERC_CAT_CREATE", "CHS", "专题类别则代表了商品的特征，而每个商品可选最多5个专题类别。请选择商品的专题类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_MERC_CAT_CREATE", "CHT", "專題類別則代表了商品的特徵，而每個商品可選最多5個專題類別。請選擇商品的專題類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_REM_PROD_MERC_CAT_CREATE", "EN", "A Merchandize Category is the feature category of a product, a product can have up to 5 merchandize categories. Please select a merchandize category for this product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE_SKU_NOTE", "CHS", "在每个SKU，每个系统颜色只能使用一次，您也可以选择不同尺码。每个尺码和颜色会变成商品的一个独立的SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE_SKU_NOTE", "CHT", "在每個SKU，每個系統顏色只能使用一次，您也可以選擇不同尺碼。每個尺碼和顏色會變成商品的一個獨立的SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_CREATE_SKU_NOTE", "EN", "For each SKU, each Color Code can only be used once. You can pick as many sizes as you need, each of the size and colour will become 1 SKU under the product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_PROD_EXPORT", "CHS", "您可下载所有商品记录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_PROD_EXPORT", "CHT", "您可下载所有商品記錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_PROD_EXPORT", "EN", "You can export all existing product records in an xlsx file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_PROD_PUBLISH", "CHS", "激活所有待处理的商品记录，它们将被显示在店面，并能够被消费者搜索及购买"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_PROD_PUBLISH", "CHT", "激活所有待處理的商品記錄，它們將被顯示在店面，並能夠被消費者搜索及購買"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_PROD_PUBLISH", "EN", "Activate all exisitng pending product records, they will be listed in storefront and able to be searched by consumers"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_PROD_IMG_UPLOAD", "CHS", "请为商品上传有正确命名格式的图片。您可以下载有详细说明的图像指南"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_PROD_IMG_UPLOAD", "CHT", "請為商品上傳有正確命名格式的圖片。您可以下載有詳細說明的圖像指南"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_PROD_IMG_UPLOAD", "EN", "Please upload Images for product with correct file naming format. You can download the image guideline for detailed instructions"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_SKU_INVENTORY_CREATE", "CHS", "SKU库存定义了商品在一个仓库中的库存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_SKU_INVENTORY_CREATE", "CHT", "SKU庫存定義了商品在一個倉庫中的庫存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_SKU_INVENTORY_CREATE", "EN", "A SKU inventory define the stock level of a product SKU in a warehouse"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_SKU_INVENTORY_EDIT", "CHS", "SKU库存定义了商品在一个仓库中的库存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_SKU_INVENTORY_EDIT", "CHT", "SKU庫存定義了商品在一個倉庫中的庫存量。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_SKU_INVENTORY_EDIT", "EN", "A SKU inventory define the stock level of a product SKU in a warehouse"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_INVENTORY_EXPORT", "CHS", "您可下载所有库存记录到一个xlsx文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_INVENTORY_EXPORT", "CHT", "您可下载所有庫存記錄到一個xlsx文件"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_INVENTORY_EXPORT", "EN", "You can export all existing inventory records in an xlsx file"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_MERCHANT_CREATE", "CHS", "商家可以是品牌旗舰店，单品牌代理或多品牌代理。单品牌代理商只能管理一个品牌，而多品牌代理商可以管理超过一个品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_MERCHANT_CREATE", "CHT", "商家可以是品牌旗舰店，單品牌代理或多品牌代理。單品牌代理商只能管理一個品牌，而多品牌代理商可以管理超過一個品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_MERCHANT_CREATE", "EN", "A merchant can be a brand flagship store, a mono-brand agent or multi-brand agent. A mono-brand agent can only manage a single brand, whereas a multi-brand agent can manage more than 1 brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_MERCHANT_EDIT", "CHS", "商家可以是品牌旗舰店，单品牌代理或多品牌代理。单品牌代理商只能管理一个品牌，而多品牌代理商可以管理超过一个品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_MERCHANT_EDIT", "CHT", "商家可以是品牌旗舰店，單品牌代理或多品牌代理。單品牌代理商只能管理一個品牌，而多品牌代理商可以管理超過一個品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_MERCHANT_EDIT", "EN", "A merchant can be a brand flagship store, a mono-brand agent or multi-brand agent. A mono-brand agent can only manage a single brand, whereas a multi-brand agent can manage more than 1 brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_BRAND_CREATE", "CHS", "创建後品牌将直接发布。品牌子域名必需是独一无二。公司商标会显示在不同的页面，因此需要上传不同尺寸的图片文档"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_BRAND_CREATE", "CHT", "創建後品牌將直接發佈。品牌子域名必需是獨一無二。公司商標會顯示在不同的頁面，因此需要上傳不同尺寸的圖片文檔"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_BRAND_CREATE", "EN", "Brand will be published directly after created. Brand sub-domain must be unique. Company logo will be displayed in different page therefore different sizes are required"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_BRAND_EDIT", "CHS", "品牌的状态可由待处理更变为激活 (在店面发布) 或从使用中改为停用 (隐藏於店面)，留意相关商品将受到这个状态而影响。品牌子域名必需是独一无二。公司商标会显示在不同的页面，因此需要上传不同尺寸的图片文档"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_BRAND_EDIT", "CHT", "品牌的狀態可由待處理更變為激活 (在店面發佈) 或從使用中改為停用 (隱藏於店面)，留意相關商品將受到這個狀態而影響。品牌子域名必需是獨一無二。公司商標會顯示在不同的頁面，因此需要上傳不同尺寸的圖片文檔"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_REM_BRAND_EDIT", "EN", "The status of a brand can be changed from pending to active or active to inactive, related product will be affected by this change. Brand sub-domain must be unique. Company logo will be displayed in different page therefore different sizes are required"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_CATEGORY_CREATE", "CHS", "类别能创建为草稿或当所有必填项目都填写後可直接发布。创建新类别时请填写表单的每个项目，请输入所有语言信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_CATEGORY_CREATE", "CHT", "類別能創建為草稿或當所有必填項目都填寫後可直接發佈。創建新類別時請填寫表單的每個項目，請輸入所有語言信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_CATEGORY_CREATE", "EN", "A category can be saved as draft or published directly given all the mandatory items are fielded. Please fill in all fields to create new category, you can enter information in other languages if required"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_CATEGORY_EDIT", "CHS", "类别的状态可由待处理更变为激活 (在店面发布) 或从使用中改为停用 (隐藏於店面)，留意相关商品将受到这个状态而影响"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_CATEGORY_EDIT", "CHT", "類別的狀態可由待處理更變為激活 (在店面發佈) 或從使用中改為停用 (隱藏於店面)，留意相關商品將受到這個狀態而影響"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_CATEGORY_EDIT", "EN", "The status of a category can be changed from pending to active or active to inactive, related product will be affected by this change. Please fill in all fields to edit category, you can enter information in other languages if required"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_MISSING_IMG_NOTE", "CHS", "每个商品必须至少上传1张精选图片为店面默认列表图像，或1张颜色图片为不同颜色的商品图像。您可以在这里检视所有没有上传图片的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_MISSING_IMG_NOTE", "CHT", "每個商品必須至少上傳1張精選圖片為店面默認列表圖像，或1張顏色圖片為不同顏色的商品圖像。您可以在這裡檢視所有沒有上傳圖片的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_MISSING_IMG_NOTE", "EN", "A Product must have at least 1 Featured image as default listing image or Colour image display when user picks an available color. You can see all existing pending Product without any image here for your reference"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGINATION_SHOWING_PAGE", "CHS", "显示第{0}页，共{1}页"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGINATION_SHOWING_PAGE", "CHT", "顯示第{0}頁，共{1}頁"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGINATION_SHOWING_PAGE", "EN", "Showing Page {0} of {1}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_WISHLIST", "CHS", "心愿单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_WISHLIST", "CHT", "心願單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_WISHLIST", "EN", "Wishlist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART", "CHS", "加入购物车"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART", "CHT", "加入購物車"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART", "EN", "Add to Cart"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REMOVE", "CHS", "删除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REMOVE", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REMOVE", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CA_CONFIRM_REMOVE", "CHS", "确认将这商品删除？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CA_CONFIRM_REMOVE", "CHT", "確認將這商品刪除？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CA_CONFIRM_REMOVE", "EN", "Confirm to remove this product?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONFIRM", "CHS", "确认"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONFIRM", "CHT", "確認"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONFIRM", "EN", "Confirm"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_TOTAL", "CHS", "合计:"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_TOTAL", "CHT", "合計:"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_TOTAL", "EN", "Total:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_QTY", "CHS", "数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_QTY", "CHT", "數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_QTY", "EN", "Quantity"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_CANCEL", "CHS", "取消朋友请求"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_CANCEL", "CHT", "取消朋友請求"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_CANCEL", "EN", "Cancel Friend Request"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_CANCEL_CONF", "CHS", "确认取消朋友请求？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_CANCEL_CONF", "CHT", "確認取消朋友請求？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_CANCEL_CONF", "EN", "Confirm to cancel Friend Request?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_NO_RESULT", "CHS", "没有可显示的信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_NO_RESULT", "CHT", "沒有可顯示的信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_NO_RESULT", "EN", "No Information to be displayed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRAND_MERCHANT", "CHS", "品牌商家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRAND_MERCHANT", "CHT", "品牌商家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRAND_MERCHANT", "EN", "Brand Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEW_PRODUCT", "CHS", "最新货品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEW_PRODUCT", "CHT", "最新貨品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEW_PRODUCT", "EN", "New Arrival"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FEATURED_PRODUCT", "CHS", "精选货品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FEATURED_PRODUCT", "CHT", "精選貨品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FEATURED_PRODUCT", "EN", "Featured Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_INFO_DISPLAY", "CHS", "没有可显示的信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_INFO_DISPLAY", "CHT", "沒有可顯示的信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_INFO_DISPLAY", "EN", "No Information to be displayed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRIEND_LIST", "CHS", "朋友列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRIEND_LIST", "CHT", "朋友列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRIEND_LIST", "EN", "Friends"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BADGE_CODE_NIL", "CHS", "标签代码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BADGE_CODE_NIL", "CHT", "標籤代碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BADGE_CODE_NIL", "EN", "Badge Code is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK_TO_LOGIN", "CHS", "返回登入页面"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK_TO_LOGIN", "CHT", "返回登入頁面"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK_TO_LOGIN", "EN", "Back to Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MISSING_FEATURE_IMG", "CHS", "缺失精选图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MISSING_FEATURE_IMG", "CHT", "缺失精選圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MISSING_FEATURE_IMG", "EN", "Missing Feature Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MISSING_COLOR_IMG", "CHS", "缺失颜色图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MISSING_COLOR_IMG", "CHT", "缺失顏色圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MISSING_COLOR_IMG", "EN", "Missing Color Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MISSING_DESC_IMG", "CHS", "缺失说明图片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MISSING_DESC_IMG", "CHT", "缺失說明圖片"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MISSING_DESC_IMG", "EN", "Missing Description Images"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_ADMIN", "CHS", "管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_ADMIN", "CHT", "管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_ADMIN", "EN", "Administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_CONTENT_MNG", "CHS", "内容管理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_CONTENT_MNG", "CHT", "內容管理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_CONTENT_MNG", "EN", "Content Manager"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_CS", "CHS", "客户服务"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_CS", "CHT", "客戶服務"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_CS", "EN", "Customer Service"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_MERC", "CHS", "采购业务"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_MERC", "CHT", "採購業務"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_MERC", "EN", "Merchandizer"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_ORDER", "CHS", "订单处理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_ORDER", "CHT", "訂單處理"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_ORDER", "EN", "Order Processing"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_PROD_RETURN", "CHS", "商品退货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_PROD_RETURN", "CHT", "商品退貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_PROD_RETURN", "EN", "Product Return"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_FINANCE", "CHS", "财务"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_FINANCE", "CHT", "財務"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_USER_ROLE_FINANCE", "EN", "Finance"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_OVERALL", "CHS", "综合排序"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_OVERALL", "CHT", "綜合排序"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_OVERALL", "EN", "Overall Sort"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_SALES_VOL", "CHS", "销量优先"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_SALES_VOL", "CHT", "銷量優先"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_SALES_VOL", "EN", "Sort by Sales"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_DATE", "CHS", "新货优先"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_DATE", "CHT", "新貨優先"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_DATE", "EN", "Sort by Date"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_PRICE_ASC", "CHS", "价格从低到高"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_PRICE_ASC", "CHT", "價格從低到高"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_PRICE_ASC", "EN", "Sort by Lowest Price"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_PRICE_DESC", "CHS", "价格从高到低"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_PRICE_DESC", "CHT", "價格從高到低"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SORT_PRICE_DESC", "EN", "Sort by Highest Price"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PRODUCT_EDITED", "CHS", "产品编辑成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PRODUCT_EDITED", "CHT", "產品編輯成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PRODUCT_EDITED", "EN", "Edited product successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PRODUCT_CREATED", "CHS", "产品创建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PRODUCT_CREATED", "CHT", "產品創建成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_PRODUCT_CREATED", "EN", "Created product successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_LIST_NIL", "CHS", "每个产品至少需要一个SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_LIST_NIL", "CHT", "每個產品至少需要一個SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_LIST_NIL", "EN", "A product needs to have at least one SKU"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CFM_SORT", "CHS", "确认"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CFM_SORT", "CHT", "確認"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CFM_SORT", "EN", "Confirm"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_EXISTED", "CHS", "SKU已经存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_EXISTED", "CHT", "SKU已經存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_EXISTED", "EN", "SKU already exist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_COLOR_SIZE_CHOOSE", "CHS", "请选择颜色或者尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_COLOR_SIZE_CHOOSE", "CHT", "請選擇顏色或者尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_COLOR_SIZE_CHOOSE", "EN", "Please choose Color or Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_NIL", "CHS", "商品代码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_NIL", "CHT", "商品代碼是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_NIL", "EN", "Style Code is mandatory"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MERCHANT", "CHS", "商家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MERCHANT", "CHT", "商家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MERCHANT", "EN", "Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEW_PRODUCT_SHORT", "CHS", "新品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEW_PRODUCT_SHORT", "CHT", "新品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEW_PRODUCT_SHORT", "EN", "New"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FREE_SHIPPING", "CHS", "免费送货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FREE_SHIPPING", "CHT", "免費送貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FREE_SHIPPING", "EN", "Free Shipping"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EXCLUSIVE", "CHS", "独家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EXCLUSIVE", "CHT", "獨家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EXCLUSIVE", "EN", "Exclusive"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TRENDING", "CHS", "热卖"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TRENDING", "CHT", "熱賣"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TRENDING", "EN", "Trending"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TV_SERIES", "CHS", "电视剧同款"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TV_SERIES", "CHT", "電視劇同款"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TV_SERIES", "EN", "TV Series"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CELEBRITY_SERIES", "CHS", "明星同款"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CELEBRITY_SERIES", "CHT", "明星同款"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CELEBRITY_SERIES", "EN", "Celebrity Series"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DELETE_SKU_CONFIRM", "CHS", "确认删除此SKU?"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DELETE_SKU_CONFIRM", "CHT", "確認刪除此SKU?"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_DELETE_SKU_CONFIRM", "EN", "Confirm to delete this SKU ?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_EDIT_TITLE", "CHS", "编辑商品 - {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_EDIT_TITLE", "CHT", "編輯商品 - {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PROD_EDIT_TITLE", "EN", "Edit Product - {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_SKU_WITHOUT_COLOR_SIZE", "CHS", "勾选以创建无需设定颜色无尺码的SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_SKU_WITHOUT_COLOR_SIZE", "CHT", "勾選以創建無需顏色無尺碼的SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_SKU_WITHOUT_COLOR_SIZE", "EN", "Check to create a SKU without color and size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CART", "CHS", "购物车"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CART", "CHT", "購物車"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CART", "EN", "Cart"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_USE_COUPON", "CHS", "使用平台优惠券"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_USE_COUPON", "CHT", "使用平台優惠券"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_USE_COUPON", "EN", "Use Coupon"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INPUT_COUPON", "CHS", "输入平台优惠券代码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INPUT_COUPON", "CHT", "輸入平台優惠券代碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INPUT_COUPON", "EN", "Please enter coupon code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_COLOR", "CHS", "颜色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_COLOR", "CHT", "顏色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_COLOR", "EN", "Color"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_SIZE", "CHS", "尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_SIZE", "CHT", "尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_SIZE", "EN", "Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_QTY", "CHS", "数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_QTY", "CHT", "數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_QTY", "EN", "Quantity"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDIT", "CHS", "编辑"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDIT", "CHT", "編輯"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDIT", "EN", "Edit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SELECT_ALL_PI", "CHS", "全选"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SELECT_ALL_PI", "CHT", "全選"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SELECT_ALL_PI", "EN", "Select All"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECTED_PI_NO", "CHS", "共选"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECTED_PI_NO", "CHT", "共選"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SELECTED_PI_NO", "EN", "Total"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DONE", "CHS", "完成"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DONE", "CHT", "完成"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DONE", "EN", "Done"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CFM_PURCHASE", "CHS", "确认购买"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CFM_PURCHASE", "CHT", "確認購買"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CFM_PURCHASE", "EN", "Confirm"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOVE2WISHLIST", "CHS", "移到心愿单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOVE2WISHLIST", "CHT", "移到心願單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOVE2WISHLIST", "EN", "Move to Wishlist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DELETE", "CHS", "删除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DELETE", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DELETE", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SIZEGRID", "CHS", "尺码表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SIZEGRID", "CHT", "尺碼表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SIZEGRID", "EN", "Size Grid"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDITITEM_QTY", "CHS", "数量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDITITEM_QTY", "CHT", "數量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDITITEM_QTY", "EN", "Quantity"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDITITEM_ADD2CART", "CHS", "加入购物车"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDITITEM_ADD2CART", "CHT", "加入購物車"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDITITEM_ADD2CART", "EN", "Add to Cart"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_SUCCESS", "CHS", "添加成功，在购物车等亲~"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_SUCCESS", "CHT", "添加成功，在購物車等親~"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_SUCCESS", "EN", "Added to cart successfully~"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_FAIL", "CHS", "添加不成功，再试试亲~"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_FAIL", "CHT", "添加不成功，在試試親~"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD2CART_FAIL", "EN", "Failed adding to cart, please try again~"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_PUB_TIME", "CHS", "错误日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_PUB_TIME", "CHT", "錯誤日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_PUB_TIME", "EN", "Invalid Date & Time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDITITEM_SUBTOTAL", "CHS", "合计"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDITITEM_SUBTOTAL", "CHT", "合計"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_EDITITEM_SUBTOTAL", "EN", "Subtotal"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT_1", "CHS", "亲，没有找到相关商品。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT_1", "CHT", "親，沒有找到相關商品。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT_1", "EN", "We haven\'t found any related products."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT_2", "CHS", "您可以换个词再试试，或者看看我们的热门搜寻。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT_2", "CHT", "您可以換個詞再試試，或者看看我們的熱門搜尋。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NO_PROD_RESULT_2", "EN", "Please try another search term, or take a look at our trending search."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_INVENTORY_LOC", "CHS", "暂停库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_INVENTORY_LOC", "CHT", "暫停庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_INVENTORY_LOC", "EN", "Inactivate Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE_INVENTORY_LOC", "CHS", "删除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE_INVENTORY_LOC", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DELETE_INVENTORY_LOC", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_INVENTORY_LOC", "CHS", "激活库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_INVENTORY_LOC", "CHT", "激活庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_INVENTORY_LOC", "EN", "Activate Inventory Location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_PRICE_RANGE", "CHS", "价格区间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_PRICE_RANGE", "CHT", "價格區間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_PRICE_RANGE", "EN", "Price Range"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_CATEGORY", "CHS", "类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_CATEGORY", "CHT", "類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_CATEGORY", "EN", "Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_BRAND", "CHS", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_BRAND", "CHT", "品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_BRAND", "EN", "Brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_COLOR", "CHS", "颜色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_COLOR", "CHT", "顏色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_COLOR", "EN", "Color"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_SIZE", "CHS", "尺码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_SIZE", "CHT", "尺碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_SIZE", "EN", "Size"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_MERCHANT", "CHS", "商家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_MERCHANT", "CHT", "商家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_MERCHANT", "EN", "Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_EN_FORMAT", "CHS", "类别名称（英文）必须在1 – 25个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_EN_FORMAT", "CHT", "類別名稱（英文）必須在1 – 25個字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_EN_FORMAT", "EN", "Category Name (English) must be between 1-25 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_TC_FORMAT", "CHS", "类别名称（繁体中文）必须在1 – 25个字符之间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_TC_FORMAT", "CHT", "類別名稱（繁體中文）必須在1 – 25個字符之間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_TC_FORMAT", "EN", "Category Name (Traditional Chinese) must be between 1-25 characters"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_EN_NIL", "CHS", "类别名称（英文）是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_EN_NIL", "CHT", "類別名稱（英文）是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_EN_NIL", "EN", "Category Name (English) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_TC_NIL", "CHS", "类别名称（繁体中文）是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_TC_NIL", "CHT", "類別名稱（繁體中文）是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CATEGORY_NAME_TC_NIL", "EN", "Category Name (Traditional Chinese) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_BRAND_PLACEHOLDER", "CHS", "搜寻品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_BRAND_PLACEHOLDER", "CHT", "搜尋品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_BRAND_PLACEHOLDER", "EN", "Search for Brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_RESET", "CHS", "重设"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_RESET", "CHT", "重設"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FILTER_RESET", "EN", "Reset"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SUBDOMAIN_UNIQUE", "CHS", "这个子域名已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SUBDOMAIN_UNIQUE", "CHT", "這個子域名已被使用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SUBDOMAIN_UNIQUE", "EN", "This Sub-domain has been taken"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_OR_MOBILE_PATTERN", "CHS", "错误的电邮或手提号码格式，电邮地址格式为：用户名@域名； 手机号码格式为：＋国际区号-手机号码，例如：+852-12345678"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_OR_MOBILE_PATTERN", "CHT", "錯誤的電郵或手提號碼格式，電郵地址格式為：用戶名@域名； 手機號碼格式為：＋國際區號-手機號碼，例如：+852-12345678"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_EMAIL_OR_MOBILE_PATTERN", "EN", "The email/mobile no you entered is wrong. Email should contain an username@domainname; i.e. mobile no# should start with +country code-, i.e +852-12345678."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_COLOR_NIL", "CHS", "SKU颜色名称是必填项目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_COLOR_NIL", "CHT", "SKU顏色名稱是必填項目"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_COLOR_NIL", "EN", "SKU color name is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_LOGIN", "CHS", "登录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_LOGIN", "CHT", "登錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_LOGIN", "EN", "Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GUEST_LOGIN", "CHS", "直接开始逛逛"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GUEST_LOGIN", "CHT", "直接開始逛逛"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GUEST_LOGIN", "EN", "Guest Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_STYLECODE_UNIQUE", "CHS", "产品代码必须唯一"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_STYLECODE_UNIQUE", "CHT", "產品代碼必須唯一"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_STYLECODE_UNIQUE", "EN", "Product Style Code must be unique"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_WECHAT_LOGIN", "CHS", "微信登录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_WECHAT_LOGIN", "CHT", "微信登錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_WECHAT_LOGIN", "EN", "WeChat Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALIPAY_LOGIN", "CHS", "支付宝登录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALIPAY_LOGIN", "CHT", "支付寶登錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALIPAY_LOGIN", "EN", "Alipay Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOBILE_REGISTRATION", "CHS", "手机注册"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOBILE_REGISTRATION", "CHT", "手機註冊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOBILE_REGISTRATION", "EN", "Mobile Registration"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PW_FORMAT", "CHS", "密码格式不正确。密码必须至少为8个字符，其中包括最少1个大写字符，1个小写字符，1个特殊字符和1个数字字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PW_FORMAT", "CHT", "密碼格式不正確。密碼必須至少為8個字符，其中包括最少1個大寫字符，1个小写字符，1個特殊字符和1個數字字符"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PW_FORMAT", "EN", "Invalid Password Format. Password must be at least 8 characters including 1 uppercase letter, 1 lowercase, 1 special character and alphanumeric character"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_ACCOUNT_PATTERN", "CHS", "账户格式错误。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_ACCOUNT_PATTERN", "CHT", "帳戶格式錯誤。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_ACCOUNT_PATTERN", "EN", "Invalid account pattern."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_ACCOUNT_NIL", "CHS", "请输入您账户信息。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_ACCOUNT_NIL", "CHT", "請輸入您帳戶信息。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_ACCOUNT_NIL", "EN", "Please enter your account information."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PASSWORD", "CHS", "密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PASSWORD", "CHT", "密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PASSWORD", "EN", "Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_PW_NIL", "CHS", "请输入您的密码。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_PW_NIL", "CHT", "請輸入您的密碼。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_PW_NIL", "EN", "Please enter your password."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_INVALID_CREDENTIALS_1", "CHS", "您的用户名或密码不正确，您还有"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_INVALID_CREDENTIALS_1", "CHT", "您的用戶名或密碼不正確，尚餘"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_INVALID_CREDENTIALS_1", "EN", "Invalid email/mobile or password combination,"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_INVALID_CREDENTIALS_2", "CHS", "重试机会。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_INVALID_CREDENTIALS_2", "CHT", "次重試機會。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_INVALID_CREDENTIALS_2", "EN", "more attempts are allowed."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USER_STATUS_INACTIVE", "CHS", "您的账户已被暂停使用, 请查阅您电邮或SMS以激活账户。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USER_STATUS_INACTIVE", "CHT", "您的帳戶已被暫停使用，請查閱您電郵或SMS以激活帳戶。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USER_STATUS_INACTIVE", "EN", "Your account status is inactive. Please check your email / SMS to activate your account."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USER_STATUS_PENDING", "CHS", "你的账户正在等待处理, 请查阅您电邮或SMS以激活账户。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USER_STATUS_PENDING", "CHT", "您的帳戶正在等待處理，請查閱您電郵或SMS以激活帳戶。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USER_STATUS_PENDING", "EN", "Your account status is pending. Please check your email / SMS to activate your account."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FORGOT_PASSWORD", "CHS", "忘记密码？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FORGOT_PASSWORD", "CHT", "忘記密碼？"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FORGOT_PASSWORD", "EN", "Forgot Password?"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_IDD_REGION", "CHS", "地区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_IDD_REGION", "CHT", "地區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_IDD_REGION", "EN", "Region"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_SKUCODE_UNIQUE_WITHIN_MERCHANT", "CHS", "SKU代码必须唯一"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_SKUCODE_UNIQUE_WITHIN_MERCHANT", "CHT", "SKU代碼必須唯一"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_SKUCODE_UNIQUE_WITHIN_MERCHANT", "EN", "SKU code needs to be unique"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALERT", "CHS", "提示"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALERT", "CHT", "提示"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALERT", "EN", "Alert"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANDIZE_CATEGORY_NIL", "CHS", "在添加下一个专题类别之前，请先设置上次选择的专题类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANDIZE_CATEGORY_NIL", "CHT", "在添加下一個專題類別之前，請先設置上次選擇的專題類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANDIZE_CATEGORY_NIL", "EN", "You can only add a new merchandize category after you complete selection after the previous one."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_MOBILE_PATTERN", "CHS", "手机号码格式错误。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_MOBILE_PATTERN", "CHT", "手機號碼格式錯誤。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_MOBILE_PATTERN", "EN", "Invalid mobile number format."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE", "CHS", "请按住滑块，拖动到最右边"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE", "CHT", "請按住滑塊，拖動到最右邊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE", "EN", "Please press and swipe to the right"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE_SENT", "CHS", "已发放验证码 ("); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE_SENT", "CHT", "已發放驗證碼 ("); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE_SENT", "EN", "Verification code has been sent ("); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INPUT_VERCODE", "CHS", "输入验证码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INPUT_VERCODE", "CHT", "輸入驗證碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INPUT_VERCODE", "EN", "Enter verification code"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_RESULT", "CHS", "缺失图片结果"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_RESULT", "CHT", "缺失圖片結果"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MISSING_IMG_RESULT", "EN", "Missing Image Result"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_ALL_MC_USERS", "CHS", "所有用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_ALL_MC_USERS", "CHT", "所有用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_ALL_MC_USERS", "EN", "All Merchant Center Users"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_INVENTORY_PRODUCTS", "CHS", "搜寻商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_INVENTORY_PRODUCTS", "CHT", "搜尋商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_INVENTORY_PRODUCTS", "EN", "Search for Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_EXISTS", "CHS", "这个库存位置已经建立了库存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_EXISTS", "CHT", "這個庫存位置已經建立了庫存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_EXISTS", "EN", "Inventory already exists for this location."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REGISTER", "CHS", "注册"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REGISTER", "CHT", "註冊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REGISTER", "EN", "Register"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INV_LOC_CREATE", "CHS", "成功创建新的库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INV_LOC_CREATE", "CHT", "成功创建新的庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INV_LOC_CREATE", "EN", "Created inventory location successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_EXTERNAL_CODE_UNIQUE_WITHIN_MERCHANT", "CHS", "位置代码是唯一的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_EXTERNAL_CODE_UNIQUE_WITHIN_MERCHANT", "CHT", "位置代码是唯一的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_EXTERNAL_CODE_UNIQUE_WITHIN_MERCHANT", "EN", "Location Code must be unique  within one merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_NAME_UNIQUE_WITHIN_MERCHANT", "CHS", "位置名称必须唯一"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_NAME_UNIQUE_WITHIN_MERCHANT", "CHT", "位置名稱必須唯一"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_LOCATION_NAME_UNIQUE_WITHIN_MERCHANT", "EN", "Location Name must be unique"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ACCOUNT", "CHS", "账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ACCOUNT", "CHT", "帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ACCOUNT", "EN", "Account"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOB_UN_EMAIL", "CHS", "手机 / 账号 / 邮箱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOB_UN_EMAIL", "CHT", "手機 / 帳號 / 郵箱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOB_UN_EMAIL", "EN", "Mobile / Username / Email"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_PW", "CHS", "输入密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_PW", "CHT", "輸入密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ENTER_PW", "EN", "Enter Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVALID_TOKEN", "CHS", "验证码无效"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVALID_TOKEN", "CHT", "驗證碼無效"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVALID_TOKEN", "EN", "Invalid Token"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_TOKEN", "CHS", "验证码无效，请检查"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_TOKEN", "CHT", "驗證碼無效，請檢查"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVALID_TOKEN", "EN", "Invalid token, please check"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INPUT_MOBILE", "CHS", "输入手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INPUT_MOBILE", "CHT", "輸入手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INPUT_MOBILE", "EN", "Enter Mobile Number"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VERCODE_INCORRECT_1", "CHS", "验证码不正确（余"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VERCODE_INCORRECT_1", "CHT", "驗證碼不正確（余"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VERCODE_INCORRECT_1", "EN", "Incorrect Verification Code ("); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VERCODE_INCORRECT_2", "CHS", "次机会）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VERCODE_INCORRECT_2", "CHT", "次機會）"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VERCODE_INCORRECT_2", "EN", "more attempts allowed)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_USERNAME", "CHS", "会员名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_USERNAME", "CHT", "會員名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_USERNAME", "EN", "Username"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISPNAME", "CHS", "昵称"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISPNAME", "CHT", "暱稱"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DISPNAME", "EN", "Display Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROFILE_PIC", "CHS", "头像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROFILE_PIC", "CHT", "頭像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROFILE_PIC", "EN", "Profile Picture"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONF_PW", "CHS", "重复输入密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONF_PW", "CHT", "重複輸入密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONF_PW", "EN", "Confirm Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TNC_CHECK", "CHS", "我已阅读并接受"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TNC_CHECK", "CHT", "我已閱讀並接受"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TNC_CHECK", "EN", "I have read and agreed to the"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TNC_LINK", "CHS", "美美用户注册协议"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TNC_LINK", "CHT", "美美用戶註冊協議"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TNC_LINK", "EN", "MM User Agreement"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USERNAME_NIL", "CHS", "请输入您的账号名称。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USERNAME_NIL", "CHT", "請輸入您的帳號名稱。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USERNAME_NIL", "EN", "Please enter your username."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USERNAME_PATTERN", "CHS", "账号格式错误。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USERNAME_PATTERN", "CHT", "帳號格式錯誤。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_USERNAME_PATTERN", "EN", "Invalid username format."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_PW_PATTERN", "CHS", "密码格式错误。密码必须8个字符以上，并至少包含一个大写、小写、数字、特别字符：(~!@#$%^&*()_+|}{:\"?><.)。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_PW_PATTERN", "CHT", "密碼格式錯誤。密碼必須8個字符以上，並至少包含一個大寫、小寫、數字、特別字符：(~!@#$%^&*()_+|}{:\"?><.)。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_PW_PATTERN", "EN", "Invalid password format. Password requires at least 8 characters, and at least 1 character for each of the following type: lowercase letter, uppercase letter, digit, special character (~!@#$%^&*()_+|}{:\"?><.) ."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_CFM_PW_NOT_MATCH", "CHS", "确认密码不匹配。 请再试一次。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_CFM_PW_NOT_MATCH", "CHT", "確認密碼不匹配。 請再試一次。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_CFM_PW_NOT_MATCH", "EN", "Confirmation password does not match. Please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DUP_USERNAME", "CHS", "账号已被使用，或改用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DUP_USERNAME", "CHT", "帳號已被使用，或改用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DUP_USERNAME", "EN", "This username has already been taken, you may try"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TNC", "CHS", "用户注册协议"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TNC", "CHT", "用戶註冊協議"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_TNC", "EN", "User Agreement"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET_PW", "CHS", "设置新密码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET_PW", "CHT", "設置新密碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET_PW", "EN", "Reset Password"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET_PW_DESC", "CHS", "请输入你需要找回登录密码的账户："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET_PW_DESC", "CHT", "請輸入你需要找回登錄密碼的帳戶："); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RESET_PW_DESC", "EN", "Please enter your account to retrieve your password:"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CREATE_SUB_CATEGORY", "CHS", "创建子类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CREATE_SUB_CATEGORY", "CHT", "創建子類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_CREATE_SUB_CATEGORY", "EN", "Create Sub-category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VERCODE_INVALID", "CHS", "验证码不正确。请重试。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VERCODE_INVALID", "CHT", "驗證碼不正確。請重試。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VERCODE_INVALID", "EN", "Invalid verification code. Please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWER_LIST", "CHS", "粉丝列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWER_LIST", "CHT", "粉絲列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWER_LIST", "EN", "Followers"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE_SENT_EMAIL", "CHS", "已发放验证链接至电子邮箱以作重置密码。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE_SENT_EMAIL", "CHT", "已發放驗證鏈接至電子郵箱以作重置密碼。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE_SENT_EMAIL", "EN", "A verification link has been sent to your email."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BACK_TO_LOGIN", "CHS", "返回登录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BACK_TO_LOGIN", "CHT", "返回登錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BACK_TO_LOGIN", "EN", "Back to Login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INTERESTS_KEYWORDS", "CHS", "你喜爱的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INTERESTS_KEYWORDS", "CHT", "你喜愛的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INTERESTS_KEYWORDS", "EN", "Interests"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATORS_FOLLOW", "CHS", "建议美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATORS_FOLLOW", "CHT", "建議美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATORS_FOLLOW", "EN", "Curators"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_SKIP", "CHS", "跳过"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_SKIP", "CHT", "跳過"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_SKIP", "EN", "Skip"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATORS_MORE", "CHS", "更多美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATORS_MORE", "CHT", "更多美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATORS_MORE", "EN", "More"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRANDS_MORE", "CHS", "更多品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRANDS_MORE", "CHT", "更多品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRANDS_MORE", "EN", "More"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRANDS_FOLLOW", "CHS", "建议品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRANDS_FOLLOW", "CHT", "建議品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_BRANDS_FOLLOW", "EN", "Brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOW_CURATORS_CONT", "CHS", "继续"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOW_CURATORS_CONT", "CHT", "繼續"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOW_CURATORS_CONT", "EN", "Continue"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("UIBT_CA_FOLLOW_BRANDS_START", "CHS", "开始"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("UIBT_CA_FOLLOW_BRANDS_START", "CHT", "開始"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("UIBT_CA_FOLLOW_BRANDS_START", "EN", "Start"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_BARCODE_UNIQUE_WITHIN_MERCHANT", "CHS", "条形码必须唯一"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_BARCODE_UNIQUE_WITHIN_MERCHANT", "CHT", "條形碼必須唯一"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PRODUCT_BARCODE_UNIQUE_WITHIN_MERCHANT", "EN", "Barcode must be unique"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_BRAND", "CHS", "全部品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_BRAND", "CHT", "全部品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALL_BRAND", "EN", "All Brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CART_ITEM_FAILED", "CHS", "商品删除不成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CART_ITEM_FAILED", "CHT", "商品刪除不成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CART_ITEM_FAILED", "EN", "Failed to remove product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_CART_ITEM_SUCCESS", "CHS", "商品删除成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_CART_ITEM_SUCCESS", "CHT", "商品刪除成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_CART_ITEM_SUCCESS", "EN", "Removed product successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_DUP_USERNAME", "CHS", "账号已被使用。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_DUP_USERNAME", "CHT", "帳號已被使用。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_DUP_USERNAME", "EN", "This username has already been taken."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("SMS_CA_MOBILE_VER_SIGNUP", "CHS", "【美美】验证码：{{MobileVerificationToken}}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("SMS_CA_MOBILE_VER_SIGNUP", "CHT", "【美美】驗證碼：{{MobileVerificationToken}}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("SMS_CA_MOBILE_VER_SIGNUP", "EN", "[MayMay] Verification Code: {{MobileVerificationToken}}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("SMS_CA_MOBILE_VER_FORGET_PW", "CHS", "【美美】验证码：{{MobileVerificationToken}}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("SMS_CA_MOBILE_VER_FORGET_PW", "CHT", "【美美】驗證碼：{{MobileVerificationToken}}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("SMS_CA_MOBILE_VER_FORGET_PW", "EN", "[MayMay] Verification Code: {{MobileVerificationToken}}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_LOCAL_UPDATE", "CHS", "成功更新库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_LOCAL_UPDATE", "CHT", "成功更新庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_INVENTORY_LOCAL_UPDATE", "EN", "Updated inventory location successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_PROFILE", "CHS", "商戶信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_PROFILE", "CHT", "商戶信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_PROFILE", "EN", "Merchant Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MSG_MERCHANT_PROFILE_DESC", "CHS", "修改信息、账户资料及更改状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MSG_MERCHANT_PROFILE_DESC", "CHT", "修改信息、賬戶資料及更改狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MSG_MERCHANT_PROFILE_DESC", "EN", "To edit profile, account information and change the status."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USERS", "CHS", "用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USERS", "CHT", "用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USERS", "EN", "Users"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USERS_DESC", "CHS", "查看、修改及建立商户用家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USERS_DESC", "CHT", "查看、修改及建立商戶用家"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USERS_DESC", "EN", "To view, edit and create merchant users"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATIONS", "CHS", "庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATIONS", "CHT", "庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATIONS", "EN", "Inventory Locations"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATIONS_DESC", "CHS", "查看、修改及建立库存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATIONS_DESC", "CHT", "查看、修改及建立庫存位置"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVENTORY_LOCATIONS_DESC", "EN", "To view, edit and create inventory location."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCTS", "CHS", "商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCTS", "CHT", "商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCTS", "EN", "Products"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCTS_DESC", "CHS", "查看丶修改及建立产品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCTS_DESC", "CHT", "查看、修改及建立產品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRODUCTS_DESC", "EN", "To view, edit and create products."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_TT_USER_ROLE", "CHS", "商户用户管理MM各个服务区的角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_TT_USER_ROLE", "CHT", "商戶用戶管理MM各個服務區的角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_TT_USER_ROLE", "EN", "A merchant user\'s role to manage different service areas in MM"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONFIRMATION", "CHS", "确定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONFIRMATION", "CHT", "確定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CONFIRMATION", "EN", "Confirmation"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PW_SUC_DESC", "CHS", "成功重设密码，你现在可以"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PW_SUC_DESC", "CHT", "成功重設密碼，你現在可以"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PW_SUC_DESC", "EN", "You have successfully reset your password. You can now"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGIN_LOWERCASE", "CHS", "登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGIN_LOWERCASE", "CHT", "登入"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_LOGIN_LOWERCASE", "EN", "login"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VIEW_PROF_PIC", "CHS", "查看头像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VIEW_PROF_PIC", "CHT", "查看頭像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VIEW_PROF_PIC", "EN", "View Profile Picture"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_CURR_PROF_PIC", "CHS", "删除当前头像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_CURR_PROF_PIC", "CHT", "刪除當前頭像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_CURR_PROF_PIC", "EN", "Delete Current Profile Picture"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_PIC_TAKE_PHOTO", "CHS", "拍照"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_PIC_TAKE_PHOTO", "CHT", "拍照"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_PIC_TAKE_PHOTO", "EN", "Take Photo"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_PIC_CHOOSE_LIBRARY", "CHS", "从相册选择"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_PIC_CHOOSE_LIBRARY", "CHT", "從相冊選擇"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_PIC_CHOOSE_LIBRARY", "EN", "Choose From Library"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VIEW_COVER_PIC", "CHS", "查看个人封面图"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VIEW_COVER_PIC", "CHT", "查看個人封面圖"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_VIEW_COVER_PIC", "EN", "View Cover Picture"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROFILE_MSG_CHAT", "CHS", "私聊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROFILE_MSG_CHAT", "CHT", "私聊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROFILE_MSG_CHAT", "EN", "Message"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_SKUCODE_NIL", "CHS", "SKU代码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_SKUCODE_NIL", "CHT", "SKU代碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_SKUCODE_NIL", "EN", "SKU code is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_BARCODE_NIL", "CHS", "条形码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_BARCODE_NIL", "CHT", "條形碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_BARCODE_NIL", "EN", "Barcode is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_TT_LOCATION_SAFETY_THRESHOLD", "CHS", "位置安全库存量值用来控制商品的库存变化，以确保商品没有从库存位置过量售出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_TT_LOCATION_SAFETY_THRESHOLD", "CHT", "位置安全庫存量值用來控制商品的庫存變化，以確保商品沒有從庫存位置過量售出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_TT_LOCATION_SAFETY_THRESHOLD", "EN", "Location Safety Threshold is used to control your inventory for a product variant to ensure that you do not over-sell from an inventory location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_LIST", "CHS", "关注列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_LIST", "CHT", "關注列表"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FOLLOWING_LIST", "EN", "Following"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INTERESTS_KEYWORDS_DESC", "CHS", "请选1个或以上你喜欢的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INTERESTS_KEYWORDS_DESC", "CHT", "請選1個或以上你喜歡的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INTERESTS_KEYWORDS_DESC", "EN", "Please select at least 1 interest"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_PATTERN", "CHS", "商品销售价需为正数或最多支持小数点后两位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_PATTERN", "CHT", "商品销售价需為正數最多支持小數點後兩位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_PRICE_PATTERN", "EN", "Product Sale Price needs to be positive or rounded to 2 digits after the decimal"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_PATTERN", "CHS", "商品零售价需为正数或最多支持小数点后两位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_PATTERN", "CHT", "商品零售價需為正數最多支持小數點後兩位"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_PATTERN", "EN", "Product Retail Price needs to be positive or rounded to 2 digits after the decimal"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_ACCEPT", "CHS", "接受"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_ACCEPT", "CHT", "接受"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_ACCEPT", "EN", "Accept"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_DEL", "CHS", "删除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_DEL", "CHT", "刪除"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FRD_REQ_DEL", "EN", "Delete"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATORS_FOLLOWER_NO", "CHS", "粉丝"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATORS_FOLLOWER_NO", "CHT", "粉絲"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CURATORS_FOLLOWER_NO", "EN", "Followers"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GENDER", "CHS", "性別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GENDER", "CHT", "性別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GENDER", "EN", "Gender"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_USER_PROF_CREATE", "CHS", "商户用户可以管理品牌以下的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_USER_PROF_CREATE", "CHT", "商戶用戶可以管理品牌以下的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_USER_PROF_CREATE", "EN", "A merchant user can manage products of assigned brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_USER_ROLE", "CHS", "商户用户管理MM各个服务区的角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_USER_ROLE", "CHT", "商戶用戶管理MM各個服務區的角色"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_USER_ROLE", "EN", "A Merchant User\'s Role to manage different service areas in MM"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_USER_PROF_EDIT", "CHS", "商户用户可以管理品牌以下的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_USER_PROF_EDIT", "CHT", "商戶用戶可以管理品牌以下的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_USER_PROF_EDIT", "EN", "A merchant user can manage products of assigned brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_INVENTORY_LOC_CREATE", "CHS", "库存位置记录了仓库和商店的资料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_INVENTORY_LOC_CREATE", "CHT", "庫存位置記錄了倉庫和商店的資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_INVENTORY_LOC_CREATE", "EN", "An inventory location will be a record of warehouses and stores"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_LOCATION_SAFETY_THRESHOLD", "CHS", "位置安全库存量值用来控制商品的库存变化，以确保商品没有从库存位置过量售出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_LOCATION_SAFETY_THRESHOLD", "CHT", "位置安全庫存量值用來控制商品的庫存變化，以確保商品沒有從庫存位置過量售出"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_LOCATION_SAFETY_THRESHOLD", "EN", "Location Safety Threshold is used to control your inventory for a product variant to ensure that you do not over-sell from an inventory location"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_INVENTORY_LOC_EDIT", "CHS", "库存位置记录了仓库和商店的资料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_INVENTORY_LOC_EDIT", "CHT", "庫存位置記錄了倉庫和商店的資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_INVENTORY_LOC_EDIT", "EN", "An inventory location will be a record of warehouses and stores"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_CREATE", "CHS", "商品名称，零售价，品牌名称，上架日期时间以及SKU为必填项，其它项目为选填。商户可存储商品为草稿或直接发布产品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_CREATE", "CHT", "商品名稱，零售價，品牌名稱，上架日期時間以及SKU為必填項，其它項目為選填。商戶可存儲商品為草稿或直接發布產品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_CREATE", "EN", "Product Name, Retail Price, Sale Price, Brand Name, Available Date & Time and SKU must be filled , others are optional fields. A product can be saved as draft or published directly"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_EDIT", "CHS", "商品的状态可由待处理更变为激活 (在店面发布) 或从使用中改为停用 (隐藏於店面)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_EDIT", "CHT", "商品的狀態可由待處理更變為激活 (在店面發佈) 或從使用中改為停用 (隱藏於店面)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_EDIT", "EN", "The status of a product can be changed from pending to activate (publish in storefront) or activate to inactivate (hidden from storefront)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_INFO", "CHS", "商品名称所有的语言栏都应填写，必须选择品牌名称和样式代码，因为它们将用作商品识别。零售价和销售价 (人民币) 代表建议原价和商品在MayMay上标示出的售价"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_INFO", "CHT", "商品名稱所有的語言欄都應填寫，必須選擇品牌名稱和樣式代碼，因為它們將用作商品識別。零售價和銷售價 (人民幣) 代表建議原價和商品在MayMay上標示出的售價"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_INFO", "EN", "Product name should be have all required languages filled in, Brand name and Style Code must be selected as they identify the product directly, so as Retail Price and Sales Price which represent the amount of Renminbi for which the company that produces a product recommends that it be sold in stores and the original price of the product sold on MayMay"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_AVAIL_FROM_TO", "CHS", "上架日期时间决定了商品将何时被显示於店面让消费者加入购买直到特定的下架日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_AVAIL_FROM_TO", "CHT", "上架日期時間決定了商品將何時被顯示於店面讓消費者加入購買直到特定的下架日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_AVAIL_FROM_TO", "EN", "Available date & time indicate when a product will be displayed and sold to consumer until a certain date & time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_CAT", "CHS", "商品的基本类别代表了商品的基本功能类别，所以每个商品只能有1个基本类别；专题类别则代表了商品的特徵，而每个商品可选最多5个专题类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_CAT", "CHT", "商品的基本類別代表了商品的基本功能類別，所以每個商品只能有1個基本類別；專題類別則代表了商品的特徵，而每個商品可選最多5個專題類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_CAT", "EN", "A Primary Category is the functional category of the product, a product can only have one primary category; a Merchandize Category is the feature category of a product, a product can have up to 5 merchandize categories"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_SHIP_INFO", "CHS", "包装後商品的重量、长度、宽度和高度，将用於计算运费"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_SHIP_INFO", "CHT", "包裝後商品的重量、長度、寬度和高度，將用於計算運費"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_SHIP_INFO", "EN", "The Weight, Length, Width and Height of the product when packaged to ship is displayed and used to calculate shipping costs"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_SKU", "CHS", "每个商品的SKU是一个独特的数字，产品供应商会分配给零售商品作识别商品。一个商品可以有多个条形码。 商品必须设置SKU库存来分配存货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_SKU", "CHT", "每個商品的SKU是一個獨特的數字，產品供應商會分配給零售商品作識別商品。一個商品可以有多個條形碼。 商品必須設置SKU庫存來分配存貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_SKU", "EN", "Unique SKU is a number assigned to retail merchandise that identifies both the product and the vendor that sells the product. A product can have several Barcodes. SKU Inventory must be set to distribute stock allocation"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_IMG", "CHS", "每个商品必须至少上传1张精选图片为店面默认列表图像，或1张颜色图片为不同颜色的商品图像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_IMG", "CHT", "每個商品必須至少上傳1張精選圖片為店面默認列表圖像，或1張顏色圖片為不同顏色的商品圖像"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_IMG", "EN", "A product must have at least 1 Featured Image as default listing image or Colour Image display when user picks an available color"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_PRIM_CAT_CREATE", "CHS", "商品的基本类别代表了商品的基本功能类别，所以每个商品只能有1个基本类别。请选择商品的基本类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_PRIM_CAT_CREATE", "CHT", "商品的基本類別代表了商品的基本功能類別，所以每個商品只能有1個基本類別。請選擇商品的基本類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_PRIM_CAT_CREATE", "EN", "A Primary Category is the functional category of the product, a product can only have one primary category. Please select a primary category for this product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_MERC_CAT_CREATE", "CHS", "专题类别则代表了商品的特徵，而每个商品可选最多5个专题类别。请选择商品的专题类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_MERC_CAT_CREATE", "CHT", "專題類別則代表了商品的特征，而每個商品可選最多5個專題類別。請選擇商品的專題類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_MERC_CAT_CREATE", "EN", "A Merchandize Category is the feature category of a product, a product can have up to 5 merchandize categories. Please select a merchandize category for this product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_CREATE_SKU", "CHS", "在每个SKU，每个系统颜色只能使用一次，您也可以选择不同尺码。每个尺码和颜色会变成商品的一个独立的SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_CREATE_SKU", "CHT", "在每個SKU，每個系統顏色只能使用一次，您也可以選擇不同尺碼。每個尺碼和顏色會變成商品的一個獨立的SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_PROD_CREATE_SKU", "EN", "For each SKU, each Color Code can only be used once. You can pick as many sizes as you need, each of the size and colour will become 1 SKU under the product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_PUBLISH", "CHS", "激活所有待处理的商品记录，它们将被显示在店面，并能够被消费者搜索及购买"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_PUBLISH", "CHT", "激活所有待處理的商品記錄，它們將被顯示在店面，並能夠被消費者搜索及購買"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_PUBLISH", "EN", "Activate all existing pending product records, they will be listed in storefront and able to be searched by consumers"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_IMG_UPLOAD", "CHS", "请为商品上传有正确命名格式的图片。您可以下载有详细说明的图像指南"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_IMG_UPLOAD", "CHT", "請為商品上傳有正確命名格式的圖片。您可以下載有詳細說明的圖像指南"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_PROD_IMG_UPLOAD", "EN", "Please upload Images for product with correct file naming format. You can download the image guideline for detailed instructions"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_SKU_INVENTORY_CREATE", "CHS", "SKU库存定义了商品在一个仓库中的库存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_SKU_INVENTORY_CREATE", "CHT", "SKU庫存定義了商品在一個倉庫中的庫存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_SKU_INVENTORY_CREATE", "EN", "A SKU inventory define the stock level of a product SKU in a warehouse"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_INVENTORY_ALLOCATION", "CHS", "存货配额是此SKU在这个库存位置的配额总数，库存配额可以是无限量的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_INVENTORY_ALLOCATION", "CHT", "存貨配額是此SKU在這個庫存位置的配額總數，庫存配額可以是無限量的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_TT_INVENTORY_ALLOCATION", "EN", "Allocation is the total number of the SKU that stored in this location, the stock could be unlimited"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_SKU_INVENTORY_EDIT", "CHS", "SKU库存定义了商品在一个仓库中的库存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_SKU_INVENTORY_EDIT", "CHT", "SKU庫存定義了商品在一個倉庫中的庫存量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_REM_SKU_INVENTORY_EDIT", "EN", "A SKU inventory define the stock level of a product SKU in a warehouse"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_INVENT_HIST_NOTE", "CHS", "您可以在这里看到SKU的库存更新历史记录，操作者和目前的可出售量 (ATS) 水平。请以前一个记录的数据作参照"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_INVENT_HIST_NOTE", "CHT", "您可以在這裡看到SKU的庫存更新歷史記錄，操作者和目前的可出售量 (ATS) 水平。請以前一個記錄的數據作參照"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_INVENT_HIST_NOTE", "EN", "You can see the Inventory update history of a SKU, who is responsible for the update and what is the current ATS level. Please refer to the previous figure of each record for former data"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_MERCHANT_CREATE", "CHS", "商家可以是品牌旗舰店，单品牌代理或多品牌代理。可以分配MM员工来管理商家及品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_MERCHANT_CREATE", "CHT", "商家可以是品牌旗舰店，單品牌代理或多品牌代理。可以分配MM員工來管理商家及品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_MERCHANT_CREATE", "EN", "A merchant can be a brand flagship store, mono-brand agent or multi-brand agent. MM user(s) will be assigned to manage a merchant and its brand(s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_MERCHANT_TYPE", "CHS", "单品牌代理商只能管理一个品牌，而多品牌代理商可以管理超过一个品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_MERCHANT_TYPE", "CHT", "單品牌代理商只能管理一個品牌，而多品牌代理商可以管理超過一個品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_MERCHANT_TYPE", "EN", "A mono-brand agent can only manage a single brand, whereas a multi-brand agent can manage more than 1 brand"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_BRAND_CREATE", "CHS", "创建後品牌将直接发布。创建新品牌时请填写表单的每个项目，请输入所有语言信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_BRAND_CREATE", "CHT", "創建後品牌將直接發佈。創建新品牌時請填寫表單的每個項目，請輸入所有語言信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_BRAND_CREATE", "EN", "Brand will be published directly after created. Please fill in all fields to create new brand, you can enter information in other languages if required"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_BRAND_SUBDOMAIN", "CHS", "品牌子域名可以随时更改 (域名必需是独一无二的)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_BRAND_SUBDOMAIN", "CHT", "品牌子域名可以隨時更改 (域名必需是獨一無二的)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_BRAND_SUBDOMAIN", "EN", "Brand sub-domain can be changed whenever as user sees fit, as long as it not already taken in the system"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_BRAND_LOGO", "CHS", "公司商标会显示在不同的页面，因此需要上传不同尺寸的图片文档。您可以下载有详细说明的图像指南"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_BRAND_LOGO", "CHT", "公司商標會顯示在不同的頁面，因此需要上傳不同尺寸的圖片文檔。您可以下載有詳細說明的圖像指南"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_BRAND_LOGO", "EN", "Company Logo will be displayed in different page therefore different sizes are required. You can download the image guideline for detail instructions"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_BRAND_EDIT", "CHS", "品牌的状态可由待处理更变为激活 (在店面发布) 或从使用中改为停用 (隐藏於店面)，留意相关商品将受到这个状态而影响"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_BRAND_EDIT", "CHT", "品牌的狀態可由待處理更變為激活 (在店面發佈) 或從使用中改為停用 (隱藏於店面)，留意相關商品將受到這個狀態而影響"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_REM_BRAND_EDIT", "EN", "The status of a brand can be changed from pending to active or active to inactive, related product will be affected by this change"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_CODE", "CHS", "类别代码会用作显示主类别和子类别的关系"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_CODE", "CHT", "類別代碼會用作顯示主類別和子類別的關係"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_CODE", "EN", "The Category Code is to show the relationship of a Parent Category and a Child Category"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_META_KEYWORD", "CHS", "自定义关键字提供标记，帮助告诉搜索引擎网页的主内容"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_META_KEYWORD", "CHT", "自定義關鍵字提供標記，幫助告訴搜索引擎網頁的主內容"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_META_KEYWORD", "EN", "Meta Keywords are a specific type of meta tag that help tell search engines what the topic of the page is"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_META_DESC", "CHS", "搜索字段描述介绍提供网页内容的简单解释"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_META_DESC", "CHT", "搜索字段描述介紹提供網頁內容的簡單解釋"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_META_DESC", "EN", "Meta Description provides concise explanations of the contents of web pages"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_PUBLISH", "CHS", "发布日期会影响类别在店面中的显示"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_PUBLISH", "CHT", "發佈日期會影響類別在店面中的顯示"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_TT_CATEGORY_PUBLISH", "EN", "Publish date affects the visibility of the category on storefront"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_MISSING_IMG_NOTE", "CHS", "每個商品必須至少上傳1張精選圖片為店面默認列表圖像，或1張颜色圖片為不同顏色的商品圖像。您可以在這裡檢視所有沒有上傳圖片的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_MISSING_IMG_NOTE", "CHT", "每個商品必須至少上傳1張精選圖片為店面默認列表圖像，或1張顏色圖片為不同顏色的商品圖像。您可以在這裡檢視所有沒有上傳圖片的商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACMC_MISSING_IMG_NOTE", "EN", "A Product must have at least 1 Featured image as default listing image or Colour image display when user picks an available color. You can see all existing pending Product without any image here for your reference"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GENDER_F", "CHS", "女"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GENDER_F", "CHT", "女"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GENDER_F", "EN", "Female"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GENDER_M", "CHS", "男"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GENDER_M", "CHT", "男"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_GENDER_M", "EN", "Male"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOBILE", "CHS", "手机号码"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOBILE", "CHT", "手機號碼"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOBILE", "EN", "Mobile No."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_LINK_ACC", "CHS", "帐户绑定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_LINK_ACC", "CHT", "帳戶綁定"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_LINK_ACC", "EN", "Linked Accounts"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALIPAY_ACC", "CHS", "支付宝帐户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALIPAY_ACC", "CHT", "支付寶帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ALIPAY_ACC", "EN", "Alipay Account"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_REVIEWS", "CHS", "给我好评"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_REVIEWS", "CHT", "給我好評"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_REVIEWS", "EN", "Reviews"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_CS", "CHS", "联络客服"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_CS", "CHT", "聯絡客服"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_CS", "EN", "Customer Service"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_LOGOUT", "CHS", "退出登录"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_LOGOUT", "CHT", "退出登錄"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PROF_LOGOUT", "EN", "Logout"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NUMBER_POSITIVE", "CHS", "{0}必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NUMBER_POSITIVE", "CHT", "{0}必须大于0"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_NUMBER_POSITIVE", "EN", "{0} must be greater than zero"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_INVALID", "CHS", "SKU代码格式错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_INVALID", "CHT", "SKU代碼格式錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_INVALID", "EN", "SKU Code is invalid"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BAR_INVALID", "CHS", "条形码格式错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BAR_INVALID", "CHT", "條形碼格式錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_BAR_INVALID", "EN", "Barcode is invalid"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_FOLLOWED", "CHS", "成功关注！"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_FOLLOWED", "CHT", "成功關注！"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_FOLLOWED", "EN", "Followed successfully!"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_INVALID", "CHS", "样式代码格式错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_INVALID", "CHT", "樣式代碼格式錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_STYLECODE_INVALID", "EN", "Style Code is invalid"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_INVALID", "CHS", "零售价格式错误"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_INVALID", "CHT", "零售價格式錯誤"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RETAIL_PRICE_INVALID", "EN", "Retail Price is invalid"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_FRIEND_REQ_SENT", "CHS", "成功发出朋友请求！"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_FRIEND_REQ_SENT", "CHT", "成功發出朋友請求！"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_FRIEND_REQ_SENT", "EN", "Sent friend request successfully!"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_UNFOLLOWED", "CHS", "成功取消关注！"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_UNFOLLOWED", "CHT", "成功取消關注！"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_SUC_UNFOLLOWED", "EN", "Unfollowed successfully!"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_PROFILE_BANNER", "CHS", "信息封面图"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_PROFILE_BANNER", "CHT", "信息封面圖"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BRAND_PROFILE_BANNER", "EN", "Profile Banner Image"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_BRAND_PROFILE", "CHS", "品牌基本信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_BRAND_PROFILE", "CHT", "品牌基本信息"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BASIC_BRAND_PROFILE", "EN", "Brand Basic Profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRIVACY_POLICY", "CHS", "私隐政策"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRIVACY_POLICY", "CHT", "私隱政策"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PRIVACY_POLICY", "EN", "Privacy Policy"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COOKIE_POLICY", "CHS", "Cookies 政策"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COOKIE_POLICY", "CHT", "Cookies 政策"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COOKIE_POLICY", "EN", "Cookies Policy"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COPYRIGHT_POLICY", "CHS", "版权政策"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COPYRIGHT_POLICY", "CHT", "版權政策"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COPYRIGHT_POLICY", "EN", "Copyright Policy"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOVE2WISHLIST_SUCCESS", "CHS", "移动成功，在心愿单等亲~"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOVE2WISHLIST_SUCCESS", "CHT", "移動成功，在心願單等親~"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOVE2WISHLIST_SUCCESS", "EN", "Moved to wishlist successfully~"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOVE2WISHLIST_FAILED", "CHS", "移动不成功，再试试亲~"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOVE2WISHLIST_FAILED", "CHT", "移動不成功，再試試親~"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MOVE2WISHLIST_FAILED", "EN", "Failed moving item to wishlist, please try again~"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_AVAIL_FROM_TO", "CHS", "上架日期时间决定了商品将何时被显示於店面让消费者加入购买直到特定的下架日期时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_AVAIL_FROM_TO", "CHT", "上架日期時間決定了商品將何時被顯示於店面讓消費者加入購買直到特定的下架日期時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_AVAIL_FROM_TO", "EN", "Available date & time indicate when a product will be displayed and sold to consumer until a certain date & time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_SHIP_INFO", "CHS", "包装後商品的重量、长度、宽度和高度，将用於计算运费"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_SHIP_INFO", "CHT", "包裝後商品的重量、長度、寬度和高度，將用於計算運費"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_SHIP_INFO", "EN", "The Weight, Length, Width and Height of the product when packaged to ship is displayed and used to calculate shipping costs"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_PRIM_CAT_CREATE", "CHS", "商品的基本类别代表了商品的基本功能类别，所以每个商品只能有1个基本类别。请选择商品的基本类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_PRIM_CAT_CREATE", "CHT", "商品的基本類別代表了商品的基本功能類別，所以每個商品只能有1個基本類別。請選擇商品的基本類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_PRIM_CAT_CREATE", "EN", "A Primary Category is the functional category of the product, a product can only have one primary category. Please select a primary category for this product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_MERC_CAT_CREATE", "CHS", "专题类别则代表了商品的特徵，而每个商品可选最多5个专题类别。请选择商品的专题类别"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_MERC_CAT_CREATE", "CHT", "專題類別則代表了商品的特徵，而每個商品可選最多5個專題類別。請選擇商品的專題類別"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_MERC_CAT_CREATE", "EN", "A Merchandize Category is the feature category of a product, a product can have up to 5 merchandize categories. Please select a merchandize category for this product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_CREATE_SKU", "CHS", "在每个SKU，每个系统颜色只能使用一次，您也可以选择不同尺码。每个尺码和颜色会变成商品的一个独立的SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_CREATE_SKU", "CHT", "在每個SKU，每個系統顏色只能使用一次，您也可以選擇不同尺碼。每個尺碼和顏色會變成商品的一個獨立的SKU"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TT_PROD_CREATE_SKU", "EN", "For each SKU, each Color Code can only be used once. You can pick as many sizes as you need, each of the size and colour will become 1 SKU under the product"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_TT_INVENTORY_ALLOCATION", "CHS", "存货配额是此SKU在这个库存位置的配额总数，库存配额可以是无限量的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_TT_INVENTORY_ALLOCATION", "CHT", "存貨配額是此SKU在這個庫存位置的配額總數，庫存配額可以是無限量的"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MC_TT_INVENTORY_ALLOCATION", "EN", "Allocation is the total number of the SKU that stored in this location, the stock could be umlimited"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ADMIN_GENERAL_ERR", "CHS", "发生系统错误，请联系管理员"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ADMIN_GENERAL_ERR", "CHT", "發生系統錯誤，請聯繫管理員"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_ADMIN_GENERAL_ERR", "EN", "Error occurs in the system, please contact administrator"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CART", "CHS", "购物车系统错误。请重试。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CART", "CHT", "購物車系統錯誤。請重試。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CART", "EN", "Shopping cart system error. Please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEW_SHIPPING_ADDR", "CHS", "新建收货地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEW_SHIPPING_ADDR", "CHT", "新建收貨地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_NEW_SHIPPING_ADDR", "EN", "New Shipping Address"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_RECEIVER_NIL", "CHS", "请输入收货人姓名。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_RECEIVER_NIL", "CHT", "請輸入收貨人姓名。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_RECEIVER_NIL", "EN", "Please enter name of receiver."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECEIVER_NAME", "CHS", "收货人姓名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECEIVER_NAME", "CHT", "收貨人姓名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECEIVER_NAME", "EN", "Name of Receiver"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HOME_WELCOME", "CHS", "欢迎"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HOME_WELCOME", "CHT", "歡迎"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_HOME_WELCOME", "EN", "Welcome Back"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ORDER_CONFIRMATION", "CHS", "订单确认"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ORDER_CONFIRMATION", "CHT", "訂單確認"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ORDER_CONFIRMATION", "EN", "Order Confirmation"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FAPIAO_TITLE", "CHS", "发票抬头"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FAPIAO_TITLE", "CHT", "發票抬頭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_FAPIAO_TITLE", "EN", "Fapiao Title"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_QTYATS_NIL", "CHS", "SKU安全库存量是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_QTYATS_NIL", "CHT", "SKU安全庫存量是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_QTYATS_NIL", "EN", "QtyAts is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADDR_PICKER_PLACEHOLDER", "CHS", "省、市、区"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADDR_PICKER_PLACEHOLDER", "CHT", "省、市、區"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADDR_PICKER_PLACEHOLDER", "EN", "Province, City, Region"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STREET_ADDR", "CHS", "街道"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STREET_ADDR", "CHT", "街道"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_STREET_ADDR", "EN", "Street"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DETAIL_STREET_ADDR", "CHS", "详细地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DETAIL_STREET_ADDR", "CHT", "詳細地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DETAIL_STREET_ADDR", "EN", "Detail Address"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MY_ADDRESS", "CHS", "我的地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MY_ADDRESS", "CHT", "我的地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MY_ADDRESS", "EN", "My Address"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD_ADDR", "CHS", "添加地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD_ADDR", "CHT", "添加地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_ADD_ADDR", "EN", "Add New Address"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESEND_REG_FAIL", "CHS", "无法发送注册链接, 请稍后再试。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESEND_REG_FAIL", "CHT", "無法發送註冊鏈接, 請稍後再試。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESEND_REG_FAIL", "EN", "Failed to resend registration link. Please try again later."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESEND_CHNG_EMAIL_FAIL", "CHS", "无法重新发送更改电子邮件链接, 请稍后再试。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESEND_CHNG_EMAIL_FAIL", "CHT", "無法重新發送更改電子郵件鏈接, 請稍後再試。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_RESEND_CHNG_EMAIL_FAIL", "EN", "Failed to resend change email link. Please try again later."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_NIL", "CHS", "SKU代码是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_NIL", "CHT", "SKU代碼是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SKU_NIL", "EN", "SKU code is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_CURATOR_PROFIX", "CHS", "个人气美范"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_CURATOR_PROFIX", "CHT", "個人氣美範"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_CURATOR_PROFIX", "EN", "hotest Curators"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_CURATOR_PREFIX", "CHS", "已为您推选了"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_CURATOR_PREFIX", "CHT", "已為您推選了"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_CURATOR_PREFIX", "EN", "We recommended"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_MERCHANT_PREFIX", "CHS", "已为您推选了"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_MERCHANT_PREFIX", "CHT", "已為您推選了"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_MERCHANT_PREFIX", "EN", "We recommended"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_MERCHANT_PROFIX", "CHS", "热卖品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_MERCHANT_PROFIX", "CHT", "熱賣品牌"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_RECOMMENDED_MERCHANT_PROFIX", "EN", "hotest Brands"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SAVE", "CHS", "保存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SAVE", "CHT", "保存"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SAVE", "EN", "Save"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SUBMIT_ORDER", "CHS", "提交订单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SUBMIT_ORDER", "CHT", "提交訂單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SUBMIT_ORDER", "EN", "Submit"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_LOCATION_ACTIVATE", "CHS", "库存位置「{0}」将被激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_LOCATION_ACTIVATE", "CHT", "庫存位置「{0}」將被激活"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_LOCATION_ACTIVATE", "EN", "Inventory location \"{0}\" will be activated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_LOCATION_INACTIVATE", "CHS", "库存位置「{0}」将被停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_LOCATION_INACTIVATE", "CHT", "庫存位置「{0}」將被停用"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_LOCATION_INACTIVATE", "EN", "Inventory location \"{0}\" will be inactivated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONFIRM_PAYMENT", "CHS", "去支付"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONFIRM_PAYMENT", "CHT", "去支付"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CONFIRM_PAYMENT", "EN", "Pay"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_LOCATION", "CHS", "将删除库存位置「{0}」。这个动作无法复原"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_LOCATION", "CHT", "將刪除庫存位置「{0}」。這個動作無法復原"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_CONF_DEL_LOCATION", "EN", "Inventory location \"{0}\" will be deleted. This action cannot be undone"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_PASSWORD_UNLOCK", "CHS", "请输入密码以解封账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_PASSWORD_UNLOCK", "CHT", "請輸入密碼以解封帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_FILL_PASSWORD_UNLOCK", "EN", "Please fill in the password in order to unlock your account"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_UNLOCK_ACCOUNT_TIPS", "CHS", "账户已经成功解封"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_UNLOCK_ACCOUNT_TIPS", "CHT", "賬戶已經成功解封"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_UNLOCK_ACCOUNT_TIPS", "EN", "Account is unlocked successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_UNLOCK_ACCOUNT", "CHS", "解封账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_UNLOCK_ACCOUNT", "CHT", "解封帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_UNLOCK_ACCOUNT", "EN", "Account Unlock"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_COMPLETE_REGISTRATION", "CHS", "完成注册"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_COMPLETE_REGISTRATION", "CHT", "完成註冊"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_COMPLETE_REGISTRATION", "EN", "Complete Registration"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE_SENT_2", "CHS", "秒)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE_SENT_2", "CHT", "秒)"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SR_REQUEST_VERCODE_SENT_2", "EN", "s)"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_WISHLIST_ITEM_SUCCESS", "CHS", "商品删除成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_WISHLIST_ITEM_SUCCESS", "CHT", "商品刪除成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_WISHLIST_ITEM_SUCCESS", "EN", "Item removed successfully"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_WISHLIST_ITEM_FAILED", "CHS", "商品删除不成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_WISHLIST_ITEM_FAILED", "CHT", "商品刪除不成功"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_DEL_WISHLIST_ITEM_FAILED", "EN", "Failed removing item"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_SWIPE2PAY_ADDR", "CHS", "请输入送货地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_SWIPE2PAY_ADDR", "CHT", "請輸入送貨地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_SWIPE2PAY_ADDR", "EN", "Please enter a shipping address"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_SWIPE2PAY_PAYMENT_METHOD", "CHS", "请输入支付方法"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_SWIPE2PAY_PAYMENT_METHOD", "CHT", "請輸入支付方法"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_SWIPE2PAY_PAYMENT_METHOD", "EN", "Please enter a payment method"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PDP_SWIPE2PAY_PURCHASE", "CHS", "立即购买"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PDP_SWIPE2PAY_PURCHASE", "CHT", "立即購買"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PDP_SWIPE2PAY_PURCHASE", "EN", "Purchase"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INTEREST_KEYWORDS_CONT", "CHS", "继续"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INTEREST_KEYWORDS_CONT", "CHT", "繼續"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_INTEREST_KEYWORDS_CONT", "EN", "Continue"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BR_INFO", "CHS", "营业执照资料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BR_INFO", "CHT", "營業執照資料"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_BR_INFO", "EN", "Business Registration (BR) Information"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_UPDATE_CATEGORY_FAIL", "CHS", "编辑类别失败，请重试"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_UPDATE_CATEGORY_FAIL", "CHT", "編輯類別失敗，請重試"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_AC_UPDATE_CATEGORY_FAIL", "EN", "Failed to update category, please try again"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_EXPORT_NOT_FOUND", "CHS", "没有库存记录可下载"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_EXPORT_NOT_FOUND", "CHT", "沒有庫存記錄可下載"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_EXPORT_NOT_FOUND", "EN", "There is no inventory record to export"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_IMPORT", "CHS", "库存纪录上传失败"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_IMPORT", "CHT", "庫存紀錄上傳失敗"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INVENTORY_IMPORT", "EN", "Failed to import inventory record"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_CREATE", "CHS", "成功创建库存：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_CREATE", "CHT", "成功創建庫存：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_CREATE", "EN", "Inventories created: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_OVERWRITE", "CHS", "成功更新库存：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_OVERWRITE", "CHT", "成功更新庫存：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_OVERWRITE", "EN", "Inventories updated: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_SKIPPED", "CHS", "被略过库存: {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_SKIPPED", "CHT", "被略過庫存: {0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_SKIPPED", "EN", "Inventories skipped: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_FAILED", "CHS", "上传失败纪录：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_FAILED", "CHT", "上傳失敗紀錄：{0}"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_IMPORT_INVENTORY_FAILED", "EN", "Record failed to upload: {0}"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_WEIGHT", "CHS", "重量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_WEIGHT", "CHT", "重量"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_WEIGHT", "EN", "Weight"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_SHIPPER", "CHS", "快递"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_SHIPPER", "CHT", "快遞"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_SHIPPER", "EN", "Shipper"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_SHIPPING_COST", "CHS", "运费"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_SHIPPING_COST", "CHT", "運費"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_PI_SHIPPING_COST", "EN", "Shipping Cost"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MERCHANT_TOTAL", "CHS", "本店合计"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MERCHANT_TOTAL", "CHT", "本店合計"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_MERCHANT_TOTAL", "EN", "Subtotal"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CHECKOUT_TOTAL", "CHS", "合计"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CHECKOUT_TOTAL", "CHT", "合計"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_CHECKOUT_TOTAL", "EN", "Total"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GET_FLASH", "CHS", "下载 Flash"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GET_FLASH", "CHT", "下載 Flash"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_GET_FLASH", "EN", "Get Flash"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INSTALL_FLASH", "CHS", "您需要安装Flash来操作某些功能"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INSTALL_FLASH", "CHT", "您需要安裝Flash來操作某些功能"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_INSTALL_FLASH", "EN", "You need to install Flash for some functions to operate"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SORRY", "CHS", "抱歉..."); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SORRY", "CHT", "抱歉..."); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SORRY", "EN", "Sorry..."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_INVALID_CREDENTIALS", "CHS", "无效的用户名/移动或密码，请重试。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_INVALID_CREDENTIALS", "CHT", "無效的用戶名/移動或密碼，請重試。"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_CA_INVALID_CREDENTIALS", "EN", "Invalid username/mobile or password, please try again."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_ORDERS", "CHS", "全商户订单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_ORDERS", "CHT", "全商戶訂單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_ORDERS", "EN", "All Merchant Orders"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER_NO", "CHS", "订单号"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER_NO", "CHT", "訂單號"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER_NO", "EN", "Order No."); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER_DATE", "CHS", "订单日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER_DATE", "CHT", "訂單日期"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER_DATE", "EN", "Order Date"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CUSTOMER_NAME", "CHS", "客户姓名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CUSTOMER_NAME", "CHT", "客戶姓名"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CUSTOMER_NAME", "EN", "Customer Name"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER_STATUS", "CHS", "订单状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER_STATUS", "CHT", "訂單狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER_STATUS", "EN", "Order Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAYMENT_STATUS", "CHS", "付款状态"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAYMENT_STATUS", "CHT", "付款狀態"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAYMENT_STATUS", "EN", "Payment Status"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AMOUNT", "CHS", "金额"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AMOUNT", "CHT", "金額"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AMOUNT", "EN", "Amount"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_ORDER", "CHS", "搜寻订单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_ORDER", "CHT", "搜尋訂單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SEARCH_ORDER", "EN", "Search for Order"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATED", "CHS", "已创建"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATED", "CHT", "已創建"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATED", "EN", "Created"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRMED", "CHS", "已确认"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRMED", "CHT", "已確認"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONFIRMED", "EN", "Confirmed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SHIPPED", "CHS", "已配送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SHIPPED", "CHT", "已配送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SHIPPED", "EN", "Shipped"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PART_SHIPPED", "CHS", "部分配送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PART_SHIPPED", "CHT", "部分配送"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PART_SHIPPED", "EN", "Partial Shipped"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CLOSED", "CHS", "已关闭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CLOSED", "CHT", "已關閉"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CLOSED", "EN", "Closed"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPIRED", "CHS", "已逾时"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPIRED", "CHT", "已逾時"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EXPIRED", "EN", "Expired"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RECEIVED", "CHS", "已收货"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RECEIVED", "CHT", "已收貨"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RECEIVED", "EN", "Received"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAIDINFULL", "CHS", "全数已付"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAIDINFULL", "CHT", "全數已付"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAIDINFULL", "EN", "Paid in Full"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UNPAID", "CHS", "未付款"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UNPAID", "CHT", "未付款"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_UNPAID", "EN", "Unpaid"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COD", "CHS", "货到付款"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COD", "CHT", "貨到付款"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COD", "EN", "COD"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGE_NOT_FOUND", "CHS", "页面不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGE_NOT_FOUND", "CHT", "頁面不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_PAGE_NOT_FOUND", "EN", "Page Not Found"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PAGE_NOT_FOUND", "CHS", "你所访问的页面不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PAGE_NOT_FOUND", "CHT", "你所訪問的頁面不存在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_PAGE_NOT_FOUND", "EN", "The URL you requested was not found in this server"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK_TO_HOME", "CHS", "回到主页"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK_TO_HOME", "CHT", "回到主頁"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_BACK_TO_HOME", "EN", "Back to homepage"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_FROM", "CHS", "开始"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_FROM", "CHT", "開始"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_FROM", "EN", "From"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TO", "CHS", "结束"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TO", "CHT", "結束"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TO", "EN", "To"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_AVAIL_FROM_NIL", "CHS", "销售价上架日期时间是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_AVAIL_FROM_NIL", "CHT", "銷售價上架日期時間是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_AVAIL_FROM_NIL", "EN", "Sale Price Available From (Date & Time) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_ AVAIL_TO_NIL", "CHS", "销售价下架日期时间是必填项"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_ AVAIL_TO_NIL", "CHT", "銷售價下架日期時間是必填項"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_SALE_ AVAIL_TO_NIL", "EN", "Sale Price Available To (Date & Time) is a mandatory field"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_ORDER", "CHS", "编辑订单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_ORDER", "CHT", "編輯訂單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_EDIT_ORDER", "EN", "Edit Order"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_PRINT", "CHS", "打印"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_PRINT", "CHT", "打印"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_PRINT", "EN", "Print"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_CANCEL_ITEMS", "CHS", "取消货品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_CANCEL_ITEMS", "CHT", "取消貨品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("BTN_CANCEL_ITEMS", "EN", "Cancel Items"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INITATED", "CHS", "发起"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INITATED", "CHT", "發起"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INITATED", "EN", "Initiated"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER", "CHS", "订单"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER", "CHT", "訂單"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ORDER", "EN", "Order"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SHIPPING_ADDRESS", "CHS", "送货地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SHIPPING_ADDRESS", "CHT", "送货地址"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_SHIPPING_ADDRESS", "EN", "Shipping Address"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVOICE_TITLE", "CHS", "发票抬头"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVOICE_TITLE", "CHT", "發票抬頭"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INVOICE_TITLE", "EN", "Invoice Title"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_WISHLIST_PROD", "CHS", "心愿商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_WISHLIST_PROD", "CHT", "心願商品"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_WISHLIST_PROD", "EN", "Wishlist"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHARE_USER_PROD", "CHS", "分享用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHARE_USER_PROD", "CHT", "分享用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_SHARE_USER_PROD", "EN", "Share this profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REPORT_USER", "CHS", "举报用户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REPORT_USER", "CHT", "舉報用戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CA_REPORT_USER", "EN", "Report this profile"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TODAY", "CHS", "今天"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TODAY", "CHT", "今天"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_TODAY", "EN", "Today"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_NOW", "CHS", "现在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_NOW", "CHT", "現在"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DATE_NOW", "EN", "Now"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TIME", "CHS", "时间"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TIME", "CHT", "時間"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TIME", "EN", "Time"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_MERC_USER", "CHS", "激活商户账户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_MERC_USER", "CHT", "激活商戶帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ACTIVATE_MERC_USER", "EN", "Activate Merchant"); 

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_MERC_USER", "CHS", "暂停商户帐户"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_MERC_USER", "CHT", "暫停商戶帳戶"); 
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_INACTIVATE_MERC_USER", "EN", "Inactivate Merchant"); 

