-- set category's IsMerchCanSelect to 1, otherwise in product edit page, category select option would show nothing.
-- for IsMerchCanSelect, it can be set in category edit page.
UPDATE Category SET IsMerchCanSelect=1;