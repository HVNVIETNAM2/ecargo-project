ALTER TABLE `User`
ADD COLUMN `GeoCountryId` int(11) NOT NULL DEFAULT 0 AFTER `FollowingBrandCount`,
ADD COLUMN `GeoProvinceId` int(11) NOT NULL DEFAULT 0 AFTER `GeoCountryId`,
ADD COLUMN `GeoCityId` int(11) NOT NULL DEFAULT 0 AFTER `GeoProvinceId`,
ADD COLUMN `Gender` varchar(50) DEFAULT NULL AFTER `GeoCityId`,
ADD COLUMN `DateOfBirth` date DEFAULT NULL AFTER `Gender`;
