-- MySQL dump 10.16  Distrib 10.1.9-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: maymay
-- ------------------------------------------------------
-- Server version	10.1.9-MariaDB-1~trusty-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `TagCulture`
--

DROP TABLE IF EXISTS `TagCulture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TagCulture` (
  `TagCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `TagId` int(11) NOT NULL DEFAULT '0',
  `CultureCode` varchar(50) DEFAULT NULL,
  `TagName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`TagCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TagCulture`
--

LOCK TABLES `TagCulture` WRITE;
/*!40000 ALTER TABLE `TagCulture` DISABLE KEYS */;
INSERT INTO `TagCulture` VALUES (37,1,'CHS','潮流女装'),(38,1,'CHT','潮流女裝'),(39,1,'EN','Women\'s Fashion'),(40,2,'CHS','时尚男装'),(41,2,'CHT','時尚男裝'),(42,2,'EN','Men\'s Fashion'),(43,3,'CHS','流行女鞋'),(44,3,'CHT','流行女鞋'),(45,3,'EN','Popular shoes'),(46,4,'CHS','流行女包'),(47,4,'CHT','流行女包'),(48,4,'EN','Popular handbags'),(49,5,'CHS','性感内衣'),(50,5,'CHT','性感內衣'),(51,5,'EN','Sexy Lingerie'),(52,6,'CHS','服饰配件'),(53,6,'CHT','服飾配件'),(54,6,'EN','Clothes accessory'),(55,7,'CHS','珠宝首饰'),(56,7,'CHT','珠寶首飾'),(57,7,'EN','Jewelry'),(58,8,'CHS','时尚饰品'),(59,8,'CHT','時尚飾品'),(60,8,'EN','Fashion accessories'),(61,9,'CHS','精品手表'),(62,9,'CHT','精品手錶'),(63,9,'EN','Gifts Watches'),(64,10,'CHS','潮流眼镜'),(65,10,'CHT','潮流眼鏡'),(66,10,'EN','Glasses'),(67,11,'CHS','运动服包'),(68,11,'CHT','運動服包'),(69,11,'EN','Sportswear'),(70,12,'CHS','户外野营'),(71,12,'CHT','戶外野營'),(72,12,'EN','Outdoor camping'),(73,13,'CHS','健身球迷'),(74,13,'CHT','健身球迷'),(75,13,'EN','Fitness fans'),(76,14,'CHS','智能生活'),(77,14,'CHT','智能生活'),(78,14,'EN','Smart Life'),(79,15,'CHS','影音娱乐'),(80,15,'CHT','影音娛樂'),(81,15,'EN','Entertainment'),(82,16,'CHS','童装配饰'),(83,16,'CHT','童裝配飾'),(84,16,'EN','Kids Accessories'),(85,17,'CHS','婴幼用品'),(86,17,'CHT','嬰幼用品'),(87,17,'EN','Baby Supplies'),(88,18,'CHS','美容护肤'),(89,18,'CHT','美容護膚'),(90,18,'EN','Beauty'),(91,19,'CHS','男士护肤'),(92,19,'CHT','男士護膚'),(93,19,'EN','Men\'s Skin Care'),(94,20,'CHS','纤体塑身'),(95,20,'CHT','纖體塑身'),(96,20,'EN','Slimming'),(97,21,'CHS','超值彩妆'),(98,21,'CHT','超值彩妝'),(99,21,'EN','Value makeup'),(100,22,'CHS','美发造型'),(101,22,'CHT','美髮造型'),(102,22,'EN','Hair styling');
/*!40000 ALTER TABLE `TagCulture` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-29  3:33:46
