DROP TABLE IF EXISTS `OrderAddress`;

DROP TABLE IF EXISTS `Order`;
CREATE TABLE IF NOT EXISTS `Order` (
  `OrderId` int(11) NOT NULL AUTO_INCREMENT,
  `OrderKey` varchar(36) NOT NULL,
  `UserId` int(11) DEFAULT '0',
  `UserIdentificationKey` varchar(50) DEFAULT NULL,
  `OrderStatusId` int(11) NOT NULL,
  `NetTotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `GrossTotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `CultureCode` varchar(5) NOT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrderId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `OrderMerchant`;
CREATE TABLE IF NOT EXISTS `OrderMerchant` (
  `OrderMerchantId` int(11) NOT NULL AUTO_INCREMENT,
  `OrderMerchantKey` varchar(36) NOT NULL,
  `OrderId` int(11) NOT NULL DEFAULT '0',
  `MerchantId` int(11) NOT NULL DEFAULT '0',
  `OrderMerchantStatusId` int(11) NOT NULL,
  `UserIdentificationKey` varchar(50) DEFAULT NULL,
  `NetTotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `GrossTotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `IsTaxInvoiceRequested` tinyint(1) DEFAULT '0',
  `TaxInvoiceName` varchar(50) DEFAULT NULL,
  `TaxInvoiceNumber` varchar(50) DEFAULT NULL,
  `RecipientName` varchar(255) NOT NULL,
  `PhoneCode` varchar(50) DEFAULT NULL,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `GeoCountryId` int(11) NOT NULL DEFAULT '0',
  `GeoProvinceId` int(11) NOT NULL DEFAULT '0',
  `GeoCityId` int(11) NOT NULL DEFAULT '0',
  `Country` varchar(50) DEFAULT NULL,
  `Province` varchar(50) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `District` varchar(50) DEFAULT NULL,
  `PostalCode` varchar(50) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `CultureCode` varchar(5) DEFAULT NULL,
  `LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrderMerchantId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `OrderMerchantItem`;
CREATE TABLE IF NOT EXISTS `OrderMerchantItem` (
  `OrderMerchantItemId` int(11) NOT NULL AUTO_INCREMENT,
  `OrderMerchantId` int(11) NOT NULL DEFAULT '0',
  `OrderId` int(11) NOT NULL DEFAULT '0',
  `MerchantId` int(11) NOT NULL DEFAULT '0',
  `QtyOrdered` int(11) NOT NULL DEFAULT '0',
  `QtyAllocated` int(11) NOT NULL DEFAULT '0',
  `QtyShipped` int(11) NOT NULL DEFAULT '0',
  `SkuId` int(11) NOT NULL DEFAULT '0',
  `SkuCode` varchar(255) DEFAULT NULL,
  `StyleCode` varchar(255) DEFAULT NULL,
  `Barcode` varchar(255) DEFAULT NULL,
  `BrandId` int(11) NOT NULL DEFAULT '0',
  `SizeId` int(11) NOT NULL DEFAULT '0',
  `ColorId` int(11) NOT NULL DEFAULT '0',
  `ColorKey` varchar(255) DEFAULT NULL,
  `PriceRetail` decimal(10,2) NOT NULL DEFAULT '0.00',
  `PriceSale` decimal(10,2) NOT NULL DEFAULT '0.00',
  `WeightKg` int(11) NOT NULL DEFAULT '0',
  `HeightCm` int(11) NOT NULL DEFAULT '0',
  `WidthCm` int(11) NOT NULL DEFAULT '0',
  `LengthCm` int(11) NOT NULL DEFAULT '0',
  `UnitPrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `NetTotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `GrossTotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrderMerchantItemId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `OrderStatus`;
CREATE TABLE IF NOT EXISTS `OrderStatus` (
  `OrderStatusId` int(11) NOT NULL AUTO_INCREMENT,
  `OrderStatusNameInvariant` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`OrderStatusId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `OrderStatusCulture`;
CREATE TABLE IF NOT EXISTS `OrderStatusCulture` (
  `OrderStatusCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `OrderStatusId` int(11) NOT NULL DEFAULT '0',
  `CultureCode` varchar(5) DEFAULT NULL,
  `OrderStatusName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`OrderStatusCultureId`),
  KEY `OrderStatusId` (`OrderStatusId`),
  KEY `CultureCode` (`CultureCode`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `OrderMerchantStatus`;
CREATE TABLE IF NOT EXISTS `OrderMerchantStatus` (
  `OrderMerchantStatusId` int(11) NOT NULL AUTO_INCREMENT,
  `OrderMerchantStatusNameInvariant` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`OrderMerchantStatusId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `OrderMerchantStatusCulture`;
CREATE TABLE IF NOT EXISTS `OrderMerchantStatusCulture` (
  `OrderMerchantStatusCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `OrderMerchantStatusId` int(11) NOT NULL DEFAULT '0',
  `CultureCode` varchar(5) DEFAULT NULL,
  `OrderMerchantStatusName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`OrderMerchantStatusCultureId`),
  KEY `OrderMerchantStatusId` (`OrderMerchantStatusId`),
  KEY `CultureCode` (`CultureCode`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

INSERT INTO `OrderStatus` (`OrderStatusId`, `OrderStatusNameInvariant`) VALUES
	(1, 'Closed'),
	(2, 'Open'),
	(3, 'Initiated');

INSERT INTO `OrderStatusCulture` (`OrderStatusCultureId`, `OrderStatusId`, `CultureCode`, `OrderStatusName`) VALUES
	(1, 1, 'EN', 'Closed'),
	(2, 1, 'CHT', '關閉'),
	(3, 1, 'CHS', '关闭'),
	(4, 2, 'EN', 'Open'),
	(5, 2, 'CHS', '开放'),
	(6, 2, 'CHT', '開放'),
	(7, 3, 'EN', 'Initiated'),
	(8, 3, 'CHS', '启动'),
	(9, 3, 'CHT', '啟動');

INSERT INTO `OrderMerchantStatus` (`OrderMerchantStatusId`, `OrderMerchantStatusNameInvariant`) VALUES
	(1, 'Closed'),
	(2, 'Confirmed'),
	(3, 'Created'),
	(4, 'Cancelled'),
	(5, 'Expired'),
	(6, 'Shipped'),
	(7, 'Received'),
	(8, 'Partial Shipped');
