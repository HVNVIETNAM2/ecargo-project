DROP TABLE IF EXISTS `RelationshipType`;
CREATE TABLE `RelationshipType` (
	`RelationshipTypeId` int(11) NOT NULL AUTO_INCREMENT,
	`RelationshipTypeName` VARCHAR(50),
  	PRIMARY KEY (`RelationshipTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `UserUserRelationship`;
CREATE TABLE `UserUserRelationship` (
	`UserUserRelationshipId` int(11) NOT NULL AUTO_INCREMENT,
	`FromUserId` int(11) NOT NULL,
	`ToUserId` int(11) NOT NULL,
	`RelationshipTypeId` int(11) NOT NULL,
	`StatusId` int(11) NOT NULL,
	`LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  	`LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  	PRIMARY KEY (`UserUserRelationshipId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `UserMerchantRelationship`;
CREATE TABLE `UserMerchantRelationship` (
	`UserMerchantRelationshipId` int(11) NOT NULL AUTO_INCREMENT,
	`FromUserId` int(11) NOT NULL,
	`ToMerchantId` int(11) NOT NULL,
	`LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  	`LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  	PRIMARY KEY (`UserMerchantRelationshipId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `UserBrandRelationship`;
CREATE TABLE `UserBrandRelationship` (
	`UserBrandRelationshipId` int(11) NOT NULL AUTO_INCREMENT,
	`FromUserId` int(11) NOT NULL,
	`ToBrandId` int(11) NOT NULL,
	`LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  	`LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  	PRIMARY KEY (`UserBrandRelationshipId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;



INSERT INTO `RelationshipType` (`RelationshipTypeId`,`RelationshipTypeName`) VALUES (1, 'Follow');
INSERT INTO `RelationshipType` (`RelationshipTypeId`,`RelationshipTypeName`) VALUES (2, 'Friend');