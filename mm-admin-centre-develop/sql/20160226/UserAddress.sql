DROP TABLE IF EXISTS `UserAddress`;
CREATE TABLE `UserAddress` (
  `UserAddressId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `RecipientName` varchar(255) NOT NULL,
  `PhoneCode` varchar(50) DEFAULT NULL,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `GeoCountryId` int(11) NOT NULL DEFAULT '0',
  `GeoProvinceId` int(11) NOT NULL DEFAULT '0',
  `GeoCityId` int(11) NOT NULL DEFAULT '0',
  `Country` varchar(50) DEFAULT NULL,
  `Province` varchar(50) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `District` varchar(50) DEFAULT NULL,
  `PostalCode` varchar(50) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserAddressId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

ALTER TABLE `User` ADD `DefaultUserAddressId` INT DEFAULT 0 AFTER `InventoryLocationId`;
