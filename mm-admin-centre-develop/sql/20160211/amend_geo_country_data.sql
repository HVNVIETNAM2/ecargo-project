UPDATE GeoCountryCulture SET GeoCountryName = '瓜德羅普島' WHERE GeoCountryId = 87 AND CultureCode = 'CHT';
UPDATE GeoCountryCulture SET GeoCountryName = '瓜德罗普岛' WHERE GeoCountryId = 87 AND CultureCode = 'CHS';

-- Delete Burma
DELETE FROM GeoCountryCulture WHERE GeoCountryId = 32;
DELETE FROM GeoCountry WHERE GeoCountryId = 32;
