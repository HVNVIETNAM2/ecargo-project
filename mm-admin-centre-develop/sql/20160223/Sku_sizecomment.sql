ALTER TABLE `mm`.`Sku`
ADD COLUMN `SkuSizeCommentInvariant` TEXT NULL DEFAULT NULL AFTER `SkuMaterialInvariant`;


ALTER TABLE `mm`.`SkuCulture`
ADD COLUMN `SkuSizeComment` TEXT NULL DEFAULT NULL AFTER `SkuMaterial`;


