ALTER TABLE `User` ADD COLUMN `InventoryLocationId` INT NULL DEFAULT NULL AFTER `MerchantId`;
ALTER TABLE `User` CHANGE COLUMN `InventoryLocationId` `InventoryLocationId` INT(11) NULL DEFAULT '0' AFTER `MerchantId`;
update User set InventoryLocationId=0;