INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_COMPLETE_REGISTRATION_TITLE", "EN", "Resend Registration Code");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_COMPLETE_REGISTRATION_TITLE", "CHS", "Resend Registration Code-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_USER_COMPLETE_REGISTRATION_TITLE", "CHT", "Resend Registration Code-CHT");


INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "EN", "You are going to resend registration code by SMS to");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "CHS", "You are going to resend registration code by SMS to-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "CHT", "You are going to resend registration code by SMS to-CHT");


INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PASSWORD_SMS", "EN", "Resend Reset Password Code");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PASSWORD_SMS", "CHS", "Resend Reset Password Code-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_RESET_PASSWORD_SMS", "CHT", "Resend Reset Password Code-CHT");


INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "EN", "You are going to resend reset passoword code by SMS to");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "CHS", "You are going to resend reset passoword code by SMS to-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSGI_USER_COMPLETE_REGISTRATION_SMS", "CHT", "You are going to resend reset passoword code by SMS to-CHT");





