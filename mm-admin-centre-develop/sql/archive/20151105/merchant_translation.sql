
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_LIST_SEARCH_PLACEHOLDER", "EN", "Search for Display Name / Company Name");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_LIST_SEARCH_PLACEHOLDER", "CHS", "Search for Display Name / Company Name-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_AC_MERCHANT_LIST_SEARCH_PLACEHOLDER", "CHT", "Search for Display Name / Company Name-CHT");


INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_NAME", "EN", "Company Name");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_NAME", "CHS", "Company Name-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_COMPANY_NAME", "CHT", "Company Name-CHT");


INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_TYPE", "EN", "Merchant Type");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_TYPE", "CHS", "Merchant Type-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MERCHANT_TYPE", "CHT", "Merchant Type-CHT");


INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_TYPES", "EN", "All Merchant Types");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_TYPES", "CHS", "All Merchant Types-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_ALL_MERCHANT_TYPES", "CHT", "All Merchant Types-CHT");


INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MERCHANT", "EN", "Create New Merchant");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MERCHANT", "CHS", "Create New Merchant-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CREATE_MERCHANT", "CHT", "Create New Merchant-CHT");

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_MERCHANT_STATUS", "EN", "Change Merchant Status");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_MERCHANT_STATUS", "CHS", "Change Merchant Status-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_TL_CHANGE_MERCHANT_STATUS", "CHT", "Change Merchant Status-CHT");
