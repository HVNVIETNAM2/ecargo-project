/*update SecurityGroupName*/
UPDATE SecurityGroupCulture SET SecurityGroupName='美美管理員' Where SecurityGroupCultureId = 2;
UPDATE SecurityGroupCulture SET SecurityGroupName='美美管理员' Where SecurityGroupCultureId = 3;

UPDATE SecurityGroupCulture SET SecurityGroupName='美美客服' Where SecurityGroupCultureId = 5;
UPDATE SecurityGroupCulture SET SecurityGroupName='美美客服' Where SecurityGroupCultureId = 6;

UPDATE SecurityGroupCulture SET SecurityGroupName='美美財務' Where SecurityGroupCultureId = 8;
UPDATE SecurityGroupCulture SET SecurityGroupName='美美财务' Where SecurityGroupCultureId = 9;

UPDATE SecurityGroupCulture SET SecurityGroupName='美美技術' Where SecurityGroupCultureId = 11;
UPDATE SecurityGroupCulture SET SecurityGroupName='美美技术' Where SecurityGroupCultureId = 12;

UPDATE SecurityGroupCulture SET SecurityGroupName='財務' Where SecurityGroupCultureId = 14;
UPDATE SecurityGroupCulture SET SecurityGroupName='财务' Where SecurityGroupCultureId = 15;

/*update StatusCulture*/
UPDATE StatusCulture SET StatusName='激活' WHERE StatusCultureId=5;
UPDATE StatusCulture SET StatusName='激活' WHERE StatusCultureId=6;

UPDATE StatusCulture SET StatusName='待验证' WHERE StatusCultureId=8;
UPDATE StatusCulture SET StatusName='待驗證' WHERE StatusCultureId=9;

UPDATE StatusCulture SET StatusName='暂停' WHERE StatusCultureId=11;
UPDATE StatusCulture SET StatusName='暫停' WHERE StatusCultureId=12;

/*update StatusCulture*/
UPDATE Translation SET TranslationName='激活' WHERE TranslationCode='LB_ACTIVE' AND CultureCode='CHS';
UPDATE Translation SET TranslationName='激活' WHERE TranslationCode='LB_ACTIVE' AND CultureCode='CHT';

UPDATE Translation SET TranslationName='待验证' WHERE TranslationCode='LB_PENDING' AND CultureCode='CHS';
UPDATE Translation SET TranslationName='待驗證' WHERE TranslationCode='LB_PENDING' AND CultureCode='CHT';

UPDATE Translation SET TranslationName='暂停' WHERE TranslationCode='LB_INACTIVE' AND CultureCode='CHS';
UPDATE Translation SET TranslationName='暫停' WHERE TranslationCode='LB_INACTIVE' AND CultureCode='CHT';
