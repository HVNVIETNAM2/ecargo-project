ALTER TABLE `SecurityGroup` ADD COLUMN `Priority` INT(11) NOT NULL DEFAULT '0' ;

update `SecurityGroup` set `Priority` = 1 where SecurityGroupId = 1;
update `SecurityGroup` set `Priority` = 2 where SecurityGroupId = 2;
update `SecurityGroup` set `Priority` = 3 where SecurityGroupId = 3;
update `SecurityGroup` set `Priority` = 4 where SecurityGroupId = 4;
update `SecurityGroup` set `Priority` = 11 where SecurityGroupId = 5;
update `SecurityGroup` set `Priority` = 5 where SecurityGroupId = 6;
update `SecurityGroup` set `Priority` = 7 where SecurityGroupId = 7;

update `SecurityGroup` set `Priority` = 6 where SecurityGroupId = 9;
update `SecurityGroup` set `Priority` = 8 where SecurityGroupId = 10;
update `SecurityGroup` set `Priority` = 9 where SecurityGroupId = 11;
update `SecurityGroup` set `Priority` = 10 where SecurityGroupId = 12;

update `SecurityGroup` set `SecurityGroupNameInvariant` = 'Merchant Customer Service' where SecurityGroupId = 7;
