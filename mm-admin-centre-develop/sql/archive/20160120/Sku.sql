ALTER TABLE `Sku`
	CHANGE COLUMN `ManufacturerName` `SkuManufacturerNameInvariant` varchar(255);

ALTER TABLE `SkuCulture`
	ADD COLUMN `SkuManufacturerName` varchar(255);
