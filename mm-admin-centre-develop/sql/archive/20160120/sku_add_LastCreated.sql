ALTER TABLE `Sku`
	ADD COLUMN `LastCreated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `LastModified`;
	
update Sku set LastCreated=LastModified;