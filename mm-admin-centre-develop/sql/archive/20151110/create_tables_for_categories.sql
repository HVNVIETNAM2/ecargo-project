DROP TABLE IF EXISTS CategoryCulture;
DROP TABLE IF EXISTS Category;

CREATE TABLE Category (
  CategoryId INT(11) NOT NULL AUTO_INCREMENT,
  ParentCategoryId INT(11) NOT NULL DEFAULT 0,  -- 0 means no parent
  CategoryCode VARCHAR(50) NOT NULL,
  CategoryNameInvariant VARCHAR(50) NOT NULL,
  DefaultCommissionRate DECIMAL(10, 2) NOT NULL DEFAULT 0.1,
  PRIMARY KEY (CategoryId),
  UNIQUE KEY uk_CategoryCode (CategoryCode)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE CategoryCulture (
  CategoryCultureId INT(11) NOT NULL AUTO_INCREMENT,
  CategoryId INT(11),
  CultureCode VARCHAR(50),
  CategoryName VARCHAR(50),
  PRIMARY KEY (CategoryCultureId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE MerchantCategory (
  MerchantCategoryId INT(11) NOT NULL AUTO_INCREMENT,
  MerchantId INT(11) NOT NULL,
  CategoryId INT(11) NOT NULL,
  CommissionRate DECIMAL(10, 2) NOT NULL,
  PRIMARY KEY (MerchantCategoryId),
  UNIQUE KEY uk_MerchantId_CategoryId (MerchantId, CategoryId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Insert the dummy root category (to avoid null ParentCategoryId in child categories)
SET SESSION sql_mode = 'NO_AUTO_VALUE_ON_ZERO';  -- disable auto increment for inserting ID=0
INSERT INTO Category (CategoryId, ParentCategoryId, CategoryCode, CategoryNameInvariant, DefaultCommissionRate) VALUES (0, 0, 'CAT0', 'N/A', 0);
SET SESSION sql_mode = '';  -- re-enable auto increment
