
CREATE TABLE IF NOT EXISTS `MerchantDocument` (
  `MerchantDocumentId` int(11) NOT NULL AUTO_INCREMENT,
  `MerchantId` int(11) NOT NULL,
  `DocumentKey` varchar(255),
  `DocumentOriginalName` varchar(255),
  `DocumentMimeType` varchar(255),
  `CreateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`MerchantDocumentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
