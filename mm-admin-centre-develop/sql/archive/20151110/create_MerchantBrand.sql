
CREATE TABLE IF NOT EXISTS `MerchantBrand` (
  `MerchantBrandId` int(11) NOT NULL AUTO_INCREMENT,
  `MerchantId` int(11) NOT NULL,
  `BrandId` int(11) NOT NULL,
  PRIMARY KEY (`MerchantBrandId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
