
/*add unique-key constraint on Translation(TranslationCode,CultureCode) */
alter table Translation
add constraint uk_translation_translationCode_and_cultureCode unique (TranslationCode, CultureCode);
