ALTER TABLE Sku ADD COLUMN `SkuNameInvariant` VARCHAR(255) NULL AFTER ColorKey;
ALTER TABLE Sku ADD COLUMN `SkuDescInvariant` TEXT NULL AFTER SkuNameInvariant;
ALTER TABLE Sku ADD COLUMN `SkuFeatureInvariant` TEXT NULL AFTER SkuDescInvariant;
ALTER TABLE Sku ADD COLUMN `SkuMaterialInvariant` TEXT NULL AFTER SkuFeatureInvariant;
ALTER TABLE Sku ADD COLUMN `SkuColorInvariant` TEXT NULL AFTER SkuMaterialInvariant;