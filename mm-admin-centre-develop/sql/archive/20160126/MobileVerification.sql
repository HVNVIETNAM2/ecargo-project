CREATE TABLE `MobileVerification` (
  `MobileVerificationId` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `MobileCode` VARCHAR(50) NOT NULL,
  `MobileNumber` VARCHAR(50) NOT NULL,
  `MobileVerificationToken` VARCHAR(50) NOT NULL,
  `LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`MobileVerificationId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
