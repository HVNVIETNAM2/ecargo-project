ALTER TABLE `maymay`.`User` 
ADD COLUMN `UserKey` VARCHAR(36) NOT NULL AFTER `UserTypeId`,
ADD COLUMN `UserName` VARCHAR(50) NOT NULL AFTER `UserKey`;

UPDATE `maymay`.`User` SET UserKey = uuid() WHERE UserKey IS NULL OR UserKey = '';

INSERT INTO `maymay`.`UserType` (`UserTypeId`,`UserTypeNameInvariant`) VALUES (3, 'Consumer');
