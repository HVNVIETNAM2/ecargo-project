-- --------------------------------------------------------
-- Host:                         192.168.33.33
-- Server version:               10.1.7-MariaDB-1~trusty-log - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping data for table maymay.InventoryLocation: ~1 rows (approximately)
/*!40000 ALTER TABLE `InventoryLocation` DISABLE KEYS */;
INSERT INTO `InventoryLocation` (`InventoryLocationId`, `MerchantId`, `LocationTypeId`, `LocationExternalCode`, `LocationName`, `StatusId`, `LastStatus`, `LastModified`) VALUES
	(2, 1, 1, '123456', 'Shop for seconds', 4, '2015-10-18 10:36:30', '2015-10-18 10:36:30'),
	(3, 1, 1, '34355', 'Shop near the intersection', 2, '2015-10-18 10:32:50', '2015-10-18 10:34:40'),
	(4, 1, 1, '75656', 'Other shop near the intersection', 2, '2015-10-18 10:33:13', '2015-10-18 10:35:18'),
	(5, 1, 1, '75658', 'Main Shop', 2, '2015-10-18 10:33:32', '2015-10-18 10:34:45'),
	(6, 1, 1, '997655', 'Top Shop', 2, '2015-10-18 10:33:50', '2015-10-18 10:34:49'),
	(7, 1, 1, '667567', 'High Street Shop', 2, '2015-10-18 10:34:12', '2015-10-18 10:34:51'),
	(8, 1, 1, '123456', 'Bling St Shop', 2, '2015-10-18 10:36:23', '2015-10-18 10:36:57'),
	(9, 1, 1, '123456', 'Shope at Central', 2, '2015-10-18 10:36:30', '2015-10-18 10:37:05');
/*!40000 ALTER TABLE `InventoryLocation` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
