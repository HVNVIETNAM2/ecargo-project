-- --------------------------------------------------------
-- Host:                         192.168.33.33
-- Server version:               10.1.7-MariaDB-1~trusty-log - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table maymay.InventoryLocationType
CREATE TABLE IF NOT EXISTS `InventoryLocationType` (
  `InventoryLocationTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `InventoryLocationTypeNameInvariant` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`InventoryLocationTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.InventoryLocationType: ~2 rows (approximately)
/*!40000 ALTER TABLE `InventoryLocationType` DISABLE KEYS */;
INSERT INTO `InventoryLocationType` (`InventoryLocationTypeId`, `InventoryLocationTypeNameInvariant`) VALUES
	(1, 'Store'),
	(2, 'Warehouse');
/*!40000 ALTER TABLE `InventoryLocationType` ENABLE KEYS */;


-- Dumping structure for table maymay.InventoryLocationTypeCulture
CREATE TABLE IF NOT EXISTS `InventoryLocationTypeCulture` (
  `InventoryLocationTypeCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `InventoryLocationTypeId` int(11) DEFAULT '0',
  `CultureCode` varchar(50) DEFAULT NULL,
  `InventoryLocationTypeCultureName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`InventoryLocationTypeCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.InventoryLocationTypeCulture: ~2 rows (approximately)
/*!40000 ALTER TABLE `InventoryLocationTypeCulture` DISABLE KEYS */;
INSERT INTO `InventoryLocationTypeCulture` (`InventoryLocationTypeCultureId`, `InventoryLocationTypeId`, `CultureCode`, `InventoryLocationTypeCultureName`) VALUES
	(1, 1, 'EN', 'Store'),
	(2, 1, 'CHS', '店'),
	(3, 1, 'CHT', '店'),
	(4, 2, 'EN', 'Warehouse'),
	(5, 2, 'CHS', '仓'),
	(6, 2, 'CHT', '倉');
/*!40000 ALTER TABLE `InventoryLocationTypeCulture` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
