INSERT INTO `maymay`.`SecurityGroup` (`UserTypeId`, `SecurityGroupNameInvariant`) VALUES (2, 'Merchant Finance');
INSERT INTO `maymay`.`SecurityGroup` (`UserTypeId`, `SecurityGroupNameInvariant`) VALUES (2, 'Merchant Admin');

INSERT INTO `SecurityGroupCulture` (`SecurityGroupCultureId`, `SecurityGroupId`, `CultureCode`, `SecurityGroupName`) VALUES (13, 5, 'EN', 'Finance');
INSERT INTO `SecurityGroupCulture` (`SecurityGroupCultureId`, `SecurityGroupId`, `CultureCode`, `SecurityGroupName`) VALUES (14, 5, 'CHT', '財經');
INSERT INTO `SecurityGroupCulture` (`SecurityGroupCultureId`, `SecurityGroupId`, `CultureCode`, `SecurityGroupName`) VALUES (15, 5, 'CHS', '财经');
INSERT INTO `SecurityGroupCulture` (`SecurityGroupCultureId`, `SecurityGroupId`, `CultureCode`, `SecurityGroupName`) VALUES (16, 6, 'EN', 'Admin');
INSERT INTO `SecurityGroupCulture` (`SecurityGroupCultureId`, `SecurityGroupId`, `CultureCode`, `SecurityGroupName`) VALUES (17, 6, 'CHT', '管理員');
INSERT INTO `SecurityGroupCulture` (`SecurityGroupCultureId`, `SecurityGroupId`, `CultureCode`, `SecurityGroupName`) VALUES (18, 6, 'CHS', '管理員');


