CREATE DEFINER=`root`@`%` FUNCTION `fnSplitStr`(`x` VARCHAR(4000), `delim` VARCHAR(12), `pos` INT
)
	RETURNS varchar(4000) CHARSET utf8
	LANGUAGE SQL
	DETERMINISTIC
	READS SQL DATA
	SQL SECURITY DEFINER
	COMMENT ''
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
       delim, '');