ALTER TABLE `SecurityGroup`
	CHANGE COLUMN `SecurityGroupTypeId` `UserTypeId` INT(11) NOT NULL DEFAULT '0' AFTER `SecurityGroupId`;

CREATE TABLE `UserType` (
	`UserTypeId` INT NOT NULL AUTO_INCREMENT,
	`UserTypeNameInvariant` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`UserTypeId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

INSERT INTO `maymay`.`UserType` (`UserTypeNameInvariant`) VALUES ('MM');
INSERT INTO `maymay`.`UserType` (`UserTypeNameInvariant`) VALUES ('Merchant');

ALTER TABLE `User`
	ADD COLUMN `UserTypeId` INT NULL DEFAULT '0' AFTER `UserId`;

update User set UserTypeId=1;

INSERT INTO `maymay`.`User` (UserTypeId,FirstName,LastName,Email,MobileCode,MobileNumber,LanguageId,TimeZoneId,StatusId,Hash,Salt) VALUES (2,'Merch','Ant','merch@ecargo.com','+852','123456',1,1,2,'1246XJ8HHFQ2cAszYJWEmC4jBNinnp6mD5kIzk/nWGG26hJK9Q4umKDR+gpEZZ62LwqOhXV0AoCaf34hjRX/QQ==','UWuQMSuxK46Zf83+aBdMiA==');
