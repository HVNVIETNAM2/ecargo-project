-- --------------------------------------------------------
-- Host:                         192.168.33.33
-- Server version:               10.1.8-MariaDB-1~trusty-log - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table maymay.JournalEvent
DROP TABLE IF EXISTS `JournalEvent`;
CREATE TABLE IF NOT EXISTS `JournalEvent` (
  `JournalEventId` int(11) NOT NULL AUTO_INCREMENT,
  `JournalEventNameInvariant` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`JournalEventId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.JournalEvent: ~2 rows (approximately)
/*!40000 ALTER TABLE `JournalEvent` DISABLE KEYS */;
INSERT INTO `JournalEvent` (`JournalEventId`, `JournalEventNameInvariant`) VALUES
	(1, 'Manual Edit'),
	(2, 'Data Import');
/*!40000 ALTER TABLE `JournalEvent` ENABLE KEYS */;


-- Dumping structure for table maymay.JournalEventCulture
DROP TABLE IF EXISTS `JournalEventCulture`;
CREATE TABLE IF NOT EXISTS `JournalEventCulture` (
  `JournalEventCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `JournalEventId` int(11) NOT NULL DEFAULT '0',
  `CultureCode` varchar(50) DEFAULT NULL,
  `JournalEventName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`JournalEventCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.JournalEventCulture: ~6 rows (approximately)
/*!40000 ALTER TABLE `JournalEventCulture` DISABLE KEYS */;
INSERT INTO `JournalEventCulture` (`JournalEventCultureId`, `JournalEventId`, `CultureCode`, `JournalEventName`) VALUES
	(1, 1, 'EN', 'Manual Edit'),
	(2, 1, 'CHS', '手动编辑'),
	(3, 1, 'CHT', '手動編輯'),
	(4, 2, 'EN', 'Data Import'),
	(5, 2, 'CHS', '数据导入'),
	(6, 2, 'CHT', '數據導入');
/*!40000 ALTER TABLE `JournalEventCulture` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
