ALTER TABLE `InventoryJournal`
	CHANGE COLUMN `Tick` `LastModified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `InventoryJournalId`,
	ADD COLUMN `LastModifiedUserId` INT(11) NOT NULL DEFAULT '0' AFTER `LastModified`,
	CHANGE COLUMN `JournalTypeId` `JournalTypeId` INT(11) NOT NULL DEFAULT '0' AFTER `LastModifiedUserId`,
	ADD COLUMN `JournalEventId` INT(11) NOT NULL DEFAULT '0' AFTER `JournalTypeId`;

ALTER TABLE `InventoryLocation`
	CHANGE COLUMN `QtySafetyThreshold` `QtySafetyThreshold` INT(11) NOT NULL DEFAULT '0' AFTER `Street`;

ALTER TABLE `InventoryJournal`
	ADD COLUMN `QtySafetyThresholdSku` INT(11) NOT NULL DEFAULT '0' AFTER `QtyOrdered`,
	ADD COLUMN `QtySafetyThresholdLocation` INT(11) NOT NULL DEFAULT '0' AFTER `QtySafetyThresholdSku`,
	ADD COLUMN `QtyAts` INT(11) NOT NULL DEFAULT '0' AFTER `QtySafetyThresholdLocation`;

update InventoryJournal set QtyAts=QtyAllocated;
	
Update InventoryJournal set JournalEventId=1;

Update InventoryJournal set LastModifiedUserId=349;