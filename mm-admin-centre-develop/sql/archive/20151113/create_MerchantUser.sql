drop table MerchantStaff;

CREATE TABLE IF NOT EXISTS `MerchantUser` (
  `MerchantUserId` int(11) NOT NULL AUTO_INCREMENT,
  `MerchantId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  PRIMARY KEY (`MerchantUserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
