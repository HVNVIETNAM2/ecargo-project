ALTER TABLE  `Category` ADD COLUMN `CategoryDescInvariant` VARCHAR(255) NULL AFTER `CategoryNameInvariant`;

ALTER TABLE  `Category` ADD COLUMN `CategoryMetaKeywordInvariant` VARCHAR(255) NULL AFTER `CategoryDescInvariant`;

ALTER TABLE  `Category` ADD COLUMN `CategoryMetaDescInvariant` VARCHAR(255) NULL AFTER `CategoryMetaKeywordInvariant`;

ALTER TABLE `Category` ADD COLUMN `IsMerchCanSelect` tinyint not null default 0 AFTER `IsIncludeInAutocomplete`;

--if StartPublishTime & EndPublishTime are both null, then means publish forever--
ALTER TABLE `Category` ADD COLUMN `StartPublishTime` datetime  null AFTER `DefaultCommissionRate`;

ALTER TABLE `Category` ADD COLUMN `EndPublishTime` datetime  null AFTER `StartPublishTime`;

ALTER TABLE `Category` ADD COLUMN `StatusId` int(11) not null AFTER `EndPublishTime`;
UPDATE Category SET StatusId=2 ;


ALTER TABLE  `CategoryCulture` ADD COLUMN `CategoryDesc` VARCHAR(255) NULL AFTER `CategoryName`;

ALTER TABLE  `CategoryCulture` ADD COLUMN `CategoryMetaKeyword` VARCHAR(255) NULL AFTER `CategoryDesc`;

ALTER TABLE  `CategoryCulture` ADD COLUMN `CategoryMetaDesc` VARCHAR(255) NULL AFTER `CategoryMetaKeyword`;
