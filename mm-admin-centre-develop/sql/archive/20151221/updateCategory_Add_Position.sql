ALTER TABLE `Category` ADD COLUMN `Position` int NOT NULL DEFAULT 0 AFTER `DefaultCommissionRate`;

UPDATE Category 
LEFT JOIN (
SELECT CategoryId, ParentCategoryId, 
case
when CategoryId = 0 THEN @count := 0
when @parent = ParentCategoryId THEN @count := @count + 1
when @parent := ParentCategoryId THEN @count := 1
end as Position
FROM Category t, (SELECT @count := 0, @parent := 0) r
) C ON Category.CategoryId = C.CategoryId
set Category.Position = C.Position;
