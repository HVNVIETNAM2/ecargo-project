CREATE TABLE IF NOT EXISTS `MerchantImage` (
	`MerchantImageId` INT(11) NOT NULL AUTO_INCREMENT,
	`MerchantId` INT(11) NOT NULL,
	`ImageTypeCode` VARCHAR(20) NOT NULL,
	`MerchantImage` VARCHAR(255) NOT NULL,
	`Position` INT(11) NOT NULL,
	PRIMARY KEY (`MerchantImageId`)
)
ENGINE=InnoDB
;
