ALTER TABLE `Merchant`
	CHANGE COLUMN `MerchantDisplayName`  `MerchantNameInvariant`  VARCHAR(255);

ALTER TABLE `Merchant`
	CHANGE COLUMN `MerchantDesc`  `MerchantDescInvariant` TEXT;

ALTER TABLE `Merchant`
	CHANGE COLUMN `LogoImage` `HeaderLogoImage` VARCHAR(255);

ALTER TABLE `Merchant`
	ADD COLUMN `SmallLogoImage` VARCHAR(255) NULL AFTER `HeaderLogoImage`;

ALTER TABLE `Merchant`
	ADD COLUMN `LargeLogoImage` VARCHAR(255) NULL AFTER `SmallLogoImage`;

ALTER TABLE `Merchant`
	CHANGE COLUMN `BackgroundImage` `ProfileBannerImage` VARCHAR(255);

ALTER TABLE `Merchant`
	ADD COLUMN `IsListedMerchant` BIT(1) NOT NULL AFTER `ProfileBannerImage`;

ALTER TABLE `Merchant`
	ADD COLUMN `IsFeaturedMerchant` BIT(1) NOT NULL AFTER `IsListedMerchant`;

ALTER TABLE `Merchant`
	ADD COLUMN `IsRecommendedMerchant` BIT(1) NOT NULL AFTER `IsFeaturedMerchant`;

ALTER TABLE `Merchant`
	ADD COLUMN `IsSuggestedSearchBar` BIT(1) NOT NULL AFTER `IsRecommendedMerchant`;
