CREATE TABLE `InventoryImport` (
	`ImportId` INT NOT NULL AUTO_INCREMENT,
	`Guid` VARCHAR(100) NULL DEFAULT NULL,
	`UserId` int(11) NOT NULL,
	`MerchantId` int(11) NOT NULL,
	`InventoryLocationId` int(11) NOT NULL,
	`Overwrite` int(11) NOT NULL DEFAULT 0,
	`FileLocation` VARCHAR(500) NULL DEFAULT NULL,
	`ErrorLocation` VARCHAR(500) NULL DEFAULT NULL,
	`ErrorCounts` int(11) NOT NULL DEFAULT 0,
	`SkippedCounts` int(11) NOT NULL DEFAULT 0,
	`UpdateCounts` int(11) NOT NULL DEFAULT 0,
	`InsertCounts` int(11) NOT NULL DEFAULT 0,
	`TotalCounts` int(11) NOT NULL DEFAULT 0,
	`StatusId` int(11) NOT NULL DEFAULT 0,
	`LastModified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`ImportId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;