INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) values ("LB_LOC_SELECT", "EN", "Inventory Location");
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) values ("LB_LOC_SELECT", "CHT", "Inventory Location-CHS");
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) values ("LB_LOC_SELECT", "CHT", "Inventory Location-CHT");


INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) values ("MSG_ERR_LOC_NIL", "EN", "Please choose at least one inventory location");
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) values ("MSG_ERR_LOC_NIL", "CHS", "Please choose at least one inventory location-CHS");
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) values ("MSG_ERR_LOC_NIL", "CHT", "Please choose at least one inventory location-CHT");