CREATE TABLE `UserInventoryLocation` (
	`UserInventoryLocationId` INT NOT NULL AUTO_INCREMENT,
	`UserId` INT NULL DEFAULT '0',
	`InventoryLocationId` INT NULL DEFAULT '0',
	PRIMARY KEY (`UserInventoryLocationId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


select UserId into @uid from User where Email='merch@ecargo.com';
insert into UserInventoryLocation (UserId,InventoryLocationId) values (@uid,2);
insert into UserInventoryLocation (UserId,InventoryLocationId) values (@uid,3);