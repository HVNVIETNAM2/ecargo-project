
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (7, 'EN', 'SuperAdmin');
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (7, 'CHS', 'SuperAdmin-CHS');
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (7, 'CHT', 'SuperAdmin-CHT');

Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (9, 'EN', 'Content Manager');
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (9, 'CHS', 'Content Manager-CHS');
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (9, 'CHT', 'Content Manager-CHT');

Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (10, 'EN', 'Merchandiser');
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (10, 'CHS', 'Merchandiser-CHS');
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (10, 'CHT', 'Merchandiser-CHT');

Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (11, 'EN', 'Order Processsing');
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (11, 'CHS', 'Order Processsing-CHS');
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (11, 'CHT', 'Order Processsing-CHT');

Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (12, 'EN', 'Product Return');
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (12, 'CHS', 'Product Return-CHS');
Insert into SecurityGroupCulture (SecurityGroupId, CultureCode, SecurityGroupName) values (12, 'CHT', 'Product Return-CHT');