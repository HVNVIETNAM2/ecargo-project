-- --------------------------------------------------------
-- Host:                         192.168.33.33
-- Server version:               10.1.10-MariaDB-1~trusty-log - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table maymay.CartType
DROP TABLE IF EXISTS `CartType`;
CREATE TABLE IF NOT EXISTS `CartType` (
  `CartTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `CartTypeName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`CartTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table maymay.CartType: ~0 rows (approximately)
/*!40000 ALTER TABLE `CartType` DISABLE KEYS */;
INSERT INTO `CartType` (`CartTypeId`, `CartTypeName`) VALUES
	(1, 'Basket'),
	(2, 'WishList');
/*!40000 ALTER TABLE `CartType` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

ALTER TABLE `CartItem`
	ADD COLUMN `StyleCode` VARCHAR(50) NOT NULL DEFAULT '0' AFTER `Qty`,
	ADD COLUMN `ColorKey` VARCHAR(50) NOT NULL DEFAULT '0' AFTER `StyleCode`,
	ADD COLUMN `StatusId` INT(11) NOT NULL DEFAULT '0' AFTER `ColorKey`;

ALTER TABLE `Cart`
	ADD COLUMN `CartTypeId` INT NOT NULL DEFAULT '0' AFTER `UserId`,
	ADD COLUMN `StatusId` INT NOT NULL DEFAULT '0' AFTER `CartTypeId`;

