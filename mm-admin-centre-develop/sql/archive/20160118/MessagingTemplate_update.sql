/** create user message**/
UPDATE MessagingTemplate SET
EmailTemplateSubject='Complete Your Registration',
EmailTemplateContent='Dear {{FirstName}},<p><p>Your {{FirstName}} account has been created! Please click <a href="{{RootURL}}/activate?at={{ActivationToken}}">here</a> and complete your registration.<p><p>This invitation is valid for only 24 hours.<p><p>Sincerely,<p><p>MM Team<p><p>',
SmsTemplateContent='MM code: {{ActivationToken}}. valid for 24 hour.'
WHERE MessagingTemplateCode="CREATE_USER" AND CultureCode="EN";

UPDATE MessagingTemplate SET
EmailTemplateSubject='請激活賬戶',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>您的帳戶已被創建，請點擊<a href="{{RootURL}}/activate?at={{ActivationToken}}">這裡</a> 激活賬戶並完成註冊。<p><p>此鏈接有效時間為24小時。請盡快完成激活。<p><p>謝謝！<p>美美<p>美美，就是愛美<p>',
SmsTemplateContent='MM 註冊碼: {{ActivationToken}}。 24 小時內有效。'
WHERE MessagingTemplateCode="CREATE_USER" AND CultureCode="CHT";


UPDATE MessagingTemplate SET
EmailTemplateSubject='请激活账户',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>您的帐户已被创建，请点击<a href="{{RootURL}}/activate?at={{ActivationToken}}">这裡</a> 激活账户并完成注册。<p><p>此链接有效时间为24小时。请尽快完成激活。<p><p>谢谢！<p>美美<p>美美，就是爱美<p>',
SmsTemplateContent='MM 注册码: {{ActivationToken}}。 24 小时内有效。'
WHERE MessagingTemplateCode="CREATE_USER" AND CultureCode="CHS";


