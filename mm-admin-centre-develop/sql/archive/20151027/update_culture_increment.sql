/*update time zone name*/
UPDATE SecurityGroupCulture SET SecurityGroupName='Admin' WHERE SecurityGroupCultureId=1;
UPDATE SecurityGroupCulture SET SecurityGroupName='管理員' WHERE SecurityGroupCultureId=2;
UPDATE SecurityGroupCulture SET SecurityGroupName='管理员' WHERE SecurityGroupCultureId=3;

UPDATE SecurityGroupCulture SET SecurityGroupName='Customer Service' WHERE SecurityGroupCultureId=4;
UPDATE SecurityGroupCulture SET SecurityGroupName='客服' WHERE SecurityGroupCultureId=5;
UPDATE SecurityGroupCulture SET SecurityGroupName='客服' WHERE SecurityGroupCultureId=6;

UPDATE SecurityGroupCulture SET SecurityGroupName='Finance' WHERE SecurityGroupCultureId=7;
UPDATE SecurityGroupCulture SET SecurityGroupName='財務' WHERE SecurityGroupCultureId=8;
UPDATE SecurityGroupCulture SET SecurityGroupName='财务' WHERE SecurityGroupCultureId=9;

UPDATE SecurityGroupCulture SET SecurityGroupName='TechOps' WHERE SecurityGroupCultureId=10;
UPDATE SecurityGroupCulture SET SecurityGroupName='運帷' WHERE SecurityGroupCultureId=11;
UPDATE SecurityGroupCulture SET SecurityGroupName='运维' WHERE SecurityGroupCultureId=12;

/*There are only 4 roles, delete the redundancy*/
/*
line below removed by Albert this is wrong on many levels. 1) there are amny more than 4 roles 2) Becuase its autoincement delete greater than id is not wise as id's different on differnt macines based on insertion order.
DELETE FROM SecurityGroupCulture WHERE SecurityGroupCultureId>=13
*/