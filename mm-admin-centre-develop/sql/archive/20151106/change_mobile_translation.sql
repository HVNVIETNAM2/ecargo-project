INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_MOBILE", "EN", "Change Mobile Number");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_MOBILE", "CHS", "Change Mobile Number-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CHANGE_MOBILE", "CHT", "Change Mobile Number-CHT");

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_MOBILE", "EN", "Type the user new mobile number for {0}");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_MOBILE", "CHS", "Type the user new mobile number for {0}-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MM_ADMIN_RESET_USER_MOBILE", "CHT", "Type the user new mobile number for {0}-CHT");

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_MOBILE", "EN", "New Mobile Number");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_MOBILE", "CHS", "New Mobile Number-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_NEW_MOBILE", "CHT", "New Mobile Number-CHT");

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_MOBILE", "EN", "Confirm Mobile Number");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_MOBILE", "CHS", "Confirm Mobile Number-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_CONF_NEW_MOBILE", "CHT", "Confirm Mobile Number-CHT");

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_MATCH", "EN", "Mobile number doesn't match");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_MATCH", "CHS", "Mobile number doesn't match-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_MATCH", "CHT", "Mobile number doesn't match-CHT");

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NO_CHANGE", "EN", "The mobile you entered is the same as the existing one");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NO_CHANGE", "CHS", "The mobile you entered is the same as the existing one-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_ERR_MOBILE_NO_CHANGE", "CHT", "The mobile you entered is the same as the existing one-CHT");

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_CHANGE_SUCCESS", "EN", "Mobile number updated successfully.");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_CHANGE_SUCCESS", "CHS", "Mobile number updated successfully.-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_CHANGE_SUCCESS", "CHT", "Mobile number updated successfully.-CHT");

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_SENT_SMS", "EN", "A SMS is sent to the user's new mobile number.");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_SENT_SMS", "CHS", "A SMS is sent to the user's new mobile number.-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("MSG_MOBILE_SENT_SMS", "CHT", "A SMS is sent to the user's new mobile number.-CHT");
