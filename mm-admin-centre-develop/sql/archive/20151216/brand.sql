ALTER TABLE `Brand` MODIFY IsListedBrand TINYINT(1);
ALTER TABLE `Brand` MODIFY IsFeatureBrand TINYINT(1);
ALTER TABLE `Brand` MODIFY IsRecommendedBrand TINYINT(1);
ALTER TABLE `Brand` MODIFY IsSearchable TINYINT(1);


ALTER TABLE `Brand`
	CHANGE COLUMN `HeaderLogo` `HeaderLogoImage` VARCHAR(255);

ALTER TABLE `Brand`
	CHANGE COLUMN `SmallLogo` `SmallLogoImage` VARCHAR(255);

ALTER TABLE `Brand`
	CHANGE COLUMN `LargeLogo` `LargeLogoImage` VARCHAR(255);

ALTER TABLE `Brand`
	CHANGE COLUMN `ProfileImage` `ProfileBannerImage` VARCHAR(255);

ALTER TABLE `BrandImage`
	ADD COLUMN `Position` INT(11);
