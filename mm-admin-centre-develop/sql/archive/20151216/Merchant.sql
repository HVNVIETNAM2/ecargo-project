alter table Merchant modify IsListedMerchant tinyint;

alter table Merchant modify IsFeaturedMerchant tinyint;

alter table Merchant modify IsRecommendedMerchant tinyint;

alter table Merchant modify IsSuggestedSearchBar tinyint;
