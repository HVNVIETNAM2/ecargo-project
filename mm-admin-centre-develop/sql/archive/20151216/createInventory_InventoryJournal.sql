drop table if exists Inventory;
CREATE TABLE `Inventory` (
	`InventoryId` INT(11) NOT NULL AUTO_INCREMENT,
	`SkuId` INT(11) NOT NULL DEFAULT '0',
	`InventoryLocationId` INT(11) NOT NULL DEFAULT '0',
	`IsPerpetual` BIT(1) NOT NULL DEFAULT b'0',
	`QtyAllocated` INT(11) NOT NULL DEFAULT '0',
	`QtyExported` INT(11) NOT NULL DEFAULT '0',
	`QtyOrdered` INT(11) NOT NULL DEFAULT '0',
	`LastModified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`InventoryId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

drop table if exists InventoryJournal;
CREATE TABLE `InventoryJournal` (
	`InventoryJournalId` INT(11) NOT NULL AUTO_INCREMENT,
	`Tick` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
	`JournalTypeId` INT(11) NULL DEFAULT '0',
	`InventoryId` INT(11) NOT NULL DEFAULT '0',
	`SkuId` INT(11) NOT NULL DEFAULT '0',
	`InventoryLocationId` INT(11) NOT NULL DEFAULT '0',
	`IsPerpetual` BIT(1) NOT NULL DEFAULT b'0',
	`QtyAllocated` INT(11) NOT NULL DEFAULT '0',
	`QtyExported` INT(11) NOT NULL DEFAULT '0',
	`QtyOrdered` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`InventoryJournalId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;