ALTER TABLE InventoryLocation ALTER QtySafetyThreshold SET DEFAULT 0;

UPDATE InventoryLocation SET QtySafetyThreshold = 0 WHERE QtySafetyThreshold IS NULL;
