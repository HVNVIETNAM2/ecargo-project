DROP TABLE IF EXISTS Sku;
CREATE TABLE IF NOT EXISTS `Sku` (
  `SkuId` int(11) NOT NULL AUTO_INCREMENT,
  `StyleCode` varchar(255) DEFAULT NULL,
  `Upc` varchar(255) DEFAULT NULL,
  `Bar` varchar(255) DEFAULT NULL,
  `BrandId` int(11) NOT NULL DEFAULT '0',
  `BadgeId` int(11) NOT NULL DEFAULT '0',
  `SeasonId` int(11) NOT NULL DEFAULT '0',
  `SizeId` int(11) NOT NULL DEFAULT '0',
  `ColorId` int(11) NOT NULL DEFAULT '0',
  `GeoCountryId` int(11) NOT NULL DEFAULT '0',
  `LaunchYear` int(11) NOT NULL DEFAULT '0',
  `ManufacturerName` varchar(255) DEFAULT NULL,
  `PriceRetail` decimal(10,2) NOT NULL DEFAULT '0',
  `PriceSale` decimal(10,2) NOT NULL DEFAULT '0',
  `SaleFrom` datetime DEFAULT NULL,
  `SaleTo` datetime DEFAULT NULL,
  `AvailableFrom` datetime DEFAULT NULL,
  `AvailableTo` datetime DEFAULT NULL,
  `WeightKg` int(11) NOT NULL DEFAULT '0',
  `HeightCm` int(11) NOT NULL DEFAULT '0',
  `WidthCm` int(11) NOT NULL DEFAULT '0',
  `LengthCm` int(11) NOT NULL DEFAULT '0',
  `LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,  
  PRIMARY KEY (`SkuId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS SkuCategory;
CREATE TABLE IF NOT EXISTS `SkuCategory` (
  `SkuCategoryId` int(11) NOT NULL AUTO_INCREMENT,
  `SkuId` int(11) NOT NULL DEFAULT '0',
  `CategoryId` int(11) NOT NULL DEFAULT '0',
  `Priority` int(11) NOT NULL DEFAULT '0',
  `LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`SkuCategoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS SkuCulture;
CREATE TABLE SkuCulture (
  SkuCultureId INT(11) NOT NULL AUTO_INCREMENT,
  SkuId INT(11) NOT NULL DEFAULT '0',
  CultureCode VARCHAR(50) DEFAULT NULL,
  SkuName VARCHAR(255) DEFAULT NULL,
  SkuDesc text DEFAULT NULL,
  SkuFeature text DEFAULT NULL,
  SkuMaterial text DEFAULT NULL,
  `LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (SkuCultureId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
