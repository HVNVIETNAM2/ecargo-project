ALTER TABLE `InventoryLocationTypeCulture`
	CHANGE COLUMN `InventoryLocationTypeCultureName` `InventoryLocationTypeName` VARCHAR(50) NULL DEFAULT NULL AFTER `CultureCode`;
	
ALTER TABLE `InventoryLocation`
	CHANGE COLUMN `LocationTypeId` `InventoryLocationTypeId` INT(11) NOT NULL DEFAULT '0' AFTER `MerchantId`;
