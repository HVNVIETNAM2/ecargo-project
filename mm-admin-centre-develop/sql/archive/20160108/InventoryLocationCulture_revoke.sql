ALTER TABLE `InventoryLocation` CHANGE `DistrictInvariant` `District` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `ApartmentInvariant` `Apartment` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `FloorInvariant` `Floor` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `BlockNoInvariant` `BlockNo` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `BuildingInvariant` `Building` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `StreetNoInvariant` `StreetNo` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `StreetInvariant` `Street` VARCHAR(50) NULL DEFAULT NULL;


ALTER TABLE InventoryLocationCulture DROP District;
ALTER TABLE InventoryLocationCulture DROP Apartment;
ALTER TABLE InventoryLocationCulture DROP Floor;
ALTER TABLE InventoryLocationCulture DROP BlockNo;
ALTER TABLE InventoryLocationCulture DROP Building;
ALTER TABLE InventoryLocationCulture DROP StreetNo;
ALTER TABLE InventoryLocationCulture DROP Street;




