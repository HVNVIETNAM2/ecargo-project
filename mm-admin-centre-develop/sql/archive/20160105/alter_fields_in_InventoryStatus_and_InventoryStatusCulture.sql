/*
 * Alter fields in InventoryStatus and InventoryStatusCulture to map the system mapping
 */
/**
 * Author:  Michael Yu <michaelyu@wweco.com>
 * Created: Jan 5, 2016
 */

ALTER TABLE `maymay`.`InventoryStatus`
CHANGE COLUMN `StatusId` `InventoryStatusId` INT(11) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `StatusNameInvariant` `InventoryStatusNameInvariant` VARCHAR(50) NULL DEFAULT NULL ;

ALTER TABLE `maymay`.`InventoryStatusCulture`
CHANGE COLUMN `StatusCultureId` `InventoryStatusCultureId` INT(11) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `StatusId` `InventoryStatusId` INT(11) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `StatusName` `InventoryStatusName` VARCHAR(50) NULL DEFAULT NULL ;
