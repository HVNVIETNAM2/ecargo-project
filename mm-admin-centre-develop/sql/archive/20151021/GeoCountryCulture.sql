-- --------------------------------------------------------
-- Host:                         192.168.33.33
-- Server version:               10.1.7-MariaDB-1~trusty-log - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table maymay.GeoCountryCulture
CREATE TABLE IF NOT EXISTS `GeoCountryCulture` (
  `GeoCountryCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `GeoCountryId` int(11) DEFAULT '0',
  `CultureCode` varchar(50) DEFAULT NULL,
  `GeoCountryName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`GeoCountryCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.GeoCountryCulture: ~2 rows (approximately)
/*!40000 ALTER TABLE `GeoCountryCulture` DISABLE KEYS */;
INSERT INTO `GeoCountryCulture` (`GeoCountryCultureId`, `GeoCountryId`, `CultureCode`, `GeoCountryName`) VALUES
	(1, 48, 'EN', 'China'),
	(2, 48, 'CHS', '中国'),
	(3, 48, 'CHT', '中國');
/*!40000 ALTER TABLE `GeoCountryCulture` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
