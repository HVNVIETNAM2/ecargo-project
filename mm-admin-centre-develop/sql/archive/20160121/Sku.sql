ALTER TABLE `Sku`
	CHANGE COLUMN `SkuManufacturerNameInvariant` `ManufacturerName` varchar(255);

ALTER TABLE `SkuCulture`
	DROP COLUMN `SkuManufacturerName`;
