-- MySQL dump 10.16  Distrib 10.1.7-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: maymay
-- ------------------------------------------------------
-- Server version	10.1.7-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `GeoCountry`
--

DROP TABLE IF EXISTS `GeoCountry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GeoCountry` (
  `GeoCountryId` int(11) NOT NULL AUTO_INCREMENT,
  `CountryCode` varchar(255) NOT NULL,
  `GeoCountryNameInvariant` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GeoCountryId`),
  KEY `CountryCode` (`CountryCode`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GeoCountry`
--

LOCK TABLES `GeoCountry` WRITE;
/*!40000 ALTER TABLE `GeoCountry` DISABLE KEYS */;
INSERT INTO `GeoCountry` VALUES (0,'','---'),(1,'AD','Andorra'),(2,'AE','United Arab Emirates'),(3,'AG','Antigua and Barbuda'),(4,'AI','Anguilla'),(5,'AL','Albania'),(6,'AM','Armenia'),(7,'AN','Netherlands Antilles'),(8,'AO','Angola'),(9,'AR','Argentina'),(10,'AS','American Samoa'),(11,'AT','Austria'),(12,'AU','Australia'),(13,'AW','Aruba'),(14,'AX','Afghanistan'),(15,'AZ','Azerbaijan'),(16,'BA','Bosnia and Herzegovina'),(17,'BB','Barbados'),(18,'BD','Bangladesh'),(19,'BE','Belgium'),(20,'BF','Burkina Faso'),(21,'BG','Bulgaria'),(22,'BH','Bahrain'),(23,'BI','Burundi'),(24,'BJ','Benin'),(25,'BL','Saint Barthélemy'),(26,'BM','Bermuda'),(27,'BN','Brunei Darussalam'),(28,'BO','Bolivia (Plurinational State of)'),(29,'BR','Brazil'),(30,'BS','Bahamas (the)'),(31,'BT','Bhutan'),(32,'BU','Burma'),(33,'BV','Bouvet Island'),(34,'BW','Botswana'),(35,'BY','Belarus'),(36,'BZ','Belize'),(37,'CA','Canada'),(38,'CC','Cocos (Keeling) Islands'),(39,'CD','Congo (the Democratic Republic of the)'),(40,'CF','Central African Republic (the)'),(41,'CG','Congo (the)'),(42,'CH','Switzerland'),(43,'CI','Côte d\'Ivoire'),(44,'CK','Cook Islands (the)'),(45,'CL','Chile'),(46,'CM','Cameroon'),(47,'CN','China'),(48,'CO','Colombia'),(49,'CR','Costa Rica'),(50,'CS','Serbia and Montenegro'),(51,'CT','Canton and Enderbury Islands'),(52,'CU','Cuba'),(53,'CV','Cabo Verde'),(54,'CW','Curaçao'),(55,'CX','Christmas Island'),(56,'CY','Cyprus'),(57,'CZ','Czech Republic'),(58,'DE','Germany'),(59,'DJ','Djibouti'),(60,'DK','Denmark'),(61,'DM','Dominica'),(62,'DO','Dominican Republic (the)'),(63,'DY','Dahomey'),(64,'DZ','Algeria'),(65,'EC','Ecuador'),(66,'EE','Estonia'),(67,'EG','Egypt'),(68,'ER','Eritrea'),(69,'ES','Spain'),(70,'ET','Ethiopia'),(71,'FI','Finland'),(72,'FJ','Fiji'),(73,'FK','Falkland Islands (the) [Malvinas]'),(74,'FM','Micronesia (Federated States of)'),(75,'FO','Faroe Islands (the)'),(76,'FR','France'),(77,'GA','Gabon'),(78,'GB','United Kingdom of Great Britain and Northern Ireland (the)'),(79,'GD','Grenada'),(80,'GE','Georgia'),(81,'GE','Gilbert and Ellice Islands'),(82,'GG','Guernsey'),(83,'GH','Ghana'),(84,'GL','Greenland'),(85,'GM','Gambia (the)'),(86,'GN','Guinea'),(87,'GP','Guadeloupe'),(88,'GQ','Equatorial Guinea'),(89,'GR','Greece'),(90,'GS','South Georgia and the South Sandwich Islands'),(91,'GT','Guatemala'),(92,'GU','Guam'),(93,'GW','Guinea-Bissau'),(94,'GY','Guyana'),(95,'HK','Hong Kong'),(96,'HM','Heard Island and McDonald Islands'),(97,'HN','Honduras'),(98,'HR','Croatia'),(99,'HT','Haiti'),(100,'HU','Hungary'),(101,'ID','Indonesia'),(102,'IE','Ireland'),(103,'IL','Israel'),(104,'IM','Isle of Man'),(105,'IN','India'),(106,'IQ','Iraq'),(107,'IR','Iran (Islamic Republic of)'),(108,'IS','Iceland'),(109,'IT','Italy'),(110,'JE','Jersey'),(111,'JM','Jamaica'),(112,'JO','Jordan'),(113,'JP','Japan'),(114,'JT','Johnston Island'),(115,'KE','Kenya'),(116,'KG','Kyrgyzstan'),(117,'KH','Cambodia'),(118,'KI','Kiribati'),(119,'KM','Comoros (the)'),(120,'KN','Saint Kitts and Nevis'),(121,'KP','Korea (the Democratic People\'s Republic of)'),(122,'KR','Korea (the Republic of)'),(123,'KW','Kuwait'),(124,'KY','Cayman Islands'),(125,'KZ','Kazakhstan'),(126,'LA','Lao People\'s Democratic Republic (the)'),(127,'LB','Lebanon'),(128,'LC','Saint Lucia'),(129,'LI','Liechtenstein'),(130,'LK','Sri Lanka'),(131,'LR','Liberia'),(132,'LS','Lesotho'),(133,'LT','Lithuania'),(134,'LU','Luxembourg'),(135,'LV','Latvia'),(136,'LY','Libya'),(137,'MA','Morocco'),(138,'MC','Monaco'),(139,'MD','Moldova (the Republic of)'),(140,'ME','Montenegro'),(141,'MF','Saint Martin (French part)'),(142,'MG','Madagascar'),(143,'MH','Marshall Islands (the)'),(144,'MI','Midway Islands'),(145,'MK','Macedonia (the former Yugoslav Republic of)'),(146,'ML','Mali'),(147,'MM','Myanmar'),(148,'MN','Mongolia'),(149,'MO','Macao'),(150,'MP','Northern Mariana Islands (the)'),(151,'MQ','Martinique'),(152,'MR','Mauritania'),(153,'MS','Montserrat'),(154,'MT','Malta'),(155,'MU','Mauritius'),(156,'MV','Maldives'),(157,'MW','Malawi'),(158,'MX','Mexico'),(159,'MY','Malaysia'),(160,'MZ','Mozambique'),(161,'NA','Namibia'),(162,'NC','New Caledonia'),(163,'NE','Niger (the)'),(164,'NF','Norfolk Island'),(165,'NG','Nigeria'),(166,'NH','New Hebrides'),(167,'NI','Nicaragua'),(168,'NL','Netherlands (the)'),(169,'NO','Norway'),(170,'NP','Nepal'),(171,'NR','Nauru'),(172,'NU','Niue'),(173,'NZ','New Zealand'),(174,'OM','Oman'),(175,'PA','Panama'),(176,'PC','Pacific Islands (Trust Territory)'),(177,'PE','Peru'),(178,'PG','Papua New Guinea'),(179,'PH','Philippines (the)'),(180,'PK','Pakistan'),(181,'PL','Poland'),(182,'PM','Saint Pierre and Miquelon'),(183,'PN','Pitcairn'),(184,'PR','Puerto Rico'),(185,'PS','Palestine, State of'),(186,'PT','Portugal'),(187,'PW','Palau'),(188,'PY','Paraguay'),(189,'QA','Qatar'),(190,'RE','Réunion'),(191,'RO','Romania'),(192,'RS','Serbia'),(193,'RU','Russian Federation (the)'),(194,'RW','Rwanda'),(195,'SA','Saudi Arabia'),(196,'SB','Solomon Islands'),(197,'SC','Seychelles'),(198,'SD','Sudan'),(199,'SE','Sweden'),(200,'SG','Singapore'),(201,'SI','Slovenia'),(202,'SJ','Svalbard and Jan Mayen'),(203,'SK','Sikkim'),(204,'SK','Slovakia'),(205,'SL','Sierra Leone'),(206,'SM','San Marino'),(207,'SN','Senegal'),(208,'SO','Somalia'),(209,'SR','Suriname'),(210,'SS','South Sudan'),(211,'ST','Sao Tome and Principe'),(212,'SV','El Salvador'),(213,'SX','Sint Maarten (Dutch part)'),(214,'SY','Syrian Arab Republic'),(215,'SZ','Swaziland'),(216,'TC','Turks and Caicos Islands'),(217,'TD','Chad'),(218,'TG','Togo'),(219,'TH','Thailand'),(220,'TJ','Tajikistan'),(221,'TK','Tokelau'),(222,'TL','Timor-Leste'),(223,'TM','Turkmenistan'),(224,'TN','Tunisia'),(225,'TO','Tonga'),(226,'TP','East Timor'),(227,'TR','Turkey'),(228,'TT','Trinidad and Tobago'),(229,'TV','Tuvalu'),(230,'TW','Taiwan, Province of China'),(231,'TZ','Tanzania, United Republic of'),(232,'UA','Ukraine'),(233,'UG','Uganda'),(234,'US','United States of America'),(235,'UY','Uruguay'),(236,'UZ','Uzbekistan'),(237,'VA','Holy See'),(238,'VC','Saint Vincent and the Grenadines'),(239,'VE','Venezuela (Bolivarian Republic of)'),(240,'VG','Virgin Islands (British)'),(241,'VI','Virgin Islands (U.S.)'),(242,'VN','Vietnam'),(243,'VU','Vanuatu'),(244,'WS','Samoa'),(245,'YE','Yemen'),(246,'YT','Mayotte'),(247,'YU','Yugoslavia'),(248,'ZA','South Africa'),(249,'ZM','Zambia'),(250,'ZR','Zaire'),(251,'ZW','Zimbabwe');
/*!40000 ALTER TABLE `GeoCountry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-09 12:21:54
