-- --------------------------------------------------------
-- Host:                         192.168.33.33
-- Server version:               10.1.8-MariaDB-1~trusty-log - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

delete from Sku;
delete from SkuCulture;
delete from SkuCategory;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping data for table maymay.Sku: ~4 rows (approximately)
/*!40000 ALTER TABLE `Sku` DISABLE KEYS */;
INSERT INTO `Sku` (`SkuId`, `StyleCode`, `Upc`, `Bar`, `BrandId`, `BadgeId`, `SeasonId`, `SizeId`, `ColorId`, `GeoCountryId`, `LaunchYear`, `ManufacturerName`, `PriceRetail`, `PriceSale`, `SaleFrom`, `SaleTo`, `AvailableFrom`, `AvailableTo`, `WeightKg`, `HeightCm`, `WidthCm`, `LengthCm`, `MerchantId`, `StatusId`, `LastModified`) VALUES
	(30, 'ProdOne', '445356', '75385', 2, 0, 4, 16, 4, 13, 2009, 'Tata Group', 500.99, 400.50, '2015-11-10 17:00:00', '2015-11-16 17:00:00', '2016-01-10 17:00:00', '2016-01-16 17:00:00', 2, 5, 50, 100, 0, 3, '2015-11-17 04:46:41'),
	(31, 'ProdOne', '445357', '75386', 2, 0, 4, 17, 5, 13, 2009, 'Tata Group', 500.99, 400.50, '2015-11-10 17:00:00', '2015-11-16 17:00:00', '2016-01-10 17:00:00', '2016-01-16 17:00:00', 2, 5, 50, 100, 0, 3, '2015-11-17 04:46:41'),
	(32, 'ProdTwo', '445358', '75388', 3, 0, 3, 18, 6, 13, 2009, 'Tata Group', 500.99, 400.50, '2015-11-10 17:00:00', '2015-11-16 17:00:00', '2016-01-10 17:00:00', '2016-01-16 17:00:00', 2, 5, 40, 100, 0, 3, '2015-11-17 04:46:41'),
	(33, 'ProdTwo', '445359', '75389', 3, 0, 3, 19, 7, 13, 2009, 'Tata Group', 500.99, 400.50, '2015-11-10 17:00:00', '2015-11-16 17:00:00', '2016-01-10 17:00:00', '2016-01-16 17:00:00', 2, 5, 40, 100, 0, 3, '2015-11-17 04:46:41');
/*!40000 ALTER TABLE `Sku` ENABLE KEYS */;

-- Dumping data for table maymay.SkuCategory: ~24 rows (approximately)
/*!40000 ALTER TABLE `SkuCategory` DISABLE KEYS */;
INSERT INTO `SkuCategory` (`SkuCategoryId`, `SkuId`, `CategoryId`, `Priority`, `LastModified`) VALUES
	(179, 30, 1, 0, '2015-11-17 04:46:41'),
	(180, 30, 11, 1, '2015-11-17 04:46:41'),
	(181, 30, 0, 2, '2015-11-17 04:46:41'),
	(182, 30, 0, 3, '2015-11-17 04:46:41'),
	(183, 30, 0, 4, '2015-11-17 04:46:41'),
	(184, 30, 0, 5, '2015-11-17 04:46:41'),
	(185, 31, 1, 0, '2015-11-17 04:46:41'),
	(186, 31, 11, 1, '2015-11-17 04:46:41'),
	(187, 31, 0, 2, '2015-11-17 04:46:41'),
	(188, 31, 0, 3, '2015-11-17 04:46:41'),
	(189, 31, 0, 4, '2015-11-17 04:46:41'),
	(190, 31, 0, 5, '2015-11-17 04:46:41'),
	(191, 32, 2, 0, '2015-11-17 04:46:41'),
	(192, 32, 1, 1, '2015-11-17 04:46:41'),
	(193, 32, 0, 2, '2015-11-17 04:46:41'),
	(194, 32, 0, 3, '2015-11-17 04:46:41'),
	(195, 32, 0, 4, '2015-11-17 04:46:41'),
	(196, 32, 0, 5, '2015-11-17 04:46:41'),
	(197, 33, 2, 0, '2015-11-17 04:46:41'),
	(198, 33, 1, 1, '2015-11-17 04:46:41'),
	(199, 33, 0, 2, '2015-11-17 04:46:41'),
	(200, 33, 0, 3, '2015-11-17 04:46:41'),
	(201, 33, 0, 4, '2015-11-17 04:46:41'),
	(202, 33, 0, 5, '2015-11-17 04:46:41');
/*!40000 ALTER TABLE `SkuCategory` ENABLE KEYS */;

-- Dumping data for table maymay.SkuCulture: ~12 rows (approximately)
/*!40000 ALTER TABLE `SkuCulture` DISABLE KEYS */;
INSERT INTO `SkuCulture` (`SkuCultureId`, `SkuId`, `CultureCode`, `SkuName`, `SkuDesc`, `SkuFeature`, `SkuMaterial`, `SkuColor`, `LastModified`) VALUES
	(61, 30, 'EN', 'Fluffy Jacket', 'Nice', 'Soft', 'Felt', 'Yellow', '2015-11-17 04:46:41'),
	(62, 30, 'CHS', '产品名称', '产品介绍', '产品特征', '材料 ', '色彩名称', '2015-11-17 04:46:41'),
	(63, 30, 'CHT', '產品名稱', '產品介紹', '產品特徵', '材料 ', '色彩名稱', '2015-11-17 04:46:41'),
	(64, 31, 'EN', 'Fluffy Jacket', 'Nice', 'Soft', 'Felt', 'Strange', '2015-11-17 04:46:41'),
	(65, 31, 'CHS', '产品名称', '产品介绍', '产品特征', '材料 ', '色彩名称', '2015-11-17 04:46:41'),
	(66, 31, 'CHT', '產品名稱', '產品介紹', '產品特徵', '材料 ', '色彩名稱', '2015-11-17 04:46:41'),
	(67, 32, 'EN', 'Funny Thing', 'Cute', 'Hard', 'Plastic', 'Odd', '2015-11-17 04:46:41'),
	(68, 32, 'CHS', '产品名称', '产品介绍', '产品特征', '材料 ', '色彩名称', '2015-11-17 04:46:41'),
	(69, 32, 'CHT', '產品名稱', '產品介紹', '產品特徵', '材料 ', '色彩名稱', '2015-11-17 04:46:41'),
	(70, 33, 'EN', 'Funny Thing', 'Cute', 'Hard', 'Plastic', 'Weird', '2015-11-17 04:46:41'),
	(71, 33, 'CHS', '产品名称', '产品介绍', '产品特征', '材料 ', '色彩名称', '2015-11-17 04:46:41'),
	(72, 33, 'CHT', '產品名稱', '產品介紹', '產品特徵', '材料 ', '色彩名稱', '2015-11-17 04:46:41');
/*!40000 ALTER TABLE `SkuCulture` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


Update Sku set MerchantId=1;