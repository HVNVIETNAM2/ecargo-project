/**clear the table MessagingTemplate**/
delete from MessagingTemplate;

/**load all template message**/
/** create user message**/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('CREATE_USER', 'EN','{{FirstName}}, a warm welcome to MM!',
  'Dear {{FirstName}},<p><p> Your MM account has been created! Please go to <a href="{{RootURL}}/activate?at={{ActivationToken}}"> activated </a> and complete your registration. This invitation is valid for only 24 hours. <p><p>Sincerely,<p> MM Team',
  'MM code: {{ActivationToken}}. valid for 24 hour.');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('CREATE_USER', 'CHT', '{{FirstName}},  歡迎來到 MM!',
	'親愛的 {{FirstName}}, 您的MM已經創建！請點擊<a href="{{RootURL}}/activate?at={{ActivationToken}}">激活</a>并完成註冊。這個邀請僅對24小时有效。MM 團隊',
	'MM 註冊碼： {{ActivationToken}}。 24 小時內有效。');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('CREATE_USER', 'CHS', '{{FirstName}},  欢迎来到 MM!',
	'亲爱的 {{FirstName}}, 您的MM已经创建！请点击<a href="{{RootURL}}/activate?at={{ActivationToken}}">激活</a>并完成注册。这个邀请仅对24小时有效。MM 团队',
  'MM 注册码: {{ActivationToken}}。 24 小时内有效。');

/**login failed many times and lock the account message**/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('SIGNIN_ATTEMPTS_EXCEEDED', 'EN', '{{FirstName}} account locked due to excessive sign-in attempts',
  'Dear {{FirstName}},<p><p>    Your account has been locked due to excessive sign-in attempts within time limit. Please sign-in with <a href="{{RootURL}}/activate?at={{ActivationToken}}">link</a> and reactivate your account.<p><p> This link is valid for only 24hours.<p><p> Sincerely,<p>MM Team',
  '{{FirstName}} Your account has been locked due to excessive sign-in attempts');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('SIGNIN_ATTEMPTS_EXCEEDED', 'CHT', '{{FirstName}} 帐户锁定由于过度登录尝试',
  '親愛的 {{FirstName}},<p><p>    您的賬戶被鎖定. 请点击链接激活账户 <a href="{{RootURL}}/activate?at={{ActivationToken}}">激活</a> .<p><p> 此鏈接僅對24小時。 <p> <p>真誠， <p> MM團隊',
  '{{FirstName}} 您的賬戶被鎖定');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('SIGNIN_ATTEMPTS_EXCEEDED', 'CHS', '{{FirstName}} 帳戶鎖定由於過度登錄嘗試',
  '亲爱的 {{FirstName}},<p><p>    您的账户被锁定. 请点击链接激活账户 <a href="{{RootURL}}/activate?at={{ActivationToken}}">激活</a> .<p><p> 此链接仅对24小时。 <p> <p>真诚， <p> MM团队',
  '{{FirstName}} 您的賬戶被鎖定');

/**activate user message**/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('ACTIVATE_USER', 'EN', '{{FirstName}}, your MM account has been activated!',
  'Dear {{FirstName}},<p><p> Your MM account has been activated! You can now <a href="{{RootURL}}">login</a><p><p>Sincerely,<p>MM Team',
  '{{FirstName}} Your MM account has been activated');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('ACTIVATE_USER', 'CHT', '{{FirstName}}, 您的MM​​账户已被激活!',
  '親愛的 {{FirstName}},<p><p>    您的MM​​账户已被激活，请 <a href="{{RootURL}}">登录</a> .<p><p> 真誠， <p> MM團隊',
   '{{FirstName}} 您的MM​​账户已被激活');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('ACTIVATE_USER', 'CHS', '{{FirstName}}, 您的MM​​账户已被激活!',
  '亲爱的 {{FirstName}},<p><p>    您的MM​​账户已被激活，请  <a href="{{RootURL}}">登录</a> .<p><p> 真诚， <p> MM团队',
  '{{FirstName}} 您的MM​​账户已被激活');

/**inactivate user message**/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('INACTIVATE_USER', 'EN', '{{FirstName}}, your MM account has been inactivated!',
  'Dear {{FirstName}},<p><p> Your MM account has been inactivated! Please contact your admin to resolve the issue.<p><p>Sincerely,<p>MM Team',
  '{{FirstName}} Your MM account has been inactivated');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('INACTIVATE_USER', 'CHT', '{{FirstName}}, 您的MM​​账户已被暂停!',
  '親愛的 {{FirstName}},<p><p>    您的MM​​账户已被暂停，请联系管理员.<p><p> 真誠， <p> MM團隊',
   '{{FirstName}} 您的MM​​账户已被暂停');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('INACTIVATE_USER', 'CHS', '{{FirstName}}, 您的MM​​账户已被暂停!',
  '亲爱的 {{FirstName}},<p><p>    您的MM​​账户已被暂停，请联系管理员.<p><p> 真诚， <p> MM团队',
  '{{FirstName}} 您的MM​​账户已被暂停');

/**change user email message**/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('CHANGE_EMAIL', 'EN', '{{FirstName}}, your email for MM has been changed',
  'Dear {{FirstName}},<p><p> The email for your MM account has been changed. Please verify your email by following <a href="{{RootURL}}/login?at={{ActivationToken}}"> activate</a> to complete the process. This link is valid for only 24hours. <p><p>Sincerely,<p>MM Team',
  '{{FirstName}} The email for your MM account has been changed');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('CHANGE_EMAIL', 'CHT', '{{FirstName}}, 您的電子郵件已被更改',
  'Dear {{FirstName}},<p><p> 您的電子郵件已被更改. Please verify your email by following <a href="{{RootURL}}/login?at={{ActivationToken}}"> activate</a> to complete the process. This link is valid for only 24hours. <p> <p>真誠， <p> MM團隊',
  '{{FirstName}} 您的電子郵件已被更改');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('CHANGE_EMAIL', 'CHS', '{{FirstName}}, 您的电子邮件已被更改',
  'Dear {{FirstName}},<p><p> 您的电子邮件已被更改. Please verify your email by following <a href="{{RootURL}}/login?at={{ActivationToken}}"> activate</a> to complete the process. This link is valid for only 24hours. <p> <p>真诚， <p> MM团队',
  '{{FirstName}} 您的电子邮件已被更改');

/**reset password message**/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('RESET_PASSWORD', 'EN', '{{FirstName}}, your password for MM has been reset',
  'Dear {{FirstName}},<p><p> The password for your MM account has been reset. Please verify your account by following <a href="{{RootURL}}/resetPass?at={{ActivationToken}}"> reset</a> to complete the process. <p><p>Sincerely,<p>MM Team',
  '{{FirstName}} The password for your MM account has been reset');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('RESET_PASSWORD', 'CHT', '{{FirstName}} 您的密碼被重置',
  '您{{FirstName}} 的密碼已被重置，<a href="{{RootURL}}/resetPass?at={{ActivationToken}}"> 設置新密碼</a> <p> <p>真誠， <p> MM團隊',
  '{{FirstName}} 您的密碼被重置');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('RESET_PASSWORD', 'CHS', '{{FirstName}} 您的密码被重置',
  '您{{FirstName}} 的密码已被重置，<a href="{{RootURL}}/resetPass?at={{ActivationToken}}"> 設置新密碼</a> <p> <p>真誠， <p> MM團隊',
  '{{FirstName}} 您的密码被重置');

/**forget password message**/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('FORGOT_PASSWORD', 'EN', '{{FirstName}}, your password for MM has been reset',
  'Dear {{FirstName}},<p><p> The password for your MM account has been reset. Please verify your account by following <a href="{{RootURL}}/resetPass?at={{ActivationToken}}"> reset</a> to complete the process.  <p><p>Sincerely,<p>MM Team',
  '{{FirstName}} your password for MM has been reset, you can type MM code: {{ActivationToken}} to active your new mobile. valid for 24 hour.');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('FORGOT_PASSWORD', 'CHT', '{{FirstName}} 您的密碼被重置',
  '您{{FirstName}} 的密碼已被重置，<a href="{{RootURL}}/resetPass?at={{ActivationToken}}"> 設置新密碼</a> 設置新密碼。<p> <p>真誠， <p> MM團隊',
  '{{FirstName}} 您的密碼被重置, 請輸入激活碼: {{ActivationToken}} 激活新的手機號碼. 24小時有效.');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('FORGOT_PASSWORD', 'CHS', '{{FirstName}} 您的密码被重置',
  '您{{FirstName}} 的密码已被重置，<a href="{{RootURL}}/resetPass?at={{ActivationToken}}"> 設置新密碼</a> 設置新密碼。<p> <p>真誠， <p> MM團隊',
  '{{FirstName}} 您的密码被重置, 请输入激活码: {{ActivationToken}} 激活新的手机号码. 24小时有效.');

/** change mobile message**/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('CHANGE_MOBILE', 'EN','{{FirstName}}, Your mobile has been changed!',
  'Dear {{FirstName}},<p><p> Your mobile has been changed! please <a href="{{RootURL}}/login?at={{ActivationToken}}">activate</a>',
  'Your mobile has been changed, you can type MM code: {{ActivationToken}} to active your new mobile. valid for 24 hour.');
/** change mobile message**/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('CHANGE_MOBILE', 'CHT','{{FirstName}}, 你的手機號碼被修改',
  '親愛的 {{FirstName}},<p><p> 你的手機號碼已被修改, <a href="{{RootURL}}/login?at={{ActivationToken}}">激活</a>!',
  '你的手機號碼已被修改，請輸入激活碼: {{ActivationToken}} 激活新的手機號碼. 24小時有效.');
/** change mobile message**/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('CHANGE_MOBILE', 'CHS','{{FirstName}}, 你的手机号码被修改！',
  '亲爱的 {{FirstName}},<p><p> 你的手机号码已被修改! <a href="{{RootURL}}/login?at={{ActivationToken}}">激活</a>!',
  '你的手机号码已被修改，请输入激活码: {{ActivationToken}} 激活新的手机号码. 24小时有效.');

/** Resend activation (only for mobile) **/
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('RESEND_ACTIVATION', 'EN','Hi {{FirstName}}, this is your mobile activation code {{ActivationToken}}',
  'Hi {{FirstName}}, this is your mobile activation code {{ActivationToken}}',
  'Hi {{FirstName}}, this is your mobile activation code {{ActivationToken}}');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('RESEND_ACTIVATION', 'CHT','你好{{FirstName}}, 這是您的手機激活碼 {{ActivationToken}}',
  '你好{{FirstName}}, 這是您的手機激活碼 {{ActivationToken}}',
  '你好{{FirstName}}, 這是您的手機激活碼 {{ActivationToken}}');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('RESEND_ACTIVATION', 'CHS','你好{{FirstName}}, 这是您的手机激活码 {{ActivationToken}}',
  '你好{{FirstName}}, 这是您的手机激活码 {{ActivationToken}}',
  '你好{{FirstName}}, 这是您的手机激活码 {{ActivationToken}}');
