/*
	create messaging template table for storing the email&&sms template
*/
CREATE TABLE `MessagingTemplate` (
	`MessagingTemplateId` INT NOT NULL AUTO_INCREMENT,
	`MessagingTemplateCode` VARCHAR(255) NOT NULL,
	`CultureCode` VARCHAR(50) NOT NULL,
	`EmailTemplateSubject` VARCHAR(2000) NULL DEFAULT NULL,
  `EmailTemplateContent` VARCHAR(15000) NULL DEFAULT NULL,
  `SmsTemplateContent` VARCHAR(2000) NULL DEFAULT NULL,
	PRIMARY KEY (`MessagingTemplateId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
