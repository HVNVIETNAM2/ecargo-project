-- MySQL dump 10.16  Distrib 10.1.7-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: maymay
-- ------------------------------------------------------
-- Server version	10.1.7-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `SecurityGroupCulture`
--

DROP TABLE IF EXISTS `SecurityGroupCulture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SecurityGroupCulture` (
  `SecurityGroupCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `SecurityGroupId` int(11) NOT NULL DEFAULT '0',
  `CultureCode` varchar(50) DEFAULT NULL,
  `SecurityGroupName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`SecurityGroupCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SecurityGroupCulture`
--

LOCK TABLES `SecurityGroupCulture` WRITE;
/*!40000 ALTER TABLE `SecurityGroupCulture` DISABLE KEYS */;
INSERT INTO `SecurityGroupCulture` VALUES (1,1,'EN','MM Admin'),(2,1,'CHT','時尚家居 管理員'),(3,1,'CHS','时尚家居 管理员'),(4,2,'EN','MM Customer Service'),(5,2,'CHT','時尚家居 客服'),(6,2,'CHS','时尚家居 顾客服务'),(7,3,'EN','MM Finance'),(8,3,'CHT','時尚家居 財經'),(9,3,'CHS','时尚家居 财经'),(10,4,'EN','MM Technology'),(11,4,'CHT','時尚家居 技術'),(12,4,'CHS','时尚家居 技术'),(13,5,'EN','Finance'),(14,5,'CHT','財經'),(15,5,'CHS','财经'),(16,6,'EN','Admin'),(17,6,'CHT','管理員'),(18,6,'CHS','管理員'),(40,7,'EN','SuperAdmin'),(41,7,'CHS','主管理员'),(42,7,'CHT','主營業員'),(43,9,'EN','Content Manager'),(44,9,'CHS','设计'),(45,9,'CHT','設計'),(46,10,'EN','Merchandiser'),(47,10,'CHS','采购'),(48,10,'CHT','採購'),(49,11,'EN','Order Processsing'),(50,11,'CHS','收单'),(51,11,'CHT','收單'),(52,12,'EN','Product Return'),(53,12,'CHS','退货'),(54,12,'CHT','退貨');
/*!40000 ALTER TABLE `SecurityGroupCulture` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-03 15:37:18
