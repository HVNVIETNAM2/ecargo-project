/** create user message**/
UPDATE MessagingTemplate SET
EmailTemplateSubject='Complete Your Registration',
EmailTemplateContent='Dear {{FirstName}},<p><p>Your {{FirstName}} account has been created! Please click <a href="{{RootURL}}/activate?at={{ActivationToken}}">here</a> and complete your registration.<p><p>This invitation is valid for only 24 hours.<p><p>Sincerely,<p><p>MM Team<p><p>',
SmsTemplateContent='MM code: {{ActivationToken}}. valid for 7 days.'
WHERE MessagingTemplateCode="CREATE_USER" AND CultureCode="EN";

UPDATE MessagingTemplate SET
EmailTemplateSubject='請激活賬戶',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>您的帳戶已被創建，請點擊<a href="{{RootURL}}/activate?at={{ActivationToken}}">這裡</a> 激活賬戶並完成註冊。<p><p>此鏈接有效時間為24小時。請盡快完成激活。<p><p>謝謝！<p>美美<p>美美，就是愛美<p>',
SmsTemplateContent='MM 註冊碼: {{ActivationToken}}。 7天內有效。'
WHERE MessagingTemplateCode="CREATE_USER" AND CultureCode="CHT";


UPDATE MessagingTemplate SET
EmailTemplateSubject='请激活账户',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>您的帐户已被创建，请点击<a href="{{RootURL}}/activate?at={{ActivationToken}}">这裡</a> 激活账户并完成注册。<p><p>此链接有效时间为24小时。请尽快完成激活。<p><p>谢谢！<p>美美<p>美美，就是爱美<p>',
SmsTemplateContent='MM 注册码: {{ActivationToken}}。 7天内有效。'
WHERE MessagingTemplateCode="CREATE_USER" AND CultureCode="CHS";


/**login failed many times and lock the account message**/
UPDATE MessagingTemplate SET
EmailTemplateSubject='Account Locked',
EmailTemplateContent='Dear {{FirstName}},<p><p>Your {{FirstName}} account has been locked due to excessive sign-in attempts within time limit. Please sign-in <a href="{{RootURL}}/activate?at={{ActivationToken}}">here</a> and reactivate your account.<p><p>This link is valid for only 24 hours.<p><p>Sincerely,<p><p>MM Team<p><p>',
SmsTemplateContent='MM code: {{ActivationToken}}. valid for 24 hours.'
WHERE MessagingTemplateCode="SIGNIN_ATTEMPTS_EXCEEDED" AND CultureCode="EN";

UPDATE MessagingTemplate SET
EmailTemplateSubject='帳戶被凍結',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>十分抱歉，您的嘗試登錄次數超出上限，我們已將您的帳戶凍結。請訪問<a href="{{RootURL}}/activate?at={{ActivationToken}}">這裡</a>驗證您的帳戶並重置密碼。<p><p>此鏈接有效時間為24小時。請盡快完成賬戶驗證及密碼重置。<p><p>謝謝！<p><p>美美<p><p>美美，就是愛美<p><p>',
SmsTemplateContent='MM 註冊碼: {{ActivationToken}}。 24小時內有效。'
WHERE MessagingTemplateCode="SIGNIN_ATTEMPTS_EXCEEDED" AND CultureCode="CHT";

UPDATE MessagingTemplate SET
EmailTemplateSubject='账户被冻结',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>十分抱歉，您的尝试登录次数超出上限，我们已将您的帐户冻结。请访问<a href="{{RootURL}}/activate?at={{ActivationToken}}">这裡</a>验证您的帐户并重置密码。<p><p>此链接有效时间为24小时。请尽快完成账户验证及密码重置。<p><p>谢谢！<p><p>美美<p><p>美美，就是爱美<p><p>',
SmsTemplateContent='MM 注册码: {{ActivationToken}}。 24小时内有效。'
WHERE MessagingTemplateCode="SIGNIN_ATTEMPTS_EXCEEDED" AND CultureCode="CHS";


/**activate user message**/
UPDATE MessagingTemplate SET
EmailTemplateSubject='Account Activated',
EmailTemplateContent='Dear {{FirstName}},<p><p>Your {{FirstName}} account has been activated! You can now login to <a href="{{RootURL}}">{{RootURL}}</a><p><p>Sincerely,<p><p>MM Team<p><p>',
SmsTemplateContent='MM code: {{ActivationToken}}. valid for 24 hours.'
WHERE MessagingTemplateCode="ACTIVATE_USER" AND CultureCode="EN";

UPDATE MessagingTemplate SET
EmailTemplateSubject='帳戶已被激活',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>您的帳戶已被重新激活，您現在可以正常登錄和使用 <a href="{{RootURL}}">{{RootURL}}</a><p><p>謝謝！p><p>美美<p><p>美美，就是愛美<p><p>',
SmsTemplateContent='MM 註冊碼: {{ActivationToken}}。 24小時內有效。'
WHERE MessagingTemplateCode="ACTIVATE_USER" AND CultureCode="CHT";

UPDATE MessagingTemplate SET
EmailTemplateSubject='账户已被激活',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>您的帐户已被重新激活，您现在可以正常登录和使用 <a href="{{RootURL}}">{{RootURL}}</a><p><p>谢谢！p><p>美美<p><p>美美，就是爱美<p><p>',
SmsTemplateContent='MM 注册码: {{ActivationToken}}。 24小时内有效。'
WHERE MessagingTemplateCode="ACTIVATE_USER" AND CultureCode="CHS";


/**inactivate user message**/
UPDATE MessagingTemplate SET
EmailTemplateSubject='Account Inactivated',
EmailTemplateContent='Dear {{FirstName}},<p><p>Your {{FirstName}} account has been inactivated. Please contact your admin to resolve the issue.<p><p>Sincerely,<p><p>MM Team<p><p>',
SmsTemplateContent='MM code: {{ActivationToken}}. valid for 24 hours.'
WHERE MessagingTemplateCode="INACTIVATE_USER" AND CultureCode="EN";

UPDATE MessagingTemplate SET
EmailTemplateSubject='帳戶已被凍結',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>您的帳戶已被凍結，請聯繫您的管理員。<p><p>謝謝<p><p>美美<p><p>美美，就是愛美<p><p>',
SmsTemplateContent='MM 註冊碼: {{ActivationToken}}。 24小時內有效。'
WHERE MessagingTemplateCode="INACTIVATE_USER" AND CultureCode="CHT";

UPDATE MessagingTemplate SET
EmailTemplateSubject='帳戶已被冻结',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>您的帐户已被冻结，请联系您的管理员。<p><p>谢谢<p><p>美美<p><p>美美，就是爱美<p><p>',
SmsTemplateContent='MM 注册码: {{ActivationToken}}。 24小时内有效。'
WHERE MessagingTemplateCode="INACTIVATE_USER" AND CultureCode="CHS";


/**change user email message**/
UPDATE MessagingTemplate SET
EmailTemplateSubject='Change Email Address',
EmailTemplateContent='Dear {{FirstName}},<p><p>The email for your {{FirstName}} account has been changed. Please verify your email by following <a href="{{RootURL}}/login?at={{ActivationToken}}">here</a> to complete the process.<p><p>This link is valid for only 24 hours.<p><p>Sincerely,<p><p>MM Team<p><p>',
SmsTemplateContent='MM code: {{ActivationToken}}. valid for 24 hours.'
WHERE MessagingTemplateCode="CHANGE_EMAIL" AND CultureCode="EN";

UPDATE MessagingTemplate SET
EmailTemplateSubject='註冊郵箱已更改',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>{{FirstName}}的郵箱地址已經被更新，請點擊<a href="{{RootURL}}/login?at={{ActivationToken}}">這裡</a>登錄並確認。<p><p>此鏈接有效時間為24小時。請盡快完成激活。<p><p>謝謝！<p><p>美美<p><p>美美，就是愛美<p><p>',
SmsTemplateContent='MM 註冊碼: {{ActivationToken}}。 24小時內有效。'
WHERE MessagingTemplateCode="CHANGE_EMAIL" AND CultureCode="CHT";

UPDATE MessagingTemplate SET
EmailTemplateSubject='注册邮箱已更改',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>{{FirstName}}的邮箱地址已经被更新，请点击<a href="{{RootURL}}/login?at={{ActivationToken}}">这裡</a>登录并确认。<p><p>此链接有效时间为24小时。请尽快完成激活。<p><p>谢谢！<p><p>美美<p><p>美美，就是爱美<p><p>',
SmsTemplateContent='MM 注册码: {{ActivationToken}}。 24小时内有效。'
WHERE MessagingTemplateCode="CHANGE_EMAIL" AND CultureCode="CHS";


/**reset password message**/
UPDATE MessagingTemplate SET
EmailTemplateSubject='Reset Password',
EmailTemplateContent='Dear {{FirstName}},<p><p>The password for your {{FirstName}} account has been reset. Please verify your account by following <a href="{{RootURL}}/resetPass?at={{ActivationToken}}">here</a> to complete the process.<p><p>This link is valid for only 24 hours.<p><p>Sincerely,<p><p>MM Team<p><p>',
SmsTemplateContent='MM code: {{ActivationToken}}. valid for24 hours.'
WHERE MessagingTemplateCode="RESET_PASSWORD" AND CultureCode="EN";

UPDATE MessagingTemplate SET
EmailTemplateSubject='密碼重置',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>{{FirstName}}的密碼已被重置，請訪問<a href="{{RootURL}}/resetPass?at={{ActivationToken}}">這裡</a>設置新密碼。<p><p>此鏈接有效時間為24小時。請盡快完成激活。<p><p>謝謝！<p><p>美美<p><p>美美，就是愛美<p><p>',
SmsTemplateContent='MM 註冊碼: {{ActivationToken}}。 24小時內有效。'
WHERE MessagingTemplateCode="RESET_PASSWORD" AND CultureCode="CHT";

UPDATE MessagingTemplate SET
EmailTemplateSubject='密码重置',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>{{FirstName}}的密码已被重置，请访问<a href="{{RootURL}}/resetPass?at={{ActivationToken}}">这裡</a>设置新密码。<p><p>此链接有效时间为24小时。请尽快完成激活。<p><p>谢谢！<p><p>美美<p><p>美美，就是爱美<p><p>',
SmsTemplateContent='MM 注册码: {{ActivationToken}}。 24小时内有效。'
WHERE MessagingTemplateCode="RESET_PASSWORD" AND CultureCode="CHS";


/**forget password message**/
UPDATE MessagingTemplate SET
EmailTemplateSubject='Reset Password',
EmailTemplateContent='Dear {{FirstName}},<p><p>The password for your {{FirstName}} account has been reset. Please verify your account by following <a href="{{RootURL}}/resetPass?at={{ActivationToken}}">here</a> to complete the process.<p><p>This link is valid for only 24 hours.<p><p>Sincerely,<p><p>MM Team<p><p>',
SmsTemplateContent='MM code: {{ActivationToken}}. valid for24 hours.'
WHERE MessagingTemplateCode="FORGOT_PASSWORD" AND CultureCode="EN";

UPDATE MessagingTemplate SET
EmailTemplateSubject='密碼重置',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>{{FirstName}}的密碼已被重置，請訪問<a href="{{RootURL}}/resetPass?at={{ActivationToken}}">這裡</a>設置新密碼。<p><p>此鏈接有效時間為24小時。請盡快完成激活。<p><p>謝謝！<p><p>美美<p><p>美美，就是愛美<p><p>',
SmsTemplateContent='MM 註冊碼: {{ActivationToken}}。 24小時內有效。'
WHERE MessagingTemplateCode="FORGOT_PASSWORD" AND CultureCode="CHT";

UPDATE MessagingTemplate SET
EmailTemplateSubject='密码重置',
EmailTemplateContent='尊敬的{{FirstName}}：<p><p>{{FirstName}}的密码已被重置，请访问<a href="{{RootURL}}/resetPass?at={{ActivationToken}}">这裡</a>设置新密码。<p><p>此链接有效时间为24小时。请尽快完成激活。<p><p>谢谢！<p><p>美美<p><p>美美，就是爱美<p><p>',
SmsTemplateContent='MM 注册码: {{ActivationToken}}。 24小时内有效。'
WHERE MessagingTemplateCode="FORGOT_PASSWORD" AND CultureCode="CHS";

