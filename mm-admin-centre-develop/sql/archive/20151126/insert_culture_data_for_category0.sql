UPDATE Category SET CategoryNameInvariant = '---' WHERE CategoryId = 0;

INSERT INTO CategoryCulture (CategoryId, CultureCode, CategoryName) VALUES (0, 'EN', '---');
INSERT INTO CategoryCulture (CategoryId, CultureCode, CategoryName) VALUES (0, 'CHT', '---');
INSERT INTO CategoryCulture (CategoryId, CultureCode, CategoryName) VALUES (0, 'CHS', '---');
