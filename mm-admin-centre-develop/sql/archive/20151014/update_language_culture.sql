/*update Language name*/
update LanguageCulture set LanguageName='中文简体' where LanguageCultureId=5;
update LanguageCulture set LanguageName='中文簡體' where LanguageCultureId=6;
update LanguageCulture set LanguageName='中文繁体' where LanguageCultureId=8;
update LanguageCulture set LanguageName='中文繁體' where LanguageCultureId=9;

/*update time zone name*/
update TimeZoneCulture set TimeZoneName='北京时间' where TimeZoneCultureId=2;
update TimeZoneCulture set TimeZoneName='北京時間' where TimeZoneCultureId=3;
update TimeZoneCulture set TimeZoneName='香港时间' where TimeZoneCultureId=5;
update TimeZoneCulture set TimeZoneName='香港時間' where TimeZoneCultureId=6;
