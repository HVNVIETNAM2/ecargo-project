CREATE TABLE `CategoryBrandMerchant` (
	`CategoryBrandMerchant` INT(11) NOT NULL AUTO_INCREMENT,
	`CategoryId` INT(11) NOT NULL DEFAULT '0',
	`Entity` VARCHAR(50) NOT NULL DEFAULT '0',
	`EntityId` INT(11) NOT NULL DEFAULT '0',
	`Priority` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`CategoryBrandMerchant`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;