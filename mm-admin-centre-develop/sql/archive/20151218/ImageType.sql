-- Create ImageType table for controlling display order of product default image
CREATE TABLE `maymay`.`ImageType` (
  `ImageTypeId` INT(11) NOT NULL AUTO_INCREMENT,
  `ImageTypeCode` VARCHAR(255) NOT NULL,
  `Priority` INT(11) NOT NULL,
  PRIMARY KEY (`ImageTypeId`));

-- Initialize the data
INSERT INTO `maymay`.`ImageType` (`ImageTypeId`, `ImageTypeCode`, `Priority`) VALUES ('1', 'Feature', '1');
INSERT INTO `maymay`.`ImageType` (`ImageTypeId`, `ImageTypeCode`, `Priority`) VALUES ('2', 'Color', '2');
INSERT INTO `maymay`.`ImageType` (`ImageTypeId`, `ImageTypeCode`, `Priority`) VALUES ('3', 'Desc', '3');


-- Add ImageTypeId at StyleImage
ALTER TABLE `maymay`.`StyleImage` 
ADD COLUMN `ImageTypeId` INT(11) NULL AFTER `ImageTypeCode`;


-- Update ImageTypeId in all record at wStyleImage
UPDATE maymay.StyleImage si
INNER JOIN maymay.ImageType it
SET si.ImageTypeId = it.ImageTypeId
WHERE si.ImageTypeCode = it.ImageTypeCode AND
si.ImageTypeCode IS NOT NULL;
