alter table Inventory modify IsPerpetual tinyint not null default 0;
alter table InventoryJournal modify IsPerpetual tinyint not null default 0;
update Inventory set IsPerpetual=0;
update InventoryJournal set IsPerpetual=0;