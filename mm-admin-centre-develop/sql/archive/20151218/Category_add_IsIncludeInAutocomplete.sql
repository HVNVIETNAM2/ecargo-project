ALTER TABLE `Category` ADD COLUMN `IsIncludeInAutocomplete` tinyint not null default 0 AFTER `CategoryNameInvariant`;
update Category set IsIncludeInAutocomplete=0;
update Category set IsIncludeInAutocomplete=1 where ParentCategoryId=0 and CategoryId<>0;