INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('BRAND_STATUS_CHANGED', 'EN','Brand Status Changed',
  'Dear {{DisplayName}},<p><p>Brand {{BrandName}} has been changed from {{StatusPrev}} to {{StatusCur}}.<p><p>Please note that the status of related products would be impacted.<p><p>Sincerely,<p><p>MM Team',
  'Dear {{DisplayName}},<p><p>Brand {{BrandName}} has been changed from {{StatusPrev}} to {{StatusCur}}.<p><p>Please note that the status of related products would be impacted.<p><p>Sincerely,<p><p>MM Team');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('BRAND_STATUS_CHANGED', 'CHT','品牌狀態已更新',
  '尊敬的 {{DisplayName}},<p><p>品牌{{BrandName}}状态已由{{StatusPrev}}改为{{StatusCur}}。<p><p>请注意相关商品的状态会受到影响。<p><p> 谢谢！<p><p> 美美 <p><p> 美美，就是爱美',
  '尊敬的 {{DisplayName}},<p><p>品牌{{BrandName}}状态已由{{StatusPrev}}改为{{StatusCur}}。<p><p>请注意相关商品的状态会受到影响。<p><p> 谢谢！<p><p> 美美 <p><p> 美美，就是爱美');
INSERT INTO MessagingTemplate (MessagingTemplateCode, CultureCode, EmailTemplateSubject, EmailTemplateContent, SmsTemplateContent)
VALUES ('BRAND_STATUS_CHANGED', 'CHS','品牌状态已更新',
  '尊敬的 {{DisplayName}},<p><p>品牌{{BrandName}}狀態已由{{StatusPrev}}改為{{StatusCur}}。<p><p>請注意相關商品的狀態會受到影響。<p><p> 謝謝！<p><p> 美美 <p><p> 美美，就是愛美',
  '尊敬的 {{DisplayName}},<p><p>品牌{{BrandName}}狀態已由{{StatusPrev}}改為{{StatusCur}}。<p><p>請注意相關商品的狀態會受到影響。<p><p> 謝謝！<p><p> 美美 <p><p> 美美，就是愛美');
