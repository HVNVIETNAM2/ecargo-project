ALTER TABLE Brand MODIFY IsListedBrand TINYINT NOT NULL DEFAULT 0;
-- Rename IsFeatureBrand to IsFeaturedBrand just to be consistent with Merchant.
ALTER TABLE Brand CHANGE IsFeatureBrand IsFeaturedBrand TINYINT NOT NULL DEFAULT 0;
ALTER TABLE Brand MODIFY IsRecommendedBrand TINYINT NOT NULL DEFAULT 0;
ALTER TABLE Brand MODIFY IsSearchable TINYINT NOT NULL DEFAULT 0;

UPDATE Brand SET IsListedBrand = 0, IsFeaturedBrand = 0, IsRecommendedBrand = 0, IsSearchable = 0;

ALTER TABLE Merchant MODIFY IsListedMerchant TINYINT NOT NULL DEFAULT 0;
ALTER TABLE Merchant MODIFY IsFeaturedMerchant TINYINT NOT NULL DEFAULT 0;
ALTER TABLE Merchant MODIFY IsRecommendedMerchant TINYINT NOT NULL DEFAULT 0;
ALTER TABLE Merchant MODIFY IsSuggestedSearchBar TINYINT NOT NULL DEFAULT 0;

UPDATE Merchant SET IsListedMerchant = 0, IsFeaturedMerchant = 0, IsRecommendedMerchant = 0, IsSuggestedSearchBar = 0;
