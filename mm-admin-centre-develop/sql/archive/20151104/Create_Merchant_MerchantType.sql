-- --------------------------------------------------------
-- Host:                         192.168.33.33
-- Server version:               10.1.7-MariaDB-1~trusty-log - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table maymay.Merchant
CREATE TABLE IF NOT EXISTS `Merchant` (
  `MerchantId` int(11) NOT NULL AUTO_INCREMENT,
  `MerchantTypeId` int(11) NOT NULL DEFAULT '0',
  `MerchantDisplayName` varchar(255) DEFAULT NULL,
  `MerchantCompanyName` varchar(255) DEFAULT NULL,
  `BusinessRegistrationNo` varchar(255) DEFAULT NULL,
  `MerchantSubdomain` varchar(255) DEFAULT NULL,
  `MerchantDesc` text,
  `LogoImage` varchar(255) DEFAULT NULL,
  `BackgroundImage` varchar(255) DEFAULT NULL,
  `GeoCountryId` int(11) NOT NULL DEFAULT '0',
  `GeoIdProvince` int(11) NOT NULL DEFAULT '0',
  `GeoIdCity` int(11) NOT NULL DEFAULT '0',
  `District` varchar(50) DEFAULT NULL,
  `PostalCode` varchar(50) DEFAULT NULL,
  `Apartment` varchar(50) DEFAULT NULL,
  `Floor` varchar(50) DEFAULT NULL,
  `BlockNo` varchar(50) DEFAULT NULL,
  `Building` varchar(50) DEFAULT NULL,
  `StreetNo` varchar(50) DEFAULT NULL,
  `Street` varchar(50) DEFAULT NULL,
  `StatusId` int(11) NOT NULL DEFAULT '0',
  `LastStatus` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MerchantId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.Merchant: ~3 rows (approximately)
/*!40000 ALTER TABLE `Merchant` DISABLE KEYS */;
INSERT INTO `Merchant` (`MerchantId`, `MerchantTypeId`, `MerchantDisplayName`, `MerchantCompanyName`, `BusinessRegistrationNo`, `MerchantSubdomain`, `MerchantDesc`, `LogoImage`, `BackgroundImage`, `GeoCountryId`, `GeoIdProvince`, `GeoIdCity`, `District`, `PostalCode`, `Apartment`, `Floor`, `BlockNo`, `Building`, `StreetNo`, `Street`, `StatusId`, `LastStatus`, `LastModified`) VALUES
	(11, 1, 'Mac Masters', 'Mac Masters', '1234', 'macmasters', 'Beach Wear for teh Office', NULL, NULL, 48, 2035607, 1806529, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2015-11-04 09:48:51', '2015-11-04 09:50:53'),
	(12, 2, 'Cool Wear', 'Cool Wear', '5678', 'cw', 'So cool its bad', NULL, NULL, 48, 2035607, 1806529, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2015-11-04 09:52:15', '2015-11-04 09:52:29'),
	(13, 3, 'Bibs Bubs', 'Bibs Bubs', '9111', 'bb', 'For babies and bears', NULL, NULL, 48, 2035607, 1806529, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2015-11-04 09:53:14', '2015-11-04 09:53:26');
/*!40000 ALTER TABLE `Merchant` ENABLE KEYS */;


-- Dumping structure for table maymay.MerchantType
CREATE TABLE IF NOT EXISTS `MerchantType` (
  `MerchantTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `MerchantTypeNameInvariant` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`MerchantTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.MerchantType: ~3 rows (approximately)
/*!40000 ALTER TABLE `MerchantType` DISABLE KEYS */;
INSERT INTO `MerchantType` (`MerchantTypeId`, `MerchantTypeNameInvariant`) VALUES
	(1, 'Brand Flagship'),
	(2, 'Mono-Brand Agent'),
	(3, 'Multi-Brand Agent');
/*!40000 ALTER TABLE `MerchantType` ENABLE KEYS */;


-- Dumping structure for table maymay.MerchantTypeCulture
CREATE TABLE IF NOT EXISTS `MerchantTypeCulture` (
  `MerchantTypeCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `MerchantTypeId` int(11) DEFAULT '0',
  `CultureCode` varchar(50) DEFAULT NULL,
  `MerchantTypeName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`MerchantTypeCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.MerchantTypeCulture: ~9 rows (approximately)
/*!40000 ALTER TABLE `MerchantTypeCulture` DISABLE KEYS */;
INSERT INTO `MerchantTypeCulture` (`MerchantTypeCultureId`, `MerchantTypeId`, `CultureCode`, `MerchantTypeName`) VALUES
	(1, 1, 'EN', 'Brand Flagship'),
	(2, 1, 'CHS', '品牌旗舰店'),
	(3, 1, 'CHT', '品牌旗艦店'),
	(4, 2, 'EN', 'Mono-Brand Agent'),
	(5, 2, 'CHS', '单品牌代理商'),
	(6, 2, 'CHT', '單品牌代理商'),
	(7, 3, 'EN', 'Multi-Brand Agent'),
	(8, 3, 'CHS', '多品牌代理商'),
	(9, 3, 'CHT', '多品牌代理商');
/*!40000 ALTER TABLE `MerchantTypeCulture` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
