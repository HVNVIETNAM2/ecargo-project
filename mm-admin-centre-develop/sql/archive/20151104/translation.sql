INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM", "EN", "MM");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM", "CHS", "MM-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_MM", "CHT", "MM-CHT");

INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DETAILS", "EN", "Details");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DETAILS", "CHS", "Details-CHS");
INSERT INTO Translation(TranslationCode, CultureCode, TranslationName) VALUES ("LB_DETAILS", "CHT", "Details-CHT");