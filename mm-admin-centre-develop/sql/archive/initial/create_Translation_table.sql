CREATE TABLE `Translation` (
	`TranslationId` INT NOT NULL AUTO_INCREMENT,
	`TranslationCode` VARCHAR(255) NULL DEFAULT NULL,
	`CultureCode` VARCHAR(50) NULL DEFAULT NULL,
	`TranslationName` VARCHAR(4000) NULL DEFAULT NULL,
	PRIMARY KEY (`TranslationId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
