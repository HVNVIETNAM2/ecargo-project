/*init Translation table which is related to the label language.*/
delete from Translation;
/*below is related to the page of "create new user"*/
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PROFILE_PIC', 'EN', 'Profile Picture');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PROFILE_PIC', 'CHT', '頭像');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PROFILE_PIC', 'CHS', '头像');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_FIRSTNAME', 'EN', 'First Name');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_FIRSTNAME', 'CHT', '名');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_FIRSTNAME', 'CHS', '名');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LASTNAME', 'EN', 'Last Name');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LASTNAME', 'CHT', '姓');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LASTNAME', 'CHS', '姓');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_OPTIONAL', 'EN', '(Optional)');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_OPTIONAL', 'CHT', '（選塡）');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_OPTIONAL', 'CHS', '（选填）');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_MIDDLE_NAME', 'EN', 'Middle Name');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_MIDDLE_NAME', 'CHT', '中間名');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_MIDDLE_NAME', 'CHS', '中间名');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_DISP_NAME', 'EN', 'Display Name');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_DISP_NAME', 'CHT', '顯示名');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_DISP_NAME', 'CHS', '显示名');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_EMAIL', 'EN', 'Email');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_EMAIL', 'CHT', '郵箱地址');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_EMAIL', 'CHS', '邮箱地址');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_MOBILE', 'EN', 'Mobile');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_MOBILE', 'CHT', '手機號碼');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_MOBILE', 'CHS', '手机号码');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LANGUAGE', 'EN', 'Language');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LANGUAGE', 'CHT', '語言');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LANGUAGE', 'CHS', '语言');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TIME_ZONE', 'EN', 'Time zone');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TIME_ZONE', 'CHT', '時區');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TIME_ZONE', 'CHS', '时区');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ROLE', 'EN', 'Role');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ROLE', 'CHT', '角色');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ROLE', 'CHS', '角色');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CREATE_ANOTHER_USER', 'EN', 'Create Another');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CREATE_ANOTHER_USER', 'CHT', '創建下一個用戶');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CREATE_ANOTHER_USER', 'CHS', '建立下一个用户');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_CREATE_USER', 'EN', 'Create User');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_CREATE_USER', 'CHT', '創建新用戶');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_CREATE_USER', 'CHS', '创建新用户');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_UPLOAD', 'EN', 'Upload');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_UPLOAD', 'CHT', '上傳');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_UPLOAD', 'CHS', '上传');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CLOSE', 'EN', 'Close');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CLOSE', 'CHT', '關閉');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CLOSE', 'CHS', '关闭');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CONFIRM', 'EN', 'Confirm');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CONFIRM', 'CHT', '確認');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CONFIRM', 'CHS', '确认');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_FIRSTNAME_NIL', 'EN', 'Please enter your Firstname.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_FIRSTNAME_NIL', 'CHT', '請輸入您的名');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_FIRSTNAME_NIL', 'CHS', '请输入您的名');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_LASTNAME_NIL', 'EN', 'Please enter your Lastname.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_LASTNAME_NIL', 'CHT', '請輸入您的姓');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_LASTNAME_NIL', 'CHS', '请输入您的姓');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_NIL', 'EN', 'Please enter your Email.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_NIL', 'CHT', '請輸入您的郵箱');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_NIL', 'CHS', '请输入您的邮箱');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_MOBILE_NIL', 'EN', 'Please enter your Mobile Number.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_MOBILE_NIL', 'CHT', '請輸入您的手機號碼');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_MOBILE_NIL', 'CHS', '请输入您的手机号码');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_PATTERN', 'EN', 'Invalid mail format.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_PATTERN', 'CHT', '郵箱格式不正確');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_PATTERN', 'CHS', '邮箱格式不正确');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_MOBILE_PATTERN', 'EN', 'Please enter a valid Mobile Number.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_MOBILE_PATTERN', 'CHT', '手機號碼格式不正確');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_MOBILE_PATTERN', 'CHS', '手机号码格式不正确');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_ROLE_NIL', 'EN', 'Please choose at least one role.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_ROLE_NIL', 'CHT', '請選擇至少一種角色。');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_ROLE_NIL', 'CHS', '请选择至少一种角色。');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_USER_CREATION_FAIL', 'EN', 'Failed to create user! Please try again. Error code: ');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_USER_CREATION_FAIL', 'CHT', '無法创建用戶帳戶，請重試。錯誤碼：');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_USER_CREATION_FAIL', 'CHS', '无法创建用户账户，请重试。错误码：');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_USER_CREATED', 'EN', 'User creation successful. An email with verification link will be sent to the following user(s).');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_USER_CREATED', 'CHT', '用戶創建成功。');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_USER_CREATED', 'CHS', '用户创建成功。');

/*below is related to the page of "edit profile"*/
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_EDIT_PROFILE', 'EN', 'Edit Profile');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_EDIT_PROFILE', 'CHT', '編輯個人信息');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_EDIT_PROFILE', 'CHS', '编辑个人信息');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_BASIC_PROFILE', 'EN', 'Basic Profile');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_BASIC_PROFILE', 'CHT', '用戶基本信息');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_BASIC_PROFILE', 'CHS', '用户基本信息');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CANCEL', 'EN', 'Cancel');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CANCEL', 'CHT', '取消');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CANCEL', 'CHS', '取消');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_SAVE', 'EN', 'Save');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_SAVE', 'CHT', '保存');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_SAVE', 'CHS', '保存');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACCOUNT', 'EN', 'Account');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACCOUNT', 'CHT', '帳戶');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACCOUNT', 'CHS', '账户');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PASSWORD', 'EN', 'Password');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PASSWORD', 'CHT', '密碼');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PASSWORD', 'CHS', '密码');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_STATUS', 'EN', 'Status');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_STATUS', 'CHT', '狀態');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_STATUS', 'CHS', '状态');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CHANGE', 'EN', 'Change');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CHANGE', 'CHT', '更改');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CHANGE', 'CHS', '更改');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESET', 'EN', 'Reset');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESET', 'CHT', '重置');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESET', 'CHS', '重置');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACTIVE', 'EN', 'Active');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACTIVE', 'CHT', '正常');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACTIVE', 'CHS', '正常');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PENDING', 'EN', 'Pending');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PENDING', 'CHT', '待處理');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PENDING', 'CHS', '待处理');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_INACTIVE', 'EN', 'Inactive');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_INACTIVE', 'CHT', '暫停');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_INACTIVE', 'CHS', '暂停');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_DELETE_USER', 'EN', 'Delete');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_DELETE_USER', 'CHT', '刪除');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_DELETE_USER', 'CHS', '刪除');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_EDIT_USER', 'EN', 'Edit');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_EDIT_USER', 'CHT', '編輯');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_EDIT_USER', 'CHS', '编辑');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PERSONAL_INFO', 'EN', 'Profile');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PERSONAL_INFO', 'CHT', '個人信息');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_PERSONAL_INFO', 'CHS', '个人信息');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_USER_EDIT_PROFILE', 'EN', 'Edit {0}\'s Profile');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_USER_EDIT_PROFILE', 'CHT', '編輯 {0} 的個人信息');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_USER_EDIT_PROFILE', 'CHS', '编辑 {0} 的个人信息');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_USER_EDITED', 'EN', 'User successfully edited!');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_USER_EDITED', 'CHT', '用戶修改成功。');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_USER_EDITED', 'CHS', '用戶修改成功。');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_USER_EDITED_FAIL', 'EN', 'Failed to create user! Please try again. Error code: ');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_USER_EDITED_FAIL', 'CHT', '無法修改用戶，請重試。錯誤碼：');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_USER_EDITED_FAIL', 'CHS', '无法修改用户，请重试。错误码：');

/*below is related to the page of "change email"*/
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CHANGE_EMAIL', 'EN', 'Change Email');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CHANGE_EMAIL', 'CHT', '修改郵箱地址');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CHANGE_EMAIL', 'CHS', '修改邮箱地址');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_NEW_EMAIL', 'EN', 'New Email');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_NEW_EMAIL', 'CHT', '新電郵地址');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_NEW_EMAIL', 'CHS', '新电邮地址');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CONF_NEW_EMAIL', 'EN', 'Confirm New Email');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CONF_NEW_EMAIL', 'CHT', '確認新電郵地址');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CONF_NEW_EMAIL', 'CHS', '确认新电邮地址');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESET_USER_EMAIL', 'EN', 'Please enter new email address for {0}');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESET_USER_EMAIL', 'CHT', '請為 {0} 輸入新的電郵地址');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESET_USER_EMAIL', 'CHS', '请为 {0} 输入新的电邮地址');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_NOT_MATCH', 'EN', 'Emails do not match.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_NOT_MATCH', 'CHT', '兩次輸入郵箱不一致');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_NOT_MATCH', 'CHS', '两次输入邮箱不一致');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_CHANGE_FAIL', 'EN', 'Failed to change email! Please try again. Error code: ');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_CHANGE_FAIL', 'CHT', '無法更改郵箱，請重試。錯誤碼：');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_EMAIL_CHANGE_FAIL', 'CHS', '无法更改邮箱，请重试。错误码：');

/*below is related to the page of "reset password"*/
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_RESET_PASSWORD', 'EN', 'Reset Password');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_RESET_PASSWORD', 'CHT', '重置密碼');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_RESET_PASSWORD', 'CHS', '重置密码');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESET_USER_PASSWORD', 'EN', 'You are going to reset the password for {0}');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESET_USER_PASSWORD', 'CHT', '您將為用戶 {0} 重設密碼');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESET_USER_PASSWORD', 'CHS', '您将为用户 {0} 重设密码');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESET_USER_EMAIL_LINK', 'EN', 'An email with verification link will be sent to the user.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESET_USER_EMAIL_LINK', 'CHT', '用戶將收到驗證電子郵件。');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESET_USER_EMAIL_LINK', 'CHS', '用户将收到验证电子邮件。');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_RESET_PASSWORD_FAIL', 'EN', 'Failed to reset password. Please try again later. Error code: ');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_RESET_PASSWORD_FAIL', 'CHT', '無法重置密碼。請重試。錯誤碼：');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_RESET_PASSWORD_FAIL', 'CHS', '无法重置密码。请重试。错误码：');

/*below is related to the page of "change status"*/
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_CHANGE_USER_STATUS', 'EN', 'Change User Status');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_CHANGE_USER_STATUS', 'CHT', '更改用戶帳戶狀態');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_CHANGE_USER_STATUS', 'CHS', '更改用户账户状态');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_INACTIVATE', 'EN', 'You are going to inactivate {0}');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_INACTIVATE', 'CHT', '{0} 帳戶將被停用。');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_INACTIVATE', 'CHS', '{0} 帐户将被停用。');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_ACTIVATE', 'EN', 'You are going to activate {0}');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_ACTIVATE', 'CHT', '{0} 帳戶將被激活。');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_ACTIVATE', 'CHS', '{0} 帐户将被激活。');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_CHANGE_STATUS_FAIL', 'EN', 'Failed to change status. Please try again later. Error code: ');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_CHANGE_STATUS_FAIL', 'CHT', '無法更改用戶狀態，請重試。錯誤碼：');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_ERR_CHANGE_STATUS_FAIL', 'CHS', '无法更改用户状态。请重试。错误码：');

/*below is related to the page of "logout"*/
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_LOGOUT', 'EN', 'Logout');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_LOGOUT', 'CHT', '登出');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_LOGOUT', 'CHS', '登出');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_USER_LOGOUT', 'EN', 'Are you sure you want to logout?');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_USER_LOGOUT', 'CHT', '您確定要登出？');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_USER_LOGOUT', 'CHS', '您确定要登出？');

/*below is related to the page of "user list"*/
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_USER', 'EN', 'User');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_USER', 'CHT', '用戶');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_USER', 'CHS', '用户');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_ALL_AC_USERS', 'EN', 'All Admin Centre Users');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_ALL_AC_USERS', 'CHT', '所有用戶');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_ALL_AC_USERS', 'CHS', '所有用户');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CREATE_USER', 'EN', 'Create New User');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CREATE_USER', 'CHT', '創建新用戶');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_CREATE_USER', 'CHS', '创建新用户');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_SEARCH', 'EN', 'Search');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_SEARCH', 'CHT', '搜索');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_SEARCH', 'CHS', '搜索');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_AC_USER_LIST_SEARCH_PLACEHOLDER', 'EN', 'Search for Name or Mobile...');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_AC_USER_LIST_SEARCH_PLACEHOLDER', 'CHT', '搜索姓名或手機號碼...');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_AC_USER_LIST_SEARCH_PLACEHOLDER', 'CHS', '搜索姓名或手机号码...');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ALL_ROLES', 'EN', 'All Roles');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ALL_ROLES', 'CHT', '所有角色');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ALL_ROLES', 'CHS', '所有角色');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ALL_STATUS', 'EN', 'All Status');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ALL_STATUS', 'CHT', '所有狀態');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ALL_STATUS', 'CHS', '所有状态');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LLD', 'EN', 'Last Login');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LLD', 'CHT', '最後登入');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LLD', 'CHS', '最后登入');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LMD', 'EN', 'Last Modified');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LMD', 'CHT', '最後更改');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_LMD', 'CHS', '最后更改');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACTIONS', 'EN', 'Action');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACTIONS', 'CHT', '操作');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACTIONS', 'CHS', '操作');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_INACTIVATE_USER', 'EN', 'Inactivate');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_INACTIVATE_USER', 'CHT', '暫停用戶帳戶');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_INACTIVATE_USER', 'CHS', '暂停用户账户');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACTIVATE_USER', 'EN', 'Activate');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACTIVATE_USER', 'CHT', '激活用戶帳戶');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_ACTIVATE_USER', 'CHS', '激活用户账户');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESET_PW_USER', 'EN', 'Reset Password');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESET_PW_USER', 'CHT', '重置密碼');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESET_PW_USER', 'CHS', '重置密码');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESEND_REG_LINK', 'EN', 'Resend Registration Link');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESEND_REG_LINK', 'CHT', '重新發送完成註冊郵件');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESEND_REG_LINK', 'CHS', '重新发送完成注册邮件');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESEND_CHANGE_EMAIL_LINK', 'EN', 'Resend Change Email Link');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESEND_CHANGE_EMAIL_LINK', 'CHT', '重新發送重置郵箱地址郵件');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_RESEND_CHANGE_EMAIL_LINK', 'CHS', '重新发送重置邮箱地址邮件');

/*below is related to the page of "resend register link"*/
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESEND_REG_LINK', 'EN', 'You are going to resend an email with a verification link. User can use that link for registration.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESEND_REG_LINK', 'CHT', '重新發送完成註冊郵件，用戶可以點擊郵件中鏈接完成註冊。');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESEND_REG_LINK', 'CHS', '重新发送完成注册邮件，用户可以点击邮件中链接完成注册。');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESEND_CHANGE_EMAIL_LINK', 'EN', 'You are going to resend an email with the unique link to the user for login. User can use that link to login with the new email.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESEND_CHANGE_EMAIL_LINK', 'CHT', '重新發送用戶登陸鏈接郵件，用戶可以點擊郵件中鏈接使用新的郵箱進行登陸。');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_MM_ADMIN_RESEND_CHANGE_EMAIL_LINK', 'CHS', '重新发送用户登陆链接邮件，用户可以点击邮件中链接使用新的邮箱进行登陆。');

/*below is related to the page of "confirm delete user"*/
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_CONF_DEL_USER', 'EN', 'Confirm Delete User');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_CONF_DEL_USER', 'CHT', '確認冊除用戶');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_TL_CONF_DEL_USER', 'CHS', '确认册除用户');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_DEL_USER', 'EN', 'You are going to delete {0}. This action cannot be reversed.');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_DEL_USER', 'CHT', '將刪除 {0} 帳戶，這個操作將不能撤銷。');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('MSG_CONF_DEL_USER', 'CHS', '将删除 {0} 帐户，这个操作将不能撤销。');

/*below is related to the page of "home page"*/
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_HOME', 'EN', 'Home');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_HOME', 'CHT', '主頁');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_HOME', 'CHS', '主页');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_MERCHANT', 'EN', 'Merchant');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_MERCHANT', 'CHT', '商戶');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_MERCHANT', 'CHS', '商户');

INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_USER_EDIT_MY_PROFILE', 'EN', 'Edit My Profile');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_USER_EDIT_MY_PROFILE', 'CHT', '編輯個人資料');
INSERT INTO Translation (TranslationCode, CultureCode, TranslationName) VALUES ('LB_USER_EDIT_MY_PROFILE', 'CHS', '编辑个人信息');
