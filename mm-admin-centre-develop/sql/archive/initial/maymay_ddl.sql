-- --------------------------------------------------------
-- Host:                         192.168.33.33
-- Server version:               10.1.7-MariaDB-1~trusty-log - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table maymay.Language
DROP TABLE IF EXISTS `Language`;
CREATE TABLE IF NOT EXISTS `Language` (
  `LanguageId` int(11) NOT NULL AUTO_INCREMENT,
  `LanguageNameInvariant` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`LanguageId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table maymay.Language: ~2 rows (approximately)
DELETE FROM `Language`;
/*!40000 ALTER TABLE `Language` DISABLE KEYS */;
INSERT INTO `Language` (`LanguageId`, `LanguageNameInvariant`) VALUES
	(1, 'EN'),
	(2, 'CHS'),
	(3, 'CHT');
/*!40000 ALTER TABLE `Language` ENABLE KEYS */;


-- Dumping structure for table maymay.LanguageCulture
DROP TABLE IF EXISTS `LanguageCulture`;
CREATE TABLE IF NOT EXISTS `LanguageCulture` (
  `LanguageCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `LanguageId` int(11) DEFAULT NULL,
  `CultureCode` varchar(50) DEFAULT NULL,
  `LanguageName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`LanguageCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.LanguageCulture: ~8 rows (approximately)
DELETE FROM `LanguageCulture`;
/*!40000 ALTER TABLE `LanguageCulture` DISABLE KEYS */;
INSERT INTO `LanguageCulture` (`LanguageCultureId`, `LanguageId`, `CultureCode`, `LanguageName`) VALUES
	(1, 1, 'EN', 'English'),
	(2, 1, 'CHS', '英语'),
	(3, 1, 'CHT', '英語'),
	(4, 2, 'EN', 'Chinese Simplified'),
	(5, 2, 'CHS', '中文维基百科'),
	(6, 2, 'CHT', '中文維基百科'),
	(7, 3, 'EN', 'Chinese Traditional'),
	(8, 3, 'CHS', '中文维基百科'),
	(9, 3, 'CHT', '中文維基百科');
/*!40000 ALTER TABLE `LanguageCulture` ENABLE KEYS */;


-- Dumping structure for table maymay.MobileCode
DROP TABLE IF EXISTS `MobileCode`;
CREATE TABLE IF NOT EXISTS `MobileCode` (
  `MobileCodeId` int(11) NOT NULL AUTO_INCREMENT,
  `MobileCodeNameInvariant` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`MobileCodeId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.MobileCode: ~2 rows (approximately)
DELETE FROM `MobileCode`;
/*!40000 ALTER TABLE `MobileCode` DISABLE KEYS */;
INSERT INTO `MobileCode` (`MobileCodeId`, `MobileCodeNameInvariant`) VALUES
	(1, '+852'),
	(2, '+86');
/*!40000 ALTER TABLE `MobileCode` ENABLE KEYS */;


-- Dumping structure for table maymay.MobileCodeCulture
DROP TABLE IF EXISTS `MobileCodeCulture`;
CREATE TABLE IF NOT EXISTS `MobileCodeCulture` (
  `MobileCodeCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `MobileCodeId` int(11) DEFAULT NULL,
  `CultureCode` varchar(50) DEFAULT NULL,
  `MobileCodeName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`MobileCodeCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.MobileCodeCulture: ~5 rows (approximately)
DELETE FROM `MobileCodeCulture`;
/*!40000 ALTER TABLE `MobileCodeCulture` DISABLE KEYS */;
INSERT INTO `MobileCodeCulture` (`MobileCodeCultureId`, `MobileCodeId`, `CultureCode`, `MobileCodeName`) VALUES
	(1, 1, 'EN', 'Hong Kong'),
	(2, 1, 'CHS', '香港'),
	(3, 1, 'CHT', '香港'),
	(4, 2, 'EN', 'China'),
	(5, 2, 'CHT', '中國'),
	(6, 2, 'CHS', '中国');
/*!40000 ALTER TABLE `MobileCodeCulture` ENABLE KEYS */;


-- Dumping structure for table maymay.SecurityGroup
DROP TABLE IF EXISTS `SecurityGroup`;
CREATE TABLE IF NOT EXISTS `SecurityGroup` (
  `SecurityGroupId` int(11) NOT NULL AUTO_INCREMENT,
  `SecurityGroupTypeId` int(11) NOT NULL DEFAULT '0',
  `SecurityGroupNameInvariant` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`SecurityGroupId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.SecurityGroup: ~3 rows (approximately)
DELETE FROM `SecurityGroup`;
/*!40000 ALTER TABLE `SecurityGroup` DISABLE KEYS */;
INSERT INTO `SecurityGroup` (`SecurityGroupId`, `SecurityGroupTypeId`, `SecurityGroupNameInvariant`) VALUES
	(1, 1, 'MM Admin'),
	(2, 1, 'MM Customer Service'),
	(3, 1, 'MM Finance'),
	(4, 1, 'MM Tech Ops');
/*!40000 ALTER TABLE `SecurityGroup` ENABLE KEYS */;


-- Dumping structure for table maymay.SecurityGroupCulture
DROP TABLE IF EXISTS `SecurityGroupCulture`;
CREATE TABLE IF NOT EXISTS `SecurityGroupCulture` (
  `SecurityGroupCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `SecurityGroupId` int(11) NOT NULL DEFAULT '0',
  `CultureCode` varchar(50) DEFAULT NULL,
  `SecurityGroupName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`SecurityGroupCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.SecurityGroupCulture: ~11 rows (approximately)
DELETE FROM `SecurityGroupCulture`;
/*!40000 ALTER TABLE `SecurityGroupCulture` DISABLE KEYS */;
INSERT INTO `SecurityGroupCulture` (`SecurityGroupCultureId`, `SecurityGroupId`, `CultureCode`, `SecurityGroupName`) VALUES
	(1, 1, 'EN', 'MM Admin'),
	(2, 1, 'CHT', '時尚家居 管理員'),
	(3, 1, 'CHS', '时尚家居 管理员'),
	(4, 2, 'EN', 'MM Customer Service'),
	(5, 2, 'CHT', '時尚家居 客服'),
	(6, 2, 'CHS', '时尚家居 顾客服务'),
	(7, 3, 'EN', 'MM Finance'),
	(8, 3, 'CHT', '時尚家居 財經'),
	(9, 3, 'CHS', '时尚家居 财经'),
	(10, 4, 'EN', 'MM Technology'),
	(11, 4, 'CHT', '時尚家居 技術'),
	(12, 4, 'CHS', '时尚家居 技术');
/*!40000 ALTER TABLE `SecurityGroupCulture` ENABLE KEYS */;


-- Dumping structure for table maymay.Status
DROP TABLE IF EXISTS `Status`;
CREATE TABLE IF NOT EXISTS `Status` (
  `StatusId` int(11) NOT NULL AUTO_INCREMENT,
  `StatusNameInvariant` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`StatusId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.Status: ~4 rows (approximately)
DELETE FROM `Status`;
/*!40000 ALTER TABLE `Status` DISABLE KEYS */;
INSERT INTO `Status` (`StatusId`, `StatusNameInvariant`) VALUES
	(1, 'Deleted'),
	(2, 'Active'),
	(3, 'Pending'),
	(4, 'Inactive');
/*!40000 ALTER TABLE `Status` ENABLE KEYS */;


-- Dumping structure for table maymay.StatusCulture
DROP TABLE IF EXISTS `StatusCulture`;
CREATE TABLE IF NOT EXISTS `StatusCulture` (
  `StatusCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `StatusId` int(11) NOT NULL DEFAULT '0',
  `CultureCode` varchar(50) DEFAULT NULL,
  `StatusName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`StatusCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.StatusCulture: ~11 rows (approximately)
DELETE FROM `StatusCulture`;
/*!40000 ALTER TABLE `StatusCulture` DISABLE KEYS */;
INSERT INTO `StatusCulture` (`StatusCultureId`, `StatusId`, `CultureCode`, `StatusName`) VALUES
	(1, 1, 'EN', 'Deleted'),
	(2, 1, 'CHT', '刪除'),
	(3, 1, 'CHS', '删除'),
	(4, 2, 'EN', 'Active'),
	(5, 2, 'CHS', '主动'),
	(6, 2, 'CHT', '主動'),
	(7, 3, 'EN', 'Pending'),
	(8, 3, 'CHS', '待定'),
	(9, 3, 'CHT', '待定'),
	(10, 4, 'EN', 'Inactive'),
	(11, 4, 'CHS', '非活动'),
	(12, 4, 'CHT', '非活動');
/*!40000 ALTER TABLE `StatusCulture` ENABLE KEYS */;


-- Dumping structure for table maymay.TimeZone
DROP TABLE IF EXISTS `TimeZone`;
CREATE TABLE IF NOT EXISTS `TimeZone` (
  `TimeZoneId` int(11) NOT NULL AUTO_INCREMENT,
  `TimeZoneNameInvariant` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`TimeZoneId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.TimeZone: ~2 rows (approximately)
DELETE FROM `TimeZone`;
/*!40000 ALTER TABLE `TimeZone` DISABLE KEYS */;
INSERT INTO `TimeZone` (`TimeZoneId`, `TimeZoneNameInvariant`) VALUES
	(1, 'Beijing Time'),
	(2, 'Hong Kong Time');
/*!40000 ALTER TABLE `TimeZone` ENABLE KEYS */;


-- Dumping structure for table maymay.TimeZoneCulture
DROP TABLE IF EXISTS `TimeZoneCulture`;
CREATE TABLE IF NOT EXISTS `TimeZoneCulture` (
  `TimeZoneCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `TimeZoneId` int(11) NOT NULL DEFAULT '0',
  `CultureCode` varchar(50) DEFAULT NULL,
  `TimeZoneName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`TimeZoneCultureId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.TimeZoneCulture: ~6 rows (approximately)
DELETE FROM `TimeZoneCulture`;
/*!40000 ALTER TABLE `TimeZoneCulture` DISABLE KEYS */;
INSERT INTO `TimeZoneCulture` (`TimeZoneCultureId`, `TimeZoneId`, `CultureCode`, `TimeZoneName`) VALUES
	(1, 1, 'EN', 'Beijing Time'),
	(2, 1, 'CHS', '(北京时间'),
	(3, 1, 'CHT', '(北京时间'),
	(4, 2, 'EN', 'Hong Kong Time'),
	(5, 2, 'CHS', '香港時間'),
	(6, 2, 'CHT', '香港時間');
/*!40000 ALTER TABLE `TimeZoneCulture` ENABLE KEYS */;


-- Dumping structure for table maymay.User
DROP TABLE IF EXISTS `User`;
CREATE TABLE IF NOT EXISTS `User` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `DisplayName` varchar(50) DEFAULT NULL,
  `Email` varchar(50) NOT NULL,
  `MobileCode` varchar(50) DEFAULT NULL,
  `MobileNumber` varchar(50) DEFAULT NULL,
  `LanguageId` int(11) NOT NULL DEFAULT '0',
  `TimeZoneId` int(11) NOT NULL DEFAULT '0',
  `ProfileImage` varchar(255) DEFAULT NULL,
  `ActivationToken` varchar(255) DEFAULT NULL,
  `Hash` varchar(255) DEFAULT NULL,
  `Salt` varchar(50) DEFAULT NULL,
  `StatusId` int(11) NOT NULL DEFAULT '0',
  `LastStatus` datetime DEFAULT CURRENT_TIMESTAMP,
  `LastModified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LastAction` datetime DEFAULT CURRENT_TIMESTAMP,
  `LastLogin` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=357 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.User: ~5 rows (approximately)
DELETE FROM `User`;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` (`UserId`, `FirstName`, `LastName`, `MiddleName`, `DisplayName`, `Email`, `MobileCode`, `MobileNumber`, `LanguageId`, `TimeZoneId`, `ProfileImage`, `ActivationToken`, `Hash`, `Salt`, `StatusId`, `LastStatus`, `LastModified`, `LastAction`, `LastLogin`) VALUES
	(349, 'Albertos', 'Dallow', 'Jules', NULL, 'adallow@hotmail.com', '+852', '45345', 1, 1, NULL, '123456', '1246XJ8HHFQ2cAszYJWEmC4jBNinnp6mD5kIzk/nWGG26hJK9Q4umKDR+gpEZZ62LwqOhXV0AoCaf34hjRX/QQ==', 'UWuQMSuxK46Zf83+aBdMiA==', 2, '2015-09-24 04:21:00', '2015-09-24 06:53:52', '2015-09-18 09:55:48', '2015-09-24 06:53:52'),
	(350, 'Yoko', 'Ono', NULL, NULL, 'Albert', '+852', '45345', 1, 2, NULL, '123456', NULL, NULL, 3, '2015-09-24 04:21:00', '2015-09-24 06:55:14', '2015-09-24 02:18:02', '2015-09-24 02:18:02'),
	(351, 'Bob', 'Brown', NULL, NULL, 'Albert2', '+852', '45345', 1, 2, NULL, '123456', NULL, NULL, 3, '2015-09-24 04:21:00', '2015-09-24 06:55:14', '2015-09-24 02:19:26', '2015-09-24 02:19:26'),
	(352, 'Albert', 'Dallow', NULL, NULL, 'adallow2@hotmail.com', '+852', '5555', 1, 1, NULL, '123456', NULL, NULL, 3, '2015-09-24 04:21:00', '2015-09-24 06:55:14', '2015-09-24 02:27:17', '2015-09-24 02:27:17'),
	(353, 'Vincent', 'Hello', NULL, NULL, 'adallow4@hotmail.com', '+852', '4555534', 1, 1, NULL, '123456', NULL, NULL, 3, '2015-09-24 04:21:00', '2015-09-24 06:55:14', '2015-09-24 02:30:18', '2015-09-24 02:30:18'),
	(354, 'Carl', 'Dallow', NULL, NULL, 'a@alter.com', '+852', '3456785', 1, 1, NULL, '123456', NULL, NULL, 3, '2015-09-24 04:21:04', '2015-09-24 06:55:14', '2015-09-24 04:21:04', '2015-09-24 04:21:04'),
	(355, 'Bart', 'Tos', NULL, NULL, 'adallow@hot.com', '+86', '567345', 1, 1, NULL, '123456', NULL, NULL, 3, '2015-09-24 06:24:47', '2015-09-24 06:29:07', '2015-09-24 06:24:47', '2015-09-24 06:24:47'),
	(356, 'Lucy', 'Cartwright', NULL, NULL, 'luc@hot.com', '+86', '567345', 1, 1, NULL, 'f112733f188cbe863c2385c4bd35673e6b2bb79c063e329d', NULL, NULL, 3, '2015-09-24 06:28:46', '2015-09-24 06:28:46', '2015-09-24 06:28:46', '2015-09-24 06:28:46');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;


-- Dumping structure for table maymay.UserSecurityGroup
DROP TABLE IF EXISTS `UserSecurityGroup`;
CREATE TABLE IF NOT EXISTS `UserSecurityGroup` (
  `UserSecurityGroupId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `SecurityGroupId` int(11) NOT NULL,
  PRIMARY KEY (`UserSecurityGroupId`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8;

-- Dumping data for table maymay.UserSecurityGroup: ~9 rows (approximately)
DELETE FROM `UserSecurityGroup`;
/*!40000 ALTER TABLE `UserSecurityGroup` DISABLE KEYS */;
INSERT INTO `UserSecurityGroup` (`UserSecurityGroupId`, `UserId`, `SecurityGroupId`) VALUES
	(214, 349, 1),
	(215, 350, 2),
	(216, 350, 3),
	(217, 351, 2),
	(218, 351, 3),
	(219, 352, 2),
	(220, 352, 4),
	(221, 353, 1),
	(222, 353, 3),
	(223, 354, 1),
	(224, 354, 2),
	(225, 355, 1),
	(226, 355, 3),
	(227, 356, 1),
	(228, 356, 3);
/*!40000 ALTER TABLE `UserSecurityGroup` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
