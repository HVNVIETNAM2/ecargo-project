CREATE TABLE IF NOT EXISTS `InventoryLocationCulture` (
  `InventoryLocationCultureId` int(11) NOT NULL AUTO_INCREMENT,
  `InventoryLocationId` int(11) NOT NULL DEFAULT '0',
  `CultureCode` VARCHAR(50) DEFAULT NULL,
  `LocationName` VARCHAR(255) NOT NULL DEFAULT '0',
  `District` VARCHAR(50) NULL DEFAULT NULL,
  `Apartment` VARCHAR(50) NULL DEFAULT NULL,
  `Floor` VARCHAR(50) NULL DEFAULT NULL,
  `BlockNo` VARCHAR(50) NULL DEFAULT NULL,
  `Building` VARCHAR(50) NULL DEFAULT NULL,
  `StreetNo` VARCHAR(50) NULL DEFAULT NULL,
  `Street` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`InventoryLocationCultureId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into InventoryLocationCulture (InventoryLocationId, CultureCode, LocationName, District, Apartment, Floor, BlockNo, Building, StreetNo, Street) select InventoryLocationId, 'EN', LocationName, District, Apartment, Floor, BlockNo, Building, StreetNo, Street from InventoryLocation;
insert into InventoryLocationCulture (InventoryLocationId, CultureCode, LocationName, District, Apartment, Floor, BlockNo, Building, StreetNo, Street) select InventoryLocationId, 'CHT', LocationName, District, Apartment, Floor, BlockNo, Building, StreetNo, Street from InventoryLocation;
insert into InventoryLocationCulture (InventoryLocationId, CultureCode, LocationName, District, Apartment, Floor, BlockNo, Building, StreetNo, Street) select InventoryLocationId, 'CHS', LocationName, District, Apartment, Floor, BlockNo, Building, StreetNo, Street from InventoryLocation;


ALTER TABLE `InventoryLocation` CHANGE `LocationName` `LocationNameInvariant` VARCHAR(255) NOT NULL DEFAULT '0';
ALTER TABLE `InventoryLocation` CHANGE `District` `DistrictInvariant` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `Apartment` `ApartmentInvariant` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `Floor` `FloorInvariant` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `BlockNo` `BlockNoInvariant` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `Building` `BuildingInvariant` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `StreetNo` `StreetNoInvariant` VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE `InventoryLocation` CHANGE `Street` `StreetInvariant` VARCHAR(50) NULL DEFAULT NULL;

