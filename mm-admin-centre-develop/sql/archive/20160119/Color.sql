ALTER TABLE `Color`
ADD COLUMN `ColorImage` varchar(255) NULL After `ColorCode`;


UPDATE Color SET `ColorImage`='7ff8798833bc07df375a981d80b4cb3b' WHERE ColorCode='RED';
UPDATE Color SET `ColorImage`='0f2793e9941bda72245b08528b2bd709' WHERE ColorCode='PINK';
UPDATE Color SET `ColorImage`='02232e14bd3789c2c7cb341c0d19688a' WHERE ColorCode='ORANGE';
UPDATE Color SET `ColorImage`='853285a2de5226d769d8cbaadd376ced' WHERE ColorCode='YELLOW';
UPDATE Color SET `ColorImage`='ab9790267b5abf50153a762cd8efd789' WHERE ColorCode='GREEN';
UPDATE Color SET `ColorImage`='a018eec6ebc444ac400bc280b7e8fac1' WHERE ColorCode='BLUE';
UPDATE Color SET `ColorImage`='4f467f0fa88faca39036a46ca45e69fd' WHERE ColorCode='PURPLE';
UPDATE Color SET `ColorImage`='7dc06b3b01b288942f949b8e026d42fd' WHERE ColorCode='BROWN';
UPDATE Color SET `ColorImage`='148fc226d13da3b5df9c76a35d663862' WHERE ColorCode='GREY';
UPDATE Color SET `ColorImage`='45a152742619422d7f893c53269846f6' WHERE ColorCode='WHITE';
UPDATE Color SET `ColorImage`='f7286587dd8b4b167e577334fdc57def' WHERE ColorCode='BLACK';
UPDATE Color SET `ColorImage`='58a2c23e3a043800651aff9778e72fcb' WHERE ColorCode='PRINT';
UPDATE Color SET `ColorImage`='363bf566fc86dd6931a3c02d8068144f' WHERE ColorCode='STRIP';
UPDATE Color SET `ColorImage`='d34d32834f2a3f00fea0cb37b142e3e6' WHERE ColorCode='FLORAL';
UPDATE Color SET `ColorImage`='cb07970600196927fec017f0041ceb8c' WHERE ColorCode='CHECK';
UPDATE Color SET `ColorImage`='87e4f1895b37e472f624d87409f57fe3' WHERE ColorCode='DOT';


