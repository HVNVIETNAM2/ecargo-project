/**
 * Author:  Michael Yu <michaelyu@wweco.com>
 * Created: Jan 19, 2016
 */
ALTER TABLE `maymay`.`Category` 
DROP INDEX `uk_CategoryCode` ;