/* 
 * Create tables and default data for Inventory status and corresponding translations
 */
/**
 * Author:  Michael Yu <michaelyu@wweco.com>
 * Created: Dec 29, 2015
 */

CREATE TABLE `maymay`.`InventoryStatus` (
  `StatusId` INT(11) NOT NULL AUTO_INCREMENT,
  `StatusNameInvariant` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`StatusId`)) DEFAULT CHARSET=utf8;

CREATE TABLE `maymay`.`InventoryStatusCulture` (
  `StatusCultureId` INT(11) NOT NULL AUTO_INCREMENT,
  `StatusId` INT(11) NOT NULL DEFAULT 0,
  `CultureCode` VARCHAR(50) NULL DEFAULT NULL,
  `StatusName` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`StatusCultureId`)) DEFAULT CHARSET=utf8;

INSERT INTO `maymay`.`InventoryStatus` (`StatusId`, `StatusNameInvariant`) VALUES ('1', 'InStock');
INSERT INTO `maymay`.`InventoryStatus` (`StatusId`, `StatusNameInvariant`) VALUES ('2', 'LowStock');
INSERT INTO `maymay`.`InventoryStatus` (`StatusId`, `StatusNameInvariant`) VALUES ('3', 'OutOfStock');
INSERT INTO `maymay`.`InventoryStatus` (`StatusId`, `StatusNameInvariant`) VALUES ('4', 'N/A');

INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('1', 'EN', 'In Stock');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('1', 'CHT', '有庫存');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('1', 'CHS', '有库存');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('2', 'EN', 'Low Stock');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('2', 'CHT', '低庫存');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('2', 'CHS', '低库存');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('3', 'EN', 'Out of Stock');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('3', 'CHT', '無庫存');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('3', 'CHS', '无库存');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('4', 'EN', 'N/A');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('4', 'CHT', '不適用');
INSERT INTO `maymay`.`InventoryStatusCulture` (`StatusId`, `CultureCode`, `StatusName`) VALUES ('4', 'CHS', '不适用');