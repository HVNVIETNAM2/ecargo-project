ALTER TABLE `Brand`
CHANGE COLUMN `IsSearchable` `IsSearchableBrand` TINYINT NOT NULL DEFAULT '0' ;

ALTER TABLE `Category`
CHANGE COLUMN `IsIncludeInAutocomplete` `IsSearchableCategory` TINYINT NOT NULL DEFAULT '0' ;

ALTER TABLE `Merchant`
CHANGE COLUMN `IsSuggestedSearchBar` `IsSearchableMerchant` TINYINT NOT NULL DEFAULT '0' ;



