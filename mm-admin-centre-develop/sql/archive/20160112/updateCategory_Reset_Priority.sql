UPDATE Category 
LEFT JOIN (
SELECT CategoryId, ParentCategoryId, 
case
when CategoryId = 0 THEN @count := 0
when @parent = ParentCategoryId THEN @count := @count + 1
when @parent := ParentCategoryId THEN @count := 1
end as Priority
FROM Category t, (SELECT @count := 0, @parent := 0) r
where t.StatusId <> 1
order by t.ParentCategoryId, t.Priority
) C ON Category.CategoryId = C.CategoryId
set Category.Priority = C.Priority;
