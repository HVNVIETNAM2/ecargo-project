ALTER TABLE `User` ADD COLUMN `FollowerCount` INT(11) NOT NULL DEFAULT 0 AFTER `StatusReasonCode`;
ALTER TABLE `User` ADD COLUMN `FriendCount` INT(11) NOT NULL DEFAULT 0 AFTER `FollowerCount`;
ALTER TABLE `User` ADD COLUMN `FollowingUserCount` INT(11) NOT NULL DEFAULT 0 AFTER `FriendCount`;
ALTER TABLE `User` ADD COLUMN `FollowingMerchantCount` INT(11) NOT NULL DEFAULT 0 AFTER `FollowingUserCount`;
ALTER TABLE `User` ADD COLUMN `FollowingBrandCount` INT(11) NOT NULL DEFAULT 0 AFTER `FollowingMerchantCount`;

ALTER TABLE `Merchant` ADD COLUMN `FollowerCount` INT(11) NOT NULL DEFAULT 0 AFTER `StatusId`;

ALTER TABLE `Brand` ADD COLUMN `FollowerCount` INT(11) NOT NULL DEFAULT 0 AFTER `StatusId`;
