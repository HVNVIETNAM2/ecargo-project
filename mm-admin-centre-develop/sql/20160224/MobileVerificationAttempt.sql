DROP TABLE IF EXISTS `MobileVerificationAttempt`;
CREATE TABLE `MobileVerificationAttempt` (
	`MobileVerificationAttemptId` int(11) NOT NULL AUTO_INCREMENT,
	`MobileNumber`varchar(50) NOT NULL,
	`MobileCode` varchar(50) NOT NULL,
	`LastCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  	PRIMARY KEY (`MobileVerificationAttemptId`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;



