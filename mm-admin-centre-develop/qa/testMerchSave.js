var assert = require('assert');
var data_driven = require('data-driven');
var moment = require('moment');
var util = require('util');

var qaUtil = require('./qaUtil');

var mCredential = {};

describe('POST /api/merch/save', function() {
  before('POST /api/auth/login', function() {
    return qaUtil.successAuthLoginRequest()
      .then(function(res) {
        mCredential = res.body;
      });
  });

  it('with valid data', function() {
    var ms = moment().unix().toString() + Math.floor(Math.random() * 1000);
    return qaUtil.successMerchSaveRequest({
      Merchant: {
        BusinessRegistrationNo: '689',
        District: 'QA District',
        GeoCountryId: 95,
        GeoIdCity: 7533598,
        GeoIdProvince: 12000000,
        IsFeaturedMerchant: 0,
        IsListedMerchant: 0,
        IsRecommendedMerchant: 0,
        IsSearchableMerchant: 0,
        MerchantCompanyName: 'QA Merchant',
        MerchantNameCHS: '质量保证商户',
        MerchantNameCHT: '質量保證商戶',
        MerchantNameEN: 'QA Merchant',
        MerchantSubdomain: 'qamerchant',
        MerchantTypeId: 1,
        Street: 'QA Street',
        StreetNo: 'No.689'
      },
      MerchantBrandIds: [189]
    }, mCredential.Token).then(function(res) {});
  });
});