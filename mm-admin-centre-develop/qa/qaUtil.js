var request = require('supertest-as-promised');

var assert = require('assert');
var moment = require('moment');
var util = require('util');

module.exports = {
  authLoginRequest: function(body, code) {
    return request(process.env.URL).post('/api/auth/login')
      .send(body)
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .expect(code);
  },

  successAuthLoginRequest: function() {
    return this.authLoginRequest({
        'Username': 'test@ecargo.com',
        'Password': 'Bart'
      },
      200);
  },

  failAuthLoginRequest: function(username, password) {
    return this.authLoginRequest({
        'Username': username,
        'Password': password
      },
      500);
  },

  productSkuSaveRequest: function(body, token, code) {
    return request(process.env.URL).post('/api/product/sku/save')
      .send(body)
      .set('Content-Type', 'application/json')
      .set('Authorization', token)
      .expect('Content-Type', /json/)
      .expect(code);
  },

  successProductSkuSaveRequest: function(body, token) {
    return this.productSkuSaveRequest(body, token, 200);
  },

  getRandomSkuBody: function() {
    var body = {}

    // static mandatory fields
    body.MerchantId = 25;
    body.RetailPrice = 1234.5;
    body.ShippingWeightKg = 0.1;
    body.BrandCode = 'QABRAND';
    body.PrimaryCategoryCode = 'women-apparel-jackets';

    // dynamic mandatory fields
    var ms = moment().unix().toString() + Math.floor(Math.random() * 1000);

    body.StyleCode = util.format('SC%s', ms.substring(ms.length - 10));
    body.SkuCode = util.format('SKU%s', ms.substring(ms.length - 9));
    body['SkuName-CHS'] = util.format('女士绒面革夹克 %s', ms);

    return body;
  },

  assertSuccessProductSkuSave: function(res) {
    var log = JSON.stringify({
      '/api/product/sku/save': {
        'Request': res.request._data,
        'Response': res.body
      }
    }, null, 2);

    assert.equal(res.body.success, true, util.format('"success" should be true\n%s', log));
    assert.equal(res.body.message, res.request._data.Overwrite ? 'updated' : 'inserted', util.format('"message" should be "inserted"\n%s', log));
    assert.equal(res.body.errors, 0, util.format('"errors" should be 0\n%s', log));
    assert.equal(res.body.errorList.length, 0, util.format('"errorList" should be empty\n%s', log));
  },

  assertFailProductSkuSave: function(res, errors) {
    var log = JSON.stringify({
      '/api/product/sku/save': {
        'Request': res.request._data,
        'Response': res.body
      }
    }, null, 2);

    assert.equal(res.body.success, false, util.format('"success" should be false\n%s', log));
    assert.equal(res.body.message, 'Error', util.format('"message" should be "Error"\n%s', log));
    assert.equal(res.body.errors, errors.length, util.format('"errors" should be %d\n%s', errors.length, log));
    assert.deepEqual(res.body.errorList, errors, util.format('"errorList" should be the same\n%s', log));
  },

  successProductStyleViewRequest: function(cc, stylecode, merchantid) {
    if (cc == undefined)
      cc = 'CHS';
    else if (cc == 'Hans')
      cc = 'CHS';
    else if (cc == 'Hant')
      cc = 'CHT';
    else if (cc == 'English')
      cc = 'EN';

    return request(process.env.URL).get('/api/product/style/view')
      .query({
        'cc': cc,
        'stylecode': stylecode,
        'merchantid': merchantid
      })
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);
  },

  assertSuccessProductStyleView: function(res, productSkuSaveRes, language) {
    if (language == undefined)
      language = 'CHS';

    var tmpLangs = [];
    if (language == 'CHS' || language == 'Hans') {
      tmpLangs = ['CHS', 'Hans'];
    } else if (language == 'CHT' || language == 'Hant') {
      tmpLangs = ['CHT', 'Hant'];
    } else if (language == 'EN' || language == 'English') {
      tmpLangs = ['English'];
    }

    var log = JSON.stringify({
      '/api/product/sku/save': {
        'Request': productSkuSaveRes.request._data,
        'Response': productSkuSaveRes.body
      },
      '/api/product/style/view': {
        'Path': res.request.req.path,
        'Response': res.body
      }
    }, null, 2);

    // matched key names
    var keys = ['StyleCode', 'ManufacturerName', 'BrandId', 'PriceSale', 'BadgeId', 'SeasonId', 'GeoCountryId', 'LaunchYear', 'WeightKg', 'HeightCm', 'WidthCm', 'LengthCm'];
    if (productSkuSaveRes.request._data.Overwrite) {
      keys.push('StatusId');
    }
    for (var i = 0; i < keys.length; i++) {
      if (productSkuSaveRes.request._data[keys[i]] != undefined)
        assert.equal(res.body[keys[i]], productSkuSaveRes.request._data[keys[i]], util.format('%s should be the same\n%s', keys[i], log));
    }

    // unmatched key names
    var keys = [{
      'a': 'SkuNameCHS',
      'x': 'SkuName-CHS'
    }, {
      'a': 'SkuNameCHS',
      'x': 'SkuName-Hans'
    }, {
      'a': 'SkuNameCHT',
      'x': 'SkuName-CHT'
    }, {
      'a': 'SkuNameCHT',
      'x': 'SkuName-Hant'
    }, {
      'a': 'SkuNameEN',
      'x': 'SkuName-English'
    }, {
      'a': 'SkuSizeCommentCHS',
      'x': 'SizeComment-CHS'
    }, {
      'a': 'SkuSizeCommentCHS',
      'x': 'SizeComment-Hans'
    }, {
      'a': 'SkuSizeCommentCHT',
      'x': 'SizeComment-CHT'
    }, {
      'a': 'SkuSizeCommentCHT',
      'x': 'SizeComment-Hant'
    }, {
      'a': 'SkuSizeCommentEN',
      'x': 'SizeComment-English'
    }, {
      'a': 'SkuMaterialCHS',
      'x': 'Material-CHS'
    }, {
      'a': 'SkuMaterialCHS',
      'x': 'Material-Hans'
    }, {
      'a': 'SkuMaterialCHT',
      'x': 'Material-CHT'
    }, {
      'a': 'SkuMaterialCHT',
      'x': 'Material-Hant'
    }, {
      'a': 'SkuMaterialEN',
      'x': 'Material-English'
    }, {
      'a': 'SkuFeatureCHS',
      'x': 'Feature-CHS'
    }, {
      'a': 'SkuFeatureCHS',
      'x': 'Feature-Hans'
    }, {
      'a': 'SkuFeatureCHT',
      'x': 'Feature-CHT'
    }, {
      'a': 'SkuFeatureCHT',
      'x': 'Feature-Hant'
    }, {
      'a': 'SkuFeatureEN',
      'x': 'Feature-English'
    }, {
      'a': 'WeightKg',
      'x': 'ShippingWeightKg'
    }];

    for (var i = 0; i < tmpLangs.length; i++) {
      keys.push({
        'a': 'SkuName',
        'x': util.format('SkuName-%s', tmpLangs[i])
      });
      keys.push({
        'a': 'SkuDesc',
        'x': util.format('Description-%s', tmpLangs[i])
      });
      keys.push({
        'a': 'SkuFeature',
        'x': util.format('Feature-%s', tmpLangs[i])
      });
    }

    for (var i = 0; i < keys.length; i++) {
      if (productSkuSaveRes.request._data[keys[i].x] != undefined)
        assert.equal(res.body[keys[i].a], productSkuSaveRes.request._data[keys[i].x], util.format('%s/%s should be the same\n%s', keys[i].a, keys[i].x, log));
    }

    // date format is unmatch
    var dateKeys = [{
      'a': 'SaleFrom',
      'x': 'SalePriceFrom'
    }, {
      'a': 'SaleTo',
      'x': 'SalePriceTo'
    }, {
      'a': 'AvailableFrom',
      'x': 'AvailableFrom'
    }, {
      'a': 'AvailableTo',
      'x': 'AvailableTo'
    }];

    for (var i = 0; i < dateKeys.length; i++) {
      if (productSkuSaveRes.request._data[dateKeys[i].x] != undefined) {
        var tmpMomentA = moment(res.body[dateKeys[i].a], 'YYYY-MM-DDTHH:mm:ss.000Z');
        var tmpMomentX = moment(productSkuSaveRes.request._data[dateKeys[i].x], 'YYYY-MM-DD HH:mm:ss');
        assert.equal(tmpMomentA.format(), tmpMomentX.format(), util.format('%s/%s should be the same\n%s', dateKeys[i].a, dateKeys[i].x, log));
      }
    }

    // badge code
    if (productSkuSaveRes.request._data.BadgeCode != undefined) {
      var inputCode = productSkuSaveRes.request._data.BadgeCode;
      var tmpBadgeCodes = require('./data/badgeCodeTestData.json');
      assert.equal(res.body.BadgeId, tmpBadgeCodes[inputCode].BadgeId, util.format('%s should be the same\n%s', 'BadgeId', log));
      assert.equal(res.body.BadgeName, tmpBadgeCodes[inputCode].BadgeName[language], util.format('%s should be the same\n%s', 'BadgeName', log));
      assert.equal(res.body.BadgeNameInvariant, tmpBadgeCodes[inputCode].BadgeNameInvariant, util.format('%s should be the same\n%s', 'BadgeNameInvariant', log));
    }

    // season code
    if (productSkuSaveRes.request._data.SeasonCode != undefined) {
      var inputCode = productSkuSaveRes.request._data.SeasonCode;
      var tmpSeasonCodes = require('./data/seasonCodeTestData.json');
      assert.equal(res.body.SeasonId, tmpSeasonCodes[inputCode].SeasonId, util.format('%s should be the same\n%s', 'SeasonId', log));
      assert.equal(res.body.SeasonName, tmpSeasonCodes[inputCode].SeasonName[language], util.format('%s should be the same\n%s', 'SeasonName', log));
      assert.equal(res.body.SeasonNameInvariant, tmpSeasonCodes[inputCode].SeasonNameInvariant, util.format('%s should be the same\n%s', 'SeasonNameInvariant', log));
    }

    // size code
    if (productSkuSaveRes.request._data.SizeCode != undefined) {
      var inputCode = productSkuSaveRes.request._data.SeasonCode;
      var tmpSizeCodes = require('./data/sizeCodeTestData.json');
      assert.equal(res.body.SizeList[0].SizeId, tmpSizeCodes[inputCode].SizeId, util.format('%s should be the same\n%s', 'SizeId', log));
      assert.equal(res.body.SizeList[0].SizeName, tmpSizeCodes[inputCode].SizeName, util.format('%s should be the same\n%s', 'SizeName', log));
    }

    // geo country code
    if (productSkuSaveRes.request._data.CountryOriginCode != undefined) {
      var inputCode = productSkuSaveRes.request._data.CountryOriginCode;
      var tmpCountryCodes = require('./data/countryCodeTestData.json');
      assert.equal(res.body.GeoCountryId, tmpCountryCodes[inputCode].GeoCountryId, util.format('%s should be the same\n%s', 'SeasonId', log));
      assert.equal(res.body.GeoCountryName, tmpCountryCodes[inputCode].GeoCountryName[language], util.format('%s should be the same\n%s', 'SeasonName', log));
      assert.equal(res.body.GeoCountryNameInvariant, tmpCountryCodes[inputCode].GeoCountryNameInvariant, util.format('%s should be the same\n%s', 'SeasonNameInvariant', log));
    }

    // sku list, color, category list
    var skuKeys = [{
      'a': 'Bar',
      'x': 'Barcode'
    }, {
      a: 'QtySafetyThreshold',
      x: 'QtySafetyThreshold'
    }];
    var colorKeys = [];
    var categoryKeys = [{
      'a': 'CategoryId',
      'x': 'CategoryId'
    }];
    var sizeKeys = [{
      'a': 'SizeId',
      'x': 'SizeId'
    }];

    for (var i = 0; i < tmpLangs.length; i++) {
      skuKeys.push({
        'a': 'SkuName',
        'x': util.format('SkuName-%s', tmpLangs[i])
      });
      colorKeys.push({
        'a': 'ColorName',
        'x': util.format('ColorName-%s', tmpLangs[i])
      });
    }

    var tmpKeysList = [{
      b: res.body.SkuList[0],
      k: skuKeys
    }, {
      b: res.body.ColorList[0],
      k: colorKeys
    }, {
      b: res.body.CategoryPriorityList[0],
      k: categoryKeys
    }, {
      b: res.body.SizeList[0],
      k: sizeKeys
    }];

    for (var i = 0; i < tmpKeysList.length; i++) {
      var tmpB = tmpKeysList[i].b;
      var tmpK = tmpKeysList[i].k;
      for (var j = 0; j < tmpK.length; j++) {
        if (productSkuSaveRes.request._data[tmpK[j].x] != undefined)
          assert.equal(tmpB[tmpK[j].a], productSkuSaveRes.request._data[tmpK[j].x], util.format('%s/%s should be the same\n%s', tmpK[j].a, tmpK[j].x, log));
      }
    }

    // category list
    for (var i = 1; i < 6; i++) {
      var keyName = util.format('MerchandisingCategoryCode%d', i);
      if (productSkuSaveRes.request._data[keyName])
        assert.equal(res.body.CategoryPriorityList[i].CategoryCode, productSkuSaveRes.request._data[keyName], util.format('%s/%s should be the same\n%s', 'CategoryCode', keyName, log));
    }
  },

  brandSaveRequest: function(body, token, code) {
    return request(process.env.URL).post('/api/brand/save')
      .send(body)
      .set('Content-Type', 'application/json')
      .set('Authorization', token)
      .expect('Content-Type', /json/)
      .expect(code);
  },

  successBrandSaveRequest: function(body, token) {
    return this.brandSaveRequest(body, token, 200);
  },

  merchSaveRequest: function(body, token, code) {
    return request(process.env.URL).post('/api/merch/save')
      .send(body)
      .set('Content-Type', 'application/json')
      .set('Authorization', token)
      .expect('Content-Type', /json/)
      .expect(code);
  },

  successMerchSaveRequest: function(body, token) {
    return this.merchSaveRequest(body, token, 200);
  }
};