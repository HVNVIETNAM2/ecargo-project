var assert = require('assert');

var qaUtil = require('./qaUtil');

describe('POST /api/auth/login', function() {
  it('with valid data', function() {
    return qaUtil.successAuthLoginRequest()
      .then(function(res) {
        assert.notEqual(res.body.Token, undefined, 'Token is undefined');
        assert.notEqual(res.body.UserId, undefined, 'UserId is undefined');
        assert.notEqual(res.body.UserKey, undefined, 'UserKey is undefined');
        assert.notEqual(res.body.MerchantId, undefined, 'MerchantId is undefined');
      });
  });

  it('with invalid username', function() {
    return qaUtil.failAuthLoginRequest('invaliduser@ecargo.com', 'Bart')
      .then(function(res) {
        assert.equal(res.body.AppCode, 'MSG_ERR_USER_AUTHENTICATION_FAIL');
      });
  });

  it('with invalid password', function() {
    return qaUtil.failAuthLoginRequest('test@ecargo.com', 'invalidpassword')
      .then(function(res) {
        assert.equal(res.body.AppCode, 'MSG_ERR_USER_AUTHENTICATION_FAIL');
      });
  });

  it('missing "Username" field', function() {
    return qaUtil.authLoginRequest({
        'Password': 'Bart'
      }, 500)
      .then(function(res) {
        assert.equal(res.body.AppCode, 'MSG_ERR_REQUIRED_FIELD_MISSING');
        assert.equal(res.body.Message, 'Missing credentials');
      });
  });

  it('missing "Password" field', function() {
    return qaUtil.authLoginRequest({
        'Username': 'test@ecargo.com'
      }, 500)
      .then(function(res) {
        assert.equal(res.body.AppCode, 'MSG_ERR_REQUIRED_FIELD_MISSING');
        assert.equal(res.body.Message, 'Missing credentials');
      });
  });
});