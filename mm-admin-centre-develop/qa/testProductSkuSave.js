var assert = require('assert');
var data_driven = require('data-driven');
var moment = require('moment');
var util = require('util');
var _ = require('lodash');

var qaUtil = require('./qaUtil');

var mCredential = {};

function successCase(skuBody, language) {
  var productSkuSaveRes = {};
  return qaUtil.successProductSkuSaveRequest(skuBody, mCredential.Token)
    .then(function(res) {
      productSkuSaveRes = res;
      qaUtil.assertSuccessProductSkuSave(res);
      return qaUtil.successProductStyleViewRequest(language, res.request._data.StyleCode, res.request._data.MerchantId);
    })
    .then(function(res) {
      qaUtil.assertSuccessProductStyleView(res, productSkuSaveRes, language);
    });
}

function successOverwriteCase(skuBody, language) {
  var productSkuSaveRes = {};
  return qaUtil.successProductSkuSaveRequest(skuBody, mCredential.Token)
    .then(function(res) {
      productSkuSaveRes = res;
      qaUtil.assertSuccessProductSkuSave(res);
      return qaUtil.successProductStyleViewRequest(language, res.request._data.StyleCode, res.request._data.MerchantId);
    })
    .then(function(res) {
      return qaUtil.assertSuccessProductStyleView(res, productSkuSaveRes, language);
    })
    .then(function(res) {
      skuBody.Overwrite = 1;
      return qaUtil.successProductSkuSaveRequest(skuBody, mCredential.Token)
    })
    .then(function(res) {
      productSkuSaveRes = res;
      qaUtil.assertSuccessProductSkuSave(res);
      return qaUtil.successProductStyleViewRequest(language, res.request._data.StyleCode, res.request._data.MerchantId);
    })
    .then(function(res) {
      qaUtil.assertSuccessProductStyleView(res, productSkuSaveRes, language);
    });
}

function failCase(skuBody, errors) {
  return qaUtil.successProductSkuSaveRequest(skuBody, mCredential.Token)
    .then(function(res) {
      qaUtil.assertFailProductSkuSave(res, errors);
    });
}

describe('POST /api/product/sku/save', function() {
  before('POST /api/auth/login', function() {
    return qaUtil.successAuthLoginRequest()
      .then(function(res) {
        mCredential = res.body;
      });
  });

  it('with minimal data', function() {
    var skuBody = qaUtil.getRandomSkuBody();
    return successCase(skuBody);
  });

  var colorCodes = [{
    v: '0'
  }, {
    v: 'RED',
    s: '红色',
    t: '紅色',
    e: 'Reds'
  }, {
    v: 'PINK',
    s: '粉红色',
    t: '粉紅色',
    e: 'Pinks'
  }, {
    v: 'ORANGE',
    s: '橙色',
    t: '橙色',
    e: 'Oranges'
  }, {
    v: 'YELLOW',
    s: '黄色',
    t: '黃色',
    e: 'Yellows'
  }, {
    v: 'GREEN',
    s: '绿色',
    t: '綠色',
    e: 'Greens'
  }, {
    v: 'BLUE',
    s: '蓝色',
    t: '藍色',
    e: 'Blues'
  }, {
    v: 'PURPLE',
    s: '紫色',
    t: '紫色',
    e: 'Purples'
  }, {
    v: 'BROWN',
    s: '棕色',
    t: '棕色',
    e: 'Browns'
  }, {
    v: 'GREY',
    s: '灰色',
    t: '灰色',
    e: 'Greys'
  }, {
    v: 'WHITE',
    s: '白色',
    t: '白色',
    e: 'Whites'
  }, {
    v: 'BLACK',
    s: '黑色',
    t: '黑色',
    e: 'Blacks'
  }, {
    v: 'PRINT',
    s: '印花布',
    t: '印花布',
    e: 'Prints'
  }, {
    v: 'STRIP',
    s: '斜紋',
    t: '斜紋',
    e: 'Strips'
  }, {
    v: 'FLORAL',
    s: '花卉图案',
    t: '花卉圖案',
    e: 'Floral'
  }, {
    v: 'CHECK',
    s: '方格',
    t: '方格',
    e: 'Checks'
  }, {
    v: 'DOT',
    s: '圆点',
    t: '圓點',
    e: 'Dots'
  }];

  data_driven(colorCodes, function() {
    it('with ColorCode = {v}, ColorName-CHS = {s}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody.ColorCode = ctx.v;
      skuBody['ColorName-CHS'] = ctx.s;
      return successCase(skuBody, 'CHS');
    });
  });

  data_driven(colorCodes, function() {
    it('with ColorCode = {v}, ColorName-Hans = {s}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody.ColorCode = ctx.v;
      skuBody['ColorName-Hans'] = ctx.s;
      return successCase(skuBody, 'CHS');
    });
  });

  data_driven(colorCodes, function() {
    it('with ColorCode = {v}, ColorName-CHT = {t}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody.ColorCode = ctx.v;
      skuBody['ColorName-CHS'] = ctx.s;
      skuBody['ColorName-CHT'] = ctx.t;
      return successCase(skuBody, 'CHT');
    });
  });

  data_driven(colorCodes, function() {
    it('with ColorCode = {v}, ColorName-Hant = {t}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody.ColorCode = ctx.v;
      skuBody['ColorName-Hans'] = ctx.s;
      skuBody['ColorName-Hant'] = ctx.t;
      return successCase(skuBody, 'CHT');
    });
  });

  data_driven(colorCodes, function() {
    it('with ColorCode = {v}, ColorName-English = {e}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody.ColorCode = ctx.v;
      skuBody['ColorName-CHS'] = ctx.s;
      skuBody['ColorName-English'] = ctx.e;
      return successCase(skuBody, 'EN');
    });
  });

  var skuNames = [{
    v: 'SkuName-CHS',
    x: '女士绒面革夹克'
  }, {
    v: 'SkuName-Hans',
    x: '女士绒面革夹克'
  }, {
    v: 'SkuName-CHT',
    x: '女士絨面革夾克(繁)'
  }, {
    v: 'SkuName-Hant',
    x: '女士絨面革夾克(繁)'
  }, {
    v: 'SkuName-English',
    x: 'Suede Jacket'
  }];

  data_driven(skuNames, function() {
    it('with {v} = {x}', function(ctx) {
      var ms = moment().unix().toString() + Math.floor(Math.random() * 1000);
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody[ctx.v] = util.format('%s %s', ctx.x, moment().unix().toString() + Math.floor(Math.random() * 1000));
      if (ctx.v == 'SkuName-Hans')
        skuBody['SkuName-CHS'] = skuBody[ctx.v];
      return successCase(skuBody, ctx.v.split('-')[1]);
    });
  });

  var manufacturerNames = [{
    v: 'ManufacturerName',
    x: '布克兄弟'
  }, {
    v: 'ManufacturerName',
    x: 'BROOKS BROTHERS GROUP, INC'
  }];

  var descriptions = [{
    v: 'Description-CHS',
    x: '这种多用途的风格特点中等重量柔滑山羊麂皮设计有来自衬衫和一件夹克属性。它塑造了翻领和抛光的金钉卡扣。\n\n换股领;超大扣盖口袋在胸前;内衬袖子;轻微的高低缺口衬衣下摆;在背双肩枷锁; 27“;专业清洁;进口。'
  }, {
    v: 'Description-Hans',
    x: '这种多用途的风格特点中等重量柔滑山羊麂皮设计有来自衬衫和一件夹克属性。它塑造了翻领和抛光的金钉卡扣。\n\n换股领;超大扣盖口袋在胸前;内衬袖子;轻微的高低缺口衬衣下摆;在背双肩枷锁; 27“;专业清洁;进口。'
  }, {
    v: 'Description-CHT',
    x: '這種多用途的風格特點中等重量柔滑山羊麂皮設計有來自襯衫和一件夾克屬性。它塑造了翻領和拋光的金釘卡扣。\n\n換股領;超大扣蓋口袋在胸前;內襯袖子;輕微的高低缺口襯衣下擺;在背雙肩枷鎖; 27“;專業清潔;進口。'
  }, {
    v: 'Description-Hant',
    x: '這種多用途的風格特點中等重量柔滑山羊麂皮設計有來自襯衫和一件夾克屬性。它塑造了翻領和拋光的金釘卡扣。\n\n換股領;超大扣蓋口袋在胸前;內襯袖子;輕微的高低缺口襯衣下擺;在背雙肩枷鎖; 27“;專業清潔;進口。'
  }, {
    v: 'Description-English',
    x: 'This versatile style features mid-weight silky goat suede and is designed with attributes from both a shirt and a jacket. It is fashioned with a convertible collar and polished gold stud snaps.\n\nConvertible collar; oversized snap-flap pockets at chest; lined sleeves; slight hi-low notched shirttail hem; shoulder yoke at back; 27"; professionally clean; imported.'
  }];

  var sizeComments = [{
    v: 'SizeComment-CHS',
    x: '备注 1'
  }, {
    v: 'SizeComment-Hans',
    x: '备注 1'
  }, {
    v: 'SizeComment-CHT',
    x: '備註 1'
  }, {
    v: 'SizeComment-Hant',
    x: '備註 1'
  }, {
    v: 'SizeComment-English',
    x: 'Comment 1'
  }];

  var materials = [{
    v: 'Material-CHS',
    x: '柔滑山羊麂皮'
  }, {
    v: 'Material-Hans',
    x: '柔滑山羊麂皮'
  }, {
    v: 'Material-CHT',
    x: '柔滑山羊麂皮'
  }, {
    v: 'Material-Hant',
    x: '柔滑山羊麂皮'
  }, {
    v: 'Material-English',
    x: 'silky goat suede'
  }];

  var features = [{
    v: 'Feature-CHS',
    x: '换股领\n超大扣盖口袋'
  }, {
    v: 'Feature-Hans',
    x: '换股领\n超大扣盖口袋'
  }, {
    v: 'Feature-CHT',
    x: '換股領\n超大扣蓋口袋'
  }, {
    v: 'Feature-Hant',
    x: '換股領\n超大扣蓋口袋'
  }, {
    v: 'Feature-English',
    x: 'convertible collar\noversized snap-flap pockets'
  }];

  var barcodes = [{
    v: 'Barcode',
    x: '0'
  }, {
    v: 'Barcode',
    x: '123456789012'
  }];

  var brandIds = [{
    v: 'BrandId',
    x: 189
  }];

  var brandCodes = [{
    v: 'BrandCode',
    x: 'QABRAND'
  }];

  var categoryIds = [{
    v: 'CategoryId',
    x: 1
  }];

  var merchandisingCategoryCodes = [{
    v: ['MerchandisingCategoryCode1', 'MerchandisingCategoryCode2', 'MerchandisingCategoryCode3', 'MerchandisingCategoryCode4', 'MerchandisingCategoryCode5'],
    x: ['women-apparel-jackets-downjackets', 'women-apparel-jackets-coats', 'women-apparel-jackets-windbreakers', 'women-apparel-jackets-vests', 'women-apparel-jackets-sweaters']
  }];

  var shippingWeightKgs = [{
    v: 'ShippingWeightKg',
    x: 0
  }];

  var priceSales = [{
    v: ['PriceSale', 'SalePriceFrom', 'SalePriceTo', 'AvailableFrom', 'AvailableTo'],
    x: [987.6, moment().add(1, 'day').format('YYYY-MM-DD HH:mm:ss'), moment().add(2, 'day').format('YYYY-MM-DD HH:mm:ss'), moment().format('YYYY-MM-DD HH:mm:ss'), moment().add(3, 'day').format('YYYY-MM-DD HH:mm:ss')],
  }];

  var badgeIds = [];
  for (var i = 0; i < 8; i++) {
    badgeIds.push({
      v: 'BadgeId',
      x: i
    })
  }

  var seasonIds = [];
  for (var i = 0; i < 11; i++) {
    seasonIds.push({
      v: 'SeasonId',
      x: i
    })
  }

  var sizeIds = [];
  for (var i = 0; i < 10; i++) { // just random pick 10 from 0 to 557
    var rSizeId = Math.floor(Math.random() * 558);
    sizeIds.push({
      v: 'SizeId',
      x: rSizeId
    })
  }

  var geoCountryIds = [];
  for (var i = 0; i < 10; i++) { // just random pick 10 from 0 to 251
    var rGeoCountryId = Math.floor(Math.random() * 252);
    geoCountryIds.push({
      v: 'GeoCountryId',
      x: rGeoCountryId
    })
  }

  var launchYears = [];
  for (var i = 0; i < 3; i++) { // just random pick 3 from 1970 to 2070
    var rLaunchYear = Math.floor(Math.random() * 101) + 1970;
    launchYears.push({
      v: 'LaunchYear',
      x: rLaunchYear
    })
  }

  var dimensions = [];
  for (var i = 0; i < 3; i++) { // just random pick 3
    var rDimension = (Math.random() * 1001).toFixed(2);
    dimensions.push({
      v: ['HeightCm', 'WidthCm', 'LengthCm'],
      x: [rDimension, rDimension, rDimension]
    })
  }

  var qtySafetyThresholds = [];
  for (var i = 0; i < 3; i++) { // just random pick 3
    var rQtySafetyThreshold = Math.floor(Math.random() * 101);
    qtySafetyThresholds.push({
      v: 'QtySafetyThreshold',
      x: rQtySafetyThreshold
    })
  }

  var inputs = [].concat(manufacturerNames, descriptions, sizeComments, materials, features, barcodes, brandIds, brandCodes, categoryIds, merchandisingCategoryCodes, shippingWeightKgs, priceSales, badgeIds, seasonIds, sizeIds, geoCountryIds, launchYears, dimensions, qtySafetyThresholds);

  data_driven(inputs, function() {
    it('with {v} = {x}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      if (ctx.v instanceof Array && ctx.x instanceof Array) {
        for (var i = 0; i < ctx.v.length; i++) {
          skuBody[ctx.v[i]] = ctx.x[i];
        }
        return successCase(skuBody);
      } else {
        skuBody[ctx.v] = ctx.x;
        return successCase(skuBody, ctx.v.split('-')[1]);
      }
    });
  });

  var tmpBadgeCodes = require('./data/badgeCodeTestData.json')
  var badgeCodes = [];
  for (var badgeCode in tmpBadgeCodes) {
    badgeCodes.push({
      v: badgeCode,
      x: 'CHS'
    });
    badgeCodes.push({
      v: badgeCode,
      x: 'CHT'
    });
    badgeCodes.push({
      v: badgeCode,
      x: 'English'
    });
  }
  data_driven(badgeCodes, function() {
    it('with BadgeCode = {v} {x}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody.BadgeCode = ctx.v;
      return successCase(skuBody, ctx.x);
    });
  });

  var tmpSeasonCodes = require('./data/seasonCodeTestData.json');
  var seasonCodes = [];
  for (var seasonCode in tmpSeasonCodes) {
    seasonCodes.push({
      v: seasonCode,
      x: 'CHS'
    });
    seasonCodes.push({
      v: seasonCode,
      x: 'CHT'
    });
    seasonCodes.push({
      v: seasonCode,
      x: 'English'
    });
  }
  data_driven(seasonCodes, function() {
    it('with SeasonCode = {v} {x}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody.SeasonCode = ctx.v;
      return successCase(skuBody, ctx.x);
    });
  });

  var tmpSizeCodes = require('./data/sizeCodeTestData.json');
  var sizeCodes = [];
  for (var i = 0; i < 10; i++) {
    var rSizeCode = _.sample(tmpSizeCodes).SizeCode;
    sizeCodes.push({
      v: rSizeCode,
      x: 'CHS'
    });
    sizeCodes.push({
      v: rSizeCode,
      x: 'CHT'
    });
    sizeCodes.push({
      v: rSizeCode,
      x: 'English'
    });
  }
  data_driven(sizeCodes, function() {
    it('with SizeCode = {v} {x}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody.SizeCode = ctx.v;
      return successCase(skuBody, ctx.x);
    });
  });

  var tmpCountryCodes = require('./data/countryCodeTestData.json');
  var countryCodes = [];
  for (var i = 0; i < 10; i++) {
    var rCountryCode = _.sample(tmpCountryCodes).CountryCode;
    countryCodes.push({
      v: rCountryCode,
      x: 'CHS'
    });
    countryCodes.push({
      v: rCountryCode,
      x: 'CHT'
    });
    countryCodes.push({
      v: rCountryCode,
      x: 'English'
    });
  }
  data_driven(countryCodes, function() {
    it('with CountryOriginCode = {v} {x}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody.CountryOriginCode = ctx.v;
      return successCase(skuBody, ctx.x);
    });
  });

  data_driven([{
    v: 2
  }, {
    v: 4
  }], function() {
    it('with StatusId = {v}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      skuBody.StatusId = ctx.v;
      return successOverwriteCase(skuBody, ctx.x);
    });
  });

  var wrongBrandIds = [{
    v: 'BrandId',
    x: 2,
    e: [{
      AppCode: 'MSG_ERR_PRODUCT_IMPORT',
      Message: 'BrandId',
    }]
  }];

  var wrongBrandCodes = [{
    v: 'BrandCode',
    x: 'WRONGBRAND',
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_FOUND',
      Message: 'BrandCode',
    }]
  }];

  var wrongCategoryIds = [{
    v: ['CategoryId', 'PrimaryCategoryCode'],
    x: [18, undefined],
    e: [{
      AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING',
      Message: 'CategoryId',
    }]
  }];

  var wrongMerchandisingCategoryCodes = [{
    v: 'MerchandisingCategoryCode2',
    x: 'women-apparel-jackets-downjackets',
    e: [{
      AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING',
      Message: 'MerchandisingCategory',
    }]
  }, {
    v: 'MerchandisingCategoryCode3',
    x: 'women-apparel-jackets-downjackets',
    e: [{
      AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING',
      Message: 'MerchandisingCategory',
    }]
  }, {
    v: 'MerchandisingCategoryCode4',
    x: 'women-apparel-jackets-downjackets',
    e: [{
      AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING',
      Message: 'MerchandisingCategory',
    }]
  }, {
    v: 'MerchandisingCategoryCode5',
    x: 'women-apparel-jackets-downjackets',
    e: [{
      AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING',
      Message: 'MerchandisingCategory',
    }]
  }, {
    v: 'MerchandisingCategoryCode1',
    x: 'women-apparel-tops-knitwears',
    e: [{
      AppCode: 'MSG_ERR_REQUIRED_FIELD_MISSING',
      Message: 'MerchandisingCategory',
    }]
  }];

  var wrongColorIds = [{
    v: 'ColorId',
    x: 17,
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_FOUND',
      Message: 'ColorId',
    }]
  }];

  var wrongPriceSales = [{
    v: ['PriceSale', 'SalePriceFrom', 'SalePriceTo', 'AvailableFrom', 'AvailableTo'],
    x: [5678.9, moment().add(1, 'day').format('YYYY-MM-DD HH:mm:ss'), moment().add(2, 'day').format('YYYY-MM-DD HH:mm:ss'), moment().format('YYYY-MM-DD HH:mm:ss'), moment().add(3, 'day').format('YYYY-MM-DD HH:mm:ss')],
    e: [{
      AppCode: 'MSG_ERR_PRODUCT_IMPORT',
      Message: 'PriceSale',
    }]
  }, {
    v: ['PriceSale', 'SalePriceFrom', 'SalePriceTo', 'AvailableFrom', 'AvailableTo'],
    x: [56.7, moment().add(2, 'day').format('YYYY-MM-DD HH:mm:ss'), moment().add(1, 'day').format('YYYY-MM-DD HH:mm:ss'), moment().format('YYYY-MM-DD HH:mm:ss'), moment().add(3, 'day').format('YYYY-MM-DD HH:mm:ss')],
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_VALID',
      Message: 'SalePriceFrom',
    }]
  }, {
    v: ['PriceSale', 'SalePriceFrom', 'SalePriceTo', 'AvailableFrom', 'AvailableTo'],
    x: [56.7, moment().add(1, 'day').format('YYYY-MM-DD HH:mm:ss'), moment().add(4, 'day').format('YYYY-MM-DD HH:mm:ss'), moment().format('YYYY-MM-DD HH:mm:ss'), moment().add(3, 'day').format('YYYY-MM-DD HH:mm:ss')],
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_VALID',
      Message: 'SalePriceTo',
    }]
  }, {
    v: ['PriceSale', 'SalePriceFrom', 'SalePriceTo', 'AvailableFrom', 'AvailableTo'],
    x: [56.7, moment().format('YYYY-MM-DD HH:mm:ss'), moment().add(2, 'day').format('YYYY-MM-DD HH:mm:ss'), moment().add(1, 'day').format('YYYY-MM-DD HH:mm:ss'), moment().add(3, 'day').format('YYYY-MM-DD HH:mm:ss')],
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_VALID',
      Message: 'SalePriceFrom',
    }]
  }];

  var wrongBadgeIds = [{
    v: 'BadgeId',
    x: 8,
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_FOUND',
      Message: 'BadgeId',
    }]
  }];

  var wrongBadgeCodes = [{
    v: 'BadgeCode',
    x: 'WRONGBADGE',
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_FOUND',
      Message: 'BadgeCode',
    }]
  }];

  var wrongSeasonIds = [{
    v: 'SeasonId',
    x: 11,
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_FOUND',
      Message: 'SeasonId',
    }]
  }];

  var wrongSeasonCodes = [{
    v: 'SeasonCode',
    x: 'WRONGSEASON',
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_FOUND',
      Message: 'SeasonCode',
    }]
  }];

  var wrongSizeIds = [{
    v: 'SizeId',
    x: 558,
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_FOUND',
      Message: 'SizeId',
    }]
  }];

  var wrongSizeCodes = [{
    v: 'SizeCode',
    x: 'WRONGSIZE',
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_FOUND',
      Message: 'SizeCode',
    }]
  }];

  var wrongGeoCountryIds = [{
    v: 'GeoCountryId',
    x: 252,
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_FOUND',
      Message: 'GeoCountryId',
    }]
  }];

  var wrongCountryCodes = [{
    v: 'CountryOriginCode',
    x: 'WRONGCOUNTRY',
    e: [{
      AppCode: 'MSG_ERR_FIELD_NOT_FOUND',
      Message: 'CountryOriginCode',
    }]
  }];

  var wrongInputs = [].concat(wrongBrandIds, wrongBrandCodes, wrongCategoryIds, wrongMerchandisingCategoryCodes, wrongColorIds, wrongPriceSales, wrongBadgeIds, wrongBadgeCodes, wrongSeasonIds, wrongSeasonCodes, wrongSizeIds, wrongSizeCodes, wrongGeoCountryIds, wrongCountryCodes);

  data_driven(wrongInputs, function() {
    it('with wrong {v} = {x}', function(ctx) {
      var skuBody = qaUtil.getRandomSkuBody();
      if (ctx.v instanceof Array && ctx.x instanceof Array) {
        for (var i = 0; i < ctx.v.length; i++) {
          if (ctx.x[i] == undefined)
            delete skuBody[ctx.v[i]];
          else
            skuBody[ctx.v[i]] = ctx.x[i];
        }
        return failCase(skuBody, ctx.e);;
      } else {
        if (ctx.x == undefined)
          delete skuBody[ctx.v];
        else
          skuBody[ctx.v] = ctx.x;
        return failCase(skuBody, ctx.e);;
      }
    });
  });
});