var assert = require('assert');
var data_driven = require('data-driven');
var moment = require('moment');
var util = require('util');

var qaUtil = require('./qaUtil');

var mCredential = {};

describe('POST /api/brand/save', function() {
  before('POST /api/auth/login', function() {
    return qaUtil.successAuthLoginRequest()
      .then(function(res) {
        mCredential = res.body;
      });
  });

  it('with valid data', function() {
    var ms = moment().unix().toString() + Math.floor(Math.random() * 1000);
    return qaUtil.successBrandSaveRequest({
      Brand: {
        DisplayName: {
          CHS: '质量保证牌',
          CHT: '質量保證牌',
          EN: 'QA Brand'
        },
        BrandSubdomain: 'qabrand',
        BrandCode: 'QABRAND'
      }
    }, mCredential.Token).then(function(res) {});
  });
});