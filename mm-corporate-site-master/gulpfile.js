var gulp = require('gulp')
var sass = require('gulp-sass')
var pump = require('pump')
var browserify = require('browserify')
var fs = require('fs')
var source = require('vinyl-source-stream')
var buffer = require('vinyl-buffer')
var prefix = require('gulp-autoprefixer')
var livereload = require('gulp-livereload')
var minifycss = require('gulp-minify-css')
var sourcemaps = require('gulp-sourcemaps')
var rename = require('gulp-rename')
var filter = require('gulp-filter')
var sequence = require('run-sequence')
var touch = require('touch')
var del = require('del')
var vinylPaths = require('vinyl-paths')
var ejs = require('gulp-ejs')
var parallel = require('parallel-multistream')

var config = require('./config.json')

gulp.task('server', function () {
  require('./server')
})

function displayError (err) {
  if (!err) return
  var errorString
  errorString = '[' + err.plugin + ']'
  errorString += ' ' + err.message.replace('\n', '')
  if (err.fileName) errorString += ' in ' + err.fileName
  if (err.lineNumber) errorString += ' on line ' + err.lineNumber
  console.error(errorString)
}

gulp.task('clean', function () {
  return pump(
    gulp.src([
      './public/css',
      './public/js'
    ], { read: false }),
    vinylPaths(del)
  )
})

gulp.task('styles', function () {
  return pump(
    gulp.src('./sass/*.scss'),
    sass(),
    prefix(),
    sourcemaps.write(),
    gulp.dest('./public/css'),
    filter('main.css'),
    rename({ suffix: '.min' }),
    minifycss(),
    gulp.dest('./public/css'),
    displayError
  )
})

gulp.task('reloadcss', function () {
  return pump(
    gulp.src('./public/css/main.css'),
    livereload()
  )
})

gulp.task('browserify', function () {
  return pump(
    browserify({
      entries: './browser/index.js',
      standalone: 'ecargo'
    })
    .bundle(),
    source('bundle.js'),
    buffer(),
    sourcemaps.init({loadMaps: true}),
    sourcemaps.write('./'),
    gulp.dest('./public/js'),
    livereload(),
    displayError
  )
})

gulp.task('i18n', function () {
  return parallel(
    config.languages.map(function (lang, i) {
      return pump(
        gulp.src('./templates/*.ejs'),
        ejs(JSON.parse(
          fs.readFileSync('./i18n/' + lang + '.json').toString()
        )),
        gulp.dest('./public' + (i > 0 ? '/' + lang : '')),
        displayError
      )
    })
  )
})

gulp.task('serverScripts', function () {
  touch('gulpfile.js')
})

gulp.task('watch', function () {
  gulp.watch([
    './templates/*.ejs',
    './i18n/*.json'
  ], ['i18n'])
  gulp.watch(['./sass/**/*.scss'], ['styles'])
  gulp.watch(['./browser/**/*.js'], ['browserify'])
  gulp.watch(['./public/**/*.html'], ['browserify'])
  gulp.watch(['./public/css/main.css'], ['reloadcss'])
  gulp.watch([
    './*.js', './lib/**/*.js', '!./gulpfile.js'
  ], ['serverScripts'])
})

gulp.task('start', function () {
  sequence('server', 'clean', ['browserify', 'styles', 'i18n'], 'watch')
})

gulp.task('build', function () {
  sequence('clean', ['browserify', 'styles', 'i18n'])
})
