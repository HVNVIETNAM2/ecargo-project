var logger = require('log4js').getLogger()
var pkg = require('./package.json')
var express = require('express')
var xtend = require('xtend')

var config = require('./config.json')

var port = config.port

var app = express()

app.use(express.static('./public'))

app.listen(port)
logger.info('%s started at http://localhost:%s', pkg.name, port)
