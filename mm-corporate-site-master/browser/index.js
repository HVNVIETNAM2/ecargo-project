require('angular')
require('angular-resource')
require('angular-route')

var xtend = require('xtend')
var angular = global.angular
var app = angular.module('app', ['ngResource', 'ngRoute'])

app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: '/templates/list.html',
      controller: 'listCtrl'
    })
    .when('/brand/:brand_id', {
      templateUrl: '/templates/list.html',
      controller: 'listCtrl'
    })
    .when('/product/:product_id', {
      templateUrl: '/templates/page.html',
      controller: 'pageCtrl'
    })
    .otherwise({
      redirectTo: '/'
    })
}])

app.controller('appCtrl', [
  '$scope', '$timeout', '$routeParams',
  function () {
  }
])
app.factory('products', ['$resource', function ($resource) {
  return $resource('/api/products/:product_id')
}])
app.factory('reviews', ['$resource', function ($resource) {
  return $resource('/api/products/:product_id/reviews')
}])

app.controller('listCtrl', [
  '$scope', '$timeout', '$routeParams', 'products',
  function ($scope, $timeout, $routeParams, products) {
    $scope.isEmpty = false
    $scope.products = []

    products.query({ brand_id: $routeParams.brand_id }, function (data) {
      $scope.products = data
      $scope.isEmpty = !data.length
    })
  }
])

app.controller('pageCtrl', [
  '$scope', '$timeout', '$routeParams', 'products', 'reviews',
  function ($scope, $timeout, $routeParams, products, reviews) {
    var product_id = $routeParams.product_id

    $scope.error = null
    $scope.product = {}
    $scope.reviews = []

    products.get({ product_id: product_id }, function (data) {
      $scope.product = data

      reviews.query({ product_id: product_id }, function (data) {
        $scope.reviews = data
      })
    }, function (err) {
      $scope.error = err.data.message
      $scope.product = null
    })

    var defaults = {
      rating: 5,
      comment: ''
    }

    $scope.reset = function () {
      $scope.form = xtend(defaults)
    }

    $scope.form = xtend(defaults)

    $scope.submit = function () {
      reviews.save({ product_id: product_id }, $scope.form, function () {
        $scope.form = xtend(defaults)
        reviews.query({ product_id: product_id }, function (data) {
          $scope.reviews = data
        })
      }, function (err) {
        $scope.error = err.data.message
      })
    }
  }
])
