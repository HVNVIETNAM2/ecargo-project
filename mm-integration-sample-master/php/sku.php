<!DOCTYPE html>
<!--
-->
<?php

include_once 'modules/SKUManager.php';
include_once 'modules/HtmlObjectPrinter.php';

$skuManager = new SKUManager();

$cultureCode = $_REQUEST['cc'];
$skuID = $_REQUEST['skuid'];
$merchantID = $_REQUEST['merchantid'];

$sku = $skuManager->getSku($cultureCode, $skuID, $merchantID);

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>mm-integration-sample-php</title>
    </head>
    <body>
        <h1>MM Integration Sample (PHP)</h1>
        <h2><?php echo $_REQUEST['title'];?></h2>
        <?php if (isset($sku)): ?>
            <?php echo HtmlObjectPrinter::obj2Table($sku); ?>
        <?php else: ?>
            <h1 style="color:red">NOT FOUND</h1>
            <?php echo HtmlObjectPrinter::obj2Table($_REQUEST); ?>
        <?php endif; ?>
        <?php include_once 'footer.php';?>
    </body>
</html>