<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class HtmlObjectPrinter {

    public static function obj2Table($obj, $showValueType=TRUE) {
        $content = '';
        if (!isset($obj)) {
            throw new Exception('Empty obj in HtmlObjectPrinter!');
        }

        if (($obj instanceof stdClass || is_array($obj))) {
            foreach ($obj as $key => $value) {
                $displayVal = (!is_object($value) && !is_array($value)) ? self::_formatVal($value) : self::obj2Table($value, $showValueType);
                $valueType = $showValueType?("<td style='vertical-align: top;background-color:gold'>".gettype($value)."</td>"):"";
                $content .= "<tr><td style='vertical-align: top;'><b>$key</b></td>$valueType<td>$displayVal</td></tr>";
            }
        } else {
            $content .= "<tr><td style='background-color:gold'>" . gettype($obj) . "</td><td><td>" .  self::_formatVal($obj) . "</td></tr>";
        }

        $output = "<table border='10'>$content</table>";
        return $output;
    }

    private static function _formatVal($val) {
        $result = $val;
        if (is_bool($val)) {
            $result = json_encode($val);
        }

        return $result;
    }

}
