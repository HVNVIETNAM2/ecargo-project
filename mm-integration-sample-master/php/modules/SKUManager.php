<?php

include_once 'ConfigReader.php';
include_once 'CSVReader.php';
include_once 'AuthManager.php';
include_once 'ColorManager.php';
include_once 'APICaller.php';
include_once 'BaseManager.php';
include_once 'HtmlObjectPrinter.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SKUManager extends BaseManager {

    private $_fieldMappings;
    private $_merchantAuthority;
    private $_colorList;

    public function __construct() {
        parent::__construct();
        $this->_fieldMappings = parse_ini_file(__DIR__ . '/../config/sku.ini');
        $authManager = new AuthManager($this->config->getLoginAPI());
        $loginResponse = $authManager->login();
        if ($loginResponse['code'] === 200) {
            $this->_merchantAuthority = $loginResponse['response'];
        } else {
            throw new Exception('Fail to login!');
        }

        $this->_setMerchantColorList();
    }

    private function _setMerchantColorList() {
        $colorDataFilePath = $this->config->getColorDataFilePath();
        $colorsCSVData = CSVReader::readData($colorDataFilePath, TRUE)['objs'];

//         echo HtmlObjectPrinter::obj2Table($colorsCSVData);

        if (!isset($colorsCSVData)) {
            throw new Exception('Missing color data!');
        }

        $this->_colorList = array();

        $colorManager = new ColorManager();
        $cultureCodes = array('EN', 'CHS', 'CHT');

        $mmColors = array();

        foreach ($cultureCodes as $cultureCode) {
            $colorList = $colorManager->getColorList($cultureCode);
            foreach ($colorList as $color) {
                $mmColors[$color->ColorCode][$cultureCode] = $color;
            }
        }

        foreach ($colorsCSVData as $colorCSVData) {
            $merchantColorDescription = $colorCSVData['Color Description'];

            foreach ($mmColors as $mmColorCode => $mmColor) {
                if (strrpos(strtolower(trim($merchantColorDescription)), strtolower(trim($mmColorCode))) !== FALSE) {
                    $merchantColorCode = $colorCSVData['Color Code'];
                    $merchantColorInfo = " [$merchantColorCode]$merchantColorDescription";

                    $this->_colorList[$merchantColorCode] = array(
                        'ColorCode' => $mmColor['EN']->ColorCode,
                        'ColorENName' => $mmColor['EN']->ColorName . $merchantColorInfo,
                        'ColorCHSName' => $mmColor['CHS']->ColorName . $merchantColorInfo,
                        'ColorCHTName' => $mmColor['CHT']->ColorName . $merchantColorInfo
                    );
                    break;
                }
            }
        }

//        echo HtmlObjectPrinter::obj2Table($this->_colorList);
    }

    private function _convert2MMObj($fields) {
        $result = array();
        foreach ($this->_fieldMappings as $mmkey => $merchantKey) {
            if (isset($fields[$merchantKey])) {
                $result[$mmkey] = $fields[$merchantKey];
            }
        }
        return $result;
    }

    private function _processMMSku(&$mmSKU, $isDebug = TRUE) {
        $success = FALSE;
        $skuSaveResult = $this->_saveSKU($mmSKU);
        if ($skuSaveResult['code'] === 200 &&
                $skuSaveResult['response']->success === TRUE) {
            if ($isDebug) {
                echo HtmlObjectPrinter::obj2Table($mmSKU);
            }
            $success = TRUE;
        } else {
            if ($isDebug) {
                echo HtmlObjectPrinter::obj2Table($mmSKU);
                echo HtmlObjectPrinter::obj2Table($skuSaveResult);
            }
        }
        return $success;
    }

    public function import($limit = 1) {

        $result = array(
            'total' => 0,
            'success' => 0,
            'fail' => 0
        );

        $total = $success = 0;

        $skuDataFilePath = $this->config->getSKUDataFilePath();
        $styleDataFilePath = $this->config->getStyleDataFilePath();

        $styleCSVData = CSVReader::readData($styleDataFilePath, TRUE, 'item_no');
        $styles = $styleCSVData['objs'];

        $skuCSVData = CSVReader::readData($skuDataFilePath, TRUE);
        $skus = $skuCSVData['objs'];

        if (sizeof($skus)) {
            foreach ($skus as $sku) {
                $sku['skucode'] = $sku['item_code'] . '-' . $sku['color'] . '-' . $sku['size'];
                $mergedSku = array_merge($sku, $styles[$sku['item_code']]);
                $mmSKU = $this->_convert2MMObj($mergedSku);

                if ($this->_processMMSku($mmSKU)) {
                    $success++;
                }

                $total++;
                $limit--;
                if (!$limit) {
                    break;
                }
            }
        }

        $result['total'] = $total;
        $result['success'] = $success;
        $result['fail'] = $total - $success;

        return $result;
    }

    private function formatDate($date) {
        $formattedDate = NULL;
        if (isset($date)) {
            $formattedDate = date_format(date_create($date), 'Y-m-d H:i:s');
        }
        return $formattedDate;
    }

    private function _saveSKU(&$mmSKU) {

        $headers = array(
            'Authorization: ' . $this->_merchantAuthority->Token
        );

        $mmSKU['MerchantId'] = $this->_merchantAuthority->MerchantId;
        $mmSKU['Overwrite'] = 1;
//        $mmSKU['Overwrite'] = 0;
//        $mmSKU['PrimaryCategoryCode'] = 'men';
        $mmSKU['PrimaryCategoryCode'] = 'men-shirts';
        $mmSKU['ShippingWeightKg'] = 0;

        $colorData = isset($this->_colorList[$mmSKU['ColorCode']]) ? $this->_colorList[$mmSKU['ColorCode']] : NULL;

        if (isset($colorData)) {
            $mmSKU['ColorCode'] = $colorData['ColorCode'];

            $mmSKU['ColorName-English'] = isset($colorData['ColorENName']) ? $colorData['ColorENName'] : "";
            $mmSKU['ColorName-Hans'] = isset($colorData['ColorCHSName']) ? $colorData['ColorCHSName'] : "";
            $mmSKU['ColorName-Hant'] = isset($colorData['ColorCHTName']) ? $colorData['ColorCHTName'] : "";
        } else {
            $mmSKU['ColorCode'] = "0";
        }

        $mmSKU['AvailableFrom'] = $this->formatDate($mmSKU['AvailableFrom']);
        $mmSKU['AvailableTo'] = $this->formatDate($mmSKU['AvailableTo']);
        $mmSKU['SalePriceFrom'] = $this->formatDate($mmSKU['SalePriceFrom']);
        $mmSKU['SalePriceTo'] = $this->formatDate($mmSKU['SalePriceTo']);

        $skuSaveResponse = APICaller::post($this->config->getSkuSaveAPI(), $mmSKU, $headers);
        return $skuSaveResponse;
    }

    public function getSku($cultureCode, $skuID, $merchantID, $styleCode = NULL) {
        $url = $this->config->getGetSkuAPI();

        $params = array(
            'cc' => $cultureCode,
            'skuid' => $skuID,
            'merchantid' => $merchantID
        );

        if (isset($styleCode)) {
            $params['stylecode'] = $styleCode;
        }

        $response = APICaller::get($url, $params);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }

    public function getSkuList($cultureCode, $merchantID = NULL, $styleCode = NULL, $statusID = NULL, $search = NULL, $page = 1, $size = 25) {
        $url = $this->config->getGetSkuListAPI();

        $params = array(
            'cc' => $cultureCode
        );

        if (isset($merchantID)) {
            $params['merchantid'] = $merchantID;
        }
        if (isset($styleCode)) {
            $params['stylecode'] = $styleCode;
        }
        if (isset($statusID)) {
            $params['statusid'] = $statusID;
        }
        if (isset($search)) {
            $params['s'] = $search;
        }
        if (isset($page)) {
            $params['page'] = $page;
        }
        if (isset($size)) {
            $params['size'] = $size;
        }

        $response = APICaller::get($url, $params);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }

}
