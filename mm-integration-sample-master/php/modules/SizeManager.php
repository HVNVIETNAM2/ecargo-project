<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once 'APICaller.php';
include_once 'BaseManager.php';

class SizeManager extends BaseManager {

    public function getSizeList($cultureCode) {
        $url = $this->config->getGetSizeListAPI();
        $params = array(
            'cc' => $cultureCode
        );

        $response = APICaller::get($url, $params);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }

}
