<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once 'APICaller.php';
include_once 'BaseManager.php';

class BrandManager extends BaseManager {

    public function getBrandList($cultureCode, $merchantID = NULL) {
        $url = $this->config->getGetBrandListAPI();
        $params = array(
            'cc' => $cultureCode
        );

        if (isset($merchantID)) {
            $params['merchantid'] = $merchantID;
        }

        $response = APICaller::get($url, $params);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }

}
