<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once 'APICaller.php';

class AuthManager {

    private $_loginAPI;

    public function __construct($loginAPI) {
        $this->_loginAPI = $loginAPI;
    }

    public function login() {
        $merchant = parse_ini_file(__DIR__ . '/../config/merchant.ini');
        $loginResult = APICaller::post($this->_loginAPI, array(
                    'Username' => $merchant['username'],
                    'Password' => $merchant['password']
        ));

        return $loginResult;
    }

}
