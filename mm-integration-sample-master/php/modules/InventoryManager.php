<?php

include_once 'ConfigReader.php';
include_once 'CSVReader.php';
include_once 'AuthManager.php';
include_once 'APICaller.php';
include_once 'BaseManager.php';
include_once 'SKUManager.php';
include_once 'HtmlObjectPrinter.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class InventoryManager extends BaseManager {

    private $_fieldMappings;
    private $_merchantAuthority;
    private $_skuManager;

    public function __construct() {
        parent::__construct();
        $this->_fieldMappings = parse_ini_file(__DIR__ . '/../config/inventory.ini');
        $authManager = new AuthManager($this->config->getLoginAPI());
        $loginResponse = $authManager->login();
        if ($loginResponse['code'] === 200) {
            $this->_merchantAuthority = $loginResponse['response'];
        } else {
            throw new Exception('Fail to login!');
        }

        $this->_skuManager = new SKUManager();
        $this->_skuCodeIDMap = array();
    }

//    private function _convert2MMObj($fields) {
//        $result = array();
//        foreach ($this->_fieldMappings as $mmkey => $merchantKey) {
//            if (isset($fields[$merchantKey])) {
//                $result[$mmkey] = $fields[$merchantKey];
//            }
//        }
//        return $result;
//    }
//
//    private function _processMMSku($mmSKU, $merchantAuthority) {
//        $success = false;
//        $skuSaveResult = $this->saveSKU($mmSKU, $merchantAuthority);
//        if ($skuSaveResult['code'] === 200 &&
//                $skuSaveResult['response']->success === true) {
//            $success = true;
//        }
//        return $success;
//    }

    public function import($limit = 1) {

        // 1. Read txt data
        // 2. Find skuID by StyleCode and SkuCode
        // 3. Find inventory Location ID
        // 4. Save Inventories

        $result = array(
            'total' => 0,
            'success' => 0,
            'fail' => 0
        );

        $total = $success = 0;

        $inventoryDataFilePath = $this->config->getInventoryDataFilePath();

        $inventoryCSVData = CSVReader::readData($inventoryDataFilePath, TRUE);
        $inventories = $inventoryCSVData['objs'];

        if (sizeof($inventories)) {

            foreach ($inventories as $inventory) {
                $styleCode = trim($inventory['Item']);
                $qty = trim($inventory['Qty']);
                $skuCode = $styleCode . '-' . trim($inventory['Color']) . '-' . trim($inventory['Size']);

                if (!isset($this->_skuCodeIDMap[$skuCode])) {
                    // Assume default 25 skus is good enough for the purpose
                    $skuList = $this->_skuManager->getSkuList('EN', $this->_merchantAuthority->MerchantId, $styleCode);

                    if (isset($skuList->PageData) && sizeof($skuList->PageData) > 0) {
                        $skus = $skuList->PageData;

                        foreach ($skus as $sku) {
                            $this->_skuCodeIDMap[$sku->SkuCode] = $sku->SkuId;
                        }
                    }
                }

                $skuID = isset($this->_skuCodeIDMap[$skuCode]) ? $this->_skuCodeIDMap[$skuCode] : NULL;

//               echo HtmlObjectPrinter::obj2Table(array($skuID, $inventoryLocationID, $qty));

                $inventoryLocationID = $this->findInventoryLocationID($inventory['Location']);

                if (isset($inventoryLocationID) && isset($skuID) && isset($qty)) {
                    $inventory = $this->saveInventory($skuID, $inventoryLocationID, $qty);

                    echo HtmlObjectPrinter::obj2Table($inventory);

                    if (isset($inventory->InventoryId)) {
                        $success++;
                    }
                }

                $total++;
                $limit--;
                if (!$limit) {
                    break;
                }
            }
        }

        $result['total'] = $total;
        $result['success'] = $success;
        $result['fail'] = $total - $success;

        return $result;
    }

    private function findInventoryLocationID($locationExternalCode) {
        $inventoryLocationId = NULL;
        $inventoryLocations = $this->getInventoryLocationList('EN');

        foreach ($inventoryLocations as $inventoryLocation) {
            if ($locationExternalCode === $inventoryLocation->LocationExternalCode) {
                $inventoryLocationId = $inventoryLocation->InventoryLocationId;
                break;
            }
        }

        return $inventoryLocationId;
    }

    public function saveInventory($skuID, $inventoryLocationID, $qty, $inventoryID = 0, $isPerpetual = 0, $JournalEventID = 2) {
        $headers = array(
            'Authorization: ' . $this->_merchantAuthority->Token
        );

        if ($inventoryID === 0) {
            $inventory = $this->getInventory('EN', $skuID, $inventoryLocationID);
            $inventoryID = isset($inventory->Inventory->InventoryId) ? $inventory->Inventory->InventoryId : 0;
        }

        $params = array(
            'MerchantId' => $this->_merchantAuthority->MerchantId,
            'SkuId' => $skuID,
            'InventoryLocationId' => $inventoryLocationID,
            'IsPerpetual' => $isPerpetual,
            'QtyAllocated' => $qty,
            'InventoryId' => $inventoryID,
            'JournalEventId' => $JournalEventID
        );

        $response = APICaller::post($this->config->getSaveInventoryAPI(), $params, $headers);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            echo HtmlObjectPrinter::obj2Table($params);
            $result = $response;
        }
        return $result;
    }

    public function getInventoryLocationList($cultureCode) {
        $url = $this->config->getGetInventoryLocationListAPI();

        $params = array(
            'cc' => $cultureCode,
            'merchantid' => $this->_merchantAuthority->MerchantId
        );

        $headers = array(
            'Authorization: ' . $this->_merchantAuthority->Token
        );

        $response = APICaller::get($url, $params, $headers);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }

    public function getInventory($cultureCode, $skuID, $inventoryLocationID) {
        $url = $this->config->getGetInventoryAPI();

        $params = array(
            'cc' => $cultureCode,
            'skuid' => $skuID,
            'inventorylocationid' => $inventoryLocationID,
            'merchantid' => $this->_merchantAuthority->MerchantId
        );

        $headers = array(
            'Authorization: ' . $this->_merchantAuthority->Token
        );

        $response = APICaller::get($url, $params, $headers);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }

    public function getInventoryListBySku($cultureCode, $skuID) {
        $url = $this->config->getGetInventoryListBySkuAPI();

        $params = array(
            'cc' => $cultureCode,
            'skuid' => $skuID,
            'merchantid' => $this->_merchantAuthority->MerchantId
        );

        $headers = array(
            'Authorization: ' . $this->_merchantAuthority->Token
        );

        $response = APICaller::get($url, $params, $headers);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }

    public function getInventoryListByLocation($inventoryLocationID, $cultureCode
    , $size = 25, $page = 1, $predicate = NULL, $order = NULL, $hasInventory = NULL
    , $inventoryStatusId = NULL, $search = NULL) {
        $url = $this->config->getGetInventoryListByLocationAPI();

        $params = array(
            'inventorylocationid' => $inventoryLocationID,
            'cc' => $cultureCode,
            'merchantid' => $this->_merchantAuthority->MerchantId,
            'size' => $size,
            'page' => $page,
        );

        if (isset($predicate)) {
            $params['predicate'] = $predicate;
        }
        
        if (isset($order)) {
            $params['order'] = $order;
        }
        
        if (isset($hasInventory)) {
            $params['hasInventory'] = $hasInventory;
        }
        
        if (isset($inventoryStatusId)) {
            $params['inventoryStatusId'] = $inventoryStatusId;
        }
        
        if (isset($search)) {
            $params['search'] = $search;
        }

        $headers = array(
            'Authorization: ' . $this->_merchantAuthority->Token
        );

        $response = APICaller::get($url, $params, $headers);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }

}
