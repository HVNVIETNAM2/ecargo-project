<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CSVReader {

    public static function readData($filePath, $hasHeader = FALSE, $keyField = null) {

        $csv = file_get_contents($filePath);
        $rawData = array_map("str_getcsv", explode("\n", $csv));

        if ($hasHeader) {
            $headers = explode("\t", array_shift($rawData)[0]);
        }
//        $total = sizeof($skusData);

        $objs = array();

        foreach ($rawData as $data) {
            $fields = explode("\t", $data[0]);
            if ($hasHeader) {
                $obj = self::_getHeaderMappedObj($fields, $headers);
            } else {
                $obj = $fields;
            }
            
            if($keyField){
                $objs[$obj[$keyField]] = $obj;
            }
            else{
                $objs[] = $obj;    
            }
//            break;
        }

        if ($hasHeader) {
            $result = array(
                'headers' => $headers,
                'objs' => $objs
            );
        } else {
            $result = $obj;
        }

        return $result;
    }

    private static function _getHeaderMappedObj($fields, $headers) {
        $obj = array();
        for ($i = 0; $i < sizeof($headers); $i++) {
            $obj[$headers[$i]] = isset($fields[$i]) ? trim($fields[$i]) : "";
        }
        return $obj;
    }

}
