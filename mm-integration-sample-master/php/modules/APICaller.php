<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class APICaller {

    public static function post($url, $data = array(), $headers = array(), $isApplicationJson = TRUE) {
        if ($isApplicationJson) {
            $data_string = json_encode($data);
        } else {
            $data_string = $data;
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($isApplicationJson) {
            $mergedHeaders = array_merge($headers, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)));
        } else {
            $mergedHeaders = $headers;
        }

        if (isset($mergedHeaders) && sizeof($mergedHeaders) > 0) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $mergedHeaders);
        }

        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $result = array(
            'code' => $httpcode,
            'response' => json_decode($response)
        );
        return $result;
    }

    public static function get($url, $params = array(), $headers = array()) {

        $getAPI = (sizeof($params) > 0) ? ($url . '?' . http_build_query($params)) : $url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $getAPI);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $mergedHeaders = array_merge($headers, array(
            'Content-Type: application/json',
        ));

        curl_setopt($ch, CURLOPT_HTTPHEADER, $mergedHeaders);

        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $result = array(
            'code' => $httpcode,
            'response' => json_decode($response)
        );
        return $result;
    }

}
