<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once 'AuthManager.php';
include_once 'APICaller.php';
include_once 'BaseManager.php';

class ProductImageManager extends BaseManager {

    private $_merchantAuthority;

    public function __construct() {
        parent::__construct();
        $authManager = new AuthManager($this->config->getLoginAPI());

        $loginResponse = $authManager->login();
        if ($loginResponse['code'] === 200) {
            $this->_merchantAuthority = $loginResponse['response'];
        } else {
            throw new Exception('Fail to login!');
        }
    }

    public function getProductImageList($styleCode) {
        $url = $this->config->getGetProductImageListAPI();
        $params = array(
            'stylecode' => $styleCode
        );

        $headers = array(
            'Authorization: ' . $this->_merchantAuthority->Token
        );
        $params['merchantid'] = $this->_merchantAuthority->MerchantId;

        $response = APICaller::get($url, $params, $headers);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }

    public function saveProductImage($file) {

        $url = $this->config->getSaveProductImageAPI();

        $tmpfile = $file['tmp_name'];
        $filename = basename($file['name']);
        
        $params = array(
            'data' => '{"MerchantId":' . $this->_merchantAuthority->MerchantId . '}',
            'file' => curl_file_create($tmpfile, $file['type'], $filename)
//            'file' => '@'.$tmpfile.';filename='.$filename,
        );

        $headers = array(
            'Authorization: ' . $this->_merchantAuthority->Token
        );

        $response = APICaller::post($url, $params, $headers, FALSE);

        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }
    
    public function deleteProductImage($styleImageID){
        $url = $this->config->getDeleteProductImageAPI();
        $params = array(
            'StyleImageId' => $styleImageID,
            'MerchantId' => $this->_merchantAuthority->MerchantId,
        );

        $headers = array(
            'Authorization: ' . $this->_merchantAuthority->Token
        );

        $response = APICaller::post($url, $params, $headers);
        
        if ($response['code'] === 200) {
            $result = $response['response'];
        } else {
            $result = $response;
        }
        return $result;
    }

}
