<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ConfigReader {

    private $_skuDataFilePath;
    private $_styleDataFilePath;
    private $_inventoryDataFilePath;
    private $_colorDataFilePath;
    
    private $_loginAPI;
    
    private $_skuSaveAPI;
    private $_getSkuAPI;
    private $_getSkuListAPI;
    
    private $_saveProductImageAPI;
    private $_deleteProductImageAPI;
    private $_getProductImageListAPI;
    private $_saveInventoryAPI;
    
    private $_getInventoryListByLocationAPI;
    private $_getInventoryListBySkuAPI;
    private $_getInventoryLocationListAPI;
    private $_getInventoryAPI;
    private $_getCategoryListFlatAPI;
    
    private $_getBrandListAPI;
    private $_getColorListAPI;
    private $_getSizeListAPI;
    
    private $_imageResizeURL;
    

    public function __construct() {

        $config = parse_ini_file(__DIR__ . '/../config/config.ini', TRUE);
        
        $currentDir = getcwd();

        $this->_skuDataFilePath = $currentDir . $config['file']['sku'];
        $this->_styleDataFilePath = $currentDir . $config['file']['style'];
        $this->_inventoryDataFilePath = $currentDir . $config['file']['inventory'];
        $this->_colorDataFilePath = $currentDir . $config['file']['color'];
        $apiBase = $config['api']['apibase'];
        $this->_loginAPI = $apiBase . $config['api']['login'];
        
        $this->_skuSaveAPI = $apiBase . $config['api']['sku.save'];
        $this->_getSkuAPI = $apiBase . $config['api']['get.sku'];
        $this->_getSkuListAPI = $apiBase . $config['api']['get.sku.list'];
        
        $this->_saveProductImageAPI = $apiBase . $config['api']['save.product.image'];
        $this->_deleteProductImageAPI = $apiBase . $config['api']['delete.product.image'];
        $this->_getProductImageListAPI = $apiBase . $config['api']['get.product.image.list'];
        $this->_saveInventoryAPI = $apiBase . $config['api']['save.inventory'];
        
        $this->_getInventoryListByLocationAPI = $apiBase . $config['api']['get.inventory.list.by.location'];
        $this->_getInventoryListBySkuAPI = $apiBase . $config['api']['get.inventory.list.by.sku'];
        $this->_getInventoryLocationListAPI = $apiBase . $config['api']['get.inventory.location.list'];
        $this->_getInventoryAPI = $apiBase . $config['api']['get.inventory'];
        $this->_getCategoryListFlatAPI = $apiBase . $config['api']['get.category.list.flat'];
        
        $this->_getBrandListAPI = $apiBase . $config['api']['get.brand.list'];
        $this->_getColorListAPI = $apiBase . $config['api']['get.color.list'];
        $this->_getSizeListAPI = $apiBase . $config['api']['get.size.list'];
        
        $this->_imageResizeURL = $apiBase . $config['api']['image.resize.url'];
    }

    public function getSKUDataFilePath() {
        return $this->_skuDataFilePath;
    }

    public function getStyleDataFilePath() {
        return $this->_styleDataFilePath;
    }
    
    public function getInventoryDataFilePath() {
        return $this->_inventoryDataFilePath;
    }

    public function getColorDataFilePath() {
        return $this->_colorDataFilePath;
    }

    public function getLoginAPI() {
        return $this->_loginAPI;
    }
    
    public function getSkuSaveAPI() {
        return $this->_skuSaveAPI;
    }
    
    public function getGetSkuAPI() {
        return $this->_getSkuAPI;
    }
    
    public function getGetSkuListAPI() {
        return $this->_getSkuListAPI;
    }
    
    public function getSaveProductImageAPI() {
        return $this->_saveProductImageAPI;
    }
    
    public function getDeleteProductImageAPI() {
        return $this->_deleteProductImageAPI;
    }
    
    public function getGetProductImageListAPI() {
        return $this->_getProductImageListAPI;
    }
    
    public function getSaveInventoryAPI() {
        return $this->_saveInventoryAPI;
    }
    
    public function getGetInventoryListByLocationAPI() {
        return $this->_getInventoryListByLocationAPI;
    }
    
    public function getGetInventoryListBySkuAPI() {
        return $this->_getInventoryListBySkuAPI;
    }
    
    public function getGetInventoryLocationListAPI() {
        return $this->_getInventoryLocationListAPI;
    }
    
    public function getGetInventoryAPI() {
        return $this->_getInventoryAPI;
    }
    
    public function getGetCategoryListFlatAPI() {
        return $this->_getCategoryListFlatAPI;
    }
    
    public function getGetBrandListAPI() {
        return $this->_getBrandListAPI;
    }
    
    public function getGetColorListAPI() {
        return $this->_getColorListAPI;
    }
    
    public function getGetSizeListAPI() {
        return $this->_getSizeListAPI;
    }
    
    public function getImageResizeURL() {
        return $this->_imageResizeURL;
    }
    
    public function print2Table(){
        return array(
            '_skuDataFilePath'=>$this->_skuDataFilePath,
            '_styleDataFilePath'=>$this->_styleDataFilePath,
            '_inventoryDataFilePath'=>$this->_inventoryDataFilePath,
            '_colorDataFilePath'=>$this->_colorDataFilePath,
            '_loginAPI'=>$this->_loginAPI,
            '_skuSaveAPI'=>$this->_skuSaveAPI,
            '_getSkuAPI'=>$this->_getSkuAPI,
            '_getSkuListAPI'=>$this->_getSkuListAPI,
            '_saveProductImageAPI'=>$this->_saveProductImageAPI,
            '_deleteProductImageAPI'=>$this->_deleteProductImageAPI,
            '_getProductImageListAPI'=>$this->_getProductImageListAPI,
            '_saveInventoryAPI'=>$this->_saveInventoryAPI,
            '_getInventoryListByLocationAPI'=>$this->_getInventoryListByLocationAPI,
            '_getInventoryListBySkuAPI'=>$this->_getInventoryListBySkuAPI,
            '_getInventoryLocationListAPI'=>$this->_getInventoryLocationListAPI,
            '_getInventoryAPI'=>$this->_getInventoryAPI,
            '_getCategoryListFlatAPI'=>$this->_getCategoryListFlatAPI,
            '_getBrandListAPI'=>$this->_getBrandListAPI,
            '_getColorListAPI'=>$this->_getColorListAPI,
            '_getSizeListAPI'=>$this->_getSizeListAPI,
            '_imageResizeURL'=>$this->_imageResizeURL,
        );
    }
}
