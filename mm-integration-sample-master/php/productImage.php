<!DOCTYPE html>
<!--
Get Product Sample
-->
<?php

include_once 'modules/ProductImageManager.php';
include_once 'modules/HtmlObjectPrinter.php';

$styleCode = $_REQUEST['stylecode'];

$productImageManager = new ProductImageManager();

$productImages = $productImageManager->getProductImageList($styleCode);

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>mm-integration-sample-php</title>
    </head>
    <body>
        <h1>MM Integration Sample (PHP)</h1>
        <h2><?php echo $_REQUEST['title'];?></h2>
        
        <?php echo HtmlObjectPrinter::obj2Table(array('styleCode'=>$styleCode)); ?>
        <br/>
        <h3>Product Image List</h3>
        <?php if (isset($productImages)): ?>
            <?php echo HtmlObjectPrinter::obj2Table($productImages); ?>
        <?php else: ?>
            <h1 style="color:red">NOT FOUND</h1>
            <?php echo HtmlObjectPrinter::obj2Table($_REQUEST); ?>
        <?php endif; ?>
        <?php include_once 'footer.php';?>
    </body>
</html>