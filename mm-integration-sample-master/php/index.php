<!DOCTYPE html>
<!--
-->
<?php
include_once 'modules/ConfigReader.php';
include_once 'modules/HtmlObjectPrinter.php';


$config = new ConfigReader();

$demo = array(
    'Login, Save SKU, Get Color List'=>'<a href="skuImport.php?limit=10&title=Login, Save SKU, Get Color List">skuImport.php</a>',
    'Get SKU'=>'<a href="sku.php?cc=EN&skuid=518&merchantid=2&title=Get SKU">sku.php</a>',
    'Get SKU List'=>'<a href="skuList.php?cc=EN&merchantid=22&title=Get SKU List">skuList.php</a>',
    'Login, Get Product Image List'=>'<a href="productImage.php?stylecode=JC10162CS***64&title=Login, Get Product Image List">productImage.php</a>',
    'Login, Save Product Image, Delete Image of Product'=>'<a href="saveDeleteImage.php?title=Login, Save Product Image, Delete Image of Product">saveDeleteImage.php</a>',
    'Get Category List Flat'=>'<a href="categoryList.php?cc=EN&title=Get Category List Flat">categoryList.php</a>',
    'Get Brand List'=>'<a href="brandList.php?cc=EN&title=Get Brand List">brandList.php</a>',
    'Get Color List'=>'<a href="colorList.php?cc=EN&title=Get Color List">colorList.php</a>',
    'Get Size List'=>'<a href="sizeList.php?cc=EN&title=Get Size List">sizeList.php</a>',
    'Login, Get Inventory Location List'=>'<a href="inventoryLocationList.php?cc=EN&title=Get Inventory Location List">inventoryLocationList.php</a>',
    'Login, Get Inventory, Save Inventory'=>'<a href="inventoryImport.php?cc=EN&limit=20&title=Login, Get Inventory, Save Inventory">inventoryImport.php</a>',
    'Login, Get Inventory'=>'<a href="inventory.php?cc=EN&inventorylocationid=14&skuid=285157&title=Login, Get Inventory">inventory.php</a>',
    'Login, Get Inventory List by Sku'=>'<a href="inventoryListBySku.php?cc=EN&inventorylocationid=14&skuid=285157&title=Login, Get Inventory List by Sku">inventoryListBySku.php</a>',
    'Login, Get Inventory List by Inventory Location'=>'<a href="inventoryListByLocation.php?cc=EN&inventorylocationid=14&title=Login, Get Inventory List by Inventory Location">inventoryListByLocation.php</a>',
);

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>mm-integration-sample-php</title>
    </head>
    <body>
        <h1>MM Integration Sample (PHP) </h1>
        
        <h2>Integration Demo:</h2>
        <?php echo HtmlObjectPrinter::obj2Table($demo, FALSE); ?>
        
        <h2>Config Info:</h2>
        <?php echo HtmlObjectPrinter::obj2Table($config->print2Table(), FALSE); ?>
        
        <?php include_once 'footer.php';?>
    </body>
</html>