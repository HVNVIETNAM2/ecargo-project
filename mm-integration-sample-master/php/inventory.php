<!DOCTYPE html>
<!--
Juicy2MM import Sample
-->
<?php
// SKU im'port sample
// 1. read config
// 2. read csv file
// 3. mapping data (catagory, color, size, etc)
// 4. login
// 5. save sku (insert/ update)
// 6. print out result

include_once 'modules/ConfigReader.php';
include_once 'modules/AuthManager.php';
include_once 'modules/InventoryManager.php';
include_once 'modules/HtmlObjectPrinter.php';


$config = new ConfigReader();

$inventoryManager = new InventoryManager();

$cultureCode =$_REQUEST['cc'];
$skuID =$_REQUEST['skuid'];
$inventoryLocationID =$_REQUEST['inventorylocationid'];

$inventory = $inventoryManager->getInventory($cultureCode, $skuID, $inventoryLocationID)


?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>mm-integration-sample-php</title>
    </head>
    <body>
        <h1>MM Integration Sample (PHP)</h1>
        <h2><?php echo $_REQUEST['title'];?></h2>
        
        <?php echo HtmlObjectPrinter::obj2Table($inventory); ?>
        
        <?php include_once 'footer.php';?>
    </body>
</html>