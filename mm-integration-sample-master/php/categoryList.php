<!DOCTYPE html>
<!--
-->
<?php

include_once 'modules/CategoryManager.php';
include_once 'modules/HtmlObjectPrinter.php';

$categoryManager = new CategoryManager();

$cultureCode = $_REQUEST['cc'];

$categories = $categoryManager->getCategoryListFlat($cultureCode);

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>mm-integration-sample-php</title>
    </head>
    <body>
        <h1>MM Integration Sample (PHP)</h1>
        <h2><?php echo $_REQUEST['title'];?></h2>
        <?php if (isset($categories)): ?>
            <?php echo HtmlObjectPrinter::obj2Table($categories); ?>
        <?php else: ?>
            <h1 style="color:red">NOT FOUND</h1>
            <?php echo HtmlObjectPrinter::obj2Table($_REQUEST); ?>
        <?php endif; ?>
        <?php include_once 'footer.php';?>
    </body>
</html>