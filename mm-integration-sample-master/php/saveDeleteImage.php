<!DOCTYPE html>
<!--
Get Product Sample
-->
<?php
include_once 'modules/ConfigReader.php';
include_once 'modules/ProductImageManager.php';
include_once 'modules/HtmlObjectPrinter.php';

$config = new ConfigReader();
$imageResizeURL = $config->getImageResizeURL();

$productImageManager = new ProductImageManager();

if (isset($_FILES['file'])) {
    $saveImageResult = $productImageManager->saveProductImage($_FILES['file']);
}

if (isset($_REQUEST['styleImageID'])) {
    $styleImageID = $_REQUEST['styleImageID'];
    $deleteImageResult = $productImageManager->deleteProductImage($styleImageID);
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>mm-integration-sample-php</title>
    </head>
    <body>
        <h1>MM Integration Sample (PHP)</h1>
        <h2><?php echo $_REQUEST['title']; ?></h2>

        <?php if (!isset($saveImageResult)): ?>
            <h3>Save Product Image</h3>
            <p>Choose and upload image</b>
        <form action="" enctype="multipart/form-data" method="POST">
            <input name="file" type="file"/>
            <button type="submit">Save Image</button>
        </form>
    <?php endif; ?>

    <?php if (isset($saveImageResult)): ?>
        <img src="<?php echo $imageResizeURL . '?key=' . $saveImageResult->StyleImage->ProductImage; ?>&w=200&h=0&b=productimages" />

        <h3>Delete Image Sample</h3>
        <div>
            <form action="" method="POST">
                <input name="styleImageID" type="hidden" value="<?php echo $saveImageResult->StyleImage->StyleImageId; ?>"/>
                <p>Click <button type="submit">Delete Image</button> to delete image</p>
            </form>
        </div>
        <div>
            <h4>Save image result:</h4>
            <?php echo HtmlObjectPrinter::obj2Table($saveImageResult); ?>
        </div>
    <?php endif; ?>

    <?php if (isset($deleteImageResult)): ?>
        <div>
            <h4>Delete image result:</h4>
            <?php echo HtmlObjectPrinter::obj2Table($deleteImageResult); ?>
        </div>
    <?php endif; ?>

    <?php include_once 'footer.php'; ?>
</body>
</html>