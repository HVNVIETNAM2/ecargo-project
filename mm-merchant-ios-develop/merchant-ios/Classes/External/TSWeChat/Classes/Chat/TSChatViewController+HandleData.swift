//
//  TSChatViewController+HandleData.swift
//  TSWeChat
//
//  Created by Hilen on 1/29/16.
//  Copyright © 2016 Hilen. All rights reserved.
//

import Foundation

// MARK: - @extension TSChatViewController
extension TSChatViewController {
    /**
     发送文字
     */
    func chatSendText() {
        dispatch_async_safely_to_main_queue({[weak self] in
            guard let strongSelf = self else {
                return
            }
            let textView = strongSelf.chatActionBarView.inputTextView
            guard textView.text.length < 1000 else {
                TSProgressHUD.ts_showWarningWithStatus("超出字数限制")
                return
            }
            
            guard textView.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).length > 0 else{
                TSProgressHUD.ts_showWarningWithStatus("不能发送空白消息")
                return
            }
            
            let string = strongSelf.chatActionBarView.inputTextView.text
            let chat = ChatModel(text: string)
            Context.getUserKey()
            if let me = WebSocketManager.sharedInstance.currentUser {
                chat.chatSenderProfileKey = me.profileImage
            }
            
            strongSelf.itemDataSouce.append(chat)
            let insertIndexPath = NSIndexPath(forRow: strongSelf.itemDataSouce.count - 1, inSection: 0)
            strongSelf.listTableView.insertRowsAtBottom([insertIndexPath])
            textView.text = "" //发送完毕后清空
            self?.sendMessageToSocket(chat)

        })
    }
    
    
    func sendMessageToSocket(chat: ChatModel){
        if WebSocketManager.sharedInstance.isConnected {
            if chat.messageContent != nil && self.conversationObject != nil {
                WebSocketManager.sharedInstance.writeData(chat.messageContent!.dataUsingEncoding(NSUTF8StringEncoding)!)
                WebSocketManager.sharedInstance.sendMessage(chat.messageContent!, toConversation: self.conversationObject!)
                WebSocketManager.sharedInstance.writePing(NSData())
            }
        }
    }
    /**
     发送声音
     */
    func chatSendVoice(audioModel: ChatAudioModel) {
        dispatch_async_safely_to_main_queue({[weak self] in
            guard let strongSelf = self else {
                return
            }
            let model = ChatModel(audioModel: audioModel)
            strongSelf.itemDataSouce.append(model)
            let insertIndexPath = NSIndexPath(forRow: strongSelf.itemDataSouce.count - 1, inSection: 0)
            strongSelf.listTableView.insertRowsAtBottom([insertIndexPath])
        })
    }

    /**
     发送图片
     */
    func chatSendImage(imageModel: ChatImageModel) {
        dispatch_async_safely_to_main_queue({[weak self] in
            guard let strongSelf = self else {
                return
            }
            let model = ChatModel(imageModel:imageModel)
            strongSelf.itemDataSouce.append(model)
            let insertIndexPath = NSIndexPath(forRow: strongSelf.itemDataSouce.count - 1, inSection: 0)
            strongSelf.listTableView.insertRowsAtBottom([insertIndexPath])
        })
    }
}







