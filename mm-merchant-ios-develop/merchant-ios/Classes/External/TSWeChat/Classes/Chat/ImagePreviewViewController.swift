//
//  ImagePreviewViewController.swift
//  merchant-ios
//
//  Created by HVN_Pivotal on 3/18/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit
protocol ImagePreviewViewControllerDelegate{
    func didChooseImage(image : UIImage)
}
class ImagePreviewViewController: MmViewController {
    
    private var titleTextAttributes : [String: AnyObject]!
    private var navigationBGImage : UIImage!
    private var navigationShadowImage : UIImage!
    private var navigationTranslucent : Bool = false
    private var navigationBGColor : UIColor!
    var delegate: ImagePreviewViewControllerDelegate?
    var image: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor()
        self.collectionView.backgroundColor = UIColor.blackColor()
        self.createBackButton()
        self.createRightButton(String.localize("LB_OK"), action: "okButtonTapped")
        self.collectionView.registerClass(DescCollectCell.self, forCellWithReuseIdentifier: "DescCollectCell")
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.title = ""
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.backupNavigationBar()
        self.setupNavigationBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.revertNavigationBar()
    }
    
    //MARK: Collection View methods and delegates
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    //MARK: Draw Cell
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("DescCollectCell", forIndexPath: indexPath) as! DescCollectCell
        cell.descImageView.image = self.image
        return cell
    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            if image != nil {
                return CGSizeMake(self.view.frame.size.width, self.image!.size.height * self.view.frame.width / self.image!.size.width)
            } else {
                return CGSizeMake(self.view.frame.size.width,self.view.frame.size.width)
            }
    }
    //MARK: Navigation Bar methods
    func backupNavigationBar() {
        titleTextAttributes = self.navigationController!.navigationBar.titleTextAttributes
        navigationBGImage = self.navigationController!.navigationBar.backgroundImageForBarMetrics(UIBarMetrics.Default)
        navigationShadowImage = self.navigationController!.navigationBar.shadowImage
        navigationTranslucent = self.navigationController!.navigationBar.translucent
        navigationBGColor = self.navigationController!.view.backgroundColor
    }
    func revertNavigationBar() {
        self.navigationController!.navigationBar.titleTextAttributes = titleTextAttributes
        self.navigationController!.navigationBar.setBackgroundImage(navigationBGImage, forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = navigationShadowImage
        self.navigationController!.navigationBar.translucent = navigationTranslucent
        self.navigationController!.view.backgroundColor = navigationBGColor
    }
    func setupNavigationBar() {
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
        self.navigationItem.setHidesBackButton(false, animated:false);
    }
    func okButtonTapped() {
        if parentViewController != nil {
            parentViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
                if self.delegate != nil {
                    self.delegate?.didChooseImage(self.image!)
                }
            })
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
        
    }
    
    //Override back button
    override func createBackButton() {
        let buttonBack = UIButton(type: .Custom)
        buttonBack.setImage(UIImage(named: "back_wht"), forState: .Normal)
        buttonBack.frame = CGRectMake(0, 0, Constants.Value.BackButtonWidth, Constants.Value.BackButtonHeight)
        buttonBack.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: Constants.Value.BackButtonMarginLeft, bottom: 0, right: 0)
        let backButtonItem = UIBarButtonItem(customView: buttonBack)
        buttonBack.addTarget(self, action: "backButtonClicked", forControlEvents: .TouchUpInside)
        self.navigationItem.leftBarButtonItem = backButtonItem
    }
    
    //Override right bar button
    override func createRightButton(title: String, action: Selector) {
        let rightButton = UIButton(type: UIButtonType.System)
        rightButton.setTitle(title, forState: .Normal)
        rightButton.titleLabel?.formatSmall()
        rightButton.setTitleColor( UIColor.whiteColor(), forState: .Normal)
        let constraintRect = CGSize(width: CGFloat.max, height: Constants.Value.BackButtonHeight)
        let boundingBox = title.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: rightButton.titleLabel!.font], context: nil)
        rightButton.frame = CGRect(x: 0, y: 0, width: boundingBox.width, height: Constants.Value.BackButtonHeight)
        rightButton.addTarget(self, action: action, forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
}


