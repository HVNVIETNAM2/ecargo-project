//
//  TSChatBaseCell.swift
//  TSWeChat
//
//  Created by Hilen on 1/11/16.
//  Copyright © 2016 Hilen. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxBlocking
import AlamofireImage


private let kChatNicknameLabelHeight: CGFloat = 20  //昵称 label 的高度
let kChatAvatarMarginLeft: CGFloat = 10             //头像的 margin left
let kChatAvatarMarginTop: CGFloat = 0               //头像的 margin top
let kChatAvatarWidth: CGFloat = 35                  //头像的宽度

class TSChatBaseCell: UITableViewCell {
    weak var delegate: TSChatCellDelegate?

    @IBOutlet weak var avatarImageView: UIImageView! {didSet{
        avatarImageView.backgroundColor = UIColor.clearColor()
        avatarImageView.width = kChatAvatarWidth
        avatarImageView.height = kChatAvatarWidth
    }}
    @IBOutlet weak var nicknameLabel: UILabel! {didSet{
        nicknameLabel.font = UIFont.systemFontOfSize(11)
        nicknameLabel.textColor = UIColor.darkGrayColor()
        }}
    var model: ChatModel?
    let disposeBag = DisposeBag()

    override func prepareForReuse() {
        self.avatarImageView.image = nil
        self.nicknameLabel.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .None
        self.contentView.backgroundColor = UIColor.clearColor()
        
        //头像点击
        let tap = UITapGestureRecognizer()
        self.avatarImageView.addGestureRecognizer(tap)
        self.avatarImageView.userInteractionEnabled = true
        tap.rx_event.subscribeNext{[weak self ] _ in
            if let strongSelf = self {
                guard let delegate = strongSelf.delegate else {
                    return
                }
                delegate.cellDidTapedAvatarImage(strongSelf)
            }
        }.addDisposableTo(self.disposeBag)
    }
    
    func setCellContent(model: ChatModel) {
        self.model = model
        
        let filter = AspectScaledToFitSizeFilter(
            size: avatarImageView.frame.size
        )
        avatarImageView.layer.cornerRadius = avatarImageView.height/2
        if model.chatSenderProfileKey != nil {
            avatarImageView.af_setImageWithURL(ImageURLFactory.get(model.chatSenderProfileKey!, category: ImageCategory.User), placeholderImage : UIImage(named: "holder"), filter: filter)
        } else {
            avatarImageView.image = UIImage(named: "profile_avatar")
        }
        self.setNeedsLayout()
    }
    
    override func layoutSubviews() {
        guard let model = self.model else {
            return
        }
        if model.fromMe {
            self.nicknameLabel.height = 0
            self.avatarImageView.left = UIScreen.width - kChatAvatarMarginLeft - kChatAvatarWidth
        } else {
            self.nicknameLabel.height = 0
            self.avatarImageView.left = kChatAvatarMarginLeft
        }
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}




