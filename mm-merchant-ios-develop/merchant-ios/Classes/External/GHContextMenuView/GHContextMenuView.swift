//
//  GHContextMenuView.swift
//  merchant-ios
//
//  Created by Tapasya on 27/01/14.
//  Copyright (c) 2014 Tapasya. All rights reserved.
//
//  Pod by Hang Yuen on 16/11/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import UIKit

enum GHContextMenuActionType {
    // Default
    case Pan,
    // Allows tap action in order to trigger an action
    Tap
}

class GHMenuItemLocation: NSObject {
    var position: CGPoint = CGPointZero
    var angle: CGFloat = 0
}

struct GHConfig {
    struct Size {
        static let mainItem : CGFloat = 44
        static let menuItem : CGFloat = 50
        static let borderWidth : CGFloat = 2
    }
    
    struct Animation {
        static let duration = 0.2
        static let delay = 0.2 / 5
    }
    
    static let GHShowAnimationID = "GHContextMenuViewRriseAnimationID"
    static let GHDismissAnimationID = "GHContextMenuViewDismissAnimationID"
}

protocol GHContextOverlayViewDataSource  {
    
    func numberOfMenuItems() -> Int
    func imageForItemAtIndex(index : Int) -> UIImage!
    
    func shouldShowMenuAtPoint(point : CGPoint) -> Bool
    
}

protocol GHContextOverlayViewDelegate {
    
    func didSelectItemAtIndex(selectedIndex: Int, forMenuAtPoint: CGPoint)
    
}

class GHContextMenuView : UIView {
    
    private var _dataSource: GHContextOverlayViewDataSource!
    var dataSource: GHContextOverlayViewDataSource! {
        get {
            return self._dataSource
        }
        set {
            self._dataSource = newValue
            reloadData()
        }
    }
    
    var delegate: GHContextOverlayViewDelegate!
    var menuActionType: GHContextMenuActionType!
    
    
    
    // private
    var longPressRecognizer: UILongPressGestureRecognizer?
    var isShowing: Bool = false
    var isPaning: Bool = false
    var longPressLocation: CGPoint = CGPointZero
    var curretnLocation: CGPoint = CGPointZero
    var menuItems: [CALayer!] = []
    var radius: CGFloat = 0
    var arcAngle: Double = 0
    var angleBetweenItems: CGFloat = 0
    var itemLocations: [GHMenuItemLocation!] = []
    var prevIndex: Int = 0
    var itemBGHighlightedColor: CGColorRef?
    var itemBGColor: CGColorRef?
    var displayLink: CADisplayLink! = nil
    
    required init() {
        super.init(frame: UIScreen.mainScreen().bounds)
        
        self.userInteractionEnabled = true
        self.backgroundColor = UIColor.clearColor()
        self.menuActionType = GHContextMenuActionType.Pan
        displayLink = CADisplayLink(target: self, selector: "highlightMenuItemForPoint")
        displayLink.addToRunLoop(NSRunLoop.mainRunLoop(), forMode: NSDefaultRunLoopMode)
        self.arcAngle = M_PI_2
        self.radius = 90
        self.itemBGColor = UIColor.grayColor().CGColor
        self.itemBGHighlightedColor = UIColor.redColor().CGColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(frame: UIScreen.mainScreen().bounds)
        
        self.userInteractionEnabled = true
        self.backgroundColor = UIColor.clearColor()
        self.menuActionType = GHContextMenuActionType.Pan
        displayLink = CADisplayLink(target: self, selector: "highlightMenuItemForPoint")
        displayLink.addToRunLoop(NSRunLoop.mainRunLoop(), forMode: NSDefaultRunLoopMode)
        self.arcAngle = M_PI_2
        self.radius = 90
        self.itemBGColor = UIColor.grayColor().CGColor
        self.itemBGHighlightedColor = UIColor.redColor().CGColor
    }
    
    // MARK: Layer Touch Tracking
    
    func indexOfClosestMatchAtPoint(point: CGPoint) -> Int {
        var i: Int = 0
        for menuItemLayer in menuItems {
            if CGRectContainsPoint(menuItemLayer.frame, point) {
                return i
            }
            i++
        }
        return -1
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        var menuAtPoint: CGPoint = CGPointZero
        if touches.count == 1 {
            if let touch =  touches.first {
                let touchPoint: CGPoint = touch.locationInView(self)
                let menuItemIndex: Int = indexOfClosestMatchAtPoint(touchPoint)
                if menuItemIndex > -1 {
                    menuAtPoint = menuItems[menuItemIndex].position
                }
                if (prevIndex >= 0 && prevIndex != menuItemIndex) {
                    resetPreviousSelection()
                }
                self.prevIndex = menuItemIndex
            }
        }
        self.dismissWithSelectedIndexForMenuAtPoint(menuAtPoint)
        super.touchesBegan(touches , withEvent:event)
    }
    
    // MARK: LongPress handler
    
    // Split this out of the longPressDetected so that we can reuse it with touchesBegan (above)
    func dismissWithSelectedIndexForMenuAtPoint(point: CGPoint) {
//        if delegate && delegate.respondsToSelector("didSelectItemAtIndex:forMenuAtPoint:") && prevIndex >= 0 {
        if delegate != nil && prevIndex >= 0 {
            delegate!.didSelectItemAtIndex(prevIndex, forMenuAtPoint: point)
            self.prevIndex = -1
        }
        hideMenu()
    }
    
    func longPressDetected(gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizerState.Began {
            self.prevIndex = -1
            let pointInView: CGPoint = gestureRecognizer.locationInView(gestureRecognizer.view)
//            if dataSource != nil && dataSource.respondsToSelector("shouldShowMenuAtPoint:") && !dataSource.shouldShowMenuAtPoint(pointInView) {
            if dataSource != nil && dataSource?.shouldShowMenuAtPoint(pointInView) == false {
                return
            }
            UIApplication.sharedApplication().keyWindow?.addSubview(self)
            self.longPressLocation = gestureRecognizer.locationInView(self)
            self.layer.backgroundColor = UIColor(white: 0.1, alpha: 0.8).CGColor
            self.isShowing = true
            animateMenu(true)
            setNeedsDisplay()
            
        }
        
        if gestureRecognizer.state == UIGestureRecognizerState.Changed {
            if isShowing == true && menuActionType == GHContextMenuActionType.Pan {
                self.isPaning = true
                self.curretnLocation = gestureRecognizer.locationInView(self)
            }
        }
        if gestureRecognizer.state == UIGestureRecognizerState.Ended && menuActionType == GHContextMenuActionType.Pan {
            let menuAtPoint: CGPoint = convertPoint(longPressLocation, toView: gestureRecognizer.view)
            self.dismissWithSelectedIndexForMenuAtPoint(menuAtPoint)
        }
    }
    
    func showMenu() {
    }
    
    func hideMenu() {
        if isShowing == true {
            self.layer.backgroundColor = UIColor.clearColor().CGColor
            self.isShowing = false
            self.isPaning = false
            animateMenu(false)
            setNeedsDisplay()
            removeFromSuperview()
        }
    }
    
    func layerWithImage(image: UIImage) -> CALayer {
        let layer: CALayer = CALayer()
        layer.bounds = CGRectMake(0, 0, GHConfig.Size.menuItem, GHConfig.Size.menuItem)
        layer.cornerRadius = GHConfig.Size.menuItem / 2
        layer.borderColor = UIColor.whiteColor().CGColor
        layer.borderWidth = GHConfig.Size.borderWidth
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSizeMake(0, -1)
        layer.backgroundColor = itemBGColor
        let imageLayer: CALayer = CALayer()
        imageLayer.contents = image.CGImage
        imageLayer.bounds = CGRectMake(0, 0, GHConfig.Size.menuItem * 2 / 3, GHConfig.Size.menuItem * 2 / 3)
        imageLayer.position = CGPointMake(GHConfig.Size.menuItem / 2, GHConfig.Size.menuItem / 2)
        layer.addSublayer(imageLayer)
        return layer
    }
    
    // MARK: menu item layout
    
    func reloadData() {
        menuItems.removeAll()
        itemLocations.removeAll()

        if dataSource != nil {
            let count: Int = dataSource!.numberOfMenuItems()
            for var i = 0; i < count; i++ {
                let image: UIImage = dataSource!.imageForItemAtIndex(i)
                let layer: CALayer = self.layerWithImage(image)
                self.layer.addSublayer(layer)
                menuItems.append(layer)
            }
        }
    }
    
    func layoutMenuItems() {
        itemLocations.removeAll()
        let itemSize: CGSize = CGSizeMake(GHConfig.Size.menuItem, GHConfig.Size.menuItem)
        let itemRadius: CGFloat = sqrt(pow(itemSize.width, 2) + pow(itemSize.height, 2)) / 2
        self.arcAngle = Double(((itemRadius * CGFloat(menuItems.count)) / radius) * 1.5)
        let count: Int = menuItems.count
        let isFullCircle: Bool = (arcAngle == M_PI * 2)
        let divisor: Int = (isFullCircle) ? count : count - 1
        self.angleBetweenItems = CGFloat(arcAngle / Double(divisor))
        
        for var i = 0; i < menuItems.count; i++ {
            let location: GHMenuItemLocation = locationForItemAtIndex(i)
            itemLocations.append(location)
            let layer: CALayer = menuItems[i]
            layer.transform = CATransform3DIdentity
            if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
                let angle = (UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeLeft) ? M_PI_2 : -M_PI_2
                layer.transform = CATransform3DRotate(CATransform3DIdentity, CGFloat(angle), 0, 0, 1)
            }
        }
    }
    
    func locationForItemAtIndex(index: Int) -> GHMenuItemLocation {
        let itemAngle: Float = Float(itemAngleAtIndex(index))
        let itemCenter: CGPoint = CGPointMake(longPressLocation.x + CGFloat(cosf(itemAngle)) * radius, longPressLocation.y + CGFloat(sinf(itemAngle)) * radius)
        let location: GHMenuItemLocation = GHMenuItemLocation()
        location.position = itemCenter
        location.angle = CGFloat(itemAngle)
        return location
    }
    
    func itemAngleAtIndex(index: Int) -> CGFloat {
        let bearingRadians: CGFloat = angleBeweenStartinPoint(longPressLocation, endingPoint: center)
        let angle: CGFloat = bearingRadians - CGFloat(arcAngle) / 2
        var itemAngle: CGFloat = angle + (CGFloat(index) * angleBetweenItems)
        if itemAngle > CGFloat(2 * M_PI) {
            itemAngle -= CGFloat(2 * M_PI)
        }
        else {
            if itemAngle < 0 {
                itemAngle += CGFloat(2 * M_PI)
            }
        }
        return itemAngle
    }
    
    // MARK: helper methods
    
    func calculateRadius() -> CGFloat {
        let mainSize: CGSize = CGSizeMake(GHConfig.Size.mainItem, GHConfig.Size.mainItem)
        let itemSize: CGSize = CGSizeMake(GHConfig.Size.menuItem, GHConfig.Size.menuItem)
        let mainRadius: CGFloat = sqrt(pow(mainSize.width, 2) + pow(mainSize.height, 2)) / 2
        let itemRadius: CGFloat = sqrt(pow(itemSize.width, 2) + pow(itemSize.height, 2)) / 2
        let minRadius: CGFloat = (mainRadius + itemRadius)
        let maxRadius: CGFloat = ((itemRadius * CGFloat(menuItems.count)) / CGFloat(arcAngle)) * 1.5
        let radius: CGFloat = max(minRadius, maxRadius)
        return radius
    }
    
    func angleBeweenStartinPoint(startingPoint: CGPoint, endingPoint: CGPoint) -> CGFloat {
        let originPoint: CGPoint = CGPointMake(endingPoint.x - startingPoint.x, endingPoint.y - startingPoint.y)
        var bearingRadians: CGFloat = CGFloat(atan2f(Float(originPoint.y), Float(originPoint.x)))
        bearingRadians = (bearingRadians > 0.0 ? bearingRadians : (CGFloat(M_PI * 2) + bearingRadians))
        return bearingRadians
    }
    
    func validaAngle(var angle: CGFloat) -> CGFloat {
        if angle > CGFloat(2 * M_PI) {
            angle = validaAngle(angle - CGFloat(2 * M_PI))
        }
        return angle
    }
    
    // MARK: animation and selection
    
    func highlightMenuItemForPoint() {
        if isShowing && isPaning {
            let angle: CGFloat = angleBeweenStartinPoint(longPressLocation, endingPoint: curretnLocation)
            var closeToIndex: Int = -1
            for var i = 0; i < menuItems.count; i++ {
                let itemLocation: GHMenuItemLocation = itemLocations[i]
                if fabs(itemLocation.angle - angle) < angleBetweenItems / 2 {
                    closeToIndex = i
                }
            }
            
            if closeToIndex >= 0 && closeToIndex < menuItems.count {
                let itemLocation: GHMenuItemLocation = itemLocations[closeToIndex]
                let distanceFromCenter: CGFloat = sqrt(pow(curretnLocation.x - longPressLocation.x, 2) + pow(curretnLocation.y - longPressLocation.y, 2))
                let toleranceDistanceSum = radius - GHConfig.Size.mainItem / (2 * sqrt(2))
                let toleranceDistanceReduce = ((GHConfig.Size.menuItem / (2 * sqrt(2))) / 2)
                let toleranceDistance: CGFloat = toleranceDistanceSum - toleranceDistanceReduce
                
                let distanceFromItem: CGFloat = CGFloat(fabsf(Float(distanceFromCenter - (radius))) - Float(GHConfig.Size.menuItem / (2 * sqrt(2))))
                
                if fabs(distanceFromItem) < toleranceDistance {
                    let layer: CALayer = menuItems[closeToIndex]
                    layer.backgroundColor = itemBGHighlightedColor
                    let distanceFromItemBorder: CGFloat = fabs(distanceFromItem)
                    var scaleFactor: CGFloat = 1 + 0.5 * (1 - distanceFromItemBorder / toleranceDistance)
                    if scaleFactor < 1.0 {
                        scaleFactor = 1.0
                    }
                    let scaleTransForm = CATransform3DScale(CATransform3DIdentity, scaleFactor, scaleFactor, 1.0)
                    
                    let xtrans = CGFloat(cosf(Float(itemLocation.angle)))
                    let ytrans = CGFloat(sinf(Float(itemLocation.angle)))
                    
                    let transLate = CATransform3DTranslate(scaleTransForm, 10*scaleFactor*xtrans, 10*scaleFactor*ytrans, 0);
                    layer.transform = transLate;
                    
                    if self.prevIndex >= 0 && self.prevIndex != closeToIndex {
                        resetPreviousSelection()
                    }
                    
                    self.prevIndex = closeToIndex;
                }
                else {
                    if prevIndex >= 0 {
                        resetPreviousSelection()
                    }
                }
            } else {
                resetPreviousSelection()
            }
        }
    }
    
    func resetPreviousSelection() {
        if prevIndex >= 0 {
            let layer: CALayer = menuItems[prevIndex]
            let itemLocation: GHMenuItemLocation = itemLocations[prevIndex]
            layer.position = itemLocation.position
            layer.backgroundColor = itemBGColor
            layer.transform = CATransform3DIdentity
            self.prevIndex = -1
        }
    }
    
    func animateMenu(isShowing: Bool) {
        if isShowing {
            layoutMenuItems()
        }
        for var index = 0; index < menuItems.count; index++ {
            let layer: CALayer = menuItems[index]
            layer.opacity = 0
            let fromPosition: CGPoint = longPressLocation
            let location: GHMenuItemLocation = itemLocations[index]
            let toPosition: CGPoint = location.position
            let delayInSeconds: Double = Double(index) * GHConfig.Animation.delay
            
            var positionAnimation: CABasicAnimation
            positionAnimation = CABasicAnimation(keyPath: "position")
            positionAnimation.fromValue = NSValue(CGPoint: isShowing ? fromPosition : toPosition)
            positionAnimation.toValue = NSValue(CGPoint: isShowing ? toPosition : fromPosition)
            positionAnimation.timingFunction = CAMediaTimingFunction(controlPoints: 0.45, 1.2, 0.75, 1.0)

            positionAnimation.duration = GHConfig.Animation.duration
            positionAnimation.beginTime = layer.convertTime(CACurrentMediaTime(), fromLayer: nil) + delayInSeconds
            positionAnimation.setValue(NSNumber(integer: index), forKey: (isShowing ? GHConfig.GHShowAnimationID : GHConfig.GHDismissAnimationID))
            positionAnimation.delegate = self
            layer.addAnimation(positionAnimation, forKey: "riseAnimation")
        }
    }
    
    override func animationDidStart(anim: CAAnimation) {
        if anim.valueForKey(GHConfig.GHShowAnimationID) != nil {
            if let inum : NSNumber = anim.valueForKey(GHConfig.GHShowAnimationID) as? NSNumber {
                let index: Int = inum.integerValue
                let layer: CALayer = menuItems[index]
                let location: GHMenuItemLocation = itemLocations[index]
                layer.position = location.position
                layer.opacity = Float(1.0)
            }
        }
        else if anim.valueForKey(GHConfig.GHDismissAnimationID) != nil {
            if let inum : NSNumber = anim.valueForKey(GHConfig.GHDismissAnimationID) as? NSNumber {
                let index: Int = inum.integerValue
                let layer: CALayer = menuItems[index]
                let toPosition: CGPoint = longPressLocation
                CATransaction.begin()
                CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
                layer.position = toPosition
                layer.backgroundColor = itemBGColor
                layer.opacity = 0.0
                layer.transform = CATransform3DIdentity
                CATransaction.commit()
            }
        }
    }
    
    func drawCircle(locationOfTouch: CGPoint) {
        let ctx: CGContextRef = UIGraphicsGetCurrentContext()!
        CGContextSaveGState(ctx)
        CGContextSetLineWidth(ctx, GHConfig.Size.borderWidth / 2)
        CGContextSetRGBStrokeColor(ctx, 0.8, 0.8, 0.8, 1.0)
        CGContextAddArc(ctx, locationOfTouch.x, locationOfTouch.y, GHConfig.Size.menuItem / 2.0, 0.0, CGFloat(M_PI * 2.0), 1)
        CGContextStrokePath(ctx)
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        if isShowing {
            drawCircle(longPressLocation)
        }
    }
    
}