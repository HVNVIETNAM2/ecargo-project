//
//  Log.swift
//  merchant-ios
//
//  Created by Alan YU on 18/12/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import XCGLogger

class Log {
    
    static private let logger: XCGLogger = {
        
        let logger = XCGLogger()
        
        #if DEBUG
            let logLevel = XCGLogger.LogLevel.Verbose
        #else
            let logLevel = XCGLogger.LogLevel.Severe
        #endif
        
        logger.setup(
            logLevel,
            showLogIdentifier: false,
            showFunctionName: false,
            showThreadName: false,
            showLogLevel: true,
            showFileNames: true,
            showLineNumbers: true,
            showDate: true,
            writeToFile: nil,
            fileLogLevel: .None
        )
        
        return logger
        
    } ()
    
    class func logObject(logLevel: XCGLogger.LogLevel, object: Any?, functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) {
        self.logger.logln(
            logLevel,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber,
            closure: {
                return object.debugDescription
            }
        )
    }
    
    // debug
    class func debug(object: Any?, functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) {
        self.logObject(
            .Debug,
            object: object,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber
        )
    }
    
    /**
     Log.debug { return "\(1 + 2)" }
     */
    class func debug(functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__, @noescape closure: () -> String?) {
        self.logger.logln(
            .Debug,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber,
            closure: closure
        )
        
    }
    
    // info
    class func info(object: AnyObject, functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) {
        self.logObject(
            .Info,
            object: object,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber
        )
    }
    
    /**
     Log.debug { return "\(1 + 2)" }
     */
    class func info(functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__, @noescape closure: () -> String?) {
        self.logger.logln(
            .Info,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber,
            closure: closure
        )
        
    }
    
    // verbose
    class func verbose(object: AnyObject, functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) {
        self.logObject(
            .Verbose,
            object: object,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber
        )
    }
    
    /**
     Log.debug { return "\(1 + 2)" }
     */
    class func verbose(functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__, @noescape closure: () -> String?) {
        self.logger.logln(
            .Verbose,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber,
            closure: closure
        )
    }
    
    // warning
    class func warning(object: AnyObject, functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) {
        self.logObject(
            .Warning,
            object: object,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber
        )
    }
    
    /**
     Log.debug { return "\(1 + 2)" }
     */
    class func warning(functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__, @noescape closure: () -> String?) {
        self.logger.logln(
            .Warning,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber,
            closure: closure
        )
        
    }
    
    // error
    class func error(object: AnyObject, functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__) {
        self.logObject(
            .Error,
            object: object,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber
        )
    }
    
    /**
     Log.debug { return "\(1 + 2)" }
     */
    class func error(functionName: String = __FUNCTION__, fileName: String = __FILE__, lineNumber: Int = __LINE__, @noescape closure: () -> String?) {
        self.logger.logln(
            .Error,
            functionName: functionName,
            fileName: fileName,
            lineNumber: lineNumber,
            closure: closure
        )
        
    }
    
}