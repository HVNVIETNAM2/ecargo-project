//
//  UIViewExtension.swift
//  merchant-ios
//
//  Created by Alan YU on 6/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

extension UIView {
    
    class func viewWithScreenSize() -> Self {
        return self.init(frame: UIScreen.mainScreen().bounds)
    }
    
    func imageValue() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.mainScreen().scale);
        
        self.drawViewHierarchyInRect(self.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image;
    }
    
    func round() {
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.frame.size.height / 2
        self.clipsToBounds = true
    }
    
    func viewBorder(color: UIColor = UIColor.blackColor(), width: CGFloat = 1) {
        self.layer.borderColor = color.CGColor
        self.layer.borderWidth = width
    }

    
}