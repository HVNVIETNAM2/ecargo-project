//
//  UIViewControllerExtension.swift
//  merchant-ios
//
//  Created by Hang Yuen on 5/11/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import UIAlertController_Blocks

extension UIViewController {
    
    func showSuccAlert(message: String) {
        UIAlertController.showAlertInViewController(self, withTitle: "", message: message, cancelButtonTitle: String.localize("LB_OK"), destructiveButtonTitle: nil, otherButtonTitles: nil) { (controller , action, buttonIndex) -> Void in
        }
    }
    
    func showErrorAlert(message: String) {
        UIAlertController.showAlertInViewController(self, withTitle: "", message: message, cancelButtonTitle: String.localize("LB_OK"), destructiveButtonTitle: nil, otherButtonTitles: nil) { (controller , action, buttonIndex) -> Void in
        }
    }
    
    func handleApiResponseError(response :  (Response<AnyObject, NSError>), reject : ((ErrorType) -> Void)? = nil) {
        if let resp = Mapper<ApiResponse>().map(response.result.value){
            if let appCode = resp.appCode {
                let skipErrors = ["MSG_ERR_WISHLIST_NOT_FOUND",
                "MSG_ERR_CART_NOT_FOUND",
                "MSG_ERR_USER_ADDRESS_EMPTY"]
                if !skipErrors.contains(appCode) {
                    let msg = String.localize(appCode)
                    self.showErrorAlert(msg)
                    Log.debug(String(data: response.data!, encoding: 4))
                    if let reject = reject {
                        reject(NSError(domain: "", code: response.response!.statusCode, userInfo: ["Error" : (String.localize(resp.appCode))]))
                    }
                }
            } else {
                self.showErrorAlert(String.localize("LB_ERROR"))
            }
        }
    }
    
    func handleLoginApiResponseError(JSON : AnyObject?) {
        if let resp = Mapper<ApiResponse>().map(JSON){
        if let appCode = resp.appCode  {
            var msg: String! = String.localize(appCode)
            if let range : Range<String.Index> = msg.rangeOfString("{0}") {
                if let loginAttempts = resp.loginAttempts {
                msg = msg.stringByReplacingCharactersInRange(range, withString: "\(Constants.Value.MaxLoginAttempts - loginAttempts)")
                }
            }
            self.showErrorAlert(msg)
        } else {
            self.showErrorAlert(String.localize("LB_ERROR"))
        }
        }
    }
    
}