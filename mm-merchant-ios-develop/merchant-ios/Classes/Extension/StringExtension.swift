//
//  StringExtension.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 29/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

extension String {
    
    mutating func clean() {
        if (self.characters.first == "+"){
            self = self.stringByReplacingOccurrencesOfString(" ", withString: "")
            self = self.stringByReplacingOccurrencesOfString("-", withString: "")
        }
    }
    
    static func localize(key : String) -> String{
        return NSBundle.mainBundle().localizedStringForKey(key, value: nil, table: Context.getCc().lowercaseString)
    }
 
    private func regCheck(regEx: String) -> Bool {
        let regTest = NSPredicate(format:"SELF MATCHES %@", regEx)
        return regTest.evaluateWithObject(self)
    }
    
    // return true if it is a valid email / phone number
    func isValidMMUsername() -> Bool {
        let isEmail = self.isValidEmail()
        let isPhone = self.isValidPhone()
        return isEmail || isPhone
    }
    
    func isValidEmail() -> Bool {
        let regEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
//        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        return regCheck(regEx)
    }

    func isValidPhone() -> Bool {
//        let regEx = "\\d{8}$"//"^[+0-9]{0,1}+[0-9]"
//        let regEx = "^[0-9+]+-[0-9]*$"
        let regEx = "\\++[0-9 -]*"   // for input must start at "+" and allow number, '-' and ' '
        return regCheck(regEx)
    }
    
    func isNumberic() -> Bool {
        let regEx = "^[0-9]*$"
        return regCheck(regEx)
    }
    
    func isValidPassword() -> Bool {
        let regEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~!@#$%^&*()_+|}{:\"?><.]).{8,}$"
        return regCheck(regEx)
    }
    
    var length : Int {
        get { return self.characters.count }
    }
    
    
    static func random(length: Int = 20) -> String {
        
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.characters.count))
            randomString += "\(base[base.startIndex.advancedBy(Int(randomValue))])"
        }
        
        return randomString
    }
    
}