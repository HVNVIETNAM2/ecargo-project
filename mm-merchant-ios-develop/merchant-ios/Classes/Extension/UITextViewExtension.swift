//
//  UITextViewExtension.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 4/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

extension UITextView{
    func format(){
        self.layer.borderColor = UIColor.secondary1().CGColor
        self.layer.borderWidth = Constants.TextField.BorderWidth
//        let paddingView = UIView(frame: CGRectMake(0, 0, Constants.TextField.LeftPaddingWidth, self.frame.height))
//        self.leftView = paddingView
//        self.leftViewMode = UITextFieldViewMode.Always
    }
}
