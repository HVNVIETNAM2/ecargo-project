//
//  Color.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 14/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

extension UIColor{
    //key theme color
    class func primary1() -> UIColor {
        return UIColor(hexString: "#ed2247")
    }
    //background
    class func primary2() -> UIColor {
        return UIColor(hexString: "#f1f1f1")
    }
    //border
    class func secondary1() -> UIColor {
        return UIColor(hexString: "#d8d8d8")
    }                                                                        
    //text
    class func secondary2() -> UIColor {
        return UIColor(hexString: "#4a4a4a")
    }
    
    //transparent text
    class func secondary3() -> UIColor {
        return UIColor(hexString: "#9B9B9B")
    }
    
    class func backgroundGray() -> UIColor {
        return UIColor(hexString: "#ECECEC")
    }
    
    class func sparkingRed() -> UIColor{
        return UIColor(hexString: "FF518B")
    }
    
    //Checkout Confirmation Page
    class func blackTitleColor() -> UIColor{
        return UIColor(hexString: "292929")
    }
    
}
