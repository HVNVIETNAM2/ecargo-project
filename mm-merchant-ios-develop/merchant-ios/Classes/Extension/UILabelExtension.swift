//
//  UILabelExtension.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 15/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectiveC

var UILabelAssociatedObjectHandle: UInt8 = 0

extension UILabel {
    
    var escapeFontSubstitution: Bool {
        get {
            return objc_getAssociatedObject(self, &UILabelAssociatedObjectHandle) as? Bool ?? false
        }
        set {
            objc_setAssociatedObject(self, &UILabelAssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var substituteAllFontName : String {
        get {
            return self.font.fontName
        }
        
        set {
            if self.escapeFontSubstitution == false && self.font.fontName.lowercaseString.rangeOfString("bold") == nil {
                self.font = UIFont(name: newValue, size: self.font.pointSize)
            }
        }
    }
    
    var substituteAllFontNameBold : String {
        get {
            return self.font.fontName
        }
        set {
            if self.escapeFontSubstitution == false && self.font.fontName.lowercaseString.rangeOfString("bold") != nil {
                self.font = UIFont(name: newValue, size: self.font.pointSize)
            }
        }
    }
    
    func format(){
        self.textColor = UIColor.secondary2()
        self.lineBreakMode = .ByWordWrapping
        self.numberOfLines = 0
    }
    
    func formatSize(size : Int){
        self.format()
        self.font = UIFont(name: self.font.fontName, size: CGFloat(size))
        
    }
    func formatSizeBold(size : Int){
        self.format()
        self.font = UIFont.boldSystemFontOfSize(CGFloat(size))
    }
    func formatSmall(){
        self.formatSize(14)
    }
    
    func formatSingleLine(fontSize: Int = 0) {
        self.textColor = UIColor.secondary2()
        self.adjustsFontSizeToFitWidth = true
        if fontSize != 0 {
            self.font = UIFont(name: self.font.fontName, size: CGFloat(fontSize))
        }
    }
        
}