//
//  TextField.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 15/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

extension UITextField{
    func format(){
        self.layer.borderColor = UIColor.secondary1().CGColor
        self.layer.borderWidth = Constants.TextField.BorderWidth
        let paddingView = UIView(frame: CGRectMake(0, 0, Constants.TextField.LeftPaddingWidth, self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = UITextFieldViewMode.Always
        self.font = UIFont(name: self.font!.fontName, size: CGFloat(14))
        self.textColor = UIColor.secondary2()

    }
    
    func formatTransparent(){
        format()
        self.layer.borderWidth = 0
    }
}
