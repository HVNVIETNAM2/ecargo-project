//
//  Loading.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import UIKit

private var loadingViewAssociationKey: UInt8 = 0

extension UIViewController{
    
    private struct AssociatedKeys {
        static var loadingView:UIView?
    }
    
    var loadingView: UIView! {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.loadingView) as? UIView
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.loadingView, newValue as UIView, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    func showLoading() {
        if self.loadingView == nil {
            let v = UIView(frame: (self.view.bounds))
            v.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            
            let aiv: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
            aiv.hidesWhenStopped = true
            aiv.center = v.center
            aiv.startAnimating()

            v.addSubview(aiv)

            self.loadingView = v
        }
        self.view.addSubview(self.loadingView)
    }

    func stopLoading() {
        self.loadingView?.removeFromSuperview()
    }
}
