//
//  WebSocketExtension.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 4/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import Starscream
import ObjectMapper

extension WebSocket{
    func writeMessage(socketMessage : SocketMessage){
        writeString(Mapper().toJSONString(socketMessage, prettyPrint : false)!)
    }
    
}