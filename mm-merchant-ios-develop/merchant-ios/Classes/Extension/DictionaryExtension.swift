//
//  DictionaryExtension.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 2/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

extension Dictionary {
    public mutating func merge<K, V>(dict: [K: V]){
        for (k, v) in dict {
            self[k as! Key] = v as? Value
        }
    }
}
