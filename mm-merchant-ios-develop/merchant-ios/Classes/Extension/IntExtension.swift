//
//  IntExtension.swift
//  merchant-ios
//
//  Created by Alan YU on 9/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

extension Int {
    
    static var formatter: NSNumberFormatter = {
        let formatter = NSNumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "zh_Hans_CN")
        formatter.usesGroupingSeparator = true
        formatter.groupingSize = 3
        return formatter
    } ()
    
    func formatQuantity() -> String? {
        Int.formatter.numberStyle = .NoStyle
        Int.formatter.minimumFractionDigits = 0
        Int.formatter.maximumFractionDigits = 0
        return Int.formatter.stringFromNumber(self)
    }
    
    func formatPrice() -> String? {
        Int.formatter.numberStyle = .CurrencyStyle
        Int.formatter.minimumFractionDigits = 0
        Int.formatter.maximumFractionDigits = 0
        return Int.formatter.stringFromNumber(self)
    }
}