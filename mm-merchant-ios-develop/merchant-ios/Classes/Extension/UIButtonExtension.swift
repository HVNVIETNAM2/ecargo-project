//
//  Button.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 15/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import UIKit

var ActionBlockKey: UInt8 = 0

typealias UIButtonActionClosure = (sender: UIButton) -> Void

class UIButtonClosureWrapper {
    var closure: UIButtonActionClosure?
    init(_ closure: UIButtonActionClosure?) {
        self.closure = closure
    }
}

extension UIButton {
    
    var touchUpClosure: UIButtonActionClosure? {
        get {
            if let closure = objc_getAssociatedObject(self, &ActionBlockKey) as? UIButtonClosureWrapper {
                return closure.closure
            }
            return nil
        }
        set {
            self.addTarget(self, action: "UIButtonActionHandler:", forControlEvents: .TouchUpInside)
            objc_setAssociatedObject(
                self,
                &ActionBlockKey,
                UIButtonClosureWrapper(newValue),
                objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC
            )
        }
    }
    
    func UIButtonActionHandler(button: UIButton) {
        if let callback = self.touchUpClosure {
            callback(sender: self)
        }
    }
    
    func formatPrimary(){
        self.layer.cornerRadius = Constants.Button.Radius
        self.layer.backgroundColor = UIColor.primary1().CGColor
        self.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.titleLabel!.font = UIFont(name: self.titleLabel!.font.fontName, size: CGFloat(14))!
    }
    
    func formatSecondary(){
        self.layer.borderWidth = Constants.Button.BorderWidth
        self.layer.borderColor = UIColor.secondary1().CGColor
        self.layer.cornerRadius = Constants.Button.Radius
        self.layer.backgroundColor = UIColor.whiteColor().CGColor
        self.setTitleColor(UIColor.secondary2(), forState: .Normal)
        self.titleLabel!.font = UIFont(name: self.titleLabel!.font.fontName, size: CGFloat(14))!
    }
    func formatSecondaryNonBorder(){
        self.layer.backgroundColor = UIColor.whiteColor().CGColor
        self.setTitleColor(UIColor.secondary2(), forState: .Normal)
        self.titleLabel!.font = UIFont(name: self.titleLabel!.font.fontName, size: CGFloat(14))!
    }
    func formatTransparent(){
        self.layer.borderWidth = Constants.Button.BorderWidth
        self.layer.borderColor = UIColor.secondary3().CGColor
        self.layer.cornerRadius = Constants.Button.Radius
        self.layer.backgroundColor = UIColor.clearColor().CGColor
        self.setTitleColor(UIColor.secondary3(), forState: .Normal)
        self.titleLabel!.font = UIFont(name: self.titleLabel!.font.fontName, size: CGFloat(14))!
    }
    
    func formatWhite(){
        self.layer.backgroundColor = UIColor.whiteColor().CGColor
        self.setTitleColor(UIColor.primary1(), forState: .Normal)
        self.titleLabel!.font = UIFont(name: self.titleLabel!.font.fontName, size: CGFloat(14))!
    }
    
    func config(normalImage normalImage: UIImage?, selectedImage: UIImage?) {
        self.setImage(normalImage, forState: .Normal)
        self.setImage(normalImage, forState:  [.Normal, .Highlighted])
        self.setImage(selectedImage, forState: .Selected)
        self.setImage(selectedImage, forState: [.Selected, .Highlighted])
    }
    
    func config(normalBackgroundImage normalBackgroundImage: UIImage?, selectedBackgroundImage: UIImage?) {
        self.setBackgroundImage(normalBackgroundImage, forState: .Normal)
        self.setBackgroundImage(normalBackgroundImage, forState: [.Normal, .Highlighted])
        self.setBackgroundImage(selectedBackgroundImage, forState: .Selected)
        self.setBackgroundImage(selectedBackgroundImage, forState: [.Selected, .Highlighted])
    }
    
}