//
//  RequestExtension.swift
//  merchant-ios
//
//  Created by Hang Yuen on 3/11/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

extension Request {
    
//    public func exResponseJSON(controller : UIViewController, completionHandler: Response<AnyObject, NSError> -> Void)
//        -> Self
//    {
//        let req = response(
//            responseSerializer: Request.JSONResponseSerializer(options: NSJSONReadingOptions.AllowFragments),
//            completionHandler: { response in
//                if response.result.isSuccess {
//                    if response.response!.statusCode == 401 {
//                        // re-generate token ? show Login page
//                        // present Login
//                    }
//                } else {
//                    
//                }
//                completionHandler(response)
//            }
//        )
//        return req
//    }
    
    public func exResponseJSON(
        options options: NSJSONReadingOptions = .AllowFragments,
        completionHandler: Response<AnyObject, NSError> -> Void)
        -> Self
    {
        let req = response(
            responseSerializer: Request.JSONResponseSerializer(options: options),
            completionHandler: {[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response!.statusCode == 401 {
                            // re-generate token ? show Login page
                            // present Login
                            if let resp : ApiResponse? = Mapper<ApiResponse>().map(response.result.value) {
                                let msg : String? = String.localize((resp?.appCode)!)
                                #if MERCHANTAPP
                                    UIAlertController.showAlertInViewController(strongSelf.getTopViewController()!, withTitle: "", message: msg, cancelButtonTitle: String.localize("LB_OK"), destructiveButtonTitle: nil, otherButtonTitles: nil, tapBlock: { (controller, action, buttonIndex) -> Void in
                                        SettingViewController.logout()
                                    })
                                #endif
                            } else {
                                #if MERCHANTAPP
                                    // if can't parse the response, logout the app
                                    SettingViewController.logout()
                                #endif
                            }
                        } else if response.response!.statusCode != 200 {
                            strongSelf.getTopViewController()?.handleApiResponseError(response)
//                            if let resp : ApiResponse? = Mapper<ApiResponse>().map(response.result.value) {
//                                let msg : String? = String.localize((resp?.appCode)!)
//                                strongSelf.showApiErrorPrompt(msg)
//                            }
                            completionHandler(response)
                        } else {
                            completionHandler(response)
                        }
                    } else {
                        strongSelf.showApiErrorPrompt(String.localize("MSG_ERR_NETWORK_FAIL"))
                        completionHandler(response)
                    }
                }
            }
        )
        return req
    }
    
    private func showApiErrorPrompt(msg: String?) {
        if let vc :UIViewController = self.getTopViewController()! as UIViewController {
            UIAlertController.showAlertInViewController(vc,
                withTitle: "", message: msg, cancelButtonTitle: String.localize("LB_OK"),
                destructiveButtonTitle: nil, otherButtonTitles: nil,
                tapBlock: { (controller, action, buttonIndex) -> Void in
            })
        }
    }
    
    private func getTopViewController() -> UIViewController? {
        if let adelegate : AppDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            if let nvc: UINavigationController = adelegate.window?.rootViewController as? UINavigationController {
                return nvc.topViewController
            } else {
                return adelegate.window?.rootViewController as UIViewController?
            }
        }
        return nil
    }
}
