//
//  IDCardService.swift
//  merchant-ios
//
//  Created by Kam on 11/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import Alamofire

class IDCardService {
    static let IDCARD_PATH = Constants.Path.Host + "/identification"
    
    class func uploadIDCardInfo(orderKey : String, firstName : String, lastName : String, idNumber : String, frontImage : NSData, backImage : NSData, success : Response<AnyObject, NSError> -> Void, fail : ErrorType -> Void) {
        let url = IDCARD_PATH + "/save"
        let parameters: [String : AnyObject] = ["UserKey" : Context.getUserKey(),"OrderKey" : orderKey, "FirstName" : firstName, "LastName": lastName, "IdentificationNumber": idNumber]
        
        Alamofire.upload(.POST, url, headers: Context.getTokenHeader(), multipartFormData: {
            multipartFormData in
                multipartFormData.appendBodyPart(data: frontImage, name: "FrontImage", fileName: "frontImage.jpg", mimeType: "image/jpg")
                multipartFormData.appendBodyPart(data: frontImage, name: "BackImage", fileName: "backImage.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
                }
            }, encodingCompletion: {
                encodingResult in
                
                switch encodingResult {
                case .Success(let upload, _, _):
                    Log.debug("Success")
                    
                    upload.responseJSON { response in
                        Log.debug(response.request)  // original URL request
                        Log.debug(response.response) // URL response
                        Log.debug(response.data)     // server data
                        Log.debug(response.result)   // result of response serialization
                        
                        if let JSON = response.result.value {
                            Log.debug("JSON: \(JSON)")
                        }
                        
                        success(response)
                    }
                    
                case .Failure(let encodingError):
                    Log.debug(encodingError)
                    
                    fail(encodingError)
                }
        })
    }
}

