//
//  CountryService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 16/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire


class CountryService {
    enum Op: Int {
        
        case GetAllCountries
        
    }
    class func request(op : Op, completion : Response<AnyObject, NSError> -> Void) -> Request{
        
        var method : Alamofire.Method
        var url : URLStringConvertible
        
        switch (op){
        case.GetAllCountries:
            method = .GET
            url = Constants.Path.CountryHost
            break
        }
        
        let request = Alamofire.request(method, url)
        request.responseJSON{response in completion(response)}
        return request
    }
    
    class func list(completion : Response<AnyObject, NSError> -> Void) -> Request {
        let url = Constants.Path.Host + "/reference/general"
        let request = RequestFactory.get(url, parameters : nil)
        request.exResponseJSON{response in completion(response)}
        return request
    }
}