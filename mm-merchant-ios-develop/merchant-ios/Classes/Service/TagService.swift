//
//  TagService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 1/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class TagService {
    static let TAG_PATH = Constants.Path.Host + "/tags"
    class func list(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = TAG_PATH + "/list"
        let parameters = ["tagtypeid" : 1]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func save(tags : [Tag], completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = TAG_PATH + "/user/save"
        var tagString = ""
        for tag : Tag in tags {
            tagString = tagString + String(tag.tagId) + ","
        }
        tagString = String(tagString.characters.dropLast())
        let parameters : [String : AnyObject] = ["TagIds" : tagString, "replace": true]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func listCurator(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = TAG_PATH + "/user/curator"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func listMerchant(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = TAG_PATH + "/user/merchant"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }

}