//
//  MerchantService.swift
//  merchant-ios
//
//  Created by Vu Dinh Trung on 3/16/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit
import Alamofire

class MerchantService {
    
    static let USER_PATH = Constants.Path.Host + "/Merchant"
    
    class func view(id : Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = Constants.Path.Host + "/view/Merchant?MerchantId=\(id)"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
        
    }
    
}
