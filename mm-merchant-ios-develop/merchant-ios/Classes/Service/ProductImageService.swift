//
//  PhotoService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 25/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage



class ProductImageService {
    
    class func view(key : String, completion : Response<Image, NSError> -> Void) -> Request{
        return self.view(key, size: 100, completion: completion)
    }
    
    class func view(key : String, size : Int, completion : Response<Image, NSError> -> Void) -> Request{
        let url = Constants.Path.Host + "/resizer/view"
        let parameters : [String:AnyObject] = ["key" : key, "w" : String(size), "b" : "productimages"]

        let request = RequestFactory.get(url, parameters : parameters)
        request.responseImage{response in completion(response)}
        return request
    }
    
}
  