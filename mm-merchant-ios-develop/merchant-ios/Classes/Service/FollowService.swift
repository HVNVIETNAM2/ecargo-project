//
//  FollowService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 16/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class FollowService {
    static let FOLLOW_PATH = Constants.Path.Host + "/follow"
    
    class func saveCurator(curators : [User], completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = FOLLOW_PATH + "/user/save"
        var curatorString = ""
        for curator : User in curators {
            curatorString = curatorString + String(curator.userKey) + ","
        }
        curatorString = String(curatorString.characters.dropLast())
        let parameters : [String : AnyObject] = ["ToUserKeys" : curatorString]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func saveMerchant(merchants : [Merchant], completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = FOLLOW_PATH + "/merchant/save"
        var merchantString = ""
        for merchant : Merchant in merchants {
            merchantString = merchantString + String(merchant.merchantId) + ","
        }
        merchantString = String(merchantString.characters.dropLast())
        let parameters : [String : AnyObject] = ["ToMerchantIds" : merchantString]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func deleteMerchant(merchant : Merchant, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = FOLLOW_PATH + "/merchant/delete"
        let merchantBody = MerchantFollowBody()
        merchantBody.userKey = Context.getUserKey()
        merchantBody.toMerchantId = merchant.merchantId
        return postMerchant(merchantBody, url: url, completion: completion)
    }
    
    class func followMerchant(merchant : Merchant, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = FOLLOW_PATH + "/merchant/save"
        let merchantBody = MerchantFollowBody()
        merchantBody.userKey = Context.getUserKey()
        merchantBody.toMerchantId = merchant.merchantId
        return postMerchant(merchantBody, url: url, completion: completion)
    }
    
    class func listFollowBrand(completion : Response<AnyObject, NSError> -> Void) -> Request {
        let url = FOLLOW_PATH + "/brand/followed"
        let parameters : [String:AnyObject] = ["UserKey" : Context.getUserKey()]
        let request = RequestFactory.get(url, parameters: parameters, appendUserKey: false)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func listFollowing(completion : Response<AnyObject, NSError> -> Void) -> Request {
        let url = FOLLOW_PATH + "/user/following"
        let parameters : [String:AnyObject] = ["UserKey" : Context.getUserKey()]
        let request = RequestFactory.get(url, parameters: parameters, appendUserKey: false)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    class func listFollowUser(completion : Response<AnyObject, NSError> -> Void) -> Request {
        let url = FOLLOW_PATH + "/user/followed"
        let parameters : [String:AnyObject] = ["UserKey" : Context.getUserKey()]
        let request = RequestFactory.get(url,parameters: parameters, appendUserKey: false)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    class func listFollowMerchant(start: Int, limit: Int, completion : Response<AnyObject, NSError> -> Void) -> Request {
        let url = FOLLOW_PATH + "/merchant/followed"
        let parameter: [String:AnyObject] = ["UserKey": Context.getUserKey(), "start": start, "limit": limit]
        let request = RequestFactory.get(url, parameters: parameter, appendUserKey: false)
        request.exResponseJSON {response in completion(response)}
        return request
    }
    class func listFollowMerchantWithUserkey(start: Int, limit: Int, userKey: String, completion : Response<AnyObject, NSError> -> Void) -> Request {
        let url = FOLLOW_PATH + "/merchant/followed"
        let parameter: [String:AnyObject] = ["UserKey": userKey, "start": start, "limit": limit]
        let request = RequestFactory.get(url, parameters: parameter, appendUserKey: false)
        request.exResponseJSON {response in completion(response)}
        return request
    }
    class func postMerchant(user : MerchantFollowBody, url : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        // create the request & response
        let request = NSMutableURLRequest(URL: NSURL(string: url)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
        // create some JSON data and configure the request
        let jsonString = Mapper().toJSONString(user, prettyPrint: true)
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        Log.debug(request.HTTPBody)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(NSUserDefaults.standardUserDefaults().objectForKey("token") as? String, forHTTPHeaderField: "Authorization")
        let alamofireRequest = Alamofire.request(request)
        alamofireRequest.exResponseJSON{response in completion(response)}
        return alamofireRequest
    }
    
    class func followUser(user : User, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = FOLLOW_PATH + "/user/save"
        let parameters : [String : AnyObject] = ["ToUserKey" : user.userKey]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func unfollowUser(user : User, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = FOLLOW_PATH + "/user/delete"
        let parameters : [String : AnyObject] = ["ToUserKey" : user.userKey]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
	class func listFollowers(start: Int, limit: Int, completion : Response<AnyObject, NSError> -> Void) -> Request {
		let url = FOLLOW_PATH + "/user/following"
		let parameter: [String:AnyObject] = ["UserKey": Context.getUserKey(), "start": start, "limit": limit]
		let request = RequestFactory.get(url, parameters: parameter, appendUserKey: false)
		request.exResponseJSON {response in completion(response)}
		return request
	}


}