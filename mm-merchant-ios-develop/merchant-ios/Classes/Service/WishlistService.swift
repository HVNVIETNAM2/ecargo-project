//
//  WishlistService.swift
//  merchant-ios
//
//  Created by HungPM on 1/22/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import Alamofire

class WishlistService {
    static let WISHLIST_PATH = Constants.Path.Host + "/wishlist"
    
    class func createWishlist(completion complete: Response<AnyObject, NSError> -> Void) -> Request{
        let url = WISHLIST_PATH + "/create"
        let request = RequestFactory.post(url)
        request.exResponseJSON{response in complete(response)}
        return request
    }
    
    class func addWishlistItem(merchantId : Int, styleCode : String, skuId : Int, colorKey : String?, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = WISHLIST_PATH + "/item/add"
        var parameters: [String : AnyObject] = ["StyleCode" : styleCode, "MerchantId" : merchantId]
        if let key = colorKey {
            parameters["ColorKey"] = key
        }
        if skuId != NSNotFound {
            parameters["SkuId"] = skuId
        }
        
        let result : (cartKey: String, isLogin: Bool) = cartKey()
        
        if !result.isLogin {
            let cartKeyParameter: [String:AnyObject] = ["CartKey":result.cartKey]
            parameters.merge(cartKeyParameter)
        }
        
        let request = RequestFactory.post(url, parameters: parameters, appendUserKey: result.isLogin ? true : false)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func removeWishlistItem(cartItemId : Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = WISHLIST_PATH + "/item/remove"
        var parameters : [String:AnyObject] = ["CartItemId":cartItemId]
        
        let result : (cartKey: String, isLogin: Bool) = cartKey()
        
        if !result.isLogin {
            let cartKeyParameter: [String:AnyObject] = ["CartKey":result.cartKey]
            parameters.merge(cartKeyParameter)
        }

        let request = RequestFactory.post(url, parameters: parameters, appendUserKey: result.isLogin ? true : false)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func userUpdate(cartKey : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = WISHLIST_PATH + "/user/update"
        let parameters : [String:AnyObject] = ["CartKey" : cartKey]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }

    class func list(completion complete: Response<AnyObject, NSError> -> Void) -> Request{
        var url : String!
        var parameters : [String:AnyObject]? = nil
        
        let result : (cartKey: String, isLogin: Bool) = cartKey()
        
        if result.isLogin {
            url = WISHLIST_PATH + "/view/user"
        } else {
            parameters = ["cartkey": result.cartKey]
            url = WISHLIST_PATH + "/view"
        }

        let request = RequestFactory.get(url, parameters: parameters, appendUserKey: result.isLogin ? true : false)
        request.exResponseJSON{response in complete(response)}
        return request
    }
    

    class func listByUser(completion complete: Response<AnyObject, NSError> -> Void) -> Request{
        let url = WISHLIST_PATH + "/view/user"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in complete(response)}
        return request
    }
    
    class func mergeWishlist(mergeCartKey : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = WISHLIST_PATH + "/merge"
        let parameters : [String:AnyObject] = ["MergeCartKey" : mergeCartKey]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    private class func cartKey() -> (String, Bool) {
        if Context.getAuthenticatedUser() {
            return ("0", true)
        }
        else {
            var cartKey = "0"
            if let cKey = Context.anonymousWishlistKey() {
                cartKey = cKey
            }
            return (cartKey, false)
            
        }
    }
}