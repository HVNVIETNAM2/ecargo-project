//
//  APIService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire


class ParseService {

    class func listProducts(completion : Response<AnyObject, NSError> -> Void) -> Request{
        
        var method : Alamofire.Method
        var url : URLStringConvertible
        var headers : [String : String]
        
        method = .GET
        url = Constants.Path.ParseHost + "Product"
        headers = Constants.Path.ParseHeaders
        
        
        let request = Alamofire.request(method, url, headers: headers)
        request.responseJSON{response in completion(response)}
        return request
    }
}
