//
//  BrandService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 23/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire

class BrandService {
    static let BRAND_PATH = Constants.Path.Host + "/brand"
    
    class func list(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = BRAND_PATH + "/list"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
}