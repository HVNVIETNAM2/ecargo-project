//
//  SkuService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 27/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class SkuService {
    static let STYLE_PATH = Constants.Path.Host + "/product/sku"
    class func list(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = STYLE_PATH + "/list"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
}