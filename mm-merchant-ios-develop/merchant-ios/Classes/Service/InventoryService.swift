//
//  InventoryService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 30/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class InventoryService{
    
    static let INVENTORY_PATH = Constants.Path.Host + "/inventory"

    
    class func view(id : Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = INVENTORY_PATH + "/location/view"
        let parameters = ["id" : id]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func list(id : Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = INVENTORY_PATH + "/location/list/user"
        let parameters = ["userid" : id]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
}
