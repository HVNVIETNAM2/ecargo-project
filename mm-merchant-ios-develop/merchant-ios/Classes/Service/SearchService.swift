//
//  SearchService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 10/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class SearchService {
    
    static let SEARCH_PATH = Constants.Path.Host + "/search"
    
    private class func searchStyle(queryString : String? = nil, pricefrom : Int? = nil, priceto : Int? = nil, brands : [Brand]? = nil, cats : [Cat]? = nil, colors : [Color]? = nil, sizes : [Size]? = nil, badges : [Badge]? = nil , merchants : [Merchant]? = nil, issale : Int? = nil, isnew : Int? = nil, sort : String? = nil, order : String? = nil, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/style"
        var parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1]
        if queryString != nil && queryString != "" {
            parameters["s"] = queryString
        }
        if pricefrom != nil && pricefrom != 0{
            parameters["pricefrom"] = pricefrom
        }
        if priceto != nil && priceto != Int(Constants.Price.Highest){
            parameters["priceto"] = priceto
        }
        if brands != nil {
            if brands?.isEmpty == false {
                var bait = ""
                for brand : Brand in brands!{
                    bait += String(brand.brandId)
                    bait += ","
                }
                bait =  String(bait.characters.dropLast())
                parameters["brandid"] = bait
            }
        }
        if cats != nil {
            if cats?.isEmpty == false {
                var bait = ""
                for cat : Cat in cats!{
                    bait += String(cat.categoryId)
                    bait += ","
                }
                bait =  String(bait.characters.dropLast())
                parameters["categoryid"] = bait
            }
        }
        if colors != nil {
            if colors?.isEmpty == false {
                var bait = ""
                for color : Color in colors!{
                    bait += String(color.colorId)
                    bait += ","
                }
                bait =  String(bait.characters.dropLast())
                parameters["colorid"] = bait
            }
        }
        if sizes != nil {
            if sizes?.isEmpty == false {
                var bait = ""
                for size : Size in sizes!{
                    bait += String(size.sizeId)
                    bait += ","
                }
                bait =  String(bait.characters.dropLast())
                parameters["sizeid"] = bait
            }
        }
        if badges != nil {
            if badges?.isEmpty == false {
                var bait = ""
                for badge : Badge in badges!{
                    bait += String(badge.badgeId)
                    bait += ","
                }
                bait =  String(bait.characters.dropLast())
                parameters["badgeid"] = bait
            }
        }
        if merchants != nil {
            if merchants?.isEmpty == false {
                var bait = ""
                for merchant : Merchant in merchants!{
                    bait += String(merchant.merchantId)
                    bait += ","
                }
                bait =  String(bait.characters.dropLast())
                parameters["merchantid"] = bait
            }
        }
        
        if issale != nil  && (issale == 1 || issale == 0){
            parameters["issale"] = issale
        }
        if isnew != nil && (isnew == 1 || isnew == 0){
            parameters["isnew"] = isnew
        }
        
        if sort != nil && sort != "" {
            parameters["sort"] = sort
        }
        if order != nil && order != "" {
            parameters["order"] = order
        }
        Log.debug(parameters)
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchStyle(styleFilter : StyleFilter, completion : Response<AnyObject, NSError> -> Void) -> Request{
        return searchStyle(styleFilter.queryString, pricefrom: styleFilter.pricefrom, priceto: styleFilter.priceto, brands: styleFilter.brands, cats: styleFilter.cats, colors: styleFilter.colors, sizes: styleFilter.sizes, badges: styleFilter.badges, merchants: styleFilter.merchants, isnew : styleFilter.isnew, issale : styleFilter.issale, sort: styleFilter.sort, order: styleFilter.order, completion: completion)
    }
    
    class func searchStyleByStyleCode(styleCode : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/style"
        let parameters : [String : AnyObject] = ["stylecode" : styleCode]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchSuggest(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/suggest"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchComplete(s : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/complete"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1, "s" : s]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchBrandCombined(s : String? = "", completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/brand/combined"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1, "s" : s!]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchBrand(s : String? = "", completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/brand"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1, "s" : s!]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchMerchant(s : String? = "", completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/merchant"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1, "s" : s!]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchCategory(s : String? = "", completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/category"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1, "s" : s!]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchSize(s : String? = "", completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/size"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1, "s" : s!]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchColor(s : String? = "", completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/color"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1, "s" : s!]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchBadge(s : String? = "", completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/badge"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1, "s" : s!]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func searchSizeGroup(s : String? = "", completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SEARCH_PATH + "/size/group"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "pageno" : 1, "s" : s!]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }

    
}