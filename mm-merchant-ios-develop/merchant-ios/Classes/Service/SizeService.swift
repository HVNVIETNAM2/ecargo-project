//
//  SizeService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class SizeService {
    static let SIZE_PATH = Constants.Path.Host + "/size"
    class func list(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = SIZE_PATH + "/list"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
}