//
//  CategoryService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 18/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire

class CatService {
    static let CATEGORY_PATH = Constants.Path.Host + "/category"
    
    class func list(id : Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = CATEGORY_PATH + "/list"
        let parameters = ["id" : id]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }

}