//
//  OrderService.swift
//  merchant-ios
//
//  Created by HungPM on 3/11/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import Alamofire

class OrderService {
    
    static let ORDER_PATH = Constants.Path.Host + "/order"
    
    class func createOrder(userAddressKey : String, skus : [Dictionary<String,AnyObject>], completion complete: Response<AnyObject, NSError> -> Void) -> Request{
        let url = ORDER_PATH + "/create"
        let parameters : [String:AnyObject] = ["UserAddressKey": userAddressKey, "Skus": skus]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in complete(response)}
        return request
    }
}