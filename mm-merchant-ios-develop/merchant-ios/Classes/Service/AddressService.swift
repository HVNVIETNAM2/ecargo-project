//
//  AddressService.swift
//  merchant-ios
//
//  Created by hungvo on 3/7/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit
import Alamofire

class AddressService {
    
    static let ADDRESS_PATH = Constants.Path.Host + "/address"
    
    class func list(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = ADDRESS_PATH + "/list"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func save(address : Address, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = ADDRESS_PATH + "/save"
        let parameters: [String : AnyObject] = ["RecipientName" : address.recipientName, "GeoCountryId" : address.geoCountryId, "GeoProvinceId": address.geoProvinceId, "GeoCityId": address.geoCityId, "PhoneCode": address.phoneCode, "PhoneNumber": address.phoneNumber, "IsDefault": address.isDefault, "Address": address.address]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func viewDefault(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = ADDRESS_PATH + "/default/view"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func saveDefault(userAddressKey : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = ADDRESS_PATH + "/default/save"
        let parameters: [String : AnyObject] = ["UserAddressKey" : userAddressKey]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
}
