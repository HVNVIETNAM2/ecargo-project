//
//  PhotoService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 25/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage



class ImageService {

    class func viewFullUrl(url : URLStringConvertible, completion : Response<Image, NSError> -> Void) -> Request{
        let request = RequestFactory.get(url)
        request.responseImage{response in completion(response)}
        return request
    }

    class func view(key : String, completion : Response<Image, NSError> -> Void) -> Request{
        return ImageService.view(key, size: 0, completion: completion)
    }
    
    class func view(key : String, size : Int, completion : Response<Image, NSError> -> Void) -> Request{
        let url = Constants.Path.Host + "/image/view"
        let parameters : [String:AnyObject]
        if size > 0 {
            parameters = ["key" : key, "s" : String(size)]
        } else {
            parameters = ["key" : key]
        }        
        let request = RequestFactory.get(url, parameters : parameters)
        request.responseImage{response in completion(response)}
        return request
    }
    
}
  