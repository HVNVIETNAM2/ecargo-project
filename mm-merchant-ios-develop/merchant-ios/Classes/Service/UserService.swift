//
//  UserService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper



class UserService {
    
    static let USER_PATH = Constants.Path.Host + "/user"
    
    class func view(id : Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = USER_PATH + "/view"
        let parameters = ["id" : id]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
        
    }
    class func viewWithUserKey(userKey : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = Constants.Path.Host + "/view/User"
        let parameters = ["UserKey" : userKey]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
        
    }
    
    class func list(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = USER_PATH + "/list/mm"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func save(user : User, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = USER_PATH + "/save"
        return postUser(user, url: url, completion: completion)
    }
    
    class func changeInventoryLocation(user : User, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = USER_PATH + "/inventorylocation/change"
        return postUser(user, url: url, completion: completion)
    }
    
    class func changeEmail(user : User, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = USER_PATH + "/email/change/immediate"
        return postUser(user, url: url, completion: completion)
    }
    
    class func changePassword(user : User, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = USER_PATH + "/password/change/immediate"
        return postUser(user, url: url, completion: completion)
    }
    
    class func changeMobile(user : User, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = USER_PATH + "/mobile/change"
        return postUser(user, url: url, completion: completion)
    }
    
    class func postUser(user : User, url : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        // create the request & response
        let request = NSMutableURLRequest(URL: NSURL(string: url)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 5)
        // create some JSON data and configure the request
        let jsonString = Mapper().toJSONString(user, prettyPrint: true)
        request.HTTPBody = jsonString!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        Log.debug(request.HTTPBody)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(NSUserDefaults.standardUserDefaults().objectForKey("token") as? String, forHTTPHeaderField: "Authorization")
        let alamofireRequest = Alamofire.request(request)
        alamofireRequest.exResponseJSON{response in completion(response)}
        return alamofireRequest
    }
    
    class func reportProblem(subject : String, message : String, completion : Response<AnyObject, NSError> -> Void) -> Request {
        let url = USER_PATH + "/problem"
        let parameters : [String : AnyObject] = ["UserId" : Context.getUserId(), "Subject" : subject, "Message" : message]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func listCurator(completion : Response<AnyObject, NSError> -> Void) -> Request {
        let url = USER_PATH + "/list/curator"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
}
