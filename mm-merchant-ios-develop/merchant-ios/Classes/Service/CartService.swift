//
//  CartService.swift
//  merchant-ios
//
//  Created by HungPM on 1/15/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import Alamofire

class CartService {
    
    static let CART_PATH = Constants.Path.Host + "/cart"
    
    class func createCart(completion complete: Response<AnyObject, NSError> -> Void) -> Request{
        let url = CART_PATH + "/create"
        let request = RequestFactory.post(url)
        request.exResponseJSON{response in complete(response)}
        return request
    }

    class func updateCartItem(cartItemId : Int, skuId : Int, qty : Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = CART_PATH + "/item/update"
        var parameters : [String:AnyObject] = ["CartItemId":cartItemId, "SkuId":skuId, "Qty":qty]

        let result : (cartKey: String, isLogin: Bool) = cartKey()
        
        if !result.isLogin {
            let cartKeyParameter: [String:AnyObject] = ["CartKey":result.cartKey]
            parameters.merge(cartKeyParameter)
        }

        let request = RequestFactory.post(url, parameters: parameters, appendUserKey: result.isLogin ? true : false)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func removeCartItem(cartItemId : Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = CART_PATH + "/item/remove"
        var parameters : [String:AnyObject] = ["CartItemId":cartItemId]
        
        let result : (cartKey: String, isLogin: Bool) = cartKey()
        
        if !result.isLogin {
            let cartKeyParameter: [String:AnyObject] = ["CartKey":result.cartKey]
            parameters.merge(cartKeyParameter)
        }

        let request = RequestFactory.post(url, parameters: parameters, appendUserKey: result.isLogin ? true : false)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func addCartItem(skuId : Int, qty : Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = CART_PATH + "/item/add"
        var parameters : [String:AnyObject] = ["SkuId":skuId, "Qty":qty]
        
        let result : (cartKey: String, isLogin: Bool) = cartKey()
        
        if !result.isLogin {
            let cartKeyParameter: [String:AnyObject] = ["CartKey":result.cartKey]
            parameters.merge(cartKeyParameter)
        }

        let request = RequestFactory.post(url, parameters: parameters, appendUserKey: result.isLogin ? true : false)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func list(completion complete: Response<AnyObject, NSError> -> Void) -> Request{
        var url : String!
        var parameters : [String:AnyObject]? = nil
        
        let result : (cartKey: String, isLogin: Bool) = cartKey()
        
        if result.isLogin {
            url = CART_PATH + "/view/user"
        }
        else {
            parameters = ["cartkey": result.cartKey]
            url = CART_PATH + "/view"
        }

        let request = RequestFactory.get(url, parameters: parameters, appendUserKey: result.isLogin ? true : false)
        request.exResponseJSON{response in complete(response)}
        return request
    }
    
    class func mergeCart(mergeCartKey : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = CART_PATH + "/merge"
        let parameters : [String:AnyObject] = ["MergeCartKey":mergeCartKey]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func viewUser(completion complete : Response<AnyObject, NSError> -> Void) -> Request{
        let url = CART_PATH + "/view/user"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in complete(response)}
        return request
    }
    
    class func userUpdate(cartKey : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = CART_PATH + "/user/update"
        let parameters : [String:AnyObject] = ["CartKey" : cartKey]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func moveItemToWishlist(cartItemId : Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = CART_PATH + "/item/move/wishlist"
        var parameters: [String : AnyObject] = ["CartItemId" : cartItemId]
        
        let result : (cartKey: String, isLogin: Bool) = cartKey()
        
        if !result.isLogin {
            let cartKeyParameter: [String:AnyObject] = ["CartKey": result.cartKey]
            parameters.merge(cartKeyParameter)
            
            let wishlistKey = wishlistCartKey()
            if wishlistKey != "0" {
                let cartKeyParameter: [String:AnyObject] = ["WishlistKey": wishlistKey]
                parameters.merge(cartKeyParameter)
            }
        }
        
        let request = RequestFactory.post(url, parameters: parameters, appendUserKey: result.isLogin ? true : false)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    private class func cartKey() -> (String, Bool) {
        if Context.getAuthenticatedUser() {
            return ("0", true)
        }
        else {
            var cartKey = "0"
            if let cKey = Context.anonymousShoppingCartKey() {
                cartKey = cKey
            }
            return (cartKey, false)

        }
    }
    
    private class func wishlistCartKey() -> String {
        var cartKey = "0"
        if !Context.getAuthenticatedUser(), let cKey = Context.anonymousWishlistKey() {
            cartKey = cKey
        }
        return cartKey
        
    }
    
}