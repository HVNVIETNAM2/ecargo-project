//
//  MmService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire


class AuthService {
    static let AUTH_PATH = Constants.Path.Host + "/auth"
    
    
    class func login(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/login", parameters: parameters, completion: completion)
        
    }
    
    class func loginWeChat(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/login/wechat", parameters: parameters, completion: completion)
        
    }

    
    class func forgotPassword(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/password/forgot", parameters: parameters, completion: completion)
        
    }
    
    class func resetPassword(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/passwordreset", parameters: parameters, completion: completion)
        
    }
    
    class func activateCode(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/code/activate", parameters: parameters, completion: completion)
    }
    
    class func reactivateCode(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/code/reactivate", parameters: parameters, completion: completion)
    }
    
    class func validateCode(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/code/validate", parameters: parameters, completion: completion)
    }
    
    class func resendCode(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/code/resend", parameters: parameters, completion: completion)
    }
    
    class func sendMobileVerification(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/mobileverification/send", parameters: parameters, completion: completion)
    }
    
    class func checkMobileVerification(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/mobileverification/check", parameters: parameters, completion: completion)
    }
    
    class func signup(parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        return post(AUTH_PATH + "/signup", parameters: parameters, completion: completion)
    }

    //MARK: Common post function for all auth services
    class func post(url : URLStringConvertible, parameters : [String : AnyObject], completion : Response<AnyObject, NSError> -> Void) -> Request{
        //Making unauthenicated call
        let request = Alamofire.request(Alamofire.Method.POST, url, parameters : parameters, encoding: .JSON)
        request.responseJSON{response in completion(response)}
        return request
    }
    
}
