//
//  MobileCodeService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 2/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//
import Foundation
import Alamofire
import ObjectMapper

class MobileCodeService{
    
    class func list(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = Constants.Path.Host + "/reference/general"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
}