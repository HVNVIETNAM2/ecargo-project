//
//  ViewService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 18/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import Alamofire
class ViewService {
    
    static let USER_PATH = Constants.Path.Host + "/view"
    
    class func viewWithUserKey(userKey : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = USER_PATH + "/user"
        let parameters = ["UserKey" : userKey]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
        
    }
    
}
