//
//  BadgeService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 15/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire

class BadgeService {
    static let BADGE_PATH = Constants.Path.Host + "/badge"
    
    class func list(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = BADGE_PATH + "/list"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
}