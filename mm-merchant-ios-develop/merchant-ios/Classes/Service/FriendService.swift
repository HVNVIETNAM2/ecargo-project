//
//  FriendService.swift
//  merchant-ios
//
//  Created by HVN_Pivotal on 3/4/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

//
//  FollowService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 16/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class FriendService {
    static let FRIEND_PATH = Constants.Path.Host + "/friend"
    
    class func listFriend(completion : Response<AnyObject, NSError> -> Void) -> Request {
        let url = FRIEND_PATH + "/list"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    class func findFriend(s : String, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = FRIEND_PATH + "/find"
        let parameters : [String : AnyObject] = ["pagesize" : 9999, "s" : s]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    class func listRequest(completion : Response<AnyObject, NSError> -> Void) -> Request {
        let url = FRIEND_PATH + "/request/receive"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func acceptRequest(friend : User, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = FRIEND_PATH + "/request/accept"
        let parameters : [String : AnyObject] = ["ToUserKey" : friend.userKey]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func deleteRequest(friend : User, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = FRIEND_PATH + "/delete"
        let parameters : [String : AnyObject] = ["ToUserKey" : friend.userKey]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func addFriendRequest(friend : User, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = FRIEND_PATH + "/request"
        let parameters : [String : AnyObject] = ["ToUserKey" : friend.userKey]
        let request = RequestFactory.post(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }

}
