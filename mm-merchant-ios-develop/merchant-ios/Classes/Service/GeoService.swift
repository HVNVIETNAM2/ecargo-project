//
//  GeoService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 2/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
class GeoService {
    
    static let GEO_PATH = Constants.Path.Host + "/geo"
    
    class func storefrontCountries(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = GEO_PATH + "/country/storefront"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func storefrontProvinces(geoCountryId :  Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = GEO_PATH + "/province?q=\(geoCountryId)"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    class func storefrontCities(geoProvinceId :  Int, completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = GEO_PATH + "/city?q=\(geoProvinceId)"
        let request = RequestFactory.get(url)
        request.exResponseJSON{response in completion(response)}
        return request
    }
    
    
}