//
//  ProductService.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 27/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class StyleService {
    static let STYLE_PATH = Constants.Path.Host + "/product/style"
    class func list(completion : Response<AnyObject, NSError> -> Void) -> Request{
        let url = STYLE_PATH + "/list"
        let parameters = ["size" : 500, "page" : 1]
        let request = RequestFactory.get(url, parameters: parameters)
        request.exResponseJSON{response in completion(response)}
        return request
    }
}