//
//  EntityURLFactory.swift
//  merchant-ios
//
//  Created by Tony Fung on 8/3/2016.
//  Copyright © 2016年 WWE & CO. All rights reserved.
//

import Foundation

class EntityURLFactory {

    class func userURL(user : User) -> NSURL{
        return NSURL(string: "https://mymm.com/u/\(user.userKey)")!
    }
    
}
