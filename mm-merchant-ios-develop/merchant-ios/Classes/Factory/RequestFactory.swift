//
//  RequestFactory.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 27/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import Alamofire

class RequestFactory {
    
    class func get(url : URLStringConvertible, parameters : [String : AnyObject]? = nil, appendUserKey: Bool = true) -> Request{
        
        var factoryParameters = ["cc" : Context.getCc()]
        
        if appendUserKey {
            factoryParameters["UserKey"] = Context.getUserKey()
            factoryParameters["userkey"] = Context.getUserKey()
        }
        
        if parameters != nil {
            factoryParameters.merge(parameters!)
        }

        let request = Alamofire.request(Alamofire.Method.GET, url, parameters: factoryParameters, headers : Context.getTokenHeader())
        Log.debug(request)
        
        return request
    }
    
    class func post(url : URLStringConvertible, parameters : [String : AnyObject]? = nil, appendUserKey: Bool = true) -> Request{
        
        var factoryParameters : [String : AnyObject] = ["cc" : Context.getCc(), "CultureCode" : Context.getCc(), "UserId" : Context.getUserId()]
        
        if appendUserKey {
            factoryParameters["UserKey"] = Context.getUserKey()
            factoryParameters["userkey"] = Context.getUserKey()
        }
        
        if parameters != nil {
            factoryParameters.merge(parameters!)
        }
        
        let request = Alamofire.request(Alamofire.Method.POST, url, parameters: factoryParameters, headers : Context.getTokenHeader(), encoding : .JSON)
        Log.debug(request)
        
        return request
    }
    
}