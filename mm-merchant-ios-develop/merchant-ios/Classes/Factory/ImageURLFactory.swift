//
//  ImageURLFactory.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 2/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

enum ImageCategory: String {
    case Product = "productimages"
    case Brand = "brandimages"
    case Merchant = "merchantimages"
    case Color = "colorimages"
    case User = "userimages"
}

class ImageURLFactory {
    
    class func get(key : String, category: ImageCategory = .Product) -> NSURL {
        Log.debug(NSURL(string: Constants.Path.Host + "/resizer/view?key=" + key + "&w=500&b=" + category.rawValue)!)
        return NSURL(string: Constants.Path.Host + "/resizer/view?key=" + key + "&w=500&b=" + category.rawValue)!
    }
    class func get(key: String, size:CGSize, category: ImageCategory = .Merchant) -> NSURL {
        Log.debug(NSURL(string: Constants.Path.Host + "/resizer/view?key=" + key + "&w=\(size.width)&h=\(size.height)&b=" + category.rawValue)!)
        return NSURL(string: Constants.Path.Host + "/resizer/view?key=" + key + "&w=\(size.width * 5)&h=\(size.height * 2)&b=" + category.rawValue)!
    }
    
}