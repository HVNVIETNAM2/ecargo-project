//
//  InventoryViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 20/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectMapper

class InventoryViewController : UITableViewController, UISearchBarDelegate{
    var user : User?
    var delegate: ChangeInventoryLocationDelegate?
    var inventoryLocations : [InventoryLocation] = []
    var filteredInventoryLocations : [InventoryLocation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.showLoading()
        self.setUpRefreshControl()

        loadInventoryLocations()
    }
    
    override func viewWillAppear(animated: Bool) {
         super.viewWillAppear(animated)
        
        // localization
        self.title = String.localize("LB_INVENTORY_LOCATION")
        self.refreshControl?.attributedTitle = NSAttributedString(string: String.localize("LB_PULL_DOWN_REFRESH"))
    }
    
    func setUpRefreshControl(){
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl!)
    }
    
    func refresh(sender : AnyObject){
        loadInventoryLocations()
    }
    
    func loadInventoryLocations(){
        InventoryService.list((self.user?.userId)!){[weak self] (response) in
            if let strongSelf = self {
                strongSelf.stopLoading()
                if response.result.isSuccess {
                    if let locations = Mapper<InventoryLocation>().mapArray(response.result.value) {
                        strongSelf.inventoryLocations = locations
                        strongSelf.filteredInventoryLocations = strongSelf.inventoryLocations
                        strongSelf.refreshControl!.endRefreshing()
                        strongSelf.tableView.reloadData()
                    }
                }
            }
        }
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("InventoryLocationCell")!
        if (self.filteredInventoryLocations.count > 0){
            cell.textLabel!.text = self.filteredInventoryLocations[indexPath.row].locationName
            cell.textLabel!.numberOfLines = 0
            cell.textLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
            
            cell.bounds = CGRect(x: 0, y: 0, width: CGRectGetWidth(tableView.bounds), height: 99999)
            cell.contentView.bounds = cell.bounds
            cell.layoutIfNeeded()
            
            cell.textLabel!.preferredMaxLayoutWidth = CGRectGetWidth(cell.textLabel!.frame)
            
            if self.filteredInventoryLocations[indexPath.row].inventoryLocationId == self.user?.inventoryLocationId{
                cell.accessoryView = UIImageView(image: UIImage(named: "tick"))
            }
        }
        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredInventoryLocations.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var inventoryLocation = InventoryLocation()
        inventoryLocation = self.filteredInventoryLocations[indexPath.row]
        delegate?.changeInventoryLocation(inventoryLocation)
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    // MARK: UISearchBarDelegate
    
    private func filter(text : String!) {
        self.filteredInventoryLocations = self.inventoryLocations.filter(){ $0.locationName.lowercaseString.rangeOfString(text.lowercaseString) != nil }
        self.tableView?.reloadData()
    }
    
    internal func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.filter(searchBar.text!)
        searchBar.resignFirstResponder()
    }
    
    internal func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.length == 0 {
            self.filteredInventoryLocations = self.inventoryLocations
            self.tableView.reloadData()
        } else {
            self.filter(searchBar.text!)
        }
    }

}