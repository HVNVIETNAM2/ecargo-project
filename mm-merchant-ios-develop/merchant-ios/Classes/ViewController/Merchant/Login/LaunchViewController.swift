//
//  LaunchViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 13/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation

class LaunchViewController : UIViewController{
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    var playerLayer : AVPlayerLayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playVideo()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
        
        // localization
        self.registerButton.setTitle(String.localize("LB_REGISTER"), forState: UIControlState.Normal)
        self.loginButton.setTitle(String.localize("LB_LOGIN"), forState: UIControlState.Normal)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
    }
    
    func playerDidFinishPlaying(note: NSNotification) {
        let seconds : Int64 = 0
        let preferredTimeScale : Int32 = 1
        let seekTime : CMTime = CMTimeMake(seconds, preferredTimeScale)
        self.playerLayer?.player?.seekToTime(seekTime)
        self.playerLayer?.player?.play()
    }
    
    func playVideo(){
        let path = NSBundle.mainBundle().pathForResource("bw", ofType:"mov")
        let videoURL = NSURL.fileURLWithPath(path!)
        let item = AVPlayerItem(URL: videoURL)
        let player = AVPlayer(playerItem: item)
        self.playerLayer = AVPlayerLayer(player: player)
        self.playerLayer!.frame = self.view.bounds
        self.view.layer.addSublayer(self.playerLayer!)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "playerDidFinishPlaying:", name: AVPlayerItemDidPlayToEndTimeNotification, object: item)
        self.playerLayer!.zPosition = -1
        player.play()
        self.registerButton.alpha = 0
        self.registerButton.formatPrimary()
        self.loginButton.alpha = 0
        self.loginButton.formatPrimary()
        UIView.animateWithDuration(5.0, animations: {
            self.registerButton.alpha = 1
            self.loginButton.alpha = 1
            }, completion: {
                (value: Bool) in
        })
    }
    
    
}
