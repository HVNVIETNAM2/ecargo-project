//
//  ViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 17/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import UIKit
import ObjectMapper
import AVKit
import AVFoundation


class LoginViewController: UIViewController, UITextFieldDelegate {
    
    var appDelegate : AppDelegate?
    var viewShiftedUp : Bool = false
    var token : Token?
    
    @IBAction func loginButtonClicked(sender: UIButton) {
        var res : Bool = true
        if userNameTextField.text?.length == 0 {
            res = false
            self.showUsernameWarning(String.localize("MSG_ERR_EMAIL_OR_MOBILE_NIL"))
        } else {
            if userNameTextField.text?.isValidMMUsername() == false {
                res = false
                self.showUsernameWarning(self.userNameWarningText)
            }
        }
        if passwordTextField.text?.length == 0 {
            res = false
            self.showPasswordWarning(String.localize("MSG_ERR_PASSWORD_NIL"))
        } else {
            self.hidePasswordWarning()
        }
        
        if res {
            sender.enabled = false
            self.showLoading()
            var userName = userNameTextField.text!
            userNameTextField.resignFirstResponder()
            let password = passwordTextField.text!
            passwordTextField.resignFirstResponder()
            userName.clean()
            login(userName, password: password, sender: sender)
        }
    }
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!

    @IBOutlet weak var hintLabel: UILabel!
    
    @IBOutlet weak var forgotButton: UIButton!

    @IBOutlet weak var userNameWarningLabel: UILabel!
    @IBOutlet weak var pwdWarningLabel: UILabel!
    // Constraint for error handling layout update
    @IBOutlet weak var pwdTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginBtnTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var userNameWarningHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pwdWarningHeightConstraint: NSLayoutConstraint!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.loginButton.enabled = true
        if let _ = UIFont(name: Constants.Font.Normal, size: Constants.Font.Size) {
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.secondary2(),
                NSFontAttributeName: UIFont(name: Constants.Font.Normal, size: Constants.Font.Size)!]
        } else {
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.secondary2(),
                NSFontAttributeName: UIFont(name: Constants.iOS8Font.Normal, size: Constants.Font.Size)!]
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        viewShiftedUp = false
        MobClick.beginLogPageView("Login")
        
        // localization
        self.title = String.localize("LB_LOGIN")
        self.userNameTextField.placeholder = String.localize("LB_EMAIL_OR_MOBILE")
        self.passwordTextField.placeholder = String.localize("LB_PASSWORD")
        self.hintLabel.text = String.localize("LB_MOBILE_NOTE")
        self.loginButton.setTitle(String.localize("LB_LOGIN"), forState: UIControlState.Normal)
        self.forgotButton.setTitle(String.localize("LB_FORGOT_PASSWORD"), forState: UIControlState.Normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.primary2()
        self.loginButton.formatPrimary()
        self.userNameTextField.delegate = self;
        self.passwordTextField.delegate = self;
        self.userNameTextField.format()
        self.passwordTextField.format()
        self.hintLabel.textColor = UIColor.secondary2()
        self.forgotButton.setTitleColor(UIColor.secondary2(), forState: .Normal)
        appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        MobClick.endLogPageView("Login")
    }
    

    func keyboardWillShow(sender: NSNotification) {
        if !viewShiftedUp{
            UIView.animateWithDuration(1.0, animations: { self.view.frame.origin.y -= 150 }, completion: nil)
            viewShiftedUp = true
        }
        
    }
    func keyboardWillHide(sender: NSNotification) {
        if viewShiftedUp{
            UIView.animateWithDuration(1.0, animations: { self.view.frame.origin.y += 150 }, completion: nil)
            viewShiftedUp = false
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [UIInterfaceOrientationMask.Portrait]
    }
    
    var tField: UITextField!
    
    func configurationTextField(textField: UITextField!)
    {
        textField.placeholder = String.localize("LB_EMAIL_OR_MOBILE")
        tField = textField
    }
    
    
    func handleCancel(alertView: UIAlertAction!)
    {
        Log.debug("Cancelled !!")
    }

    @IBAction func forgotButtonClicked(sender: AnyObject) {
        let alert = UIAlertController(title: String.localize("LB_TL_RESET_PASSWORD"), message: String.localize("LB_TL_SEND_ACTIVATION_CODE"), preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: String.localize("LB_CANCEL"), style: UIAlertActionStyle.Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: String.localize("LB_OK"), style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
            self.showLoading()
            let parameters = ["UserKey" : self.tField.text!]
            AuthService.forgotPassword(parameters){[weak self] (response) in
                if let strongSelf = self {
                    strongSelf.stopLoading()
                    if response.result.isSuccess {
                        if (response.response?.statusCode == 200){
                            let resetPasswordViewController = UIStoryboard(name: "Password", bundle: nil).instantiateViewControllerWithIdentifier("ResetPasswordViewController") as! ResetPasswordViewController
                            resetPasswordViewController.userKey = strongSelf.tField.text!
                            strongSelf.navigationController?.pushViewController(resetPasswordViewController, animated: true)
                        } else {
                            strongSelf.handleApiResponseError(response)
                        }
                    } else {
                        strongSelf.showErrorAlert(String.localize("MSG_ERR_NETWORK_FAIL"))
                    }
                }
            }
        }))
        self.presentViewController(alert, animated: true, completion: {
            Log.debug("completion block")
        })
    }
    
    func login(userName : String, password : String, sender:UIButton ){
        let parameters = [
            "Username" : userName,
            "Password" : password,
        ]

        AuthService.login(parameters){[weak self] (response) in
            if let strongSelf = self {
                strongSelf.stopLoading()

                if response.result.isSuccess{
                    if response.response!.statusCode == 200 {
                        let token = Mapper<Token>().map(response.result.value)
                        let tokenString = token?.token
                        let userId = token?.userId
                        Context.setToken(tokenString!)
                        Context.setUserId(userId!)
                        Context.setUsername(userName)
                        Context.setPassword(password)
                        Context.setAuthenticatedUser(true)
                        
                        MobClick.profileSignInWithPUID(userName)
                        let barcodeViewController = UIStoryboard(name: "User", bundle: nil).instantiateViewControllerWithIdentifier("UserViewController")
                        strongSelf.navigationController?.setViewControllers([barcodeViewController], animated: true)
                    } else {
                        sender.enabled = true
                        strongSelf.handleLoginApiResponseError(response.result.value)
//                            strongSelf.showPasswordWarning(msg)
//                            strongSelf.showPasswordWarning(String.localize("LB_ERROR"))
                    }
                } else {
                    sender.enabled = true
                    strongSelf.showErrorAlert(String.localize("MSG_ERR_NETWORK_FAIL"))
                }
            }
        }

    }

    // MARK: UITextFieldDelegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == self.userNameTextField {
            self.hideUsernameWarning()
        } else if textField == self.passwordTextField {
            self.hidePasswordWarning()
        }
        return true
        // input checking code
//        if textField == self.userNameTextField {
//            if string.length == 0 {
//                if textField.text?.length > 0 {
//                    let text : String! = textField.text?.substringToIndex((textField.text?.startIndex.advancedBy(((textField.text?.length)! - 1)))!)
//                    if text.isValidEmail() {
//                        self.pwdTopConstraint.constant = 0;
//                    } else {
//                        self.pwdTopConstraint.constant = 28;
//                    }
//                }
//            } else {
//                if let text: String = textField.text! + string {
//                    if text.isValidEmail() {
//                        self.pwdTopConstraint.constant = 0;
//                    } else {
//                        self.pwdTopConstraint.constant = 28;
//                    }
//                }
//            }
//        }
//        return true
    }

    var userNameWarningText: String {
        get {
            return String.localize("MSG_ERR_FIELDNAME_PATTERN").stringByReplacingOccurrencesOfString(Constants.PlaceHolder.FieldName, withString: self.userNameTextField.placeholder!)
        }
    }

    func showUsernameWarning(message: String) {
        self.userNameWarningLabel.text = message
        let size = self.userNameWarningLabel.sizeThatFits(CGSizeMake(CGRectGetWidth(self.userNameWarningLabel.frame), CGFloat.max))
        self.userNameWarningHeightConstraint.constant = max(17, size.height)
        self.pwdTopConstraint.constant = (self.userNameWarningHeightConstraint.constant + 11)
        self.userNameWarningLabel.hidden = false
    }
    
    func hideUsernameWarning() {
        self.pwdTopConstraint.constant = 0
            self.userNameWarningLabel.hidden = true
    }
    
    func showPasswordWarning(message: String) {
        self.pwdWarningLabel.text = message
        let size = self.pwdWarningLabel.sizeThatFits(CGSizeMake(CGRectGetWidth(self.pwdWarningLabel.frame), CGFloat.max))
        self.pwdWarningHeightConstraint.constant = max(17, size.height)
        self.loginBtnTopConstraint.constant = (self.pwdWarningHeightConstraint.constant + 11)
        self.pwdWarningLabel.hidden = false
    }
    
    func hidePasswordWarning() {
        self.loginBtnTopConstraint.constant = 8;
        self.pwdWarningLabel.hidden = true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField == self.userNameTextField {
            self.hideUsernameWarning()
        } else if textField == self.passwordTextField {
            self.hidePasswordWarning()
        }
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == self.userNameTextField {
            if textField.text?.length > 0 {
                if textField.text?.isValidEmail() == true {
                    self.hideUsernameWarning()
                } else if textField.text?.isValidPhone() == true {
                    self.hidePasswordWarning()
                } else {
                    self.showUsernameWarning(self.userNameWarningText)
                }
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true
    }
    
}

