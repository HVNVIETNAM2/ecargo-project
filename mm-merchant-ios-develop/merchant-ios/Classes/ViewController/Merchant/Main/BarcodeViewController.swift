//
//  barcodeViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 18/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import QRCodeReader

class BarcodeViewController: UIViewController, QRCodeReaderViewControllerDelegate {
    @IBOutlet weak var scannedProductTextField: UITextField!
    
    var appDelegate : AppDelegate?

    lazy var reader = QRCodeReaderViewController(metadataObjectTypes: [AVMetadataObjectTypeQRCode])

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController!.navigationBar.hidden = false
        self.title = "Scan"

        appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
        
        let scanButton : UIBarButtonItem = UIBarButtonItem(title: "Scan", style: UIBarButtonItemStyle.Plain, target: self, action: "scanAction:")
        
        self.navigationItem.rightBarButtonItem = scanButton
       
    }
   
    @IBAction func userListButtonClicked(sender: AnyObject) {
        let userStoryBoard = UIStoryboard(name: "User", bundle: nil)
        let userListViewController = userStoryBoard.instantiateViewControllerWithIdentifier("UserListViewController")
        self.navigationController?.pushViewController(userListViewController, animated: true)

    
    }
    
    
    @IBAction func ProfileButtonClicked(sender: UIButton) {
        let userStoryBoard = UIStoryboard(name: "User", bundle: nil)
        let profileViewController = userStoryBoard.instantiateViewControllerWithIdentifier("UserViewController")
        self.navigationController?.pushViewController(profileViewController, animated: true)

        
    }
    
    @IBAction func CollectionViewButtonClicked(sender: UIButton) {
        let productStoryBoard = UIStoryboard(name: "Product", bundle: nil)
        let productCollectionViewController = productStoryBoard.instantiateViewControllerWithIdentifier("ProductCollectionViewController")
        self.navigationController?.pushViewController(productCollectionViewController, animated: true)
        
    }
    
    @IBAction func TableViewButtonClicked(sender: UIButton) {
        let productStoryBoard = UIStoryboard(name: "Product", bundle: nil)
        let productTableViewController = productStoryBoard.instantiateViewControllerWithIdentifier("ProductTableViewController")
        self.navigationController?.pushViewController(productTableViewController, animated: true)
      
    }
    
    
    @IBAction func scanAction(sender: AnyObject) {
        if (!QRCodeReader.supportsMetadataObjectTypes()){
            Alert.alert(self, title: "Media type not support", message: "Your phone does not support the media type")
            return
        }

        reader.delegate = self
        reader.modalPresentationStyle = .FormSheet
        presentViewController(reader, animated: true, completion: nil)
    }
    
    // MARK: - QRCodeReader Delegate Methods
    
    func reader(reader: QRCodeReaderViewController, didScanResult result: String){
        Log.debug(result)
        self.scannedProductTextField.text = result
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func readerDidCancel(reader: QRCodeReaderViewController){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
//    @IBAction func toggle(sender: AnyObject) {
//        navigationController?.setNavigationBarHidden(navigationController?.navigationBarHidden == false, animated: true) //or animated: false
//    }
//    
//    override func prefersStatusBarHidden() -> Bool {
//        return navigationController?.navigationBarHidden == true
//    }
//    
//    override func preferredStatusBarUpdateAnimation() -> UIStatusBarAnimation {
//        return UIStatusBarAnimation.Fade
//    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        MobClick.beginLogPageView("BarcodeView")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        MobClick.endLogPageView("BarcodeView")
    }
    
    

}