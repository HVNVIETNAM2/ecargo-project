//
//  AddressSelectionViewController.swift
//  merchant-ios
//
//  Created by hungvo on 2/19/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class AddressSelectionViewController: MmViewController {
    
    private final let AddressSelectionCellID = "AddressSelectionCellID"
    private final let DefaultCellID = "DefaultCellID"
    private final let AddressSelectionCellHeight = CGFloat(91)
    private final let AddNewAddressViewHeight = CGFloat(66)
    
    private let addNewAddressPadding = CGFloat(12)
    
    var selectionAddreses = [Address] ()
    
    var selectedAddress : Address?
    var didAddressSelectedHandler: ((Address) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBarHidden = false
        self.title = String.localize("LB_CA_MY_ADDRESS")
        
        setupNavigationBar()
        
        self.collectionView.frame = CGRectMake(self.collectionView.frame.origin.x, self.collectionView.frame.origin.y, self.collectionView.frame.width, self.collectionView.frame.height - AddNewAddressViewHeight)
        self.collectionView!.backgroundColor = UIColor.whiteColor()
        
        self.collectionView!.registerClass(AddressSelectionCell.self, forCellWithReuseIdentifier: AddressSelectionCellID)
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: DefaultCellID)
        
        let addNewAddressView = { () -> UIView in
            
            let view = UIView(frame: CGRectMake(0, self.view.bounds.height - AddNewAddressViewHeight, self.view.bounds.width, AddNewAddressViewHeight))
            
            let borderView = UIView(frame: CGRectMake(0, 0, self.view.bounds.width, 1))
            borderView.backgroundColor = UIColor(hexString: "#F1F1F1")
            view.addSubview(borderView)
        
            let addNewAddressButton = UIButton(frame: CGRectMake(addNewAddressPadding, addNewAddressPadding, self.view.bounds.width - addNewAddressPadding*2 , AddNewAddressViewHeight - addNewAddressPadding*2))
            addNewAddressButton.setTitle(String.localize("LB_CA_ADD_ADDR"), forState: .Normal)
            addNewAddressButton.formatPrimary()
            addNewAddressButton.backgroundColor = UIColor(hexString: "#EF385A")
            addNewAddressButton.addTarget(self, action: "addNewAddressButtonTapped", forControlEvents: .TouchUpInside)
            view.addSubview(addNewAddressButton)
            
            return view
        } ()
        
        self.view.addSubview(addNewAddressView)
        self.navigationController?.navigationBarHidden = false
        
        self.showLoading()
        firstly{
            return self.listAddressItem()
            }.then { _ -> Void in
                // reload UI
                self.collectionView.reloadData()
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldHideTabBar() -> Bool {
        return  true
    }
    
    func setupNavigationBar() {
        self.createBackButton()
    }
    
    
    func listAddressItem(completion complete:(() -> Void)? = nil) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            AddressService.list({
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            if let addreses: Array<Address> = Mapper<Address>().mapArray(response.result.value) {
                                strongSelf.selectionAddreses = addreses
                            }
                            
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
                })
        }
    }
    
    func saveDefaultAddress(userAddressKey :String, completion complete:(() -> Void)? = nil) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            AddressService.saveDefault(userAddressKey) {
                (response) in
                if response.result.isSuccess{
                    if response.response?.statusCode == 200 {
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        reject(error)
                    }
                }
                else{
                    reject(response.result.error!)
                }
            }
        }
    }
    
    //MARK: Action Handler
    func addNewAddressButtonTapped() {
        let addressAdditionViewController = AddressAdditionViewController()
        self.navigationController?.pushViewController(addressAdditionViewController, animated: true)
    }
    
    func navigateToOtherViewControllers() {
        
        if let callback = self.didAddressSelectedHandler, let selectedAddress =  self.selectedAddress {
            callback(selectedAddress)
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: CollectionView Data Source, Delegate Method
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectionAddreses.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(AddressSelectionCellID, forIndexPath: indexPath) as! AddressSelectionCell
        
        //TODO: need set an address schema
        cell.data = selectionAddreses[indexPath.row]
        
        if let selectedAddress = self.selectedAddress {
            if selectedAddress.userAddressKey == selectionAddreses[indexPath.row].userAddressKey {
                cell.checkboxButton.selected = true
            } else {
                cell.checkboxButton.selected = false
            }
        }
        
        cell.checkboxTappedHandler = { [weak self] (data) in
            
            if let strongSelf = self {
                strongSelf.selectedAddress = self!.selectionAddreses[indexPath.row]
                strongSelf.navigateToOtherViewControllers()
            }
        }
        
        return cell;
    }
    
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(DefaultCellID, forIndexPath: indexPath)
        return cell
    }

    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSizeMake(self.view.bounds.width, AddressSelectionCellHeight)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            
            let cell = collectionView.cellForItemAtIndexPath(indexPath) as! AddressSelectionCell
            cell.checkboxButton.selected = true
            
            selectedAddress = selectionAddreses[indexPath.row]
            if selectedAddress != nil {
                firstly{
                    return saveDefaultAddress(selectedAddress!.userAddressKey)
                    }.then { _ -> Void in
                        self.navigateToOtherViewControllers()
                    }.error { _ -> Void in
                        Log.error("error")
                }
            }
    }
}
