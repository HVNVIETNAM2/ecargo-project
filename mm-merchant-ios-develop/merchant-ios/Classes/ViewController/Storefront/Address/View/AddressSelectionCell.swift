//
//  AddressSelectionCell.swift
//  merchant-ios
//
//  Created by hungvo on 2/19/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit

class AddressSelectionCell: UICollectionViewCell {
    
    private final let FontSize = 14
    private final let LabelHeight: CGFloat = 23
    private final let ReceiverLabelPaddingTop: CGFloat = 9
    private final let DisclosureIndicatorWidth: CGFloat = 30
    private final let DisclosureIndicatorXPosition: CGFloat = 42

    var checkboxButton: UIButton!
    private var receiverLabel: UILabel!
    private var phoneLabel: UILabel!
    private var descriptionLabel: UILabel!
    var checkboxTappedHandler: (() -> Void)?
    
    var data: Address? {
        didSet {
            if let data = self.data {
                receiverLabel.text = data.recipientName
                phoneLabel.text = "(\(data.phoneCode)) \(data.phoneNumber)"
                descriptionLabel.text = "\(data.address), \(data.city), \(data.province), \(data.country)"
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let checkBoxContainer = { () -> UIView in
            let view = CenterLayoutView(frame: CGRectMake(0, 0, ShoppingCartSectionCheckBoxWidth, frame.size.height))
            let button = UIButton(type: .Custom)
            button.config(
                normalImage: UIImage(named: "icon_checkbox_unchecked"),
                selectedImage: UIImage(named: "icon_checkbox_checked")
            )
            button.sizeToFit()
            button.addTarget(self, action: "toggleCheckbox:", forControlEvents: .TouchUpInside)
            view.addSubview(button)
            self.checkboxButton = button
            return view
        } ()
        contentView.addSubview(checkBoxContainer)
        
        let disclosureIndicatorImageView = UIImageView(frame: CGRectMake(frame.size.width - DisclosureIndicatorXPosition, 0, DisclosureIndicatorWidth, frame.size.height))
        disclosureIndicatorImageView.image = UIImage(named: "icon_arrow_small")
        disclosureIndicatorImageView.contentMode = .ScaleAspectFit
        contentView.addSubview(disclosureIndicatorImageView)
        
        let widthOfLabel = disclosureIndicatorImageView.frame.minX - checkBoxContainer.frame.maxX
        receiverLabel = { () -> UILabel in
            let label = UILabel(frame: CGRectMake(checkBoxContainer.frame.maxX, ReceiverLabelPaddingTop, widthOfLabel, LabelHeight))
            label.formatSize(FontSize)
            label.textColor = UIColor(hexString: "#292929")
            return label
            } ()
        contentView.addSubview(receiverLabel)
        
        phoneLabel = { () -> UILabel in
            let label = UILabel(frame: CGRectMake(checkBoxContainer.frame.maxX, receiverLabel.frame.maxY, widthOfLabel, LabelHeight))
            label.formatSize(FontSize)
            label.textColor = UIColor(hexString: "#292929")
            return label
            } ()
        contentView.addSubview(phoneLabel)
        
        descriptionLabel = { () -> UILabel in
            let label = UILabel(frame: CGRectMake(checkBoxContainer.frame.maxX, phoneLabel.frame.maxY, widthOfLabel, LabelHeight))
            label.formatSize(FontSize)
            label.textColor = UIColor(hexString: "#292929")
            return label
            } ()
        contentView.addSubview(descriptionLabel)
        
        let borderView = UIView(frame: CGRectMake(0, frame.size.height - 1, frame.size.width, 1))
        borderView.backgroundColor = UIColor(hexString: "#F1F1F1")
        contentView.addSubview(borderView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func toggleCheckbox(button: UIButton) {
        
        checkboxButton.selected = !checkboxButton.selected
        
        if let callback = self.checkboxTappedHandler {
            callback()
        }
    }
}
