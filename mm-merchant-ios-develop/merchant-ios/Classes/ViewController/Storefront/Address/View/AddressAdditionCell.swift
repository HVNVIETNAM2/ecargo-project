//
//  AddressAdditionCell.swift
//  merchant-ios
//
//  Created by hungvo on 2/19/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit

class AddressAdditionCell: UICollectionViewCell, UITextViewDelegate {
    
    private final let FontSize = 14
    private final let ArrowWidth : CGFloat = 7
    private final let ArrowHeight : CGFloat = 15
    
    private let textFieldMarginLeft: CGFloat = 11
    private let textFieldPaddingLeft: CGFloat = 23
    private let arrowViewPaddingRight: CGFloat = 13
    
    private let textContainerInset  = UIEdgeInsetsMake(5, 18, 5, 0)
    private let textHolderColor = UIColor(hexString:"#CDCFD2")
    
    var textField: UITextField!
    var textView: UITextView!
    var arrowView: UIImageView!

    var checkboxTappedHandler: (() -> Void)?
    
    var textViewBeginEditting : (() -> Void)?
    var textViewEndEditting : (() -> Void)?
    
    var placeholder: String? {
        didSet {
            if let placeholder = self.placeholder {
                self.textField.placeholder = placeholder
                self.textView.text = placeholder
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let textField = UITextField(frame: CGRectMake(textFieldMarginLeft, 0, frame.size.width - textFieldMarginLeft * 2, frame.size.height + 1))
        textField.layer.borderColor = UIColor.secondary1().CGColor
        textField.layer.borderWidth = Constants.TextField.BorderWidth
        let paddingView = UIView(frame: CGRectMake(0, 0, textFieldPaddingLeft, self.frame.height))
        textField.leftView = paddingView
        textField.leftViewMode = UITextFieldViewMode.Always
        textField.font = UIFont(name: textField.font!.fontName, size: CGFloat(FontSize))
        textField.textColor = UIColor.secondary2()
        self.textField = textField
        
        let textView = UITextView(frame: CGRectMake(textFieldMarginLeft, 0, frame.size.width - textFieldMarginLeft * 2, frame.size.height + 1))
        textView.layer.borderColor = UIColor.secondary1().CGColor
        textView.layer.borderWidth = Constants.TextField.BorderWidth
        textView.font = UIFont(name: textField.font!.fontName, size: CGFloat(FontSize))
        textView.textColor = textHolderColor
        textView.textContainerInset = textContainerInset;
        textView.delegate = self
        self.textView = textView
        
        arrowView = { () -> UIImageView in
            let imageView = UIImageView(frame: CGRectMake(textField.bounds.width - ArrowWidth - arrowViewPaddingRight, textField.bounds.height / 2 - ArrowHeight/2, ArrowWidth, ArrowHeight))
            imageView.image = UIImage(named: "icon_arrow")
            imageView.contentMode = .ScaleAspectFit
            return imageView
        } ()

        textField.addSubview(arrowView)
        
        contentView.addSubview(textField)
        contentView.addSubview(textView)
    }
    
    func hiddenTextField(hidden:Bool) {
        self.textField.hidden = hidden
        self.textView.hidden = !hidden
    }

    func hiddenArrowView(hidden: Bool) {
        self.arrowView.hidden = hidden
    }
    
    func removeMarginLeft() {
        var frame = self.textField.frame
        frame.origin.x = -1
        frame.size.width = frame.size.width + textFieldMarginLeft + 1
        self.textField.frame = frame
    }
    
    func removeMarginRight() {
        var frame = self.textField.frame
        frame.size.width = frame.size.width + textFieldMarginLeft
        self.textField.frame = frame
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        if textView.textColor == textHolderColor {
            textView.textColor = UIColor.secondary2()
            textView.text = nil
        }
        
        if let callback = self.textViewBeginEditting {
            callback()
        }
        
        return true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholder
            textView.textColor = textHolderColor
        }
        
        if let callback = self.textViewEndEditting {
            callback()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
