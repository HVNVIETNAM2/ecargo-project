//
//  AddressInputView.swift
//  merchant-ios
//
//  Created by hungvo on 2/21/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit

class AddressInputView: UIView {
    
    var pickerView : UIPickerView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        pickerView = UIPickerView(frame: CGRectMake(0, 0, frame.size.width, frame.size.height))
        pickerView.delegate = nil
        pickerView.dataSource = nil
        pickerView.showsSelectionIndicator = true
        
        self.addSubview(pickerView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
