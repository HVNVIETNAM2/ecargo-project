//
//  AddressAdditionViewController.swift
//  merchant-ios
//
//  Created by hungvo on 2/19/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit
import PromiseKit
import ObjectMapper

class AddressAdditionViewController: SignupModeViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    enum IndexPathRow: Int {
        case ReceiverName
        case Country
        case PhoneCode
        case PhoneNumber
        case Address
        case StreetAddress
        case PostalZip
    }

    private final let AddressAdditionCellID = "AddressAdditionCellID"
    private final let DefaultCellID = "DefaultCellID"
    private final let HeaderViewHeight = CGFloat(25)
    private final let DefaultCellHeight = CGFloat(46)
    private final let AddressCellHeight = CGFloat(60)
    private final let PhoneCodeCellWidth = CGFloat(120)
    private final let ConfirmViewHeight = CGFloat(61)
    private final let InputViewHeight = CGFloat(206)

    private let titleHeaderPaddingTop = CGFloat(3)
    
    private let sectionInsets = UIEdgeInsets(top: 27, left: 0, bottom: 0, right: 0)
    
    private let phoneCodeTextFieldTag = 1001
    private let phoneNumberTextFieldTag = 1002
    
    var tapGesture: UITapGestureRecognizer?
    var activeTextField: UITextField?
    var activeTextView: UITextView?
    var countryTextField: UITextField?
    var provinceTextField: UITextField?

    //TODO: dummy data - need to modify
    private let placeholders = [String.localize("LB_CA_RECEIVER_NAME"), String.localize("LB_SELECT_COUNTRY"), String.localize("PhoneCode"), String.localize("LB_CA_INPUT_MOBILE"), String.localize("LB_CA_ADDR_PICKER_PLACEHOLDER"), String.localize("LB_CA_DETAIL_STREET_ADDR"), String.localize("LB_POSTAL_OR_ZIP")]
    
    var countryPicker : UIPickerView!
    var countryPickerDataSource = Array<GeoCountry> ()
    var geoCountrySelected : GeoCountry?

    var cityPicker : UIPickerView!
    var cityPickerDataSource : NSMutableArray = [Array<GeoProvince> (), Array<GeoCity> ()]
    var geoProvinceSelected : GeoProvince?
    var geoCitySelected : GeoCity?
    
    var recipientCell : AddressAdditionCell!
    var countryCell : AddressAdditionCell!
    var phoneCodeCell : AddressAdditionCell!
    var phoneNumberCell : AddressAdditionCell!
    var cityCell : AddressAdditionCell!
    var addressCell : AddressAdditionCell!
    var postalCell : AddressAdditionCell!
    
    var addressCreated : Address?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBarHidden = false
        self.title = String.localize("LB_CA_NEW_SHIPPING_ADDR")
        
        setupNavigationBar()
        
        let headerView = { () -> UIView in
            
            let view = UIView(frame: CGRectMake(0, self.collectionView.frame.origin.y, self.view.bounds.width, HeaderViewHeight))
            view.backgroundColor = UIColor(hexString: "#ECECEC")
            
            let titleHeader = UILabel(frame: CGRectMake(0, titleHeaderPaddingTop, self.view.bounds.width, HeaderViewHeight - titleHeaderPaddingTop))
            titleHeader.text = String.localize("LB_CA_SHIPPING_ADDR")
            titleHeader.formatSize(12)
            titleHeader.textColor = UIColor(hexString: "#4A4A4A")
            titleHeader.textAlignment = .Center
            view.addSubview(titleHeader)
            
            return view
        } ()
        
        self.view.addSubview(headerView)
        
        
        self.collectionView.frame = CGRectMake(self.collectionView.frame.origin.x, self.collectionView.frame.origin.y + HeaderViewHeight, self.collectionView.frame.width, self.collectionView.frame.height - ConfirmViewHeight)
        
        self.collectionView!.registerClass(AddressAdditionCell.self, forCellWithReuseIdentifier: AddressAdditionCellID)
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: DefaultCellID)
        
        let confirmView = { () -> UIView in
            
            let Margin = CGFloat(10)
            let view = UIView(frame: CGRectMake(Margin, self.view.bounds.height - ConfirmViewHeight - Margin, self.view.bounds.width - 2 * Margin, ConfirmViewHeight))
            
            let borderView = UIView(frame: CGRectMake(0, 0, self.view.bounds.width, 1))
            borderView.backgroundColor = UIColor.primary2()
            view.addSubview(borderView)
            
            let confirmButton = UIButton(frame: CGRectMake(0, 0, view.bounds.width, view.bounds.height))
            confirmButton.setTitle(String.localize("LB_CA_SAVE"), forState: .Normal)
            confirmButton.backgroundColor = UIColor(hexString: "EF385A")
            confirmButton.layer.cornerRadius = 5
            confirmButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            confirmButton.titleLabel!.font = UIFont(name: confirmButton.titleLabel!.font.fontName, size: CGFloat(14))!
            confirmButton.addTarget(self, action: "confirmButtonTapped", forControlEvents: .TouchUpInside)
            view.addSubview(confirmButton)
            
            return view
        } ()
        
        self.view.addSubview(confirmView)

        tapGesture = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        self.showLoading()
        firstly{
            return self.listGeoCountry()
            }.then { _ -> Void in
                // reload country picker
                self.countryPicker.reloadAllComponents()
                
                var index = 0
                for i in 0 ..< self.countryPickerDataSource.count {
                    let country = self.countryPickerDataSource[i]
                    if country.mobileCode == "+86" {
                        index = i
                        break
                    }
                }
                self.countryPicker.selectRow(index, inComponent: 0, animated: false)
                self.selectCountryAtRow(index)
                
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldHideTabBar() -> Bool {
        return  true
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setupNavigationBar() {
        let buttonBack = UIButton(type: .Custom)
        buttonBack.setImage(UIImage(named: "back"), forState: .Normal)
        buttonBack.frame = CGRectMake(0, 0, Constants.Value.BackButtonWidth, Constants.Value.BackButtonHeight)
        buttonBack.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: Constants.Value.BackButtonMarginLeft, bottom: 0, right: 0)
        let leftButton = UIBarButtonItem(customView: buttonBack)
        
        buttonBack.addTarget(self, action: "backButtonTapped", forControlEvents: .TouchUpInside)
        
        if self.signupMode == .Normal {
            self.navigationItem.leftBarButtonItem = leftButton
            self.navigationItem.hidesBackButton = false
        } else if self.signupMode == .Checkout || self.signupMode == .CheckoutSwipeToPay {
            self.navigationItem.hidesBackButton =  true
            self.navigationItem.leftBarButtonItem = nil
        }
        
    }
    
    //MARK: Action handler
    
    func backButtonTapped() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func confirmButtonTapped() {
        Log.debug("confirmButtonTapped")
        
        guard !(self.recipientCell.textField.text?.length < 1) else {
            self.showError(String.localize("Missing recipient name"), animated: true)//TODO: need to change correct string
            return
        }
        
        guard geoCountrySelected != nil else {
            self.showError(String.localize("Missing country"), animated: true)//TODO: need to change correct string
            return
        }
        
        guard !(self.phoneCodeCell.textField.text?.length < 1) else {
            self.showError(String.localize("Missing phone code"), animated: true)//TODO: need to change correct string
            return
        }
        
        guard !(self.phoneNumberCell.textField.text?.length < 1) else {
            self.showError(String.localize("Missing phone number"), animated: true)//TODO: need to change correct string
            return
        }

        guard (geoProvinceSelected != nil) else {
            self.showError(String.localize("Missing province"), animated: true)//TODO: need to change correct string
            return
        }
        
        guard (geoCitySelected != nil) else {
            self.showError(String.localize("Missing city"), animated: true)//TODO: need to change correct string
            return
        }
        
        guard (self.addressCell.textView.textColor == UIColor.secondary2() && self.addressCell.textView.text?.length >= 1) else {
            self.showError(String.localize("Missing address"), animated: true)//TODO: need to change correct string
            return
        }
        
        guard !(self.postalCell.textField.text?.length < 1) else {
            self.showError(String.localize("Missing postal code"), animated: true)//TODO: need to change correct string
            return
        }
        
        let newAddress = Address()
        newAddress.recipientName = self.recipientCell.textField.text!
        newAddress.geoCountryId = geoCountrySelected!.geoCountryId
        newAddress.geoProvinceId = geoProvinceSelected!.geoId
        newAddress.geoCityId = geoCitySelected!.geoId
        newAddress.phoneCode = self.phoneCodeCell.textField.text!
        newAddress.phoneNumber = self.phoneNumberCell.textField.text!
        newAddress.address = self.addressCell.textView.text!
        newAddress.postalCode = self.postalCell.textField.text!
        newAddress.isDefault = true
        
        self.showLoading()
        firstly{
            return self.saveNewAddress(newAddress)
            }.then { _ -> Void in
                
                if self.signupMode == .Normal {
                    for viewController: UIViewController in (self.navigationController?.viewControllers)! {
                        if viewController.dynamicType == CheckoutConfirmationViewController.self {
                            if let addressCreated = self.addressCreated {
                                (viewController as! CheckoutConfirmationViewController).selectedAddress = addressCreated
                            }
                            
                            self.navigationController?.popToViewController(viewController, animated: true)
                            return
                        }
                    }
                }
                else if self.signupMode == .Checkout {
                    if let presentingViewController = self.presentingViewController { // as StoreFontViewController
                        presentingViewController.dismissViewControllerAnimated(false, completion: {
                            
                            if (presentingViewController.dynamicType == StorefrontController.self) {
                                
                                let storeFrontVC = presentingViewController as! StorefrontController
                                
                                if let navigation = storeFrontVC.selectedViewController where navigation.dynamicType == UINavigationController.self  {
                                    (navigation as! UINavigationController).navigationBarHidden = false

                                    let checkoutConfirmationViewController = CheckoutConfirmationViewController()
                                    
                                    if let addressCreated = self.addressCreated {
                                        checkoutConfirmationViewController.selectedAddress = addressCreated
                                    }

                                    if let shoppingCartVC = (navigation as! UINavigationController).viewControllers.last as? ShoppingCartViewController {
                                        checkoutConfirmationViewController.listData = shoppingCartVC.listDataSelected()
                                    }
                                    
                                    (navigation as! UINavigationController).pushViewController(checkoutConfirmationViewController, animated: true)
                                }
                            }
                        })
                    }
                }
                else if self.signupMode == .CheckoutSwipeToPay {
                    if let presentingViewController = self.presentingViewController { // as UINavigation
                        presentingViewController.dismissViewControllerAnimated(false, completion: {

                            if let navViewController = presentingViewController as? UINavigationController {
                                navViewController.navigationBarHidden = false

                                let viewController : MmViewController
                                if true {
                                    viewController = IDCardCollectionPageViewController()
                                }
                                else {
                                    viewController = PaymentMethodSelectionViewController()
                                }
                                
                                navViewController.pushViewController(viewController, animated: true)
                            }

                        })
                    }
                    
                }
                
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    
    func listGeoCountry(completion complete:(() -> Void)? = nil) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            GeoService.storefrontCountries({
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            let countries: Array<GeoCountry> = Mapper<GeoCountry>().mapArray(response.result.value)!
                            strongSelf.countryPickerDataSource = countries
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
                })
        }
    }
    
    func listGeoProvince(geoCountryId : Int, completion complete:(() -> Void)? = nil) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            GeoService.storefrontProvinces(geoCountryId, completion:{
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            if let provinces: Array<GeoProvince> = Mapper<GeoProvince>().mapArray(response.result.value) {
                                strongSelf.cityPickerDataSource[0] = provinces
                            }
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
                })
        }
    }
    
    func listGeoCity(geoProvinceId : Int, completion complete:(() -> Void)? = nil) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            GeoService.storefrontCities(geoProvinceId, completion: {
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            if let cities: Array<GeoCity> = Mapper<GeoCity>().mapArray(response.result.value) {
                                strongSelf.cityPickerDataSource[1] = cities
                            }
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
                })
        }
    }
    
    func saveNewAddress(address : Address, completion complete:(() -> Void)? = nil) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            AddressService.save(address, completion: {
                
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            if let address = Mapper<Address>().map(response.result.value) {
                                strongSelf.addressCreated = address
                            }
                            
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
                })
        }
    }
    
    func selectCountryAtRow(row: Int) {
        geoCountrySelected = countryPickerDataSource[row]
        countryCell.textField.text = geoCountrySelected!.geoCountryName
        phoneCodeCell.textField.text = geoCountrySelected!.mobileCode
        cityCell.textField.text = ""
        cityCell.userInteractionEnabled = true
        
        firstly{
            return self.listGeoProvince(geoCountrySelected!.geoCountryId)
            }.then { _ -> Void in
                // get cities
                if self.cityPickerDataSource[0].count > 0 {
                    self.geoProvinceSelected = self.cityPickerDataSource[0][0] as? GeoProvince
                    self.listGeoCity(self.geoProvinceSelected!.geoId)
                }
                // reload picker
                self.cityPicker.reloadAllComponents()
            }.error { _ -> Void in
                Log.error("error")
        }
    }

    //MARK: CollectionView Data Source, Delegate Method
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(AddressAdditionCellID, forIndexPath: indexPath) as! AddressAdditionCell

        cell.placeholder = placeholders[indexPath.row]
        
        switch indexPath.row {
        case IndexPathRow.ReceiverName.rawValue :
            cell.textField.inputView = nil;
            cell.textField.delegate = self
            cell.hiddenTextField(false)
            cell.hiddenArrowView(true)
            self.recipientCell = cell
        case IndexPathRow.Country.rawValue:
            let inputView = AddressInputView (frame: CGRectMake(0 , 0 ,  self.view.bounds.width,  InputViewHeight))
            inputView.pickerView.delegate = self
            inputView.pickerView.dataSource = self
            self.countryPicker = inputView.pickerView
            cell.textField.inputView = inputView
            cell.textField.delegate = self
            self.countryTextField = cell.textField

            cell.hiddenTextField(false)
            cell.hiddenArrowView(false)
            self.countryCell = cell
            
        case IndexPathRow.PhoneCode.rawValue:
            cell.textField.inputView = nil;
            cell.textField.delegate = self
            cell.textField.keyboardType = .PhonePad
            cell.hiddenTextField(false)
            cell.hiddenArrowView(true)
            cell.removeMarginRight()
            cell.textField.tag = phoneCodeTextFieldTag
            self.phoneCodeCell = cell
            
        case IndexPathRow.PhoneNumber.rawValue:
            cell.textField.inputView = nil;
            cell.textField.delegate = self
            cell.textField.keyboardType = .NumberPad
            cell.hiddenTextField(false)
            cell.hiddenArrowView(true)
            cell.removeMarginLeft()
            cell.textField.tag = phoneNumberTextFieldTag
            self.phoneNumberCell = cell
            
        case IndexPathRow.Address.rawValue:
            let inputView = AddressInputView (frame: CGRectMake(0 , 0 ,  self.view.bounds.width,  InputViewHeight))
            inputView.pickerView.delegate = self
            inputView.pickerView.dataSource = self
            self.cityPicker = inputView.pickerView
            cell.textField.inputView = inputView
            cell.textField.delegate = self
            self.provinceTextField = cell.textField
            
            cell.hiddenTextField(false)
            cell.hiddenArrowView(false)
            self.cityCell = cell
            self.cityCell.userInteractionEnabled = false
            
        case IndexPathRow.StreetAddress.rawValue:
            cell.textField.inputView = nil;
            cell.textField.delegate = nil
            cell.hiddenTextField(true)
            cell.hiddenArrowView(true)
            
            cell.textViewBeginEditting = {
                self.activeTextView = cell.textView
                self.activeTextField = nil
            }
            
            cell.textViewEndEditting = {
                self.activeTextView = nil
            }
            
            self.addressCell = cell
            
        case IndexPathRow.PostalZip.rawValue:
            cell.textField.inputView = nil;
            cell.textField.delegate = self
            cell.hiddenTextField(false)
            cell.hiddenArrowView(true)
            self.postalCell = cell
            
        default:
            cell.textField.inputView = nil;
            cell.textField.delegate = self
            cell.hiddenArrowView(true)
        }
        
        return cell;
    }
    
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(DefaultCellID, forIndexPath: indexPath)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            
        return sectionInsets
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch indexPath.row {
            case IndexPathRow.PhoneCode.rawValue:
                return CGSizeMake(PhoneCodeCellWidth, DefaultCellHeight)
            case IndexPathRow.PhoneNumber.rawValue:
                return CGSizeMake(self.view.bounds.width - PhoneCodeCellWidth, DefaultCellHeight)
            case IndexPathRow.StreetAddress.rawValue:
                return CGSizeMake(self.view.bounds.width, AddressCellHeight)
            default:
                return CGSizeMake(self.view.bounds.width, DefaultCellHeight)
            }
            
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            
    }
    
    //MARK: Picker Data Source, Delegate method
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        
        if pickerView == cityPicker {
            return cityPickerDataSource.count
        } else if pickerView == countryPicker {
            return 1
        }
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == cityPicker {
            return cityPickerDataSource[component].count
        } else if pickerView == countryPicker {
            return countryPickerDataSource.count
        }
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == cityPicker {
            if component == 0 {
                if row < cityPickerDataSource[0].count {
                    return (cityPickerDataSource[0][row] as! GeoProvince).geoName
                }
            } else if component == 1 {
                if row < cityPickerDataSource[1].count {
                    return (cityPickerDataSource[1][row] as! GeoCity).geoName
                }
            }
        } else if pickerView == countryPicker {
            return countryPickerDataSource[row].geoCountryName + " (\(countryPickerDataSource[row].mobileCode))"
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == cityPicker {
            
            if cityPickerDataSource[component].count > 0 {
                if let geoProvinceSelected = cityPickerDataSource[component][row] where geoProvinceSelected.dynamicType == GeoProvince.self {
                    
                    self.geoProvinceSelected = geoProvinceSelected as? GeoProvince
                    
                    firstly{
                        return self.listGeoCity((geoProvinceSelected as! GeoProvince).geoId)
                        }.then { _ -> Void in
                            // reload picker
                            self.cityPicker.reloadAllComponents()
                        }.error { _ -> Void in
                            Log.error("error")
                    }
                    
                    cityCell.textField.text = (geoProvinceSelected as! GeoProvince).geoName
                    
                } else if let geoCitySelected = cityPickerDataSource[component][row] where geoCitySelected.dynamicType == GeoCity.self {
                    
                    self.geoCitySelected = geoCitySelected as? GeoCity
                    
                    cityCell.textField.text = self.geoProvinceSelected!.geoName + ", \((geoCitySelected as! GeoCity).geoName)"
                }
            }
            
        } else if pickerView == countryPicker {
            selectCountryAtRow(row)
        }
    }
    
    //MARK: KeyboardWilShow/Hide callback
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.size.height - ConfirmViewHeight, 0.0)
            self.collectionView.contentInset = contentInsets
            self.collectionView.scrollIndicatorInsets = contentInsets;
            
            if let activeTextField = self.activeTextField {
                let rect = self.collectionView.convertRect(activeTextField.bounds, fromView: activeTextField)
                self.collectionView.scrollRectToVisible(rect, animated: false)
            } else if let activeTextView = self.activeTextView {
                let rect = self.collectionView.convertRect(activeTextView.bounds, fromView: activeTextView)
                self.collectionView.scrollRectToVisible(rect, animated: false)
            }
        }
        self.collectionView.addGestureRecognizer(tapGesture!)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.collectionView.contentInset = UIEdgeInsetsZero;
        self.collectionView.scrollIndicatorInsets = UIEdgeInsetsZero;
        self.collectionView.removeGestureRecognizer(tapGesture!)
    }
    
    //MARK: TextField Delegate Method
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.activeTextField = nil
        self.activeTextView = nil
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == phoneCodeTextFieldTag {

            let currentText = textField.text ?? ""
            let prospectiveText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            var isCountryMatching = false
            for country in self.countryPickerDataSource {
                if prospectiveText == country.mobileCode {
                    countryCell.textField.text = country.geoCountryName
                    self.geoCountrySelected = country
                    isCountryMatching =  true
                }
            }
            
            if !isCountryMatching {
                countryCell.textField.text = ""
                self.geoCountrySelected = nil
            }
            
            return prospectiveText.isValidPhone()
        } else if textField.tag == phoneNumberTextFieldTag {

            let currentText = textField.text ?? ""
            let prospectiveText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            return prospectiveText.isNumberic()
        } else {
            return true
        }
    }

    
    func textFieldDidEndEditing(textField: UITextField)  {
        Log.debug("textFieldDidEndEditing \(textField.text)")
        self.activeTextField = nil
        
        if textField == self.countryTextField {
            
            if let inputView = self.countryTextField?.inputView as? AddressInputView {
                self.geoCountrySelected = self.countryPickerDataSource[inputView.pickerView.selectedRowInComponent(0)]
                self.countryCell.textField.text = self.countryPickerDataSource[inputView.pickerView.selectedRowInComponent(0)].geoCountryName
                self.phoneCodeCell.textField.text = self.countryPickerDataSource[inputView.pickerView.selectedRowInComponent(0)].mobileCode
            }
            self.cityCell.textField.text = ""
            self.cityCell.userInteractionEnabled = true
            
            firstly{
                return self.listGeoProvince(self.geoCountrySelected!.geoCountryId)
                }.then { _ -> Void in
                        // get cities
                        if self.cityPickerDataSource[0].count > 0 {
                            self.geoProvinceSelected = self.cityPickerDataSource[0][0] as? GeoProvince
                            self.listGeoCity(self.geoProvinceSelected!.geoId)
                        }
                        // reload picker
                        self.cityPicker.reloadAllComponents()
                    }.error { _ -> Void in
                        Log.error("error")
                }
        }
        else if textField == self.provinceTextField {
            
            if let inputView = self.provinceTextField?.inputView as? AddressInputView {
                let provinceIndex = inputView.pickerView.selectedRowInComponent(0)
                
                if provinceIndex != -1 && provinceIndex < self.cityPickerDataSource[0].count {
                    
                    if let geoProvinceSelected = self.cityPickerDataSource[0][provinceIndex] where geoProvinceSelected.dynamicType == GeoProvince.self {
                        
                        self.geoProvinceSelected = geoProvinceSelected as? GeoProvince
                        
                        firstly{
                            return self.listGeoCity((geoProvinceSelected as! GeoProvince).geoId)
                            }.then { _ -> Void in
                                // reload picker
                                self.cityPicker.reloadAllComponents()
                            }.error { _ -> Void in
                                Log.error("error")
                        }
                        
                        self.cityCell.textField.text = (geoProvinceSelected as! GeoProvince).geoName
                        
                    }
                    
                    let cityIndex = inputView.pickerView.selectedRowInComponent(1)
                    
                    if cityIndex != -1 && cityIndex < self.cityPickerDataSource[1].count {
                        
                        if let geoCitySelected = self.cityPickerDataSource[1][cityIndex] where geoCitySelected.dynamicType == GeoCity.self {
                            
                            self.geoCitySelected = geoCitySelected as? GeoCity
                            
                            self.cityCell.textField.text = self.geoProvinceSelected!.geoName + ", \((geoCitySelected as! GeoCity).geoName)"
                        }
                    }
                }

            }
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.activeTextField = textField
        self.activeTextView = nil
        return true
    }
}
