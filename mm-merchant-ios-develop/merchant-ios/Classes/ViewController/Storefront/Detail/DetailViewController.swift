//
//  DetailViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 27/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import SwiftDate
import AlamofireImage


class DetailViewController : MmCartViewController, BuyDelegate, MHFacebookImageViewerDatasource {
    var style = Style()
    var suggestStyles : [Style] = []
    var styleFilter = StyleFilter()
    var styleCode: String?
    var defaultImage : UIImage! = UIImage(named: "holder")!
    var bought : Bool = false
    var buySwipe : Bool = false
    var point = CGPointZero
    var featureCollectionView : UICollectionView?
    var userCollectionView : UICollectionView?
    var suggestColectionView : UICollectionView?
    var colorIndexSelected : Int = -1
    var sizeIndexSelected : Int = -1
    var selectedSizeId = -1
    var selectedColorId = -1
    var currentPage = 0
    var buyView: BuyCell?
    var filteredColorImageList : [Img] = []
    
    private final let CellId : String = "Cell"
    private final let FeatureCellId : String = "FeatureCell"
    private final let NameCellId : String = "NameCell"
    private final let BuyCellId : String = "BuyCell"
    private final let MenuCellId : String = "MenuCell"
    private final let ColorCellId : String = "ColorCell"
    private final let ColorCollectionCellId : String = "ColorCollectionCell"
    private final let DescCellId : String = "DescCell"
    private final let CommentCellId : String = "CommentCell"
    private final let RecommendCellId : String = "RecommendCell"
    private final let DescriptionImageCellId : String = "DescriptionImageCell"
    private final let DescCollectCellId : String = "DescCollectCell"
    private final let UserCellId : String = "UserCell"
    private final let UserCollectionCellId : String = "UserCollectionCell"
    private final let OutfitCellId : String = "OutfitCell"
    private final let SuggestionCellId : String = "SuggestionCell"
    private final let ScoreCellId : String = "ScoreCell"
    private final let CollectCellId : String = "CollectCell"
    private final let FeatureCollectCellId : String = "FeatureCollectCell"
    private final let BuyCellHeight : CGFloat = 74
    private final let ColorCellHeight : CGFloat = 70
    private final let DescCellHeight : CGFloat = 120
    private final let ParameterCellHeight : CGFloat = 60
    private final let CommentCellHeight : CGFloat = 350
    private final let RecommendCellHeight : CGFloat = 25
    private final let DescriptionImageCellHeight : CGFloat = 60
    private final let UserCellHeight : CGFloat = 50
    private final let DetailImageAspectRatio : CGFloat = 1.2
    private final let OutfitCellHeight : CGFloat = 530
    private final let SuggestionCellHeight : CGFloat = 330
    private final let ScoreCellHeight : CGFloat = 150
    private final let MarginLeftImageBar : CGFloat = 20
    private final let MarginRightImageBar : CGFloat = 135
    private final let WidthImageBar : CGFloat = 30
    private final let HeightImageBar : CGFloat = 45
    private final let WidthItemBar : CGFloat = 30
    private final let HeightItemBar : CGFloat = 25
    private final let HeaderHeight : CGFloat = 20
    
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: Constants.Margin.Left, bottom: 0.0, right: Constants.Margin.Right)
    private final let DefaultUIEdgeInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView!.backgroundColor = UIColor.whiteColor()
        //register all cells
        self.collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: CellId)
        self.collectionView.registerClass(FeatureImageCell.self, forCellWithReuseIdentifier: FeatureCellId)
        self.collectionView.registerClass(NameCell.self, forCellWithReuseIdentifier: NameCellId)
        self.collectionView.registerClass(MenuCell.self, forCellWithReuseIdentifier: MenuCellId)
        self.collectionView.registerClass(ColorCell.self, forCellWithReuseIdentifier: ColorCellId)
        self.collectionView.registerClass(DescCell.self, forCellWithReuseIdentifier: DescCellId)
        self.collectionView.registerClass(CommentCell.self, forCellWithReuseIdentifier: CommentCellId)
        self.collectionView.registerClass(RecommendedCell.self, forCellWithReuseIdentifier: RecommendCellId)
        self.collectionView.registerClass(DescriptionImageCell.self, forCellWithReuseIdentifier: DescriptionImageCellId)
        self.collectionView.registerClass(UserCell.self, forCellWithReuseIdentifier: UserCellId)
        self.collectionView.registerClass(OutfitCell.self, forCellWithReuseIdentifier: OutfitCellId)
        self.collectionView.registerClass(SuggestionCell.self, forCellWithReuseIdentifier: SuggestionCellId)
        self.collectionView.registerClass(ScoreCell.self, forCellWithReuseIdentifier: ScoreCellId)
        self.collectionView.registerClass(ColorCollectionCell.self, forCellWithReuseIdentifier: ColorCollectionCellId)
        self.collectionView.registerClass(DescCollectCell.self, forCellWithReuseIdentifier: DescCollectCellId)
        self.collectionView.registerClass(HeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")
        
        if !self.styleFilter.colors.isEmpty {
            self.selectedColorId = self.styleFilter.colors[0].colorId
            let colorList = self.style.colorList.filter(){$0.colorId == selectedColorId}
            let colorKey = colorList[0].colorKey
            let colorImages = self.style.colorImageList
            filteredColorImageList = colorImages.filter(){$0.colorKey.lowercaseString == colorKey.lowercaseString}
            filteredColorImageList = filteredColorImageList.sort(){$0.position < $1.position}
            self.reloadAllData()
            self.featureCollectionView?.scrollsToTop = true
        }
        
        self.setupNavigationBarButtons()
        
        let initLayout = {
            self.reloadPrice()
            self.setupNavigationBarTitle()
            self.reloadAllData()
        }
        
        if let styleCode = self.styleCode {
            self.showLoading()
            firstly{
                return self.searchStyleWithStyleCode(styleCode)
                }.then { _ -> Void in
                    initLayout()
                }.always {
                    self.stopLoading()
                }.error { _ -> Void in
                    Log.error("error")
            }
        }
        else {
            initLayout()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadAllData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if buyView == nil {
            initBuyView()
        }
    }
    
    //MARK: Search Style
    func searchStyleWithStyleCode(styleCode : String) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchStyleByStyleCode(styleCode){ [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        
                        if let styleResponse = Mapper<SearchResponse>().map(response.result.value) {
                            
                            if let styles = styleResponse.pageData {
                                if styles.count > 0 {
                                    strongSelf.style = styles[0]
                                }
                            }
                        }
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                    
                }
            }
        }
    }
    
    //MARK: CollectionView Data Source, Delegate Method
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        switch collectionView {
        case self.collectionView!:
            return 13
        case self.featureCollectionView!:
            return 1
        case self.userCollectionView!:
            return 1
        case self.suggestColectionView!:
            return 1
        default:
            return 1
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.collectionView!:
            switch section {
            case 2:
                return self.style.colorList.count
            case 3:
                return self.style.sizeList.count
            case 8:
                return self.style.featuredImageList.count
            default:
                return 1
                
            }
        case self.featureCollectionView!:
            return self.style.featuredImageList.count + self.filteredColorImageList.count
            
        case self.userCollectionView!:
            return 7
        case self.suggestColectionView!:
            return self.suggestStyles.count
        default:
            return 0
        }
    }
    
    //MARK: Draw Cell
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
            
        case self.collectionView!:
            
            switch (indexPath.section){
            case 0: // Image Default Cell
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FeatureCellId, forIndexPath: indexPath) as! FeatureImageCell
                cell.featureCollectionView!.dataSource = self
                cell.featureCollectionView!.delegate = self
                self.featureCollectionView = cell.featureCollectionView
                self.featureCollectionView?.registerClass(FeatureCollectionCell.self, forCellWithReuseIdentifier: FeatureCollectCellId)
                if self.style.isWished() {
                    cell.heartImageView.image = UIImage(named: "like_on")
                } else {
                    cell.heartImageView.image = UIImage(named: "like_rest")
                }
                cell.heartImageView.userInteractionEnabled = true
                cell.heartImageView.addGestureRecognizer(UITapGestureRecognizer(target:self, action:Selector("likeTapped:")))
                
                
                cell.heartImageView.isAccessibilityElement = true
                cell.heartImageView.accessibilityIdentifier = "heart_imageview_0"
                
                cell.pageControl.numberOfPages = self.style.featuredImageList.count + self.filteredColorImageList.count
                cell.pageControl.currentPage = self.currentPage
                
                return cell
            case 1:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(NameCellId, forIndexPath: indexPath) as! NameCell
                cell.setImage(self.style.brandSmallLogoImage, imageCategory: .Brand)
                cell.nameLabel.text = self.style.skuName
                return cell
                
            case 2:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ColorCollectionCellId, forIndexPath: indexPath) as! ColorCollectionCell
                let filteredColorImageList = self.style.colorImageList.filter({$0.colorKey.lowercaseString == self.style.colorList[indexPath.row].colorKey.lowercaseString})
                if filteredColorImageList.isEmpty{
                    cell.imageView.af_setImageWithURL(ImageURLFactory.get(self.style.colorList[indexPath.row].colorImage,category: .Color), placeholderImage : UIImage(named: "holder"))
                } else {
                    cell.imageView.af_setImageWithURL(ImageURLFactory.get((filteredColorImageList[0].imageKey)), placeholderImage : UIImage(named: "holder") )
                }
                
                cell.label.text = ""
                if indexPath.row == colorIndexSelected{
                    cell.border()
                } else {
                    cell.unBorder()
                }
                
                var filteredSkuList = self.style.skuList.filter({$0.sizeId == self.selectedSizeId})
                filteredSkuList = filteredSkuList.filter({$0.colorId == self.style.colorList[indexPath.row].colorId})
                if filteredSkuList.isEmpty && self.sizeIndexSelected != -1 {
                    cell.cross()
                    self.style.colorList[indexPath.row].isValid = false
                } else {
                    cell.unCross()
                    self.style.colorList[indexPath.row].isValid = true
                    
                }
                
                return cell
                
            case 3:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ColorCollectionCellId, forIndexPath: indexPath) as! ColorCollectionCell
                cell.imageView.image = nil
                cell.label.text = self.style.sizeList[indexPath.row].sizeName
                if indexPath.row == sizeIndexSelected{
                    cell.border()
                } else {
                    cell.unBorder()
                }
                var filteredSkuList = self.style.skuList.filter({$0.colorId == self.selectedColorId})
                filteredSkuList = filteredSkuList.filter({$0.sizeId == self.style.sizeList[indexPath.row].sizeId})
                if filteredSkuList.isEmpty && self.colorIndexSelected != -1 {
                    cell.cross()
                    self.style.sizeList[indexPath.row].isValid = false
                    
                } else {
                    cell.unCross()
                    self.style.sizeList[indexPath.row].isValid = true
                }
                
                return cell
            case 4:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(DescCellId, forIndexPath: indexPath) as! DescCell
                cell.descLabel.text = self.style.skuDesc
                return cell
            case 5:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CommentCellId, forIndexPath: indexPath) as! CommentCell
                cell.userImageView.image = UIImage(named:"user00.jpg")
                cell.textLabel.text = String.localize("LB_CA_PROD_REVIEW") + " (112" + String.localize("LB_CA_COMMENT") + ")"
                cell.userNameLabel.text = "岺丽香"
                cell.commentLabel.text = "不錯，衣服挺好的，我挺喜歡的"
                cell.dateLabel.text = self.style.availableFrom.toString()
                cell.firstImageView.image = UIImage(named:"comment_demo1.jpg")
                cell.secondImageView.image = UIImage(named:"comment_demo2.jpg")
                cell.arrowView.image = UIImage(named: "arrow_close")
                cell.expandView.hidden = true
                cell.arrowView.frame = CGRect(x: cell.bounds.maxX - 35, y: cell.bounds.maxY - 45, width: 17, height: 9)
                return cell
            case 6:
                
                // CHANGE VIEW
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(UserCellId, forIndexPath: indexPath) as! UserCell
                cell.userCollectionView!.dataSource = self
                cell.userCollectionView!.delegate = self
                self.userCollectionView = cell.userCollectionView
                self.userCollectionView?.registerClass(UserCollectionCell.self, forCellWithReuseIdentifier: UserCollectionCellId)
                return cell
            case 7:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(RecommendCellId, forIndexPath: indexPath) as! RecommendedCell
                cell.numberLabel.text = "112"
                return cell
            case 8:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(DescCollectCellId, forIndexPath: indexPath) as! DescCollectCell
                cell.setImage(self.style.featuredImageList[indexPath.row].imageKey)
                cell.descImageView.setupImageViewerWithDatasource(self, initialIndex: indexPath.row, parentTag:indexPath.section, onOpen: { () -> Void in }, onClose: { () -> Void in })
                return cell
            case 9:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(OutfitCellId, forIndexPath: indexPath) as! OutfitCell
                cell.logoView.image = UIImage(named: "brooksbrotherlogo.jpg")
                cell.outfitImageView.image = UIImage(named: "outfit_demo")
                cell.textLabel.text = (self.style.brandName) + String.localize("LB_CA_STORE_RCMD")
                let formatter = NSNumberFormatter()
                formatter.numberStyle = .CurrencyStyle
                formatter.locale = NSLocale(localeIdentifier: "zh_Hans_CN")
                formatter.maximumFractionDigits = 0
                if self.style.priceSale > 0 {
                    cell.buyView.priceLabel.text = formatter.stringFromNumber((self.style.priceSale))
                } else {
                    cell.buyView.priceLabel.text = formatter.stringFromNumber((self.style.priceRetail))
                }
                cell.buyView.priceLabel.font = UIFont.boldSystemFontOfSize(14.0)
                cell.buyView.buyDelegate = self
                cell.likeNumberLabel.text = "219"
                cell.commentNumberLabel.text = "12"
                cell.brandLabel.text = self.style.brandName
                if self.style.isWished() {
                    cell.heartImageView.image = UIImage(named: "like_on")
                } else {
                    cell.heartImageView.image = UIImage(named: "like_rest")
                }
                cell.heartImageView.userInteractionEnabled = true
                
                cell.heartImageView.accessibilityIdentifier = "heart_imageview_9"
                
                cell.heartImageView.addGestureRecognizer(UITapGestureRecognizer(target:self, action:Selector("likeTapped:")))
                
                return cell
            case 10:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(SuggestionCellId, forIndexPath: indexPath) as! SuggestionCell
                cell.suggestCollectionView!.dataSource = self
                cell.suggestCollectionView!.delegate = self
                self.suggestColectionView = cell.suggestCollectionView
                self.suggestColectionView?.registerClass(CollectCell.self, forCellWithReuseIdentifier: CollectCellId)
                cell.borderView.hidden = true;
                return cell
                
            case 11:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ScoreCellId, forIndexPath: indexPath) as! ScoreCell
                cell.serviceRatingLabel.text = "5.0"
                cell.styleRatingLabel.text = "5.0"
                cell.logisticRatingLabel.text = "5.0" ///LB_CA_PROD_REVIEW
                cell.prodReviewLabel.text = String.localize("LB_CA_PROD_REVIEW")
                return cell
                
            default:
                return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
                
            }
        case self.featureCollectionView!:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FeatureCollectCellId, forIndexPath: indexPath) as! FeatureCollectionCell
            if colorIndexSelected < 0 && selectedColorId < 0 {
                cell.setImage(self.style.featuredImageList[indexPath.row].imageKey)
            } else {
                if indexPath.row < self.filteredColorImageList.count {
                    cell.setImage(self.filteredColorImageList[indexPath.row].imageKey)
                } else {
                    cell.setImage(self.style.featuredImageList[indexPath.row - self.filteredColorImageList.count].imageKey)
                }
            }
            cell.featureImageView.setupImageViewerWithDatasource(self, initialIndex: indexPath.row, parentTag:indexPath.section, onOpen: { () -> Void in }, onClose: { () -> Void in })
            return cell
            
        case self.userCollectionView!:
            
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(UserCollectionCellId, forIndexPath: indexPath) as! UserCollectionCell
            let fileName : String = "user0" + String(indexPath.row) + ".jpg"
            cell.userImageView.image = UIImage(named: fileName)
            return cell
            
        case self.suggestColectionView!:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CollectCellId, forIndexPath: indexPath) as! CollectCell
            if (self.suggestStyles.count > 0){
                cell.setBrandImage(self.suggestStyles[indexPath.row].brandSmallLogoImage)
                cell.setProductImage(self.suggestStyles[indexPath.row].imageDefault)
                cell.nameLabel.text = self.suggestStyles[indexPath.row].skuName
                cell.nameLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
                cell.nameLabel.numberOfLines = 2
                cell.nameLabel.textAlignment = .Center
                cell.setPrice(self.suggestStyles[indexPath.row])
                if self.suggestStyles[indexPath.row].isWished() {
                    cell.heartImageView.image = UIImage(named: "like_on")
                } else {
                    cell.heartImageView.image = UIImage(named: "like_rest")
                }
                cell.heartImageView.userInteractionEnabled = true
                
                cell.heartImageView.accessibilityIdentifier = "heart_imageview_suggest"
                
                cell.heartImageView.addGestureRecognizer(UITapGestureRecognizer(target:self, action:Selector("likeTapped:")))
                
            }
            return cell
            
        default:
            return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
        }
    }
    
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
        return cell
    }
    
    //MARK: Header and Footer View for collection view
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        switch collectionView {
        case self.collectionView:
            switch section {
            case 2:
                if self.style.colorList.count == 0 {
                    return CGSizeMake(0,0)
                }
                return CGSizeMake(self.view.frame.width, HeaderHeight)
            case 3:
                if self.style.sizeList.count == 0 {
                    return CGSizeMake(0,0)
                }
                return CGSizeMake(self.view.frame.width, HeaderHeight)
            default:
                return CGSizeMake(0,0)
                
            }
        default:
            return CGSizeMake(0,0)
        }
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if collectionView == self.collectionView{
            switch kind {
                
            case UICollectionElementKindSectionHeader:
                
                let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Header", forIndexPath: indexPath) as! HeaderView
                switch indexPath.section{
                case 2:
                    headerView.label.text = String.localize("LB_CA_COLOUR")
                    break
                case 3:
                    headerView.label.text = String.localize("LB_CA_SIZE")
                    break
                default:
                    break
                    
                }
                return headerView
                
            default:
                
                assert(false, "Unexpected element kind")
                let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Header", forIndexPath: indexPath) as! HeaderView
                return headerView
            }
        } else{
            assert(false, "Unexpected collection view requesting header view")
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Header", forIndexPath: indexPath) as! HeaderView
            return headerView
            
        }
    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            
            switch collectionView {
                
            case self.collectionView!:
                return DefaultUIEdgeInsets
            case self.featureCollectionView!:
                return DefaultUIEdgeInsets
                
            case self.userCollectionView!:
                return UIEdgeInsets(top: 0.0, left: (self.view.frame.width/2 - (self.view.frame.width/7)*3 + 6*4 ) , bottom: 0.0, right: 0.0)
            case self.suggestColectionView!:
                return sectionInsets
            default:
                return DefaultUIEdgeInsets
                
            }
            // return sectionInsets
            
    }
    //MARK: Item Size Delegate for Collection View
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
                
            case self.collectionView!:
                switch (indexPath.section){
                case 0:
                    return CGSizeMake(self.view.frame.size.width, self.view.frame.size.width * Constants.Ratio.ProductImageHeight)
                case 1:
                    return CGSizeMake(self.view.frame.size.width, Constants.Value.BrandImageHeight + 45)
                    
                case 2:
                    return CGSizeMake(ColorCellHeight,ColorCellHeight)
                case 3:
                    return CGSizeMake(ColorCellHeight,ColorCellHeight)
                case 4:
                    return CGSizeMake(self.view.frame.size.width, DescCellHeight)
                case 5:
                    return CGSizeMake(self.view.frame.size.width, CommentCellHeight)
                case 6:
                    return CGSizeMake(self.view.frame.size.width, UserCellHeight)
                case 7:
                    return CGSizeMake(self.view.frame.size.width, RecommendCellHeight)
                case 8:
                    return CGSizeMake(self.view.frame.size.width,self.view.frame.size.width * Constants.Ratio.ProductImageHeight)
                case 9:
                    return CGSizeMake(self.view.frame.size.width, OutfitCellHeight)
                case 10:
                    return CGSizeMake(self.view.frame.size.width, self.getSuggestionCellWidth() * Constants.Ratio.ProductImageHeight + Constants.Value.ProductBottomViewHeight)
                case 11:
                    return CGSizeMake(self.view.frame.size.width, ScoreCellHeight)
                default:
                    return CGSizeMake(self.view.frame.size.width, 0)
                    
                }
            case self.featureCollectionView!:
                return CGSizeMake(self.view.frame.size.width , self.view.frame.size.width * Constants.Ratio.ProductImageHeight)
                
                
            case self.userCollectionView!:
                return CGSizeMake(self.view.frame.size.width / 7 - 20, UserCellHeight )
                
                
            case self.suggestColectionView!:
                let width = self.getSuggestionCellWidth()
                return CGSizeMake(width, width * Constants.Ratio.ProductImageHeight +  Constants.Value.ProductBottomViewHeight)
            default:
                return CGSizeMake(self.view.frame.size.width, 40)
                
            }
    }
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch collectionView {
            
        case self.collectionView!:
            switch section {
            case 8:
                return Constants.LineSpacing.ImageCell
            default:
                return 0.0
                
            }
        case self.featureCollectionView!:
            return 0.0
        case self.userCollectionView!:
            return 6.0
        case self.suggestColectionView!:
            return Constants.LineSpacing.ImageCell
        default:
            return 0.0
            
        }
    }
    
    //MARK: Collection View Delegate methods
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            
            switch collectionView {
                
            case self.collectionView!:
                switch indexPath.section {
                case 2:
                    if self.style.colorList[indexPath.row].isValid {
                        colorIndexSelected = indexPath.row
                        selectedColorId = style.colorList[indexPath.row].colorId
                        filteredColorImageList = style.colorImageList.filter({$0.colorKey.lowercaseString == style.colorList[indexPath.row].colorKey.lowercaseString})
                        filteredColorImageList = filteredColorImageList.sort(){$0.position < $1.position}
                        reloadAllData()
                        featureCollectionView?.scrollsToTop = true
                    }
                    break
                case 3:
                    if self.style.sizeList[indexPath.row].isValid {
                        
                        self.sizeIndexSelected = indexPath.row
                        self.selectedSizeId = self.style.sizeList[indexPath.row].sizeId
                        self.reloadAllData()
                        break
                    }
                default:
                    break
                }
            case self.featureCollectionView!:
                break
            case self.userCollectionView!:
                break
            case self.suggestColectionView!:
                let detailViewController = DetailViewController()
                detailViewController.style = self.suggestStyles[indexPath.row]
                detailViewController.suggestStyles = self.suggestStyles
                self.navigationController?.pushViewController(detailViewController, animated: true)
                break
            default:
                break
            }
            
            collectionView.reloadData()
            
    }
    
    func reloadAllData(){
        self.featureCollectionView?.reloadData()
        self.userCollectionView?.reloadData()
        self.suggestColectionView?.reloadData()
        self.collectionView?.reloadData()
    }
    
    func reloadPrice() {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = NSLocale(localeIdentifier: "zh_Hans_CN")
        formatter.maximumFractionDigits = 0
        
        if let buyView = self.buyView {
            if self.style.priceSale > 0 {
                buyView.priceLabel.text = formatter.stringFromNumber((self.style.priceSale))
            } else {
                buyView.priceLabel.text = formatter.stringFromNumber((self.style.priceRetail))
            }
        }
    }
    
    func navigateToshoppingCart() {
        let shoppingcart = ShoppingCartViewController()
        self.navigationController?.pushViewController(shoppingcart, animated: true)
    }
    
    func initBuyView () {
        let frame = CGRectMake( 0, self.view.frame.height - BuyCellHeight  , self.view.frame.width, BuyCellHeight)
        buyView = BuyCell.init(frame: frame)
        buyView?.backgroundColor = UIColor.whiteColor()
        buyView?.buyDelegate = self
        self.view.addSubview(self.buyView!)
        reloadPrice()
    }
    
    func buy(){
        
        if (self.style.colorList.count == 0 || self.selectedColorId != -1) && (self.style.sizeList.count == 0 || self.selectedSizeId != -1) {
            
            let sku = self.style.searchSku(self.selectedSizeId, colorId: self.selectedColorId)
            
            guard (sku != nil) else {
                // missing handling
                self.showFailPopupWithText(String.localize("Fail to add shopping cart: Missing Sku"))
                return
            }
            
            firstly{
                self.addCartItem(sku!.skuId, qty: 1)
                }.then { _ -> Void in
                    self.showSuccessPopupWithText(String.localize("LB_CA_ADD2CART_SUCCESS"))
                    self.updateButtonCartState()
                }.error { _ -> Void in
                    self.showFailPopupWithText(String.localize("LB_CA_ADD2CART_FAIL"))
                    Log.debug("error")
            }
            
        } else {
            
            var colorKey = ""
            if colorIndexSelected >= 0 && selectedColorId >= 0  {
                if let filteredColorImage = self.filteredColorImageList.first {
                    colorKey = filteredColorImage.colorKey
                }
            }
            
            let checkOutViewController = CheckOutViewController(
                style: self.style,
                selectedColorId: self.selectedColorId,
                selectedSizeId: self.selectedSizeId,
                redDotButton: self.buttonCart,
                colorKey: colorKey
            )
            checkOutViewController.checkOutMode = CheckOutMode.StyleOnly
            
            self.presentViewController(checkOutViewController, animated: false, completion: nil)
            checkOutViewController.didDismissHandler = {
                self.updateButtonCartState()
            }
            self.reloadAllData()
        }
    }
    
    //MARK: Like Count Action
    func likeTapped(sender : UIGestureRecognizer){
        Log.debug("heart clicked!")
    
        let point : CGPoint = sender.view!.convertPoint(CGPointZero, toView:self.suggestColectionView)
        var indexPath = self.suggestColectionView?.indexPathForItemAtPoint(point)
        var aStyle : Style?
        
        if indexPath?.section != nil && indexPath?.row != nil {
            aStyle = self.suggestStyles[indexPath!.row]
        } else  {
            let point : CGPoint = sender.view!.convertPoint(CGPointZero, toView:self.collectionView)
            indexPath = self.collectionView!.indexPathForItemAtPoint(point)
            if indexPath?.section != nil && indexPath?.row != nil {
                aStyle = self.style
            }
        }
        if let style = aStyle {
            if style.isWished() {
                let cartItemId = CacheManager.sharedManager.cartItemIdForStyle(style)
                firstly{
                    return self.removeWishlistItem(cartItemId)
                    }.then { _ -> Void in
                        // reload UI
                        self.reloadAllData()
                        self.updateButtonWishlistState()
                    }.error { _ -> Void in
                        Log.error("error")
                }
            } else {
                
                var colorKey : String? = nil
                if  !(sender.view?.superview is CollectCell) {
                    if colorIndexSelected >= 0 && selectedColorId >= 0  {
                        if let filteredColorImage = self.filteredColorImageList.first {
                            colorKey = filteredColorImage.colorKey
                        }
                    }
                }
                
                firstly{
                
                    return self.addWishlistItem(style.merchantId, styleCode: style.styleCode, skuId: NSNotFound, colorKey: colorKey)
                    
                    }.then { _ -> Void in
                        
                        let wishlistAnimation = WishlistAnimation(heartImage: sender.view as! UIImageView, redDotButton: self.buttonWishlist)
                        wishlistAnimation.showAnimation(completion: { () -> Void in
                            // reload UI
                            self.reloadAllData()
                            self.updateButtonWishlistState()
                        })
                        
                    }.error { _ -> Void in
                        Log.error("error")
                }
            }
        }
    }
    
    //MARK: Scroll View Method to control page control
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scrollView == self.featureCollectionView{
            self.currentPage = Int(self.featureCollectionView!.contentOffset.x / self.featureCollectionView!.frame.size.width)
            self.reloadAllData()
        }
    }
    
    
    func initButtonBar(imageName: String, selectorName: String, size:CGSize,left: CGFloat, right: CGFloat) -> UIBarButtonItem
    {
        let button: UIButton = UIButton()
        button.setImage(UIImage(named: imageName), forState: .Normal)
        button.frame = CGRectMake(0, 0, size.width, size.height)
        button.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: left, bottom: 0, right: right)
        button .addTarget(self, action:Selector(selectorName), forControlEvents: UIControlEvents.TouchUpInside)
        
        let temp:UIBarButtonItem = UIBarButtonItem()
        temp.customView = button
        return temp
    }
    
    func likeNaviBarTapped(sender: UIBarButtonItem){
        let wishlist = WishListCartViewController()
        self.navigationController?.pushViewController(wishlist, animated: true)
    }
    
    func setupNavigationBarButtons() {
        setupNavigationBarCartButton()
        setupNavigationBarWishlistButton()
        
        var rightButtonItems = [UIBarButtonItem]()
        rightButtonItems.append(UIBarButtonItem(customView: buttonCart!))
        rightButtonItems.append(UIBarButtonItem(customView: buttonWishlist!))
        
        
        buttonCart!.addTarget(self, action: "navigateToshoppingCart", forControlEvents: .TouchUpInside)
        buttonWishlist!.addTarget(self, action: "likeNaviBarTapped:", forControlEvents: .TouchUpInside)
        
        self.navigationItem.rightBarButtonItems = rightButtonItems

        self.createBackButton()
    }
    
    func setupNavigationBarTitle() {
        self.title = String.localize("LB_DETAILS")
        let viewImageTile = UIImageView(frame: CGRectMake(0, 0, 150, 40))
        viewImageTile.contentMode = .ScaleAspectFit
        
        self.navigationItem.titleView = viewImageTile
        
        let filter = AspectScaledToFitSizeFilter(
            size: viewImageTile.frame.size
        )
        viewImageTile.af_setImageWithURL(ImageURLFactory.get(self.style.brandHeaderLogoImage, category: .Brand), placeholderImage : UIImage(named: "spacer"), filter: filter)
    }
    
    func getSuggestionCellWidth() -> CGFloat {
        return (self.view.frame.size.width - (Constants.Margin.Left + Constants.Margin.Right + Constants.LineSpacing.ImageCell)) / 2
    }
    //MARK: Buy Delegate
    func doBuy(swipe isSwipe: Bool?) {
        if let swipe = isSwipe where swipe == true {
            
            let navigationController = UINavigationController()
            let checkOutViewController = CheckOutViewController(style: self.style, redDotButton: self.buttonCart)
            checkOutViewController.isSwipeToPay = true
            navigationController.viewControllers = [checkOutViewController]
            navigationController.modalPresentationStyle = .OverFullScreen
            
            self.presentViewController(navigationController, animated: false, completion: nil)
            
            checkOutViewController.didDismissHandler = {
                self.updateButtonCartState()
                self.updateButtonWishlistState()
            }

        }
        else {
            self.buy()
        }
        
    }
    
    // config tab bar
    override func shouldHideTabBar() -> Bool {
        return  true
    }
    
    override func collectionViewBottomPadding() -> CGFloat {
        return BuyCellHeight
    }
    
    func numberImagesForImageViewer(imageViewer :MHFacebookImageViewer) -> NSInteger {
        if imageViewer.parentTag == 0 {
            return self.style.featuredImageList.count + self.filteredColorImageList.count;
        }
        else {
            return self.style.featuredImageList.count;
        }
    }
    
    func imageURLAtIndex(index: NSInteger, imageViewer:MHFacebookImageViewer ) -> NSURL {
        var imageKey = ""
        if imageViewer.parentTag == 0 {
            if index < self.filteredColorImageList.count {
                imageKey = self.filteredColorImageList[index].imageKey
            } else {
                imageKey = self.style.featuredImageList[index - self.filteredColorImageList.count].imageKey
            }
        }
        else {
            imageKey = self.style.featuredImageList[index].imageKey
        }
        Log.debug("imageURLAtIndex : \(index)")
        return ImageURLFactory.get(imageKey)
        
    }
    
    func imageDefaultAtIndex(index: NSInteger, imageViewer:MHFacebookImageViewer ) -> UIImage {
        return UIImage(named: "holder")!
    }
    
}

