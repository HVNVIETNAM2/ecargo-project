//
//  ScoreCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 3/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class ScoreCell : UICollectionViewCell {
    var styleRatingImageView = UIImageView()
    var serviceRatingImageView = UIImageView()
    var logisticRatingImageView = UIImageView()
    var styleTextLabel = UILabel()
    var serviceTextLabel = UILabel()
    var logisticTextLabel = UILabel()
    var styleRatingLabel = UILabel()
    var serviceRatingLabel = UILabel()
    var logisticRatingLabel = UILabel()
    var prodReviewLabel = UILabel()
    var borderView = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        styleRatingImageView.image = UIImage(named:"ico_score1")
        addSubview(styleRatingImageView)
        styleTextLabel.formatSmall()
        styleTextLabel.text = String.localize("LB_CA_PROD_DESC")
        addSubview(styleTextLabel)
        styleRatingLabel.formatSmall()
        styleRatingLabel.textAlignment = .Left
        addSubview(styleRatingLabel)
        serviceRatingImageView.image = UIImage(named:"ico_score2")
        addSubview(serviceRatingImageView)
        serviceTextLabel.formatSmall()
        serviceTextLabel.text = String.localize("LB_CA_CUST_SERVICE")
        addSubview(serviceTextLabel)
        serviceRatingLabel.formatSmall()
        serviceRatingLabel.textAlignment = .Left
        addSubview(serviceRatingLabel)
        logisticRatingImageView.image = UIImage(named:"ico_score3")
        addSubview(logisticRatingImageView)
        logisticTextLabel.formatSmall()
        logisticTextLabel.text = String.localize("LB_CA_SHIPMENT_RATING")
        addSubview(logisticTextLabel)
        logisticRatingLabel.formatSmall()
        logisticRatingLabel.textAlignment = .Left
        addSubview(logisticRatingLabel)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        prodReviewLabel.formatSize (14)
        prodReviewLabel.textAlignment = .Center
        addSubview(prodReviewLabel)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        styleRatingImageView.frame = CGRect(x: bounds.minX + 43, y: bounds.minY + 40, width: 40, height: 40)
        styleTextLabel.frame = CGRect(x: bounds.minX + 23, y: bounds.minY + 80, width: 60, height: 30)
        styleRatingLabel.frame = CGRect(x: bounds.minX + 83, y: bounds.minY + 80, width: 60, height: 30)
        serviceRatingImageView.frame = CGRect(x: bounds.width / 3 + 43, y: bounds.minY + 40, width: 40, height: 40)
        serviceTextLabel.frame = CGRect(x: bounds.width / 3 + 23, y: bounds.minY + 80, width: 60, height: 30)
        serviceRatingLabel .frame = CGRect(x: bounds.width / 3 + 83, y: bounds.minY + 80, width: 60, height: 30)
        logisticRatingImageView.frame = CGRect(x: bounds.width / 3 * 2 + 43, y: bounds.minY + 40, width: 40, height: 40)
        logisticTextLabel.frame = CGRect(x: bounds.width / 3 * 2 + 23, y: bounds.minY + 80, width: 60, height: 30)
        logisticRatingLabel.frame = CGRect(x: bounds.width / 3 * 2 + 83, y: bounds.minY + 80, width: 60, height: 30)
        borderView.frame = CGRect(x: bounds.minX, y: bounds.minY + 19 , width: bounds.width, height: 1)
        prodReviewLabel.frame = CGRect(x: bounds.midX - 35, y: bounds.minY - 11 , width: 70, height: 30)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}