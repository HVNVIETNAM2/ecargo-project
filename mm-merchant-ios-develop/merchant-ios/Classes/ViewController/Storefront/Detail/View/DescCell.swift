//
//  DescCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 2/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class DescCell : UICollectionViewCell{
    var descLabel = UILabel()
    var upperBorderView = UIView()
    var lowerBorderView = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        descLabel.formatSize(14)
        addSubview(descLabel)
        upperBorderView.backgroundColor = UIColor.secondary1()
        addSubview(upperBorderView)
        lowerBorderView.backgroundColor = UIColor.secondary1()
        addSubview(lowerBorderView)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        descLabel.frame = CGRect(x: bounds.minX + 10 , y: bounds.minY
            , width: bounds.width - 20, height: bounds.height)
        upperBorderView.frame = CGRect(x: bounds.minX, y: bounds.minY, width: bounds.width, height: 1)
        lowerBorderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}