//
//  FeatureCollectionCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 7/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import AlamofireImage
class FeatureCollectionCell : UICollectionViewCell {
    var featureImageView : UIImageView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        featureImageView = UIImageView(frame: bounds)
        featureImageView.image = UIImage(named: "holder")
        addSubview(featureImageView)
    }
    
    func setImageRoundedCorners(key : String, imageCategory : ImageCategory ) {
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
            size: featureImageView.frame.size,
            radius: 0.0
        )
        featureImageView.af_setImageWithURL(ImageURLFactory.get(key, category: imageCategory), placeholderImage : UIImage(named: "holder"), filter: filter)
    }
    
    func setImage(imageKey : String){
        let filter = AspectScaledToFillSizeFilter(
            size: featureImageView.frame.size
        )
        featureImageView.af_setImageWithURL(ImageURLFactory.get(imageKey), placeholderImage : UIImage(named: "holder"), filter : filter)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}