//
//  UserCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 3/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class UserCell : UICollectionViewCell{
    var userCollectionView : UICollectionView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        userCollectionView = UICollectionView(frame: bounds, collectionViewLayout: layout)
        userCollectionView!.backgroundColor = UIColor.whiteColor()
        userCollectionView.showsHorizontalScrollIndicator = false
        addSubview(userCollectionView!)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}