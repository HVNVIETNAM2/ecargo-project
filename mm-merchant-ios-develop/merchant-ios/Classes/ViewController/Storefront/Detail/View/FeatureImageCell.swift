//
//  ImageDefaultCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 30/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class FeatureImageCell : UICollectionViewCell{
    var featureCollectionView : UICollectionView!
    var heartImageView = UIImageView()
    var badgeImageView = UIImageView()
    var borderView = UIView()
    var pageControl = UIPageControl()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        let layout: SnapFlowLayout = SnapFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        featureCollectionView = UICollectionView(frame: bounds, collectionViewLayout: layout)
        featureCollectionView!.backgroundColor = UIColor.whiteColor()
        featureCollectionView.showsHorizontalScrollIndicator = false
        addSubview(featureCollectionView)
        heartImageView.image = UIImage(named: "like_on")
        heartImageView.contentMode = .ScaleAspectFit
        addSubview(heartImageView)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        pageControl.numberOfPages = 5
        pageControl.currentPage = 0
        pageControl.tintColor = UIColor.primary1()
        pageControl.pageIndicatorTintColor = UIColor.primary2()
        pageControl.currentPageIndicatorTintColor = UIColor.primary1()
        addSubview(pageControl)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        heartImageView.frame = CGRect(x: bounds.maxX - 35, y: bounds.maxY - 35, width: 25, height: 25)
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        pageControl.frame = CGRect(x: bounds.midX - 40, y: bounds.maxY - 20, width: 80, height: 20)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}