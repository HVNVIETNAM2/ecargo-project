//
//  CommentCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 2/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class CommentCell : UICollectionViewCell{
    var textLabel = UILabel()
    var expandLabel = UILabel()
    var borderView = UIView()
    var expandView = UIImageView()
    var userImageView = UIImageView()
    var userNameLabel = UILabel()
    var commentLabel = UILabel()
    var dateLabel = UILabel()
    var firstImageView = UIImageView()
    var secondImageView = UIImageView()
    var arrowView = UIImageView()
    private final let ImageWidth = CGFloat(80.0)
    private final let MarginLeft = CGFloat(5.0)
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        textLabel.formatSize(14)
        textLabel.text = String.localize("LB_CA_PROD_REVIEW")
        addSubview(textLabel)
        userImageView.layer.borderColor = UIColor.secondary1().CGColor
        userImageView.layer.borderWidth = 1.0
        addSubview(userImageView)
        userNameLabel.formatSmall()
        addSubview(userNameLabel)
        commentLabel.formatSmall()
        addSubview(commentLabel)
        dateLabel.formatSmall()
        addSubview(dateLabel)
        addSubview(firstImageView)
        addSubview(secondImageView)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        expandLabel.formatSmall()
        addSubview(expandLabel)
        expandView.image = UIImage(named: "icon_arrow_small")
        addSubview(expandView)
        arrowView.image = UIImage(named: "icon_arrow_small")
        addSubview(arrowView)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        textLabel.frame = CGRect(x: bounds.minX + 10, y: bounds.minY + 10 , width: 200, height: 60)
        userImageView.frame = CGRect(x: bounds.minX + 10 , y: bounds.minY + 60, width: 50, height: 50)
        userImageView.round()
        userNameLabel.frame = CGRect(x: bounds.minX + 70, y: bounds.minY + 60, width: 100, height: 50)
        commentLabel.frame = CGRect(x: bounds.minX + 10, y: bounds.minY + 120, width: bounds.width, height: 50)
        dateLabel.frame = CGRect(x: bounds.minX + 10, y: bounds.minY + 160, width: bounds.width, height : 30)
        firstImageView.frame = CGRect(x: bounds.minX + 10, y: bounds.minY + 200, width: ImageWidth, height: 120)
        secondImageView.frame = CGRect(x: firstImageView.frame.maxX + MarginLeft, y: bounds.minY + 200, width: ImageWidth, height: 120)
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 10, width: bounds.width, height: 1)
        expandLabel = UILabel(frame: CGRect(x: bounds.center.x - 30, y: bounds.maxY , width: 60, height: 20))
        expandView = UIImageView(frame: CGRect(x: bounds.maxX - 45, y: bounds.maxY - 60, width: 17, height: 9))
        arrowView = UIImageView(frame: CGRect(x: bounds.maxX - 45 , y: bounds.maxY - 65 , width: 30, height: 45))
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}