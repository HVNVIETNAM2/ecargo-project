//
//  ColorCollectionCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 1/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class ColorCollectionCell : UICollectionViewCell {
    var label = UILabel()
    var imageView = UIImageView()
    var crossView = UIImageView(image: UIImage(named: "size_btn_outline"))
    var view = UIView()
    var isCrossed = false
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        imageView.image = nil
        imageView.layer.borderColor = UIColor.secondary1().CGColor
        imageView.layer.borderWidth = 1
        addSubview(imageView)
        label.formatSize(14)
        label.textAlignment = .Center
        addSubview(label)
        view.backgroundColor = UIColor.whiteColor()
        view.alpha = 0.0
        addSubview(view)
        crossView.alpha = 0.0
        addSubview(crossView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: bounds.minX + 5, y: bounds.minY + 5, width: bounds.width - 10, height: bounds.height - 10)
        label.frame = CGRect(x: bounds.midX - 20, y: bounds.midY - 20 , width: 40, height: 40)
        imageView.round()
        view.frame = CGRect(x: bounds.minX + 5, y: bounds.minY + 5, width: bounds.width - 10, height: bounds.height - 10)
        view.round()
        crossView.frame = CGRect(x: bounds.minX + 5, y: bounds.minY + 5, width: bounds.width - 10, height: bounds.height - 10)
        crossView.round()
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func cross(){
        view.alpha = 0.7
        crossView.alpha = 1.0
        isCrossed = true
    }
    
    func unCross(){
        view.alpha = 0.0
        crossView.alpha = 0.0
        isCrossed = false
    }
    
    func border(){
        imageView.layer.borderColor = UIColor.blackColor().CGColor

    }
    
    func unBorder(){
        imageView.layer.borderColor = UIColor.secondary1().CGColor

    }

}