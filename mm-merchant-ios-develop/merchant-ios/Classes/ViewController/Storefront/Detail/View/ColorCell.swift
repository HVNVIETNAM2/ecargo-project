//
//  ColorCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 1/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class ColorCell : UICollectionViewCell{
    var colorCollectionView : UICollectionView!
    var borderView = UIView()
    var arrowView = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        layout.headerReferenceSize = CGSize(width: 60, height: bounds.height)
        colorCollectionView = UICollectionView(frame: bounds, collectionViewLayout: layout)
        colorCollectionView!.backgroundColor = UIColor.whiteColor()
        colorCollectionView.showsHorizontalScrollIndicator = false
        addSubview(colorCollectionView!)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        arrowView.image = UIImage(named: "icon_arrow_small")
        addSubview(arrowView)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        arrowView.frame = CGRect(x: bounds.maxX - 40 , y: bounds.minY + 15 , width: 30, height: 45)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}