//
//  NameCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 30/11/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import AlamofireImage
class NameCell : UICollectionViewCell{
    var logoImageView = UIImageView()
    var nameLabel = UILabel()
    private final let MarginTop : CGFloat = 10
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        addSubview(logoImageView)
        nameLabel.formatSmall()
        nameLabel.textAlignment = .Center
        addSubview(nameLabel)
        layoutSubviews()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        logoImageView.frame = CGRect(x: bounds.midX - Constants.Value.BrandImageHeight / 2 , y: bounds.minY + MarginTop, width: Constants.Value.BrandImageHeight, height: Constants.Value.BrandImageHeight)
        nameLabel.frame = CGRect(x: bounds.minX , y: bounds.minY + Constants.Value.BrandImageHeight + MarginTop, width: bounds.width, height: 25)
    }
    
    func setImage(imageKey : String, imageCategory : ImageCategory){
        let filter = AspectScaledToFitSizeFilter(
            size: logoImageView.frame.size
        )
        logoImageView.af_setImageWithURL(ImageURLFactory.get(imageKey, category: imageCategory), placeholderImage : UIImage(named: "holder"), filter: filter)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}