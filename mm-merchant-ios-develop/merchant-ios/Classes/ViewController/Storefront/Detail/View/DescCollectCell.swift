//
//  DescCollectCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 2/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import AlamofireImage
class DescCollectCell : UICollectionViewCell {
    var descImageView = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        descImageView.image = UIImage(named: "holder")
        addSubview(descImageView)
        layoutSubviews()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        descImageView.frame = CGRect(x: bounds.minX, y: bounds.minY, width: bounds.width, height: bounds.height)
    }
    
    func setImage(imageKey : String){
        let filter = AspectScaledToFillSizeFilter(
            size: descImageView.frame.size
        )
        descImageView.af_setImageWithURL(ImageURLFactory.get(imageKey), placeholderImage : UIImage(named: "holder"), filter : filter)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}