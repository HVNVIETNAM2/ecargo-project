//
//  OutfitCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 3/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class OutfitCell : UICollectionViewCell {
    var headLabel = UILabel()
    var borderView = UIView()
    var logoView = UIImageView()
    var outfitImageView = UIImageView()
    var textLabel = UILabel()
    var likeTextLabel = UILabel()
    var likeNumberLabel = UILabel()
    var commentImageView = UIImageView()
    var commentTextLabel = UILabel()
    var commentNumberLabel = UILabel()
    var brandLabel = UILabel()
    var heartImageView = UIImageView()
    var moreImageView = UIImageView()
    var borderViewHeader = UIView()
    var textLabel1 = UILabel()
    var buyView : BuyCell!
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        
        headLabel.formatSmall()
        headLabel.textAlignment = .Center
        headLabel.text = String.localize("LB_CA_RCMD_TO_CONSUMER")
        addSubview(headLabel)
        
       
        borderViewHeader.backgroundColor = UIColor.secondary1()
        addSubview(borderViewHeader)
       
        logoView.layer.borderColor = UIColor.secondary1().CGColor
        logoView.layer.borderWidth = 1
        addSubview(logoView)
        brandLabel.formatSmall()
        brandLabel.font = UIFont.boldSystemFontOfSize(14)
        addSubview(brandLabel)
        addSubview(outfitImageView)
        textLabel.formatSmall()
        addSubview(textLabel)
        likeTextLabel.formatSmall()
        likeTextLabel.text = String.localize("LB_CA_LIKE")
        addSubview(likeTextLabel)
        likeNumberLabel.formatSmall()
        addSubview(likeNumberLabel)
        commentImageView.image = UIImage(named: "icon_comment")
        addSubview(commentImageView)
        commentTextLabel.formatSmall()
        commentTextLabel.text = String.localize("LB_CA_COMMENT")
        addSubview(commentTextLabel)
        commentNumberLabel.formatSmall()
        addSubview(commentNumberLabel)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        let frame = CGRectMake( 0, bounds.maxY - 150  , self.frame.width, 74)
        buyView = BuyCell.init(frame: frame)
        buyView.borderView.hidden = true
        buyView.moreImageView.hidden = true
        buyView.isHideMore = true
        buyView.imImageView.hidden = true
        addSubview(buyView)
        heartImageView.image = UIImage(named: "like_rest")
        addSubview(heartImageView)
        moreImageView.image = UIImage(named: "icon_more")
        addSubview(moreImageView)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        headLabel.frame = CGRect(x:bounds.minX, y: bounds.minY , width: bounds.width, height: 30)
        borderViewHeader.frame = CGRect(x: bounds.minX, y: bounds.minY + 30, width: bounds.width, height: 1)
        logoView.frame = CGRect(x: bounds.minX + 20, y: bounds.minY + 42, width: 50, height: 50)
        logoView.round()
        brandLabel.frame = CGRect(x: bounds.minX + 80, y: bounds.minY + 42, width: bounds.width - 80, height: 50)
        outfitImageView.frame = CGRect(x: bounds.minX + 20, y: bounds.minY + 95, width: bounds.width - 40, height: bounds.height - 180)
        textLabel.frame = CGRect(x: bounds.minX + 20, y: bounds.maxY - 83, width: bounds.width, height: 40)
        likeTextLabel.frame = CGRect(x: bounds.minX + 20, y: bounds.maxY - 45, width: 40, height: 40)
        likeNumberLabel.frame = CGRect(x: bounds.minX + 60, y: bounds.maxY - 45, width: 40, height: 40)
        commentImageView.frame = CGRect(x:bounds.maxX - 35, y:  bounds.maxY - 35, width: 20, height: 20)
        commentTextLabel.frame = CGRect(x: bounds.minX + 120, y:  bounds.maxY - 45, width: 40, height: 40)
        commentNumberLabel.frame = CGRect(x: bounds.minX + 160, y: bounds.maxY - 45, width: 40, height: 40)
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        heartImageView.frame = CGRect(x: bounds.maxX - 35, y:bounds.maxY - 125, width: 25, height: 25)
        moreImageView.frame = CGRect(x: bounds.maxX - 40, y: bounds.minY + 50, width: 35, height: 35)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}