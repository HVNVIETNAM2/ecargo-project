//
//  ParameterViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 8/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class ParameterViewController : MmViewController {
    private final let CellId : String = "Cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = String.localize("LB_DETAILS")
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo_demo_L"))
        self.collectionView!.backgroundColor = UIColor.whiteColor()
    }
    //MARK: CollectionView Data Source, Delegate Method
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        switch collectionView {
        case self.collectionView!:
            return 1
        default:
            return 1
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.collectionView!:
            return 6
        default:
            return 0
        }
    }
    
    //MARK: Draw Cell
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
            
        case self.collectionView!:
            
            switch (indexPath.row){
            case 0: // Image Default Cell
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
                return cell
            default:
                return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
                
            }
        default:
            return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
        }
    }
    
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
        return cell
    }
    
    
    //MARK: Item Size Delegate for Collection View
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
                
            case self.collectionView!:
                switch (indexPath.row){
                case 0:
                    return CGSizeMake(self.view.frame.size.width, 80)
                case 1:
                    return CGSizeMake(self.view.frame.size.width, 40)
                case 2:
                    return CGSizeMake(self.view.frame.size.width, 40)
                case 3:
                    return CGSizeMake(self.view.frame.size.width, 80)
                case 4:
                    return CGSizeMake(self.view.frame.size.width, 40)
                case 5:
                    return CGSizeMake(self.view.frame.size.width, 80)
                    
                default:
                    return CGSizeMake(self.view.frame.size.width, 40)
                    
                }
                
            default:
                return CGSizeMake(self.view.frame.size.width, 40)
                
            }
    }
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    //MARK: Collection View Delegate methods
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            
            switch collectionView {
                
            case self.collectionView!:
                break
            default:
                break
            }
            
            collectionView.reloadData()
            
    }
    
    func reloadAllData(){
        self.collectionView?.reloadData()
    }
    

}