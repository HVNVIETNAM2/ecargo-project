//
//  MobileLoginViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 29/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import ObjectMapper
import PromiseKit


protocol MobileLoginDelegate{
    func forgotPasswordClicked()
}

class MobileLoginViewController :  SignupModeViewController, UIGestureRecognizerDelegate, SelectGeoCountryDelegate, UITextFieldDelegate{
    
    private var blurEffectView = UIView()
    private var mobileLoginView = MobileLoginView()
    private var tapGesture = UITapGestureRecognizer()
    
    private let ContentViewHeight : CGFloat = 280
    private let ExpandedContentViewHeight : CGFloat = 355
    var geoCountries : [GeoCountry] = []
    var isDetectingCountry : Bool = false
    var isCodeInput = false
    var mobileLoginDelegate : MobileLoginDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        self.view.addGestureRecognizer(tapGesture)
        mobileLoginView.button.addTarget(self, action: "login:", forControlEvents: .TouchUpInside)
        mobileLoginView.cornerButton.addTarget(self, action: "forgotPassword:", forControlEvents: .TouchUpInside)
        mobileLoginView.upperTextField.delegate = self
        mobileLoginView.lowerTextField.delegate = self
        loadGeo()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animateWithDuration(0.3) { () -> Void in
            self.mobileLoginView.frame =  CGRectMake(0, self.view.frame.maxY - self.mobileLoginView.bounds.height, self.mobileLoginView.bounds.width, self.mobileLoginView.bounds.height)
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    //MARK: KeyboardWilShow/Hide callback
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.mobileLoginView.frame =  CGRectMake(0, self.view.frame.maxY - self.mobileLoginView.bounds.height - keyboardSize.height, self.mobileLoginView.bounds.width, self.mobileLoginView.bounds.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        self.mobileLoginView.frame =  CGRectMake(0, self.view.frame.maxY - self.mobileLoginView.bounds.height, self.mobileLoginView.bounds.width, self.mobileLoginView.bounds.height)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK: Set up Layout on viewDidLoad
    
    func setupLayout(){
        
        self.title = String.localize("LB_DETAILS")
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo_demo_L"))
        mobileLoginView.frame = CGRectMake(0, self.view.bounds.height, self.view.bounds.width, ContentViewHeight)
        mobileLoginView.backgroundColor = UIColor.whiteColor()
        view.addSubview(mobileLoginView)
        mobileLoginView.upperTextField.placeholder = String.localize("LB_CA_MOB_UN_EMAIL")
        mobileLoginView.lowerTextField.placeholder = String.localize("LB_ENTER_PW")
        mobileLoginView.button.setTitle(String.localize("LB_CA_LOGIN"), forState: .Normal)
        mobileLoginView.cornerButton.setTitle(String.localize("LB_CA_FORGOT_PASSWORD"), forState: .Normal)
        mobileLoginView.hideCodeInput()
        self.blurEffectView.frame = self.view.bounds
        self.blurEffectView.backgroundColor = UIColor.clearColor()
        self.blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.blurEffectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "dismiss"))
        self.view.insertSubview(self.blurEffectView, belowSubview: mobileLoginView)
        self.tapGesture.addTarget(self, action: "dismissKeyboard")
        self.view.backgroundColor = UIColor.clearColor()
        
    }
    
    func dismiss(){
        dismissKeyboard()
        
        UIView.animateWithDuration(
            0.3,
            animations: { () -> Void in
                self.mobileLoginView.frame = CGRectMake(0, self.view.bounds.height, self.view.bounds.width, self.ContentViewHeight)
            },
            completion: { (success) -> Void in
                self.dismissViewControllerAnimated(false, completion: nil)
            }
        )
        
    }
    
    override func shouldHaveCollectionView() -> Bool {
        return false
    }
    
    //MARK: Forgot Password Service
    func forgotPassword(sender:UIButton ){
        dismissKeyboard()

        UIView.animateWithDuration(
            0.3,
            animations: { () -> Void in
                self.mobileLoginView.frame = CGRectMake(0, self.view.bounds.height, self.view.bounds.width, self.ContentViewHeight)
            },
            completion: { (success) -> Void in
                self.dismissViewControllerAnimated(false) { () -> Void in
                    if self.mobileLoginDelegate != nil {
                        self.mobileLoginDelegate?.forgotPasswordClicked()
                    }
                }
            }

        )

    }
    
    //MARK: Login Service
    func login(sender:UIButton ){
        var parameters = [String : AnyObject]()
        var username = ""
        if isCodeInput {
            username = "\(mobileLoginView.signupInputView.codeTextField.text!)-\(mobileLoginView.signupInputView.mobileNumberTextField.text!)"
        } else {
            username = mobileLoginView.upperTextField.text ?? ""
        }
        let password = mobileLoginView.lowerTextField.text ?? ""
        parameters["Username"] = username
        parameters["Password"] = password
        sender.enabled = false
        AuthService.login(parameters){[weak self] (response) in
            if let strongSelf = self {
                strongSelf.stopLoading()
                
                if response.result.isSuccess{
                    if response.response!.statusCode == 200 {
                        if let token = Mapper<Token>().map(response.result.value) {
                            Log.debug(String(data: response.data!, encoding: 4))
                            LoginManager.login(token, username: username, password: password)
                        }
                        strongSelf.view.endEditing(true)
                        
                        if strongSelf.signupMode == .Normal {
                            LoginManager.goToStorefront()
                        } else if strongSelf.signupMode == .Checkout {
                        
                            if let presentingViewController = strongSelf.presentingViewController { // as UINavigation
                                if let rootPresentingViewController = presentingViewController.presentingViewController { // as StoreFontViewController
                                    
                                    rootPresentingViewController.dismissViewControllerAnimated(false, completion: {
                                        
                                        let checkoutConfirmationViewController = CheckoutConfirmationViewController()
                                        if (rootPresentingViewController.dynamicType == StorefrontController.self) {
                                            
                                            let storeFrontVC = rootPresentingViewController as! StorefrontController
                                            if let navigation = storeFrontVC.selectedViewController where navigation.dynamicType == UINavigationController.self  {
                                                if let shoppingCartVC = (navigation as! UINavigationController).viewControllers.last as? ShoppingCartViewController {
                                                    checkoutConfirmationViewController.listData = shoppingCartVC.listDataSelected()
                                                }
                                                (navigation as! UINavigationController).pushViewController(checkoutConfirmationViewController, animated: true)
                                            }
                                        }
                                    })
                                }
                            }
                        }
                        else if strongSelf.signupMode == .CheckoutSwipeToPay {
                            
                            if let presentingViewController = strongSelf.presentingViewController { // as UINavigation
                                if let rootPresentingViewController = presentingViewController.presentingViewController { // as StoreFontViewController
                                    
                                    rootPresentingViewController.dismissViewControllerAnimated(false, completion: {
                                        if let navViewController = rootPresentingViewController as? UINavigationController {
                                            if let checkoutViewController = navViewController.viewControllers.first where checkoutViewController.dynamicType == CheckOutViewController.self {
                                                (checkoutViewController as! CheckOutViewController).collectionView.reloadData()
                                            }
                                        }
                                    })
                                }
                            }
                            
                        }
                        else if strongSelf.signupMode == .Profile {
                            if let presentingViewController = strongSelf.presentingViewController { // as UINavigation
                                if let rootPresentingViewController = presentingViewController.presentingViewController { // as StoreFontViewController
                                    
                                    rootPresentingViewController.dismissViewControllerAnimated(false, completion: {
                                        if (rootPresentingViewController.dynamicType == StorefrontController.self) {
                                            let storeFrontVC = rootPresentingViewController as! StorefrontController
                                            storeFrontVC.selectedIndex = TabIndex.Profile.rawValue
                                        }
                                    })
                                }
                            }
                        } else if strongSelf.signupMode == .IM {
                            if let presentingViewController = strongSelf.presentingViewController { // as UINavigation
                                if let rootPresentingViewController = presentingViewController.presentingViewController { // as StoreFontViewController
                                    
                                    rootPresentingViewController.dismissViewControllerAnimated(false, completion: {
                                        if (rootPresentingViewController.dynamicType == StorefrontController.self) {
                                            let storeFrontVC = rootPresentingViewController as! StorefrontController
                                            storeFrontVC.selectedIndex = TabIndex.IM.rawValue
                                        }
                                    })
                                }
                            }
                        }
                    } else {
                        sender.enabled = true
                        strongSelf.handleError(response, animated: true)
                        if !strongSelf.isCodeInput{
                            if let resp = Mapper<ApiResponse>().map(response.result.value){
                                if resp.isMobile {
                                    strongSelf.showCodeInput()
                                }
                            }
                        }
                    }
                } else {
                    sender.enabled = true
                    strongSelf.showError(String.localize("MSG_ERR_NETWORK_FAIL"), animated: true)
                }
            }
        }
        
    }
    
    func showCodeInput(){
        mobileLoginView.frame = CGRectMake(self.mobileLoginView.frame.minX, self.mobileLoginView.frame.minY - ExpandedContentViewHeight + ContentViewHeight, self.mobileLoginView.frame.width, ExpandedContentViewHeight)
        mobileLoginView.showCodeInput()
        mobileLoginView.signupInputView.countryButton.addTarget(self, action: "selectCountry:", forControlEvents: .TouchUpInside)
        mobileLoginView.signupInputView.codeTextField.text = "+86"
        mobileLoginView.signupInputView.mobileNumberTextField.becomeFirstResponder()
        mobileLoginView.signupInputView.codeTextField.delegate = self
        mobileLoginView.signupInputView.mobileNumberTextField.delegate = self
        isCodeInput = true
        
    }
    
    
    //MARK: Button delegate
    func selectCountry(button : UIButton){
        let mobileSelectCountryViewController = MobileSelectCountryViewController()
        mobileSelectCountryViewController.selectGeoCountryDelegate = self
        mobileSelectCountryViewController.isNeedToDismiss = true
        let mobileSelectCountryNavController = UINavigationController()
        mobileSelectCountryNavController.viewControllers = [mobileSelectCountryViewController]
        presentViewController(mobileSelectCountryNavController, animated: true, completion: nil)
    }
    
    func selectGeoCountry(geoCountry: GeoCountry) {
        mobileLoginView.signupInputView.codeTextField.text = geoCountry.mobileCode
        mobileLoginView.signupInputView.countryButton.setTitle(geoCountry.geoCountryName, forState: .Normal)
    }
    
    //MARK: UITextFieldDelegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        Log.debug("location: \(range.location) string: \(string)")
        switch textField.tag {
        case 1: //Mobile Code
            if string == "" && range.location == 0 { //Don't allow todelete +
                return false
            }
            if !isDetectingCountry {
                isDetectingCountry = true
                NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "detectCountry", userInfo: nil, repeats: false)
            }
            break
        default:
            break
        }
        
        return true
    }
    
    func detectCountry() {
        let code = mobileLoginView.signupInputView.codeTextField.text
        for geoCountry : GeoCountry in self.geoCountries {
            Log.debug("code : \(code) countryCode: \(geoCountry.mobileCode)")
            if code == geoCountry.mobileCode {
                mobileLoginView.signupInputView.countryButton.setTitle(geoCountry.geoCountryName, forState: .Normal)
                isDetectingCountry = false
                return
            }
        }
        isDetectingCountry = false
        mobileLoginView.signupInputView.countryButton.setTitle(String.localize("LB_COUNTRY_PICK"), forState: .Normal)
    }
    
    func loadGeo() {
        self.showLoading()
        firstly{
            return self.listGeo()
            }.then
            { _ -> Void in
                
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
        
    }
    
    func listGeo() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            GeoService.storefrontCountries(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response!.statusCode == 200 {
                            strongSelf.geoCountries = Mapper<GeoCountry>().mapArray(response.result.value) ?? []
                            fulfill("OK")
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    //MARK: UITextFieldDelegate
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.tag == 7 {
            if mobileLoginView.upperTextField.text?.length < 1 {
                self.showError(String.localize("MSG_ERR_CA_USERNAME_NIL"), animated: true)
            }
            if mobileLoginView.upperTextField.text!.length > 0 && RegexManager.matchesForRegexInText("^[a-zA-Z]\\w{0,50}$", text: mobileLoginView.upperTextField.text).isEmpty && RegexManager.matchesForRegexInText("^(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*$", text: mobileLoginView.upperTextField.text).isEmpty && RegexManager.matchesForRegexInText("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", text: mobileLoginView.upperTextField.text).isEmpty{
                self.showError(String.localize("MSG_ERR_CA_ACCOUNT_PATTERN"), animated: true)
            }
        }
        
        if textField.tag == 8 {
            if mobileLoginView.lowerTextField.text?.length < 1 {
                self.showError(String.localize("MSG_ERR_CA_PW_NIL"), animated: true)
            }
            if mobileLoginView.lowerTextField.text?.length > 0 && RegexManager.matchesForRegexInText("(?=^.{8,50}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\\s).*$", text: mobileLoginView.lowerTextField.text).isEmpty {
                self.showError(String.localize("MSG_ERR_CA_PW_PATTERN"), animated: true)
            }
        }
    }
    
    
    
    
}