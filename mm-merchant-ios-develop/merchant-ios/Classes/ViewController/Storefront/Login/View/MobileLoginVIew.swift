//
//  LoginContentView.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 29/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class MobileLoginView : UIView {
    struct Padding{
        static let width : CGFloat = 22
        static let top : CGFloat = 26
    }
    struct Size{
        static let height: CGFloat = 46.5
        static let width : CGFloat = 120
    }

    var upperTextField = UITextField()
    var lowerTextField = UITextField()
    var imageView = UIImageView(image: UIImage(named: "input_box")!.resizableImageWithCapInsets(UIEdgeInsetsMake(0, 100, 0, 100)))
    var button = UIButton()
    var cornerButton = UIButton()
    
    var signupInputView = SignupInputView()
    var isTall = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageView)
        addSubview(upperTextField)
        upperTextField.tag = 7
        addSubview(lowerTextField)
        lowerTextField.tag = 8
        button.formatPrimary()
        addSubview(button)
        cornerButton.formatWhite()
        addSubview(cornerButton)
        upperTextField.autocorrectionType = .No
        upperTextField.spellCheckingType = .No
        upperTextField.autocapitalizationType = .None
        lowerTextField.spellCheckingType = .No
        lowerTextField.autocapitalizationType = .None
        lowerTextField.secureTextEntry = true
        addSubview(signupInputView)
        layout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
    
    func layout(){
        if isTall {
            layoutTall()
        } else {
            layoutShort()
        }

    }
    
    func layoutShort(){
        imageView.frame = CGRect(x: bounds.minX + Padding.width , y: bounds.minY + Padding.top, width: bounds.width - 2 * Padding.width, height: Size.height * 2)
        imageView.image = UIImage(named: "input_box")!.resizableImageWithCapInsets(UIEdgeInsetsMake(0, 100, 0, 100))

        upperTextField.frame = CGRect(x: bounds.width / 3 , y: bounds.minY + Padding.top, width: bounds.width / 3 * 2, height: Size.height)
        lowerTextField.frame = CGRect(x:  bounds.width / 3, y: bounds.minY + Padding.top + upperTextField.bounds.height, width: bounds.width / 3 * 2, height: Size.height)
        button.frame = CGRect(x: bounds.minX + Padding.width, y: bounds.minY + Padding.top * 2 + Size.height * 2 , width: bounds.width - 2 * Padding.width, height: Size.height)
        cornerButton.frame = CGRect(x: bounds.maxX - Padding.width - Size.width, y: button.frame.maxY + Padding.top, width: Size.width, height: Size.height)
    }
    
    func layoutTall(){
        signupInputView.frame = CGRect(x: bounds.minX + Padding.width , y: bounds.minY + Padding.top, width: bounds.width - 2 * Padding.width, height: Size.height * 2 + 2)
        signupInputView.layout()
        imageView.frame = CGRect(x: bounds.minX + Padding.width , y: signupInputView.bounds.maxY + Padding.top * 2 , width: bounds.width - 2 * Padding.width, height: Size.height + 2)
        imageView.image = UIImage(named: "password")!.resizableImageWithCapInsets(UIEdgeInsetsMake(0, 100, 0, 100))
        lowerTextField.frame = CGRect(x:  bounds.width / 3, y: signupInputView.bounds.maxY + Padding.top * 2, width: bounds.width / 3 * 2, height: Size.height)
        button.frame = CGRect(x: bounds.minX + Padding.width, y: signupInputView.bounds.maxY  + Padding.top * 3 + Size.height , width: bounds.width - 2 * Padding.width, height: Size.height)
        cornerButton.frame = CGRect(x: bounds.maxX - Padding.width - Size.width, y: button.frame.maxY + Padding.top, width: Size.width, height: Size.height)

    }
    
    func showCodeInput(){
        signupInputView.hidden = false
        upperTextField.hidden = true
        isTall = true
        layout()
    }
    
    func hideCodeInput(){
        signupInputView.hidden = true
        upperTextField.hidden = false
        isTall = false
        layout()

    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}