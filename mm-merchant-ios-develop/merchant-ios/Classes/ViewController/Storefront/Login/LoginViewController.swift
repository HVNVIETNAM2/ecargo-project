//
//  LoginVC.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 28/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginViewController : SignupModeViewController, WXApiDelegate, MobileLoginDelegate {
    var loginButtonView = LoginButtonView()
    var signupButtonView = SignupButtonView()
    let loginButtonsPadding: CGFloat = 14.0
    var crossView : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let backgroundImageView = UIImageView(frame: self.view.frame)
        backgroundImageView.image = UIImage(named: "login_bg")
        self.view.addSubview(backgroundImageView)
        loginButtonView.frame = CGRect(x: self.view.frame.minX, y: self.view.frame.maxY - loginButtonView.upperButton.frame.height - loginButtonView.lowerButton.frame.height - 71, width: self.view.frame.width, height: loginButtonView.upperButton.frame.height + loginButtonView.lowerButton.frame.height + loginButtonsPadding)
        self.view.addSubview(loginButtonView)
        signupButtonView.frame = CGRect(x: self.view.frame.minX, y: self.view.frame.maxY - 160 - 80, width: self.view.frame.width, height: 80)
        self.view.addSubview(signupButtonView)
        loginButtonView.upperButton.addTarget(self, action: "mobileLogin:", forControlEvents: .TouchUpInside)
        loginButtonView.lowerButton.addTarget(self, action: "guestLogin:", forControlEvents: .TouchUpInside)
        signupButtonView.leftLabel.text = String.localize("LB_CA_WECHAT_LOGIN")
        signupButtonView.rightLabel.text = String.localize("LB_CA_MOBILE_REGISTRATION")
        signupButtonView.leftButton.addTarget(self, action:"leftButtonClicked:", forControlEvents: .TouchUpInside )
        signupButtonView.rightButton.addTarget(self, action:"mobileSignup:", forControlEvents: .TouchUpInside )
        crossView = { () -> UIView in
            
            let buttonWidth = CGFloat(30)
            let buttonHeight = CGFloat(30)
            let buttonPadding = CGFloat(5)
            let viewTopPadding = CGFloat(20)
            let viewRightPadding = CGFloat(5)
            let viewWidth = buttonWidth + 2*buttonPadding
            let viewWHeight = buttonHeight + 2*buttonPadding
            let view = UIView (frame: CGRectMake(self.view.frame.size.width - viewWidth - viewRightPadding, viewTopPadding, viewWidth, viewWHeight))
            let button = UIButton(type: .Custom)
            button.setImage(UIImage(named: "icon_cross"), forState: .Normal)
            button.frame = CGRectMake(buttonPadding, buttonPadding, buttonWidth, buttonHeight)
            button.addTarget(self, action: "closeButtonTapped", forControlEvents: .TouchUpInside)
            view.addSubview(button)
            
            return view
        } ()
        self.view.addSubview(crossView)
        
        adjustUIBySignupMode()
    }
    
    func adjustUIBySignupMode () {
        if self.signupMode == .Normal {
            loginButtonView.lowerButton.hidden = false
            crossView.hidden = true
            
        } else {
            loginButtonView.lowerButton.hidden = true
            crossView.hidden = false
        }
    }
    
    func closeButtonTapped () {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loginWeChat:", name: "LoginWeChat", object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController!.navigationBarHidden = false
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
    }
    
    //MARK: Guest Login
    func guestLogin(button: UIButton) {
        LoginManager.goToStorefront()
    }
    
    //MARK: Mobile Login
    func mobileLogin(button: UIButton) {
        let mobileLoginViewController = MobileLoginViewController()
        mobileLoginViewController.mobileLoginDelegate = self
        mobileLoginViewController.signupMode = self.signupMode
        mobileLoginViewController.modalPresentationStyle = .OverFullScreen
        presentViewController(mobileLoginViewController, animated: false, completion: nil)
    }
    
    //MARK: Left button
    func leftButtonClicked(button: UIButton){
        Log.debug("leftButtonClicked")
        if WXApi.isWXAppInstalled() {
            let req = SendAuthReq()
            req.scope = "snsapi_message,snsapi_userinfo,snsapi_friend,snsapi_contact"
            req.state = "xxx"
            WXApi.sendAuthReq(
                req,
                viewController: self,
                delegate: self
            )

        } else {
            Alert.alert(self, title: String.localize("MSG_ERR_CA_WECHAT_NOT_INSTALLED"), message: String.localize("MSG_ERR_CA_WECHAT_NOT_INSTALLED"))
        }
        
    }
    
    //MARK: Mobile Signup
    func mobileSignup(button : UIButton){
        let mobileSignupViewController = MobileSignupViewController()
        mobileSignupViewController.signupMode = self.signupMode
        self.navigationController?.pushViewController(mobileSignupViewController, animated: true)
    }
    
    //MARK: WXApiDelegate method
    func onResp(resp: BaseResp!) {
        Log.debug("on Resp from WeChat")
        Log.debug("Err Code from WeChat : \(resp.errCode)")
        Log.debug("Err String from WeChat : \(resp.errStr)")
        let sendAuthResp = resp as! SendAuthResp
        Log.debug("Auth code from WeChat : \(sendAuthResp.code)")
        Log.debug("State from WeChat : \(sendAuthResp.state)")
        Log.debug("Lang from WeChat : \(sendAuthResp.lang)")
        Log.debug("Country from WeChat : \(sendAuthResp.country)")
    }
    
    func onReq(req: BaseReq!) {
        Log.debug("on Request from WeChat")
        
    }
    
    //MARK: Login Service
    
    func loginWeChat(sender : NSNotification){
        var parameters = [String : AnyObject]()
        Log.debug(sender.userInfo!["Code"] as! String)
        parameters["AuthorizationCode"] = sender.userInfo!["Code"] as! String
        self.showLoading()
        AuthService.loginWeChat(parameters){[weak self] (response) in
            if let strongSelf = self {
                strongSelf.stopLoading()
                
                if response.result.isSuccess{
                    if response.response!.statusCode == 200 {
                        if let token = Mapper<Token>().map(response.result.value){
                            Log.debug(token)
                            LoginManager.login(token)
                            if token.isSignup {
                                
                                let viewController: UIViewController
                                
                                if strongSelf.signupMode == .CheckoutSwipeToPay || strongSelf.signupMode == .Checkout {
                                    // handle the sign up mode form check out
                                    viewController = AddressAdditionViewController()
                                    (viewController as! AddressAdditionViewController).signupMode =  strongSelf.signupMode
                                } else {
                                    viewController = InterestKeywordViewController()
                                    (viewController as! InterestKeywordViewController).isMobileSignup = false
                                }
                                strongSelf.navigationController?.pushViewController(viewController, animated: true)

                            } else {
                                if strongSelf.signupMode == .CheckoutSwipeToPay {
                                    
                                    if let presentingViewController = strongSelf.presentingViewController { // as UINavigation

                                        presentingViewController.dismissViewControllerAnimated(false, completion: {
                                            
                                            if let navViewController = presentingViewController as? UINavigationController {
                                                let checkoutViewController = navViewController.viewControllers[0]
                                                
                                                if checkoutViewController.dynamicType == CheckOutViewController.self {
                                                    (checkoutViewController as! CheckOutViewController).collectionView.reloadData()
                                                }
                                            }
                                            
                                        })
                                        
                                    }
                                }
                                else if strongSelf.signupMode == .Checkout {

                                    if let presentingViewController = strongSelf.presentingViewController { // as StoreFontViewController
                                        
                                        presentingViewController.dismissViewControllerAnimated(false, completion: {
                                            
                                            let checkoutConfirmationViewController = CheckoutConfirmationViewController()
                                            if (presentingViewController.dynamicType == StorefrontController.self) {
                                                
                                                let storeFrontVC = presentingViewController as! StorefrontController
                                                if let navigation = storeFrontVC.selectedViewController where navigation.dynamicType == UINavigationController.self  {
                                                    if let shoppingCartVC = (navigation as! UINavigationController).viewControllers.last as? ShoppingCartViewController {
                                                        checkoutConfirmationViewController.listData = shoppingCartVC.listDataSelected()
                                                    }
                                                    (navigation as! UINavigationController).pushViewController(checkoutConfirmationViewController, animated: true)
                                                }
                                            }
                                        })

                                    }
                                }
                                else {
                                    LoginManager.goToStorefront()
                                }
                            }
                        }
                    } else {
                        Log.debug(String(data: response.data!, encoding : 4))
                        strongSelf.handleError(response, animated: true)
                    }
                } else {
                    strongSelf.showError(String.localize("MSG_ERR_NETWORK_FAIL"), animated: true)
                }
            }
        }
        
    }
    //MARK: Mobile Login Delegate
    func forgotPasswordClicked() {
        self.navigationController?.pushViewController(MobileForgotPasswordViewController(), animated: true)
    }
}