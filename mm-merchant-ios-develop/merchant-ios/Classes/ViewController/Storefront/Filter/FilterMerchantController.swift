//
//  FilterMerchantController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 19/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import AlamofireImage

class FilterMerchantController : MmViewController, UISearchBarDelegate{
    var searchBar = UISearchBar()
    var searchString = ""
    var merchants : [Merchant] = []
    var validMerchants : [Merchant] = []
    var filteredMerchants : [Merchant] = []
    var filterStyleDelegate : FilterStyleDelegate?
    var aggregations : Aggregations?
    var styleFilter : StyleFilter?
    var styles : [Style] = []
    var refreshControl = UIRefreshControl()
    var buttonCell = ButtonCell()
    
    private let HeaderHeight : CGFloat = 50
    private let ImageMenuCellHeight : CGFloat = 60
    private let SearchBarHeight : CGFloat = 40
    private let ButtonCellHeight : CGFloat = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge.None
        self.title = String.localize("LB_CA_FILTER_MERCHANT")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.frame = CGRect(x: self.view.bounds.minX, y: SearchBarHeight, width: self.view.bounds.width, height: self.view.bounds.height - SearchBarHeight - ButtonCellHeight - (self.navigationController?.navigationBar.frame.height)! - UIApplication.sharedApplication().statusBarFrame.size.height)
        loadMerchant()
        self.createBackButton()
        self.createRightButton(String.localize("LB_CA_RESET"), action: "resetTapped:")
        self.collectionView.registerClass(ImageMenuCell.self, forCellWithReuseIdentifier: "ImageMenuCell")
        self.searchBar.sizeToFit()
        self.searchBar.delegate = self
        self.searchBar.searchBarStyle = UISearchBarStyle.Default
        self.searchBar.showsCancelButton = false
        self.searchBar.frame = CGRect(x: self.view.bounds.minX, y: self.view.bounds.minY, width: self.view.bounds.width, height: SearchBarHeight)
        self.view.insertSubview(self.searchBar, aboveSubview: self.collectionView)
        self.buttonCell.frame = CGRect(x: self.view.bounds.minX, y: self.view.bounds.maxY - ButtonCellHeight - (self.navigationController?.navigationBar.frame.height)! - UIApplication.sharedApplication().statusBarFrame.size.height, width: self.view.bounds.width, height: ButtonCellHeight)
        self.view.insertSubview(self.buttonCell, aboveSubview: self.collectionView)
        self.buttonCell.layoutSubviews()
        self.buttonCell.button.setTitle(String.localize("LB_CA_SUBMIT"), forState: .Normal)
        self.buttonCell.itemLabel.text = ""
        self.buttonCell.button.addTarget(self, action: "confirmClicked:", forControlEvents: .TouchUpInside)
        self.setUpRefreshControl()
        
        
    }
    
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func loadMerchant() {
        self.showLoading()
        firstly{
            return self.listMerchant()
            }.then
            { _ -> Void in
                self.validMerchants = self.merchants.filter({(self.aggregations?.merchantArray.contains($0.merchantId))!})
                self.filteredMerchants = self.validMerchants
                self.collectionView.reloadData()
            }.always {
                self.stopLoading()
                self.refreshControl.endRefreshing()
                
            }.error { _ -> Void in
                Log.error("error")
        }
        
    }
    
    func listMerchant() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchMerchant(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        strongSelf.merchants = Mapper<Merchant>().mapArray(response.result.value) ?? []
                        strongSelf.merchants = strongSelf.merchants.filter({$0.merchantId != 0})
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageMenuCell", forIndexPath: indexPath) as! ImageMenuCell
        if (self.filteredMerchants.count > indexPath.row) {
            cell.upperLabel.text = self.filteredMerchants[indexPath.row].merchantName
            cell.lowerLabel.text = self.filteredMerchants[indexPath.row].merchantNameInvariant
            cell.setImage(self.filteredMerchants[indexPath.row].headerLogoImage, imageCategory: .Merchant)
            if self.styleFilter!.merchants.contains({$0.merchantId == filteredMerchants[indexPath.row].merchantId }){
                cell.tickImageView.image = UIImage(named: "tick")
            } else {
                cell.tickImageView.image = nil
            }
        }
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filteredMerchants.count
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
            case self.collectionView!:
                switch indexPath.row {
                default:
                    return CGSizeMake(self.view.frame.size.width, ImageMenuCellHeight)
                }
            default:
                return CGSizeMake(0,0)
            }
    }
    
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            if self.styleFilter!.merchants.contains({$0.merchantId == filteredMerchants[indexPath.row].merchantId }){
                self.styleFilter!.merchants = (self.styleFilter?.merchants.filter({$0.merchantId != filteredMerchants[indexPath.row].merchantId}))!
            } else {
                self.styleFilter!.merchants.append(filteredMerchants[indexPath.row])
            }
            self.collectionView.reloadData()
    }
    
    //MARK : Refresh Control
    
    func setUpRefreshControl(){
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.collectionView.addSubview(refreshControl)
        self.collectionView.alwaysBounceVertical = true
    }
    
    func refresh(sender : AnyObject){
        loadMerchant()
    }
    
    
    // MARK: UISearchBarDelegate
    
    private func filter(text : String!) {
        self.filteredMerchants = self.validMerchants.filter(){ $0.merchantDisplayName.lowercaseString.rangeOfString(text.lowercaseString) != nil || $0.merchantCompanyName.lowercaseString.rangeOfString(text.lowercaseString) != nil }
        self.collectionView?.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.filter(searchBar.text!)
        searchBar.resignFirstResponder()
    }
    
    
    internal func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchString = searchBar.text!
        if searchString.length == 0 {
            self.filteredMerchants = self.validMerchants
            self.collectionView.reloadData()
        } else {
            self.filter(searchString)
        }
        
    }
    
    
    // MARK: Confirm and Reset
    func resetTapped (sender:UIBarButtonItem){
        self.styleFilter?.merchants = []
        self.collectionView.reloadData()
    }
    
    func confirmClicked(sender: UIButton){
        filterStyleDelegate?.filterStyle(self.styles, styleFilter: self.styleFilter!)
        self.navigationController!.popViewControllerAnimated(true)
    }
    
}