//
//  FilterColorController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import AlamofireImage

class FilterColorController : MmViewController, UISearchBarDelegate{
    var colors : [Color] = []
    var validColors : [Color] = []
    var filteredColors : [Color] = []
    var filterStyleDelegate : FilterStyleDelegate?
    var aggregations : Aggregations?
    var styleFilter : StyleFilter?
    var styles : [Style] = []
    var refreshControl = UIRefreshControl()
    var buttonCell = ButtonCell()
    
    private let HeaderHeight : CGFloat = 50
    private let ImageMenuCellHeight : CGFloat = 60
    private let ButtonCellHeight : CGFloat = 60
    
    private let MarginLeft : CGFloat = 15
    private let MinSpacing : CGFloat = 20
    private var numberOfCollumn : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge.None
        self.title = String.localize("LB_CA_COLOUR")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.frame = CGRect(x: self.view.bounds.minX, y: 0, width: self.view.bounds.width, height: self.view.bounds.height - ButtonCellHeight - (self.navigationController?.navigationBar.frame.height)! - UIApplication.sharedApplication().statusBarFrame.size.height)
        loadColor()
        self.createBackButton()
        self.createRightButton(String.localize("LB_CA_RESET"), action: "resetTapped:")
        self.collectionView.registerClass(FilterColorCell.self, forCellWithReuseIdentifier: "FilterColorCell")
      
        self.buttonCell.frame = CGRect(x: self.view.bounds.minX, y: self.view.bounds.maxY - ButtonCellHeight - (self.navigationController?.navigationBar.frame.height)! - UIApplication.sharedApplication().statusBarFrame.size.height, width: self.view.bounds.width, height: ButtonCellHeight)
        self.view.insertSubview(self.buttonCell, aboveSubview: self.collectionView)
        self.buttonCell.layoutSubviews()
        self.buttonCell.button.setTitle(String.localize("LB_CA_SUBMIT"), forState: .Normal)
        self.buttonCell.itemLabel.text = ""
        self.buttonCell.button.addTarget(self, action: "confirmClicked:", forControlEvents: .TouchUpInside)
        numberOfCollumn = Int((self.view.frame.maxX - (MarginLeft * 2)) / (Constants.Value.FilterColorWidth + MinSpacing))
    }
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func loadColor() {
        self.showLoading()
        firstly{
            return self.listColor()
            }.then
            { _ -> Void in
                self.validColors = self.colors.filter({(self.aggregations?.colorArray.contains($0.colorId))!})
                self.filteredColors = self.validColors
                self.collectionView.reloadData()
            }.always {
                self.stopLoading()
                self.refreshControl.endRefreshing()
                
            }.error { _ -> Void in
                Log.error("error")
        }
        
    }
    
    func listColor() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchColor(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        Log.debug(String(data: response.data!, encoding: 4))
                        strongSelf.colors = Mapper<Color>().mapArray(response.result.value) ?? []
                        strongSelf.colors = strongSelf.colors.filter({$0.colorId != 0})
                        strongSelf.colors.sortInPlace {(color1:Color, color2:Color) -> Bool in
                            color1.colorId < color2.colorId
                        }
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FilterColorCell", forIndexPath: indexPath) as! FilterColorCell
        if (self.filteredColors.count > indexPath.row) {
            cell.labelName.text = self.filteredColors[indexPath.row].colorName
            
            let filter = AspectScaledToFitSizeFilter(
                size: cell.imageView.frame.size
            )
            cell.imageView.af_setImageWithURL(ImageURLFactory.get(self.filteredColors[indexPath.row].colorImage, category: .Color), placeholderImage : UIImage(named: "holder"), filter : filter)
            if self.styleFilter!.colors.contains({$0.colorId == filteredColors[indexPath.row].colorId }) {
                cell.border()
            } else {
                cell.unBorder()
            }
        }
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filteredColors.count
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
            case self.collectionView!:
                switch indexPath.row {
                default:
                    return CGSizeMake((self.view.frame.maxX - (MarginLeft * 2)) / CGFloat(numberOfCollumn), Constants.Value.FilterColorWidth + 54.0)
                }
            default:
                return CGSizeMake(0,0)
            }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch collectionView {
        case self.collectionView!:
                return 0.0
        default:
            return 0.0
        }
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch collectionView {
        default:
            return 0.0
        }
        
    }

    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            switch collectionView {
            case self.collectionView!:
                return UIEdgeInsets(top: 25 , left: MarginLeft , bottom: 0, right: MarginLeft)
            default:
                return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
            }
    }

    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            if self.styleFilter!.colors.contains({$0.colorId == filteredColors[indexPath.row].colorId }) {
                self.styleFilter!.colors = (self.styleFilter?.colors.filter({$0.colorId != filteredColors[indexPath.row].colorId}))!
            } else {
                self.styleFilter!.colors.append(filteredColors[indexPath.row])
            }
            self.collectionView.reloadData()
    }
    

    func setUpRefreshControl(){
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.collectionView.addSubview(refreshControl)
        self.collectionView.alwaysBounceVertical = true
    }
    
    func refresh(sender : AnyObject){
        loadColor()
    }
    
    
    // MARK: UISearchBarDelegate
    
    private func filter(text : String!) {
        self.filteredColors = self.colors.filter(){ $0.colorName.lowercaseString.rangeOfString(text.lowercaseString) != nil }
        self.collectionView.reloadData()
    }
    
    internal func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.filter(searchBar.text!)
        searchBar.resignFirstResponder()
    }
    
    internal func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.length == 0 {
            self.filteredColors = self.colors
            self.collectionView.reloadData()
        } else {
            self.filter(searchBar.text!)
        }
    }
    
    // MARK: Generate color cell

    
    func getImageWithColor(color: UIColor, bounds: CGRect) -> UIImage {
        let rect = bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    // MARK: Confirm and Reset
    func resetTapped (sender:UIBarButtonItem){
        self.styleFilter?.colors = []
        self.collectionView.reloadData()
    }
    
    func confirmClicked(sender: UIButton){
        filterStyleDelegate?.filterStyle(self.styles, styleFilter: self.styleFilter!)
        self.navigationController!.popViewControllerAnimated(true)
    }


}