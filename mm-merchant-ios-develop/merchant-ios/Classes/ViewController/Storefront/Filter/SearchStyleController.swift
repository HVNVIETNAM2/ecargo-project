//
//  SearchStyleController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 11/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectMapper
import PromiseKit
import AlamofireImage

class SearchStyleController : MmViewController, UISearchBarDelegate, UISearchResultsUpdating{
    var searchController: UISearchController!
    var searchString : String?
    var searchBarButtonItem : UIBarButtonItem!
    var styles : [Style]!
    var styleFilter : StyleFilter!
    var filterStyleDelegate : FilterStyleDelegate!
    var searchTerms : [SearchTerm]?
    var histories : [String]!
    var popTwice = false
    private var contentInset = UIEdgeInsetsZero;
    private final let MenuCellId = "MenuCell"
    private final let ImageMenuCellId = "ImageMenuCell"
    private final let searchButtonUIImage = "search"
    private final let searchBarcancelButton = "cancelButton"
    private final let sectionInsets = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0)
    private final let SearchCellHeight : CGFloat = 45
    private final let SearchBrandCellHeight : CGFloat = 70
    private final let SearchHeaderHeight : CGFloat = 36
    private final let MarginCenter : CGFloat = 21
    private final let LogoMarginRight : CGFloat = 10
    private final let LabelMarginTop : CGFloat = 15
    private final let LabelMarginRight : CGFloat = 30
    private final let LogoWidth : CGFloat = 44
    private final let LabelLowerMarginTop : CGFloat = 33
    private final let MarginCell : CGFloat = 5
    private final let MarginLeft : CGFloat = 20
    private final let SearchIconTag : Int = 8080
    private var isSearching : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.delegate = self
        self.collectionView!.dataSource = self
        self.collectionView!.backgroundColor = UIColor.whiteColor();
        // setup navigation menu search bar and button
        self.searchBarButtonItem = UIBarButtonItem(image: UIImage(named: searchButtonUIImage), style: .Plain, target: self, action: "showSearchBar:")
        self.navigationItem.leftBarButtonItem = self.searchBarButtonItem
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.sizeToFit()
        definesPresentationContext = true
        self.searchController.searchBar.delegate = self
        
        self.searchController.searchBar.accessibilityIdentifier = "search_style_searchbar"
        
        self.searchController.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        self.searchController.searchBar.showsCancelButton = true
        self.showSearchBar()
        self.collectionView!.registerClass(SearchMenuCell.self, forCellWithReuseIdentifier: MenuCellId)
        self.collectionView!.registerClass(ImageMenuCell.self, forCellWithReuseIdentifier: ImageMenuCellId)
        self.collectionView?.registerClass(HeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")
        if self.styleFilter == nil {
            self.styleFilter = StyleFilter()
        }
        firstly{
            return self.searchComplete("")
            }.then { _ -> Void in
                self.collectionView?.reloadData()
        }
        contentInset = self.collectionView.contentInset
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.histories = Context.getHistory()
        //resetFilter()//Remove to fix bug MM-4256
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.collectionView.contentInset = contentInset
    }
    
    func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            if let keyboardSize: CGSize =  userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size {
                let heightOfset = ((keyboardSize.height + self.collectionView.bounds.height + 64) - self.view.bounds.size.height)
                if heightOfset > 0 {
                    let edgeInset = UIEdgeInsetsMake(contentInset.top, contentInset.left,contentInset.bottom + heightOfset,  contentInset.right);
                    self.collectionView.contentInset = edgeInset
                }
            }
        }
    }

    
    //MARK: Search Controller methods
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if searchController.searchBar.text?.length < 1{
            self.searchController.searchBar.viewWithTag(SearchIconTag)?.hidden = false
            isSearching = true
        }else {
            self.searchController.searchBar.viewWithTag(SearchIconTag)?.hidden = true
            isSearching = false
        }
        if !searchController.active {
            return
        }
        
        searchString = searchController.searchBar.text
        firstly{
            return self.searchComplete(searchString!)
            }.then { _ -> Void in
                self.collectionView?.reloadData()
        }
        
    }
    
    //MARK: Search bar methods
    
    func showSearchBar(sender : UISearchBar) {
        showSearchBar()
    }
    func showSearchBar(){
        self.searchController.searchBar.alpha = 0
        navigationItem.titleView = self.searchController.searchBar
        navigationItem.setLeftBarButtonItem(nil, animated: false)
        let uiButton = self.searchController.searchBar.valueForKey(searchBarcancelButton) as! UIButton
        uiButton.setTitle(String.localize("LB_CANCEL"), forState: UIControlState.Normal)
        self.searchController.searchBar.setImage(UIImage(named: "icon_search"), forSearchBarIcon: UISearchBarIcon.Clear, state: UIControlState.Normal)
        self.searchController.searchBar.tintColor = UIColor.secondary2()
        for view in self.searchController.searchBar.subviews {
            for subsubView in view.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.borderStyle = UITextBorderStyle.None
                    textField.layer.cornerRadius = 11
                    textField.backgroundColor = UIColor.backgroundGray()
                    textField.layer.borderColor = UIColor.backgroundGray().CGColor
                    textField.layer.borderWidth = 1.0
                    textField.placeholder = String.localize("LB_SEARCH")
                }
                
            }
        }
        
        UIView.animateWithDuration(0.5, animations: {
            self.searchController.searchBar.alpha = 1
            }, completion: { finished in
                let searchImage = UIImageView(image: UIImage(named: "icon_search_on"))
                let height = self.searchController.searchBar.frame.height - 20
                searchImage.frame = CGRect(x: self.searchController.searchBar.frame.maxX - 90, y: self.searchController.searchBar.frame.minY + 10 , width: height, height: height)
                searchImage.tag = self.SearchIconTag
                self.searchController.searchBar.addSubview(searchImage)
                self.searchController.searchBar.becomeFirstResponder()
        })
    }
    
    
    //MARK: UISearchBarDelegate
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        if self.popTwice {
            self.navigationController?.popToViewController((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 3])!, animated: false)
        } else {
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.styleFilter.queryString = searchBar.text!
        Context.addHistory(searchBar.text!)
        doSearch()
    }
    
    func doSearch(){
        firstly{
            return searchStyle()
            }.then { _ -> Void in
                if self.styles.count > 0 {
                    self.resetFilter()
                    self.filterStyleDelegate.filterStyle(self.styles, styleFilter: self.styleFilter)
                    self.navigationController?.popViewControllerAnimated(false)
                } else {
                    self.navigationController?.pushViewController(NoResultViewController(), animated: false)
                }
        }
    }
    
    func resetFilter(){
        self.styleFilter.reset()
    }
    
    //MARK: Collection View methods and delegates
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            if self.searchString == ""{
                return self.histories.count
            } else {
                return 0
            }
        case 1:
            if self.searchTerms == nil {
                return 0
            }
            return self.searchTerms!.count
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(MenuCellId, forIndexPath: indexPath) as! SearchMenuCell
            cell.textLabel.text = self.histories[indexPath.row]
            if(indexPath.row == self.histories.count - 1) {
                cell.borderView.hidden = true
            }
            else {
                cell.borderView.hidden = false
            }
            return cell
            
        case 1:
            switch self.searchTerms![indexPath.row].entity{
            case "Brand":
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ImageMenuCellId, forIndexPath: indexPath) as! ImageMenuCell
                cell.setImage(self.searchTerms![indexPath.row].entityImage, imageCategory: .Brand)
                cell.upperLabel.text = self.searchTerms![indexPath.row].searchTermIn
                cell.lowerLabel.text = self.searchTerms![indexPath.row].searchTerm
                return cell
                
            case "Category":
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ImageMenuCellId, forIndexPath: indexPath) as! ImageMenuCell
                cell.setImage(self.searchTerms![indexPath.row].entityImage, imageCategory: .Merchant)
                cell.upperLabel.text = self.searchTerms![indexPath.row].searchTermIn
                cell.lowerLabel.text = self.searchTerms![indexPath.row].searchTerm
                
                return cell
                
            case "Merchant":
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ImageMenuCellId, forIndexPath: indexPath) as! ImageMenuCell
                cell.setImage(self.searchTerms![indexPath.row].entityImage, imageCategory: .Merchant)
                cell.upperLabel.text = self.searchTerms![indexPath.row].searchTermIn
                cell.lowerLabel.text = self.searchTerms![indexPath.row].searchTerm
                
                return cell
                
            default:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(MenuCellId, forIndexPath: indexPath) as! SearchMenuCell
                cell.textLabel.text = self.searchTerms![indexPath.row].searchTerm
                
                return cell
                
            }
            
        default:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(MenuCellId, forIndexPath: indexPath) as! SearchMenuCell
            return cell
        }
        
    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch indexPath.section {
            case 0:
                return CGSizeMake(self.view.frame.size.width , SearchCellHeight)
            case 1:
                if self.searchTerms != nil && (self.searchTerms![indexPath.row].entity == "Brand" || self.searchTerms![indexPath.row].entity == "Category" || self.searchTerms![indexPath.row].entity == "Merchant"){
                    return CGSizeMake(self.view.frame.size.width - MarginLeft * 2, SearchBrandCellHeight)
                }
                return CGSizeMake(self.view.frame.size.width , SearchCellHeight)
            default:
                return CGSizeMake(self.view.frame.size.width , SearchCellHeight)
                
            }
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return sectionInsets
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    //MARK: Header and Footer View for collection view
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Header", forIndexPath: indexPath) as! HeaderView
            headerView.label.formatSize(12)
            headerView.borderView.hidden = true
            switch indexPath.section{
            case 0:
                headerView.label.text = String.localize("LB_CA_RECENT_SEARCH")
                headerView.label.frame = CGRect(x: MarginLeft, y: 0 , width: headerView.frame.maxX - MarginLeft
                    * 2, height: headerView.bounds.maxY)
                headerView.backgroundColor = UIColor.backgroundGray()
                headerView.imageView.image = UIImage(named: "icon_clear_bin")
                headerView.imageView.userInteractionEnabled = true
                headerView.imageView.frame = CGRect(x: headerView.bounds.maxX - (headerView.imageView.image!.size.width + 5), y: headerView.bounds.midY - headerView.imageView.image!.size.height / 2, width: headerView.imageView.image!.size.width, height: headerView.imageView.image!.size.height)
                headerView.imageView.addGestureRecognizer(UITapGestureRecognizer(target:self, action:Selector("binTapped:")))
                break
            case 1:
                headerView.label.text = String.localize("LB_CA_TRENDING_SEARCH")
                headerView.label.frame =  CGRect(x: 20, y: headerView.bounds.minY  - 2, width: self.view.frame.size.width - 40, height: 40)
                headerView.backgroundColor = UIColor.backgroundGray()
                headerView.imageView.image = nil
                headerView.imageView.userInteractionEnabled = false
                break
            default:
                break
            }
            return headerView
            
        default:
            
            assert(false, "Unexpected element kind")
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Header", forIndexPath: indexPath) as! HeaderView
            return headerView
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section{
        case 0:
            if !isSearching{
                return CGSizeMake( 0 , 0 )
            }
            if self.histories.count > 0 {
                return CGSizeMake(self.view.frame.size.width, SearchHeaderHeight)
            }
            return CGSizeMake( 0 , 0 )
        case 1:
            return CGSizeMake(self.view.frame.size.width, SearchHeaderHeight)
        default:
            return CGSizeMake(self.view.frame.size.width, SearchCellHeight)
        }
    }
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            switch indexPath.section {
            case 0: //recently search
                self.styleFilter.queryString = self.histories[indexPath.row]
                Context.addHistory(self.styleFilter.queryString)
                break
            case 1: //hot picks
                switch self.searchTerms![indexPath.row].entity {
                case "Brand":
                    let brand = Brand()
                    brand.brandId = self.searchTerms![indexPath.row].entityId
                    brand.brandName = self.searchTerms![indexPath.row].searchTerm
                    brand.brandNameInvariant = self.searchTerms![indexPath.row].searchTermIn
                    self.styleFilter.brands = [brand]
                    break
                case "Category":
                    let cat = Cat()
                    cat.categoryId = self.searchTerms![indexPath.row].entityId
                    cat.categoryName = self.searchTerms![indexPath.row].searchTerm
                    self.styleFilter.cats = [cat]
                    break
                    
                case "Merchant":
                    let merchant = Merchant()
                    merchant.merchantId = self.searchTerms![indexPath.row].entityId
                    merchant.merchantName = self.searchTerms![indexPath.row].searchTerm
                    merchant.merchantNameInvariant = self.searchTerms![indexPath.row].searchTermIn
                    self.styleFilter.merchants = [merchant]
                    break
                    
                default:
                    self.styleFilter.queryString = self.searchTerms![indexPath.row].searchTerm
                    break
                    
                }
                Context.addHistory(self.searchTerms![indexPath.row].searchTerm)
            default:
                break
            }

            doSearch()
    }
    //MARK: Filter API Promise Call
    
    func searchStyle() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchStyle(self.styleFilter){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if let styleResponse = Mapper<SearchResponse>().map(response.result.value) {
                            if let styles = styleResponse.pageData{
                                strongSelf.styles = styles
                            } else {
                                strongSelf.styles = []
                            }
                            strongSelf.collectionView!.reloadData()
                        }
                        fulfill("OK")
                    }
                    else {
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    func searchComplete(s : String) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchComplete(s){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        Log.debug(response.result.value.dynamicType)
                        if (response.result.value as! [AnyObject]).count == 0 {
                            strongSelf.searchTerms = []
                        } else {
                            strongSelf.searchTerms = Mapper<SearchTerm>().mapArray(response.result.value) ?? []
                            //                            strongSelf.searchTerms = strongSelf.searchTerms!.sort({ $0.entity < $1.entity})
                            let brandSearchTerms = strongSelf.searchTerms?.filter({$0.entity == "Brand"})
                            let categorySearchTerms = strongSelf.searchTerms?.filter({$0.entity == "Category"})
                            let merchantSearchTerms = strongSelf.searchTerms?.filter({$0.entity == "Merchant"})
                            let residualSearchTerms = strongSelf.searchTerms?.filter({$0.entity != "Brand" && $0.entity != "Category" && $0.entity != "Merchant"})
                            strongSelf.searchTerms = brandSearchTerms! + merchantSearchTerms! + categorySearchTerms! + residualSearchTerms!
                            
                        }
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    //MARK: Bin Tapped
    func binTapped(sender : UIImageView){
        self.histories = []
        Context.setHistoryWeight([:])
        self.collectionView?.reloadData()
    }
}
