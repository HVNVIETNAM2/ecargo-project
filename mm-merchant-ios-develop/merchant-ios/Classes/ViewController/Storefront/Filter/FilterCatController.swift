//
//  FilterCatController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import AlamofireImage

class FilterCatController : MmViewController, UISearchBarDelegate{
    var searchBar = UISearchBar()
    var searchString = ""
    var cats : [Cat] = []
    var validCats : [Cat] = []
    var filteredCats : [Cat] = []
    var filterStyleDelegate : FilterStyleDelegate?
    var aggregations : Aggregations?
    var styleFilter : StyleFilter?
    var styles : [Style] = []
    var refreshControl = UIRefreshControl()
    var buttonCell = ButtonCell()
    private final let FilterCatCellId = "FilterCatCell"
    private final let KeyHeader : String = "Header"
    private var textFont = UIFont()
    private let HeaderHeight : CGFloat = 40
    private let SearchBarHeight : CGFloat = 40
    private let ButtonCellHeight : CGFloat = 60
    
    private final let MarginLeft : CGFloat = 20
    private final let LineSpacing : CGFloat = 10
    private final let CellHeight : CGFloat = 25
    private final let TextMargin : CGFloat = 30
    
    var tagSelectedCount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge.None
        self.title = String.localize("LB_CA_FILTER_CATEGORY")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        loadBrand()
        self.createRightButton(String.localize("LB_CA_RESET"), action: "resetTapped:")
        self.collectionView?.registerClass(FilterCatCell.self, forCellWithReuseIdentifier: FilterCatCellId)
        self.collectionView.registerClass(PickerCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: KeyHeader)
        self.collectionView.frame = CGRect(x: self.view.bounds.minX, y: 0, width: self.view.bounds.width, height: self.view.bounds.height - (ButtonCellHeight + (self.navigationController?.navigationBar.frame.height)! - UIApplication.sharedApplication().statusBarFrame.size.height))
        self.collectionView.backgroundColor = UIColor.whiteColor()
        self.buttonCell.frame = CGRect(x: self.view.bounds.minX, y: self.view.bounds.maxY - ButtonCellHeight - (self.navigationController?.navigationBar.frame.height)! - UIApplication.sharedApplication().statusBarFrame.size.height, width: self.view.bounds.width, height: ButtonCellHeight)
        self.view.insertSubview(self.buttonCell, aboveSubview: self.collectionView)
        self.buttonCell.layoutSubviews()
        self.buttonCell.button.setTitle(String.localize("LB_CA_SUBMIT"), forState: .Normal)
        self.buttonCell.itemLabel.text = ""
        self.buttonCell.button.addTarget(self, action: "confirmClicked:", forControlEvents: .TouchUpInside)
        self.buttonCell.backgroundColor = UIColor.whiteColor()
        self.setUpRefreshControl()
        
        let label = UILabel()
        label.formatSize(13)
        textFont = label.font
        self.createBackButton()
        
    }
    
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func loadBrand() {
        self.showLoading()
        firstly{
            return self.listBrand()
            }.then
            { _ -> Void in
                self.validCats = self.cats.filter({(self.aggregations?.categoryArray.contains($0.categoryId))!})
                self.filteredCats = self.validCats
                for cat in self.filteredCats {
                    for subCat in cat.categoryList! {
                        for selectedCat in self.styleFilter!.cats {
                            if subCat.categoryId == selectedCat.categoryId {
                                subCat.isSelected = true
                            }
                        }
                    }
                }
                self.collectionView.reloadData()
            }.always {
                self.stopLoading()
                self.refreshControl.endRefreshing()
                
            }.error { _ -> Void in
                Log.error("error")
        }
        
    }
    
    func listBrand() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchCategory(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        strongSelf.cats = Mapper<Cat>().mapArray(response.result.value) ?? []
                        strongSelf.cats = strongSelf.cats.filter({$0.categoryId != 0})
                        
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FilterCatCellId, forIndexPath: indexPath) as! FilterCatCell
        let tag = self.filteredCats[indexPath.section].categoryList![indexPath.row]
        cell.nameLabel.text = tag.categoryName
        cell.selected(tag.isSelected)
        return cell
        
    }

    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return self.filteredCats.count
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.filteredCats[section].categoryList!.count
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        var width = self.getTextWidth(filteredCats[indexPath.section].categoryList![indexPath.row].categoryName, height: CellHeight, font: textFont)
        if width > self.view.bounds.width - MarginLeft * 2 {
            width = self.view.bounds.width - MarginLeft * 2
        }
        return CGSize(width: width,height: CellHeight)
        
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        if section == self.filteredCats.count - 1 {
            return UIEdgeInsets(top: LineSpacing, left: MarginLeft, bottom: LineSpacing + ButtonCellHeight , right: MarginLeft)
        }
        return UIEdgeInsets(top: LineSpacing, left: MarginLeft, bottom: LineSpacing , right: MarginLeft)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return LineSpacing
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return LineSpacing
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: KeyHeader, forIndexPath: indexPath) as! PickerCell
            headerView.label.text = self.filteredCats[indexPath.section].categoryName
            headerView.borderView.hidden = false
            return headerView
        default:
            assert(false, "Unexpected element kind")
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: KeyHeader, forIndexPath: indexPath) as! PickerCell
            return headerView
        }
    }
    
    //MARK: Header and Footer View for collection view
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        switch collectionView {
        case self.collectionView:
            return CGSizeMake(self.view.frame.width, HeaderHeight)
        default:
            return CGSizeMake(0,0)
        }
    }

    //MARK: Collection View Delegate methods
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let tag = self.filteredCats[indexPath.section].categoryList![indexPath.row]
        tag.isSelected = !tag.isSelected
        if tag.isSelected {
            tagSelectedCount++
        }
        else {
            tagSelectedCount--
        }
        self.collectionView.reloadData()
    }
    
     //MARK : Refresh Control
    
    func setUpRefreshControl(){
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.collectionView.addSubview(refreshControl)
        self.collectionView.alwaysBounceVertical = true
    }
    
    func refresh(sender : AnyObject){
        loadBrand()
    }
    
    
    // MARK: UISearchBarDelegate
    
    private func filter(text : String!) {
        self.filteredCats = self.validCats.filter(){ $0.categoryName.lowercaseString.rangeOfString(text.lowercaseString) != nil }
        self.collectionView?.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.filter(searchBar.text!)
        searchBar.resignFirstResponder()
    }
    
    internal func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchString = searchBar.text!
        if searchString.length == 0 {
            self.filteredCats = self.validCats
            self.collectionView.reloadData()
        } else {
            self.filter(searchString)
        }
        
    }
    
    // MARK: Confirm and Reset
    func resetTapped (sender:UIBarButtonItem) {
        for cat in self.filteredCats {
            for subCat in cat.categoryList! {
                subCat.isSelected = false
            }
        }
        self.styleFilter?.cats = []
        self.collectionView.reloadData()
    }
    
    func confirmClicked(sender: UIButton){
        var selectedCats : [Cat] = []
        for cat in self.filteredCats {
            for subCat in cat.categoryList! {
                if subCat.isSelected {
                    selectedCats.append(subCat)
                }
            }
        }
        self.styleFilter?.cats = selectedCats
        filterStyleDelegate?.filterStyle(self.styles, styleFilter: self.styleFilter!)
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    func getTextWidth(text: String, height: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        Log.debug("text : \(text) width : \(boundingBox.width)")
        return boundingBox.width + TextMargin
    }
}
