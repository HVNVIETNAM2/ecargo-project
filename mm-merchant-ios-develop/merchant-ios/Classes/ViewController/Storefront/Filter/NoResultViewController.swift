//
//  NoResultViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 8/1/2016.
//  Copyright © 2016 Koon Kit Chan. All rights reserved.
//

import Foundation
class NoResultViewController : MmViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerClass(NoResultCell.self, forCellWithReuseIdentifier: "NoResultCell")
        collectionView.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(collectionView)
    }
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("NoResultCell", forIndexPath: indexPath) as! NoResultCell
        cell.label.text = String.localize("LB_CA_NO_PROD_RESULT_1")
        cell.lowerLabel.text = String.localize("LB_CA_NO_PROD_RESULT_2")
        return cell
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSize(width: self.view.frame.width, height: 80)
    }
}