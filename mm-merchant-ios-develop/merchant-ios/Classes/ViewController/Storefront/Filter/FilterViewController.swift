//
//  FilterViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 19/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import NMRangeSlider
import PromiseKit
import ObjectMapper

class FilterViewController : MmViewController, FilterStyleDelegate {
    
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    private final let CellId: String = "Cell"
    private final let FilterCellId : String = "FilterItemCell"
    private final let SliderCellId : String = "SliderCell"
    private final let BadgeCellId : String = "BadgeCell"
    private final let ButtonCellId : String = "ButtonCell"
    private final let TitleCellId : String = "TitleCell"
    static let LowestPrice : Float = 0
    static let HighestPrice : Float = Constants.Price.Highest
    private static let StepPrice : Float = 2500
    private final let BadgeCellHeight : CGFloat = 40
    private final let BadgeCellWidth : CGFloat = 100
    private final let CellHeight : CGFloat = 60
    var sliderMoved : Bool = false
    var styles : [Style]!
    var aggregations = Aggregations()
    var filterStyleDelegate : FilterStyleDelegate!
    var styleFilter : StyleFilter!
    var badges : [Badge] = []
    var buttonCell = ButtonCell()
    
    private let ButtonCellHeight : CGFloat = 60
    private let HeaderHeight : CGFloat = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: CellId)
        self.collectionView!.registerClass(FilterItemCell.self, forCellWithReuseIdentifier: FilterCellId)
        self.collectionView!.registerClass(SliderCell.self, forCellWithReuseIdentifier: SliderCellId)
        self.collectionView!.registerClass(ButtonCell.self, forCellWithReuseIdentifier: ButtonCellId)
        self.collectionView!.registerClass(TitleCell.self, forCellWithReuseIdentifier: TitleCellId)
        self.collectionView.registerClass(BadgeCell.self, forCellWithReuseIdentifier: BadgeCellId)
        self.collectionView.registerClass(HeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")
        self.collectionView.frame = CGRect(x: self.view.bounds.minX, y: self.view.bounds.minY, width: self.view.bounds.width, height: self.view.bounds.height - ButtonCellHeight)
        self.collectionView.alwaysBounceVertical = true
        self.edgesForExtendedLayout = .None
        self.view.insertSubview(self.buttonCell, aboveSubview: self.collectionView)
        
        self.buttonCell.backgroundColor = UIColor.whiteColor()
        self.buttonCell.layoutSubviews()
        self.buttonCell.button.setTitle(String.localize("LB_CA_SUBMIT"), forState: .Normal)
        self.updateItemCount()
        self.buttonCell.button.addTarget(self, action: "confirmClicked:", forControlEvents: .TouchUpInside)
        
        self.createBackButton()
        self.createRightButton(String.localize("LB_CA_RESET"), action: "resetTapped:")
        

        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: ButtonCellHeight, right: 0)
        
    }
    
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.buttonCell.frame = CGRect(x: self.view.bounds.minX, y: self.view.bounds.maxY - ButtonCellHeight, width: self.view.bounds.width, height: ButtonCellHeight)
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.title = String.localize("LB_CA_FILTER")
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
        self.collectionView?.backgroundColor = UIColor.whiteColor()
        self.navigationController!.interactivePopGestureRecognizer!.enabled = false
        self.sliderMoved = true
        firstly{
            return self.searchStyle()
            }.then
            { _  in
                return self.listBadge()
            }.then
            {_ -> Void in
                self.reloadAllData()
        }
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController!.interactivePopGestureRecognizer!.enabled = true
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 3
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
            
        case self.collectionView!:
            switch section {
            case 0:
                return 2
            case 1:
                return self.badges.count
            default:
                return 7
            }
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.collectionView!:
            switch (indexPath.section){
            case 0:
                let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(BadgeCellId, forIndexPath: indexPath) as! BadgeCell
                cell.border()
                switch indexPath.row {
                case 0:
                    cell.label.text = String.localize("LB_CA_NEW_PRODUCT_SHORT")
                    if self.styleFilter.isnew == 1 {
                        cell.highlight(true)
                    } else {
                        cell.highlight(false)
                    }
                default:
                    cell.label.text = String.localize("LB_CA_DISCOUNT")
                    if self.styleFilter.issale == 1 {
                        cell.highlight(true)
                    } else {
                        cell.highlight(false)
                    }
                }
                return cell
            case 1:
                if !self.badges.isEmpty{
                    let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier(BadgeCellId, forIndexPath: indexPath) as! BadgeCell
                    cell.highlight(self.badges[indexPath.row].isSelected)
                    cell.label.text = self.badges[indexPath.row].badgeName
                    cell.border()
                    return cell
                } else {
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
                    return cell
                }
                
            default:
                
                switch (indexPath.row){
                    
                case 0:
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(TitleCellId, forIndexPath: indexPath) as! TitleCell
                    cell.textLabel.text = String.localize("LB_CA_PRICE_RANGE")
                    return cell
                case 1:
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(SliderCellId, forIndexPath: indexPath) as! SliderCell
                    cell.slider.maximumValue = FilterViewController.HighestPrice
                    cell.slider.minimumValue = FilterViewController.LowestPrice
                    cell.slider.upperValue = Float(self.styleFilter.priceto)
                    cell.slider.lowerValue = Float(self.styleFilter.pricefrom)
                    cell.slider.stepValue = FilterViewController.StepPrice
                    cell.slider.addTarget(self, action: "sliderValueDidChange:", forControlEvents: .ValueChanged)
                    let formatter = NSNumberFormatter()
                    formatter.numberStyle = .CurrencyStyle
                    formatter.locale = NSLocale(localeIdentifier: "zh_Hans_CN")
                    cell.lowerLabel.text = formatter.stringFromNumber(self.styleFilter.pricefrom)
                    cell.upperLabel.text = formatter.stringFromNumber(self.styleFilter.priceto)
                    if self.styleFilter.priceto == Int(Constants.Price.Highest){
                        cell.upperLabel.text! += "+"
                    }
                    return cell
                    
                case 2:
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FilterCellId, forIndexPath: indexPath) as! FilterItemCell
                    cell.textLabel.text = String.localize("LB_CA_BRAND")
                    cell.selectLabel.text = ""
                    if !self.styleFilter.brands.isEmpty{
                        for brand : Brand in self.styleFilter.brands {
                            cell.selectLabel.text! += brand.brandName
                            cell.selectLabel.text! += " "
                        }
                    }
                    return cell
                case 3:
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FilterCellId, forIndexPath: indexPath) as! FilterItemCell
                    cell.textLabel.text = String.localize("LB_CA_CATEGORY")
                    cell.selectLabel.text = ""
                    if !self.styleFilter.cats.isEmpty{
                        for cat: Cat in self.styleFilter.cats{
                            cell.selectLabel.text! += cat.categoryName
                            cell.selectLabel.text! += " "
                        }
                    }
                    return cell
                case 4:
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FilterCellId, forIndexPath: indexPath) as! FilterItemCell
                    cell.textLabel.text = String.localize("LB_CA_COLOUR")
                    cell.selectLabel.text = ""
                    if !self.styleFilter.colors.isEmpty{
                        for color : Color in self.styleFilter.colors{
                            cell.selectLabel.text! += color.colorName
                            cell.selectLabel.text! += " "
                        }
                    }
                    return cell
                case 5:
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FilterCellId, forIndexPath: indexPath) as! FilterItemCell
                    cell.textLabel.text = String.localize("LB_CA_SIZE")
                    cell.selectLabel.text = ""
                    if !self.styleFilter.sizes.isEmpty{
                        for size : Size in self.styleFilter.sizes{
                            cell.selectLabel.text! += size.sizeName
                            cell.selectLabel.text! += " "
                        }
                    }
                    return cell
                    
                case 6:
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FilterCellId, forIndexPath: indexPath) as! FilterItemCell
                    cell.textLabel.text = String.localize("LB_CA_FILTER_MERCHANT")
                    cell.selectLabel.text = ""
                    if !self.styleFilter.merchants.isEmpty{
                        for merchant : Merchant in self.styleFilter.merchants{
                            cell.selectLabel.text! += merchant.merchantName
                            cell.selectLabel.text! += " "
                        }
                    }
                    return cell
                    
                default:
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
                    return cell
                }
            }
        default:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
            return cell
            
        }
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
            case self.collectionView!:
                switch indexPath.section{
                case 0:
                    return CGSizeMake(BadgeCellWidth, BadgeCellHeight)
                case 1:
                    return CGSizeMake(BadgeCellWidth, BadgeCellHeight)
                    
                default:
                    switch indexPath.row {
                    default:
                        return CGSizeMake(self.view.frame.size.width, CellHeight)
                        
                    }
                }
            default:
                return CGSizeMake(0,0)
            }
    }
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets{
        switch section {
        default:
            return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        }
    }
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            switch collectionView {
            case self.collectionView!:
                switch indexPath.section{
                case 0:
                    switch indexPath.row {
                    case 0:
                        if self.styleFilter.isnew == -1 {
                            self.styleFilter.isnew = 1
                        } else {
                            self.styleFilter.isnew  = -1
                        }
                        
                    default:
                        if self.styleFilter.issale == -1 {
                            self.styleFilter.issale = 1
                        } else {
                            self.styleFilter.issale  = -1
                        }
                    }
                    firstly{
                        return self.searchStyle()
                        }.then
                        {_ -> Void in
                            self.reloadAllData()
                    }
                    break
                case 1:
                    if self.badges[indexPath.row].isSelected{
                        self.badges[indexPath.row].isSelected = false
                    } else {
                        self.badges[indexPath.row].isSelected = true
                    }
                    self.styleFilter.badges = self.badges.filter(){$0.isSelected}
                    firstly{
                        return self.searchStyle()
                        }.then
                        {_ -> Void in
                            self.reloadAllData()
                    }
                    break
                default:
                    switch indexPath.row {
                    case 2:
                        let filterBrandController = FilterBrandController()
                        filterBrandController.filterStyleDelegate = self
                        filterBrandController.styleFilter = self.styleFilter
                        filterBrandController.aggregations = self.aggregations
                        filterBrandController.styles = self.styles
                        self.navigationController?.pushViewController(filterBrandController, animated: true)
                        break
                    case 3:
                        let filterCatController = FilterCatController()
                        filterCatController.filterStyleDelegate = self
                        filterCatController.styleFilter = self.styleFilter
                        filterCatController.aggregations = self.aggregations
                        filterCatController.styles = self.styles
                        self.navigationController?.pushViewController(filterCatController, animated: true)
                        break
                    case 4:
                        let filterColorController = FilterColorController()
                        filterColorController.filterStyleDelegate = self
                        filterColorController.aggregations = self.aggregations
                        filterColorController.styleFilter = self.styleFilter
                        filterColorController.styles = self.styles
                        self.navigationController?.pushViewController(filterColorController, animated: true)
                        break
                    case 5:
                        let filterSizeController = FilterSizeController()
                        filterSizeController.filterStyleDelegate = self
                        filterSizeController.styleFilter = self.styleFilter
                        filterSizeController.aggregations = self.aggregations
                        filterSizeController.styles = self.styles
                        self.navigationController?.pushViewController(filterSizeController, animated: true)
                        break
                    case 6:
                        let filterMerchantController = FilterMerchantController()
                        filterMerchantController.filterStyleDelegate = self
                        filterMerchantController.styleFilter = self.styleFilter
                        filterMerchantController.aggregations = self.aggregations
                        filterMerchantController.styles = self.styles
                        self.navigationController?.pushViewController(filterMerchantController, animated: true)
                        break
                    default:
                        break
                    }
                    
                }
                
            default:
                break
            }
            
    }
    
    
    //MARK:- Slider Action
    
    func sliderValueDidChange(sender : NMRangeSlider){
        self.styleFilter.pricefrom = Int(sender.lowerValue)
        self.styleFilter.priceto = Int(sender.upperValue)
        self.sliderMoved = true
        self.collectionView?.reloadData()
        if (self.styleFilter.pricefrom % Int(FilterViewController.StepPrice) == 0) && (self.styleFilter.priceto % Int(FilterViewController.StepPrice) == 0) {
            firstly{
                return self.searchStyle()
                }.then
                {_ -> Void in
                    self.reloadAllData()
            }
        }
    }
    
    //MARK:- UIBarButton Reset
    
    func resetTapped (sender: UIBarButtonItem){
        self.styleFilter.reset()
        self.sliderMoved = true
        self.reloadAllData()
        firstly{
            return self.searchStyle()
            }.then
            { _  in
                return self.listBadge()
            }.then
            {_ -> Void in
                self.reloadAllData()
        }
        
        
    }
    
    //MARK:- UIButton Click Confirm
    
    func confirmClicked(sender: UIButton){
        self.filterStyleDelegate.filterStyle(self.styles, styleFilter: self.styleFilter)
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    //MARK: FilterStyle Delegate
    func filterStyle(styles : [Style], styleFilter : StyleFilter){
        self.styles = styles
        self.styleFilter = styleFilter
        self.searchStyle()
    }
    
    //MARK: Search Style
    
    func searchStyle() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchStyle(self.styleFilter){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        
                        if let styleResponse = Mapper<SearchResponse>().map(response.result.value){
                            if let styles = styleResponse.pageData{
                                strongSelf.styles = styles
                                
                            } else {
                                strongSelf.styles = []
                                
                            }
                            if let aggregations = styleResponse.aggregations{
                                strongSelf.aggregations = aggregations
                                
                            } else {
                                strongSelf.aggregations = Aggregations()
                                
                            }
                        }
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                    
                }
            }
        }
    }
    
    //MARK: List Badge API
    func listBadge()-> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchBadge(){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        
                        strongSelf.badges = Mapper<Badge>().mapArray(response.result.value) ?? []
                        if !strongSelf.badges.isEmpty{
                            strongSelf.badges = strongSelf.badges.filter({$0.badgeId != 0})
                            strongSelf.badges = strongSelf.badges.filter({strongSelf.aggregations.badgeArray.contains($0.badgeId)})
                        }
                        for badge : Badge in strongSelf.badges{
                            for badgeSelected : Badge  in strongSelf.styleFilter.badges{
                                if badge.badgeId == badgeSelected.badgeId{
                                    badge.isSelected = true
                                }
                            }
                        }
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                    
                }
                
            }
        }
    }
    
    //MARK: Reload
    func reloadAllData(){
        self.collectionView?.reloadData()
        self.updateItemCount()
    }
    
    func updateItemCount(){
        self.buttonCell.itemLabel.text = String.localize("LB_CA_NUM_PRODUCTS_1") + String(self.styles.count) + String.localize("LB_CA_NUM_PRODUCTS_2")
    }
    
    //MARK: Header and Footer View for collection view
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        switch collectionView {
        case self.collectionView:
            switch section {
            case 1:
                return CGSizeMake(self.view.frame.width, HeaderHeight)
            default:
                return CGSizeMake(0,0)
                
            }
        default:
            return CGSizeMake(0,0)
        }
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if collectionView == self.collectionView{
            switch kind {
                
            case UICollectionElementKindSectionHeader:
                
                let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Header", forIndexPath: indexPath) as! HeaderView
                switch indexPath.section{
                case 1:
                    headerView.label.text = ""
                    break
                default:
                    break
                    
                }
                return headerView
                
            default:
                
                assert(false, "Unexpected element kind")
                let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Header", forIndexPath: indexPath) as! HeaderView
                return headerView
            }
        } else{
            assert(false, "Unexpected collection view requesting header view")
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "Header", forIndexPath: indexPath) as! HeaderView
            return headerView
            
        }
    }
    
    
}
