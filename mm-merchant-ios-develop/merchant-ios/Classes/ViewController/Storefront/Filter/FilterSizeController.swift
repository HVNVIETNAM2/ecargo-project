//
//  FilterSizeController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 25/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import AlamofireImage

class FilterSizeController : MmViewController, UISearchBarDelegate{
    //var searchBar = UISearchBar()
    var searchString = ""
    var sizes : [Size] = []
    var validSizes : [Size] = []
    var filteredSizes : [Size] = []
    var filterStyleDelegate : FilterStyleDelegate?
    var aggregations : Aggregations?
    var styleFilter : StyleFilter?
    var styles : [Style] = []
    var refreshControl = UIRefreshControl()
    var buttonCell = ButtonCell()
    var groups : [NSMutableDictionary] = []
    private final let HeaderHeight : CGFloat = 30
    private final let SizeCellHeight : CGFloat = 49
    private final let SearchBarHeight : CGFloat = 0
    private final let ButtonCellHeight : CGFloat = 60
    private final let KeyGroupName : String = "groupName"
    private final let KeyArray : String = "arraySize"
    private final let KeyHeader : String = "Header"
    private final let MarginLeft : CGFloat = 15
    private final let MinSpacing : CGFloat = 5
    private final let SectionMarginTop : CGFloat = 10
    private var lineSpacing : CGFloat = 5
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge.None
        self.title = String.localize("LB_CA_FILTER_SIZE")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.frame = CGRect(x: self.view.bounds.minX, y: SearchBarHeight, width: self.view.bounds.width, height: self.view.bounds.height - SearchBarHeight - ButtonCellHeight - (self.navigationController?.navigationBar.frame.height)! - UIApplication.sharedApplication().statusBarFrame.size.height)
        loadBrand()
        self.createBackButton()
        self.createRightButton(String.localize("LB_CA_RESET"), action: "resetTapped:")
        self.collectionView.registerClass(FilterSizeViewCell.self, forCellWithReuseIdentifier: "FilterSizeViewCell")
        self.collectionView.registerClass(PickerCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: KeyHeader)
        self.buttonCell.frame = CGRect(x: self.view.bounds.minX, y: self.view.bounds.maxY - ButtonCellHeight - (self.navigationController?.navigationBar.frame.height)! - UIApplication.sharedApplication().statusBarFrame.size.height, width: self.view.bounds.width, height: ButtonCellHeight)
        self.view.insertSubview(self.buttonCell, aboveSubview: self.collectionView)
        self.buttonCell.layoutSubviews()
        self.buttonCell.button.setTitle(String.localize("LB_CA_SUBMIT"), forState: .Normal)
        self.buttonCell.itemLabel.text = ""
        self.buttonCell.button.addTarget(self, action: "confirmClicked:", forControlEvents: .TouchUpInside)
        self.setUpRefreshControl()
        self.initCommon()
    }
    
    func initCommon() {
        let numberOfCollumn = Int((self.view.frame.maxX - (MarginLeft * 2 + MinSpacing)) / (SizeCellHeight + MinSpacing))
        lineSpacing = (self.view.frame.maxX - (CGFloat(numberOfCollumn) * SizeCellHeight)) / CGFloat(numberOfCollumn + 1)
    }
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func loadBrand() {
        self.showLoading()
        firstly{
            return self.listBrand()
            }.then
            { _ -> Void in
                self.validSizes = self.sizes.filter({(self.aggregations?.sizeArray.contains($0.sizeId))!})
                self.filteredSizes = self.validSizes
                self.groups = self.groupSizeByGroupId()
                self.collectionView.reloadData()
            }.always {
                self.stopLoading()
                self.refreshControl.endRefreshing()
                
            }.error { _ -> Void in
                Log.error("error")
        }
        
    }
    
    func listBrand() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchSize(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        strongSelf.sizes = Mapper<Size>().mapArray(response.result.value) ?? []
                        strongSelf.sizes = strongSelf.sizes.filter({$0.sizeId != 0})
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FilterSizeViewCell", forIndexPath: indexPath) as! FilterSizeViewCell
        let arraySize = self.groups[indexPath.section].objectForKey(KeyArray) as! [Size]
        if (arraySize.count > indexPath.row) {
            cell.imageView.image = nil
            cell.label.text = arraySize[indexPath.row].sizeName
            if self.styleFilter!.sizes.contains({$0.sizeId == arraySize[indexPath.row].sizeId}) {
                cell.border()
            } else {
                cell.unBorder()
            }
        }
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        switch collectionView {
        case self.collectionView!:
            return groups.count
        default:
            return 1
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.groups[section].objectForKey(KeyArray) as! [Size]).count
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        switch collectionView {
        case self.collectionView!:
            switch indexPath.row {
            default:
                return CGSizeMake(SizeCellHeight, SizeCellHeight)
            }
        default:
            return CGSizeMake(0,0)
        }
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: KeyHeader, forIndexPath: indexPath) as! PickerCell
            
            let group = self.groups[indexPath.section] as NSMutableDictionary
            headerView.label.text = group.objectForKey(KeyGroupName) as? String
            headerView.borderView.hidden = false
            return headerView
        default:
            assert(false, "Unexpected element kind")
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: KeyHeader, forIndexPath: indexPath) as! PickerCell
            return headerView
        }
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            switch collectionView {
            case self.collectionView!:
                return UIEdgeInsets(top: SectionMarginTop , left: MarginLeft , bottom: SectionMarginTop, right: MarginLeft)
            default:
                return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
            }
    }
    
    //MARK: Header and Footer View for collection view
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        switch collectionView {
        case self.collectionView:
            return CGSizeMake(self.view.frame.width, HeaderHeight)
        default:
            return CGSizeMake(0,0)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch collectionView {
        case self.collectionView!:
            return lineSpacing
        default:
            return 0.0
        }
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch collectionView {
        default:
            return lineSpacing
        }
        
    }
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            let arraySize = self.groups[indexPath.section].objectForKey(KeyArray) as! [Size]
            if self.styleFilter!.sizes.contains({$0.sizeId == arraySize[indexPath.row].sizeId}) {
                self.styleFilter!.sizes = (self.styleFilter?.sizes.filter({$0.sizeId != arraySize[indexPath.row].sizeId}))!
            } else {
                self.styleFilter!.sizes.append(arraySize[indexPath.row])
            }
            self.collectionView.reloadData()
    }
    
    //MARK : Refresh Control
    
    func setUpRefreshControl(){
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.collectionView.addSubview(refreshControl)
        self.collectionView.alwaysBounceVertical = true
    }
    
    func refresh(sender : AnyObject){
        loadBrand()
    }
    
    
    // MARK: UISearchBarDelegate
    
    private func filter(text : String!) {
        self.filteredSizes = self.validSizes.filter(){ $0.sizeName.lowercaseString.rangeOfString(text.lowercaseString) != nil }
        self.collectionView?.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.filter(searchBar.text!)
        searchBar.resignFirstResponder()
    }
    
    
    internal func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        searchString = searchBar.text!
        if searchString.length == 0 {
            self.filteredSizes = self.validSizes
            self.collectionView.reloadData()
        } else {
            self.filter(searchString)
        }
        
    }
    
    // MARK: Confirm and Reset
    func resetTapped (sender:UIBarButtonItem){
        self.styleFilter?.sizes = []
        self.collectionView.reloadData()
    }
    
    func confirmClicked(sender: UIButton){
        filterStyleDelegate?.filterStyle(self.styles, styleFilter: self.styleFilter!)
        self.navigationController!.popViewControllerAnimated(true)
    }
    func groupSizeByGroupId() -> [NSMutableDictionary] {
        self.filteredSizes.sortInPlace {(size1:Size, size2:Size) -> Bool in
            size1.sizeGroupId < size2.sizeGroupId
        }
        var groupId: Int = -1
        var arrayDictionary: [NSMutableDictionary] = []
        var arraySize: [Size] = []
        var groupDictionary = NSMutableDictionary()
        for size in self.filteredSizes {
            if groupId != size.sizeGroupId {
                Log.debug("Group Name: \(size.sizeGroupName) Group Id: \(size.sizeGroupId)")
                if(groupId != -1) {
                    arraySize.sortInPlace {(size1:Size, size2:Size) -> Bool in
                        size1.sizeId < size2.sizeId
                    }
                    groupDictionary.setObject(arraySize, forKey: KeyArray)
                    arrayDictionary.append(groupDictionary)
                }
                arraySize = []
                arraySize.append(size)
                groupDictionary = NSMutableDictionary()
                groupDictionary.setObject(size.sizeGroupName, forKey: KeyGroupName)
                groupId = size.sizeGroupId
            }
            else {
                arraySize.append(size)
            }
        }
        arraySize.sortInPlace {(size1:Size, size2:Size) -> Bool in
            size1.sizeId < size2.sizeId
        }
        groupDictionary.setObject(arraySize, forKey: KeyArray)
        arrayDictionary.append(groupDictionary)
        return arrayDictionary
    }


}