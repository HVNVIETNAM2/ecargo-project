//
//  UIImageView+MHFacebookImageViewer.h
//  FBImageViewController_Demo
//
//  Created by Jhonathan Wyterlin on 14/03/15.
//  Copyright (c) 2015 Michael Henry Pantaleon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MHFacebookImageViewer.h"

@interface UIImageView (MHFacebookImageViewer)

- (void) setupImageViewer;
- (void) setupImageViewerWithCompletionOnOpen:(MHFacebookImageViewerOpeningBlock)open onClose:(MHFacebookImageViewerClosingBlock)close;
- (void) setupImageViewerWithImageURL:(NSURL*)url;
- (void) setupImageViewerWithImageURL:(NSURL*)url rightButtonTitle:(NSString*)rightButtonTitle;
- (void) setupImageViewerWithImageURL:(NSURL *)url onOpen:(MHFacebookImageViewerOpeningBlock)open onClose:(MHFacebookImageViewerClosingBlock)close rightButtonTitle:(NSString*)rightButtonTitle;
- (void) setupImageViewerWithImage:(UIImage*) image onOpen:(MHFacebookImageViewerOpeningBlock)open onClose:(MHFacebookImageViewerClosingBlock)close rightButtonTitle:(NSString*)rightButtonTitle;

- (void) setupImageViewerWithDatasource:(id<MHFacebookImageViewerDatasource>)imageDatasource onOpen:(MHFacebookImageViewerOpeningBlock)open onClose:(MHFacebookImageViewerClosingBlock)close;
- (void) setupImageViewerWithDatasource:(id<MHFacebookImageViewerDatasource>)imageDatasource initialIndex:(NSInteger)initialIndex parentTag:(NSInteger)tag onOpen:(MHFacebookImageViewerOpeningBlock)open onClose:(MHFacebookImageViewerClosingBlock)close;
- (void)removeImageViewer;

- (void) didTap:(MHFacebookImageViewerTapGestureRecognizer*)gestureRecognizer;
- (void)didTapProfile: (MHFacebookImageViewerTapGestureRecognizer*)gestureRecognizer;
@property (retain, nonatomic) MHFacebookImageViewer *imageBrowser;
@end