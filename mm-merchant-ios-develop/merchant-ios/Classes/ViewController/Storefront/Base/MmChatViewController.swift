//
//  MmIMViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 8/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController
import Starscream
import ObjectMapper

class MmChatViewController : JSQMessagesViewController{
    var userInfo: [String: AnyObject]!
    var messages = [JSQMessage]()
    
    var outgoingBubbleImageData = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleLightGrayColor())
    var incomingBubbleImageData = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleGreenColor())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var frame = self.view.frame
        
        if self.shouldHideTabBar() {
            self.automaticallyAdjustsScrollViewInsets = false
            frame.origin.y = self.navigationController!.navigationBar.frame.maxY + self.collectionViewTopPadding()
            frame.size.height -= self.navigationController!.navigationBar.frame.maxY + self.collectionViewBottomPadding() + self.collectionViewTopPadding()
        }
        
        self.senderId = "1"
        self.senderDisplayName = "Andrew"
        
        collectionView?.registerClass(JSQMessagesCollectionViewCell.self, forCellWithReuseIdentifier: "JSQMessagesCollectionViewCell")
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let tabBar = self.navigationController?.tabBarController?.tabBar where self.shouldHideTabBar() {
            tabBar.transform = CGAffineTransformMakeTranslation(0, tabBar.bounds.height)
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didReceiveMessage:", name:"IMDidReceiveMessage", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didReceiveData:", name:"IMDidReceiveData", object: nil)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if let tabBar = self.navigationController?.tabBarController?.tabBar where self.shouldHideTabBar() {
            tabBar.transform = CGAffineTransformIdentity
        }
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView, messageDataForItemAtIndexPath indexPath: NSIndexPath) -> JSQMessageData {
        return messages[indexPath.item]
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        
        if message.senderId == self.senderId {
            cell.textView!.textColor = UIColor.blackColor()
        } else {
            cell.textView!.textColor = UIColor.whiteColor()
        }
        
        cell.textView!.linkTextAttributes = [
            NSForegroundColorAttributeName : cell.textView!.textColor!,
            NSUnderlineStyleAttributeName : [NSUnderlineStyle.StyleSingle.rawValue, NSUnderlineStyle.PatternSolid.rawValue]
        ]
        
        return cell
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource? {
        return nil
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return  outgoingBubbleImageData
        }
        
        return incomingBubbleImageData
    }
    
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        if WebSocketManager.sharedInstance.isConnected {
            let message = JSQMessage(senderId: "1", displayName: "Andrew", text: text)
            messages.append(message)
            WebSocketManager.sharedInstance.writeData(text.dataUsingEncoding(NSUTF8StringEncoding)!)
            WebSocketManager.sharedInstance.sendMessage(text)
            WebSocketManager.sharedInstance.writePing(NSData())
            self.finishSendingMessage()
        }
    }
    
    override func didPressAccessoryButton(sender: UIButton!) {
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return 0.0
    }
    
    override func collectionView(collectionView: UICollectionView, layout:JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAtIndexPath: NSIndexPath ) -> CGFloat {
        return 0.0
    }
    
    func shouldHideTabBar() -> Bool {
        return true
    }
    
    func collectionViewBottomPadding() -> CGFloat {
        return 0
    }
    
    func collectionViewTopPadding() -> CGFloat {
        return 0
    }
    
    //MARK: IM Notifcation Event
    func didReceiveMessage(notification: NSNotification){
        messages.append(notification.object as! JSQMessage)
        self.finishReceivingMessage()
    }
    
    func didReceiveData(notification: NSNotification){
        messages.append(notification.object as! JSQMessage)
        self.finishReceivingMessage()
    }
    
    //MARK: Reload methods
    func reloadAllViews(){
        self.collectionView?.reloadData()
    }
    
}