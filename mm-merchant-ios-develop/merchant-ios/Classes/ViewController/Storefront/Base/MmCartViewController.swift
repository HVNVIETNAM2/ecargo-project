//
//  MmCartViewController.swift
//  merchant-ios
//
//  Created by hungvo on 2/2/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit
import PromiseKit
import ObjectMapper

class MmCartViewController: MmViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

    // MARK: - wishlist cart
    func listWishlistItem(completion complete:(() -> Void)? = nil) -> Promise<AnyObject> {
        return CacheManager.sharedManager.listWishlistItem(completion: complete)
    }

    func addWishlistItem(merchantId: Int, styleCode: String, skuId: Int, colorKey: String?) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            WishlistService.addWishlistItem(merchantId, styleCode: styleCode, skuId: skuId, colorKey: colorKey, completion:{ (response) in
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        // cached it
                        let wishlist = Mapper<Wishlist>().map(response.result.value)
                        
                        if !Context.getAuthenticatedUser(), let cartKey = wishlist?.cartKey {
                            Context.setAnonymousWishlistKey(cartKey)
                            Log.debug("local wishlist cart key: \(cartKey)")
                        }

                        CacheManager.sharedManager.wishlist = wishlist
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        reject(error)
                    }
                } else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    func removeWishlistItem(cartItemId : Int) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            if cartItemId != NSNotFound {
                WishlistService.removeWishlistItem(cartItemId, completion:{ (response) in
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            // cached it
                            let wishlist = Mapper<Wishlist>().map(response.result.value)
                            CacheManager.sharedManager.wishlist = wishlist
                            
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }

                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    } else{
                        reject(response.result.error!)
                    }
                })
            }
        }
    }
    
    // MARK: - shopping cart
    func listCartItem() -> Promise<AnyObject> {
        return CacheManager.sharedManager.listCartItem()
    }

    func addCartItem(skuId : Int, qty : Int) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            CartService.addCartItem(skuId, qty: qty, completion: { (response) in
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        // cached it
                        let cart = Mapper<Cart>().map(response.result.value)
                        
                        if !Context.getAuthenticatedUser(), let cartKey = cart?.cartKey {
                            Context.setAnonymousShoppingCartKey(cartKey)
                            Log.debug("local shopping cart key: \(cartKey)")
                        }

                        CacheManager.sharedManager.cart = cart
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        reject(error)
                    }
                } else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    func removeCartItem(cartItemId : Int) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            CartService.removeCartItem(cartItemId, completion: {(response) in
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        // cached it
                        let cart = Mapper<Cart>().map(response.result.value)
                        CacheManager.sharedManager.cart = cart
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        reject(error)
                    }
                } else{
                    reject(response.result.error!)
                }
            })
        }
    }

    func updateCartItem(cartItemId : Int, skuId : Int, qty : Int) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            CartService.updateCartItem(cartItemId, skuId: skuId, qty: qty, completion: { (response) in
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        // cached it
                        let cart = Mapper<Cart>().map(response.result.value)
                        CacheManager.sharedManager.cart = cart
                        
                        fulfill("OK")
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        reject(error)
                    }
                } else{
                    reject(response.result.error!)
                }
            })
        }
    }
    
    func moveItemToWishlist(cartItemId : Int) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            CartService.moveItemToWishlist(cartItemId, completion:{ (response) in
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        // cached it
                        let cart = Mapper<Cart>().map(response.result.value)
                        
                        if !Context.getAuthenticatedUser(), let wishlistKey = cart?.wishlistKey {
                            Context.setAnonymousWishlistKey(wishlistKey)
                            Log.debug("local wishlist cart key: \(wishlistKey)")
                        }
                        
                        CacheManager.sharedManager.cart = cart
                        
                        self.listWishlistItem(completion: { () -> Void in
                            fulfill("OK")
                        })
                        
                    }
                    else {
                        var statusCode = 0
                        if let code = response.response?.statusCode {
                            statusCode = code
                        }
                        
                        let error = NSError(domain: "", code: statusCode, userInfo: nil)
                        reject(error)
                    }
                } else{
                    reject(response.result.error!)
                }
            })
        }
    }
}
