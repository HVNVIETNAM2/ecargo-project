//
//  MMViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 11/1/2016.
//  Copyright © 2016 Koon Kit Chan. All rights reserved.
//

import Foundation
import MBProgressHUD
import ObjectMapper
import PromiseKit
import Alamofire

class MmViewController : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    var collectionView : UICollectionView!
    
    var buttonCart : ButtonRedDot?
    var buttonWishlist : ButtonRedDot?
    var errorView : IncorrectView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        var frame = self.view.frame
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        var navigationBarMaxY = CGFloat(0)
        if let navigationController = self.navigationController where !navigationController.navigationBarHidden {
            navigationBarMaxY = navigationController.navigationBar.frame.maxY
        }
        
        var tabBarHeight = CGFloat(0)
        if let tabBarController = self.tabBarController where !shouldHideTabBar() {
            tabBarHeight = tabBarController.tabBar.bounds.height
        }
        
        frame.origin.y = navigationBarMaxY + self.collectionViewTopPadding()
        frame.size.height -= navigationBarMaxY + tabBarHeight + self.collectionViewBottomPadding() + self.collectionViewTopPadding()
                
        if shouldHaveCollectionView() {
            
            let layout: UICollectionViewFlowLayout = getCustomFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.itemSize = CGSize(width: self.view.frame.width, height: 120)
            
            
            collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
            collectionView.backgroundColor = UIColor.whiteColor()
            self.view.addSubview(collectionView)
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func getCustomFlowLayout() -> UICollectionViewFlowLayout {
        return UICollectionViewFlowLayout()
    }
    func setupNavigationBarWishlistButton() {
        let ButtonHeight = CGFloat(25)
        let ButtonWidth = CGFloat(30)
        
        buttonWishlist = ButtonRedDot(type: .Custom)
        buttonWishlist!.setImage(UIImage(named: "icon_heart_stroke"), forState: .Normal)
        buttonWishlist!.frame = CGRectMake(0, 0, ButtonWidth, ButtonHeight)
    }
    
    func setupNavigationBarCartButton() {
        let ButtonHeight = CGFloat(25)
        let ButtonWidth = CGFloat(30)
        
        buttonCart = ButtonRedDot(type: .Custom)
        buttonCart!.setImage(UIImage(named: "cart_grey"), forState: .Normal)
        buttonCart!.frame = CGRectMake(0, 0, ButtonWidth, ButtonHeight)
    }
    
    func updateButtonCartState() {
        buttonCart?.hasRedDot(CacheManager.sharedManager.hasCartItem())
    }
    
    func updateButtonWishlistState() {
        buttonWishlist?.hasRedDot(CacheManager.sharedManager.hasWishlistItem())
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func showSuccessPopupWithText(text : String) {
        if let view = self.navigationController?.view {
            let hud = MBProgressHUD.showHUDAddedTo(view, animated: true)
            hud.mode = .CustomView
            hud.opacity = 0.7
            let imageView = UIImageView(image: UIImage(named: "alert_ok"))
            imageView.contentMode = .ScaleAspectFit
            imageView.frame = CGRectMake(0, 0, 60, 60)
            hud.customView = imageView
            hud.labelText = text
            hud.hide(true, afterDelay: 1.5)
        }
    }
    
    func showFailPopupWithText(text : String) {
        if let view = self.navigationController?.view {
            let hud = MBProgressHUD.showHUDAddedTo(view, animated: true)
            hud.mode = .CustomView
            hud.opacity = 0.7
            let imageView = UIImageView(image: UIImage(named: "alert_error"))
            imageView.contentMode = .ScaleAspectFit
            imageView.frame = CGRectMake(0, 0, 60, 60)
            hud.customView = imageView
            hud.labelText = text
            hud.hide(true, afterDelay: 1.5)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let tabBar = self.navigationController?.tabBarController?.tabBar where self.shouldHideTabBar() {
            tabBar.transform = CGAffineTransformMakeTranslation(0, tabBar.bounds.height)
        }
        updateButtonCartState()
        updateButtonWishlistState()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if let tabBar = self.navigationController?.tabBarController?.tabBar where self.shouldHideTabBar() {
            tabBar.transform = CGAffineTransformIdentity
        }
    }
    
    func shouldHideTabBar() -> Bool {
        return false
    }
    
    func collectionViewBottomPadding() -> CGFloat {
        return 0
    }
    
    func collectionViewTopPadding() -> CGFloat {
        return 0
    }
    
    func shouldHaveCollectionView() -> Bool {
        return true
    }
    func handleError(response : (Response<AnyObject, NSError>), animated: Bool, reject : ((ErrorType) -> Void)? = nil) {
        if let resp = Mapper<ApiResponse>().map(response.result.value){
            if let appCode = resp.appCode {
                var msg = String.localize(appCode)
                if let range : Range<String.Index> = msg.rangeOfString("{0}") {
                    if let loginAttempts = resp.loginAttempts {
                        msg = msg.stringByReplacingCharactersInRange(range, withString: "\(Constants.Value.MaxLoginAttempts - loginAttempts)")
                    }
                }
                self.showError(msg,animated: animated)
                Log.debug(String(data: response.data!, encoding: 4))
                if let reject = reject {
                    reject(NSError(domain: "", code: response.response!.statusCode, userInfo: ["Error" : (String.localize(appCode))]))
                }
            } else {
                self.showError(String.localize("LB_ERROR"),animated: animated)
            }

        }

        
    }
    func showError(message: String, animated: Bool) {
        if errorView == nil {
            var y : CGFloat = 0
            if self.navigationController != nil && !self.navigationController!.navigationBarHidden {
                y = self.navigationController!.navigationBar.frame.maxY
            }
            errorView = IncorrectView(frame: CGRect(x: 0, y: y, width: self.view.bounds.width, height: 40))
            errorView?.displayTime = 5
            self.view.addSubview(errorView!)
        }
        errorView?.showMessage(message, animated: animated)
    }
    
    //Back button clicked event's
    func backButtonClicked()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //Override back button of navigation bar
    func createBackButton() {
        let buttonBack = UIButton(type: .Custom)
        buttonBack.setImage(UIImage(named: "back"), forState: .Normal)
        buttonBack.frame = CGRectMake(0, 0, Constants.Value.BackButtonWidth, Constants.Value.BackButtonHeight)
        buttonBack.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: Constants.Value.BackButtonMarginLeft, bottom: 0, right: 0)
        let backButtonItem = UIBarButtonItem(customView: buttonBack)
        buttonBack.addTarget(self, action: "backButtonClicked", forControlEvents: .TouchUpInside)
        self.navigationItem.leftBarButtonItem = backButtonItem
    }
    
    func createRightButton(title: String, action: Selector) {
        let rightButton = UIButton(type: UIButtonType.System)
        rightButton.setTitle(title, forState: .Normal)
        rightButton.titleLabel?.formatSmall()
        rightButton.setTitleColor( UIColor.primary1(), forState: .Normal)
        let constraintRect = CGSize(width: CGFloat.max, height: Constants.Value.BackButtonHeight)
        let boundingBox = title.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: rightButton.titleLabel!.font], context: nil)
        rightButton.frame = CGRect(x: 0, y: 0, width: boundingBox.width, height: Constants.Value.BackButtonHeight)
        rightButton.addTarget(self, action: action, forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    func promtLogin(mode : SignupMode = .Checkout) {
        let loginNavController = UINavigationController()
        let loginViewController = LoginViewController()
        loginViewController.signupMode = mode
        loginNavController.viewControllers = [loginViewController]
        loginNavController.modalPresentationStyle = .OverFullScreen
        
        self.presentViewController(loginNavController, animated: true, completion: nil)
    }
}
