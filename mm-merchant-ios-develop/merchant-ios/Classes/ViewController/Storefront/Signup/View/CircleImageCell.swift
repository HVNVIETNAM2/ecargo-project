//
//  CircleImageCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 5/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import AlamofireImage

class CircleImageCell : UICollectionViewCell{
    private final let ImageHeight: CGFloat = 22
    private final let MarginTop: CGFloat = 4
    private final let CheckMarkScale : CGFloat = 1.4
    private final let OverWidth : CGFloat = 5
    var imageView = UIImageView()
    var blurView = UIView()
    var checkboxImageView = UIImageView()
    var isAnimating : Bool = false
    var circleView : CircleView?
    var circleOverlayView = CircleOverlayView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageView)
        addSubview(blurView)
        blurView.backgroundColor = UIColor.blackColor()
        blurView.alpha = 0.0
        checkboxImageView.image = UIImage(named: "icon_keyword_selected")
        checkboxImageView.hidden = true
        
        addSubview(checkboxImageView)
        circleOverlayView.alpha = 0.0
        addSubview(circleOverlayView)
        layout()

    }

    override func layoutSubviews() {
        layout()
    }
    
    func layout(){
        imageView.frame = bounds
        imageView.round()
        blurView.frame = bounds
        blurView.round()
        if isAnimating == true {
            let width = ImageHeight * CheckMarkScale
            checkboxImageView.frame = CGRect(x: (imageView.bounds.maxX - ImageHeight) - (width - ImageHeight) / 2, y: -(width - ImageHeight) / 2, width: width, height: width)
        }
        else {
            checkboxImageView.frame = CGRect(x: imageView.bounds.maxX - ImageHeight, y: 0, width: ImageHeight, height: ImageHeight)
        }
        circleView?.frame = imageView.bounds
        circleOverlayView.frame = CGRect(x: -OverWidth, y: -OverWidth, width: bounds.width + OverWidth * 2, height: bounds.width + OverWidth * 2)
    }
    func selected(isSelected: Bool) {
        if isSelected {
            checkboxImageView.hidden = false
        }
        else {
            checkboxImageView.hidden = true
        }
        
    }
    
    func drawSelect(isSelected: Bool, selecting: Bool) {
        if isSelected {
            checkboxImageView.hidden = false
            self.addCircleView(selecting)
        }
        else {
            checkboxImageView.hidden = true
            self.removeCircleView()
        }
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImage(imageKey : String, category : ImageCategory){
        let filter = AspectScaledToFitSizeFilter(
            size: imageView.frame.size
        )
        imageView.af_setImageWithURL(ImageURLFactory.get(imageKey, category: category), placeholderImage : UIImage(named: "holder"), filter: filter, imageTransition: .CrossDissolve(0.3))


    }
    func addCircleView(animated: Bool) {
        let circleWidth = imageView.bounds.width
        let circleHeight = circleWidth
        // Create a new CircleView
        if circleView == nil {
            circleView = CircleView(frame: CGRectMake(0, 0, circleWidth, circleHeight))
            imageView.addSubview(circleView!)
        }
       
        circleView?.hidden = false
        // Animate the drawing of the circle over the course of 1 second
        if animated {
            circleView!.animateCircle(0.2)
            self.scaleCheckBoxImage()
        } else {
            circleView!.animateCircle(0.0)
        }
    }
    
    func removeCircleView() {
        circleView?.hidden = true
    }
    
    func scaleCheckBoxImage() {
       
        self.checkboxImageView.transform = CGAffineTransformMakeScale(0.1, 0.1)
        isAnimating = true
        UIView.animateWithDuration(0.25, delay: 0.0, options:
            UIViewAnimationOptions.AllowUserInteraction, animations: {
                self.checkboxImageView.transform = CGAffineTransformMakeScale(self.CheckMarkScale, self.CheckMarkScale)
            }, completion: { finished in
                self.checkboxImageView.transform = CGAffineTransformMakeScale(1.0, 1.0)
                self.isAnimating = false
            })
    }
    func centerAnimation()
    {
        self.circleOverlayView.alpha = 0.6
        UIView.animateWithDuration(0.2, delay: 0.0, options:
            UIViewAnimationOptions.CurveEaseIn, animations: {
                self.circleOverlayView.alpha = 0.3
                self.circleOverlayView.transform =  CGAffineTransformMakeScale(2.0, 2.0)
            }, completion: { finished in
                self.circleOverlayView.alpha = 0.0
//                UIView.animateWithDuration(0.1, delay: 0.0, options:
//                    UIViewAnimationOptions.CurveEaseIn, animations: {
//                        self.circleOverlayView.alpha = 0.0
//                    }, completion: { finished in
//                        
//                        
//                })
        })
    }
}