//
//  MobileSignupProfileView.swift
//  merchant-ios
//
//  Created by Sang on 2/2/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import AlamofireImage

class MobileSignupProfileView: UIView {
    private final let InputViewMarginLeft : CGFloat = 20
    private final let ShortLabelWith : CGFloat = 85
    private final let LabelHeight : CGFloat = 47
    private final let ViewAvatarWidth : CGFloat = 93
    private final let ImageAvatarWidth : CGFloat = 44
    private final let InputBoxHeight : CGFloat = 48
    private final let TextFieldMarginLeft : CGFloat = 10
    var usernameTextField = UITextField()
    var displaynameTextField = UITextField()
    var passwordTextField = UITextField()
    var passwordConfirmTextField = UITextField()
    var avatarImageView = UIImageView()
    var checkboxButton = UIButton()
    var linkButton = UIButton()
    var avatarView = UIView()
    var inputView1 = UIView()
    var inputBackground = UIImageView()
    var displaynameLabel = UILabel()
    var usernameLabel = UILabel()
    var profileLabel = UILabel()
    var inputView2 = UIView()
    var imagePassword = UIImageView()
    var imagePasswordConfirm = UIImageView()
    var imageCheckbox = UIImage(named: "square_check_box")
    var registerButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        self.clipsToBounds = true
        let profileInputImage = UIImage(named: "mobile_signup_input_profile")
        profileInputImage?.resizableImageWithCapInsets(UIEdgeInsets(top: 20, left: 90, bottom: 20, right: 200))
        inputBackground.image = profileInputImage
        inputView1.addSubview(inputBackground)
       
        usernameLabel.textAlignment = .Center
        usernameLabel.formatSize(14)
        usernameLabel.text = String.localize("LB_CA_USERNAME")
        inputView1.addSubview(usernameLabel)
        
        usernameTextField.textAlignment = .Left
        usernameTextField.textColor = UIColor.secondary2()
        usernameTextField.font = UIFont.systemFontOfSize(14)
        usernameTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        inputView1.addSubview(usernameTextField)
        usernameTextField.autocapitalizationType = .None
        usernameTextField.autocorrectionType = .No
        usernameTextField.tag = 1
        displaynameLabel.textAlignment = .Center
        displaynameLabel.formatSize(14)
        displaynameLabel.text = String.localize("LB_CA_DISPNAME")
        inputView1.addSubview(displaynameLabel)


        displaynameTextField.textAlignment = .Left
        displaynameTextField.tag = 2
        displaynameTextField.textColor = UIColor.secondary2()
        displaynameTextField.font = UIFont.systemFontOfSize(14)
        displaynameTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        inputView1.addSubview(displaynameTextField)
        displaynameTextField.autocapitalizationType = .None
        displaynameTextField.autocorrectionType = .No
        
        //Create avatar view
        avatarView.backgroundColor = UIColor.clearColor()
        avatarImageView.image = UIImage(named: "profile_avatar")
        avatarImageView.userInteractionEnabled = true;
        avatarImageView.round()
        avatarView.addSubview(avatarImageView)
        
        profileLabel.textAlignment = .Center
        profileLabel.formatSize(14)
        profileLabel.text = String.localize("LB_CA_PROFILE_PIC")
        avatarView.addSubview(profileLabel)
        inputView1.addSubview(avatarView)
        self.addSubview(inputView1)

        let inputBoxSingle = UIImage(named: "input_box_single")
        inputBoxSingle?.resizableImageWithCapInsets(UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        imagePassword.image = inputBoxSingle
        imagePasswordConfirm.image = inputBoxSingle
        inputView2.addSubview(imagePassword)
        inputView2.addSubview(imagePasswordConfirm)
        
        //Create password
        passwordTextField.textAlignment = .Left
        passwordTextField.textColor = UIColor.secondary2()
        passwordTextField.font = UIFont.systemFontOfSize(14)
        passwordTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        passwordTextField.secureTextEntry = true
        passwordTextField.tag = 3
        passwordTextField.placeholder = String.localize("LB_ENTER_PW")
        inputView2.addSubview(passwordTextField)

        //Create password confirm
        passwordConfirmTextField.textAlignment = .Left
        passwordConfirmTextField.textColor = UIColor.secondary2()
        passwordConfirmTextField.font = UIFont.systemFontOfSize(14)
        passwordConfirmTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        passwordConfirmTextField.secureTextEntry = true
        passwordConfirmTextField.placeholder = String.localize("LB_CA_CONF_PW")
        passwordConfirmTextField.tag = 4
        inputView2.addSubview(passwordConfirmTextField)
        self.addSubview(inputView2)
        
        //Create checkbox button
        checkboxButton.setImage(imageCheckbox, forState: UIControlState.Normal)
        checkboxButton.setImage(UIImage(named: "square_check_box_selected"), forState: UIControlState.Selected)
        checkboxButton.setTitle(" " + String.localize("LB_CA_TNC_CHECK"), forState: UIControlState.Normal)
        checkboxButton.setTitleColor(UIColor.secondary2(), forState: UIControlState.Normal)
        checkboxButton.titleLabel?.formatSize(14)
        checkboxButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        self.addSubview(checkboxButton)
        
        //Create link button
        linkButton.setTitle(String.localize("LB_CA_TNC_LINK"), forState: UIControlState.Normal)
        linkButton.titleLabel?.formatSize(14)
        linkButton.setTitleColor(UIColor.primary1(), forState: UIControlState.Normal)
        self.addSubview(linkButton)
        
        //Create register button
        registerButton.layer.cornerRadius = 2
        registerButton.layer.borderColor = UIColor.primary1().CGColor
        registerButton.layer.borderWidth = 1
        registerButton.titleLabel?.formatSize(14)
        registerButton.setTitleColor(UIColor.primary1(), forState: UIControlState.Normal)
        registerButton.setTitle(String.localize("LB_CA_REGISTER"), forState: UIControlState.Normal)
        self.addSubview(registerButton)
        let dismissGesture = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        self.addGestureRecognizer(dismissGesture)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        inputView1.frame = CGRect(x: InputViewMarginLeft, y: 53, width: self.bounds.width - InputViewMarginLeft * 2, height: 95)
        inputBackground.frame = inputView1.bounds
        usernameLabel.frame = CGRect(x: 0, y: 0, width: ShortLabelWith, height: LabelHeight)
        usernameTextField.frame = CGRect(x: ShortLabelWith + TextFieldMarginLeft, y: 0, width: inputView1.bounds.width - (ShortLabelWith + TextFieldMarginLeft + ViewAvatarWidth), height: LabelHeight)
        displaynameLabel.frame = CGRect(x: 0, y: usernameLabel.bounds.maxY, width: ShortLabelWith, height: LabelHeight)
        displaynameTextField.frame = CGRect(x: ShortLabelWith + TextFieldMarginLeft, y: usernameTextField.bounds.maxY, width: inputView1.bounds.width - (ShortLabelWith + TextFieldMarginLeft + ViewAvatarWidth), height: LabelHeight)
        avatarView.frame = CGRect(x: inputView1.bounds.maxX - ViewAvatarWidth, y: 0, width: ViewAvatarWidth, height: ViewAvatarWidth)
        avatarImageView.frame = CGRect(x: (ViewAvatarWidth - ImageAvatarWidth) / 2, y: 15, width: ImageAvatarWidth, height: ImageAvatarWidth)
        profileLabel.frame = CGRect(x: 0, y: avatarImageView.frame.origin.y + avatarImageView.bounds.height + 10, width: avatarView.bounds.maxX, height: 15)
        inputView2.frame = CGRect(x: InputViewMarginLeft, y: inputView1.frame.maxY + 35, width: self.bounds.width - InputViewMarginLeft * 2, height: 95)
        imagePassword.frame = CGRect(x: 0, y: 0, width: self.bounds.maxX - InputViewMarginLeft * 2, height: InputBoxHeight)
        imagePasswordConfirm.frame = CGRect(x: 0, y: imagePassword.frame.maxY + 2, width: self.bounds.maxX - InputViewMarginLeft * 2, height: InputBoxHeight)
        passwordTextField.frame = CGRect(x: TextFieldMarginLeft , y: 0, width: inputView2.bounds.width - TextFieldMarginLeft * 2, height: LabelHeight)
        passwordConfirmTextField.frame = CGRect(x: TextFieldMarginLeft , y: imagePasswordConfirm.frame.minY, width: inputView2.bounds.width - TextFieldMarginLeft * 2, height: LabelHeight)
        checkboxButton.frame = CGRect(x: InputViewMarginLeft, y: inputView2.frame.maxY + 15, width: self.bounds.maxX - InputViewMarginLeft * 2, height: 40)
        let width = self.getTextWidth((linkButton.titleLabel?.text)!, height: 40, font: (linkButton.titleLabel?.font)!)
        let checkboxWidth = self.getTextWidth((checkboxButton.titleLabel?.text)!, height: 40, font: (checkboxButton.titleLabel?.font)!)
        linkButton.frame = CGRect(x: InputViewMarginLeft + imageCheckbox!.size.width + checkboxWidth + 2 , y: checkboxButton.frame.minY, width: width, height: 40)
        registerButton.frame = CGRect(x: InputViewMarginLeft, y: linkButton.frame.maxY + 15, width: self.bounds.maxX - InputViewMarginLeft * 2, height: 42)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    
    func getTextWidth(text: String, height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.width
    }
    
    func setProfileImage(key : String){
        let filter = AspectScaledToFitSizeFilter(
            size: avatarImageView.frame.size
        )
        avatarImageView.af_setImageWithURL(ImageURLFactory.get(key, category: .User), placeholderImage : UIImage(named: "profile_avatar"), filter: filter, imageTransition: .CrossDissolve(0.3))

    }
}