//
//  TagCell.swift
//  UICollectionViewTest
//
//  Created by HVN_Pivotal on 1/28/16.
//  Copyright © 2016 HVN_Pivotal. All rights reserved.
//

import Foundation
import UIKit
class KeywordCell: UICollectionViewCell {
    private final let ImageHeight: CGFloat = 22
    private final let TextMarginLeft: CGFloat = 14
    private final let BackgroundMarginLeft: CGFloat = 2
    
    private final let MarginTop: CGFloat = 4
    var nameLabel = UILabel()
    var viewBackground = UIView()
    var checkboxImageView = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        viewBackground.backgroundColor = UIColor.clearColor()
        viewBackground.layer.borderColor = UIColor.whiteColor().CGColor
        viewBackground.layer.borderWidth = 1.0
        addSubview(viewBackground)
        
        nameLabel.formatSize(13)
        nameLabel.textColor = UIColor.whiteColor()
        nameLabel.textAlignment = .Center

        nameLabel.numberOfLines = 1
        nameLabel.lineBreakMode = .ByTruncatingTail
        addSubview(nameLabel)
        checkboxImageView.image = UIImage(named: "icon_keyword_selected")
        checkboxImageView.hidden = true
        addSubview(checkboxImageView)
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        nameLabel.frame = CGRect(x: TextMarginLeft , y: 0, width: self.bounds.maxX - TextMarginLeft * 2, height: self.bounds.maxY)
        viewBackground.frame = CGRect(x: BackgroundMarginLeft, y: MarginTop, width: self.bounds.maxX - BackgroundMarginLeft * 2, height: self.bounds.maxY - MarginTop)
        checkboxImageView.frame = CGRect(x: self.bounds.maxX - (ImageHeight - MarginTop), y: 0, width: ImageHeight, height: ImageHeight)
    }
    func selected(isSelected: Bool) {
        if isSelected {
            nameLabel.textColor = UIColor.secondary2()
            viewBackground.backgroundColor = UIColor.whiteColor()
            checkboxImageView.hidden = false
        }
        else {
            nameLabel.textColor = UIColor.whiteColor()
            viewBackground.backgroundColor = UIColor.clearColor()
            checkboxImageView.hidden = true
        }
    }
    
}