//
//  IncorrectView.swift
//  merchant-ios
//
//  Created by Sang on 2/2/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class IncorrectView: UIView {
    var backgroundView = UIView()
    var messageLabel = UILabel()
    var displayTime : Double = 3 //Default value is 3 seconds, this value can be set outside this class
    var isAnimated = false
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        self.clipsToBounds = true
        backgroundView.backgroundColor = UIColor.primary1()
        backgroundView.frame = CGRect(x: 0, y: -self.bounds.height, width: self.bounds.width, height: self.bounds.height)
        messageLabel.formatSize(14)
        messageLabel.textColor = UIColor.whiteColor()
        messageLabel.textAlignment = .Center
        messageLabel.frame = backgroundView.bounds
        backgroundView.addSubview(messageLabel)
        self.addSubview(backgroundView)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    func showMessage(message: String, animated: Bool) {
        isAnimated = animated
        messageLabel.text = message
        var frame = backgroundView.frame
        frame.origin.y = 0
        if isAnimated {
            UIView.animateWithDuration(
                0.2,
                animations: { () -> Void in
                    self.backgroundView.frame = frame
                },
                completion: { (success) in }
            )
        }
        else {
            backgroundView.frame = frame
        }
        NSTimer.scheduledTimerWithTimeInterval(displayTime, target: self, selector: "hideMessage", userInfo: nil, repeats: false)
    }
    func hideMessage() {
        var frame = backgroundView.frame
        frame.origin.y = -self.bounds.height
        if isAnimated {
            UIView.animateWithDuration(
                0.2,
                animations: { () -> Void in
                    self.backgroundView.frame = frame
                },
                completion: { (success) in }
            )
        }
        else {
            backgroundView.frame = frame
        }
    }
}