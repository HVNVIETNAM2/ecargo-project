//
//  SelectCountryCell.swift
//  merchant-ios
//
//  Created by Sang on 2/4/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class SelectCountryCell : UICollectionViewCell{
    var countryNameLabel = UILabel()
    var countryCodeLabel = UILabel()
    var separatorView  = UIView()
    private final let LabelMarginLeft : CGFloat = 20
    private final let CountryCodeWidth : CGFloat = 60
    override init(frame: CGRect) {
        super.init(frame: frame)
        countryNameLabel.formatSize(14)
        addSubview(countryNameLabel)
        countryCodeLabel.formatSize(14)
        countryCodeLabel.textAlignment = .Right
        addSubview(countryCodeLabel)
        separatorView.backgroundColor = UIColor.secondary1()
        addSubview(separatorView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
    
    func layout(){
        countryNameLabel.frame = CGRect(x: bounds.minX + LabelMarginLeft, y: bounds.minY, width: bounds.width - (LabelMarginLeft * 2 + CountryCodeWidth), height: self.bounds.height)
        countryCodeLabel.frame = CGRect(x: bounds.maxX - (LabelMarginLeft + CountryCodeWidth), y: bounds.minY, width: CountryCodeWidth, height: self.bounds.height)
        separatorView.frame = CGRect(x: 0, y: bounds.height - 1, width: bounds.width, height: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}