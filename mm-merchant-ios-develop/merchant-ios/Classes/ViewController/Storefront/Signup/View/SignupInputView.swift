//
//  SignupInputView.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 2/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class SignupInputView : UIView{
    var inputBackground = UIImageView ()
    var countryLabel = UILabel()
    var codeTextField = UITextField()
    var countryButton = UIButton()
    var mobileNumberTextField = UITextField()
    private final let ShortLabelWidth : CGFloat = 75
    private final let LabelHeight : CGFloat = 46
    override init(frame: CGRect) {
        super.init(frame: frame)
        inputBackground.image = UIImage(named: "mobile_signup_input")
        addSubview(inputBackground)
        countryLabel.textAlignment = .Center
        countryLabel.formatSize(14)
        countryLabel.text = String.localize("LB_CA_IDD_REGION")
        addSubview(countryLabel)
        codeTextField.textAlignment = .Center
        addSubview(codeTextField)
        addSubview(countryButton)
        addSubview(mobileNumberTextField)
        countryButton.formatTransparent()
        countryButton.layer.borderWidth = 0
        countryButton.titleLabel?.formatSize(14)
        countryButton.setTitleColor(UIColor.secondary2(), forState: .Normal)
        countryButton.contentHorizontalAlignment = .Left
        countryButton.setTitle(String.localize("LB_COUNTRY_PICK"), forState: .Normal)
        mobileNumberTextField.keyboardType = .PhonePad
        mobileNumberTextField.formatTransparent()
        codeTextField.keyboardType = .NumberPad
        codeTextField.formatTransparent()
        codeTextField.tag = 1
        mobileNumberTextField.tag = 2
        layout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
    
    func layout(){
        inputBackground.frame = bounds
        countryLabel.frame = CGRect(x: 0, y: 0, width: ShortLabelWidth, height: LabelHeight)
        codeTextField.frame = CGRect(x: 0, y: LabelHeight, width: ShortLabelWidth, height: LabelHeight)
        countryButton.frame = CGRect(x: ShortLabelWidth + 30, y: 0, width: bounds.width - ShortLabelWidth, height: LabelHeight)
        mobileNumberTextField.frame = CGRect(x: ShortLabelWidth + 15 , y: LabelHeight, width: bounds.width - ShortLabelWidth, height: LabelHeight)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}