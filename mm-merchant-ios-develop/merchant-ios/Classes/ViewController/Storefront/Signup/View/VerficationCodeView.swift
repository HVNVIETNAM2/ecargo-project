//
//  VerficationCodeView.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 2/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class VerificationCodeView : UIView{
    var textfield = UITextField()
    var button = UIButton()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(textfield)
        textfield.format()
        textfield.placeholder = String.localize("LB_CA_INPUT_VERCODE")
        textfield.keyboardType = .NumberPad
        button.formatPrimary()
        addSubview(button)
        button.setTitle(String.localize("LB_CA_REGISTER"), forState: .Normal)
        layout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
    
    func layout(){
        textfield.frame = CGRect(x: bounds.minX, y: bounds.minY, width: bounds.width, height: 46)
        button.frame = CGRect(x: bounds.minX, y: bounds.minY + 46 + 20, width: bounds.width, height: 42)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}