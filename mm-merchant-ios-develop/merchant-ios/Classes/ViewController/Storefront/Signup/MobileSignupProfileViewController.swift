//
//  MobileSignupProfileViewController.swift
//  merchant-ios
//
//  Created by Sang on 2/2/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import Alamofire

class MobileSignupProfileViewController: SignupModeViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, CropImageViewControllerDelegate {
  
    private var mobileSignupProfileView : MobileSignupProfileView!

    var picker = UIImagePickerController()
    var profileImage = UIImage()

    var mobileNumber = ""
    var mobileCode = ""
    var mobileVerificationId = ""
    var mobileVerficationToken = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mobileSignupProfileView = MobileSignupProfileView(frame: CGRect(x: 0, y: 65, width: self.view.bounds.width, height: 400))
        mobileSignupProfileView.linkButton.addTarget(self, action: "linkClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        mobileSignupProfileView.checkboxButton.addTarget(self, action: "checkboxClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        mobileSignupProfileView.registerButton.addTarget(self, action: "registerClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        mobileSignupProfileView.registerButton.enabled = false
        mobileSignupProfileView.usernameTextField.delegate = self
        mobileSignupProfileView.displaynameTextField.delegate = self
        mobileSignupProfileView.passwordConfirmTextField.delegate = self
        mobileSignupProfileView.passwordTextField.delegate = self
        var gesture = UITapGestureRecognizer(target: self, action: "avatarClicked")
        mobileSignupProfileView.avatarImageView.addGestureRecognizer(gesture)
        self.view.addSubview(mobileSignupProfileView)
        self.title = String.localize("LB_CA_REGISTER")
        picker.delegate = self
        
        gesture = UITapGestureRecognizer(target: self, action: "dissmissKeyboard")
        self.view.addGestureRecognizer(gesture)
        mobileSignupProfileView.addGestureRecognizer(gesture)
        self.createBackButton()
    }
    
    func linkClicked(sender : UIButton) {
        self.navigationController?.pushViewController(TncViewController(), animated: true)
        self.dissmissKeyboard()
    }
    
    func registerClicked(sender : UIButton) {
        self.dissmissKeyboard()
        Log.debug("registerClicked")
        firstly{
            return self.signup()
        }.then { _ in
            return self.uploadProfileImage()
        }.then { _ -> Void in
            
            let viewController: UIViewController
            
            if self.signupMode == .Checkout || self.signupMode == .CheckoutSwipeToPay {
                // handle the sign up mode form check out
                viewController = AddressAdditionViewController()
                (viewController as! AddressAdditionViewController).signupMode =  self.signupMode
            } else {
                viewController = InterestKeywordViewController()
            }
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func checkboxClicked(sender : UIButton) {
        sender.selected = !sender.selected
        self.checkValidData()
        self.dissmissKeyboard()
    }
    
    
    func avatarClicked()
    {
        Log.debug("avatarClicked")
        changeProfileImage()
        
    }
    func signup()-> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            var parameters = [String : AnyObject]()
            parameters["MobileNumber"] = self.mobileNumber
            parameters["MobileCode"] = self.mobileCode
            parameters["MobileVerificationId"] = self.mobileVerificationId
            parameters["MobileVerificationToken"] =  self.mobileVerficationToken
            parameters["UserName"] = self.mobileSignupProfileView.usernameTextField.text
            parameters["Password"] = self.mobileSignupProfileView.passwordConfirmTextField.text
            parameters["DisplayName"] = self.mobileSignupProfileView.displaynameTextField.text
            AuthService.signup(parameters) { [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                            if response.response!.statusCode == 200 {
                                if let token = Mapper<Token>().map(response.result.value){
                                    LoginManager.login(token, username: strongSelf.mobileSignupProfileView.usernameTextField.text!, password: strongSelf.mobileSignupProfileView.passwordConfirmTextField.text!)
                                }
                                strongSelf.view.endEditing(true)
                                fulfill("OK")

                        } else{
                            strongSelf.handleError(response, animated: true, reject: reject)

                        }
                    } else {
                        reject(response.result.error!)
                    }
                }
                
            }
        }
    }

    //# MARK: - Photo Methods
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            picker.cameraDevice = UIImagePickerControllerCameraDevice.Front
            self.presentViewController(picker, animated: true, completion: nil)
        }else {
            Alert.alert(self, title: "Camera not found", message: "Cannot access the front camera. Please use photo gallery instead.")
        }
    }
    
    func openGallery(){
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            self.presentViewController(picker, animated: true, completion: nil)
        } else {
            Alert.alert(self, title: "Tablet not suported", message: "Tablet is not supported in this function")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        profileImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        profileImage = profileImage.normalizedImage()
       
        
        let controller = CropImageViewController()
        controller.isSquare = true;
        controller.rightButtonTitle = String.localize("LB_DONE");
        controller.delegate = self;
        controller.imgEdit = profileImage.copy() as! UIImage;
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true) { () -> Void in
        }
    }
    
    func uploadProfileImage(){
        if profileImage.size.width > 0 { //Make sure user did choose image
            Alamofire.upload(
                .POST,
                Constants.Path.Host + "/user/upload/profileimage", headers: Context.getTokenHeader(),
                multipartFormData: { multipartFormData in
                    multipartFormData.appendBodyPart(data: UIImageJPEGRepresentation(self.profileImage, 0.1)!, name: "file", fileName: "iosFile.jpg", mimeType: "image/jpg")
                },
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .Success(let upload, _, _):
                        upload.responseJSON { response in
                            Log.debug(String(data: response.data!, encoding: 4))
                            if let imageUploadResponse = Mapper<ImageUploadResponse>().map(response.result.value) {
                                self.mobileSignupProfileView.setProfileImage(imageUploadResponse.profileImage)
                            }
                            
                            
                        }
                    case .Failure(_): break
                    }
                }
            )
        }
    }
    func changeProfileImage(){
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let deleteAction = UIAlertAction(title: String.localize("LB_TAKE_PHOTO"), style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        let saveAction = UIAlertAction(title: String.localize("LB_PHOTO_LIBRARY"), style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        let cancelAction = UIAlertAction(title: String.localize("LB_CANCEL"), style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    func checkValidData() {
        var isValid = true
        if mobileSignupProfileView.usernameTextField.text?.length < 1 {
            isValid = false
        }
        else if mobileSignupProfileView.displaynameTextField.text?.length < 1 {
            isValid = false
        }
        else if mobileSignupProfileView.passwordTextField.text?.length < 1 {
            isValid = false
        }
        else if mobileSignupProfileView.passwordConfirmTextField.text?.length < 1 {
            isValid = false
        }
        else if mobileSignupProfileView.passwordConfirmTextField.text != mobileSignupProfileView.passwordTextField.text {
            isValid = false
        }
        else if !mobileSignupProfileView.checkboxButton.selected {
            isValid = false
        }
        self.enableRegisterButton(isValid)
    }
    
    func enableRegisterButton(isEnable: Bool) {
        if isEnable {
            mobileSignupProfileView.registerButton.backgroundColor = UIColor.primary1()
            mobileSignupProfileView.registerButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            mobileSignupProfileView.registerButton.enabled = true
        }
        else {
            mobileSignupProfileView.registerButton.backgroundColor = UIColor.whiteColor()
            mobileSignupProfileView.registerButton.setTitleColor(UIColor.primary1(), forState: UIControlState.Normal)
            mobileSignupProfileView.registerButton.enabled = false
        }
    }
    
    func dissmissKeyboard() {
        self.view.endEditing(true)
    }
    //MARK: UITextFieldDelegate
    func textFieldDidEndEditing(textField: UITextField) {
        self.checkValidData()
        if textField.tag == 1 {
            if mobileSignupProfileView.usernameTextField.text?.length < 1 {
                self.showError(String.localize("MSG_ERR_CA_USERNAME_NIL"), animated: true)
                self.enableRegisterButton(false)
            }
            if mobileSignupProfileView.usernameTextField.text!.length > 0 && RegexManager.matchesForRegexInText("^[a-zA-Z]\\w{0,50}$", text: mobileSignupProfileView.usernameTextField.text).isEmpty {
                self.showError(String.localize("MSG_ERR_CA_USERNAME_PATTERN"), animated: true)
                self.enableRegisterButton(false)
            }
        }
        if textField.tag == 2 {
            if mobileSignupProfileView.displaynameTextField.text?.length < 1 {
                self.showError(String.localize("MSG_ERR_CA_USERNAME_NIL"), animated: true)
                self.enableRegisterButton(false)
            }
        }
        if textField.tag == 3 {
            if mobileSignupProfileView.passwordTextField.text?.length < 1 {
                self.showError(String.localize("MSG_ERR_CA_PW_NIL"), animated: true)
                self.enableRegisterButton(false)
            }
            if mobileSignupProfileView.passwordTextField.text!.length > 0 && RegexManager.matchesForRegexInText("(?=^.{8,50}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\\s).*$", text: mobileSignupProfileView.passwordTextField.text).isEmpty {
                self.showError(String.localize("MSG_ERR_CA_PW_PATTERN"), animated: true)
                self.enableRegisterButton(false)
            }
        }
        if textField.tag == 4 {
            if mobileSignupProfileView.passwordConfirmTextField.text?.length < 1 {
                self.showError(String.localize("MSG_ERR_CA_PW_NIL"), animated: true)
                self.enableRegisterButton(false)
            }
            if mobileSignupProfileView.passwordConfirmTextField.text!.length > 0 && RegexManager.matchesForRegexInText("(?=^.{8,50}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\\s).*$", text: mobileSignupProfileView.passwordConfirmTextField.text).isEmpty {
                self.showError(String.localize("MSG_ERR_CA_PW_PATTERN"), animated: true)
                self.enableRegisterButton(false)
            }
        }
        if textField.tag == 3 || textField.tag == 4 {
            if mobileSignupProfileView.passwordConfirmTextField.text!.length > 0
                && mobileSignupProfileView.passwordTextField.text!.length > 0
                && mobileSignupProfileView.passwordConfirmTextField.text != mobileSignupProfileView.passwordTextField.text {
                self.showError(String.localize("MSG_ERR_CA_CFM_PW_NOT_MATCH"), animated: true)
                self.enableRegisterButton(false)

            }
        }
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        self.enableRegisterButton(false)
        return true
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "checkValidData", userInfo: nil, repeats: false)
        return true
    }
    
    //MARK: CropImageViewControllerDelegate
    func didCropedImage(imageCroped: UIImage!) {
        profileImage = imageCroped
        self.mobileSignupProfileView.avatarImageView.round()
        self.mobileSignupProfileView.avatarImageView.image = profileImage.af_imageAspectScaledToFillSize(self.mobileSignupProfileView.avatarImageView.frame.size)
    }
}