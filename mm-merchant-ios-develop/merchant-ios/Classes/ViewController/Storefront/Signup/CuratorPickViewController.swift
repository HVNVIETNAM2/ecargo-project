//
//  CuratorPickViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 5/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import KDCircularProgress
import PromiseKit
import ObjectMapper

class CuratorPickViewController : MmViewController {
    var circleProgressView : KDCircularProgress!
    var backgroundImageView : UIImageView!
    var curators : [User] = []
    var selectedCuratorCount = 0
    var rowCentered = -1
    var distanceRatio = CGFloat(1)
    var fansLabel : FansLabel!
    var countLabel = UILabel()
    private let continueButton = UIButton()
    private var titleTextAttributes : [String: AnyObject]!
    private var navigationBGImage : UIImage!
    private var navigationShadowImage : UIImage!
    private var navigationTranslucent : Bool = false
    private var navigationBGColor : UIColor!
    private final let CircleImageCellHeight : CGFloat = 60
    private final let CircleExpansion : CGFloat = 60
    private final let ContinueButtonHeight : CGFloat = 50
    private final let MinCuratorCount = 4
    private final let MinFollower = 10000 //TODO Will be define later
    private final let CircleWithPercent : CGFloat = 0.8
    private final let DefaultCuratorNumber : Int = 5
    private final let MarginLeft : CGFloat = 10
    private final let SpacingLine : CGFloat = 30
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImageView = UIImageView(frame: self.view.frame)
        backgroundImageView.image = UIImage(named: "curator_bg")
        self.view.insertSubview(backgroundImageView, belowSubview : collectionView)
        collectionView.backgroundColor = UIColor.clearColor()
        collectionView.frame = CGRect(x: self.view.frame.midX - (CircleImageCellHeight + CircleExpansion) / 2  - MarginLeft, y: 50, width: CircleImageCellHeight + CircleExpansion + MarginLeft * 2 , height: self.view.frame.height - 100)
        collectionView.center = self.view.center
        collectionView.alwaysBounceVertical = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.registerClass(CircleImageCell.self, forCellWithReuseIdentifier: "CircleImageCell")
        
        //setupCircleProgressView() remove center circle 
        setupLabels()//Lebel should front of circle
        setupContinueButton()
        couldContinue(false)
        loadCurator()
        self.createBackButton()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.title = String.localize("LB_CA_CURATORS_FOLLOW")
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.backupNavigationBar()
        self.setupNavigationBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.revertNavigationBar()
    }
    
    //MARK: Navigation Bar methods
    func backupNavigationBar() {
        titleTextAttributes = self.navigationController!.navigationBar.titleTextAttributes
        navigationBGImage = self.navigationController!.navigationBar.backgroundImageForBarMetrics(UIBarMetrics.Default)
        navigationShadowImage = self.navigationController!.navigationBar.shadowImage
        navigationTranslucent = self.navigationController!.navigationBar.translucent
        navigationBGColor = self.navigationController!.view.backgroundColor
    }
    func revertNavigationBar() {
        self.navigationController!.navigationBar.titleTextAttributes = titleTextAttributes
        self.navigationController!.navigationBar.setBackgroundImage(navigationBGImage, forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = navigationShadowImage
        self.navigationController!.navigationBar.translucent = navigationTranslucent
        self.navigationController!.view.backgroundColor = navigationBGColor
    }
    func setupNavigationBar() {
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
        self.navigationItem.setHidesBackButton(false, animated:false);
    }
    
    func setupContinueButton(){
        continueButton.frame = CGRect(x: 0, y: self.view.bounds.maxY - ContinueButtonHeight, width: self.view.bounds.width, height: ContinueButtonHeight)
        continueButton.layer.backgroundColor = UIColor.primary1().CGColor
        continueButton.setTitle(String.localize("LB_CA_FOLLOW_CURATORS_CONT"), forState: .Normal)
        continueButton.addTarget(self, action: "continueClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(continueButton)
    }
    
    func setupCircleProgressView(){
        
        let with = self.view.bounds.width * CircleWithPercent
        
        circleProgressView = KDCircularProgress(frame: CGRect(x: self.view.bounds.center.x - with / 2, y: self.view.bounds.center.y - with / 2, width: with , height: with))
        
        self.view.insertSubview(circleProgressView, aboveSubview: backgroundImageView )
        circleProgressView.progressColors = [UIColor.sparkingRed()]
        circleProgressView.roundedCorners = true
        circleProgressView.trackColor = UIColor.whiteColor().colorWithAlphaComponent(0.1)
        circleProgressView.progressThickness = 0.2 //Follow sketch file
        circleProgressView.trackThickness = 0.2 //Follow sketch file
        circleProgressView.startAngle = -90
    }
    
    func setupLabels(){
        fansLabel = FansLabel(frame: CGRect(x: self.view.frame.midX + (CircleImageCellHeight + CircleExpansion) / 2 + 5 , y: self.view.frame.midY - 20, width: self.view.frame.midX - (CircleImageCellHeight + CircleExpansion) / 2, height: 60))
        self.fansLabel.bottomLabel.hidden = true //Hide it temporary, will show again in future.
        self.view.insertSubview(fansLabel, aboveSubview: collectionView )
        countLabel.frame = CGRect(x: 0, y: 76, width: self.view.frame.width, height: 17)
        countLabel.formatSize(14)
        countLabel.textColor = UIColor.whiteColor()
        countLabel.textAlignment = .Center
        countLabel.backgroundColor = UIColor.clearColor()
        countLabel.text = "\(String.localize("LB_CA_RECOMMENDED_CURATOR_PREFIX"))0\(String.localize("LB_CA_RECOMMENDED_CURATOR_PROFIX"))"
        self.view.insertSubview(countLabel, aboveSubview: collectionView )


    }
    
    func scrollToFirstItem () {
        self.collectionView.setContentOffset(CGPoint(x: 0,y: -(self.view.frame.height / 2 - (self.CircleImageCellHeight + self.SpacingLine)) ), animated: true)
    }
    
    func loadCurator() {
        self.showLoading()
        firstly{
            return self.listCurator()
            }.then
            { _ -> Void in
                self.collectionView.contentInset = UIEdgeInsets(top: self.collectionView.frame.height / 2 , left: 0, bottom: self.collectionView.frame.height / 2, right: 0)
                self.reloadAllData()
                self.scrollToFirstItem()
                self.createRightButton(String.localize("LB_CA_FOLLOWING_SKIP"), action: "skipButtonClicked:")
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
        
    }
    //MARK: Promise Call
    func listCurator() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            TagService.listCurator(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        strongSelf.curators = Mapper<User>().mapArray(response.result.value) ?? []
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    func saveCurator(curators : [User])-> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FollowService.saveCurator(curators){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.response?.statusCode == 200 {
                        Log.debug(String(data: response.data!, encoding : 4))
                        strongSelf.navigationController?.pushViewController(MerchantPickViewController(), animated: true)
                        fulfill("OK")
                    } else {
                        strongSelf.handleError(response, animated: true)
                    }

                }
            }
        }
    }

    
    //MARK: Collection View Delegates
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CircleImageCell", forIndexPath: indexPath) as! CircleImageCell
        if (self.curators.count > indexPath.row) {
            cell.setImage(self.curators[indexPath.row].profileImage, category: .User)
        }
        if indexPath.row == rowCentered {
            cell.imageView.alpha = 1.0
        } else {
            if indexPath.row == rowCentered + 1 || indexPath.row == rowCentered - 1 {
                cell.imageView.alpha = 0.6
            } else {
                if indexPath.row == rowCentered + 2 || indexPath.row == rowCentered - 2 {
                    cell.imageView.alpha = 0.3
                } else {
                    cell.imageView.alpha = 0.1

                }
            }
        }
        cell.drawSelect(curators[indexPath.row].isSelected,selecting: curators[indexPath.row].isClicking)
        curators[indexPath.row].isClicking = false
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return curators.count
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
            case self.collectionView!:
                switch indexPath.row {
                case rowCentered:
                    let with = CircleImageCellHeight + (1 - distanceRatio) * CircleExpansion
                    return CGSizeMake(with, with)
                default:
                    return CGSizeMake(CircleImageCellHeight, CircleImageCellHeight)
                }
            default:
                return CGSizeMake(0,0)
            }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
        return SpacingLine
    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
          return UIEdgeInsets(top: 0, left: MarginLeft, bottom: 0, right: MarginLeft)
    }
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {

            if indexPath.row != rowCentered {
                return;
            }
            curators[indexPath.row].isSelected = !curators[indexPath.row].isSelected
            if curators[indexPath.row].isSelected {
                selectedCuratorCount++
                curators[indexPath.row].isClicking = true
            } else {
                selectedCuratorCount--
                curators[indexPath.row].isClicking = false
            }
            //updateProgress() //Remove center circle
            couldContinue(selectedCuratorCount >= MinCuratorCount)
            self.reloadAllData()
    }
    
    func reloadAllData(){
        self.collectionView.reloadData()
        if rowCentered >= 0 {
            self.fansLabel.topLabel.text = curators[rowCentered].lastName + curators[rowCentered].firstName
            self.fansLabel.midLabel.text = curators[rowCentered].displayName
            let followerCount = curators[rowCentered].count * 9999
            if followerCount < MinFollower {
                self.fansLabel.bottomLabel.text = ""
            } else {
                let formatter = NSNumberFormatter()
                formatter.numberStyle = .DecimalStyle
                formatter.locale = NSLocale(localeIdentifier: "zh_Hans_CN")
                formatter.maximumFractionDigits = 0
                self.fansLabel.bottomLabel.text = "\(String.localize("LB_CA_CURATORS_FOLLOWER_NO"))" + formatter.stringFromNumber(followerCount)!
            }
        }

        let formatter = NSNumberFormatter()
        formatter.numberStyle = .DecimalStyle
        formatter.locale = NSLocale(localeIdentifier: "zh_Hans_CN")
        formatter.maximumFractionDigits = 0
        self.countLabel.text = "\(String.localize("LB_CA_RECOMMENDED_CURATOR_PREFIX"))\(formatter.stringFromNumber(curators.count)!)\(String.localize("LB_CA_RECOMMENDED_CURATOR_PROFIX"))"
        
    }
    
    //MARK: Scroll View Method to control page control
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let cellHeight = (CircleImageCellHeight + CircleExpansion) / 2 + SpacingLine
        var itemIndex = Int(ceil( ((self.collectionView.frame.height / 2 - cellHeight) + scrollView.contentOffset.y) / cellHeight))
        if itemIndex < 0 {
            itemIndex = 0
        }
        if itemIndex > curators.count - 1 {
            itemIndex = curators.count - 1
        }
        if distanceRatio < 0.3 {
            
            let cell = self.collectionView.cellForItemAtIndexPath(NSIndexPath(forItem:  itemIndex, inSection: 0))
            if cell != nil {
                rowCentered = itemIndex
                (cell as! CircleImageCell).centerAnimation()
            }
            else {
                rowCentered = -1
            }
            
        }
        else {
            rowCentered = -1
        }
        
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if scrollView == collectionView{
            var point : CGPoint = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds))
            point = view.convertPoint(point, toView: collectionView)
            let indexPath = collectionView.indexPathForItemAtPoint(point)

            if indexPath != nil {
                if rowCentered != indexPath!.row {
                    rowCentered = indexPath!.row
                    self.reloadAllData()
                }
                let myPoint : CGPoint = (collectionView.layoutAttributesForItemAtIndexPath(indexPath!))!.center
                let distance = myPoint.y - point.y
                distanceRatio = distance / ((CircleImageCellHeight + (1 - distanceRatio) * CircleExpansion) / 2)
                if distanceRatio < 0 {
                    distanceRatio = -distanceRatio
                }
                if distanceRatio > 1 {
                    distanceRatio = 1
                }
                collectionView.collectionViewLayout.invalidateLayout()
                
            }
        }
    }

    //MARK : Override parent class
    override func getCustomFlowLayout() -> UICollectionViewFlowLayout{
        let layout = CenterCellCollectionViewFlowLayout()
        layout.spacingLine = SpacingLine
        layout.cellHeight = CircleImageCellHeight
        return layout
    }
    
    func couldContinue(couldContinue : Bool){
        if couldContinue {
            continueButton.enabled = true
            continueButton.alpha = 1.0
        } else {
            continueButton.enabled = false
            continueButton.alpha = 0.5
        }
        
    }

    
    func continueClicked(sender : UIButton) {
        var selectedCurators : [User] = []
        for curator in curators {
            if curator.isSelected {
                selectedCurators.append(curator)
            }
        }
        Log.debug("count: \(selectedCurators.count) curators")
        self.saveCurator(selectedCurators)
    }
    
    func updateProgress(){
        if selectedCuratorCount <= MinCuratorCount {
            circleProgressView.animateToAngle(360 / MinCuratorCount * selectedCuratorCount, duration: 0.4, completion: nil)
        }
    }
    
    //Override back button
    override func createBackButton() {
        let buttonBack = UIButton(type: .Custom)
        buttonBack.setImage(UIImage(named: "back_wht"), forState: .Normal)
        buttonBack.frame = CGRectMake(0, 0, Constants.Value.BackButtonWidth, Constants.Value.BackButtonHeight)
        buttonBack.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: Constants.Value.BackButtonMarginLeft, bottom: 0, right: 0)
        let backButtonItem = UIBarButtonItem(customView: buttonBack)
        buttonBack.addTarget(self, action: "backButtonClicked", forControlEvents: .TouchUpInside)
        self.navigationItem.leftBarButtonItem = backButtonItem
    }

    //Override right bar button
    override func createRightButton(title: String, action: Selector) {
        let rightArrow = UIImageView(image: UIImage(named: "bar_icon_arrow_white"))
        let rightButton = UIButton(type: UIButtonType.System)
        rightButton.setTitle(title, forState: .Normal)
        rightButton.titleLabel?.formatSmall()
        rightButton.setTitleColor( UIColor.whiteColor(), forState: .Normal)
        let constraintRect = CGSize(width: CGFloat.max, height: Constants.Value.BackButtonHeight)
        let boundingBox = title.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: rightButton.titleLabel!.font], context: nil)
        let rightArrowWith = rightArrow.image?.size.width
        rightButton.frame = CGRect(x: 0, y: 0, width: boundingBox.width + rightArrowWith! * 2, height: Constants.Value.BackButtonHeight)
        var frame = rightArrow.frame
        frame.origin.x = rightButton.bounds.maxX - rightArrowWith!
        frame.origin.y = rightButton.center.y - (rightArrow.image?.size.height)! / 2
        rightArrow.frame = frame
        rightButton.addSubview(rightArrow)
        rightButton.addTarget(self, action: action, forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    // MARK: Skip button
    func skipButtonClicked (sender:UIBarButtonItem) {
        var selectedCurators : [User] = []
        var number = 0
        for curator in curators {
            if number >= DefaultCuratorNumber {
                break
            }
            else {
                selectedCurators.append(curator)
            }
            number++
        }
        Log.debug("count: \(selectedCurators.count) curators")
        self.saveCurator(selectedCurators)
    }
}