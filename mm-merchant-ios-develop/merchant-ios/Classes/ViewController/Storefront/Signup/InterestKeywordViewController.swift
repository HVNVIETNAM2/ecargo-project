//
//  ViewController.swift
//  UICollectionViewTest
//
//  Created by HVN_Pivotal on 1/28/16.
//  Copyright © 2016 HVN_Pivotal. All rights reserved.
//

import UIKit
import PromiseKit
import ObjectMapper

class InterestKeywordViewController: MmViewController {
    private final let KeywordCellId = "TagCell"
    private final let SubCatCellId = "SubCatCell"
    private final let MarginLeft : CGFloat = 10
    private final let LineSpacing : CGFloat = 5
    private final let CellHeight : CGFloat = 40
    private final let TextMargin : CGFloat = 30
    private final let ConditionButtonHeight : CGFloat = 50
    private final let MandatoryViewHeight : CGFloat = 50
    private var textFont = UIFont()
    
    private var titleTextAttributes : [String: AnyObject]!
    private var navigationBGImage : UIImage!
    private var navigationShadowImage : UIImage!
    private var navigationTranslucent : Bool = false
    private var navigationBGColor : UIColor!
    private let continueButton = UIButton()
    let buttonCheckbox = UIButton()
    let buttonLink = UIButton()
    var tags : [Tag] = []
    var tagGroups : [TagGroup] = []
    var tagSelectedCount = 0
    
    var isMobileSignup = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView?.registerClass(KeywordCell.self, forCellWithReuseIdentifier: KeywordCellId)
        self.collectionView?.registerClass(SubCatCell.self, forCellWithReuseIdentifier: SubCatCellId)
        self.collectionView?.backgroundColor = UIColor.whiteColor()
        self.setupBackground()
        firstly{
            return self.listTags()
            }.then { _ -> Void in
                self.crateTagGroup()
                self.reloadAllData()
                self.createBottomView()
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.title = String.localize("LB_CA_INTERESTS_KEYWORDS")
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.backupNavigationBar()
        self.setupNavigationBar()
        if self.isMobileSignup {
            buttonCheckbox.hidden = true
            buttonLink.hidden = true
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.revertNavigationBar()
    }
    func setupBackground() {
        let imageView = UIImageView(image: UIImage(named: "interest_keyword_background"))
        let overlayView = UIView(frame: imageView.bounds)
        overlayView.backgroundColor = UIColor.blackColor()
        overlayView.alpha = 0.8
        imageView.addSubview(overlayView)
        self.collectionView.backgroundView = imageView
    }
    func createBottomView() {
        
        let agreeString = "  " + String.localize("LB_CA_TNC_CHECK")
        let agreeWidth = self.getTextWidth(agreeString, height: MandatoryViewHeight, font: textFont)
        let linkString = String.localize("LB_CA_TNC_LINK")
        let linkWidth = self.getTextWidth(linkString, height: MandatoryViewHeight, font: textFont)
        let iconCheckbox = UIImage(named: "square_check_box")
        let agreeTextMarginLink = CGFloat(28)
        let mandatoryViewWidth = iconCheckbox!.size.width +  agreeWidth + agreeTextMarginLink + linkWidth
        let mandatoryView = UIView(frame: CGRect(x: (self.view.bounds.maxX - mandatoryViewWidth) / 2, y: self.view.bounds.maxY - (MandatoryViewHeight + ConditionButtonHeight), width: mandatoryViewWidth, height: MandatoryViewHeight))
        
        
        //Create checkbox button
        buttonCheckbox.frame = CGRect(x: 0, y: 0, width: iconCheckbox!.size.width + agreeWidth, height: MandatoryViewHeight)
        buttonCheckbox.setImage(iconCheckbox, forState: UIControlState.Normal)
        buttonCheckbox.setImage(UIImage(named: "square_check_box_selected"), forState: UIControlState.Selected)
        buttonCheckbox.setTitle(agreeString, forState: UIControlState.Normal)
        buttonCheckbox.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        buttonCheckbox.titleLabel?.font = textFont
        buttonCheckbox.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        buttonCheckbox.addTarget(self, action: "checkboxClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        mandatoryView.backgroundColor = UIColor.clearColor()
        mandatoryView.addSubview(buttonCheckbox)
        //Create link button
        buttonLink.frame = CGRect(x: buttonCheckbox.frame.maxX + agreeTextMarginLink, y: 0, width: linkWidth, height: MandatoryViewHeight)
        buttonLink.titleLabel?.font = textFont
        buttonLink.titleLabel!.escapeFontSubstitution = true
        let attributes = [
            NSFontAttributeName : textFont,
            NSForegroundColorAttributeName : UIColor.primary1(),
            NSUnderlineStyleAttributeName : NSUnderlineStyle.StyleSingle.rawValue]
        let linkStringAttribute = NSAttributedString(
            string: linkString,
            attributes: attributes)
        
        buttonLink.setAttributedTitle(linkStringAttribute, forState: UIControlState.Normal)
        buttonLink.setTitleColor(UIColor.primary1(), forState: UIControlState.Normal)
        buttonLink.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
        buttonLink.addTarget(self, action: "linkClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        buttonLink.backgroundColor = UIColor.clearColor()
        mandatoryView.addSubview(buttonLink)
        
        self.view.addSubview(mandatoryView)
        continueButton.frame = CGRect(x: 0, y: self.view.bounds.maxY - ConditionButtonHeight, width: self.view.bounds.width, height: ConditionButtonHeight)
        continueButton.layer.backgroundColor = UIColor.primary1().CGColor
        continueButton.setTitle(String.localize("LB_CA_FOLLOW_CURATORS_CONT"), forState: .Normal)
        continueButton.addTarget(self, action: "continueClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        self.updateConditionButton()
        self.view.addSubview(continueButton)
        
    }
    
    func updateConditionButton()
    {
        if tagSelectedCount >= 1 && (isMobileSignup || buttonCheckbox.selected) {
            continueButton.alpha = 1.0
            continueButton.enabled = true
        }
        else {
            continueButton.alpha = 0.7
            continueButton.enabled = false
        }
    }
    
    func checkboxClicked(sender : UIButton){
        sender.selected = !sender.selected
        self.updateConditionButton()
    }
    
    func linkClicked(sender : UIButton){
        self.navigationController?.pushViewController(TncViewController(), animated: true)
    }
    
    func continueClicked(sender : UIButton) {
        var selectedTags : [Tag] = []
        for tag in tags {
            if tag.isSelected {
                //                var item = [String : AnyObject]()
                //                item["TagId"] = tag.tagId
                //                item["Priority"] = tag.priority
                selectedTags.append(tag)
            }
        }
        Log.debug("count: \(selectedTags.count) tags")
        self.saveTags(selectedTags)
    }
    
    func backupNavigationBar() {
        titleTextAttributes = self.navigationController!.navigationBar.titleTextAttributes
        navigationBGImage = self.navigationController!.navigationBar.backgroundImageForBarMetrics(UIBarMetrics.Default)
        navigationShadowImage = self.navigationController!.navigationBar.shadowImage
        navigationTranslucent = self.navigationController!.navigationBar.translucent
        navigationBGColor = self.navigationController!.view.backgroundColor
    }
    func revertNavigationBar() {
        self.navigationController!.navigationBar.titleTextAttributes = titleTextAttributes
        self.navigationController!.navigationBar.setBackgroundImage(navigationBGImage, forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = navigationShadowImage
        self.navigationController!.navigationBar.translucent = navigationTranslucent
        self.navigationController!.view.backgroundColor = navigationBGColor
    }
    func setupNavigationBar() {
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
        self.navigationItem.setHidesBackButton(true, animated:false);
    }
    func crateTagGroup () {
        let label = UILabel()
        label.formatSize(13)
        textFont = label.font
        let availableWidth = self.view.bounds.width - (MarginLeft * 2 + LineSpacing)
        var rowWidth = CGFloat(0)
        var tagGroup = TagGroup()
        tagGroup.groupName = String.localize("LB_CA_INTERESTS_KEYWORDS_DESC")
        tagGroups.append(tagGroup)
        tagGroup = TagGroup()
        for tag in tags {
            var tagWidth = self.getTextWidth(tag.tagName, height: CellHeight, font: textFont)
            if tagWidth > availableWidth {
                tagWidth = availableWidth
            }
            if tagWidth + rowWidth <= availableWidth {
                tagGroup.arrayTags.append(tag)
                rowWidth += (tagWidth + LineSpacing)
            }
            else
            {
                tagGroup.width = rowWidth - LineSpacing
                tagGroups.append(tagGroup)
                tagGroup = TagGroup()
                tagGroup.arrayTags.append(tag)
                rowWidth = (tagWidth + LineSpacing)
            }
        }
        tagGroup.width = rowWidth
        tagGroups.append(tagGroup)
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(SubCatCellId, forIndexPath: indexPath) as! SubCatCell
            cell.label.text = tagGroups[indexPath.section].groupName
            cell.label.textColor = UIColor.whiteColor()
            cell.backgroundColor = UIColor.clearColor()
            cell.label.backgroundColor = UIColor.clearColor()
            return cell
        default:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(KeywordCellId, forIndexPath: indexPath) as! KeywordCell
            let tag = tagGroups[indexPath.section].arrayTags[indexPath.row]
            cell.nameLabel.text = tag.tagName
            cell.selected(tag.isSelected)
            return cell
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return tagGroups[section].arrayTags.count
        }
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return tagGroups.count
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return CGSize(width: self.view.bounds.width,height: CellHeight)
        default:
            var width = self.getTextWidth(tagGroups[indexPath.section].arrayTags[indexPath.row].tagName, height: CellHeight, font: textFont)
            if width > self.view.bounds.width - MarginLeft * 2 {
                width = self.view.bounds.width - MarginLeft * 2
            }
            return CGSize(width: width,height: CellHeight)
        }
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        switch section {
        case 0:
            return UIEdgeInsets(top: LineSpacing, left: 0, bottom: 30, right: 0)
        default:
            let marginLeft =  (self.view.bounds.width - tagGroups[section].width) / 2
            var marginBottom = CGFloat(0)
            if section == tagGroups.count - 1 {
                marginBottom = ConditionButtonHeight + MandatoryViewHeight
            }
            return UIEdgeInsets(top: LineSpacing, left: marginLeft, bottom: marginBottom, right: marginLeft)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return LineSpacing
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return LineSpacing
    }
    
    //MARK: Collection View Delegate methods
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let tag = tagGroups[indexPath.section].arrayTags[indexPath.row]
        tag.isSelected = !tag.isSelected
        if tag.isSelected {
            tagSelectedCount++
        }
        else {
            tagSelectedCount--
        }
        self.reloadAllData()
        self.updateConditionButton()
    }
    func getTextWidth(text: String, height: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.width + TextMargin
    }
    
    //MARK: Promise Call
    func listTags() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            TagService.list(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        strongSelf.tags = Mapper<Tag>().mapArray(response.result.value) ?? []
                        Log.debug(strongSelf.tags.count)
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    func saveTags(tags : [Tag]) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            TagService.save(tags){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            Log.debug(String(data: response.data!, encoding : 4))
                            strongSelf.navigationController?.pushViewController(CuratorPickViewController(), animated: true)
                            fulfill("OK")
                        } else {
                            strongSelf.handleError(response, animated: true)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    //MARK: Reload all data
    func reloadAllData(){
        self.collectionView?.reloadData()
        
    }
    
}

//TODO temporary, will implement this class later
class TagGroup: NSObject {
    var groupName = ""
    var width = CGFloat(0)
    var arrayTags : [Tag] = []
}

