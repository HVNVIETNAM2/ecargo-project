//
//  MobileSelectCountryViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 2/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import AlamofireImage

class MobileSelectCountryViewController : MmViewController, UISearchBarDelegate{
    var searchBar = UISearchBar()
    var searchString = ""
    var geoCountries : [GeoCountry] = []
    var filteredGeoCountries : [GeoCountry] = []
    var selectGeoCountryDelegate : SelectGeoCountryDelegate!
    var refreshControl = UIRefreshControl()
    var buttonCell = ButtonCell()
    var isNeedToDismiss = false
    
    private let ImageMenuCellHeight : CGFloat = 40
    private let SearchBarHeight : CGFloat = 40
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge.None
        self.title = String.localize("LB_COUNTRY_PICK")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.frame = CGRect(x: self.view.bounds.minX, y: SearchBarHeight, width: self.view.bounds.width, height: self.view.bounds.height - SearchBarHeight - (self.navigationController?.navigationBar.frame.height)! - UIApplication.sharedApplication().statusBarFrame.size.height)
        loadBrand()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: String.localize("LB_CA_RESET"), style: .Plain, target: self, action: "resetTapped:")
        self.collectionView.registerClass(SelectCountryCell.self, forCellWithReuseIdentifier: "SelectCountryCell")
        self.searchBar.sizeToFit()
        self.searchBar.delegate = self
        self.searchBar.searchBarStyle = UISearchBarStyle.Default
        self.searchBar.showsCancelButton = false
        self.searchBar.frame = CGRect(x: self.view.bounds.minX, y: self.view.bounds.minY, width: self.view.bounds.width, height: SearchBarHeight)
        self.view.insertSubview(self.searchBar, aboveSubview: self.collectionView)
        self.setUpRefreshControl()
        self.createBackButton()
        
        
    }
    
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func loadBrand() {
        self.showLoading()
        firstly{
            return self.listBrand()
            }.then
            { _ -> Void in
                self.filteredGeoCountries = self.geoCountries
                self.collectionView.reloadData()
            }.always {
                self.stopLoading()
                self.refreshControl.endRefreshing()
                
            }.error { _ -> Void in
                Log.error("error")
        }
        
    }
    
    func listBrand() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            GeoService.storefrontCountries(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        strongSelf.geoCountries = Mapper<GeoCountry>().mapArray(response.result.value) ?? []
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("SelectCountryCell", forIndexPath: indexPath) as! SelectCountryCell
        if (self.filteredGeoCountries.count > indexPath.row) {
            cell.countryNameLabel.text = self.filteredGeoCountries[indexPath.row].geoCountryName
            cell.countryCodeLabel.text = self.filteredGeoCountries[indexPath.row].mobileCode
        }
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filteredGeoCountries.count
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
            case self.collectionView!:
                switch indexPath.row {
                default:
                    return CGSizeMake(self.view.frame.size.width, ImageMenuCellHeight)
                }
            default:
                return CGSizeMake(0,0)
            }
    }
    
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            selectGeoCountryDelegate?.selectGeoCountry(filteredGeoCountries[indexPath.row])
            if isNeedToDismiss {
                self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
            }
            else {
                self.navigationController!.popViewControllerAnimated(true)
            }
            
    }
    
    //MARK : Refresh Control
    
    func setUpRefreshControl(){
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.collectionView.addSubview(refreshControl)
        self.collectionView.alwaysBounceVertical = true
    }
    
    func refresh(sender : AnyObject){
        loadBrand()
    }
    
    
    // MARK: UISearchBarDelegate
    
    private func filter(text : String!) {
        self.filteredGeoCountries = self.geoCountries.filter(){ $0.mobileCode.lowercaseString.rangeOfString(text.lowercaseString) != nil || $0.geoCountryName.lowercaseString.rangeOfString(text.lowercaseString) != nil }
        self.collectionView?.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.filter(searchBar.text!)
        searchBar.resignFirstResponder()
    }
    
    
    internal func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchString = searchBar.text!
        if searchString.length == 0 {
            self.filteredGeoCountries = self.geoCountries
            self.collectionView.reloadData()
        } else {
            self.filter(searchString)
        }
        
    }
    
    
    // MARK: Confirm and Reset
    func resetTapped (sender:UIBarButtonItem){
        self.geoCountries = []
        self.collectionView.reloadData()
    }

}