//
//  MobileSignupViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 1/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//
import Foundation
import ObjectMapper
import PromiseKit

protocol SelectGeoCountryDelegate{
    func selectGeoCountry(geoCountry : GeoCountry)
}

class MobileSignupViewController : SignupModeViewController, SwipeSMSDelegate, SelectGeoCountryDelegate, UITextFieldDelegate {
    private final let WidthItemBar : CGFloat = 30
    private final let HeightItemBar : CGFloat = 25
    private final let InputViewMarginLeft : CGFloat = 20
    var signupInputView : SignupInputView!
    var verificationCodeView : VerificationCodeView?
    var mobileVerification = MobileVerification()
    var geoCountries : [GeoCountry] = []
    var isDetectingCountry : Bool = false
    var swipeSMSView : SwipeSMSView?
    var scrollView = UIScrollView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createSubviews()
        self.createBackButton()
        self.title = String.localize("LB_CA_REGISTER")
        loadGeo()
        let tapGesture = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        self.scrollView.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    func createSubviews() {
        signupInputView = SignupInputView(frame: CGRect(x: InputViewMarginLeft, y: 118, width: self.view.bounds.width - InputViewMarginLeft * 2, height: 95))
        scrollView.addSubview(signupInputView)
        signupInputView.countryButton.addTarget(self, action: "selectCountry:", forControlEvents: .TouchUpInside)
        swipeSMSView = SwipeSMSView(frame: CGRect(x: InputViewMarginLeft, y: signupInputView.frame.maxY + 23, width: self.view.bounds.width - InputViewMarginLeft * 2, height: 45))
        swipeSMSView!.timeCountdown = 60
        swipeSMSView!.swipeSMSDelegate = self
        swipeSMSView!.isEnableSwipe = false
        scrollView.addSubview(swipeSMSView!)
        verificationCodeView = VerificationCodeView(frame: CGRect(x: InputViewMarginLeft, y: 306, width: self.view.bounds.width - InputViewMarginLeft * 2, height: 46 + 42 + 20))
        scrollView.addSubview(verificationCodeView!)
        verificationCodeView?.hidden = true
        verificationCodeView?.button.addTarget(self, action: "checkMobileVerification:", forControlEvents: .TouchUpInside)
        signupInputView.codeTextField.text = "+86"
        signupInputView.mobileNumberTextField.becomeFirstResponder()
        signupInputView.codeTextField.delegate = self
        signupInputView.mobileNumberTextField.delegate = self
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: verificationCodeView!.frame.minY + verificationCodeView!.frame.height)
        scrollView.contentSize = self.scrollView.bounds.size
        self.view.addSubview(scrollView)
    }
    
    //MARK: SwipeSMSDelegate
    func startSMS() {
        //TODO
        Log.debug("startSMS")
        firstly{
            return sendMobileVerifcation()
            }.then { _ -> Void in
                self.verificationCodeView?.hidden = false
                self.verificationCodeView?.textfield.becomeFirstResponder()
                
        }
    }
    
    func resetSMS() {
        //TODO
        Log.debug("resetSMS")
    }
    
    //MARK: Button delegate
    func selectCountry(button : UIButton){
        let mobileSelectCountryViewController = MobileSelectCountryViewController()
        mobileSelectCountryViewController.selectGeoCountryDelegate = self
        self.navigationController?.pushViewController(mobileSelectCountryViewController, animated: true)
    }
    
    func selectGeoCountry(geoCountry: GeoCountry) {
        signupInputView.codeTextField.text = geoCountry.mobileCode
        signupInputView.countryButton.setTitle(geoCountry.geoCountryName, forState: .Normal)
    }
    
    func sendMobileVerifcation()-> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            var parameters = [String : AnyObject]()
            parameters["MobileNumber"] = signupInputView.mobileNumberTextField.text
            parameters["MobileCode"] = signupInputView.codeTextField.text
            AuthService.sendMobileVerification(parameters) { [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if let mobileVerification = Mapper<MobileVerification>().map(response.result.value){
                            strongSelf.mobileVerification = mobileVerification
                        }
                        Log.debug(String(data: response.data!, encoding: 4))
                        fulfill("OK")
                    } else{
                        strongSelf.handleError(response, animated: true, reject: reject)
                    }
                }
                
            }
        }
    }
    
    func checkMobileVerifcation()-> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            var parameters = [String : AnyObject]()
            parameters["MobileNumber"] = signupInputView.mobileNumberTextField.text
            parameters["MobileCode"] = signupInputView.codeTextField.text
            parameters["MobileVerificationId"] = self.mobileVerification.mobileVerificationId
            parameters["MobileVerificationToken"] = verificationCodeView?.textfield.text
            AuthService.checkMobileVerification(parameters) { [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            Log.debug(String(data: response.data!, encoding: 4))
                            fulfill("OK")
                        } else{
                            strongSelf.handleError(response, animated: true, reject: reject)
                        }} else {
                        reject(response.result.error!)
                    }
                }
                
            }
        }
    }
    
    func checkMobileVerification(button : UIButton){
        firstly{
            return checkMobileVerifcation()
            }.then { _ -> Void in
                let mobileSignupProfileViewController = MobileSignupProfileViewController()
                mobileSignupProfileViewController.signupMode = self.signupMode
                mobileSignupProfileViewController.mobileNumber = self.signupInputView.mobileNumberTextField.text ?? ""
                mobileSignupProfileViewController.mobileCode = self.signupInputView.codeTextField.text ?? ""
                mobileSignupProfileViewController.mobileVerificationId = self.mobileVerification.mobileVerificationId
                mobileSignupProfileViewController.mobileVerficationToken = self.verificationCodeView!.textfield.text ?? ""
                self.navigationController?.pushViewController(mobileSignupProfileViewController, animated: true)
                
        }
    }
    
    func loadGeo() {
        self.showLoading()
        firstly{
            return self.listGeo()
            }.then
            { _ -> Void in
               
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
        
    }
    
    func listGeo() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            GeoService.storefrontCountries(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response!.statusCode == 200 {
                            strongSelf.geoCountries = Mapper<GeoCountry>().mapArray(response.result.value) ?? []
                            fulfill("OK")
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    //MARK: UITextFieldDelegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        Log.debug("location: \(range.location) string: \(string)")
        switch textField.tag {
            case 1: //Mobile Code
                if string == "" && range.location == 0 { //Don't allow todelete +
                    return false
                }
                if !isDetectingCountry {
                    isDetectingCountry = true
                    NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "detectCountry", userInfo: nil, repeats: false)
                }
                break
            case 2: //Mobile number
                 NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "updateSwipeButton", userInfo: nil, repeats: false)
                break
            default:
                break
        }
       
        return true
    }

    func detectCountry() {
        let code = signupInputView.codeTextField.text
        for geoCountry : GeoCountry in self.geoCountries {
            Log.debug("code : \(code) countryCode: \(geoCountry.mobileCode)")
            if code == geoCountry.mobileCode {
                signupInputView.countryButton.setTitle(geoCountry.geoCountryName, forState: .Normal)
                isDetectingCountry = false
                return
            }
        }
        isDetectingCountry = false
        signupInputView.countryButton.setTitle(String.localize("LB_COUNTRY_PICK"), forState: .Normal)
    }
    
    func updateSwipeButton() {
        swipeSMSView!.isEnableSwipe = signupInputView.mobileNumberTextField.text?.length > 7 ? true : false
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let contentInset = UIEdgeInsetsZero;
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
        scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y)
    }
    
    func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            if let keyboardSize: CGSize =  userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size {
                let heightOfset = ((keyboardSize.height + scrollView.contentSize.height + 64) - self.view.bounds.size.height)
                if heightOfset > 0 {
                    let contentInset = UIEdgeInsetsMake(0.0, 0.0, heightOfset,  0.0);
                    scrollView.contentInset = contentInset
                    scrollView.scrollIndicatorInsets = contentInset
                    
                }
            }
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
}