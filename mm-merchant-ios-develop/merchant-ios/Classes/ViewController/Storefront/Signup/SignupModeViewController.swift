//
//  SignupModeViewController.swift
//  merchant-ios
//
//  Created by Alan YU on 22/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

enum SignupMode: Int {
    case Normal
    case Checkout
    case CheckoutSwipeToPay
    case Profile
    case IM
}

class SignupModeViewController: MmViewController {
    
    var signupMode = SignupMode.Normal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}