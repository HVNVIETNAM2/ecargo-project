//
//  CuratorListViewCell.swift
//  merchant-ios
//
//  Created by Trung Vu on 3/8/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit

class CuratorListViewCell: UICollectionViewCell {
    var imageView = UIImageView()
    var diamondImageView = UIImageView()
    var upperLabel = UILabel()
    var borderView = UIView()
    var lowerLabel = UILabel()
    var bottomLabel = UILabel()
    var imageViewIcon = UIImageView()
    var buttonView = UIView()
    var followButton = UIButton()
    private final let MarginRight : CGFloat = 20
    private final let MarginLeft : CGFloat = 15
    private final let LabelMarginTop : CGFloat = 15
    private final let LabelMarginRight : CGFloat = 30
    private final let ImageWidth : CGFloat = 44
    private final let ImageDiamondWidth : CGFloat = 16
    private final let LabelRightWidth : CGFloat = 63
    private final let LabelLowerMarginTop : CGFloat = 33
    private final let ButtonHeight : CGFloat = 30
    private final let ButtonWidth : CGFloat = 64
    private final let ChatButtonWidth : CGFloat = 30
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = UIColor.primary1().CGColor
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = ImageWidth / 2
        addSubview(imageView)
        
        diamondImageView.image = UIImage(named: "curator_diamond")
        addSubview(diamondImageView)
        addSubview(diamondImageView)
        
        upperLabel.formatSize(15)
        upperLabel.text = "胡杏儿"
        upperLabel.textColor = UIColor.secondary2()
        addSubview(upperLabel)
        
        lowerLabel.text = "Hvn"
        lowerLabel.formatSize(12)
        lowerLabel.textColor = UIColor.secondary3()
        addSubview(lowerLabel)
        
        followButton.addTarget(self, action: "onFollowHandle:", forControlEvents: UIControlEvents.TouchUpInside)
        followButton.setTitle(String.localize("LB_CA_FOLLOW"), forState: UIControlState.Normal)
        followButton.setTitleColor(UIColor.secondary2(), forState: .Normal)
        followButton.formatSecondaryNonBorder()
        buttonView.addSubview(followButton)
        
        imageViewIcon.image = UIImage(named: "btn_ok")
        buttonView.addSubview(imageViewIcon)
        
        
        addSubview(buttonView)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: bounds.minX + MarginLeft, y: bounds.midY - ImageWidth / 2, width: ImageWidth, height: ImageWidth)
        upperLabel.frame = CGRect(x: imageView.frame.maxX + MarginRight, y: bounds.minY + LabelMarginTop, width: bounds.width - (imageView.frame.maxX + MarginRight + LabelRightWidth + MarginRight) , height: (bounds.height - LabelMarginTop * 2) / 2)
        lowerLabel.frame = CGRect(x: imageView.frame.maxX + MarginRight, y: upperLabel.frame.origin.y + upperLabel.frame.height, width: bounds.width - (imageView.frame.maxX + MarginRight * 2) , height:(bounds.height - LabelMarginTop * 2) / 2 )
        imageViewIcon.frame = CGRectMake(0, (buttonView.frame.height - ButtonHeight) / 2, 30, 30)
        buttonView.frame = CGRect(x: upperLabel.frame.maxX , y: bounds.minY , width: LabelRightWidth, height:bounds.height)
        let widthButton = getTextWidth((followButton.titleLabel?.text)!, height: ButtonHeight, font: (followButton.titleLabel?.font)!)
        followButton.frame = CGRect(x: 30, y: (buttonView.frame.height - ButtonHeight) / 2 , width: widthButton, height:ButtonHeight)
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        diamondImageView.frame = CGRect(x: imageView.frame.maxX - (ImageDiamondWidth - 2), y: imageView.frame.minY - 5, width: ImageDiamondWidth, height: ImageDiamondWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func onFollowHandle(sender: UIButton) {
        
    }
    func getTextWidth(text: String, height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.width
    }
}
