//
//  MerchantListViewCell.swift
//  merchant-ios
//
//  Created by Trung Vu on 3/8/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit
import AlamofireImage

@objc
protocol MerchantCellDelegate: NSObjectProtocol {
    optional func onTapFollowHandle(rowIndex: Int)
}
class MerchantListViewCell : UICollectionViewCell{
    var imageView = UIImageView()
    var upperLabel = UILabel()
    var borderView = UIView()
    var lowerLabel = UILabel()
    var bottomLabel = UILabel()
    var imageViewIcon = UIImageView()
    var buttonView = UIView()
    var followButton = UIButton()
    private final let MarginRight : CGFloat = 20
    private final let MarginLeft : CGFloat = 15
    private final let LabelMarginTop : CGFloat = 15
    private final let LabelMarginRight : CGFloat = 30
    private final let ImageWidth : CGFloat = 44
    private final let ImageDiamondWidth : CGFloat = 16
    private final let LabelRightWidth : CGFloat = 63
    private final let LabelLowerMarginTop : CGFloat = 33
    private final let ButtonHeight : CGFloat = 30
    private final let ButtonWidth : CGFloat = 64
    private final let ChatButtonWidth : CGFloat = 30
    var merchant: Merchant?
    var IsFollow: Bool = true
    var delegateMerchantList: MerchantCellDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        addSubview(imageView)
        upperLabel.formatSize(15)
        upperLabel.text = "francis"
        upperLabel.textColor = UIColor.secondary2()
        upperLabel.numberOfLines = 0
        addSubview(upperLabel)
        
        lowerLabel.text = "Hvn"
        lowerLabel.formatSize(12)
        lowerLabel.textColor = UIColor.secondary3()
        addSubview(lowerLabel)
        
        bottomLabel.text = "ecago"
        bottomLabel.formatSize(12)
        bottomLabel.textColor = UIColor.secondary3()
        addSubview(bottomLabel)
        
        followButton.addTarget(self, action: "onFollowHandle:", forControlEvents: UIControlEvents.TouchUpInside)
        followButton.setTitle(String.localize("LB_CA_FOLLOWED"), forState: UIControlState.Normal)
        followButton.setTitleColor(UIColor.secondary2(), forState: .Normal)
        followButton.formatSecondaryNonBorder()
        buttonView.addSubview(followButton)
        
        imageViewIcon.image = UIImage(named: "btn_ok")
        buttonView.addSubview(imageViewIcon)
        
        addSubview(buttonView)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: bounds.minX + MarginLeft, y: bounds.midY - ImageWidth / 2, width: ImageWidth, height: ImageWidth)
        upperLabel.frame = CGRect(x: imageView.frame.maxX + MarginRight, y: bounds.minY + LabelMarginTop, width: bounds.width - (imageView.frame.maxX + MarginRight + LabelRightWidth + MarginRight) , height: (bounds.height - LabelMarginTop * 2) / 3)
        lowerLabel.frame = CGRect(x: imageView.frame.maxX + MarginRight, y: upperLabel.frame.origin.y + upperLabel.frame.height, width: bounds.width - (imageView.frame.maxX + MarginRight * 2) , height:(bounds.height - LabelMarginTop * 2) / 3 )
        bottomLabel.frame = CGRect(x: imageView.frame.maxX + MarginRight, y: lowerLabel.frame.origin.y + lowerLabel.frame.height, width: bounds.width - (imageView.frame.maxX + MarginRight * 2) , height:(bounds.height - LabelMarginTop * 2) / 3 )
        imageViewIcon.frame = CGRectMake(0, (buttonView.frame.height - ButtonHeight) / 2, 30, 30)
        buttonView.frame = CGRect(x: upperLabel.frame.maxX , y: bounds.minY , width: LabelRightWidth, height:bounds.height)
        let widthButton = getTextWidth((followButton.titleLabel?.text)!, height: ButtonHeight, font: (followButton.titleLabel?.font)!)
        followButton.frame = CGRect(x: 30, y: (buttonView.frame.height - ButtonHeight) / 2 , width: widthButton, height:ButtonHeight)
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func getTextWidth(text: String, height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.width
    }
    
    //MARK: - setup data
    
    func setImage(imageKey : String, category : ImageCategory){
        let filter = AspectScaledToFitSizeFilter(
            size: imageView.frame.size
        )
        imageView.af_setImageWithURL(ImageURLFactory.get(imageKey, category: category), placeholderImage : UIImage(named: "holder"), filter: filter, imageTransition: .CrossDissolve(0.3))
    }
    func setupDataCell(merchant: Merchant) {
        self.merchant = merchant
        setImage(merchant.smallLogoImage, category: .Merchant)
        self.upperLabel.text = String(format: "%@", merchant.merchantNameInvariant)
        self.lowerLabel.text = String(format: "%@", merchant.merchantName)
        self.bottomLabel.text = String(format: "%d %@", merchant.followerCount, String.localize("LB_CA_NO_OF_FOLLOWER"))
        if merchant.followStatus == true {
            self.imageViewIcon.image = UIImage(named: "btn_ok")
            self.followButton.setTitle(String.localize("LB_CA_FOLLOWED"), forState: .Normal)
        } else {
            self.imageViewIcon.image = UIImage(named: "btn_add")
            self.followButton.setTitle(String.localize("LB_CA_FOLLOW"), forState: .Normal)
        }
        
    }
    
    //MARK:  - handle follow
    func onFollowHandle(sender: UIButton) {
        self.delegateMerchantList?.onTapFollowHandle!(sender.tag)
    }
}