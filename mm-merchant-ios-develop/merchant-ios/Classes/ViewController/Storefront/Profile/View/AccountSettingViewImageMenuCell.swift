//
//  AccountSettingViewImageMenuCell.swift
//  merchant-ios
//
//  Created by Stephen Yuen on 18/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

class AccountSettingViewImageMenuCell: UICollectionViewCell {
    
    private final let MarginLeft: CGFloat = 14
    private final let MenuItemImageViewWidth: CGFloat = 30
    private final let MenuItemImageViewHeight: CGFloat = 30
    private final let MenuItemLabelMarginLeft: CGFloat = 12
    private final let MenuItemLabelHeight: CGFloat = 42
    
    var menuItemLabel = UILabel()
    private var menuItemImageView = UIImageView()
    private var disclosureIndicatorImageView = UIImageView()
    private var borderView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        self.clipsToBounds = true
        
        addSubview(menuItemImageView)
        
        menuItemLabel.formatSize(14)
        addSubview(menuItemLabel)
        
        disclosureIndicatorImageView.image = UIImage(named: "filter_right_arrow")
        addSubview(disclosureIndicatorImageView)
        
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        menuItemImageView.frame = CGRect(x: MarginLeft, y: bounds.midY - (MenuItemImageViewWidth / 2), width: MenuItemImageViewWidth , height: MenuItemImageViewHeight)
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        disclosureIndicatorImageView.frame = CGRect(x: bounds.maxX - 35 , y: bounds.midY - disclosureIndicatorImageView.image!.size.height / 2 , width: disclosureIndicatorImageView.image!.size.width, height: disclosureIndicatorImageView.image!.size.height)
        
        menuItemLabel.frame = CGRect(
            x: menuItemImageView.frame.origin.x + menuItemImageView.frame.size.width + MenuItemLabelMarginLeft,
            y: bounds.midY - (MenuItemLabelHeight / 2),
            width: disclosureIndicatorImageView.frame.origin.x - (menuItemImageView.frame.origin.x + menuItemImageView.frame.size.width + MenuItemLabelMarginLeft),
            height: MenuItemLabelHeight
        )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setItemImage(imageName imageName: String){
        menuItemImageView.image = UIImage(named: imageName)
    }
    
    func showBorder(isShow: Bool) {
        borderView.hidden = !isShow
    }
}
