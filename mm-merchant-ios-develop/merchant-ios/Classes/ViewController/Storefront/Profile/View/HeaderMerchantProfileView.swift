//
//  HeaderMerchantProfileView.swift
//  merchant-ios
//
//  Created by Trung Vu on 3/10/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//
import UIKit
import AlamofireImage
import Kingfisher

class HeaderMerchantProfileView: UICollectionReusableView {
    
    private final let HeightForCell: CGFloat = 134.0
    private final let HeightNavigation: CGFloat = 44.0
    private final let ImageAvatarWidth : CGFloat = 70.0
    private final let ImageIconWidth: CGFloat = 25.0
    private final let HeighLabelUserName: CGFloat = 18.0
    private final let HeightAvatar: CGFloat = 92.0
    
    private final let HeightActionView: CGFloat = 70.0
    private final let HeightBottomView: CGFloat = 44.0
    private final let marginTop:CGFloat = 10.0
    private final let ImageCameraWidth:CGFloat = 25.0
    private final let marginLeftCamera:CGFloat = 15.0
    private final let ImageCameraHeight: CGFloat = 20.0
    private final let MarginActionButton:CGFloat = 20.0
    private final let WidthImageCameraBottom:CGFloat = 80.0
    private final let WidthItemButton:CGFloat = 30
    private final let HeightItemButton:CGFloat = 30
    private final let space:CGFloat = 10.0
    private final let widthLogo: CGFloat = 120.0
    private final let HeightLogo:CGFloat = 35.0
    private final let marginLeft:CGFloat = 48.0
    private final let HeightDescriptionView:CGFloat = 100.0
    
    private final let grayColor = UIColor(hexString: "#B1B1B1")
    private final let NotiHasAvatar = "NotiHasAvatar"
    
    var coverImageView          = UIImageView()
    var overlay                 = UIImageView()

    var  bottomView             = UIView()
    var labelFollower           = UILabel()
    var labelNumberFollower     = UILabel()
    
    var viewRight               = UIView()
    var imageViewAdd            = UIImageView()
    var buttonAddFollow         = UIButton()
    var imageViewOk             = UIImageView()
    var buttonShare             = UIButton()
    var imageViewChat           = UIImageView()
    var buttonChat              = UIButton()
    
    var imageviewLogo           = UIImageView()
    var viewScore               = UIView()
    var buttonDesc              = UIButton()
    var buttonService           = UIButton()
    var buttonRating            = UIButton()
    
    var viewDescrip             = UIView()
    var labelName               = UILabel()
    var labelDescription        = UILabel()
    var line                    = UIView()
    var imageviewArrow          = UIImageView()
    
    var merchant = Merchant()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.userInteractionEnabled = true
        self.backgroundColor = UIColor.whiteColor()
        
        coverImageView.image = UIImage(named: "deflaut_cover")
        coverImageView.userInteractionEnabled = false
        self.addSubview(coverImageView)
        
        overlay.image = UIImage(named: "overlay")
        self.addSubview(overlay)
        
        //create bottom View
        bottomView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.addSubview(bottomView)
        
        labelFollower.text = String.localize("LB_CA_FOLLOWER")
        labelFollower.formatSize(14)
        labelFollower.textColor = UIColor.whiteColor()
        bottomView.addSubview(labelFollower)
        labelNumberFollower.text = ""
        labelNumberFollower.formatSize(18)
        labelNumberFollower.textColor = UIColor.whiteColor()
        bottomView.addSubview(labelNumberFollower)
        
        // right view
        
        imageViewAdd.image = UIImage(named: "btn_add")
        viewRight.addSubview(imageViewAdd)
        bottomView.addSubview(buttonAddFollow)
        buttonAddFollow.setTitle(String.localize("LB_CA_FOLLOW"), forState: .Normal)
        buttonAddFollow.titleLabel?.formatSize(14)
        bottomView.addSubview(imageViewOk)
        imageViewOk.image = UIImage(named: "btn_share_wht")
        viewRight.addSubview(buttonShare)
        buttonShare.setTitle(String.localize("LB_CA_SHARE"), forState: .Normal)
        buttonShare.titleLabel?.formatSize(14)
        bottomView.addSubview(buttonChat)
        imageViewChat.image = UIImage(named: "btn_cs_wht")
        bottomView.addSubview(imageViewChat)
        buttonChat.setTitle(String.localize("LB_CA_PROF_CS"), forState: .Normal)
        buttonChat.titleLabel?.formatSize(14)
        viewRight.backgroundColor = UIColor.blackColor()
        bottomView.addSubview(viewRight)
        
        
        
        buttonDesc = self.createButton(20, textName: String.localize("LB_CA_PROD_DESC"), number: "5.0")
        buttonService = self.createButton(21, textName: String.localize("LB_CA_CUST_SERVICE"), number: "5.0")
        buttonRating = self.createButton(22, textName: String.localize("LB_CA_SHIPMENT_RATING"), number: "5.0")
        if let button = buttonRating.viewWithTag(9) {
            button.backgroundColor = UIColor.clearColor()
        }
        // logo
        imageviewLogo.image = UIImage(named: "logo_demo")
        imageviewLogo.contentMode = .Center
        viewScore.addSubview(imageviewLogo)
        self.addSubview(viewDescrip)
        self.addSubview(viewScore)
        viewScore.addSubview(buttonDesc)
        viewScore.addSubview(buttonService)
        viewScore.addSubview(buttonRating)
        line.backgroundColor = UIColor(hexString: "#F1F1F1")
        viewScore.addSubview(line)
        
        labelName.text = String.localize("关于Kate Spade")
        labelName.formatSize(15)
        labelName.textColor = UIColor.secondary2()
        viewDescrip.addSubview(labelName)
        
        labelDescription.text = ""
        labelDescription.formatSize(14)
        labelDescription.textColor = UIColor.secondary3()
        labelDescription.numberOfLines = 0
        viewDescrip.addSubview(labelDescription)
        
        imageviewArrow.image = UIImage(named: "icon_arrow")
        viewDescrip.addSubview(imageviewArrow)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        coverImageView.frame = CGRectMake(0, 0, self.frame.width, self.frame.height - HeightDescriptionView * 2)
        overlay.frame = coverImageView.frame
        
        // set up description view
        viewDescrip.frame = CGRectMake(0, self.frame.height - HeightDescriptionView, self.frame.width, HeightDescriptionView)
        
        
        viewScore.frame = CGRectMake(0, self.frame.height - HeightDescriptionView * 2, self.frame.width, HeightDescriptionView)
        imageviewLogo.frame = CGRectMake((self.frame.width - widthLogo)/2, 10, widthLogo, HeightLogo)
        
        let width = (self.frame.width - marginLeft * 2) / 3
        let orginYBtn = imageviewLogo.frame.origin.y + HeightLogo + space

        buttonDesc.frame = CGRectMake(marginLeft, orginYBtn, width, 40)
        buttonService.frame = CGRectMake(buttonDesc.frame.origin.x + width, orginYBtn, width, 40)
        buttonRating.frame = CGRectMake(buttonService.frame.origin.x + width, orginYBtn, width, 40)
        
        
        //setup bottomView
        bottomView.frame = CGRectMake(0, viewScore.frame.origin.y - HeightBottomView, self.frame.width
            , HeightBottomView)
        
        let widthLabelFollower = self.getTextWidth(labelFollower.text!, height: 22.0, font: labelFollower.font)
        labelFollower.frame = CGRectMake(16, (bottomView.frame.height - 22.0) / 2, widthLabelFollower, 22.0)
        updateFrameLabelFolling()
        
        // add view follow
//        let WidthViewRight = self.frame.width - (self.labelNumberFollower.frame.origin.x + self.labelNumberFollower.frame.width + 0)
//        viewRight.frame =  CGRectMake(bottomView.frame.size.width - WidthViewRight, 0, WidthViewRight, HeightBottomView)


        let widthButtonChat = getTextWidth((buttonChat.titleLabel?.text)!, height: buttonChat.frame.height, font: (buttonChat.titleLabel?.font)!)
        buttonChat.frame = CGRectMake(bottomView.frame.width - space*4 - WidthItemButton, (HeightBottomView - HeightItemButton)/2, widthButtonChat, HeightItemButton)
        imageViewChat.frame = CGRectMake(buttonChat.frame.origin.x - WidthItemButton, (HeightBottomView - HeightItemButton)/2, WidthItemButton, HeightItemButton)
        
        let widthItemFollow = getTextWidth((buttonShare.titleLabel?.text)!, height: buttonShare.frame.height, font: (buttonShare.titleLabel?.font)!)
        buttonShare.frame = CGRectMake(imageViewChat.frame.origin.x - widthItemFollow - space, (HeightBottomView - HeightItemButton)/2, widthItemFollow, HeightItemButton)
        imageViewOk.frame = CGRectMake(buttonShare.frame.origin.x - WidthItemButton, (HeightBottomView - HeightItemButton)/2, WidthItemButton, HeightItemButton)
        
        let widthItemFollowBtn = getTextWidth((buttonAddFollow.titleLabel?.text)!, height: buttonAddFollow.frame.height, font: (buttonAddFollow.titleLabel?.font)!)
        buttonAddFollow.frame = CGRectMake(imageViewOk.frame.origin.x - widthItemFollowBtn - space, (HeightBottomView - HeightItemButton)/2, widthItemFollowBtn, HeightItemButton)
        imageViewAdd.frame = CGRectMake(buttonAddFollow.frame.origin.x - WidthItemButton, (HeightBottomView - HeightItemButton)/2, WidthItemButton, HeightItemButton)
        
        updateFrame()
        labelName.frame = CGRectMake(marginTop, marginTop, self.frame.width - marginTop*2, 21)
        
        line.frame = CGRectMake(marginTop, HeightDescriptionView - 1, self.frame.width - marginTop * 2, 1)
        imageviewArrow.frame = CGRectMake(self.frame.width - 25, (HeightDescriptionView - 15)/2, 8, 15)
    }
    func updateFrame() {
        var height = getTextHeight(labelDescription.text!, width: self.frame.width - 40, font: labelDescription.font)
        if height > HeightDescriptionView - space {
            height = HeightDescriptionView - space - labelName.frame.height - marginTop
        }
        labelDescription.frame = CGRectMake(marginTop, labelName.frame.origin.y + 21, self.frame.width - 60, height)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func updateFrameLabelFolling() {
        let widthLabelFollower = self.getTextWidth(labelFollower.text!, height: 22.0, font: labelFollower.font)
        let widthLabelNumberFollower = self.getTextWidth(labelNumberFollower.text!, height: 22.0, font: labelNumberFollower.font)
        let xLabel = 16 + widthLabelFollower + 10
        labelNumberFollower.frame = CGRectMake(xLabel, (bottomView.frame.height - 22.0) / 2 , widthLabelNumberFollower, 22.0)
    }
    
    func getTextWidth(text: String, height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.width
    }
    func getTextHeight(text: String, width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.height
    }
    func originY() -> CGFloat
    {
        var originY:CGFloat = 0;
        let application: UIApplication = UIApplication.sharedApplication()
        if (application.statusBarHidden)
        {
            originY = application.statusBarFrame.size.height
        }
        return originY;
    }
    func createButton(index: Int, textName: String, number: String) -> UIButton {
        let width = (self.frame.width - marginLeft * 2) / 3
        let frm = CGRectMake(0, 0, width, HeightActionView)
        let buttonParent = UIButton(frame: frm)
        buttonParent.tag = index
        buttonParent.backgroundColor = UIColor.whiteColor()
        buttonParent.setTitle(String(format: "%@ %@", textName, number), forState: .Normal)
        buttonParent.titleLabel?.formatSize(10)
        buttonParent.setTitleColor(UIColor.secondary2(), forState: .Normal)
        
        let border = UIView(frame: CGRectMake(width - 1, 5, 1, 30))
        border.backgroundColor = UIColor(hexString: "#F1F1F1")
        border.tag = 9
        border.alpha = 0.5
        buttonParent.addSubview(border)
        return buttonParent
    }
    
    //MARK: loading data
    
    func configDataWithMerchant(merchant: Merchant) {
        self.merchant = merchant
        labelNumberFollower.text = String(format: "%d", merchant.followerCount)
        setCoverImage(merchant.profileBannerImage)
        labelName.text = String(format: "%@", merchant.merchantName)
        labelDescription.text = String(format: "%@", merchant.merchantDesc)
        updateFrame()
        setDataImageviewLogo(merchant.headerLogoImage)
    }
    
    func setCoverImage(key : String){
        if self.frame.height > 0 {
//            let filter = AspectScaledToFillSizeFilter(
//                size: coverImageView.frame.size
//            )
//            coverImageView.af_setImageWithURL(ImageURLFactory.get(key, category: .Merchant), placeholderImage : UIImage(named: "deflaut_cover"), filter: filter, imageTransition: .CrossDissolve(0.3))
            coverImageView.kf_setImageWithURL(ImageURLFactory.get(key, category: .Merchant), placeholderImage: UIImage(named: "deflaut_cover"))
        }
    }
    
    func setDataImageviewLogo(key : String){        
            let filter = AspectScaledToFitSizeFilter(
                size: imageviewLogo.frame.size
            )
        if filter.size.width > 0 {
            imageviewLogo.af_setImageWithURL(ImageURLFactory.get(key, category: .Merchant), placeholderImage : UIImage(named: "deflaut_cover"), filter: filter, imageTransition: .CrossDissolve(0.3))
        }
        
        
    }
}
