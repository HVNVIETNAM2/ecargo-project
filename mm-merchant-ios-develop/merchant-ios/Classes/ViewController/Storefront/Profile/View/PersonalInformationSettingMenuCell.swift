//
//  PersonalInformationSettingMenuCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

class PersonalInformationSettingMenuCell: UICollectionViewCell {
    
    private final let MarginLeft: CGFloat = 20
    private final let MenuItemLabelHeight: CGFloat = 42
    private final let ValueImageDimension: CGFloat = 18
    private final let ValueObjectMarginRight: CGFloat = 16
    private final let CenterPaddingWidth: CGFloat = 8
    
    var itemLabel = UILabel()
    var valueLabel = UILabel()
    var valueImageView = UIImageView()
    private var disclosureIndicatorImageView = UIImageView()
    private var borderView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        self.clipsToBounds = true
        
        itemLabel.formatSize(14)
        addSubview(itemLabel)
        
        disclosureIndicatorImageView.image = UIImage(named: "filter_right_arrow")
        addSubview(disclosureIndicatorImageView)
        
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        
        valueLabel.formatSize(12)
        valueLabel.textAlignment = .Right
        addSubview(valueLabel)
        
        addSubview(valueImageView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        disclosureIndicatorImageView.frame = CGRect(x: bounds.maxX - 35 , y: bounds.midY - disclosureIndicatorImageView.image!.size.height / 2 , width: disclosureIndicatorImageView.image!.size.width, height: disclosureIndicatorImageView.image!.size.height)
        
        let availableWidth = disclosureIndicatorImageView.frame.origin.x - MarginLeft - ValueObjectMarginRight - CenterPaddingWidth
        
        itemLabel.frame = CGRect(
            x: MarginLeft,
            y: bounds.midY - (MenuItemLabelHeight / 2),
            width: availableWidth / 2,
            height: MenuItemLabelHeight
        )
        
        valueLabel.frame = CGRect(
            x: MarginLeft + availableWidth / 2 + CenterPaddingWidth,
            y: bounds.midY - (MenuItemLabelHeight / 2),
            width: availableWidth / 2,
            height: MenuItemLabelHeight
        )
        
        valueImageView.frame = CGRect(
            x: disclosureIndicatorImageView.frame.origin.x - ValueImageDimension - ValueObjectMarginRight,
            y: bounds.midY - (ValueImageDimension / 2),
            width: ValueImageDimension,
            height: ValueImageDimension
        )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setValueImage(imageName imageName: String){
        valueImageView.image = UIImage(named: imageName)
    }
    
    func showBorder(isShow: Bool) {
        borderView.hidden = !isShow
    }
    
    func showDisclosureIndicator(isShow: Bool) {
        disclosureIndicatorImageView.hidden = !isShow
    }
    
}