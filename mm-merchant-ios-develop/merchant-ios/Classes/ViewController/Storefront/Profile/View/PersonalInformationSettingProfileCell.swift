//
//  PersonalInformationSettingProfileCell.swift
//  merchant-ios
//
//  Created by Sang on 2/2/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import AlamofireImage

class PersonalInformationSettingProfileCell: UICollectionViewCell {
    
    private final let MarginLeft: CGFloat = 20
    private final let ItemLabelHeight: CGFloat = 42
    private final let ProfileImageDimension: CGFloat = 40
    private final let ProfileImageMarginRight: CGFloat = 16
    
    var itemLabel = UILabel()
    var profileImageView = UIImageView()
    private var disclosureIndicatorImageView = UIImageView()
    private var borderView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        self.clipsToBounds = true
        
        itemLabel.formatSize(14)
        addSubview(itemLabel)
        
        profileImageView.image = UIImage(named: "profile_avatar")
        profileImageView.userInteractionEnabled = true
        profileImageView.round()
        addSubview(profileImageView)
        
        disclosureIndicatorImageView.image = UIImage(named: "filter_right_arrow")
        addSubview(disclosureIndicatorImageView)
        
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        disclosureIndicatorImageView.frame = CGRect(x: bounds.maxX - 35 , y: bounds.midY - disclosureIndicatorImageView.image!.size.height / 2 , width: disclosureIndicatorImageView.image!.size.width, height: disclosureIndicatorImageView.image!.size.height)
        
        profileImageView.frame = CGRect(
            x: disclosureIndicatorImageView.frame.origin.x - ProfileImageDimension - ProfileImageMarginRight,
            y: bounds.midY - (ProfileImageDimension / 2),
            width: ProfileImageDimension,
            height: ProfileImageDimension
        )
        
        itemLabel.frame = CGRect(
            x: MarginLeft,
            y: bounds.midY - (ItemLabelHeight / 2),
            width: bounds.width - MarginLeft - profileImageView.frame.origin.x,
            height: ItemLabelHeight
        )
        
        profileImageView.layer.cornerRadius = ProfileImageDimension / 2
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setProfileImage(key: String){
        let filter = AspectScaledToFitSizeFilter(
            size: profileImageView.frame.size
        )
        
        profileImageView.af_setImageWithURL(ImageURLFactory.get(key, category: .User), placeholderImage: UIImage(named: "profile_avatar"), filter: filter, imageTransition: .CrossDissolve(0.3))
    }
    
    func showBorder(isShow: Bool) {
        borderView.hidden = !isShow
    }
    
    func showDisclosureIndicator(isShow: Bool) {
        disclosureIndicatorImageView.hidden = !isShow
    }
    
}