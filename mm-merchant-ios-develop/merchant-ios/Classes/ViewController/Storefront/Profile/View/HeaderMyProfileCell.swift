
//
//  HeaderMyProfileCell.swift
//  merchant-ios
//
//  Created by Trung Vu on 2/29/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit
import AlamofireImage
import Kingfisher
@objc
protocol HeaderMyProfileDelegate: NSObjectProtocol {
    optional func onTapWishlistButton()
    optional func onTapAvatarView(sender: UITapGestureRecognizer)
    optional func onTapEditCoverView(sender: UITapGestureRecognizer)
    optional func onTapFriendList()
    optional func onHandleAddFriend(friendStatus: Bool)
    optional func onHandleAddFollow(followStatus: Bool)
    optional func onTapMyFollowersListView(sender: UIButton)
    optional func onTapMerchantListView(sender: UIButton)
}

enum ButtonType: Int {
	case FriendOrWishlist = 10,
	FollowingMerchants,
	FollowingCurators,
	FollowingUsers,
	MyFollowers
}
enum ImageType: Int {
    case Cover = 20,
    Avatar
}

class HeaderMyProfileCell: UICollectionReusableView, MHFacebookImageViewerDatasource {
    
    private final let HeightForCell: CGFloat = 134.0
    private final let HeightNavigation: CGFloat = 44.0
    private final let ImageAvatarWidth : CGFloat = 70.0
    private final let ImageIconWidth: CGFloat = 20.0
    private final let HeighLabelUserName: CGFloat = 18.0
    private final let HeightAvatar: CGFloat = 92.0
    
    private final let HeightActionView: CGFloat = 70.0
    private final let HeightBottomView: CGFloat = 44.0
    private final let marginTop:CGFloat = 10.0
    private final let ImageCameraWidth:CGFloat = 25.0
    private final let marginLeftCamera:CGFloat = 15.0
    private final let ImageCameraHeight: CGFloat = 20.0
    private final let MarginActionButton:CGFloat = 20.0
    private final let WidthImageCameraBottom:CGFloat = 80.0
    private final let grayColor = UIColor(hexString: "#B1B1B1")
    private final let NotiHasAvatar = "NotiHasAvatar"
    private final let WidthItemButton:CGFloat = 30
    private final let HeightItemButton:CGFloat = 30
    private final let space:CGFloat = 0.0
    
    
    var coverImageView          = UIImageView()
    var overlay                 = UIImageView()
    var avatarViewContain       = UIView()
    var avatarView              = UIView()
    var imageViewAvatar         = UIImageView()
    var imageViewCameraAvatar   = UIImageView()
    var backgroundViewCamara    = UIImageView()
    var imageViewIcon           = UIImageView()
    var labelUsername           = UILabel()
    
    var actionView              = UIView()
    var buttonWhistlist         = UIButton()
    var buttonFollowBrand       = UIButton()
    var buttonFollowCurator     = UIButton()
    var buttonFollowUser        = UIButton()
    var buttonMyFollowers       = UIButton()
    
    var  bottomView             = UIView()
    var viewCameraCover         = UIImageView()
    var imageViewCamera         = UIImageView()
    var labelEdit               = UILabel()
    var labelFollower           = UILabel()
    var labelNumberFollower     = UILabel()
    
    var viewRight               = UIView()
    var imageViewAdd            = UIImageView()
    var buttonAddFriend         = UIButton()
    var imageViewOk             = UIImageView()
    var buttonFollow            = UIButton()
    var buttonOption            = UIButton()
    
    var IsCuratorUser:Bool = false
    var IsPriviteProfile: Bool = false
    var delegateMyProfile: HeaderMyProfileDelegate?
    var currentUser: User?
    var isTapProfile: Bool = false
    var wishlistCount: Int = 0
    var statusFriend: Bool = false
    var statusFollow: Bool = false
    var currentTypeProfile: TypeProfile = TypeProfile.Private
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.userInteractionEnabled = true
        self.backgroundColor = UIColor.whiteColor()
        
        coverImageView.image = UIImage(named: "deflaut_cover")
        coverImageView.tag = ImageType.Cover.rawValue
        coverImageView.userInteractionEnabled = false
        self.addSubview(coverImageView)
        
        overlay.image = UIImage(named: "overlay")
        self.addSubview(overlay)
        
        //create avatar View
        imageViewAvatar.image = UIImage(named: "deflaut_profile_icon")
        imageViewAvatar.tag = ImageType.Avatar.rawValue
        imageViewAvatar.setupImageViewerWithDatasource(self, initialIndex: 0, parentTag:ImageType.Avatar.rawValue, onOpen: { () -> Void in }, onClose: { () -> Void in })
        backgroundViewCamara.image = UIImage(named: "profile_border_edit")
        backgroundViewCamara.backgroundColor = UIColor.clearColor()
        imageViewAvatar.addSubview(backgroundViewCamara)
        imageViewAvatar.backgroundColor = UIColor.clearColor()
        let tapAvatar = UITapGestureRecognizer(target: self, action: Selector("onTapAvatar:"))
        backgroundViewCamara.userInteractionEnabled = true
        backgroundViewCamara.addGestureRecognizer(tapAvatar)
        
        
        avatarView.addSubview(imageViewAvatar)
        imageViewIcon.image = UIImage(named: "")
        avatarView.addSubview(imageViewIcon)
        avatarView.backgroundColor = UIColor.clearColor()
        avatarViewContain.addSubview(avatarView)
        
        //create label user name
        labelUsername.text = " "
        labelUsername.formatSize(15)
        labelUsername.textColor = UIColor.whiteColor()
        labelUsername.textAlignment = NSTextAlignment.Center
        labelUsername.backgroundColor = UIColor.clearColor()
//        avatarViewContain.addSubview(labelUsername)
        self.addSubview(labelUsername)
        avatarViewContain.backgroundColor = UIColor.clearColor()
        self.addSubview(avatarViewContain)
        
        //create action View
        buttonWhistlist = self.createButton(ButtonType.FriendOrWishlist.rawValue, textName: String.localize(""),number: "")
        buttonFollowBrand = self.createButton(ButtonType.FollowingMerchants.rawValue, textName: String.localize("LB_CA_FOLLOWING_BRAND"),number: "")
        buttonFollowCurator = self.createButton(ButtonType.FollowingCurators.rawValue, textName: String.localize("LB_CA_FOLLOWING_CURATOR"),number: "")
        buttonFollowUser = self.createButton(ButtonType.FollowingUsers.rawValue, textName: String.localize("LB_CA_FOLLOWING_USER"),number: "")
        if let view = buttonFollowUser.viewWithTag(9) {
            view.backgroundColor = UIColor.clearColor()
        }
        actionView.addSubview(buttonWhistlist)
        actionView.addSubview(buttonFollowBrand)
        actionView.addSubview(buttonFollowCurator)
        actionView.addSubview(buttonFollowUser)
        self.addSubview(actionView)
        
        viewCameraCover.image = UIImage(named: "btn_edit")
        labelEdit.text = String.localize("LB_CA_EDIT")
        labelEdit.formatSize(15)
        labelEdit.textColor = UIColor.whiteColor()
        viewCameraCover.addSubview(labelEdit)
        
        let tapEdit = UITapGestureRecognizer(target: self, action: Selector("onTapEdit:"))
        viewCameraCover.addGestureRecognizer(tapEdit)
        viewCameraCover.userInteractionEnabled = true
        bottomView.addSubview(viewCameraCover)
        
        labelFollower.text = String.localize("LB_CA_FOLLOWER")
        labelFollower.formatSize(13)
        labelFollower.textColor = UIColor.whiteColor()
        
        bottomView.addSubview(labelFollower)
        labelNumberFollower.text = ""
        labelNumberFollower.formatSize(18)
        labelNumberFollower.textColor = UIColor.whiteColor()
        bottomView.addSubview(labelNumberFollower)

        
        viewRight.hidden = true
        imageViewAdd.image = UIImage(named: "btn_add")
        viewRight.addSubview(imageViewAdd)
        viewRight.addSubview(buttonAddFriend)
        buttonAddFriend.setTitle(String.localize("LB_CA_ADD_FRIEND"), forState: .Normal)
        buttonAddFriend.titleLabel?.formatSize(14)
        buttonAddFriend.addTarget(self, action: Selector("onHandleAddFriend:"), forControlEvents: .TouchUpInside)
        viewRight.addSubview(imageViewOk)
        imageViewOk.image = UIImage(named: "btn_ok")
        viewRight.addSubview(buttonFollow)
        buttonFollow.setTitle(String.localize("LB_CA_FOLLOW"), forState: .Normal)
        buttonFollow.titleLabel?.formatSize(14)
        buttonFollow.addTarget(self, action: Selector("onHandleFollow:"), forControlEvents: .TouchUpInside)
        buttonFollow.backgroundColor = UIColor.clearColor()
//        viewRight.addSubview(buttonOption)
//        buttonOption.setImage(UIImage(named: "option"), forState: .Normal)
        viewRight.backgroundColor = UIColor.clearColor()
        
        bottomView.addSubview(viewRight)
		
		//create My Followers button
		buttonMyFollowers = self.createButton(ButtonType.MyFollowers.rawValue, textName: "",number: "")
        if let view = buttonMyFollowers.viewWithTag(9) {
            view.backgroundColor = UIColor.clearColor()
        }
		bottomView.addSubview(buttonMyFollowers)

        //create bottom View
        bottomView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.addSubview(bottomView)
        let objects = [imageViewAvatar, coverImageView]
        
        coverImageView.setupImageViewerWithDatasource(self, initialIndex: 0, parentTag:ImageType.Cover.rawValue, onOpen: { () -> Void in }, onClose: { () -> Void in })
        if currentTypeProfile == .Private {
            NSNotificationCenter.defaultCenter().postNotificationName(NotiHasAvatar, object: objects)
        }
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        coverImageView.frame = CGRectMake(0, 0, self.frame.width, self.frame.height - 5)
        overlay.frame = coverImageView.frame
        
        //setup bottomView
        bottomView.frame = CGRectMake(0, self.frame.height - HeightBottomView - 5, self.frame.width
            , HeightBottomView)
        let widthLabelEdit = self.getTextWidth(labelEdit.text!, height: 22.0, font: labelEdit.font)
        let widthViewCameraCover = WidthImageCameraBottom
        viewCameraCover.frame = CGRectMake(bottomView.frame.size.width - widthViewCameraCover, 14, widthViewCameraCover, bottomView.frame.height - 14)
        labelEdit.frame = CGRectMake(viewCameraCover.frame.width - widthLabelEdit - marginTop, (viewCameraCover.frame.size.height - 22.0)/2, widthLabelEdit, 22.0)
        imageViewCamera.frame = CGRectMake(marginLeftCamera, (viewCameraCover.frame.size.height - ImageCameraHeight)/2, ImageCameraWidth, ImageCameraHeight)
        let widthLabelFollower = self.getTextWidth(labelFollower.text!, height: 22.0, font: labelFollower.font)
        labelFollower.frame = CGRectMake(16, (bottomView.frame.height - 22.0) / 2, widthLabelFollower, 22.0)
		buttonMyFollowers.frame = CGRectMake(0, 5, widthViewCameraCover, bottomView.frame.height - 5)
        updateFrameLabelFolling()
        

        // add view follow
        let WidthViewRight =  self.frame.width / 2 - 20
        viewRight.frame =  CGRectMake(bottomView.frame.size.width - WidthViewRight, 0, WidthViewRight, HeightBottomView)
        imageViewAdd.frame = CGRectMake(space, (HeightBottomView - HeightItemButton)/2, WidthItemButton, HeightItemButton)
        let widthItemAdd = getTextWidth((buttonAddFriend.titleLabel?.text)!, height: buttonAddFriend.frame.height, font: (buttonAddFriend.titleLabel?.font)!)
        buttonAddFriend.frame = CGRectMake(imageViewAdd.frame.origin.x + WidthItemButton + space, (HeightBottomView - HeightItemButton)/2, widthItemAdd, HeightItemButton)
        imageViewOk.frame = CGRectMake(buttonAddFriend.frame.origin.x + buttonAddFriend.frame.size.width + space, (HeightBottomView - HeightItemButton)/2, WidthItemButton, HeightItemButton)
        let widthItemFollow = getTextWidth((buttonFollow.titleLabel?.text)!, height: buttonFollow.frame.height, font: (buttonFollow.titleLabel?.font)!)
        buttonFollow.frame = CGRectMake(imageViewOk.frame.origin.x + imageViewOk.frame.width + space, (HeightBottomView - HeightItemButton)/2, widthItemFollow, HeightItemButton)
        
        //setup action View
        actionView.frame = CGRectMake(0, bottomView.frame.origin.y - HeightActionView, self.frame.width, HeightActionView + HeightBottomView)
        let widthButton = (self.frame.width - MarginActionButton * 5) / 4
        let heightButton = HeightActionView
        buttonWhistlist.frame = CGRectMake(MarginActionButton, 0, widthButton, heightButton)
        buttonFollowBrand.frame = CGRectMake(MarginActionButton + MarginActionButton + widthButton, 0, widthButton, heightButton)
        buttonFollowCurator.frame = CGRectMake(MarginActionButton * 3 + widthButton * 2, 0, widthButton, heightButton)
        buttonFollowUser.frame = CGRectMake(MarginActionButton * 4 + widthButton * 3, 0, widthButton, heightButton)
		
        // setup avatar
        avatarViewContain.frame = CGRectMake((self.frame.width - ImageAvatarWidth)/2, actionView.frame.origin.y - HeightAvatar, ImageAvatarWidth, ImageAvatarWidth)
        avatarView.frame = CGRectMake((avatarViewContain.frame.width - ImageAvatarWidth-10)/2, 0, ImageAvatarWidth, ImageAvatarWidth)
        backgroundViewCamara.frame = CGRectMake(0, 0, ImageAvatarWidth - 2, ImageAvatarWidth - 2)
        imageViewAvatar.frame = CGRectMake(2, 0, ImageAvatarWidth - 2, ImageAvatarWidth - 2)
        imageViewAvatar.layer.cornerRadius = ImageAvatarWidth / 2
        imageViewAvatar.layer.borderColor = UIColor.whiteColor().CGColor
        imageViewAvatar.layer.borderWidth = 2.0
        imageViewAvatar.layer.masksToBounds = true
        imageViewIcon.frame = CGRectMake(ImageAvatarWidth - ImageIconWidth - 5, 0, ImageIconWidth, ImageIconWidth)
        setFrameLabelUserName()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func updateFrame(){
        let widthItemFollow = getTextWidth((buttonFollow.titleLabel?.text)!, height: buttonFollow.frame.height, font: (buttonFollow.titleLabel?.font)!)

        buttonFollow.frame = CGRectMake(imageViewOk.frame.origin.x + imageViewOk.frame.width + space, (HeightBottomView - HeightItemButton)/2, widthItemFollow, HeightItemButton)
    }
    func setFrameLabelUserName() {
        labelUsername.frame = CGRectMake(0, avatarViewContain.frame.origin.y + avatarViewContain.frame.height, self.frame.size.width, HeighLabelUserName)
    }
    func getTextWidth(text: String, height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.width
    }
    func originY() -> CGFloat
    {
        var originY:CGFloat = 0;
        let application: UIApplication = UIApplication.sharedApplication()
        if (application.statusBarHidden)
        {
            originY = application.statusBarFrame.size.height
        }
        return originY;
    }
    
    func createButton(index: Int, textName: String, number: String) -> UIButton {
        let width = (self.frame.width - MarginActionButton * 5) / 4
        let frm = CGRectMake(0, 0, width, HeightActionView)
        let buttonParent = UIButton(frame: frm)
        buttonParent.tag = index
        buttonParent.backgroundColor = UIColor.clearColor()
        
        let labelNumber = UILabel(frame: CGRectMake(0, 10, width, 22))
        labelNumber.textAlignment = NSTextAlignment.Center
        labelNumber.text = number
        labelNumber.tag = 2
        labelNumber.formatSize(15)
        labelNumber.textColor = UIColor.whiteColor()
        buttonParent.addSubview(labelNumber)
        
        let labelName = UILabel(frame: CGRectMake(0, labelNumber.frame.origin.y + labelNumber.frame.size.height, width, 22))
        labelName.textAlignment = NSTextAlignment.Center
        labelName.text = textName
        labelName.formatSize(13)
        labelName.tag = 3
        labelName.textColor = UIColor.whiteColor()
        buttonParent.addSubview(labelName)
        
        let border = UIView(frame: CGRectMake(width + MarginActionButton / 2, 20, 1, HeightActionView - 40))
        border.backgroundColor = UIColor.whiteColor()
        border.tag = 9
        border.alpha = 0.5
        buttonParent.addSubview(border)
        
        buttonParent.addTarget(self, action: Selector("onActionHeader:"), forControlEvents: UIControlEvents.TouchUpInside)
       
        return buttonParent
    }
    func getButtonOnActionView(tag: Int) -> UIButton{
        let button = actionView.viewWithTag(tag) as? UIButton
        return button!
    }
    
    //MARK: - Handle Action
    
    func onActionHeader(sender: UIButton)
    {
        switch (sender.tag){
        case ButtonType.FriendOrWishlist.rawValue:
            if IsPriviteProfile == false {
                self.delegateMyProfile?.onTapWishlistButton!()
            } else {
                self.delegateMyProfile?.onTapFriendList!()
            }
            break
        case ButtonType.FollowingMerchants.rawValue:
            self.delegateMyProfile?.onTapMerchantListView!(sender)
            break
        case ButtonType.FollowingCurators.rawValue:
            break
        case ButtonType.FollowingUsers.rawValue:
            break
		case ButtonType.MyFollowers.rawValue:
            self.delegateMyProfile?.onTapMyFollowersListView!(sender)
			break
		default:
            break
        }
    }
    
    func onTapEdit(sender: UITapGestureRecognizer) {
        self.isTapProfile = false
        self.delegateMyProfile?.onTapEditCoverView!(sender)
    }
    func onTapAvatar(sender: UITapGestureRecognizer) {
        self.isTapProfile = true
        self.delegateMyProfile?.onTapAvatarView!(sender)
    }
    func onHandleAddFriend (sender: UIButton) {
        
        self.delegateMyProfile?.onHandleAddFriend!(statusFriend)
    }
    func onHandleFollow(sender: UIButton){
        statusFollow = !statusFollow
        configButtonAddFollow(statusFollow)
        self.delegateMyProfile?.onHandleAddFollow!(statusFollow)
    }
    //MARK - setup Data
    func setCoverImage(key : String){
        if self.frame.height > 0 {
            let filter = AspectScaledToFillSizeFilter(
                size: coverImageView.frame.size
            )
            coverImageView.af_setImageWithURL(ImageURLFactory.get(key, category: .User), placeholderImage : UIImage(named: "deflaut_cover"), filter: filter, imageTransition: .CrossDissolve(0.3))
        }
    }
    func setAvatarImage(key : String){
        let filter = AspectScaledToFitSizeFilter(
            size: imageViewAvatar.frame.size
        )
        imageViewAvatar.af_setImageWithURL(ImageURLFactory.get(key, category: .User), placeholderImage : UIImage(named: "deflaut_profile_icon"), filter: filter, imageTransition: .CrossDissolve(0.3))
    }
    
    func setupDataWithUser (user: User) {
        self.currentUser = user
        statusFriend = user.isFriendUser
        statusFollow = user.isFollowUser
        
        if  (user.profileImage != "") {
            setAvatarImage(user.profileImage)
            if currentTypeProfile == TypeProfile.Public {
                imageViewAvatar.userInteractionEnabled = true
            }
            
        } else {
            imageViewAvatar.image = UIImage(named: "deflaut_profile_icon")
            if currentTypeProfile == TypeProfile.Public {
                imageViewAvatar.userInteractionEnabled = false
            }
            
        }
        if (user.coverImage != "") {
            setCoverImage(user.coverImage)
            if currentTypeProfile == TypeProfile.Public {
                coverImageView.userInteractionEnabled = true
            }
            
        } else {
            coverImageView.image = UIImage(named: "deflaut_cover")
            if currentTypeProfile == TypeProfile.Public {
                coverImageView.userInteractionEnabled = false
            }
        }
        labelUsername.text = String(format: "%@", user.displayName)
        if user.IsCuratorUser == 1 {
            imageViewIcon.image = UIImage(named: "curator_diamond")
        } else {
            imageViewIcon.image = UIImage(named: "")
            
        }
        setupDataForFollowerBrand(user.followingMerchantCount)
        setupDataForFollowerUser(user.followingUserCount)
        if IsPriviteProfile {
            setupDataForWishlistAndFrienlist(user.friendCount)
            viewCameraCover.hidden = false
            viewRight.hidden = true
            backgroundViewCamara.hidden = false
        } else {
            //wish list count
            setupDataForWishlistAndFrienlist(wishlistCount)
            viewCameraCover.hidden = true
            viewRight.hidden = false
            backgroundViewCamara.hidden = true
            self.isTapProfile = true
            // config button with isFriend
            configButtonAddFriend(user.isFriendUser)
            configButtonAddFollow(user.isFollowUser)
        }
        setupDataForNumberFollowing(user.followerCount)
        setupDataForFollowerCurator(user.followingCuratorCount)
        
    }
    func configButtonAddFriend(isFriend: Bool) {
        
        if isFriend {
            imageViewAdd.image = UIImage(named: "btn_ok")
            buttonAddFriend.setTitle(String.localize("LB_CA_BEFRIENDED"), forState: UIControlState.Normal)
        } else {
            imageViewAdd.image = UIImage(named: "btn_add")
            buttonAddFriend.setTitle(String.localize("LB_CA_ADD_FRIEND"), forState: UIControlState.Normal)
        }
    }
    func configButtonAddFollow(isFollow: Bool) {
        if isFollow {
            imageViewOk.image = UIImage(named: "btn_ok")
            buttonFollow.setTitle(String.localize("LB_CA_FOLLOWED"), forState: .Normal)
        } else {
            imageViewOk.image = UIImage(named: "btn_add")
            buttonFollow.setTitle(String.localize("LB_CA_FOLLOW"), forState: .Normal)
        }
        updateFrame()
    }
    func setupDataForWishlistAndFrienlist(number: Int) {
        
        let btnWishlist = getButtonOnActionView(ButtonType.FriendOrWishlist.rawValue)
        (btnWishlist.viewWithTag(2) as? UILabel)?.text = String(format: "%d", number)
        if IsPriviteProfile {
            (btnWishlist.viewWithTag(3) as? UILabel)?.text = String(format: "%@", String.localize("LB_CA_FRIEND_LIST"))
        } else {
            (btnWishlist.viewWithTag(3) as? UILabel)?.text = String(format: "%@", String.localize("LB_CA_WISHLIST_PROD"))
        }
    }
    func setupDataForFollowerBrand(numberFollowerBrand: Int) {
        let btnFolowBrand = getButtonOnActionView(ButtonType.FollowingMerchants.rawValue)
        (btnFolowBrand.viewWithTag(2) as? UILabel)?.text = String(format: "%d", numberFollowerBrand)
    }
    func setupDataForFollowerCurator(numberFollowerCurator: Int){
        let btnFolowCurator = getButtonOnActionView(ButtonType.FollowingCurators.rawValue)
        (btnFolowCurator.viewWithTag(2) as? UILabel)?.text = String(format: "%d", numberFollowerCurator)
    }
    func setupDataForFollowerUser( numberFollowerUser: Int) {
        let btnFolowUser = getButtonOnActionView(ButtonType.FollowingUsers.rawValue)
        (btnFolowUser.viewWithTag(2) as? UILabel)?.text = String(format: "%d", numberFollowerUser)
    }
    func setupDataForNumberFollowing(numberFollowing: Int) {
        labelNumberFollower.text = String(format: "%d", numberFollowing)
        updateFrameLabelFolling()
    }
    func updateFrameLabelFolling() {
        let widthLabelFollower = self.getTextWidth(labelFollower.text!, height: 22.0, font: labelFollower.font)
        let widthLabelNumberFollower = self.getTextWidth(labelNumberFollower.text!, height: 22.0, font: labelNumberFollower.font)
        let xLabel = 16 + widthLabelFollower + 10
        labelNumberFollower.frame = CGRectMake(xLabel, (bottomView.frame.height - 22.0) / 2 , widthLabelNumberFollower, 22.0)
    }
    
    //MARK: -- datasource ImageViewer
    func numberImagesForImageViewer(imageViewer :MHFacebookImageViewer) -> NSInteger {
        return 1
    }
    
    func imageURLAtIndex(index: NSInteger, imageViewer:MHFacebookImageViewer ) -> NSURL {
        Log.debug("imageURLAtIndex : \(index)")
        imageViewer.isProfile = true;
        let key:String?
        if imageViewer.parentTag == ImageType.Avatar.rawValue {
            key = self.currentUser?.profileImage
        } else {
            key = self.currentUser?.coverImage
        }
        return ImageURLFactory.get(key!, category: .User);
        
        
    }
    
    func imageDefaultAtIndex(index: NSInteger, imageViewer:MHFacebookImageViewer ) -> UIImage {
        return UIImage(named: "holder")!
    }
    
    
}
