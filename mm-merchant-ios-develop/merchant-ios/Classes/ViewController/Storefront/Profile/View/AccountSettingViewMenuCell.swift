//
//  AccountSettingViewMenuCell.swift
//  merchant-ios
//
//  Created by Stephen Yuen on 18/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

class AccountSettingViewMenuCell: UICollectionViewCell {
    
    private final let MarginLeft: CGFloat = 20
    private final let MenuItemLabelHeight: CGFloat = 42
    
    var menuItemLabel = UILabel()
    private var disclosureIndicatorImageView = UIImageView()
    private var borderView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        self.clipsToBounds = true
        
        menuItemLabel.formatSize(14)
        addSubview(menuItemLabel)
        
        disclosureIndicatorImageView.image = UIImage(named: "filter_right_arrow")
        addSubview(disclosureIndicatorImageView)
        
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        disclosureIndicatorImageView.frame = CGRect(x: bounds.maxX - 35 , y: bounds.midY - disclosureIndicatorImageView.image!.size.height / 2 , width: disclosureIndicatorImageView.image!.size.width, height: disclosureIndicatorImageView.image!.size.height)
        
        menuItemLabel.frame = CGRect(
            x: MarginLeft,
            y: bounds.midY - (MenuItemLabelHeight / 2),
            width: disclosureIndicatorImageView.frame.origin.x - MarginLeft,
            height: MenuItemLabelHeight
        )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showBorder(isShow: Bool) {
        borderView.hidden = !isShow
    }
    
    func showDisclosureIndicator(isShow: Bool) {
        disclosureIndicatorImageView.hidden = !isShow
    }
}
