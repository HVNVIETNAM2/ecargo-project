//
//  MyFollowersListViewController.swift
//  merchant-ios
//
//  Created by Markus Chow on 14/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit

import PromiseKit
import ObjectMapper
import Alamofire

class MyFollowersListViewController: MmViewController, SearchFriendViewCellDelegate {

	private final let CellId = "Cell"
	private final let searchFriendViewCell = "SearchFriendViewCell"
	
	private final let CatCellHeight : CGFloat = 40
	private final let CellHeight : CGFloat = 85
	var contentView = UIView()
	var followers: NSMutableArray = NSMutableArray()
	var start: Int = 0
	var limit: Int = 6
//	var isLoadMore:Bool = false
//	var isEmpty: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		setupCollectionView()
    }

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		start = 0
		self.followers.removeAllObjects()
		self.updateFollowersListView(start, pageSize: limit)
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	//MARK: - style View
	func setupCollectionView() {
		self.collectionView.delegate = self
		self.collectionView.dataSource = self
		self.collectionView.frame = CGRectMake(0, 0, self.view.bounds.width, self.view.bounds.height - 104)
		self.collectionView!.registerClass(SearchFriendViewCell.self, forCellWithReuseIdentifier: searchFriendViewCell)
		
	}
	
	//MARK: SearchFriendViewCellDelegate
	func addFriendClicked(rowIndex: Int){
		Log.debug("addFriendClicked: \(rowIndex)")
		let user = self.followers[rowIndex] as! User
		if user.friendStatus.length == 0 || user.friendStatus == String.localize("LB_CA_ADD_FRIEND") { //Not friend
			self.addFriend(user)
		} else if user.friendStatus == String.localize("LB_CA_FRD_REQ_CANCEL") {
			self.deleteRequest(user)
		}
		self.renderFollowersListView()
	}
	func followClicked(rowIndex: Int){
		Log.debug("followClicked: \(rowIndex)")
		let user = self.followers[rowIndex] as! User
		if user.followStatus.length == 0 || user.followStatus == String.localize("LB_CA_FOLLOW") { //Not follow
			user.followStatus = String.localize("LB_CA_FOLLOWED")
			//TODO call api here but api net ready yet
			self.showSuccessPopupWithText(String.localize("MSG_SUC_FOLLOWED"))
		}
		self.renderFollowersListView()
	}
	func deleteRequest(user:User) {
		self.showLoading()
		firstly{
			return self.deleteFriendRequest(user)
			}.then
			{ _ -> Void in
				user.friendStatus = String.localize("LB_CA_ADD_FRIEND")
				self.updateFollowersListView(self.start, pageSize: self.limit)
			}.always {
				self.stopLoading()
			}.error { _ -> Void in
				Log.error("error")
		}
	}
	
	func deleteFriendRequest(user:User) -> Promise<AnyObject> {
		return Promise{ fulfill, reject in
			FriendService.deleteRequest(user, completion:
				{
					[weak self] (response) in
					if let strongSelf = self {
						Log.debug(String(data: response.data!, encoding: 4))
						if response.result.isSuccess {
							if response.response?.statusCode == 200 {
								fulfill("OK")
							} else {
								strongSelf.handleApiResponseError(response, reject: reject)
							}
						}
						else{
							reject(response.result.error!)
							strongSelf.handleApiResponseError(response, reject: reject)
						}
					}
				})
		}
	}
	
	func addFriend(user:User) {
		self.showLoading()
		firstly{
			return self.addFriendRequest(user)
			}.then
			{ _ -> Void in
				user.friendStatus = String.localize("LB_CA_FRD_REQ_CANCEL")
				self.showSuccessPopupWithText(String.localize("MSG_SUC_FRIEND_REQ_SENT"))
				self.updateFollowersListView(self.start, pageSize: self.limit)
			}.always {
				self.stopLoading()
			}.error { _ -> Void in
				Log.error("error")
		}
	}
	func addFriendRequest(user:User) -> Promise<AnyObject> {
		return Promise{ fulfill, reject in
			FriendService.addFriendRequest(user, completion:
				{
					[weak self] (response) in
					if let strongSelf = self {
						Log.debug(String(data: response.data!, encoding: 4))
						if response.result.isSuccess {
							if response.response?.statusCode == 200 {
								fulfill("OK")
							} else {
								strongSelf.handleApiResponseError(response, reject: reject)
							}
						}
						else{
							reject(response.result.error!)
							strongSelf.handleApiResponseError(response, reject: reject)
						}
					}
				})
		}
	}
	
	func fetchPublicUser(userKey: String) -> Promise<AnyObject>{
		return Promise{ fulfill, reject in
			UserService.viewWithUserKey(userKey){[weak self] (response) in
				if response.result.isSuccess {
					if response.response?.statusCode == 200 {
						
						let user = Mapper<User>().map(response.result.value)!
						
						self!.showPublicProfile(user)
						
						fulfill("OK")
						
					} else {
						var statusCode = 0
						if let code = response.response?.statusCode {
							statusCode = code
						}
						
						let error = NSError(domain: "", code: statusCode, userInfo: nil)
						reject(error)
					}
				} else {
					reject(response.result.error!)
				}
			}
		}
	}
	
	func showPublicProfile(user: User) {
		let publicProfileVC = ProfileViewController()
		publicProfileVC.currentType = TypeProfile.Public
		publicProfileVC.publicUser = user
		self.navigationController?.pushViewController(publicProfileVC, animated: true)
	}
	
	//MARK: - delegate & datasource
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	
	override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		switch (collectionView) {
		case self.collectionView:
			return self.followers.count
		default:
			return	0
		}
	}
	
	override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		switch collectionView {
		case self.collectionView:
			let cell = collectionView.dequeueReusableCellWithReuseIdentifier(searchFriendViewCell, forIndexPath: indexPath) as! SearchFriendViewCell
			cell.searchFriendViewCellDelegate = self
			//            cell.followButton.addTarget(self, action: Selector("onHandleFollow:"), forControlEvents:.n)
			if self.followers.count > 0 {
				let follower = self.followers[indexPath.row]
				cell.setData(follower as! User)
			}
//			if indexPath.row == self.followers.count - 1 && isEmpty == false {
//				start = self.followers.count + 1
//				isLoadMore = true
//				self.fetchFollowsList(start, pageSize: limit)
//			}
			return cell
			
		default:
			return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
			
		}
	}
	
	func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
		return cell
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
		return 0.0
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
		return 0.0
	}
	func collectionView(collectionView: UICollectionView,
		layout collectionViewLayout: UICollectionViewLayout,
		sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
			switch (collectionView) {
			case self.collectionView:
				return CGSizeMake(self.view.frame.size.width , CellHeight)
			default:
				return CGSizeMake(self.view.frame.size.width / 4, Constants.Value.CatCellHeight)
			}
			
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		let thisUser = self.followers[indexPath.row] as! User
		self.fetchPublicUser(thisUser.userKey)
	}
	
	func renderFollowersListView() {
		self.collectionView.reloadData()
	}
	//MARK: - Loading Data
	func fetchFollowsList(pageIndex: Int, pageSize: Int) -> Promise<AnyObject>{
		return Promise{ fulfill, reject in
			FollowService.listFollowers(pageIndex, limit: pageSize, completion: { [weak self] (response) in
				if let strongSelf = self {
					if response.result.isSuccess {
						if response.response?.statusCode == 200 {
							let followers:[User] = Mapper<User>().mapArray(response.result.value) ?? []

							Log.debug("followers.count : \(followers.count)")
							
							if followers.count > 0 {
//								self?.isEmpty = false
								for follower in followers {
									strongSelf.followers.addObject(follower)
								}
							} else {
//								self?.isEmpty = true
							}
//							if self!.isLoadMore == true {
//								self?.renderFollowersListView()
//								self?.isLoadMore = false
//							}
							fulfill("OK")
						} else {
							var statusCode = 0
							if let code = response.response?.statusCode {
								statusCode = code
							}
							
							let error = NSError(domain: "", code: statusCode, userInfo: nil)
							reject(error)
						}
					} else {
						reject(response.result.error!)
					}
				}
				
				})
		}
	}
	
	func updateFollowersListView(pageIndex: Int, pageSize: Int){
		self.showLoading()
		firstly{
			
			return self.fetchFollowsList(pageIndex, pageSize: pageSize)
			}.then { _ -> Void in
				self.renderFollowersListView()
			}.always {
				self.renderFollowersListView()
				self.stopLoading()
			}.error { _ -> Void in
				Log.error("error")
		}
	}
	
	//MARK: - delegate merchant cell
	func onTapFollowHandle(sender: UIButton) {
		
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
