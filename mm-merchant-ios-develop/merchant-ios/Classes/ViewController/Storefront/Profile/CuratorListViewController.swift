//
//  CuratorListViewController.swift
//  merchant-ios
//
//  Created by Trung Vu on 3/7/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit

class CuratorListViewController: MmViewController {
    
    
    private final let CellId = "Cell"
    private final let CuratorListViewCellId = "CuratorListViewCell"
    
    private final let CatCellHeight : CGFloat = 40
    private final let CellHeight : CGFloat = 65
    private final let heightTopView: CGFloat = 144
    var contentView = UIView()
    var dataSource  = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - style View
    func setupCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.frame = CGRectMake(0, 0, self.view.bounds.width, self.view.bounds.height - heightTopView)
        self.collectionView!.registerClass(CuratorListViewCell.self, forCellWithReuseIdentifier: CuratorListViewCellId)
    }
    override func shouldHideTabBar() -> Bool {
        return true
    }
    //MARK: - delegate & datasource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch (collectionView) {
        case self.collectionView:
            return 10
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.collectionView:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CuratorListViewCellId, forIndexPath: indexPath) as! CuratorListViewCell
            return cell
            
        default:
            return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
            
        }
    }
    
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch (collectionView) {
            case self.collectionView:
                return CGSizeMake(self.view.frame.size.width , CellHeight)
            default:
                return CGSizeMake(self.view.frame.size.width / 4, Constants.Value.CatCellHeight)
            }
            
    }
    
    
}
