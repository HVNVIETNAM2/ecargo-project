//
//  MerchantListViewController.swift
//  merchant-ios
//
//  Created by Trung Vu on 3/7/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit
import PromiseKit
import ObjectMapper
import Alamofire
class MerchantListViewController: MmViewController, MerchantCellDelegate, FollowViewControllerDelegate {
    
    private final let CellId = "Cell"
    private final let MerchantListCellId = "MerchantListViewCell"
    
    private final let CatCellHeight : CGFloat = 40
    private final let CellHeight : CGFloat = 85
    private final let heightTopView: CGFloat = 144
    var contentView = UIView()
    var dataSource  = NSArray()
    var merchants: NSMutableArray = NSMutableArray()
    var start: Int = 0
    var limit: Int = 6
//    var IsLoadMore:Bool = false
//    var IsEmpty: Bool = false
    var orgMerchants = [Merchant]()
    var arrayMerchant = [Merchant]()
    
    var currentTypeProfile: TypeProfile = TypeProfile.Private
    var user: User = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        start = 0
        self.merchants.removeAllObjects()
        
        switch (currentTypeProfile) {
        case .Private:
            self.updateMerchantView(start, pageSize: limit)
            break
        case .Public:
            self.getMerchantListForPublic(start, pageSize: limit, userKey: user.userKey)
            break
        }
        
    }
    //MARK: - style View
    func setupCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.frame = CGRectMake(0, 0, self.view.bounds.width, self.view.bounds.height - heightTopView)
        self.collectionView!.registerClass(MerchantListViewCell.self, forCellWithReuseIdentifier: MerchantListCellId)
        
    }
    override func shouldHideTabBar() -> Bool {
        return true
    }
    //MARK: - delegate & datasource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch (collectionView) {
        case self.collectionView:
            return self.merchants.count
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.collectionView:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(MerchantListCellId, forIndexPath: indexPath) as! MerchantListViewCell
            cell.delegateMerchantList = self
            cell.followButton.tag = indexPath.row
            if self.merchants.count > 0 {
                let merchant = self.merchants[indexPath.row]
                cell.setupDataCell(merchant as! Merchant)
            }

            return cell
            
        default:
            return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
            
        }
    }
    
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch (collectionView) {
            case self.collectionView:
                return CGSizeMake(self.view.frame.size.width , CellHeight)
            default:
                return CGSizeMake(self.view.frame.size.width / 4, Constants.Value.CatCellHeight)
            }
            
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let merchantDetailVC = MerchantDetailViewController()
        merchantDetailVC.merchant = self.orgMerchants[indexPath.row]
        Log.debug(self.merchants[indexPath.row])
        self.navigationController?.pushViewController(merchantDetailVC, animated: true)
    }
    
    func renderMerchantView() {
        self.merchants = NSMutableArray(array: self.arrayMerchant)
        self.collectionView.reloadData()
    }
    //MARK: - Loading Data Private
    func fetchFollowMerchantList(pageIndex: Int, pageSize: Int) -> Promise<AnyObject>{
        return Promise{ fulfill, reject in
            FollowService.listFollowMerchant(pageIndex, limit: pageSize, completion: { [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            let merchants:[Merchant] = Mapper<Merchant>().mapArray(response.result.value) ?? []
                            strongSelf.orgMerchants = merchants
                            if merchants.count > 0 {
//                                self?.IsEmpty = false
                                for merchant in merchants {
                                    merchant.followStatus = true
                                    strongSelf.merchants.addObject(merchant)
                                }
                                strongSelf.arrayMerchant = NSArray(array: strongSelf.merchants) as! [Merchant]
                            } else {
                            }
                            fulfill("OK")
                        } else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    } else {
                        reject(response.result.error!)
                    }
                }
                
                })
        }
    }
    
    func filter(text: String){
        let array = self.arrayMerchant.filter(){ ($0.merchantNameInvariant).lowercaseString.rangeOfString(text.lowercaseString) != nil }
        self.merchants = NSMutableArray(array: array)
        self.collectionView.reloadData()
    }
    
    func updateMerchantView(pageIndex: Int, pageSize: Int){
        self.showLoading()
        firstly{
            
            return self.fetchFollowMerchantList(pageIndex, pageSize: pageSize)
            }.then { _ -> Void in
                self.renderMerchantView()
            }.always {
                self.renderMerchantView()
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    func getMerchantListForPublic(pageIndex: Int, pageSize: Int, userKey: String) {
        self.showLoading()
        firstly{
            
            return self.fetchFollowMerchantListForPublic(pageIndex, pageSize: pageSize, userKey: userKey)
            }.then { _ -> Void in
                self.renderMerchantView()
            }.always {
                self.renderMerchantView()
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    func unfollowMerchant(merchant: Merchant) {
        firstly{
            
            return unfollow(merchant)
            }.then { _ -> Void in
                self.renderMerchantView()
            }.always {
                self.renderMerchantView()
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    func unfollow(merchant : Merchant)-> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FollowService.deleteMerchant(merchant){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.response?.statusCode == 200 {
                        Log.debug(String(data: response.data!, encoding : 4))
                        fulfill("OK")
                    } else {
                        strongSelf.handleError(response, animated: true)
                    }
                    
                }
            }
        }
    }
    func followMerchant(merchant: Merchant) {
        firstly{
            
            return follow(merchant)
            }.then { _ -> Void in
                self.renderMerchantView()
            }.always {
                self.renderMerchantView()
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    func follow(merchant : Merchant)-> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FollowService.followMerchant(merchant){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.response?.statusCode == 200 {
                        Log.debug(String(data: response.data!, encoding : 4))
                        fulfill("OK")
                    } else {
                        strongSelf.handleError(response, animated: true)
                    }
                    
                }
            }
        }
    }
    
    //MARK: - loading api Public 
    func fetchFollowMerchantListForPublic(pageIndex: Int, pageSize: Int, userKey: String) -> Promise<AnyObject>{
        return Promise{ fulfill, reject in
            FollowService.listFollowMerchantWithUserkey(pageIndex, limit: pageSize, userKey: userKey, completion: { [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            let merchants:[Merchant] = Mapper<Merchant>().mapArray(response.result.value) ?? []
                            if merchants.count > 0 {
//                                self?.IsEmpty = false
                                for merchant in merchants {
                                    merchant.followStatus = true
                                    strongSelf.merchants.addObject(merchant)
                                }
                                strongSelf.arrayMerchant = NSArray(array: strongSelf.merchants) as! [Merchant]
                            } else {
//                                self?.IsEmpty = true
                            }
//                            if self!.IsLoadMore == true {
//                                self?.renderMerchantView()
//                                self?.IsLoadMore = false
//                            }
                            fulfill("OK")
                        } else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    } else {
                        reject(response.result.error!)
                    }
                }
                
                })
        }
    }

    
    //MARK: - delegate merchant cell
    func onTapFollowHandle(rowIndex: Int) {
        let merchant = self.merchants[rowIndex] as! Merchant
        if merchant.followStatus == true {
            self.unfollowMerchant(merchant)
        } else {
            self.followMerchant(merchant)
        }
        merchant.followStatus = !merchant.followStatus
    }
    
    func didSelectCancelButton() {
        log.debug("Cancel Search")
    }
    func didSelectSearchButton(text: String) {
        self.filter(text)
    }
    func didTextChange(text: String) {
        if text.length == 0 {
            self.renderMerchantView()
        } else {
            self.filter(text)
        }
    }
}
