//
//  AccountSettingViewController.swift
//  merchant-ios
//
//  Created by Stephen Yuen on 18/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit

class AccountSettingViewController: MmViewController, UIGestureRecognizerDelegate {
    
    private final let CellHeight: CGFloat = 45
    private final let EmptyCellHeight: CGFloat = 22
    private final let LogoutButtonHeight: CGFloat = 45
    
    private final let MenuItems = [
        [
            ["name": String.localize("LB_CA_PERSONAL_INFO"), "image": "icon_personal_info", "hasBorder": true, "selector": "showPersonalInformationSetting"],
            ["name": String.localize("LB_CA_ACCT_MGMT"), "image": "icon_acct_mgmt", "hasBorder": true],
            ["name": String.localize("LB_CA_SHIPPING_ADDR"), "image": "icon_shipping_addr", "hasBorder": true],
            ["name": String.localize("LB_CA_PAYMENT_METHOD"), "image": "icon_payment_method", "hasBorder": true]
        ],
        [
            ["hasBorder": true]
        ],
        [
            ["name": String.localize("LB_CA_ABOUT"), "hasBorder": true],
            ["name": String.localize("LB_CA_FEEDBACK"), "hasBorder": true],
            ["name": String.localize("LB_CA_MM_RATING"), "hasBorder": true],
            ["name": String.localize("LB_CA_GENERAL"), "hasBorder": true]
        ]
    ]
    
    let logoutButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = String.localize("LB_CA_SETTINGS")
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationController!.interactivePopGestureRecognizer!.delegate = self
        
        self.createBackButton()
        self.setupSubViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setup Views
    
    func setupSubViews() {
        self.collectionView.registerClass(AccountSettingViewImageMenuCell.self, forCellWithReuseIdentifier: "AccountSettingViewImageMenuCell")
        self.collectionView.registerClass(AccountSettingViewMenuCell.self, forCellWithReuseIdentifier: "AccountSettingViewMenuCell")
        
        logoutButton.frame = CGRect(x: 10, y: self.view.bounds.height - (LogoutButtonHeight + 54), width: self.view.frame.width - 20, height: LogoutButtonHeight )
        logoutButton.formatSecondary()
        logoutButton.setTitleColor(UIColor.primary1(), forState: UIControlState.Normal)
        logoutButton.setTitle(String.localize("LB_CA_PROF_LOGOUT"), forState: .Normal)
        logoutButton.addTarget(self, action: "logout:", forControlEvents: .TouchUpInside)
        
        self.view.addSubview(logoutButton)
    }
    
    // MARK: - Action
    
    func showPersonalInformationSetting() {
        self.navigationController?.pushViewController(PersonalInformationSettingViewController(), animated: true)
    }
    
    func logout(button: UIButton){
        LoginManager.logout()
        LoginManager.goToLogin()
    }
    
    // MARK: - Collection View Data Source methods
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let menuItem = self.MenuItems[indexPath.section][indexPath.row]
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AccountSettingViewImageMenuCell", forIndexPath: indexPath) as! AccountSettingViewImageMenuCell
            
            if let itemName = menuItem["name"] as? String {
                cell.menuItemLabel.text = itemName
            }
            
            if let itemImageName = menuItem["image"] as? String {
                cell.setItemImage(imageName: itemImageName)
            }
            
            if let itemHasBorder = menuItem["hasBorder"] as? Bool {
                cell.showBorder(itemHasBorder)
            }
            
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AccountSettingViewMenuCell", forIndexPath: indexPath) as! AccountSettingViewMenuCell
            
            if let itemName = menuItem["name"] as? String {
                cell.menuItemLabel.text = itemName
            }
            
            if let itemHasBorder = menuItem["hasBorder"] as? Bool {
                cell.showBorder(itemHasBorder)
            }
            
            cell.showDisclosureIndicator(true)
            
            return cell
        default:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AccountSettingViewMenuCell", forIndexPath: indexPath) as! AccountSettingViewMenuCell
            
            cell.menuItemLabel.text = ""
            
            if let itemHasBorder = menuItem["hasBorder"] as? Bool {
                cell.showBorder(itemHasBorder)
            }
            
            cell.showDisclosureIndicator(false)
            
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.section == 1 {
            return CGSizeMake(self.view.frame.size.width, EmptyCellHeight)
        } else {
            return CGSizeMake(self.view.frame.size.width, CellHeight)
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return self.MenuItems.count
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.MenuItems[section].count
    }
    
    // MARK: - Collection View Delegate methods
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let menuItem = self.MenuItems[indexPath.section][indexPath.row]
        
        if let selectorName = menuItem["selector"] as? String {
            if self.respondsToSelector(Selector(selectorName)) {
                self.performSelector(Selector(selectorName))
            }
        }
    }
    
    func reloadAllData() {
        self.collectionView.reloadData()
    }
    
}
