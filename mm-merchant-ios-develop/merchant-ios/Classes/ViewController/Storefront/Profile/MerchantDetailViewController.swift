//
//  MerchantDetailViewController.swift
//  merchant-ios
//
//  Created by Trung Vu on 3/10/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import CSStickyHeaderFlowLayout
import Alamofire
import AlamofireImage
import Kingfisher

class MerchantDetailViewController : MmViewController, GKFadeNavigationControllerDelegate, UINavigationControllerDelegate {
    private final let WidthItemBar : CGFloat = 25
    private final let HeightItemBar : CGFloat = 25
    private final let DefaultUIEdgeInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    private final let HeaderProfileIdentifier = "HeaderMerchantProfileView"
    private final let CellId = "Cell"
    private final let HeightCellMyProfile: CGFloat = 450.0
    private final let colorBarItemBackup: UIColor = UIColor()
    private final let WidthLogo:CGFloat = 120.0
    private final let HeightLogo:CGFloat = 35.0

    var searchBarButtonItem : UIBarButtonItem!
    var buttonSearch = UIButton()
    var buttonBack = UIButton()
    var backButtonItem = UIBarButtonItem()
    var imageViewLogo: UIImageView?
    var tabBarHeight = CGFloat(0)
    var header : HeaderMerchantProfileView?
    
    var merchant = Merchant()

    
    private var layout : CSStickyHeaderFlowLayout? {
        return self.collectionView?.collectionViewLayout as? CSStickyHeaderFlowLayout
    }
    
    var navigaionBarVisibility: GKFadeNavigationControllerNavigationBarVisibility? = GKFadeNavigationControllerNavigationBarVisibility.Hidden
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let tabBarController = self.tabBarController where !shouldHideTabBar() {
            tabBarHeight = tabBarController.tabBar.bounds.height
        }
        backupButtonColorOn()
        configCollectionView()
        setupNavigationProfile()
        
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigaionBarVisibility = GKFadeNavigationControllerNavigationBarVisibility.Hidden;
        self.navigationController?.updateViewConstraints()
        self.updateMerchantView()
        
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        removeLogo()
    }
    // MARK: - setup UI
    func configCollectionView() {
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.frame = CGRect(x: 0 , y: 0, width: self.view.bounds.width, height: self.view.bounds.height - tabBarHeight)
        self.collectionView.backgroundColor = UIColor.blackColor()
        let flowLayout = CSStickyHeaderFlowLayout()
        flowLayout.disableStickyHeaders = true
        flowLayout.parallaxHeaderReferenceSize = CGSizeZero
        self.collectionView.setCollectionViewLayout(flowLayout, animated: false)
        self.collectionView.bounces = false
        // Setup Cell
        self.collectionView.registerClass(PostItemCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.layout?.itemSize = CGSizeMake(self.view.frame.size.width, 44)
        
        // Setup Header
        self.collectionView?.registerClass(HeaderMerchantProfileView.self, forSupplementaryViewOfKind: CSStickyHeaderParallaxHeader, withReuseIdentifier: HeaderProfileIdentifier)
        self.layout?.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, HeightCellMyProfile)
        
        if let navigationController = self.navigationController as? GKFadeNavigationController {
            navigationController.setNeedsNavigationBarVisibilityUpdateAnimated(false)
            
        }
    }
    
    func setupNavigationProfile() {
        setupNavigationBarButtons()
        setupNavigationBar()
    }
    func setupNavigationBar() {
        let navigationBarAppearace = UINavigationBar.appearance()
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor(hexString: "#888888")]
        
    }
    func setupNavigationBarButtons() {
        setupNavigationBarCartButton()
        setupNavigationBarWishlistButton()
        setupBarButtons()
        var rightButtonItems = [UIBarButtonItem]()
        rightButtonItems.append(UIBarButtonItem(customView: buttonCart!))
        rightButtonItems.append(UIBarButtonItem(customView: buttonWishlist!))
        
        
        
        buttonCart!.addTarget(self, action: "navigateToshoppingCart", forControlEvents: .TouchUpInside)
        buttonWishlist!.addTarget(self, action: "likeNaviBarTapped:", forControlEvents: .TouchUpInside)
        
        self.backButtonItem = self.createBack("back_wht", selectorName: "onBackButton", size: CGSize(width: Constants.Value.BackButtonWidth,height: Constants.Value.BackButtonHeight), left: -36, right: 0)
        self.searchBarButtonItem = backButtonItem
        let searchButtonItem = self.initSearchBarButton("search_wht", selectorName: "searchIconClicked", size: CGSize(width: WidthItemBar,height: 24), left: -41, right: 0)
        var leftButtonItems = [UIBarButtonItem]()
        leftButtonItems.append(backButtonItem)
        leftButtonItems.append(searchButtonItem)
        
        self.navigationItem.rightBarButtonItems = rightButtonItems
        self.navigationItem.leftBarButtonItems = leftButtonItems
        
    }
    
    func initSearchBarButton(imageName: String, selectorName: String, size:CGSize,left: CGFloat, right: CGFloat) -> UIBarButtonItem {
        buttonSearch.setImage(UIImage(named: imageName), forState: .Normal)
        buttonSearch.frame = CGRectMake(0, 0, size.width, size.height)
        buttonSearch.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: left, bottom: 0, right: right)
        buttonSearch .addTarget(self, action:Selector(selectorName), forControlEvents: UIControlEvents.TouchUpInside)
        let temp:UIBarButtonItem = UIBarButtonItem()
        temp.customView = buttonSearch
        return temp
    }
    func createBack(imageName: String, selectorName: String, size:CGSize,left: CGFloat, right: CGFloat) -> UIBarButtonItem {
        buttonBack.setImage(UIImage(named: imageName), forState: .Normal)
        buttonBack.frame = CGRectMake(0, 0, size.width, size.height)
        buttonBack.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: left, bottom: 0, right: right)
        buttonBack .addTarget(self, action:Selector(selectorName), forControlEvents: UIControlEvents.TouchUpInside)
        let temp:UIBarButtonItem = UIBarButtonItem()
        temp.customView = buttonBack
        return temp
    }
    func backupButtonColorOn() {
        buttonCart?.setImage(UIImage(named: "cart_grey"), forState: .Normal)
        buttonWishlist?.setImage(UIImage(named: "icon_heart_stroke"), forState: .Normal)
        buttonSearch.setImage(UIImage(named: "search_grey"), forState: .Normal)
        buttonBack.setImage(UIImage(named: "back_grey"), forState: .Normal)
        
        addLogoOnNavi()
    }
    func setupBarButtons() {
        
        if buttonCart != nil {
            buttonCart?.setImage(UIImage(named:"shop"), forState: .Normal)
        }
        if buttonWishlist != nil {
            buttonWishlist?.setImage(UIImage(named: "cart"), forState: .Normal)
        }
        if searchBarButtonItem != nil {
            buttonSearch.setImage(UIImage(named: "search_wht"), forState: .Normal)
        }
        buttonBack.setImage(UIImage(named: "back_wht"), forState: .Normal)
        removeLogo()
        
    }
    func addLogoOnNavi() {
        imageViewLogo = UIImageView(frame: CGRectMake((self.view.frame.width - WidthLogo)/2, 0, WidthLogo, HeightLogo))
//        imageViewLogo?.image = UIImage(named: "logo6")
        setDataImageviewLogo(merchant.headerLogoImage)
        imageViewLogo?.tag = 99
        self.navigationController?.navigationBar.addSubview(imageViewLogo!)
    }
    func setDataImageviewLogo(key : String){
//        let filter = AspectScaledToFitSizeFilter(
//            size: imageViewLogo!.frame.size
//        )
//        if filter.size.width > 0 {
//            imageViewLogo!.af_setImageWithURL(ImageURLFactory.get(key, size: imageViewLogo!.frame.size, category: .Merchant), placeholderImage : UIImage(named: "deflaut_cover"), filter: filter, imageTransition: .CrossDissolve(0.3))
//        }
        imageViewLogo?.contentMode = .ScaleAspectFit
        imageViewLogo?.kf_setImageWithURL(ImageURLFactory.get(merchant.headerLogoImage, category: .Merchant), placeholderImage: UIImage(named: "deflaut_cover"))
    }
    func removeLogo() {
        let views = self.navigationController?.navigationBar.subviews
        for (var i = 0; i < views?.count; i++) {
            let v = views![i]
            if v.tag == 99 {
                v.removeFromSuperview()
            }
        }
    }
    func originY() -> CGFloat
    {
        var originY:CGFloat = 0;
        let application: UIApplication = UIApplication.sharedApplication()
        if (application.statusBarHidden)
        {
            originY = application.statusBarFrame.size.height
        }
        return originY;
    }
    //MARK: - action button bar
    func onBackButton()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func searchIconClicked() {
		let searchStyleController = SearchStyleController()
		//        searchStyleController.styles = self.styles
		//        searchStyleController.filterStyleDelegate = self
		//        searchStyleController.styleFilter = self.styleFilter
		//        searchStyleController.popTwice = popTwice
		self.navigationController?.pushViewController(searchStyleController, animated: false)
    }
    func navigateToshoppingCart() {
        let shoppingcart = ShoppingCartViewController()
        self.navigationController?.pushViewController(shoppingcart, animated: true)
    }
    func likeNaviBarTapped(sender: UIBarButtonItem){
        let wishlist = WishListCartViewController()
        self.navigationController?.pushViewController(wishlist, animated: true)
    }
    
    //MARK: - Delegate & Datasource
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! PostItemCollectionViewCell
        cell.text = "label + \(indexPath.row)"
        cell.backgroundColor = UIColor.grayColor()
        return cell
        
    }
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
        
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return DefaultUIEdgeInsets
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSizeMake(self.view.frame.size.width, 100.0)
    }
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        if kind == CSStickyHeaderParallaxHeader {
            let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: HeaderProfileIdentifier, forIndexPath: indexPath)
            header = (view as! HeaderMerchantProfileView)
            header?.configDataWithMerchant(merchant)
            
            return header!
        }
        return UICollectionReusableView()
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSizeMake(self.view.frame.width, 0.0)
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSizeMake(self.view.frame.width, 0.0)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let scrollOffsetY = 80.0 - scrollView.contentOffset.y
        if (scrollOffsetY < 44.0) {
            self.navigaionBarVisibility = GKFadeNavigationControllerNavigationBarVisibility.Visible
            if let navigationController = self.navigationController as? GKFadeNavigationController {
                navigationController.setNavigationBarVisibility(GKFadeNavigationControllerNavigationBarVisibility.Visible, animated: true)
                backupButtonColorOn()
            }
        } else {
            self.navigaionBarVisibility = GKFadeNavigationControllerNavigationBarVisibility.Hidden
            
            if let navigationController = self.navigationController as? GKFadeNavigationController {
                navigationController.setNavigationBarVisibility(GKFadeNavigationControllerNavigationBarVisibility.Hidden, animated: true)
                setupBarButtons()
            }
        }
    }
    
    func preferredNavigationBarVisibility() -> GKFadeNavigationControllerNavigationBarVisibility {
        return self.navigaionBarVisibility!
    }
    
    func reloadDataSource() {
        self.collectionView.reloadData()
    }
    //MARK: - handle data
    func updateMerchantView(){
        self.showLoading()
        firstly{
            
            // update inventory location if needed
            // if it is not updated, it will return success without api call
            return self.fetchMerchant(merchant)
            }.then { _ -> Void in
                self.reloadDataSource()
            }.always {
                self.reloadDataSource()
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    func fetchMerchant(merchant: Merchant) -> Promise<AnyObject>{
        return Promise{ fulfill, reject in
            MerchantService.view(merchant.merchantId){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            strongSelf.merchant = Mapper<Merchant>().map(response.result.value)!
                            fulfill("OK")
                        } else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    } else {
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    

    
    //MARK: - Delegate Header Profile
    
    func onTapWishlistButton() {
        likeNaviBarTapped(UIBarButtonItem())
    }
    
}