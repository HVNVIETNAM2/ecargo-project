//
//  PersonalInformationSettingViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 19/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import FXBlurView
import PromiseKit
import Alamofire

class PersonalInformationSettingViewController: MmViewController, UINavigationControllerDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, CropImageViewControllerDelegate {
    
    private final let CellHeight: CGFloat = 45
    private final let ProfileViewCellHeight: CGFloat = 58
    
    private final let Items = [
        [
            ["name": String.localize("LB_CA_PROFILE_PIC"), "hasBorder": true],
            ["name": String.localize("LB_CA_NAME"), "valueKey": "name", "hasBorder": true],
            ["name": String.localize("LB_CA_NICKNAME"), "valueKey": "nickname", "hasBorder": true],
            ["name": String.localize("LB_CA_GENDER"), "valueKey": "gender", "hasBorder": true],
            ["name": String.localize("LB_CA_MY_QRCODE"), "image": "icon_qr_code", "hasBorder": true]
        ],
        [
            ["name": String.localize("LB_CA_MY_ACCT_LOCATION"), "hasBorder": true],
            ["name": String.localize("LB_CA_DOB"), "hasBorder": true],
            ["name": String.localize("LB_CA_ID_CARD_VER"), "hasBorder": true]
        ]
    ]
    
    var user = User()
    var picker = UIImagePickerController()
    var profileImage = UIImage()
    
    var values = [
        "name": "-",
        "nickname": "-",
        "gender": "-",
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = String.localize("LB_CA_PERSONAL_INFO")
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationController!.interactivePopGestureRecognizer!.delegate = self
        
        self.createBackButton()
        self.setupSubViews()
        self.updateUserView()
    }
    
    func setupSubViews() {
        self.collectionView.registerClass(PersonalInformationSettingProfileCell.self, forCellWithReuseIdentifier: "PersonalInformationSettingProfileCell")
        self.collectionView.registerClass(PersonalInformationSettingMenuCell.self, forCellWithReuseIdentifier: "PersonalInformationSettingMenuCell")
        self.collectionView.backgroundColor = UIColor.primary2()
        
        picker.delegate = self
    }
    
    // MARK: - Collection View Data Source methods
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let item = self.Items[indexPath.section][indexPath.row]
        
        if indexPath.section == 0 && indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PersonalInformationSettingProfileCell", forIndexPath: indexPath) as! PersonalInformationSettingProfileCell
            
            // Reset content
            cell.itemLabel.text = ""
            cell.showBorder(false)
            
            if let itemName = item["name"] as? String {
                cell.itemLabel.text = itemName
            }
            
            if let itemHasBorder = item["hasBorder"] as? Bool {
                cell.showBorder(itemHasBorder)
            }
            
            let gesture = UITapGestureRecognizer(target: self, action: "avatarClicked")
            cell.profileImageView.addGestureRecognizer(gesture)
            
            self.user.profileImage.length > 0 ? cell.setProfileImage(self.user.profileImage) : cell.setProfileImage("")
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PersonalInformationSettingMenuCell", forIndexPath: indexPath) as! PersonalInformationSettingMenuCell
            
            // Reset content
            cell.itemLabel.text = ""
            cell.valueLabel.text = ""
            cell.valueImageView.image = nil
            cell.showBorder(false)
            
            if let itemName = item["name"] as? String {
                cell.itemLabel.text = itemName
            }
            
            if let valueKey = item["valueKey"] as? String {
                if let value = values[valueKey] {
                    cell.valueLabel.text = value
                }
            }
            
            if let imageName = item["image"] as? String {
                cell.setValueImage(imageName: imageName)
            }
            
            if let itemHasBorder = item["hasBorder"] as? Bool {
                cell.showBorder(itemHasBorder)
            }
            
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        switch section {
        case 0:
            return UIEdgeInsets(top: 10.0, left: 0.0, bottom: 5.0, right: 0.0)
        case 1:
            return UIEdgeInsets(top: 5.0, left: 0.0, bottom: 5.0, right: 0.0)
        default:
            return UIEdgeInsets(top: 0.0, left: 0.0, bottom:0.0, right: 0.0)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if indexPath.section == 0 && indexPath.row == 0 {
            return CGSizeMake(self.view.frame.size.width, ProfileViewCellHeight)
        } else {
            return CGSizeMake(self.view.frame.size.width, CellHeight)
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return self.Items.count
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.Items[section].count
    }
    
    // MARK: - Collection View Delegate methods
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        Log.debug("didSelectItemAtIndexPath: \(indexPath.row)")
        //TODO implement action here
    }
    
    func reloadAllData() {
        self.collectionView.reloadData()
    }
    
    func updateUserView() {
        firstly {
            return fetchUser()
            }.then { _ -> Void in
                self.reloadAllData()
        }
    }
    
    func fetchUser() -> Promise<AnyObject> {
        return Promise { fulfill, reject in
            UserService.view(Context.getUserId()){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            //Log.debug(String(data: response.data!, encoding: 4))
                            strongSelf.user = Mapper<User>().map(response.result.value) ?? User()
                            fulfill("OK")
                        } else {
                            reject(response.result.error!)
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    } else {
                        reject(response.result.error!)
                        strongSelf.handleApiResponseError(response, reject: reject)
                    }
                }
            }
        }
    }
    
    func avatarClicked() {
        changeProfileImage()
    }
    
    func changeProfileImage() {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let deleteAction = UIAlertAction(title: String.localize("LB_TAKE_PHOTO"), style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        let saveAction = UIAlertAction(title: String.localize("LB_PHOTO_LIBRARY"), style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        let cancelAction = UIAlertAction(title: String.localize("LB_CANCEL"), style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    // MARK: - Photo Methods
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            picker.cameraDevice = UIImagePickerControllerCameraDevice.Front
            self.presentViewController(picker, animated: true, completion: nil)
        } else {
            Alert.alert(self, title: "Camera not found", message: "Cannot access the front camera. Please use photo gallery instead.")
        }
    }
    
    func openGallery() {
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            self.presentViewController(picker, animated: true, completion: nil)
        } else {
            Alert.alert(self, title: "Tablet not suported", message: "Tablet is not supported in this function")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        profileImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        profileImage = profileImage.normalizedImage()
        let controller = CropImageViewController()
        controller.isSquare = true;
        controller.rightButtonTitle = String.localize("LB_DONE");
        controller.delegate = self;
        controller.imgEdit = profileImage.copy() as! UIImage;
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
    
    func uploadProfileImage() {
        if profileImage.size.width > 0 { // Make sure user did choose image
            Alamofire.upload(
                .POST,
                Constants.Path.Host + "/user/upload/profileimage", headers: Context.getTokenHeader(),
                multipartFormData: { multipartFormData in
                    multipartFormData.appendBodyPart(data: UIImageJPEGRepresentation(self.profileImage, 0.1)!, name: "file", fileName: "iosFile.jpg", mimeType: "image/jpg")
                },
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .Success(let upload, _, _):
                        upload.responseJSON { response in
                            Log.debug(String(data: response.data!, encoding: 4))
                            if let imageUploadResponse = Mapper<ImageUploadResponse>().map(response.result.value) {
                                self.user.profileImage = imageUploadResponse.profileImage
                                self.reloadAllData()
                            }
                        }
                    case .Failure(_): break
                    }
                }
            )
        }
    }
    
    // MARK: - CropImageViewControllerDelegate
    
    func didCropedImage(imageCroped: UIImage!) {
        profileImage = imageCroped
        self.uploadProfileImage()
    }
    
}
