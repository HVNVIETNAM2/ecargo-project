//
//  MyFollowersViewController.swift
//  merchant-ios
//
//  Created by Markus Chow on 11/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit

class MyFollowersViewController: FollowViewController {

	var myFollowersListViewController = MyFollowersListViewController()
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		activeViewController = myFollowersListViewController

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func createTopView() {
		super.createTopView()
		
		self.catCollectionView.removeFromSuperview()
		
		searchBar.frame = CGRectMake(0, 64, self.view.bounds.width, 40)
	}
	
	override func createContentView() {
		super.createContentView()
		
		let contentViewY: CGFloat = contentView.frame.origin.y - 40
		contentView.frame = CGRectMake(0, contentViewY, self.view.frame.width, self.view.frame.height - contentViewY)
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
