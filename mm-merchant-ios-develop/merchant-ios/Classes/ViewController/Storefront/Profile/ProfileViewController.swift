//
//  AccountSettingViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 19/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import CSStickyHeaderFlowLayout
import Alamofire

enum TypeProfile: Int {
    case Private = 0
    case Public
}

class ProfileViewController : MmViewController, GKFadeNavigationControllerDelegate, HeaderMyProfileDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropImageViewControllerDelegate {
    private final let WidthItemBar : CGFloat = 25
    private final let HeightItemBar : CGFloat = 25
    private final let DefaultUIEdgeInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    private final let HeaderProfileIdentifier = "HeaderMyProfileCell"
    private final let CellId = "Cell"
    private final let HeightCellMyProfile: CGFloat = 256.0
    private final let colorBarItemBackup: UIColor = UIColor()
    private final let NotiHasAvatar = "NotiHasAvatar"
    var user = User()
    var publicUser = User()
    var wishlist: Wishlist?
    var brands: [Brand] = []
    var curators: [User] = []
    var followedUsers: [User] = []
    var followingUsers: [User] = []
    let settingButton = UIButton(type: .Custom)
    var picker = UIImagePickerController()
    var profileImage = UIImage()
    var avatarImage = UIImageView()
    var IsTapAvatar: Bool = false
    var coverImage = UIImage()
    var coverImageView = UIImageView()
    var IsRefesh: Bool = false
    var currentType: TypeProfile = .Private
    var buttonBack = UIButton()
    var buttonSearch = UIButton()
    var header: HeaderMyProfileCell?
    var tabBarHeight: CGFloat = 0

    private var layout : CSStickyHeaderFlowLayout? {
        return self.collectionView?.collectionViewLayout as? CSStickyHeaderFlowLayout
    }
    
    var navigaionBarVisibility: GKFadeNavigationControllerNavigationBarVisibility? = GKFadeNavigationControllerNavigationBarVisibility.Hidden
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let tabBarController = self.tabBarController where !shouldHideTabBar() {
            tabBarHeight = tabBarController.tabBar.bounds.height
        }
        backupButtonColorOn()
        configCollectionView()
        setupNavigationProfile()
        setupPicker()
        addObserverGetAvatar()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigaionBarVisibility = GKFadeNavigationControllerNavigationBarVisibility.Hidden;
        self.navigationController?.updateViewConstraints()
        if currentType == TypeProfile.Private {
            updateUserView()
        } else {
            updateUserPublicProfile()
        }
        if header != nil {
            self.avatarImage = (header?.imageViewAvatar)!
            self.coverImageView = (header?.coverImageView)!
        }

    }
    
    // MARK: - setup UI
    func configCollectionView() {
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.frame = CGRect(x: 0 , y: 0, width: self.view.bounds.width, height: self.view.bounds.height - tabBarHeight)
        self.collectionView.backgroundColor = UIColor.blackColor()
        let flowLayout = CSStickyHeaderFlowLayout()
        flowLayout.disableStickyHeaders = true
        flowLayout.parallaxHeaderReferenceSize = CGSizeZero
        self.collectionView.setCollectionViewLayout(flowLayout, animated: false)
        self.collectionView.bounces = false
        // Setup Cell
        self.collectionView.registerClass(PostItemCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.layout?.itemSize = CGSizeMake(self.view.frame.size.width, 44)
        
        // Setup Header
        self.collectionView?.registerClass(HeaderMyProfileCell.self, forSupplementaryViewOfKind: CSStickyHeaderParallaxHeader, withReuseIdentifier: HeaderProfileIdentifier)
        self.layout?.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, 266.0)
        
        if let navigationController = self.navigationController as? GKFadeNavigationController {
            navigationController.setNeedsNavigationBarVisibilityUpdateAnimated(false)
          
        }
        
        
    }
    func setupNavigationProfile() {
        setupNavigationBarButtons()
        setupNavigationBar()
    }
    func setupNavigationBar() {
        let navigationBarAppearace = UINavigationBar.appearance()
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor(hexString: "#888888")]

    }
    func setupNavigationBarButtons() {
        setupNavigationBarCartButton()
        setupNavigationBarWishlistButton()
        setupBarButtons()
        var rightButtonItems = [UIBarButtonItem]()
        rightButtonItems.append(UIBarButtonItem(customView: buttonCart!))
        rightButtonItems.append(UIBarButtonItem(customView: buttonWishlist!))
        
        
        
        let leftButton = UIBarButtonItem(customView: settingButton)
        if currentType == .Private {
            settingButton.setImage(UIImage(named: "setting_btn_wht"), forState: .Normal)
            settingButton.frame = CGRectMake(0, 0, WidthItemBar, HeightItemBar)
            settingButton.tintColor = UIColor.whiteColor()
            settingButton.addTarget(self, action: "onSettingButton", forControlEvents: .TouchUpInside)
            self.navigationItem.leftBarButtonItem = leftButton
        } else {
            //public profile
            let backBarItem = self.initCreateBack("back_wht", selectorName: "onBackButton", size: CGSize(width: 30,height: HeightItemBar), left: -15, right: 0)
            let searchBarItem = self.initSearchButton("search_wht", selectorName: "searchIconClicked", size: CGSize(width: WidthItemBar,height: 24), left: -21, right: 0)
            var leftButtonItems = [UIBarButtonItem]()
            leftButtonItems.append(backBarItem)
            leftButtonItems.append(searchBarItem)
            self.navigationItem.leftBarButtonItems = leftButtonItems
        }
        
        buttonCart!.addTarget(self, action: "navigateToshoppingCart", forControlEvents: .TouchUpInside)
        buttonWishlist!.addTarget(self, action: "likeNaviBarTapped:", forControlEvents: .TouchUpInside)
        
        self.navigationItem.rightBarButtonItems = rightButtonItems
        
    }
    
    func initCreateBack(imageName: String, selectorName: String, size:CGSize,left: CGFloat, right: CGFloat) -> UIBarButtonItem {

        buttonBack.setImage(UIImage(named: imageName), forState: .Normal)
        buttonBack.frame = CGRectMake(0, 0, size.width, size.height)
        buttonBack.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: left, bottom: 0, right: right)
        buttonBack .addTarget(self, action:Selector(selectorName), forControlEvents: UIControlEvents.TouchUpInside)
        
        let temp:UIBarButtonItem = UIBarButtonItem()
        temp.customView = buttonBack
        return temp
    }
    
    func initSearchButton(imageName: String, selectorName: String, size:CGSize,left: CGFloat, right: CGFloat) -> UIBarButtonItem {
        
        buttonSearch.setImage(UIImage(named: imageName), forState: .Normal)
        buttonSearch.frame = CGRectMake(0, 0, size.width, size.height)
        buttonSearch.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: left, bottom: 0, right: right)
        buttonSearch .addTarget(self, action:Selector(selectorName), forControlEvents: UIControlEvents.TouchUpInside)
        
        let temp:UIBarButtonItem = UIBarButtonItem()
        temp.customView = buttonSearch
        return temp
    }
    
    func backupButtonColorOn() {
        buttonCart?.setImage(UIImage(named: "cart_grey"), forState: .Normal)
        buttonWishlist?.setImage(UIImage(named: "icon_heart_stroke"), forState: .Normal)
        settingButton.setImage(UIImage(named: "setting_btn"), forState: .Normal)
        buttonBack.setImage(UIImage(named: "back_grey"), forState: .Normal)
        buttonSearch.setImage(UIImage(named: "search_grey"), forState: .Normal)
    }
    func setupBarButtons() {
        settingButton.setImage(UIImage(named: "setting_btn_wht"), forState: .Normal)
        if buttonCart != nil {
            buttonCart?.setImage(UIImage(named:"shop"), forState: .Normal)
        }
        if buttonWishlist != nil {
            buttonWishlist?.setImage(UIImage(named: "cart"), forState: .Normal)
        }
        buttonBack.setImage(UIImage(named: "back_wht"), forState: .Normal)
        buttonSearch.setImage(UIImage(named: "search_wht"), forState: .Normal)
        
    }
    func setupPicker() {
        picker.delegate = self
    }
    func addObserverGetAvatar() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "getImageAvatar:", name:NotiHasAvatar, object: nil)
    }
    func getImageAvatar(notification: NSNotification) {
        let imageViews = notification.object as! NSArray
        self.avatarImage = imageViews[0] as! UIImageView
        self.coverImageView = imageViews[1] as! UIImageView
    }
    func originY() -> CGFloat
    {
        var originY:CGFloat = 0;
        let application: UIApplication = UIApplication.sharedApplication()
        if (application.statusBarHidden)
        {
            originY = application.statusBarFrame.size.height
        }
        return originY;
    }
    //MARK: - action button bar
    
    // Public profile actioin 
    func onBackButton() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func searchIconClicked() {
        let searchStyleController = SearchStyleController()
//        searchStyleController.styles = self.styles
//        searchStyleController.filterStyleDelegate = self
//        searchStyleController.styleFilter = self.styleFilter
//        searchStyleController.popTwice = popTwice
        self.navigationController?.pushViewController(searchStyleController, animated: false)
    }
    func onSettingButton() {
        self.navigationController?.pushViewController(AccountSettingViewController(), animated: true)
    }
    func navigateToshoppingCart() {
        let shoppingcart = ShoppingCartViewController()
        self.navigationController?.pushViewController(shoppingcart, animated: true)
    }
    func likeNaviBarTapped(sender: UIBarButtonItem){
        let wishlist = WishListCartViewController()
        self.navigationController?.pushViewController(wishlist, animated: true)
    }
    
    //MARK: - Delegate & Datasource
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! PostItemCollectionViewCell
        cell.text = "label + \(indexPath.row)"
        return cell
        
    }
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return DefaultUIEdgeInsets
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSizeMake(self.view.frame.size.width, 100.0)
    }
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        if kind == CSStickyHeaderParallaxHeader {
            let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: HeaderProfileIdentifier, forIndexPath: indexPath)
            header = (view as! HeaderMyProfileCell)
//            self.headerGlobal = header
            header!.delegateMyProfile = self
            if currentType == TypeProfile.Private {
                header!.IsPriviteProfile = true
                header!.currentTypeProfile = .Private
            } else {
                header!.IsPriviteProfile = false
                header!.currentTypeProfile = .Public
            }
            
            if  IsRefesh == false  {
                if wishlist?.cartItems?.count > 0 {
                    header!.wishlistCount = (wishlist?.cartItems?.count)!
                }
                header!.setupDataWithUser(user)
                
                IsRefesh = true
            }
            return header!
        }
        return UICollectionReusableView()
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSizeMake(self.view.frame.width, 0.0)
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSizeMake(self.view.frame.width, 0.0)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let scrollOffsetY = 80.0 - scrollView.contentOffset.y
        if (scrollOffsetY < 44.0) {
            self.navigaionBarVisibility = GKFadeNavigationControllerNavigationBarVisibility.Visible
            if let navigationController = self.navigationController as? GKFadeNavigationController {
                navigationController.setNavigationBarVisibility(GKFadeNavigationControllerNavigationBarVisibility.Visible, animated: true)
                navigationController.navigationBar.topItem?.title = user.displayName
                backupButtonColorOn()
            }
        } else {
            self.navigaionBarVisibility = GKFadeNavigationControllerNavigationBarVisibility.Hidden
            
            if let navigationController = self.navigationController as? GKFadeNavigationController {
                navigationController.setNavigationBarVisibility(GKFadeNavigationControllerNavigationBarVisibility.Hidden, animated: true)
                setupBarButtons()
            }
        }
    }
    
    func preferredNavigationBarVisibility() -> GKFadeNavigationControllerNavigationBarVisibility {
        return self.navigaionBarVisibility!
    }
    
    //MARK: - handle data
    func updateUserView(){
        self.showLoading()
        firstly{
            
            // update inventory location if needed
            // if it is not updated, it will return success without api call
            return self.fetchUser()
            }.then { _ -> Void in
                self.renderUserView()
            }.always {
                self.renderUserView()
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    
    func updateUserPublicProfile(){
        self.showLoading()
        firstly{
            
            // update inventory location if needed
            // if it is not updated, it will return success without api call
            return self.fetchPublicUser()
            }.then{ _ -> Void in
                self.renderUserView()        
            }.always {
                self.renderUserView()
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    func renderUserView() {
        self.collectionView.reloadData()
    }
    
    func fetchPublicUser() -> Promise<AnyObject>{
        return Promise{ fulfill, reject in
            UserService.viewWithUserKey(publicUser.userKey){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            strongSelf.user = Mapper<User>().map(response.result.value)!
                            //dummy data 
                            strongSelf.user.isFriendUser = true
                            fulfill("OK")
                            self!.IsRefesh = false
                        } else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    } else {
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    func fetchUser() -> Promise<AnyObject>{
        return Promise{ fulfill, reject in
            UserService.view(Context.getUserId()){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            strongSelf.user = Mapper<User>().map(response.result.value)!
                            fulfill("OK")
                            self!.IsRefesh = false
                        } else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    } else {
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    func fetcheWishList() -> Promise<AnyObject>{
        return Promise{ fulfill, reject in
            WishlistService.listByUser {[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            strongSelf.wishlist = Mapper<Wishlist>().map(response.result.value)
                            fulfill("OK")
                            self!.IsRefesh = false
                        } else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    } else {
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    // friend api
    func addFriend(user:User) {
        self.showLoading()
        firstly{
            return self.addFriendRequest(user)
            }.then
            { _ -> Void in
                user.isFriendUser = true
                self.renderUserView()
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    func addFriendRequest(user:User) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FriendService.addFriendRequest(user, completion:
                {
                    [weak self] (response) in
                    if let strongSelf = self {
                        Log.debug(String(data: response.data!, encoding: 4))
                        if response.result.isSuccess {
                            if response.response?.statusCode == 200 {
                                fulfill("OK")
                            } else {
                                strongSelf.handleApiResponseError(response, reject: reject)
                            }
                        }
                        else{
                            reject(response.result.error!)
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                })
        }
    }
    func deleteRequest(user:User) {
        self.showLoading()
        firstly{
            return self.deleteFriendRequest(user)
            }.then
            { _ -> Void in
                user.isFriendUser = false
                self.renderUserView()
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    func deleteFriendRequest(user:User) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FriendService.deleteRequest(user, completion:
                {
                    [weak self] (response) in
                    if let strongSelf = self {
                        Log.debug(String(data: response.data!, encoding: 4))
                        if response.result.isSuccess {
                            if response.response?.statusCode == 200 {
                                fulfill("OK")
                            } else {
                                strongSelf.handleApiResponseError(response, reject: reject)
                            }
                        }
                        else{
                            reject(response.result.error!)
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                })
        }
    }
    
    // follow api 
    func followUser(user:User) {
        self.showLoading()
        firstly{
            return self.requestFollow(user)
            }.then
            { _ -> Void in
                user.isFollowUser = true
                self.showSuccessPopupWithText(String.localize("MSG_SUC_FOLLOWED"))
                self.renderUserView()
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    func requestFollow(user:User) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FollowService.followUser(user, completion:
                {
                    [weak self] (response) in
                    if let strongSelf = self {
                        Log.debug(String(data: response.data!, encoding: 4))
                        if response.result.isSuccess {
                            if response.response?.statusCode == 200 {
                                fulfill("OK")
                            } else {
                                strongSelf.handleApiResponseError(response, reject: reject)
                            }
                        }
                        else{
                            reject(response.result.error!)
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                })
        }
    }
    func unfollowUser(user:User) {
        self.showLoading()
        firstly{
            return self.requestUnfollow(user)
            }.then
            { _ -> Void in
                user.isFollowUser = false
                self.showSuccessPopupWithText(String.localize("MSG_SUC_UNFOLLOWED"))
                self.renderUserView()
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    func requestUnfollow(user:User) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FollowService.unfollowUser(user, completion:
                {
                    [weak self] (response) in
                    if let strongSelf = self {
                        Log.debug(String(data: response.data!, encoding: 4))
                        if response.result.isSuccess {
                            if response.response?.statusCode == 200 {
                                fulfill("OK")
                            } else {
                                strongSelf.handleApiResponseError(response, reject: reject)
                            }
                        }
                        else{
                            reject(response.result.error!)
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                })
        }
    }
    //MARK: - Delegate Header Profile
    
    func onTapWishlistButton() {
        likeNaviBarTapped(UIBarButtonItem())
    }
    
    func onTapAvatarView(sender: UITapGestureRecognizer) {
        self.IsTapAvatar = true
        changeProfileImage(sender, isTapAvatar: self.IsTapAvatar)
        
    }
    func onTapEditCoverView(sender: UITapGestureRecognizer) {
        self.IsTapAvatar = false
        changeProfileImage(sender, isTapAvatar: self.IsTapAvatar)
    }
    func onTapMerchantListView(sender: UIButton) {
        let followVC = FollowViewController()
        followVC.user = self.user
        followVC.currentTypeProfile = .Private
        self.navigationController?.pushViewController(followVC, animated: true)
    }
//    func onTapMerchanListView(sender: UIButton) {
//        
//        let followVC = FollowViewController()
//        followVC.user = self.user
//        followVC.currentTypeProfile = .Public
//        self.navigationController?.pushViewController(followVC, animated: true)
//    }

    func onTapFriendList() {
        self.navigationController?.pushViewController(FriendListViewController(), animated: true)
    }

    func onHandleAddFriend(friendStatus: Bool) {
        if friendStatus {
			
			let str = String.localize("LB_CA_REMOVE_FRD_CONF")
			let message = str.stringByReplacingOccurrencesOfString("{0}", withString: " \(self.user.displayName) ")
			
            Alert.alert(self, title: "", message: message, okActionComplete: { () -> Void in
//                statusFriend = !statusFriend
//                configButtonAddFriend(statusFriend)
                self.header!.statusFriend = !friendStatus
                self.header!.configButtonAddFriend(!friendStatus)
                self.deleteRequest(self.user)
                }, cancelActionComplete:nil)
        } else {
            header!.statusFriend = !friendStatus
            header!.configButtonAddFriend(!friendStatus)
            addFriend(user)
        }
    }
    
    func onHandleAddFollow(followStatus: Bool) {
        if !followStatus {
            followUser(user)
        } else {
            unfollowUser(user)
        }
    }

	func onTapMyFollowersListView(sender: UIButton) {
		self.navigationController?.pushViewController(MyFollowersViewController(), animated: true)
	}
    //MARK: - Handle When edit avatar
    
    func changeProfileImage(sender: UITapGestureRecognizer, isTapAvatar: Bool){
        var keyViewProfile: String?
        var keyDeleteProfile: String?
        var keyTakeProfile: String?
        var keyChooseProfile: String?
        
        if isTapAvatar {
            keyViewProfile = String.localize("LB_CA_VIEW_PROF_PIC")
        } else {
            keyViewProfile = String.localize("LB_CA_VIEW_COVER_PIC")
        }
        keyDeleteProfile = String.localize("LB_CA_DEL_CURR_PROF_PIC")
        keyTakeProfile = String.localize("LB_CA_PROF_PIC_TAKE_PHOTO")
        keyChooseProfile = String.localize("LB_CA_PROF_PIC_CHOOSE_LIBRARY")
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        if (self.user.profileImage != "" && self.IsTapAvatar == true) ||  (self.user.coverImage != "" && self.IsTapAvatar == false)  {
                
            createButtonDeleteAndViewFullScreen(optionMenu, keyView: keyViewProfile!, keyDelete: keyDeleteProfile!)
        }
        
        let takePhoto = UIAlertAction(title: keyTakeProfile , style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        let saveAction = UIAlertAction(title: keyChooseProfile, style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        let cancelAction = UIAlertAction(title: String.localize("LB_CANCEL"), style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(takePhoto)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    // create button delete and view Full screen
    func createButtonDeleteAndViewFullScreen (optionMenu: UIAlertController, keyView: String, keyDelete: String) {
        let viewFullScreen = UIAlertAction(title: keyView, style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.viewFullScreen()
        })
        let deleteAction = UIAlertAction(title: keyDelete, style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.deleteImages()
        })
        optionMenu.addAction(viewFullScreen)
        optionMenu.addAction(deleteAction)
    }
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            if self.IsTapAvatar == true {
                picker.cameraDevice = UIImagePickerControllerCameraDevice.Front
            } else {
                picker.cameraDevice = UIImagePickerControllerCameraDevice.Rear
            }
            
            self.presentViewController(picker, animated: true, completion: nil)
        }else {
            Alert.alert(self, title: "Camera not found", message: "Cannot access the front camera. Please use photo gallery instead.")
        }
    }
    
    func openGallery(){
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            self.presentViewController(picker, animated: true, completion: nil)
        } else {
            Alert.alert(self, title: "Tablet not suported", message: "Tablet is not supported in this function")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        let controller = CropImageViewController()
        controller.isSquare = true;
        controller.rightButtonTitle = String.localize("LB_DONE");
        controller.delegate = self;
        if self.IsTapAvatar == true {
            profileImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            profileImage = profileImage.normalizedImage()
            controller.imgEdit = profileImage.copy() as! UIImage;
            
        } else {
            coverImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            coverImage = coverImage.normalizedImage()
            controller.imgEdit = coverImage.copy() as! UIImage;
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true) { () -> Void in
        }
    }
    
    func uploadProfileImage(){
        if profileImage.size.width > 0 { //Make sure user did choose image
            Alamofire.upload(
                .POST,
                Constants.Path.Host + "/user/upload/profileimage", headers: Context.getTokenHeader(),
                multipartFormData: { multipartFormData in
                    multipartFormData.appendBodyPart(data: UIImageJPEGRepresentation(self.profileImage, 0.1)!, name: "file", fileName: "iosFile.jpg", mimeType: "image/jpg")
                },
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .Success(let upload, _, _):
                        upload.responseJSON { response in
                            Log.debug(String(data: response.data!, encoding: 4))
                            if let imageUploadResponse = Mapper<ImageUploadResponse>().map(response.result.value) {
                                self.user.profileImage = imageUploadResponse.profileImage
                                self.IsRefesh = false
                                self.renderUserView()
                            }
                        }
                    case .Failure(_): break
                    }
                }
            )
        }
    }
    func uploadCoverImage() {

        if coverImage.size.width > 0 { //Make sure user did choose image
            Alamofire.upload(
                .POST,
                Constants.Path.Host + "/user/upload/coverimage", headers: Context.getTokenHeader(),
                multipartFormData: { multipartFormData in
                    multipartFormData.appendBodyPart(data: UIImageJPEGRepresentation(self.coverImage, 0.1)!, name: "file", fileName: "iosFile.jpg", mimeType: "image/jpg")
                },
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .Success(let upload, _, _):
                        upload.responseJSON { response in
                            Log.debug(String(data: response.data!, encoding: 4))
                            if let imageUploadResponse = Mapper<ImageUploadResponse>().map(response.result.value) {
                                self.user.coverImage = imageUploadResponse.coverImage
                                self.IsRefesh = false
                                self.renderUserView()
                            }
                        }
                    case .Failure(_): break
                    }
                }
            )
        }
    }
    func viewFullScreen() {
        if (self.IsTapAvatar == true) {
            for recognizer in self.avatarImage.gestureRecognizers ?? [] {
                if (recognizer.isKindOfClass(MHFacebookImageViewerTapGestureRecognizer)) {
                    self.avatarImage.didTapProfile(recognizer as! MHFacebookImageViewerTapGestureRecognizer)
                }
            }
        } else {
            for recognizer in self.coverImageView.gestureRecognizers ?? [] {
                if (recognizer.isKindOfClass(MHFacebookImageViewerTapGestureRecognizer)) {
                    self.coverImageView.didTapProfile(recognizer as! MHFacebookImageViewerTapGestureRecognizer)
                }
            }
        }
        
    }
    func deleteImages() {
        if self.IsTapAvatar == true {
            self.deleteAvatar()
        } else {
            self.deletCover()
        }
    }
    func deleteAvatar() {
        Alamofire.upload(
            .POST,
            Constants.Path.Host + "/user/upload/profileimage", headers: Context.getTokenHeader(),
            multipartFormData: { multipartFormData in

            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { response in
                        Log.debug(String(data: response.data!, encoding: 4))
                        if let imageUploadResponse = Mapper<ImageUploadResponse>().map(response.result.value) {
                            self.user.profileImage = imageUploadResponse.profileImage
                            self.IsRefesh = false
                            self.renderUserView()
                        }
                    }
                case .Failure(_): break
                }
            }
        )
    }
    
    func deletCover() {
        Alamofire.upload(
            .POST,
            Constants.Path.Host + "/user/upload/coverimage", headers: Context.getTokenHeader(),
            multipartFormData: { multipartFormData in
                
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { response in
                        Log.debug(String(data: response.data!, encoding: 4))
                        if let imageUploadResponse = Mapper<ImageUploadResponse>().map(response.result.value) {
                            self.user.coverImage = imageUploadResponse.coverImage
                            self.IsRefesh = false
                            self.renderUserView()
                        }
                    }
                case .Failure(_): break
                }
            }
        )
    }
    //MARK: CropImageViewControllerDelegate
    func didCropedImage(imageCroped: UIImage!) {
        if self.IsTapAvatar == true {
            profileImage = imageCroped
            self.uploadProfileImage()
        } else {
            coverImage = imageCroped
            self.uploadCoverImage()
        }
        
    }
    
    
}