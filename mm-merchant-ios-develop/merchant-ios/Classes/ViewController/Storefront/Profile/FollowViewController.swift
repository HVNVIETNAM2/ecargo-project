
//
//  MerchantListViewController.swift
//  merchant-ios
//
//  Created by Trung Vu on 3/7/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit

enum ModeList: Int {
    case MerchantList = 0
    case CuratorList
    case UsersList
}

@objc
protocol FollowViewControllerDelegate: NSObjectProtocol {
    optional func didTextChange(text: String)
    optional func didSelectCancelButton()
    optional func didSelectSearchButton(text: String)
}

class FollowViewController: MmViewController, UISearchBarDelegate {
    
    private final let SubCatCellId = "SubCatCell"
    private final let CellId = "Cell"
    private final let CatCellHeight : CGFloat = 40
    private final let orginYContentView: CGFloat = 144
    private final let orginYSearhBar: CGFloat = 104
    var selectedIndex: Int = 0
    var searchActive : Bool = false
    
    var catCollectionView : UICollectionView!
    var contentView: UIView = UIView()
    var searchBar: UISearchBar = UISearchBar()
    var merchantListViewController = MerchantListViewController()
    var curatorListViewController = CuratorListViewController()
    var followingUserListViewController = FollowingUserListViewController()
    var user: User?
    var currentTypeProfile: TypeProfile = TypeProfile.Private
    var delegate_: FollowViewControllerDelegate?
    
    
    var activeViewController: UIViewController? {
        didSet {
            removeInactiveViewController(oldValue)
            updateActiveViewController(self.contentView, activeViewController: activeViewController)
        }
    }
	
	// config tab bar
	override func shouldHideTabBar() -> Bool {
		return  true
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.createBackButton()
        createTopView()
        createContentView()
        
        if user != nil {
            merchantListViewController.user = user!
        }
        merchantListViewController.currentTypeProfile = currentTypeProfile
        self.delegate_ = merchantListViewController.self
        activeViewController = merchantListViewController
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.title = String.localize("LB_CA_FOLLOWING_LIST")

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createTopView() {
        let layout: SnapFlowLayout = SnapFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        self.catCollectionView = UICollectionView(frame: CGRect(x: 0, y: 64, width: self.view.bounds.width, height: CatCellHeight), collectionViewLayout: layout)
        self.catCollectionView.delegate = self
        self.catCollectionView.dataSource = self
        self.catCollectionView.backgroundColor = UIColor.whiteColor()
        self.catCollectionView.registerClass(SubCatCell.self, forCellWithReuseIdentifier: SubCatCellId)
        self.catCollectionView.scrollEnabled = false
        self.view.addSubview(self.catCollectionView)
        
        searchBar.frame = CGRectMake(0, orginYSearhBar, self.view.bounds.width, 40)
        searchBar.placeholder = String.localize("LB_CA_SEARCH")
        self.view.addSubview(self.searchBar)
        searchBar.delegate = self

        var textField : UITextField
        textField = searchBar.valueForKey("_searchField") as! UITextField
        textField.layer.cornerRadius = 15
        textField.layer.masksToBounds = true
        
        
    }
    func createContentView() {
        contentView.frame = CGRectMake(0, orginYContentView, self.view.frame.width, self.view.frame.height - orginYContentView - 60)
        contentView.backgroundColor = UIColor.grayColor()
        self.view.addSubview(contentView)
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    //MARK: - delegate & datasource
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
            
        case self.catCollectionView:
            return 3
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
            
        case self.catCollectionView:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(SubCatCellId, forIndexPath: indexPath) as! SubCatCell
            switch indexPath.row {
            case 0:
                cell.label.text = String.localize("LB_CA_BRAND")
                cell.label.textColor = UIColor.secondary2()
                cell.label.font = UIFont.boldSystemFontOfSize(14.0)
                cell.imageView.hidden = true
                
                break
            case 1:
                cell.label.text = String.localize("LB_CA_CURATOR")
                cell.label.textColor = UIColor.secondary2()
                cell.label.font = UIFont.boldSystemFontOfSize(14.0)
                cell.imageView.hidden = true
                break
            case 2:
                cell.label.text = String.localize("LB_CA_USER")
                cell.label.textColor = UIColor.secondary2()
                cell.label.font = UIFont.boldSystemFontOfSize(14.0)
                cell.imageView.hidden = true
                break
                
            default:
                break
            }
            if indexPath.row == selectedIndex {
                selectedCell(cell)
            }
            return cell
        default:
            return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
            
        }
    }
    
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            switch collectionView {
                
            case self.catCollectionView:
                
                
                switch indexPath.row {
                case 0:
                    showMerchantList()
                    break
                case 1:
                    showCuratorList()
                    break
                case 2:
                    showFollowingUser()
                    break
                    
                default:
                    break
                }
                
            default:
                break
            }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch collectionView {
        case self.catCollectionView:
            return Constants.LineSpacing.SubCatCell
            
        default:
            return 0.0
            
        }
        
    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            switch collectionView {
                
            case self.catCollectionView:
                let space = ((self.view.frame.width / 6) * 3 - Constants.LineSpacing.SubCatCell) / 2
                return  UIEdgeInsets(top: 0.0, left: space, bottom: 0.0, right: space)
                
            default:
                return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                
            }
    }
    
    
    
    //MARK: Item Size Delegate for Collection View
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
                
            case self.catCollectionView:
                switch (indexPath.row){
                default:
                    return CGSizeMake(self.view.frame.size.width / 6, Constants.Value.CatCellHeight)
                }
                
            default:
                return CGSizeMake(self.view.frame.size.width, 40)
                
            }
    }
    
    func showMerchantList() {
        selectedIndex = 0
        setSelectedViewControler(selectedIndex)
        merchantListViewController.user = user!
        merchantListViewController.currentTypeProfile = currentTypeProfile
        activeViewController = merchantListViewController
        self.view.endEditing(true)
    }
    func showCuratorList() {
        selectedIndex = 1
        setSelectedViewControler(selectedIndex)
        activeViewController = curatorListViewController
        self.view.endEditing(true)
    }
    func showFollowingUser() {
        selectedIndex = 2
        setSelectedViewControler(selectedIndex)
        activeViewController = followingUserListViewController
        self.view.endEditing(true)
    }
    func setSelectedViewControler(index: Int) {
        setDefaultCell()
        if selectedIndex == index {
            let cells = self.catCollectionView.visibleCells() as! [SubCatCell]
            let cell = cells[index]
            cell.imageView.image = UIImage(named: "underLineBrand")
            cell.label.font = UIFont.boldSystemFontOfSize(14.0)
            cell.imageView.hidden = false
        }
        self.catCollectionView.reloadData()
    }
    func setDefaultCell() {
        let cells = self.catCollectionView.visibleCells() as! [SubCatCell]
        for (var i = 0; i < self.catCollectionView.visibleCells().count; i++) {
            
            let cell = cells[i] as SubCatCell
            cell.label.textColor = UIColor.secondary2()
            cell.label.font = UIFont.boldSystemFontOfSize(14.0)
            cell.imageView.hidden = true
        }
    }
    func selectedCell(cell: SubCatCell) {
        cell.imageView.image = UIImage(named: "underLineBrand")
        cell.label.font = UIFont.boldSystemFontOfSize(14.0)
        cell.imageView.hidden = false
    }
    
    //MARK: - searchbar delegate
    
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
        self.view.endEditing(true)
        self.delegate_?.didSelectCancelButton!()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
        self.delegate_?.didSelectSearchButton!(searchBar.text!)
        searchBar.resignFirstResponder()
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        switch(selectedIndex) {
        case ModeList.MerchantList.rawValue:
            self.delegate_?.didTextChange!(searchText)
            break
        case ModeList.CuratorList.rawValue:
            break
        case ModeList.UsersList.rawValue:
            break
        default:
            break
        }
        
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        
        searchBar.showsCancelButton = true
        styleCancelButton(true)
        return true
    }
    
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = false
        return true
    }
    
    func styleCancelButton(enable: Bool){
        if enable {
            if let _cancelButton = searchBar.valueForKey("_cancelButton"),
                let cancelButton = _cancelButton as? UIButton {
                    cancelButton.enabled = enable //comment out if you want this button disabled when keyboard is not visible
                    if title != nil {
                        cancelButton.setTitle(String.localize("LB_CANCEL"), forState: UIControlState.Normal)
                    }
            }
        }
    }
    func removeInactiveViewController(inactiveViewController: UIViewController?) {
        if let inActiveVC = inactiveViewController {
            // call before removing child view controller's view from hierarchy
            inActiveVC.willMoveToParentViewController(nil)
            
            inActiveVC.view.removeFromSuperview()
            
            // call after removing child view controller's view from hierarchy
            inActiveVC.removeFromParentViewController()
        }
    }
    func updateActiveViewController(viewContent: UIView, activeViewController: UIViewController!) {
        if let activeVC = activeViewController {
            // call before adding child view controller's view as subview
            addChildViewController(activeVC)
            
            activeVC.view.frame = viewContent.bounds
            viewContent.addSubview(activeVC.view)
            
            // call before adding child view controller's view as subview
            activeVC.didMoveToParentViewController(self)
        }
    }
}

