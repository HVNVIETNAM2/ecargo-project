//
//  FollowingUserListViewController.swift
//  merchant-ios
//
//  Created by Trung Vu on 3/7/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//


import UIKit

class FollowingUserListViewController: MmViewController {
    
    private final let heightTopView: CGFloat = 144
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func shouldHideTabBar() -> Bool {
        return true
    }
    
}
