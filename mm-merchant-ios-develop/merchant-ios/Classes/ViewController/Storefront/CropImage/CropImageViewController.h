

#import <UIKit/UIKit.h>
#import "BJImageCropper.h"

@protocol CropImageViewControllerDelegate;
@interface CropImageViewController : UIViewController
{
    BJImageCropper *imageCropper;
    void (^block_getImage)(UIImage*);
}
@property (strong,nonatomic) NSString* rightButtonTitle;
@property (assign,nonatomic) BOOL isSquare;
@property (nonatomic, strong) BJImageCropper *imageCropper;
@property (nonatomic, strong) UIImage * imgEdit;
- (void) setBlockGetImage:(void(^)(UIImage *))f_getImage;
@property (assign,nonatomic)id<CropImageViewControllerDelegate>delegate;
@end

@protocol CropImageViewControllerDelegate <NSObject>

-(void)didCropedImage:(UIImage*)imageCroped;

@end