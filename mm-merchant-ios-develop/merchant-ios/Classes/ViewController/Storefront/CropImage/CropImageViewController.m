
#import "CropImageViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface CropImageViewController ()

@end
@implementation CropImageViewController
@synthesize imageCropper;
@synthesize imgEdit;

- (void)viewDidLoad {
    self.view.backgroundColor = [self colorFromHexString:@"#f1f1f1"];
    [super viewDidLoad];
}

- (void)viewDidUnload {
    [self setImageCropper:nil];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated {
    [self createNavigationbar];
    if (!self.imageCropper) {
        CGRect mainFrame = [UIScreen mainScreen].bounds;
        self.imageCropper = [[BJImageCropper alloc] initWithImage:self.imgEdit andMaxSize:CGSizeMake(mainFrame.size.width, mainFrame.size.height - 50) isSquare:self.isSquare];
        [self.view addSubview:self.imageCropper];
        [self.imageCropper setCenter:CGPointMake(mainFrame.size.width / 2, (mainFrame.size.height - 50)/2)];
        self.imageCropper.imageView.layer.shadowColor = [[UIColor blackColor] CGColor];
        self.imageCropper.imageView.layer.shadowRadius = 3.0f;
        self.imageCropper.imageView.layer.shadowOpacity = 0.8f;
        self.imageCropper.imageView.layer.shadowOffset = CGSizeMake(1, 1);
    }
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden   =   NO;
}


-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

#pragma mark - Create navigation bar
- (void) createNavigationbar {
    /* set title for navigation bar */
    self.navigationItem.title = @"";
    [self createBackButton];
    [self createRightButton];
    self.navigationController.navigationBarHidden   =   NO;
}

#pragma mark - action
- (void) backClicked {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void) doneClicked {
    [self.navigationController popViewControllerAnimated:NO];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCropedImage:)]) {
        [self.delegate didCropedImage:[self.imageCropper getCroppedImage:self.isSquare]];
    } else {
        if(block_getImage) {
            block_getImage([self.imageCropper getCroppedImage:self.isSquare]);
        }
    }
}

- (void) setBlockGetImage:(void(^)(UIImage *))f_getImage {
    if (block_getImage) {
        block_getImage = nil;
    }
    block_getImage = [f_getImage copy];
}

-(void) createBackButton {
    UIButton* buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    buttonBack.frame = CGRectMake(0, 0, 30, 25);
    buttonBack.contentEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0) ;
    [buttonBack addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:buttonBack];
    self.navigationItem.leftBarButtonItem = backButtonItem;
}

-(void) createRightButton {
    UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [rightButton setTitle:self.rightButtonTitle forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [rightButton setTitleColor:[self colorFromHexString:@"#ed2247"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 40, 35);
    [rightButton addTarget:self action:@selector(doneClicked) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    if([hexString length] > 0){
        unsigned rgbValue = 0;
        NSScanner *scanner = [NSScanner scannerWithString:hexString];
        [scanner setScanLocation:1];
        [scanner scanHexInt:&rgbValue];
        return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    } else {
        return [UIColor whiteColor];
    }
}
@end
