//
//  FilterCatCell.swift
//  merchant-ios
//
//  Created by HVN_Pivotal on 2/16/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import UIKit
class FilterCatCell: UICollectionViewCell {

    private final let TextMarginLeft: CGFloat = 14
    private final let BackgroundMarginLeft: CGFloat = 2
    private final let MarginTop: CGFloat = 0.0
    var nameLabel = UILabel()
    var viewBackground = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        viewBackground.backgroundColor = UIColor.clearColor()
        viewBackground.layer.borderColor = UIColor.secondary1().CGColor
        viewBackground.layer.borderWidth = 1.0
        viewBackground.layer.cornerRadius = 3.0
        addSubview(viewBackground)
        
        nameLabel.formatSize(13)
        nameLabel.textColor = UIColor.secondary2()
        nameLabel.textAlignment = .Center
        
        nameLabel.numberOfLines = 1
        nameLabel.lineBreakMode = .ByTruncatingTail
        addSubview(nameLabel)
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        nameLabel.frame = CGRect(x: TextMarginLeft , y: 0, width: self.bounds.maxX - TextMarginLeft * 2, height: self.bounds.maxY)
        viewBackground.frame = CGRect(x: BackgroundMarginLeft, y: MarginTop, width: self.bounds.maxX - BackgroundMarginLeft * 2, height: self.bounds.maxY - MarginTop)
    }
    func selected(isSelected: Bool) {
        if isSelected {
            viewBackground.backgroundColor = UIColor.backgroundGray()
        }
        else {
            viewBackground.backgroundColor = UIColor.clearColor()
        }
    }
    
}