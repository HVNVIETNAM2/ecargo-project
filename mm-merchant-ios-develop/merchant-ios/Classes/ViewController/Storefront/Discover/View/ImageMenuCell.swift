//
//  ImageMenuCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 8/1/2016.
//  Copyright © 2016 Koon Kit Chan. All rights reserved.
//

import Foundation
import AlamofireImage

class ImageMenuCell : UICollectionViewCell{
    var imageView = UIImageView()
    var upperLabel = UILabel()
    var lowerLabel = UILabel()
    var borderView = UIView()
    var tickImageView = UIImageView()
    
    private final let MarginCenter : CGFloat = 21
    private final let LogoMarginRight : CGFloat = 10
    private final let LogoMarginLeft : CGFloat = 10
    private final let LabelMarginTop : CGFloat = 15
    private final let LabelMarginRight : CGFloat = 30
    private final let LogoWidth : CGFloat = 44
    private final let LabelLowerMarginTop : CGFloat = 33
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        addSubview(imageView)
        upperLabel.formatSize(15)
        addSubview(upperLabel)
        lowerLabel.formatSize(12)
        addSubview(lowerLabel)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        addSubview(tickImageView)
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: bounds.minX + LogoMarginLeft, y: bounds.midY - LogoWidth / 2, width: LogoWidth, height: LogoWidth)
        upperLabel.frame = CGRect(x: imageView.frame.width + LogoMarginLeft + LogoMarginRight, y: bounds.minY + LabelMarginTop, width: bounds.width - LabelMarginRight , height: bounds.height/3 )
        lowerLabel.frame = CGRect(x: imageView.frame.width + LogoMarginLeft +  LogoMarginRight, y: LabelLowerMarginTop, width: bounds.width - LabelMarginRight , height:bounds.height/3 )
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        tickImageView.frame = CGRect(x: bounds.maxX - 40, y: bounds.midY - 5, width: 16, height: 10)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImage(key : String, imageCategory : ImageCategory ){
        layoutSubviews()
        let filter = AspectScaledToFitSizeFilter(
            size: imageView.frame.size
        )
        imageView.af_setImageWithURL(ImageURLFactory.get(key, category: imageCategory), placeholderImage : UIImage(named: "holder"), filter: filter)
    }
}