//
//  SubCatCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 27/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class SubCatCell : UICollectionViewCell {
    var label = UILabel()
    var imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        imageView.frame = CGRect(x: 0, y: bounds.height - 7, width: bounds.width, height: 1)
        imageView.clipsToBounds = true
        addSubview(imageView)
        label.frame = bounds
        label.formatSmall()
        label.textAlignment = .Center
        label.backgroundColor = UIColor.whiteColor()
        contentView.addSubview(label)
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}