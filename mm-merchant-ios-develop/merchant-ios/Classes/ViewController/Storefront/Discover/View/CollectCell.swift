//
//  CollectCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 23/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class CollectCell : UICollectionViewCell {
    var nameLabel = UILabel()
    var imageView = UIImageView()
    var priceLabel = UILabel()
    var heartImageView = UIImageView()
    var brandImageView = UIImageView()
    
    private final let SaleFont = UIFont.boldSystemFontOfSize(16.0)
    private final let RetailFont = UIFont.systemFontOfSize(11)
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        addSubview(imageView)
        addSubview(brandImageView)
        nameLabel.formatSize(13)
        nameLabel.textAlignment = .Center
        addSubview(nameLabel)
        priceLabel.formatSize(16)
        priceLabel.font = SaleFont
        priceLabel.textAlignment = .Center
        priceLabel.escapeFontSubstitution = true
        addSubview(priceLabel)
        heartImageView.image = UIImage(named: "like_rest")
        addSubview(heartImageView)
        layoutSubviews()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: bounds.minX  , y: bounds.minY, width: bounds.width , height: bounds.width * Constants.Ratio.ProductImageHeight)
        brandImageView.frame = CGRect(x: bounds.midX - Constants.Value.BrandImageHeight / 2, y: imageView.frame.maxY , width: Constants.Value.BrandImageHeight , height: Constants.Value.BrandImageHeight)
        nameLabel.frame = CGRect(x: bounds.minX + 25 , y: brandImageView.frame.maxY  , width: bounds.width - 50 , height: 40)
        priceLabel.frame = CGRect(x: bounds.minX, y: nameLabel.frame.maxY + 4 , width: bounds.width, height: 20)
        
        let paddingRightOfHeartImageView : CGFloat = 35
        let paddingBottomOfHeartImageView : CGFloat = 33
        let sizeOfHeartImageView : CGSize = CGSizeMake(25, 25)
        heartImageView.frame = CGRect(x: imageView.frame.maxX - paddingRightOfHeartImageView, y: imageView.frame.maxY  - paddingBottomOfHeartImageView, width: sizeOfHeartImageView.width, height: sizeOfHeartImageView.height)
        heartImageView.contentMode = .ScaleAspectFit
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setBrandImage(imageKey : String){
        let filter = AspectScaledToFitSizeFilter(
            size: brandImageView.frame.size
        )
        brandImageView.af_setImageWithURL(ImageURLFactory.get(imageKey, category: .Brand), placeholderImage : UIImage(named: "Spacer"), filter: filter)
    }
    func setProductImage(imageKey : String){
        let filter = AspectScaledToFillSizeFilter(
            size: imageView.frame.size
        )
        imageView.af_setImageWithURL(ImageURLFactory.get(imageKey), placeholderImage : UIImage(named: "holder"), filter : filter)
    }
    func setPrice(style : Style)
    {
        let priceText = NSMutableAttributedString()
        if style.priceSale > 0 {
            if let priceSale = style.priceSale.formatPrice() {
                let saleText = NSAttributedString(
                    string: priceSale + " ",
                    attributes: [
                        NSForegroundColorAttributeName: UIColor(hexString: "#DB1E49"),
                        NSFontAttributeName: SaleFont,
                        NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleNone.rawValue
                    ]
                )
                priceText.appendAttributedString(saleText)
            }
            
            if let priceRetail = style.priceRetail.formatPrice() where style.priceRetail != style.priceSale {
                let retailText = NSAttributedString(
                    string: priceRetail,
                    attributes: [
                        NSForegroundColorAttributeName: UIColor(hexString: "#757575"),
                        NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue,
                        NSFontAttributeName: RetailFont,
                        NSBaselineOffsetAttributeName: (SaleFont.capHeight - RetailFont.capHeight) / 2
                    ]
                )
                
                priceText.appendAttributedString(retailText)
            }
        } else {
            if let priceRetail = style.priceRetail.formatPrice() where style.priceRetail != style.priceSale {
                let retailText = NSAttributedString(
                    string: priceRetail,
                    attributes: [
                        NSForegroundColorAttributeName: UIColor(hexString: "#4A4A4A"),
                        NSFontAttributeName: SaleFont
                    ]
                )
                priceText.appendAttributedString(retailText)
            }
        }
        priceLabel.attributedText = priceText
    }
    
}