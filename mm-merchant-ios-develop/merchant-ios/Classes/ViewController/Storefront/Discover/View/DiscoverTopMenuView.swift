//
//  DiscoverTopMenuView.swift
//  merchant-ios
//
//  Created by Alan YU on 6/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

class DiscoverTopMenuView: UIView {
    
    private final var underlineHeight = CGFloat(1)
    private final var underlineBottomPadding = CGFloat(5)
    private var underline = UIView()
    private var button = UIButton()
    
    var titleLabel = UILabel()
    
    var selected: Bool {
        set {
            if newValue {
                titleLabel.textColor = UIColor.primary1()
                underline.hidden = false
            } else {
                titleLabel.textColor = UIColor.secondary2()
                underline.hidden = true
            }
        }
        
        get {
            return !underline.hidden
        }
    }
    
    var touchUpClosure: UIButtonActionClosure? {
        didSet {
            button.touchUpClosure = touchUpClosure
        }
    }
    
    init(frame: CGRect, title: String) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.whiteColor()
        
        titleLabel.formatSmall()
        titleLabel.textAlignment = .Center
        titleLabel.font = UIFont.boldSystemFontOfSize(14.0)
        titleLabel .text = title
        addSubview(titleLabel)
        
        underline.backgroundColor = UIColor.primary1()
        addSubview(underline)
        
        addSubview(button)
        
        self.selected = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        button.frame = bounds
        titleLabel.frame = bounds
        underline.frame = CGRectMake(0, bounds.maxY - underlineHeight - underlineBottomPadding, bounds.width, underlineHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}