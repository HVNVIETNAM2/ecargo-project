//
//  ImageMarginCollectCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 22/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import AlamofireImage

class ImageMarginCollectCell : UICollectionViewCell{
    var imageView = UIImageView()
    var filter = UIView()
    var label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        imageView.image = UIImage(named: "holder")
        addSubview(imageView)
        filter.backgroundColor = UIColor.blackColor()
        filter.alpha = 0.3
        addSubview(filter)
        label.formatSmall()
        label.textAlignment = .Center
        addSubview(label)
        layout()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()

    }
    
    func layout(){
        imageView.frame = CGRect(x: bounds.minX, y:bounds.minY, width: bounds.width, height: bounds.height)
        filter.frame = self.bounds
        label.frame = CGRect(x: bounds.midX - 30, y: bounds.midY - 20, width: 60, height: 40)
    }
    
    func setImage(imageKey : String, category : ImageCategory){
        let filter = AspectScaledToFitSizeFilter(
            size: imageView.frame.size
        )
        imageView.af_setImageWithURL(ImageURLFactory.get(imageKey, category: category), placeholderImage : UIImage(named: "holder"), filter: filter)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}