//
//  SubCatDiscoverViewCell.swift
//  merchant-ios
//
//  Created by Tho NT Chan on 06/01/16.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class SubCatDiscoverViewCell : UICollectionViewCell {
    var label = UILabel()
    var imageView = UIImageView()
    var imageViewUnderLine = UIImageView()
    var countLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        label.formatSmall()
        label.backgroundColor = UIColor.whiteColor()
        contentView.addSubview(label)
        addSubview(imageView)
        addSubview(imageViewUnderLine)
        countLabel.formatSize(11)
        addSubview(countLabel)
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        label.frame = CGRect(x: bounds.midX - 15, y: bounds.midY - 10, width: 30, height: 20)
        imageView.frame = CGRect(x: bounds.maxX - 15 - 16, y: bounds.midY - 2, width: 10, height: 5)
        imageViewUnderLine.frame = CGRect(x: bounds.midX - 40, y: bounds.midY + 11, width: 80, height: 2)
        countLabel.frame = CGRect(x: label.frame.maxX, y: bounds.midY - 10, width: 20, height: 20)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setText(text: String) {
        let textWidth = self.getTextWidth(text, height: 20, font: label.font)
        label.frame = CGRect(x: bounds.midX - textWidth / 2, y: bounds.midY - 10, width: textWidth, height: 20)
        label.text = text
        imageView.frame = CGRect(x: label.frame.maxX + 5, y: bounds.midY - 2, width: 10, height: 5)
        countLabel.frame = CGRect(x: label.frame.maxX, y: bounds.midY - 10, width: 20, height: 20)
    }
    
    func getTextWidth(text: String, height: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.width
    }
}