//
//  BadgeCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 19/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class BadgeCell : UICollectionViewCell {
    var imageView = UIImageView()
    var label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageView)
        addSubview(label)
        label.formatSize(14)
        label.textAlignment = .Center
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: bounds.minX + 10, y: bounds.minY + 5, width: bounds.width - 20, height: bounds.height - 10)
        imageView.layer.cornerRadius = 5
        label.frame = CGRect(x: bounds.minX, y: bounds.minY, width: bounds.width, height: bounds.height)
    }
    
    func highlight(isSelected : Bool){
        if isSelected{
            imageView.backgroundColor = UIColor.secondary1()
        }else{
            imageView.backgroundColor = UIColor.whiteColor()
        }
    }
    
    func border() {
        imageView.layer.borderColor = UIColor.secondary1().CGColor
        imageView.layer.borderWidth = 1
    }
}