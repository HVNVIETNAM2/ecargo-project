//
//  FilterBrandController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 23/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import AlamofireImage

class ListBrandController : MmViewController, UISearchBarDelegate{
    var searchBar = UISearchBar()
    var searchString = ""
    var brands : [Brand] = []
    var validBrands : [Brand] = []
    var filteredBrands : [Brand] = []
    var filterStyleDelegate : FilterStyleDelegate?
    var aggregations : Aggregations?
    var styleFilter : StyleFilter?
    var styles : [Style] = []
    var refreshControl = UIRefreshControl()
    
    private let HeaderHeight : CGFloat = 50
    private let ImageMenuCellHeight : CGFloat = 60
    private let SearchBarHeight : CGFloat = 40
    private let ButtonCellHeight : CGFloat = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge.None
        self.title = String.localize("LB_CA_ALL_BRAND")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.frame = CGRect(x: self.view.bounds.minX, y: SearchBarHeight, width: self.view.bounds.width, height: self.view.bounds.height - SearchBarHeight - (self.navigationController?.navigationBar.frame.height)! - UIApplication.sharedApplication().statusBarFrame.size.height)
        loadBrand()
        self.collectionView.registerClass(ImageMenuCell.self, forCellWithReuseIdentifier: "ImageMenuCell")
        self.searchBar.sizeToFit()
        self.searchBar.delegate = self
        self.searchBar.searchBarStyle = UISearchBarStyle.Default
        self.searchBar.showsCancelButton = false
        self.searchBar.frame = CGRect(x: self.view.bounds.minX, y: self.view.bounds.minY, width: self.view.bounds.width, height: SearchBarHeight)
        self.view.insertSubview(self.searchBar, aboveSubview: self.collectionView)
        self.searchBar.placeholder = String.localize("LB_CA_SEARCH_FILTER_PLACEHOLDER")
        self.setUpRefreshControl()
        
        self.createBackButton()
    }
    
//    override var hidesBottomBarWhenPushed: Bool {
//        get { return true }
//        set {}
//    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func loadBrand() {
        self.showLoading()
        firstly{
            return self.listBrand()
            }.then
            { _ -> Void in
                self.validBrands = self.brands
                self.filteredBrands = self.validBrands
                self.collectionView.reloadData()
            }.always {
                self.stopLoading()
                self.refreshControl.endRefreshing()
                
            }.error { _ -> Void in
                Log.error("error")
        }
        
    }
    
    func listBrand() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchBrand(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        strongSelf.brands = Mapper<Brand>().mapArray(response.result.value) ?? []
                        strongSelf.brands = strongSelf.brands.filter({$0.brandId != 0})
                        
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageMenuCell", forIndexPath: indexPath) as! ImageMenuCell
        if (self.filteredBrands.count > indexPath.row) {
            cell.upperLabel.text = self.filteredBrands[indexPath.row].brandName
            cell.lowerLabel.text = self.filteredBrands[indexPath.row].brandNameInvariant
            cell.setImage(self.filteredBrands[indexPath.row].headerLogoImage, imageCategory: .Brand)
        }
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filteredBrands.count
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
            case self.collectionView!:
                switch indexPath.row {
                default:
                    return CGSizeMake(self.view.frame.size.width, ImageMenuCellHeight)
                }
            default:
                return CGSizeMake(0,0)
            }
    }
    
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            self.collectionView.reloadData()
            let discoverViewController = DiscoverViewController()
            let styleFilter = StyleFilter()
            let brand = Brand()
            brand.brandId = self.filteredBrands[indexPath.row].brandId
            brand.brandName = self.filteredBrands[indexPath.row].brandName
            styleFilter.brands = [brand]
            discoverViewController.styleFilter = styleFilter
            self.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(discoverViewController, animated: true)
    }
    
    //MARK : Refresh Control
    
    func setUpRefreshControl(){
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.collectionView.addSubview(refreshControl)
        self.collectionView.alwaysBounceVertical = true
    }
    
    func refresh(sender : AnyObject){
        loadBrand()
    }
    
    
    // MARK: UISearchBarDelegate
    
    private func filter(text : String!) {
        self.filteredBrands = self.validBrands.filter(){ $0.brandName.lowercaseString.rangeOfString(text.lowercaseString) != nil || $0.brandNameInvariant.lowercaseString.rangeOfString(text.lowercaseString) != nil }
        self.collectionView?.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.filter(searchBar.text!)
        searchBar.resignFirstResponder()
    }
    
    
    internal func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchString = searchBar.text!
        if searchString.length == 0 {
            self.filteredBrands = self.validBrands
            self.collectionView.reloadData()
        } else {
            self.filter(searchString)
        }
        
    }
 
    override func shouldHideTabBar() -> Bool {
        return true
    }
}