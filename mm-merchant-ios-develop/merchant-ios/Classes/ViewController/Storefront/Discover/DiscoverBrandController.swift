//
//  DiscoverBrandController.swift
//  merchant-ios
//
//  Created by Alan YU on 6/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import AlamofireImage

class DiscoverBrandController: MmViewController {
    
    private enum LayoutSectionKey: Int {
        case FeaturedBrands     = 0
        case RecommandHeader    = 1
        case RecommandBrands    = 2
    }
    
    private var featureBrandImages: [String]!
    private var brandImages: [String]!
    private var brands = [BrandUnionMerchant]()
    private var featuredBrands = [BrandUnionMerchant]()
    private var topMenuView: UIView!
    private var featuredCollectionViewHeight = CGFloat(0)
    private var featuredCollectionView: UICollectionView!
    private var recommandHeaderView: UIView!
    private var pageControl: UIPageControl!
    private var layoutSource: [LayoutSectionKey]!
    private var currentPage = 0
    private var timer: NSTimer?
    private var validToAutoScroll = false {
        didSet {
            if validToAutoScroll {
                timer = NSTimer.scheduledTimerWithTimeInterval(
                    5.0,
                    target: self,
                    selector: "update",
                    userInfo: nil,
                    repeats: true
                )
            } else {
                timer?.invalidate()
                timer = nil
            }
        }
    }
    
    private final let RecommandHeaderViewHeight = CGFloat(40)
    private final let TopMenuHeight: CGFloat = 40
    private final let FeaturedCollectionCellId = "FeaturedCollectionCellId"
    private final let FeaturedBrandsCellId = "FeaturedBrandsCellId"
    private final let RecommandHeaderCellId = "RecommandHeaderCellId"
    private final let RecommandBrandsCellId = "RecommandBrandsCellId"
    
    private final let DefaultCellId = "DefaultCellId"
    
    func setupNavigationBar() {
        
        setupNavigationBarCartButton()
        setupNavigationBarWishlistButton()
        
        buttonCart!.touchUpClosure = { [unowned self] button in
            self.navigationController?.pushViewController(ShoppingCartViewController(), animated: true)
        }
        
        buttonWishlist!.touchUpClosure = { [unowned self] button in
            self.navigationController?.pushViewController(WishListCartViewController(), animated: true)
        }
        
        buttonCart!.accessibilityIdentifier = "view_cart_button"
        buttonWishlist!.accessibilityIdentifier = "view_wishlist_button"
        
        let rightButtonItems = [
            UIBarButtonItem(customView: buttonCart!),
            UIBarButtonItem(customView: buttonWishlist!)
        ]
        
        let viewTitle = UIView(frame: CGRect(x: 50, y: 0, width: self.view.bounds.maxX * 2 / 3, height: 32.5))
        
        let searchButton = UIButton(type: UIButtonType.Custom)
        searchButton.frame = viewTitle.bounds
        searchButton.accessibilityIdentifier = "discover_search_button"
        searchButton.addTarget(self, action: Selector("searchIconClicked"), forControlEvents: UIControlEvents.TouchUpInside)
        
        viewTitle.addSubview(searchButton)
        
        let searchBarImage = UIImageView(image: UIImage(named: "search_bar"))
        searchBarImage.frame = CGRect(x: 0, y: 0, width: viewTitle.bounds.maxX, height: searchBarImage.bounds.maxY * viewTitle.bounds.maxX / searchBarImage.bounds.maxX)
        searchButton.addSubview(searchBarImage)
        
        let labelTitle = UILabel(frame: CGRect(x: 25, y: 5, width: viewTitle.bounds.maxX - 55, height: viewTitle.bounds.maxY - 10))
        labelTitle.text = String.localize("LB_CA_SEARCH_PLACEHOLDER")+"?"
        labelTitle.textColor = UIColor.grayColor()
        labelTitle.font = UIFont.boldSystemFontOfSize(14.0)
        searchButton.addSubview(labelTitle)
        
        self.navigationItem.titleView = viewTitle
        self.title = String.localize("LB_CA_DISCOVER")
        self.navigationItem.rightBarButtonItems = rightButtonItems
        
    }
    
    func searchIconClicked() {
        let discoverViewController = DiscoverViewController()
        discoverViewController.isSearch = true
        self.navigationController?.pushViewController(discoverViewController, animated: false)
    }
    
    func setupTopMenu() {
        
        var yPos = CGFloat(0)
        if let navigationBar = self.navigationController?.navigationBar {
            yPos = navigationBar.frame.maxY
        }
        
        topMenuView = UIView(frame: CGRectMake(0, yPos, view.bounds.width, TopMenuHeight))
        
        let MidPadding = CGFloat(20)
        let ButtonWidth = view.bounds.size.width / 4
        let xPos = (view.bounds.width - ButtonWidth * 2 - MidPadding) / 2.0
        
        let brandButton = DiscoverTopMenuView(
            frame: CGRectMake(xPos, 0, ButtonWidth, TopMenuHeight),
            title: String.localize("LB_AC_BRAND")
        )
        brandButton.selected = true
        
        let categoryButton = DiscoverTopMenuView(
            frame: CGRectMake(xPos + ButtonWidth + MidPadding, 0, ButtonWidth, TopMenuHeight),
            title: String.localize("LB_CA_CATEGORY_BRAND")
        )
        
        categoryButton.touchUpClosure = { [unowned self] button in
            self.navigationController?.pushViewController(DiscoverCatController(), animated: false)
        }
        
        categoryButton.accessibilityIdentifier = "category_button"
        
        topMenuView.addSubview(brandButton)
        topMenuView.addSubview(categoryButton)
        
        view.addSubview(topMenuView)
        
    }
    
    func setupFeaturedCollectionView() {
        
        featuredCollectionViewHeight = view.bounds.size.width * Constants.Ratio.PanelImageHeight
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        
        featuredCollectionView = UICollectionView(
            frame: CGRectMake(0, 0, view.bounds.width, featuredCollectionViewHeight),
            collectionViewLayout: layout
        )
        
        featuredCollectionView.pagingEnabled = true
        featuredCollectionView.backgroundColor = UIColor.whiteColor()
        
        featuredCollectionView.showsHorizontalScrollIndicator = false
        featuredCollectionView.dataSource = self
        featuredCollectionView.delegate = self
        featuredCollectionView.registerClass(FeatureCollectionCell.self, forCellWithReuseIdentifier: FeaturedCollectionCellId)
        
        let BottomPadding = CGFloat(10)
        
        pageControl = UIPageControl()
        pageControl.hidden = true
        pageControl.currentPage = 0
        pageControl.numberOfPages = 0
        pageControl.tintColor = UIColor.primary1()
        pageControl.pageIndicatorTintColor = UIColor.primary2()
        pageControl.currentPageIndicatorTintColor = UIColor.primary1()
        pageControl.center = CGPointMake(featuredCollectionView.center.x, featuredCollectionView.bounds.maxY - BottomPadding)
        
        featuredCollectionView.addSubview(pageControl)
    }
    
    func setupCollectionView() {
        
        collectionView.registerClass(ImageCollectCell.self, forCellWithReuseIdentifier: RecommandBrandsCellId)
        collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: RecommandHeaderCellId)
        collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: FeaturedBrandsCellId)
        collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: DefaultCellId)
        
    }
    
    
    func setupRecommandHeaderView() {
        
        let width = view.bounds.width
        recommandHeaderView = UIView(frame: CGRectMake(0, 0, width, RecommandHeaderViewHeight))
        
        let arrowWidth  = CGFloat(7.0)
        let padding = CGFloat(10)
        
        let recommandLabel = UILabel()
        recommandLabel.text = String.localize("LB_CA_RECOMMENDED")
        recommandLabel.textColor = UIColor.secondary2()
        recommandLabel.font = UIFont.boldSystemFontOfSize(14.0)
        recommandLabel.sizeToFit()
        recommandLabel.frame = CGRectMake(padding, 0, recommandLabel.bounds.width, RecommandHeaderViewHeight)
        recommandHeaderView.addSubview(recommandLabel)
        
        let arrowImageView = UIImageView(frame: CGRectMake(width - padding - arrowWidth, 0, arrowWidth, RecommandHeaderViewHeight))
        arrowImageView.image = UIImage(named: "icon_arrow")
        arrowImageView.contentMode = .ScaleAspectFit
        recommandHeaderView.addSubview(arrowImageView)
        
        let allLabel = UILabel()
        allLabel.text = String.localize("LB_CA_ALL")
        allLabel.textColor = UIColor.secondary2()
        allLabel.font = UIFont.boldSystemFontOfSize(12.0)
        allLabel.sizeToFit()
        allLabel.frame = CGRectMake(arrowImageView.frame.minX - padding - allLabel.bounds.width, 0, allLabel.bounds.width, RecommandHeaderViewHeight)
        recommandHeaderView.addSubview(allLabel)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupTopMenu()
        setupFeaturedCollectionView()
        setupCollectionView()
        setupRecommandHeaderView()
        
        firstly {
            return self.searchBrand()
            }.then { _ -> Void in
                self.reloadAllData()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        validToAutoScroll = true
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        validToAutoScroll = false
    }
    
    func update() {
        
        currentPage++
        if currentPage >= self.featuredBrands.count {
            currentPage = 0
        }
        
        guard !featuredBrands.isEmpty else {
            return
        }
        
        self.featuredCollectionView.scrollToItemAtIndexPath(
            NSIndexPath(forRow: currentPage, inSection: 0),
            atScrollPosition: .Left,
            animated: true
        )
        
    }
    
    func reloadAllData() {
        pageControl.currentPage = 0
        pageControl.numberOfPages = self.featuredBrands.count
        
        collectionView.reloadData()
        featuredCollectionView.reloadData()
    }
    
    func searchBrand() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchBrandCombined(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        let brands = Mapper<BrandUnionMerchant>().mapArray(response.result.value) ?? []
                        strongSelf.brands = brands.filter({$0.entityId != 0})
                        strongSelf.featuredBrands = strongSelf.brands.filter({$0.isFeatured == true})
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.backgroundColor = UIColor .whiteColor()
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if collectionView == self.collectionView {
            return 3
        } else if collectionView == self.featuredCollectionView {
            return 1
        }
        return 0
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionView {
            if section == LayoutSectionKey.RecommandBrands.rawValue {
                return brands.count
            } else {
                return 1
            }
        } else if collectionView == self.featuredCollectionView {
            return featuredBrands.count
        }
        return 0
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var resultCell: UICollectionViewCell!
        
        if collectionView == self.collectionView {
            
            switch indexPath.section  {
            case LayoutSectionKey.FeaturedBrands.rawValue:
                
                resultCell = collectionView.dequeueReusableCellWithReuseIdentifier(FeaturedBrandsCellId, forIndexPath: indexPath)
                resultCell.addSubview(featuredCollectionView)
                
            case LayoutSectionKey.RecommandHeader.rawValue:
                
                resultCell = collectionView.dequeueReusableCellWithReuseIdentifier(RecommandHeaderCellId, forIndexPath: indexPath)
                resultCell.addSubview(recommandHeaderView)
                
            case LayoutSectionKey.RecommandBrands.rawValue:
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(RecommandBrandsCellId, forIndexPath: indexPath) as! ImageCollectCell
                let brand = self.brands[indexPath.row]
                
                cell.setImage(brand.largeLogoImage, category: brand.imageCategory)
                cell.filter.alpha = 0.0
                
                resultCell = cell
                
            default:
                
                resultCell = collectionView.dequeueReusableCellWithReuseIdentifier(DefaultCellId, forIndexPath: indexPath)
                
            }
            
        } else if collectionView == self.featuredCollectionView {
            
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FeaturedCollectionCellId, forIndexPath: indexPath) as! FeatureCollectionCell
            let brand = self.featuredBrands[indexPath.row]
            
            cell.setImageRoundedCorners(brand.profileBannerImage, imageCategory: brand.imageCategory)
            
            resultCell = cell
        }
        
        return resultCell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if collectionView == self.collectionView {
            
            if indexPath.section == LayoutSectionKey.RecommandHeader.rawValue {
                self.navigationController?.pushViewController(ListBrandController(), animated: true)
            } else if indexPath.section == LayoutSectionKey.RecommandBrands.rawValue {
                
                let brand = Brand()
                brand.brandId = self.brands[indexPath.row].entityId
                brand.brandName = self.brands[indexPath.row].name
                
                let styleFilter = StyleFilter()
                styleFilter.brands = [brand]
                
                let discoverViewController = DiscoverViewController()
                discoverViewController.styleFilter = styleFilter
                
                self.navigationController?.pushViewController(discoverViewController, animated:true)
                
            }
            
        } else if collectionView == self.featuredCollectionView {
            
            let brand = Brand()
            brand.brandId = self.featuredBrands[indexPath.row].entityId
            brand.brandName = self.featuredBrands[indexPath.row].name
            
            let styleFilter = StyleFilter()
            styleFilter.brands = [brand]
            
            let discoverViewController = DiscoverViewController()
            discoverViewController.styleFilter = styleFilter
            
            self.navigationController?.pushViewController(discoverViewController, animated: true)
            
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        if collectionView == self.collectionView && section == LayoutSectionKey.RecommandBrands.rawValue {
            return Constants.LineSpacing.ImageCell
        }
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        if collectionView == self.collectionView && section == LayoutSectionKey.RecommandBrands.rawValue {
            return UIEdgeInsetsMake(Constants.Margin.Top, Constants.Margin.Left, Constants.Margin.Bottom, Constants.Margin.Right)
        }
        return UIEdgeInsetsZero
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if collectionView == self.collectionView {
            switch indexPath.section {
            case LayoutSectionKey.FeaturedBrands.rawValue:
                return featuredCollectionView.bounds.size
            case LayoutSectionKey.RecommandHeader.rawValue:
                return recommandHeaderView.bounds.size
            default:
                let width = (self.view.frame.size.width - (Constants.Margin.Left + Constants.Margin.Right + Constants.LineSpacing.ImageCell)) / 2
                return CGSizeMake(width, width)
            }
            
        } else if collectionView == self.featuredCollectionView {
            return self.featuredCollectionView.bounds.size
        }
        
        return CGSizeZero
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scrollView == self.featuredCollectionView {
            self.currentPage = Int(self.featuredCollectionView.contentOffset.x / self.featuredCollectionView.frame.size.width)

        }
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.validToAutoScroll = false
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.validToAutoScroll = true
    }
    
    override func collectionViewTopPadding() -> CGFloat {
        return TopMenuHeight
    }
    
}