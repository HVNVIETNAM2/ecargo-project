//
//  UICollectionViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 23/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import BTNavigationDropdownMenu
import PromiseKit
import AlamofireImage

//MARK:- Delegate Protocol
protocol FilterStyleDelegate{
    func filterStyle(styles : [Style], styleFilter : StyleFilter)
}

class DiscoverViewController : MmCartViewController, GHContextOverlayViewDelegate, GHContextOverlayViewDataSource, LiquidFloatingActionButtonDataSource, LiquidFloatingActionButtonDelegate, FilterStyleDelegate, UIGestureRecognizerDelegate {
    
    private let ReuseIdentifier = "CollectCell"
    private let SortMenuHeight : CGFloat = 50
    private let HeaderHeight : CGFloat = 50
    private final let WidthItemBar : CGFloat = 30
    private final let HeightItemBar : CGFloat = 25
    private final let SearchIconWidth : CGFloat = 16
    private final let MarginLeft : CGFloat = 10
    private final let SearchBoxWidthRatio : CGFloat = 242 / 375
    private final let BackgroundColor : UIColor = UIColor(hexString: "#FAFAFA")
    var subCatCollectionView : UICollectionView?
    var sortCollectionView : UICollectionView?
    var containerView : ContainerView?
    var containerViewBottom : ContainerView?
    var dismissOverlayButton : UIButton?
    
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: Constants.Margin.Left, bottom: 0.0, right: Constants.Margin.Right)
    var styles : [Style] = []
    var filteredStyles : [Style] = []
    var cats : [Cat] = []
    var image : [UIImage] = []
    var menu : GHContextMenuView!
    var productCategoryView : UIView?
    var sortMenuOpen = false
    var liquidFloatingCell : LiquidFloatingCell?
    var searchBarButtonItem : UIBarButtonItem!
    var styleFilter : StyleFilter!
    var isSearch : Bool = false
    var sortMenu : [String] = []
    var aggregations : Aggregations?
    var brandLogos : [String]!
    var sortSelected : Int = 0
    var sortStringSelected : String = String.localize("LB_CA_SORT_OVERALL")
    var filteredBrands : [Brand]?
    var isDoingAnimation = false
    override var hidesBottomBarWhenPushed: Bool {
        get { return false }
        set {}
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        //        self.collectionView?.registerClass(SubCatView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SubCatView")
        self.navigationController!.navigationBarHidden = false
        self.collectionView!.delegate = self
        self.collectionView!.dataSource = self
        self.collectionView!.backgroundColor = UIColor.whiteColor();
        self.collectionView!.registerClass(CollectCell.self, forCellWithReuseIdentifier: ReuseIdentifier)
        if self.styleFilter == nil {
            self.styleFilter = StyleFilter()
        }
        self.navigationController!.interactivePopGestureRecognizer!.delegate = self
        //set up liquid action button for filtering
        self.liquidFloatingCell = LiquidFloatingCell(icon: UIImage(named: "filter_rest")!)
        let floatingActionButton = LiquidFloatingActionButton(frame: CGRect(x: (self.collectionView?.bounds.width)!/2 - 15, y: (self.collectionView?.bounds.height)!/6 * 5 - 15, width: 30, height: 30))
        floatingActionButton.dataSource = self
        floatingActionButton.delegate = self
        //self.view.addSubview(floatingActionButton)
        
        //set up sort menu data sourcefds
        self.sortMenu.append(String.localize("LB_CA_SORT_OVERALL"))
        self.sortMenu.append(String.localize("LB_CA_SORT_SALES_VOL"))
        self.sortMenu.append(String.localize("LB_CA_SORT_DATE"))
        self.sortMenu.append(String.localize("LB_CA_SORT_PRICE_ASC"))
        self.sortMenu.append(String.localize("LB_CA_SORT_PRICE_DESC"))
        //set up underlying view to later clip subview
        self.containerView = ContainerView(frame: CGRect(x: 0, y: 100, width: self.view.frame.width, height: self.view.frame.height))
        self.containerView!.clipsToBounds = true
        self.containerView!.hidden = true
        self.view.addSubview(containerView!)
        let height = self.tabBarController?.tabBar.frame.height
        self.containerViewBottom = ContainerView(frame: CGRect(x: 0, y: self.view.frame.height - height!, width: self.view.frame.width, height: height!))
        self.containerViewBottom!.clipsToBounds = true
        self.containerViewBottom!.hidden = true
        
        
        //set up the sort collection view
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = UICollectionViewScrollDirection.Vertical
        self.sortCollectionView = UICollectionView(frame: CGRect(x: 0, y: -CGFloat(self.sortMenu.count) * SortMenuHeight, width: self.view.frame.width, height: CGFloat(self.sortMenu.count) * SortMenuHeight), collectionViewLayout: layout)
        self.sortCollectionView?.dataSource = self
        self.sortCollectionView?.delegate = self
        self.sortCollectionView?.backgroundColor = UIColor.brownColor()
        self.containerView!.addSubview(self.sortCollectionView!)
        self.sortCollectionView?.registerClass( PickerCell.self, forCellWithReuseIdentifier: "PickerCell")
        self.sortCollectionView?.backgroundColor = BackgroundColor
        
        
        self.dismissOverlayButton = UIButton(type: .Custom)
        self.dismissOverlayButton?.backgroundColor = UIColor.clearColor()
        self.dismissOverlayButton?.frame = CGRect(x: 0, y: sortCollectionView!.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - sortCollectionView!.frame.maxY)
        self.dismissOverlayButton?.addTarget(self, action: "dismissOverlayClicked:", forControlEvents: UIControlEvents.TouchDown)
        
        self.title = String.localize("LB_CA_DISCOVER")
        if self.styleFilter != nil {
            if !self.styleFilter.cats.isEmpty {
                self.title = self.styleFilter.cats[0].categoryName
            } else if !self.styleFilter.brands.isEmpty {
                self.title = self.styleFilter.brands[0].brandName
            }
        }
        
        if self.isSearch {
            self.isSearch = false
            doSearch(true)
        }
        self.showLoading()
        firstly{
            return self.listCat()
            }.then { _ in
                return self.searchStyle()
            }.then { _ -> Void in
                // setup context menu with long press gesture
                self.menu = GHContextMenuView()
                self.menu.dataSource = self
                self.menu.delegate = self
                
                let gesture = UILongPressGestureRecognizer(target: self.menu, action: "longPressDetected:")
                self.collectionView!.addGestureRecognizer(gesture)
                
                // setup navigation menu (first level category)
                var items : [String] = [String.localize("LB_CA_ALL")]
                for cat in self.cats {
                    Log.debug(cat.categoryName)
                    items.append(cat.categoryName)
                }
                
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
        self.createTopView()
    }
    
    func createTopView() {
        let layout: SnapFlowLayout = SnapFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        self.subCatCollectionView = UICollectionView(frame: CGRect(x: 0, y: 64, width: self.view.bounds.width, height: HeaderHeight), collectionViewLayout: layout)
        self.subCatCollectionView!.delegate = self
        self.subCatCollectionView!.dataSource = self
        self.subCatCollectionView!.backgroundColor = UIColor.whiteColor()
        self.subCatCollectionView!.registerClass(SubCatDiscoverViewCell.self, forCellWithReuseIdentifier: "SubCatDiscoverViewCell")
        self.view.addSubview(self.subCatCollectionView!)
    }
    
    func likeNaviBarTapped() {
        Log.debug("heart clicked!")
        let wishlist = WishListCartViewController()
        self.navigationController?.pushViewController(wishlist, animated: true)
    }
    
    // back button clicked event's
    func onBackButton()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func navigateToshoppingCart() {
        Log.debug("Shopping cart Tapped!")
        let shoppingcart = ShoppingCartViewController()
        
        self.navigationController?.pushViewController(shoppingcart, animated: true)
    }
    
    func searchIconClicked(){
        doSearch(false)
    }
    
    
    func doSearch(popTwice : Bool){
        let searchStyleController = SearchStyleController()
        searchStyleController.styles = self.styles
        searchStyleController.filterStyleDelegate = self
        searchStyleController.styleFilter = self.styleFilter
        searchStyleController.popTwice = popTwice
        self.navigationController?.pushViewController(searchStyleController, animated: false)
    }
    
    
    func searchStyle() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchStyle(self.styleFilter){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if let styleResponse = Mapper<SearchResponse>().map(response.result.value) {
                            
                            
                            if let styles = styleResponse.pageData {
                                strongSelf.styles = styles
                                strongSelf.filteredStyles = strongSelf.styles
                            } else {
                                strongSelf.styles = []
                                strongSelf.filteredStyles = []
                            }
                            if let aggregations = styleResponse.aggregations {
                                strongSelf.aggregations = aggregations
                            } else {
                                strongSelf.aggregations = Aggregations()
                            }
                            strongSelf.reloadAllData()
                        }
                        fulfill("OK")
                    } else {
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    func listCat() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            CatService.list(0){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        var cats = Mapper<Cat>().mapArray(response.result.value) ?? []
                        cats = cats.filter(){ $0.categoryId != 0 }
                        strongSelf.cats = cats
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.sortCollectionView!:
            return self.sortMenu.count
        case self.collectionView!:
            return self.filteredStyles.count
        case self.subCatCollectionView!:
            return 3
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.sortCollectionView!:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PickerCell", forIndexPath: indexPath) as! PickerCell
            cell.label.text = self.sortMenu[indexPath.row]
            if indexPath.row == self.sortSelected {
                cell.imageView.image = UIImage(named:"tick")
            } else {
                cell.imageView.image = nil
            }
            cell.backgroundColor = BackgroundColor
            return cell
        case self.collectionView!:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ReuseIdentifier, forIndexPath: indexPath) as! CollectCell
            cell.backgroundColor = UIColor.whiteColor()
            if (filteredStyles.count > 0) {
                if styleFilter.colors.isEmpty {
                    cell.setProductImage(self.filteredStyles[indexPath.row].imageDefault)
                } else {
                    let colorList = self.filteredStyles[indexPath.row].colorList.filter(){$0.colorId == styleFilter.colors[0].colorId}
                    if colorList.isEmpty {
                        cell.setProductImage(self.filteredStyles[indexPath.row].imageDefault)
                    } else {
                        let colorKey = colorList[0].colorKey
                        let colorImages = self.filteredStyles[indexPath.row].colorImageList
                        var shortListedColorImages = colorImages.filter(){$0.colorKey.lowercaseString == colorKey.lowercaseString}
                        shortListedColorImages = shortListedColorImages.sort(){$0.position < $1.position}
                        if shortListedColorImages.isEmpty {
                            cell.setProductImage(self.filteredStyles[indexPath.row].imageDefault)
                        } else {
                            cell.setProductImage(shortListedColorImages[0].imageKey)
                        }
                    }
                    
                }
                cell.nameLabel.text =  self.filteredStyles[indexPath.row].skuName
                cell.nameLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
                cell.nameLabel.numberOfLines = 2
                cell.nameLabel.textAlignment = .Center
                cell.setPrice(self.filteredStyles[indexPath.row])
                
                if self.filteredStyles[indexPath.row].isWished() {
                    cell.heartImageView.image = UIImage(named: "like_on")
                } else {
                    cell.heartImageView.image = UIImage(named: "like_rest")
                }
                
                cell.heartImageView.userInteractionEnabled = true
                cell.heartImageView.addGestureRecognizer(UITapGestureRecognizer(target:self, action:Selector("imageTapped:")))
                cell.setBrandImage(self.styles[indexPath.row].brandSmallLogoImage)
                
                cell.accessibilityIdentifier = "discover_view_cell_\(indexPath.section)_\(indexPath.row)"
                
                cell.heartImageView.isAccessibilityElement = true
                cell.heartImageView.accessibilityIdentifier = "heart_imageview_\(indexPath.section)_\(indexPath.row)"
            }
            
            return cell
        case self.subCatCollectionView!:
            if self.subCatCollectionView != nil{
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier("SubCatDiscoverViewCell", forIndexPath: indexPath) as! SubCatDiscoverViewCell
                
                switch indexPath.row {
                case 0:
                    
                    if sortMenuOpen == true {
                        cell.imageView.image = UIImage(named: "arrow_open")
                    } else {
                        cell.imageView.image = UIImage(named: "arrow_close")
                    }
                    cell.countLabel.text = ""
                    cell.setText(sortStringSelected)
                    break
                case 1:
                    cell.imageView.image = nil
                    cell.countLabel.text = "(" + String(self.styleFilter.brands.count) + ")"
                    cell.setText(String.localize("LB_CA_BRAND"))
                    break
                case 2:
                    cell.imageView.image = nil
                    cell.countLabel.text = "(" + String(self.styleFilter.count()) + ")"
                    cell.setText(String.localize("LB_CA_FILTER"))
                    break
                default:
                    break
                }
                return cell
            }
            else {
                
                return self.subCatCollectionView!.dequeueReusableCellWithReuseIdentifier("SubCatDiscoverViewCell", forIndexPath: indexPath) as! SubCatDiscoverViewCell
            }
        default:
            return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
        }
    }
    
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ReuseIdentifier, forIndexPath: indexPath)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
            case self.sortCollectionView!:
                return CGSizeMake(self.view.frame.width, SortMenuHeight)
            case self.collectionView!:
                let width = (self.view.frame.size.width - (Constants.Margin.Left + Constants.Margin.Right + Constants.LineSpacing.ImageCell)) / 2
                return CGSizeMake(width, width * Constants.Ratio.ProductImageHeight + Constants.Value.ProductBottomViewHeight)
            case self.subCatCollectionView!:
                return CGSizeMake(self.view.frame.width / 3 ,HeaderHeight)
            default:
                return CGSizeMake(0,0)
                
            }
            
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            switch collectionView {
            case self.collectionView!:
                return self.sectionInsets
            default:
                return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
            }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch collectionView {
        default:
            return 0.0
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch collectionView {
        case self.collectionView!:
            return Constants.LineSpacing.ImageCell
        default:
            return 0.0
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        switch collectionView {
        case self.collectionView:
            switch section {
            case 0:
                return CGSizeMake(self.view.frame.width, HeaderHeight)
            default:
                return CGSizeMake(0,0)
                
            }
        default:
            return CGSizeMake(0,0)
        }
    }
    
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            switch collectionView{
            case self.sortCollectionView!:
                self.sortSelected = indexPath.row
                self.sortStringSelected = sortMenu[indexPath.row]
                switch indexPath.row {
                case 0:
                    break
                case 1:
                    break
                case 2:
                    break
                case 3:
                    //sort PriceRetail order ASC
                    self.styleFilter.sort = "PriceRetail"
                    self.styleFilter.order = "asc"
                    self.searchStyle()
                    break
                case 4:
                    //sort PriceRetail order DESC
                    self.styleFilter.sort = "PriceRetail"
                    self.styleFilter.order = "desc"
                    self.searchStyle()
                    break
                default:
                    break
                }
                
                self.hide(self.sortCollectionView!)
            case self.collectionView!:
                //selection done at Product
                let detailViewController = DetailViewController()
                detailViewController.style = self.filteredStyles[indexPath.row]
                detailViewController.suggestStyles = self.filteredStyles
                detailViewController.styleFilter = self.styleFilter
                self.navigationController?.pushViewController(detailViewController, animated: true)
                break
                
            case self.subCatCollectionView!:
                switch indexPath.row {
                case 0:
                    if isDoingAnimation { //Fix but when user tap continueously
                        return
                    }
                    if sortMenuOpen == false {
                        self.show(self.sortCollectionView!)
                    } else {
                        self.hide(self.sortCollectionView!)
                    }
                    break
                case 1:
                    filterBrandVC()
                    break
                case 2:
                    filterVC()
                    break
                default:
                    break
                }
                break
            default:
                break
                
            }
            reloadAllData()
            
    }
    
    //    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
    //        if collectionView == self.collectionView {
    //            switch kind {
    //            case UICollectionElementKindSectionHeader:
    //                let headerView = self.collectionView!.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "SubCatView", forIndexPath: indexPath) as! SubCatView
    //                headerView.subCatCollectionView!.dataSource = self
    //                headerView.subCatCollectionView!.delegate = self
    //                self.subCatCollectionView = headerView.subCatCollectionView
    //                self.subCatCollectionView?.registerClass(SubCatDiscoverViewCell.self, forCellWithReuseIdentifier: "SubCatDiscoverViewCell")
    //                self.subCatCollectionView?.backgroundColor = BackgroundColor
    //                return headerView
    //            default:
    //                assert(false, "Unexpected element kind")
    //            }
    //        }
    //        return self.collectionView!.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "SubCatView", forIndexPath: indexPath)
    //    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        MobClick.beginLogPageView("ProductCollectionView")
        self.navigationController!.navigationBar.shadowImage = nil
        self.navigationController!.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
        
        self.collectionView?.reloadData()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let win:UIWindow = UIApplication.sharedApplication().delegate!.window!!
        win.addSubview(self.containerViewBottom!)
        
        self.dismissOverlayButton?.removeFromSuperview()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        sortMenuOpen = false
        self.reloadAllData()
        self.hide(self.sortCollectionView!)
        self.containerViewBottom?.removeFromSuperview()
        MobClick.endLogPageView("ProductCollectionView")
    }
    
    // MARK: GHContextOverlayViewDelegate, GHContextOverlayViewDataSource
    func numberOfMenuItems() -> Int {
        return 3
    }
    
    func imageForItemAtIndex(index: Int) -> UIImage! {
        let imageName : String
        switch index {
        case 0:
            imageName = "fan_share"
            break
        case 1:
            imageName = "fan_cart"
            break
        case 2:
            imageName = "fan_cs"
            break
        default:
            imageName = "fan_cart"
            break
        }
        return UIImage(named: imageName)
    }
    
    func shouldShowMenuAtPoint(point: CGPoint) -> Bool {
        //If there is a cell, then the menu should activate.
        if let indexPath = self.collectionView?.indexPathForItemAtPoint(point) {
            let cell = self.collectionView!.cellForItemAtIndexPath(indexPath)
            return cell != nil;
        }
        return false
    }
    
    func didSelectItemAtIndex(selectedIndex: Int, forMenuAtPoint point: CGPoint) {
        let indexPath = collectionView!.indexPathForItemAtPoint(point)
        let message = self.filteredStyles[indexPath!.row].skuName
        switch selectedIndex{
        case 0:
            let objectsToShare = [message]
            
            let activityViewController      = UIActivityViewController(activityItems: objectsToShare as [AnyObject], applicationActivities: nil)
            
            presentViewController(activityViewController, animated: true, completion: nil)
            break
        case 1:
            buy(indexPath!)
            break
        case 2:
            UIApplication.sharedApplication().openURL(NSURL(string:"tel:90433672")!)
            break
        default:
            break
        }
    }
    
    // MARK: LiquidFloatingActionButton Delegate, DataSource
    
    func numberOfCells(liquidFloatingActionButton: LiquidFloatingActionButton) -> Int {
        return 1
    }
    
    func cellForIndex(index: Int) -> LiquidFloatingCell {
        return self.liquidFloatingCell!
        
    }
    
    func liquidFloatingActionButton(liquidFloatingActionButton: LiquidFloatingActionButton, didSelectItemAtIndex index: Int){
        Log.debug("Liquide Floating Button did Tapped! \(index)")
        liquidFloatingActionButton.close()
        filterVC()
    }
    
    func filterVC() -> FilterViewController{
        let filterViewController = FilterViewController()
        filterViewController.styles = self.styles
        filterViewController.filterStyleDelegate = self
        filterViewController.styleFilter = self.styleFilter
        filterViewController.aggregations = self.aggregations!
        self.navigationController?.pushViewController(filterViewController, animated: true)
        return filterViewController
    }
    
    func filterBrandVC() -> FilterBrandController{
        let filterBrandController = FilterBrandController()
        filterBrandController.styles = self.styles
        filterBrandController.filterStyleDelegate = self
        filterBrandController.styleFilter = self.styleFilter
        filterBrandController.aggregations = self.aggregations!
        self.navigationController?.pushViewController(filterBrandController, animated: true)
        return filterBrandController
    }
    
    // MARK: Heart Image View On Tap
    
    func imageTapped(sender : UIGestureRecognizer){
        Log.debug("heart clicked!")
        let point : CGPoint = sender.view!.convertPoint(CGPointZero, toView:collectionView)
        let indexPath = collectionView!.indexPathForItemAtPoint(point)
        
        if let style = filteredStyles[(indexPath?.row)!] as Style?  {
            if style.isWished() {
                let cartItemId = CacheManager.sharedManager.cartItemIdForStyle(style)
                firstly{
                    return self.removeWishlistItem(cartItemId)
                    }.then { _ -> Void in
                        // reload UI
                        self.collectionView?.reloadData()
                        self.updateButtonWishlistState()
                    }.error { _ -> Void in
                        Log.error("error")
                }
            } else {
                firstly{
                    return self.addWishlistItem(style.merchantId, styleCode: style.styleCode, skuId: NSNotFound, colorKey: nil)
                    }.then { _ -> Void in
                        let wishlistAnimation = WishlistAnimation(heartImage: sender.view as! UIImageView, redDotButton: self.buttonWishlist)
                        wishlistAnimation.showAnimation(completion: { () -> Void in
                            // reload UI
                            self.collectionView?.reloadData()
                            self.updateButtonWishlistState()
                        })
                        
                    }.error { _ -> Void in
                        Log.error("error")
                }
            }
        }
    }
    
    func buy(indexPath : NSIndexPath){
        let checkOutViewController = CheckOutViewController(
            style: self.filteredStyles[indexPath.row],
            redDotButton: self.buttonCart
        )
        checkOutViewController.checkOutMode = CheckOutMode.StyleOnly
        
        self.presentViewController(checkOutViewController, animated: false, completion: nil)
        checkOutViewController.didDismissHandler = {
            self.updateButtonCartState()
        }
        self.collectionView?.reloadData()
        
    }
    
    func reloadAllData(){
        self.collectionView?.reloadData()
        self.subCatCollectionView?.reloadData()
        self.sortCollectionView?.reloadData()
    }
    
    //MARK: FilterStyle Delegate
    func filterStyle(styles : [Style], styleFilter : StyleFilter){
        //Create search box
        var searchTerm = styleFilter.queryString
        if(!styleFilter.brands.isEmpty) {
            searchTerm = styleFilter.brands[0].brandName
        }
        else if(!styleFilter.cats.isEmpty) {
            searchTerm = styleFilter.cats[0].categoryName
        }
        else if(!styleFilter.merchants.isEmpty) {
            searchTerm = styleFilter.merchants[0].merchantName
        }
        self.createSearchBox(searchTerm)
        self.styles = styles
        self.filteredStyles = styles
        self.styleFilter = styleFilter
        firstly {
            return self.searchStyle()
            }.then { _ -> Void in
                self.reloadAllData()
        }
    }
    
    //MARK: Helper function to hide and unhide the container view and manipulate the arrow sign
    func hide(collectionView : UICollectionView){
        self.dismissOverlayButton?.removeFromSuperview()
        sortMenuOpen = false
        isDoingAnimation = true
        self.containerView?.hide()
        self.containerViewBottom?.hide()
        UIView.animateWithDuration(0.5, animations: {collectionView.frame = CGRectMake(0, -CGFloat(self.sortMenu.count) * self.SortMenuHeight, self.view.frame.width, CGFloat(self.sortMenu.count) * self.SortMenuHeight)}, completion: {(finished: Bool) -> Void in self.didHideViewOverlay()})
        
    }
    
    func didHideViewOverlay() {
        NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "doneAnimating", userInfo: nil, repeats: false)
    }
    
    func didShowViewOverlay() {
        NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "doneAnimating", userInfo: nil, repeats: false)
    }
    
    func doneAnimating() {
        isDoingAnimation = false
    }
    
    func dismissOverlayClicked(sender: UIControl){
        hide(self.sortCollectionView!)
    }
    
    func show(collectionView : UICollectionView) {
        sortMenuOpen = true
        isDoingAnimation = true
        self.containerView?.show()
        
        self.containerViewBottom?.show()
        UIView.animateWithDuration(0.5, animations: {collectionView.frame = CGRectMake(0, 13, self.view.frame.width, CGFloat(self.sortMenu.count) * self.SortMenuHeight)}, completion: {(finished: Bool) -> Void in
            self.didShowViewOverlay()
            self.view.window?.insertSubview(self.dismissOverlayButton!, belowSubview: collectionView)
            let collecitonViewRect = collectionView.superview?.convertRect(collectionView.frame, toView: self.view.window)
            self.dismissOverlayButton?.frame = CGRect(x: 0, y: collecitonViewRect!.maxY , width: self.view.frame.width, height: self.view.frame.height - collecitonViewRect!.maxY)
            
        })
    }
    
    func initButtonBar(imageName: String, selectorName: String, size:CGSize,left: CGFloat, right: CGFloat) -> UIBarButtonItem {
        let button: UIButton = UIButton()
        button.setImage(UIImage(named: imageName), forState: .Normal)
        button.frame = CGRectMake(0, 0, size.width, size.height)
        button.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: left, bottom: 0, right: right)
        button .addTarget(self, action:Selector(selectorName), forControlEvents: UIControlEvents.TouchUpInside)
        
        let temp:UIBarButtonItem = UIBarButtonItem()
        temp.customView = button
        return temp
    }
    
    func setupNavigationBar() {
        setupNavigationBarCartButton()
        setupNavigationBarWishlistButton()
        
        var rightButtonItems = [UIBarButtonItem]()
        rightButtonItems.append(UIBarButtonItem(customView: buttonCart!))
    
        rightButtonItems.append(UIBarButtonItem(customView: buttonWishlist!))
    
        let backButtonItem = self.createBack()
        self.searchBarButtonItem = backButtonItem
        let searchButtonItem = self.initButtonBar("search_grey", selectorName: "searchIconClicked", size: CGSize(width: WidthItemBar,height: 24), left: -41, right: 0)
        var leftButtonItems = [UIBarButtonItem]()
        leftButtonItems.append(backButtonItem)
        leftButtonItems.append(searchButtonItem)
        
        buttonCart!.addTarget(self, action: "navigateToshoppingCart", forControlEvents: .TouchUpInside)
        buttonWishlist!.addTarget(self, action: "likeNaviBarTapped", forControlEvents: .TouchUpInside)
        
        buttonCart!.accessibilityIdentifier = "view_cart_button"
        buttonWishlist!.accessibilityIdentifier = "view_wishlist_button"
        
        self.navigationItem.rightBarButtonItems = rightButtonItems
        self.navigationItem.leftBarButtonItems = leftButtonItems
    }
    
    func createSearchBox(searchTerm : String) {
        if searchTerm.length == 0 {
            return
        }
        //View search box
        let searchView = UIView(frame: CGRect(x: -21, y: 0, width: self.view.bounds.maxX * SearchBoxWidthRatio, height: 29))
        searchView.layer.cornerRadius = searchView.bounds.maxY / 2;
        searchView.layer.borderColor = UIColor.secondary1().CGColor
        searchView.layer.borderWidth = 1.0
        searchView.userInteractionEnabled = false;
        let searchIconImageView = UIImageView(image: UIImage(named: "search_grey"))
        searchIconImageView.frame = CGRect(x: MarginLeft, y: searchView.bounds.midY - SearchIconWidth / 2, width: SearchIconWidth, height: SearchIconWidth)
        searchView.addSubview(searchIconImageView)
        
        //Label search term
        let searchTermLabel = UILabel(frame: CGRect(x: searchIconImageView.frame.maxX + MarginLeft, y: 0, width: searchView.bounds.maxX - (searchIconImageView.bounds.maxX + MarginLeft), height: searchView.bounds.maxY))
        searchTermLabel.formatSize(12)
        searchTermLabel.text = searchTerm
        searchView.addSubview(searchTermLabel)
        
        //Button Search contains search box
        let searchButton = UIButton(type: UIButtonType.Custom)
        searchButton.frame = searchView.bounds
        searchButton .addTarget(self, action:Selector("searchIconClicked"), forControlEvents: UIControlEvents.TouchUpInside)
        searchButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        searchButton.addSubview(searchView)
        
        self.navigationItem.leftBarButtonItems = [createBack(),UIBarButtonItem(customView: searchButton)]
        self.title = ""
    }
    
    func createBack() -> UIBarButtonItem {
        return self.initButtonBar("back", selectorName: "onBackButton", size: CGSize(width: WidthItemBar,height: HeightItemBar), left: -36, right: 0)
    }
}