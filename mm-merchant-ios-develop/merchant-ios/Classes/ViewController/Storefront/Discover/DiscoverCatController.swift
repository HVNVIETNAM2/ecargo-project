//
//  DiscoverCatController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 17/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import AlamofireImage
class DiscoverCatController : MmViewController{
    
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    private final let CellId = "Cell"
    private final let CatCellId = "CatCell"
    private final let SubCatCellId = "SubCatCell"
    private final let FeatureImageCellId = "FeatureImageCell"
    private final let FeatureCollectionCellId = "FeatureCollectionCell"
    private final let ImageCellId = "ImageCell"
    private final let ImageCollectCellId = "ImageCollectCell"
    private final let ExpandedImageCollectCellId = "ExpandedImageCollectCell"
    private final let ImageMarginCollectCellId = "ImageMarginCollectCell"
    
    private final let CatCellHeight : CGFloat = 40
    private final let FeatureImageCellHeight : CGFloat = 230
    private final let ImageCollectCellHeight : CGFloat = 80
    private final let ExpandedImageCollectCellHeight :CGFloat = 280
    private final let SubCatCellHeight : CGFloat = 40
    private final let LabelWidth : CGFloat = 45
    private final let LabelMarginRight : CGFloat = 14
    private final let LabelHeight : CGFloat = 9
    private final let LabelMarginLeft : CGFloat = 30
    private final let WidthItemBar : CGFloat = 30
    private final let HeightItemBar : CGFloat = 25
    private final let BrandHeight : CGFloat = 60
    private final let BrandNumberCollumn : CGFloat = 5
    
    var currentPage = 0
    var catCollectionView : UICollectionView!
    var brandCollectionView : UICollectionView!
    var subCatCollectionView : UICollectionView!
    var catSelected = -1
    var cats = [Cat]()
    var subCats = [Cat]()
    var brandSpacingLine : CGFloat = 0
    var catSelectedTemporary = -1
    var contentInsetPrevious = UIEdgeInsets()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        self.navigationController!.navigationBarHidden = false
        self.title = String.localize("LB_CA_DISCOVER")
        
        self.collectionView!.delegate = self
        self.collectionView!.dataSource = self
        self.collectionView!.backgroundColor = UIColor.whiteColor();
        self.collectionView?.registerClass(CatCell.self, forCellWithReuseIdentifier: CatCellId)
        self.collectionView?.registerClass(FeatureImageCell.self, forCellWithReuseIdentifier: FeatureImageCellId)
        self.collectionView!.registerClass(ImageCell.self, forCellWithReuseIdentifier: ImageCellId)
        self.collectionView!.registerClass(ImageCollectCell.self, forCellWithReuseIdentifier: ImageCollectCellId)
        self.collectionView!.registerClass(ExpandedImageCollectCell.self, forCellWithReuseIdentifier: ExpandedImageCollectCellId)
        contentInsetPrevious = UIEdgeInsetsZero
        firstly{
            return self.listCat()
            }.then { _ -> Void in
                self.reloadAllData()
        }
        brandSpacingLine = (self.view.bounds.width - BrandNumberCollumn * BrandHeight) / (BrandNumberCollumn + 1)
        self.createTopView()
    }
    
    func createTopView() {
        let layout: SnapFlowLayout = SnapFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        self.catCollectionView = UICollectionView(frame: CGRect(x: 0, y: 64, width: self.view.bounds.width, height: CatCellHeight), collectionViewLayout: layout)
        self.catCollectionView.delegate = self
        self.catCollectionView.dataSource = self
        self.catCollectionView.backgroundColor = UIColor.whiteColor()
        self.catCollectionView.registerClass(SubCatCell.self, forCellWithReuseIdentifier: SubCatCellId)
        self.view.addSubview(self.catCollectionView)
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        switch collectionView {
        case self.collectionView!:
            return 2
        default:
            return 1
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
            
        case self.collectionView!:
            switch section {
            case 0:
                return 0
            default:
                return self.cats.count
            }
        case self.catCollectionView:
            return 2
            
        case self.brandCollectionView:
            if catSelected >= 0 {
                return self.cats[self.catSelected].categoryBrandMerchantList.count
            }
            return 0
        case self.subCatCollectionView:
            return self.subCats.count
        default:
            return 0
        }
    }
    
    //MARK: Draw Cell
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
            
        case self.collectionView!:
            switch indexPath.section {
            case 1:
                switch indexPath.row {
                case self.catSelected:
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ExpandedImageCollectCellId, forIndexPath: indexPath) as! ExpandedImageCollectCell
                    cell.setImage(self.cats[indexPath.row].categoryImage, imageCategory: .Merchant)
                    cell.brandCollectionView.dataSource = self
                    cell.brandCollectionView.delegate = self
                    cell.label.text = self.cats[indexPath.row].categoryName
                    self.brandCollectionView = cell.brandCollectionView
                    self.brandCollectionView.backgroundColor = UIColor.whiteColor()
                    self.brandCollectionView.registerClass(ImageMarginCollectCell.self, forCellWithReuseIdentifier: ImageMarginCollectCellId)
                    cell.subCatCollectionView.dataSource = self
                    cell.subCatCollectionView.delegate = self
                    self.subCatCollectionView = cell.subCatCollectionView
                    var frame = self.subCatCollectionView.frame
                    frame.size.height = self.getHeightOfSubCatCollectionview()
                    self.subCatCollectionView.frame = frame
                    self.subCatCollectionView.registerClass(SubCatCell.self, forCellWithReuseIdentifier: SubCatCellId)
                    
                    return cell
                default:
                    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ImageCollectCellId, forIndexPath: indexPath) as! ImageCollectCell
                    cell.setImageRoundedCorners(self.cats[indexPath.row].categoryImage, category: .Merchant)
                    cell.label.formatSize(17)
                    cell.label.textColor = UIColor.whiteColor()
                    cell.label.text = self.cats[indexPath.row].categoryName
                    cell.filter.alpha = 0.3
                    
                    cell.accessibilityIdentifier = "discover_cat_image_collection_cell_\(indexPath.row)"
                    
                    return cell
                }
                
            default:
                return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
            }
        case self.catCollectionView:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(SubCatCellId, forIndexPath: indexPath) as! SubCatCell
            switch indexPath.row {
            case 0:
                cell.label.text = String.localize("LB_AC_BRAND")
                cell.label.textColor = UIColor.secondary2()
                cell.label.font = UIFont.boldSystemFontOfSize(14.0)
                cell.imageView.hidden = true
                break
            case 1:
                cell.label.text = String.localize("LB_CA_CATEGORY_BRAND")
                cell.imageView.image = UIImage(named: "underLineBrand")
                cell.label.textColor = UIColor.primary1()
                cell.label.font = UIFont.boldSystemFontOfSize(14.0)
                cell.imageView.hidden = false
                break
            default:
                break
            }
            return cell
        case self.brandCollectionView:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ImageMarginCollectCellId, forIndexPath: indexPath) as! ImageMarginCollectCell
            if self.cats[catSelected].categoryBrandMerchantList[indexPath.row].entity == "Brand"{
            cell.setImage(self.cats[catSelected].categoryBrandMerchantList[indexPath.row].smallLogoImage, category: .Brand)
            } else{
                cell.setImage(self.cats[catSelected].categoryBrandMerchantList[indexPath.row].smallLogoImage, category: .Merchant)
            }
            cell.filter.alpha = 0.0
            
            cell.accessibilityIdentifier = "discover_cat_brand_cell_\(indexPath.section)_\(indexPath.row)"
            
            return cell
            
        case self.subCatCollectionView:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(SubCatCellId, forIndexPath: indexPath) as! SubCatCell
            cell.label.text = self.subCats[indexPath.row].categoryName
            return cell
        default:
            return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
            
        }
    }
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
        return cell
    }
    
    
    //MARK: Collection View Delegate methods
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            switch collectionView {
            case self.collectionView!:
                switch indexPath.section {
                case 1:
                    switch indexPath.row {
                    default:
                        catSelectedTemporary = indexPath.row
                        self.catSelected = -1
                        self.subCats = self.cats[indexPath.row].categoryList!
                        self.reloadAllData()
                        contentInsetPrevious = self.collectionView.contentInset;
                        self.collectionView.contentInset = UIEdgeInsetsMake(contentInsetPrevious.top, contentInsetPrevious.left, ExpandedImageCollectCellHeight + self.getHeightOfSubCatCollectionview(), contentInsetPrevious.right)
                        self.collectionView?.scrollToItemAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
                        NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: Selector("scrollViewDidEndScrollingAnimation:"), userInfo: nil, repeats: false)
                        break
                    }
                    
                default:
                    break
                }
                
            case self.catCollectionView:
                switch indexPath.row {
                case 0:
                    self.navigationController?.pushViewController(DiscoverBrandController(), animated: false)
                    break
                default:
                    break
                }
            case self.brandCollectionView:
                let discoverViewController = DiscoverViewController()
                let styleFilter = StyleFilter()
                styleFilter.cats = [self.cats[catSelected]]
                if self.cats[catSelected].categoryBrandMerchantList[indexPath.row].entity == "Brand"{
                    let brand = Brand()
                    brand.brandId = self.cats[catSelected].categoryBrandMerchantList[indexPath.row].entityId
                    brand.brandName = self.cats[catSelected].categoryBrandMerchantList[indexPath.row].name
                    brand.brandNameInvariant = self.cats[catSelected].categoryBrandMerchantList[indexPath.row].nameInvariant
                    styleFilter.brands = [brand]

                } else {
                    let merchant = Merchant()
                    merchant.merchantId = self.cats[catSelected].categoryBrandMerchantList[indexPath.row].entityId
                    merchant.merchantName = self.cats[catSelected].categoryBrandMerchantList[indexPath.row].name
                    merchant.merchantNameInvariant = self.cats[catSelected].categoryBrandMerchantList[indexPath.row].nameInvariant
                    styleFilter.merchants = [merchant]

                }

                discoverViewController.styleFilter = styleFilter
                self.navigationController?.pushViewController(discoverViewController, animated: false)
                break
            case self.subCatCollectionView:
                switch indexPath.row {
                default:
                    let discoverViewController = DiscoverViewController()
                    let styleFilter = StyleFilter()
                    styleFilter.cats = [self.subCats[indexPath.row]]
                    discoverViewController.styleFilter = styleFilter
                    self.navigationController?.pushViewController(discoverViewController, animated: false)
                    break
                }
                
            default:
                break
            }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch collectionView {
        case self.collectionView!:
            return 0.0
        case self.catCollectionView:
            return Constants.LineSpacing.SubCatCell
        
        case self.brandCollectionView:
            return brandSpacingLine
        case self.subCatCollectionView:
            return 0.0
        default:
            return 0.0
        
        }

    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        switch collectionView {
            
        case self.collectionView!:
            switch section {
            case 0:
                return UIEdgeInsets(top: 0.0, left: 0.0, bottom: CatCellHeight, right: 0.0)
            default:
                return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
            }
        case self.catCollectionView:
            let space = (self.view.frame.width / 2 - Constants.LineSpacing.SubCatCell) / 2
            return  UIEdgeInsets(top: 0.0, left: space, bottom: 0.0, right: space)
            
        case self.brandCollectionView:
             return UIEdgeInsets(top: 0.0, left: brandSpacingLine, bottom: 0.0, right: brandSpacingLine)
        case self.subCatCollectionView:
            return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        default:
             return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
            
        }
    }
    
    //MARK: Item Size Delegate for Collection View
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch collectionView {
                
            case self.collectionView!:
                switch (indexPath.section){
                case 0:
                    return CGSizeMake(self.view.frame.size.width, CatCellHeight)
                default:
                    switch (indexPath.row){
                    case catSelected:
                        return CGSizeMake(self.view.frame.size.width, ExpandedImageCollectCellHeight + self.getHeightOfSubCatCollectionview())
                    default:
                        return CGSizeMake(self.view.frame.size.width, ImageCollectCellHeight)
                        
                    }
                    
                }
            case self.catCollectionView:
                switch (indexPath.row){
                default:
                     return CGSizeMake(self.view.frame.size.width / 4, Constants.Value.CatCellHeight)
                    
                }
                
            case self.brandCollectionView:
                switch (indexPath.row){
                default:
                    return CGSizeMake(BrandHeight, BrandHeight)
                    
                }
            case self.subCatCollectionView:
                switch (indexPath.row){
                default:
                    return CGSizeMake(self.view.frame.size.width / 4, Constants.Value.CatCellHeight)
                }
            default:
                return CGSizeMake(self.view.frame.size.width, 40)
                
            }
    }
    
    
    func reloadAllData(){
        
        UIView.transitionWithView(self.collectionView!,
            duration: 0.2,
            options:.TransitionCrossDissolve,
            animations:
            { () -> Void in
                self.collectionView!.reloadData()
                self.catCollectionView.reloadData()
                if self.brandCollectionView != nil{
                    self.brandCollectionView.reloadData()
                }
                if self.subCatCollectionView != nil {
                    self.subCatCollectionView.reloadData()
                }
            },
            completion: {(Bool)-> Void in
                if self.contentInsetPrevious != UIEdgeInsetsZero {
                    self.collectionView.contentInset = self.contentInsetPrevious
                    self.contentInsetPrevious = UIEdgeInsetsZero
                }
        });
        
    }
    
    
    func listCat() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchCategory(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        var cats = Mapper<Cat>().mapArray(response.result.value) ?? []
                        cats = cats.filter(){ $0.categoryId != 0 }
                        strongSelf.cats = cats
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    //MARK: Menu Button delegates
    func addTapped(sender : UIBarButtonItem){
        Log.debug("Shopping cart Tapped!")
        let shoppingcart = ShoppingCartViewController()
        
        self.navigationController?.pushViewController(shoppingcart, animated: true)
        
    }
    func getHeightOfSubCatCollectionview() -> CGFloat{
        let row = ((self.subCats.count / 4) + ((self.subCats.count % 4) > 0 ? 1 : 0))
        return CGFloat((CGFloat(row) * CatCellHeight))
    }
    func searchIconClicked(){
        let discoverViewController = DiscoverViewController()
        discoverViewController.isSearch = true
        self.navigationController?.pushViewController(discoverViewController, animated: false)
    }
    func initButtonBar(imageName: String, selectorName: String, size:CGSize,left: CGFloat, right: CGFloat) -> UIBarButtonItem
    {
        let button: UIButton = UIButton()
        button.setImage(UIImage(named: imageName), forState: .Normal)
        button.frame = CGRectMake(0, 0, size.width, size.height)
        button.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: left, bottom: 0, right: right)
        button .addTarget(self, action:Selector(selectorName), forControlEvents: UIControlEvents.TouchUpInside)
        
        let temp:UIBarButtonItem = UIBarButtonItem()
        temp.customView = button
        return temp
    }
    
    func buyTapped(){
        let shoppingcart = ShoppingCartViewController()
        self.navigationController?.pushViewController(shoppingcart, animated: true)
    }
    
    func likeTapped(){
        let wishlist = WishListCartViewController()
        self.navigationController?.pushViewController(wishlist, animated: true)
    }
    
    func setupNavigationBar() {
        setupNavigationBarCartButton()
        setupNavigationBarWishlistButton()
        
        var rightButtonItems = [UIBarButtonItem]()
        rightButtonItems.append(UIBarButtonItem(customView: buttonCart!))
        rightButtonItems.append(UIBarButtonItem(customView: buttonWishlist!))
        
        buttonCart!.addTarget(self, action: "buyTapped", forControlEvents: .TouchUpInside)
        buttonWishlist!.addTarget(self, action: "likeTapped", forControlEvents: .TouchUpInside)
        
        buttonCart!.accessibilityIdentifier = "view_cart_button"
        buttonWishlist!.accessibilityIdentifier = "view_wishlist_button"
        
        let viewTitle = UIView(frame: CGRect(x: 50, y: 0, width: self.view.bounds.maxX * 2 / 3, height: 32.5))
        let searchButton = UIButton(type: UIButtonType.Custom)
        searchButton.frame = viewTitle.bounds
        searchButton.addTarget(self, action: Selector("searchIconClicked"), forControlEvents: UIControlEvents.TouchUpInside)
        viewTitle.addSubview(searchButton)
        let searchBarImage = UIImageView(image: UIImage(named: "search_bar"))
        searchBarImage.frame = CGRect(x: 0, y: 0, width: viewTitle.bounds.maxX, height: searchBarImage.bounds.maxY * viewTitle.bounds.maxX / searchBarImage.bounds.maxX)
        searchButton.addSubview(searchBarImage)
        let labelTitle = UILabel(frame: CGRect(x: 25, y: 5, width: viewTitle.bounds.maxX - 55, height: viewTitle.bounds.maxY - 10))
        labelTitle.text = String.localize("LB_CA_SEARCH_PLACEHOLDER")+"?"
        labelTitle.textColor = UIColor.grayColor()
        labelTitle.font = UIFont.boldSystemFontOfSize(14.0)
        searchButton.addSubview(labelTitle)
        self.navigationItem.titleView = viewTitle
        self.title = String.localize("LB_CA_DISCOVER")
        
        self.navigationItem.rightBarButtonItems = rightButtonItems
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    
    //MARK: Scroll View Method to control page control
    func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView) {
        if catSelectedTemporary != -1 {
            self.catSelected = catSelectedTemporary
            catSelectedTemporary = -1
            self.reloadAllData()
        }
    }
}