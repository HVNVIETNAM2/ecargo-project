//
//  File.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 20/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
enum TabIndex: Int {
    case Home = 0
    case Discover
    case MM
    case IM
    case Profile
}
class StorefrontController : UITabBarController, UITabBarControllerDelegate{
    
    func controllerForTab(rootViewController: UIViewController, title: String, image: UIImage?, tag: Int, accessibilityIdentifier: String)  -> UIViewController {
        let item = UITabBarItem(title: title, image: image, tag: tag)
        item.accessibilityIdentifier = accessibilityIdentifier
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.tabBarItem = item
        return navigationController
        
    }
    func controllerForUserTab(rootViewController: UIViewController, title: String, image: UIImage?, tag: Int, accessibilityIdentifier: String)  -> UIViewController {
        let item = UITabBarItem(title: title, image: image, tag: tag)
        item.accessibilityIdentifier = accessibilityIdentifier
        let navigationController = GKFadeNavigationController(rootViewController: rootViewController)
        navigationController.tabBarItem = item
        return navigationController
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.viewControllers = [
            
            controllerForTab(
                HomeViewController(),
                title: String.localize("LB_CA_NEWSFEED"),
                image: UIImage(named: "feed"),
                tag: 0,
                accessibilityIdentifier: "tab_home"
            ),
            
            controllerForTab(
                DiscoverBrandController(),
                title: String.localize("LB_CA_DISCOVER"),
                image: UIImage(named: "discover"),
                tag: 1,
                accessibilityIdentifier: "tab_discover"
            ),
            
            controllerForTab(
                StyleFeedController(),
                title: String.localize("LB_CA_STYLEFEED"),
                image: UIImage(named: "mm"),
                tag: 2,
                accessibilityIdentifier: "tab_stylefeed"
            ),
            
            controllerForTab(
                IMViewController(),
                title: String.localize("LB_CA_MESSENGER"),
                image: UIImage(named: "chat"),
                tag: 3,
                accessibilityIdentifier: "tab_chat"
            ),
            
            controllerForUserTab(
                ProfileViewController(),
                title: String.localize("LB_CA_ME"),
                image: UIImage(named: "profile"),
                tag: 4,
                accessibilityIdentifier: "tab_profile"
            )
            
        ]

    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }

    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        if (!Context.getAuthenticatedUser()) {
            if viewController.isKindOfClass(UINavigationController) {
                let viewControllers =  (viewController as! UINavigationController).viewControllers
                if viewControllers.count > 0 {
                    let className = viewControllers[0].dynamicType.description()
                    switch(className) {
                    case ProfileViewController.description():
                        self .showLogin(.Profile)
                        return false
                    case IMViewController.description():
                        self .showLogin(.IM)
                        return false
                    default:
                        return true
                    }
                }
            }
        }
        return true
    }
    
    func showLogin(signupMode:SignupMode) {
        let loginNavController = UINavigationController()
        let loginViewController = LoginViewController()
        loginViewController.signupMode = signupMode
        loginNavController.viewControllers = [loginViewController]
        loginNavController.modalPresentationStyle = .OverFullScreen
        self.presentViewController(loginNavController, animated: true, completion: nil)
    }

}