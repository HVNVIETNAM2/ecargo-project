//
//  HomeViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 18/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

class HomeViewController : MmViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = String.localize("LB_CA_NEWSFEED")
        view.backgroundColor = UIColor.whiteColor()
    
        self.navigationController?.navigationBarHidden = true
        
        if let path = NSBundle.mainBundle().pathForResource("mockup_newsfeed", ofType: "png") {
            let webView = UIWebView(frame: view.bounds)
            webView.scalesPageToFit = true
            webView.loadRequest(NSURLRequest(URL: NSURL(fileURLWithPath: path)))
            view.addSubview(webView)

        }
    }

    override func shouldHaveCollectionView() -> Bool {
        return false
    }

}