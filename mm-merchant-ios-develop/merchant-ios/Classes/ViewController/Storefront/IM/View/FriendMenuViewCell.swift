//
//  FriendMenuViewCell.swift
//  merchant-ios
//
//  Created by Tony Fung on 4/3/2016.
//  Copyright © 2016年 WWE & CO. All rights reserved.
//

import UIKit

class FriendMenuViewCell: UICollectionViewCell {
    var imageView = UIImageView()
    var textLabel = UILabel()
    var lowerLabel = UILabel()
    var borderView = UIView()
    var upperBorderView = UIView()
    
    
    private final let MarginRight : CGFloat = 20
    private final let MarginLeft : CGFloat = 10
    private final let LabelMarginTop : CGFloat = 11
    private final let LabelMarginRight : CGFloat = 30
    private final let ImageWidth : CGFloat = 44
    private final let LabelRightWidth : CGFloat = 100
    private final let LabelLowerMarginTop : CGFloat = 33
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        textLabel.formatSize(15)
        addSubview(textLabel)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        upperBorderView.backgroundColor = UIColor.secondary1()
        addSubview(upperBorderView)
        
        textLabel.textColor = UIColor.secondary2()
        lowerLabel.textColor = UIColor.secondary3()
        lowerLabel.font = UIFont(name: lowerLabel.font.fontName, size: 11)
        lowerLabel.lineBreakMode = .ByWordWrapping
        lowerLabel.numberOfLines = 0
        addSubview(lowerLabel)

        textLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 15)
        
        imageView.contentMode = .ScaleToFill
        addSubview(imageView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        imageView.frame = CGRect(x: bounds.minX + MarginLeft, y: bounds.midY - ImageWidth / 2, width: ImageWidth, height: ImageWidth)
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        upperBorderView.frame = CGRect(x: bounds.minX, y: bounds.minY - 1, width: bounds.width, height: 1)
        
        if lowerLabel.text != nil && lowerLabel.text?.length > 0 {
            textLabel.frame = CGRect(x: imageView.frame.maxX + MarginLeft, y: bounds.minY + LabelMarginTop, width: bounds.width - (imageView.frame.maxX + MarginRight * 2) , height: bounds.height/3 )
            lowerLabel.frame = CGRect(x: imageView.frame.maxX + MarginLeft, y: bounds.midY + 3 , width: bounds.width - (imageView.frame.maxX + MarginRight * 2) , height:bounds.height/3 )
        }else {
            textLabel.frame = CGRect(x: imageView.frame.maxX + MarginLeft, y: bounds.midY - bounds.height / 6, width: bounds.width - (imageView.frame.maxX + MarginRight * 2) , height: bounds.height/3 )
        }
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
