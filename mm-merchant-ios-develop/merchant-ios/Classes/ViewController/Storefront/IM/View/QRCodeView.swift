//
//  QRCodeView.swift
//  merchant-ios
//
//  Created by Tony Fung on 7/3/2016.
//  Copyright © 2016年 WWE & CO. All rights reserved.
//

import UIKit
import QRCode
import AlamofireImage

class QRCodeView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    private var profileImageSize = CGSizeMake(70, 70)
    
    
    var profileImageView = UIImageView()
    var qrImageView = UIImageView()
    var qrLogoImageView = UIImageView()
    var userNameLabel = UILabel()
    var instructionLabel = UILabel()
    private var holderView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

 
        profileImageView.layer.cornerRadius = profileImageSize.height / 2
        profileImageView.layer.borderWidth = 2.0
        profileImageView.layer.borderColor = UIColor.whiteColor().CGColor
        profileImageView.clipsToBounds = true
        profileImageView.image = UIImage(named: "deflaut_profile_icon")
        profileImageView.backgroundColor = UIColor.whiteColor()
        
        instructionLabel.text = String.localize("LB_CA_IM_MY_QRCODE_NOTE")
        instructionLabel.formatSize(12)
        instructionLabel.textAlignment = .Center
        
        userNameLabel.formatSize(15)
        userNameLabel.textAlignment = .Center
        
        holderView.backgroundColor = UIColor.whiteColor()
        holderView.layer.cornerRadius = 10
        self.addSubview(holderView)
        
        holderView.addSubview(qrImageView)
        holderView.addSubview(userNameLabel)
        holderView.addSubview(instructionLabel)
        
//        holderView.pointInside(<#T##point: CGPoint##CGPoint#>, withEvent: <#T##UIEvent?#>)
        
        self.addSubview(profileImageView)
        

        qrLogoImageView.hidden = true
        qrLogoImageView.image = UIImage(named: "QR_MMicon")
        holderView.addSubview(qrLogoImageView)
        
//        self.userInteractionEnabled = false
//        self.holderView.userInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private let userNameLabelSize = CGSizeMake(260, 40)
    private let qrCodeImageSize = CGSizeMake(220, 220)
    private let qrLogoImageSize = CGSizeMake(40, 40)
    
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImageView.frame = CGRect(x: bounds.midX - profileImageSize.width/2, y: bounds.minY, width: profileImageSize.width, height: profileImageSize.height)
        holderView.frame = CGRect(x: bounds.minX, y: bounds.minY + profileImageSize.height / 2, width: bounds.width, height: bounds.height - profileImageSize.height)
        userNameLabel.frame = CGRect(x: (holderView.bounds.width - userNameLabelSize.width)/2, y: holderView.bounds.minY + profileImageSize.height/2 + 5, width: userNameLabelSize.width, height: userNameLabelSize.height)
        qrImageView.frame = CGRect(x: bounds.midX - qrCodeImageSize.width/2, y: userNameLabel.frame.maxY + 5, width: qrCodeImageSize.width, height: qrCodeImageSize.height)
        instructionLabel.frame = CGRect(x: holderView.bounds.minX + 10, y: holderView.bounds.maxY - 40, width: holderView.bounds.width - 20, height: 30)
        
        qrLogoImageView.frame = CGRect(x: 0, y: 0, width: qrLogoImageSize.width, height: qrLogoImageSize.height)
        qrLogoImageView.center = qrImageView.center
    }
    
    func configUser(user: User){
        userNameLabel.text = user.displayName
        var qrCode = QRCode(EntityURLFactory.userURL(user).absoluteString)
        qrCode?.color = CIColor(rgba: "ed2247")
        qrImageView.image = qrCode?.image
        setProfileImage(user.profileImage)
        qrLogoImageView.hidden = false
    }
    
    func setProfileImage(key : String){
        let filter = AspectScaledToFitSizeFilter(
            size: profileImageView.frame.size
        )
        profileImageView.af_setImageWithURL(ImageURLFactory.get(key, category: .User), placeholderImage : UIImage(named: "profile_avatar"), filter: filter, imageTransition: .CrossDissolve(0.3))
    }
    
    
}
