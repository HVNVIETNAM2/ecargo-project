//
//  IMViewCell.swift
//  merchant-ios
//
//  Created by HVN_Pivotal on 3/3/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import AlamofireImage
protocol FriendListViewCellDelegate{
    func chatClicked(rowIndex: Int)
    func acceptClicked(rowIndex: Int)
    func deleteClicked(rowIndex: Int)
}
class FriendListViewCell : UICollectionViewCell{
    var imageView = UIImageView()
    var upperLabel = UILabel()
    var borderView = UIView()
    var diamondImageView = UIImageView()
    var rightLabel = UILabel()
    var chatView = UIView()
    var buttonView = UIView()
    var chatButton = UIButton()
    var acceptButton = UIButton()
    var deleteButton = UIButton()
    private final let MarginRight : CGFloat = 20
    private final let MarginLeft : CGFloat = 15
    private final let LabelMarginTop : CGFloat = 15
    private final let LabelMarginRight : CGFloat = 30
    private final let ImageWidth : CGFloat = 40
    private final let ImageDiamondWidth : CGFloat = 16
    private final let LabelRightWidth : CGFloat = 100
    private final let LabelLowerMarginTop : CGFloat = 33
    private final let ButtonHeight : CGFloat = 44
    private final let ButtonWidth : CGFloat = 44
    private final let ChatButtonWidth : CGFloat = 30
    var friendListViewCellDelegate: FriendListViewCellDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = UIColor.primary1().CGColor
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = ImageWidth / 2
        addSubview(imageView)
        upperLabel.formatSize(15)
        addSubview(upperLabel)
        diamondImageView.image = UIImage(named: "curator_diamond")
        addSubview(diamondImageView)
        addSubview(diamondImageView)
        rightLabel.formatSize(12)
        rightLabel.textAlignment = .Right
        addSubview(rightLabel)
        chatButton.setImage(UIImage(named: "chat_on"), forState: UIControlState.Normal)
        chatButton.addTarget(self, action: "chatClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        chatView.addSubview(chatButton)
        chatView.backgroundColor = UIColor.whiteColor()
        addSubview(chatView)
        acceptButton.addTarget(self, action: "acceptClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        acceptButton.setImage(UIImage(named: "accept_btn"), forState: .Normal)
        deleteButton.addTarget(self, action: "deleteClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        deleteButton.setImage(UIImage(named: "reject_btn"), forState: .Normal)
        buttonView.addSubview(acceptButton)
        buttonView.addSubview(deleteButton)
        buttonView.backgroundColor = UIColor.whiteColor()
        addSubview(buttonView)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        layoutSubviews()
    }
    
    private let mainLabelHeight :CGFloat = 30.0
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: bounds.minX + MarginLeft, y: bounds.midY - ImageWidth / 2, width: ImageWidth, height: ImageWidth)
        
        upperLabel.frame = CGRect(x: imageView.frame.maxX + MarginRight, y: bounds.minY, width: bounds.width - (imageView.frame.maxX + MarginRight + LabelRightWidth + MarginRight) , height: bounds.height)
        
        
        rightLabel.frame = CGRect(x: upperLabel.frame.maxX , y: bounds.minY , width: LabelRightWidth, height:bounds.height)
        chatView.frame = CGRect(x: upperLabel.frame.maxX , y: bounds.minY , width: LabelRightWidth, height:bounds.height)
        chatButton.frame = CGRect(x: chatView.frame.width - ChatButtonWidth , y: (chatView.frame.height - ButtonHeight) / 2 , width: ChatButtonWidth, height:ButtonHeight)
        buttonView.frame = CGRect(x: bounds.maxX - LabelRightWidth, y: bounds.minY , width: LabelRightWidth, height:bounds.height)
        deleteButton.frame = CGRect(x: buttonView.frame.width - ButtonWidth - 10 , y: (buttonView.frame.height - ButtonHeight) / 2 , width: ButtonWidth, height:ButtonHeight)
        acceptButton.frame = CGRect(x: buttonView.frame.width - (ButtonWidth * 2 + 10) , y: (buttonView.frame.height - ButtonHeight) / 2 , width: ButtonWidth, height:ButtonHeight)
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        diamondImageView.frame = CGRect(x: imageView.frame.maxX - (ImageDiamondWidth - 2), y: imageView.frame.minY - 5, width: ImageDiamondWidth, height: ImageDiamondWidth)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func chatClicked(sender: UIButton) {
        if self.friendListViewCellDelegate != nil {
            self.friendListViewCellDelegate?.chatClicked(sender.tag)
        }
    }
    
    func acceptClicked(sender: UIButton) {
        if self.friendListViewCellDelegate != nil {
            self.friendListViewCellDelegate?.acceptClicked(sender.tag)
        }
    }
    
    func deleteClicked(sender: UIButton) {
        if self.friendListViewCellDelegate != nil {
            self.friendListViewCellDelegate?.deleteClicked(sender.tag)
        }
    }
    
    func setImage(imageKey : String, category : ImageCategory){
        let filter = AspectScaledToFitSizeFilter(
            size: imageView.frame.size
        )
        imageView.af_setImageWithURL(ImageURLFactory.get(imageKey, category: category), placeholderImage : UIImage(named: "holder"), filter: filter, imageTransition: .CrossDissolve(0.3))
    }
    
    func setData (user: User, isFriend: Bool) {
        if isFriend {
            chatView.hidden = false
            buttonView.hidden = true
        } else {
            chatView.hidden = true
            if user.friendStatus.length > 0 {
                buttonView.hidden = true
                rightLabel.text = user.friendStatus
            } else {
                buttonView.hidden = false
            }
        }
        if user.IsCuratorUser == 1 {
            diamondImageView.hidden = false
            imageView.layer.borderWidth = 1.0
        } else {
            diamondImageView.hidden = true
            imageView.layer.borderWidth = 0.0
        }
    }
}

