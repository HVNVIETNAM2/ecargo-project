//
//  IMViewCell.swift
//  merchant-ios
//
//  Created by HVN_Pivotal on 3/3/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import AlamofireImage

class IMViewCell : SwipeActionMenuCell{
    var imageView = UIImageView()
    var upperLabel = UILabel()
    var lowerLabel = UILabel()
    var borderView = UIView()
    var diamondImageView = UIImageView()
    var rightLabel = UILabel()
    private final let MarginRight : CGFloat = 20
    private final let MarginLeft : CGFloat = 15
    private final let LabelMarginTop : CGFloat = 15
    private final let LabelMarginRight : CGFloat = 30
    private final let ImageWidth : CGFloat = 44
    private final let ImageDiamondWidth : CGFloat = 16
    private final let LabelRightWidth : CGFloat = 100
    private final let LabelLowerMarginTop : CGFloat = 33
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        imageView.layer.borderWidth = 1.0
        imageView.clipsToBounds = true
        imageView.contentMode = .ScaleAspectFit
        imageView.layer.borderColor = UIColor.primary1().CGColor
        contentView.addSubview(imageView)
        upperLabel.formatSize(15)
        contentView.addSubview(upperLabel)
        lowerLabel.formatSize(12)
        contentView.addSubview(lowerLabel)
        borderView.backgroundColor = UIColor.secondary1()
        contentView.addSubview(borderView)
        diamondImageView.image = UIImage(named: "curator_diamond")
        contentView.addSubview(diamondImageView)
        contentView.addSubview(diamondImageView)
        rightLabel.formatSize(12)
        rightLabel.textAlignment = .Right
        contentView.addSubview(rightLabel)
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: bounds.minX + MarginLeft, y: bounds.midY - ImageWidth / 2, width: ImageWidth, height: ImageWidth)
        upperLabel.frame = CGRect(x: imageView.frame.maxX + MarginRight, y: bounds.minY + LabelMarginTop, width: bounds.width - (imageView.frame.maxX + MarginRight + LabelRightWidth + MarginRight) , height: bounds.height/3 )
        lowerLabel.frame = CGRect(x: imageView.frame.maxX + MarginRight, y: LabelLowerMarginTop, width: bounds.width - (imageView.frame.maxX + MarginRight * 2) , height:bounds.height/3 )
        rightLabel.frame = CGRect(x: upperLabel.frame.maxX , y: bounds.minY + LabelMarginTop, width: LabelRightWidth, height:bounds.height/3 )
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        diamondImageView.frame = CGRect(x: imageView.frame.maxX - (ImageDiamondWidth - 2), y: imageView.frame.minY - 5, width: ImageDiamondWidth, height: ImageDiamondWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImage(key : String, imageCategory : ImageCategory ){
        let filter = AspectScaledToFitSizeFilter(
            size: imageView.frame.size
        )
        imageView.af_setImageWithURL(ImageURLFactory.get(key, category: imageCategory), placeholderImage : UIImage(named: "holder"), filter: filter)
    }
    func configImage (type: Int) {
        //TODO will be define later with real data
        let temp = type % 4
        if temp == 0 {
            diamondImageView.hidden = true
            imageView.layer.cornerRadius = imageView.frame.height / 2
            imageView.layer.borderWidth = 0.0
        } else if temp == 1 {
            diamondImageView.hidden = false
            imageView.layer.cornerRadius = imageView.frame.height / 2
            imageView.layer.borderWidth = 1.0
        } else if temp == 2{
            diamondImageView.hidden = true
            imageView.layer.cornerRadius = 3
            imageView.layer.borderWidth = 0.0
        } else {
            diamondImageView.hidden = true
            imageView.layer.cornerRadius = 0
            imageView.layer.borderWidth = 0.0
        }
    }
}

