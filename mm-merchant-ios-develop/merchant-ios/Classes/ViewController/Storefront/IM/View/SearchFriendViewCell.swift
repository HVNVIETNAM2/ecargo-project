//
//  SearchFriendViewCell.swift
//  merchant-ios
//
//  Created by HVN_Pivotal on 3/8/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import AlamofireImage

protocol SearchFriendViewCellDelegate{
    func addFriendClicked(rowIndex: Int)
    func followClicked(rowIndex: Int)
}
class SearchFriendViewCell : SwipeActionMenuCell{
    var imageView = UIImageView()
    var upperLabel = UILabel()
    var borderView = UIView()
    var diamondImageView = UIImageView()
    var buttonView = UIView()
    var addFriendButton = UIButton()
    var addFollowButton = UIButton()
    var iconAddFriend = UIImageView()
    var iconAddFollow = UIImageView()
    var searchFriendViewCellDelegate: SearchFriendViewCellDelegate?
    private final let MarginRight : CGFloat = 20
    private final let MarginLeft : CGFloat = 15
    private final let LabelMarginTop : CGFloat = 15
    private final let LabelMarginRight : CGFloat = 30
    private final let ImageWidth : CGFloat = 44
    private final let ImageDiamondWidth : CGFloat = 16
    private final let LabelRightWidth : CGFloat = 100
    private final let LabelLowerMarginTop : CGFloat = 33
    private final let ButtonHeight : CGFloat = 30
    private final let ButtonWidth : CGFloat = 95
    private final let IconAddWidth : CGFloat = 21
    private final let Padding : CGFloat = 5
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
        backgroundColor = UIColor.whiteColor()
        imageView.layer.borderWidth = 1.0
        imageView.clipsToBounds = true
        imageView.contentMode = .ScaleAspectFit
        imageView.layer.borderColor = UIColor.primary1().CGColor
        addSubview(imageView)
        upperLabel.formatSize(15)
        addSubview(upperLabel)
        
        diamondImageView.image = UIImage(named: "curator_diamond")
        addSubview(diamondImageView)
        addSubview(diamondImageView)
        
        iconAddFriend.image = UIImage(named: "icon_add_red")
        addFriendButton.addTarget(self, action: "addFriendClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        addFriendButton.setTitle(String.localize("LB_CA_ADD_FRIEND"), forState: .Normal)
        addFriendButton.setTitleColor(UIColor.secondary2(), forState: .Normal)
        addFriendButton.contentEdgeInsets = UIEdgeInsetsMake(0, IconAddWidth + Padding, 0, 0);
        addFriendButton.titleLabel!.font = UIFont.systemFontOfSize(12)
        addFriendButton.titleLabel!.minimumScaleFactor = 0.5
        addFriendButton.titleLabel!.adjustsFontSizeToFitWidth = true
        addFriendButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        addFriendButton.addSubview(iconAddFriend)
        
        
        iconAddFollow.image = UIImage(named: "icon_add_red")
        addFollowButton.addTarget(self, action: "followClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        addFollowButton.setTitle(String.localize("LB_CA_FOLLOW"), forState: UIControlState.Normal)
        addFollowButton.setTitleColor(UIColor.secondary2(), forState: .Normal)
        addFollowButton.contentEdgeInsets = UIEdgeInsetsMake(0, IconAddWidth + Padding, 0, 0);
        addFollowButton.titleLabel!.font = UIFont.systemFontOfSize(12)
        addFollowButton.titleLabel!.adjustsFontSizeToFitWidth = true
        addFollowButton.titleLabel!.minimumScaleFactor = 0.5
        addFollowButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        addFollowButton.addSubview(iconAddFollow)
        
        buttonView.addSubview(addFriendButton)
        buttonView.addSubview(addFollowButton)
        buttonView.backgroundColor = UIColor.whiteColor()
        addSubview(buttonView)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRect(x: bounds.minX + MarginLeft, y: bounds.midY - ImageWidth / 2, width: ImageWidth, height: ImageWidth)
        upperLabel.frame = CGRect(x: imageView.frame.maxX + MarginRight, y: bounds.minY, width: bounds.width * 2 / 5 - (imageView.frame.maxX + MarginRight) , height: bounds.height)
        
        buttonView.frame = CGRect(x: upperLabel.frame.maxX , y: bounds.minY , width: (bounds.width - MarginLeft * 2) * 3 / 5, height:bounds.height )
        iconAddFriend.frame = CGRect(x: 0 , y: (ButtonHeight - IconAddWidth) / 2 , width: IconAddWidth, height:IconAddWidth)
        iconAddFollow.frame = CGRect(x: 0 , y: (ButtonHeight - IconAddWidth) / 2 , width: IconAddWidth, height:IconAddWidth)
        
        var followWidth = self.getTextWidth(addFollowButton.titleLabel!.text!, height: ButtonHeight, font: addFollowButton.titleLabel!.font) + (self.iconAddFollow.hidden ? 0 : IconAddWidth + Padding)
        if followWidth > ButtonWidth {
            followWidth = ButtonWidth
        }
        addFollowButton.frame = CGRect(x: buttonView.frame.width - followWidth , y: (buttonView.frame.height - ButtonHeight) / 2 , width: followWidth, height:ButtonHeight)
        
        var friendWidth = self.getTextWidth(addFriendButton.titleLabel!.text!, height: ButtonHeight, font: addFriendButton.titleLabel!.font) + (self.iconAddFriend.hidden ? 0 : IconAddWidth + Padding)
        if friendWidth > (ButtonWidth * 2) - followWidth {
            friendWidth = (ButtonWidth * 2) - followWidth
        }
        addFriendButton.frame = CGRect(x: addFollowButton.frame.minX - (friendWidth + 5) , y: (buttonView.frame.height - ButtonHeight) / 2 , width: friendWidth, height:ButtonHeight)
       
        borderView.frame = CGRect(x: bounds.minX, y: bounds.maxY - 1, width: bounds.width, height: 1)
        diamondImageView.frame = CGRect(x: imageView.frame.maxX - (ImageDiamondWidth - 2), y: imageView.frame.minY - 5, width: ImageDiamondWidth, height: ImageDiamondWidth)
    }
    
    func setButtonFrame(user:User){
        var followWidth = self.getTextWidth(user.followStatus, height: ButtonHeight, font: addFollowButton.titleLabel!.font) + (self.iconAddFollow.hidden ? 0 : IconAddWidth + Padding)
        if followWidth > ButtonWidth {
            followWidth = ButtonWidth
        }
        addFollowButton.frame = CGRect(x: buttonView.frame.width - followWidth , y: (buttonView.frame.height - ButtonHeight) / 2 , width: followWidth, height:ButtonHeight)
        var friendWidth = self.getTextWidth(user.friendStatus, height: ButtonHeight, font: addFriendButton.titleLabel!.font) + (self.iconAddFriend.hidden ? 0 : IconAddWidth + Padding)
        if friendWidth > (ButtonWidth * 2) - followWidth {
            friendWidth = (ButtonWidth * 2) - followWidth
        }
        addFriendButton.frame = CGRect(x: addFollowButton.frame.minX - (friendWidth + 5) , y: (buttonView.frame.height - ButtonHeight) / 2 , width: friendWidth, height:ButtonHeight)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImage(key : String, imageCategory : ImageCategory ){
        let filter = AspectScaledToFitSizeFilter(
            size: imageView.frame.size
        )
        imageView.af_setImageWithURL(ImageURLFactory.get(key, category: imageCategory), placeholderImage : UIImage(named: "holder"), filter: filter)
    }
    
    func getTextWidth(text: String, height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.width
    }
    
    func addFriendClicked(sender: UIButton) {
        if self.searchFriendViewCellDelegate != nil {
            self.searchFriendViewCellDelegate?.addFriendClicked(sender.tag)
        }
    }
    
    func followClicked(sender: UIButton) {
        if self.searchFriendViewCellDelegate != nil {
            self.searchFriendViewCellDelegate?.followClicked(sender.tag)
        }
    }
    
    func setData(user:User){
        self.diamondImageView.hidden = true
        self.imageView.layer.borderWidth = 0.0
        if user.IsCuratorUser == 1 {
            self.diamondImageView.hidden = false
            self.imageView.layer.cornerRadius = self.imageView.frame.height / 2
            self.imageView.layer.borderWidth = 1.0
        } else {
            self.diamondImageView.hidden = true
            self.imageView.layer.borderWidth = 0.0
            if user.merchantId != 0 {
                self.imageView.layer.cornerRadius = 0
            } else {
                self.imageView.layer.cornerRadius = self.imageView.frame.height / 2
            }
        }
        self.upperLabel.text = user.displayName
        self.setImage(user.profileImage, imageCategory: .User)
        if user.friendStatus.length == 0 {
            user.friendStatus = String.localize("LB_CA_ADD_FRIEND")
        }
        if user.followStatus.length == 0 {
            user.followStatus = String.localize("LB_CA_FOLLOW")
        }
        if  user.friendStatus == String.localize("LB_CA_ADD_FRIEND"){
            self.addFriendButton.setTitle(String.localize("LB_CA_ADD_FRIEND"), forState: .Normal)
            self.iconAddFriend.hidden = false
            iconAddFriend.image = UIImage(named: "icon_add_red")
            addFriendButton.contentEdgeInsets = UIEdgeInsetsMake(0, IconAddWidth + Padding, 0, 0);
        } else if user.friendStatus == String.localize("LB_CA_FRD_REQ_CANCEL") {
            self.addFriendButton.setTitle(String.localize("LB_CA_FRD_REQ_CANCEL"), forState: .Normal)
            self.iconAddFriend.hidden = false
            iconAddFriend.image = UIImage(named: "icon_cancel_friend")
            addFriendButton.contentEdgeInsets = UIEdgeInsetsMake(0, IconAddWidth + Padding, 0, 0);
        } else {
            self.addFriendButton.setTitle(String.localize("LB_CA_BEFRIENDED"), forState: .Normal)
            self.iconAddFriend.hidden = true;
            addFriendButton.contentEdgeInsets = UIEdgeInsetsZero
        }
        
        if user.followStatus.length == 0 || user.followStatus == String.localize("LB_CA_FOLLOW"){
            addFollowButton.setTitle(String.localize("LB_CA_FOLLOW"), forState: .Normal)
            self.iconAddFollow.hidden = false
            addFollowButton.contentEdgeInsets = UIEdgeInsetsMake(0, IconAddWidth + Padding, 0, 0);
        } else {
            addFollowButton.setTitle(String.localize("LB_CA_FOLLOWED"), forState: .Normal)
            self.iconAddFollow.hidden = true;
            addFollowButton.contentEdgeInsets = UIEdgeInsetsZero;
        }
        self.setButtonFrame(user)
    }

}

