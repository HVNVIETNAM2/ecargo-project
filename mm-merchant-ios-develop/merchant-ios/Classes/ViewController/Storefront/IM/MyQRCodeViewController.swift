//
//  MyQRCodeViewController.swift
//  merchant-ios
//
//  Created by Tony Fung on 4/3/2016.
//  Copyright © 2016年 WWE & CO. All rights reserved.
//

import UIKit
import PromiseKit
import ObjectMapper

class MyQRCodeViewController: UIViewController {

    var user = User()
    var qrCodeView : QRCodeView!

    
    private var qrCodeSize = CGSizeMake(300, 420)
//    private var tapRecognizer = UITapGestureRecognizer(target: self, action: "tappedDismiss:")
    private var dismissButton = UIButton(type: UIButtonType.Custom)
    
    
    static func presentQRCodeController(fromViewController: UIViewController){
        let qrCodeViewController = MyQRCodeViewController()
        qrCodeViewController.modalPresentationStyle = .OverFullScreen
        qrCodeViewController.modalTransitionStyle = .CrossDissolve
        fromViewController.presentViewController(qrCodeViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupSubViews()
        updateUserView()
        
    }

    
    func setupSubViews(){

        dismissButton.frame = self.view.bounds
        self.view.addSubview(dismissButton)
        dismissButton.addTarget(self, action: "dismissClicked:", forControlEvents: UIControlEvents.TouchDown)
        
        qrCodeView = QRCodeView(frame: CGRect(x: (self.view.frame.width - qrCodeSize.width)/2 , y: (self.view.frame.height - qrCodeSize.height)/2, width: qrCodeSize.width, height: qrCodeSize.height))
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)  
        self.view.addSubview(qrCodeView)
        
    }
    
    func dismissClicked(sender: UIControl){
        Log.debug(sender)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadUI(){
        qrCodeView.configUser(user)
    }
    
    func updateUserView(){
        if(Context.getAuthenticatedUser()){
            firstly{
                return fetchUser()
                }.then { _ -> Void in
                    self.reloadUI()
            }
        }
    }
    

    
    func fetchUser() -> Promise<AnyObject>{
        return Promise{ fulfill, reject in
            UserService.view(Context.getUserId()){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            //Log.debug(String(data: response.data!, encoding: 4))
                            strongSelf.user = Mapper<User>().map(response.result.value) ?? User()
                            fulfill("OK")
                        } else {
                            reject(response.result.error!)
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    } else {
                        reject(response.result.error!)
                        strongSelf.handleApiResponseError(response, reject: reject)
                    }
                }
            }
        }
    }

}
