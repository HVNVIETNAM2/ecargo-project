//
//  FriendListViewController.swift
//  merchant-ios
//
//  Created by HVN_Pivotal on 3/4/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import AlamofireImage
class FriendListViewController : MmViewController, UISearchBarDelegate, FriendListViewCellDelegate{
    private final let CellHeight : CGFloat = 60
    private final let CatCellHeight : CGFloat = 40
    private final let FriendStatusAccepted : String = String.localize("LB_CA_IM_ACCEPTED")
    private final let FriendStatusDeleted : String = String.localize("LB_CA_IM_DELETED")
    private let SearchBarHeight : CGFloat = 40
    private var dataSource: [User] = []
    private var friends: [User] = []
    private var friendRequests: [User] = []
    private var isShowingFriendList : Bool = true
    private var catCollectionView : UICollectionView!
    var searchBar = UISearchBar()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = String.localize("LB_CA_FRIEND_LIST")
        self.view.backgroundColor = UIColor.whiteColor()
        self.collectionView!.registerClass(FriendListViewCell.self, forCellWithReuseIdentifier: "FriendListViewCell")
        self.createBackButton()
        self.createTopView()
        self.createRightBarButton()
        self.getDataSource()
    }
    
    //MARK: Collection View methods and delegates
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch (collectionView) {
        case self.collectionView:
            return self.dataSource.count
        default:
            return 2
        }
    }
    
    func createRightBarButton() {
        let rightButton = UIButton(type: UIButtonType.Custom)
        rightButton.setTitle("", forState: .Normal)
        rightButton.setImage(UIImage(named: "im_add_friend"), forState: UIControlState.Normal)
        rightButton.frame = CGRect(x: 0, y: 0, width: 36, height: 24)
        rightButton.addTarget(self, action: "addFriendClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    func getPaddingTop() -> CGFloat {
        return self.navigationController!.navigationBar.frame.maxY
    }
    
    func createTopView() {
        let paddingTop = self.getPaddingTop()
        let layout: SnapFlowLayout = SnapFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        self.catCollectionView = UICollectionView(frame: CGRect(x: 0, y: paddingTop, width: self.view.bounds.width, height: CatCellHeight), collectionViewLayout: layout)
        self.catCollectionView.delegate = self
        self.catCollectionView.dataSource = self
        self.catCollectionView.backgroundColor = UIColor.whiteColor()
        self.catCollectionView.registerClass(SubCatCell.self, forCellWithReuseIdentifier: "SubCatCell")
        self.view.addSubview(self.catCollectionView)
        self.searchBar.sizeToFit()
        self.searchBar.delegate = self
        self.searchBar.searchBarStyle = UISearchBarStyle.Default
        self.searchBar.showsCancelButton = false
        self.searchBar.frame = CGRect(x: self.view.bounds.minX, y: paddingTop + CatCellHeight, width: self.view.bounds.width, height: SearchBarHeight)
        self.searchBar.placeholder = String.localize("LB_CA_SEARCH")
        self.view.addSubview(self.searchBar)
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch (collectionView) {
        case self.collectionView:
            let user = self.dataSource[indexPath.row]
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FriendListViewCell", forIndexPath: indexPath) as! FriendListViewCell
            cell.upperLabel.text = dataSource[indexPath.row].displayName
            cell.setImage(self.dataSource[indexPath.row].profileImage, category: .User)
            cell.chatButton.tag = indexPath.row
            cell.acceptButton.tag = indexPath.row
            cell.deleteButton.tag = indexPath.row
            cell.friendListViewCellDelegate = self
            cell.setData(user, isFriend: isShowingFriendList)
            return cell
        default :
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("SubCatCell", forIndexPath: indexPath) as! SubCatCell
            switch indexPath.row {
            case 0:
                cell.label.text = String.localize("LB_CA_ALL")
                cell.label.font = UIFont.boldSystemFontOfSize(14.0)
                if isShowingFriendList {
                    cell.imageView.hidden = false
                    cell.label.textColor = UIColor.primary1()
                    cell.imageView.image = UIImage(named: "underLineBrand")
                } else {
                    cell.imageView.hidden = true
                    cell.label.textColor = UIColor.secondary2()
                }
                break
            case 1:
                cell.label.text = String.localize("LB_CA_FRIENDS_REQUEST")
                cell.label.font = UIFont.boldSystemFontOfSize(14.0)
                if isShowingFriendList {
                    cell.imageView.hidden = true
                    cell.label.textColor = UIColor.secondary2()
                } else {
                    cell.imageView.hidden = false
                    cell.label.textColor = UIColor.primary1()
                    cell.imageView.image = UIImage(named: "underLineBrand")
                }
                break
            default:
                break
            }
            
            return cell
            
        }
        
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch (collectionView) {
            case self.collectionView:
                return CGSizeMake(self.view.frame.size.width , CellHeight)
            default:
                return CGSizeMake(self.view.frame.size.width / 4, Constants.Value.CatCellHeight)
            }
            
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            switch (collectionView) {
            case self.collectionView:
                return UIEdgeInsets(top: CatCellHeight + SearchBarHeight, left: 0.0, bottom: 0.0, right: 0.0)
            default:
                let space = (self.view.frame.width / 2 - Constants.LineSpacing.SubCatCell) / 2
                return  UIEdgeInsets(top: 0.0, left: space, bottom: 0.0, right: space)
            }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch collectionView {
        case self.collectionView!:
            return 0.0
        default:
            return Constants.LineSpacing.SubCatCell
        }
    }
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            switch collectionView {
            case self.collectionView!:
                let publicProfileVC = ProfileViewController()
                publicProfileVC.currentType = TypeProfile.Public
                publicProfileVC.publicUser = self.friends[indexPath.row]
                self.navigationController?.pushViewController(publicProfileVC, animated: true)
                break
            default:
                if indexPath.row == 0 {
                    if !isShowingFriendList {
                        isShowingFriendList = true
                        self.reloadDataSource()
                        self.getDataSource()
                    } else {
                        isShowingFriendList = true
                    }
                } else {
                    if isShowingFriendList {
                        isShowingFriendList = false
                        self.reloadDataSource()
                        self.getDataSource()
                    } else {
                        isShowingFriendList = false
                    }
                }
                break
            }
         self.view.endEditing(true)
    }
    
    func addFriendClicked (sender : UIBarButtonItem){
        self.navigationController?.pushViewController(AddFriendViewController(), animated: true)
    }
    
    func reloadDataSource() {
        if isShowingFriendList {
            self.dataSource = self.friends
        } else {
            self.dataSource = self.friendRequests
        }
        self.collectionView.reloadData()
        self.catCollectionView.reloadData()
    }
    
    func getDataSource() {
        if isShowingFriendList {
            self.loadFriend()
        } else {
            self.loadFriendRequest()
        }
    }
//    // config tab bar
//    override func shouldHideTabBar() -> Bool {
//        return  true
//    }
    
    // MARK: UISearchBarDelegate
    private func filter(text : String!) {
        if isShowingFriendList {
            self.dataSource = self.friends.filter(){ ($0.displayName).lowercaseString.rangeOfString(text.lowercaseString) != nil }
        } else {
            self.dataSource = self.friendRequests.filter(){ ($0.displayName).lowercaseString.rangeOfString(text.lowercaseString) != nil }
        }
        self.collectionView.reloadData()
    }
    
    internal func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.filter(searchBar.text!)
        searchBar.resignFirstResponder()
    }
    
    internal func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.length == 0 {
            self.reloadDataSource()
        } else {
            self.filter(searchBar.text!)
        }
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        
        searchBar.showsCancelButton = true
        styleCancelButton(true)
        return true
    }
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = false
        return true
    }
    func styleCancelButton(enable: Bool){
        if enable {
            if let _cancelButton = searchBar.valueForKey("_cancelButton"),
                let cancelButton = _cancelButton as? UIButton {
                    cancelButton.enabled = enable //comment out if you want this button disabled when keyboard is not visible
                    if title != nil {
                        cancelButton.setTitle(String.localize("LB_CANCEL"), forState: UIControlState.Normal)
                    }
            }
        }
    }
    func loadFriend() {
        self.showLoading()
        firstly{
            return self.listFriend()
            }.then
            { _ -> Void in
                self.reloadDataSource()
            }.always {
               self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    func loadFriendRequest() {
        self.showLoading()
        firstly{
            return self.listFriendRequest()
            }.then
            { _ -> Void in
                self.reloadDataSource()
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    func deleteRequest(user:User) {
        firstly{
            return self.deleteFriendRequest(user)
            }.then
            { _ -> Void in
                self.collectionView.reloadData()
            }.always {

            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    func acceptRequest(user:User) {
        firstly{
            return self.acceptFriendRequest(user)
            }.then
            { _ -> Void in
                self.collectionView.reloadData()
            }.always {
                
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    //MARK: Promise Call
    func deleteFriendRequest(user:User) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FriendService.deleteRequest(user, completion:
            {
                [weak self] (response) in
                if let strongSelf = self {
                     Log.debug(String(data: response.data!, encoding: 4))
                    if response.result.isSuccess {
                       
                        if response.response?.statusCode == 200 {
                            fulfill("OK")
                        } else {
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                    else{
                        reject(response.result.error!)
                        strongSelf.handleApiResponseError(response, reject: reject)
                    }
                }
            })
        }
    }
    
    func acceptFriendRequest(user:User) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FriendService.acceptRequest(user, completion:
                {
                    [weak self] (response) in
                    if let strongSelf = self {
                        Log.debug(String(data: response.data!, encoding: 4))
                        if response.result.isSuccess {
                            if response.response?.statusCode == 200 {
                                fulfill("OK")
                            } else {
                                strongSelf.handleApiResponseError(response, reject: reject)
                            }
                        }
                        else{
                            reject(response.result.error!)
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                })
        }
    }
    
    func listFriend() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FriendService.listFriend(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        Log.debug(String(data: response.data!, encoding: 4))
                        if response.response?.statusCode == 200 {
                            strongSelf.friends = Mapper<User>().mapArray(response.result.value) ?? []
                            fulfill("OK")
                        } else {
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                    else{
                        reject(response.result.error!)
                        strongSelf.handleApiResponseError(response, reject: reject)
                    }
                }
            }
        }
    }
    
    func listFriendRequest() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FriendService.listRequest(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            strongSelf.friendRequests = Mapper<User>().mapArray(response.result.value) ?? []
                            fulfill("OK")
                        } else {
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                    else{
                        reject(response.result.error!)
                        strongSelf.handleApiResponseError(response, reject: reject)
                    }
                }
            }
        }
    }
    //MARK: FriendListViewCellDelegate
    func chatClicked(rowIndex: Int) {
        Log.debug("chatClicked: \(rowIndex)")
        let friend = friends[rowIndex]
        
        WebSocketManager.sharedInstance.startConversationWithUser([friend.userKey]) { (conversation) -> Void in
            
            let viewController = TSChatViewController.initFromNib() as! TSChatViewController
            viewController.conversationObject = conversation
            viewController.targetUser = friend
            
            self.ts_pushAndHideTabbar(viewController)
        }
    }
    
    func acceptClicked(rowIndex: Int) {
        Log.debug("acceptClicked: \(rowIndex)")
        let user = self.dataSource[rowIndex]
        user.friendStatus = FriendStatusAccepted
        self.acceptRequest(user)
    }
    
    func deleteClicked(rowIndex: Int) {
        Log.debug("deleteClicked: \(rowIndex)")
        let user = self.dataSource[rowIndex]
        user.friendStatus = FriendStatusDeleted
        self.deleteRequest(user)
    }
}