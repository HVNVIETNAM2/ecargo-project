//
//  IMViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 18/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
import PromiseKit

class IMViewController : MmViewController{
    private final let CellHeight : CGFloat = 70
    private var convList: [Conv] = []
    private var userList : [User] = []
    private var friends: [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = String.localize("LB_CA_MESSENGER")
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "addButtonClicked:")
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(title: String.localize("LB_CA_IM_FRD"), style: UIBarButtonItemStyle.Plain, target: self, action: "imFriend:")
        self.collectionView!.registerClass(IMViewCell.self, forCellWithReuseIdentifier: "IMViewCell")
        
        firstly {
                return self.listFriend()
            }.then
            { _ -> Void in
                return self.fetchMyself()
            }.then
            { _ -> Void in
                self.loadConversationList()
            }.then
            { _ -> Void in
                self.reloadDataSource()
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didUpdateConvList:", name: "IMDidUpdateConvList", object: nil)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
    }
    
    func didUpdateConvList(notification: NSNotification){
        firstly {
                return self.listFriend()
            }.then
            { _ -> Void in
                return self.fetchMyself()
            }.then
            { _ -> Void in
                self.loadConversationList()
            }.then
            { _ -> Void in
                self.reloadDataSource()
        }
        
    }
    func loadConversationList() {
        self.convList = WebSocketManager.sharedInstance.convList
        for conv : Conv in self.convList{
            let toUserKey = conv.userList.filter({$0 != Context.getUserKey()}).first()
            let user = friends.filter({$0.userKey == toUserKey}).first() ?? User()
            userList.append(user)
        }
    }
    
    func fetchMyself() -> Promise<AnyObject>{
        return Promise{ fulfill, reject in
            UserService.view(Context.getUserId()){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            WebSocketManager.sharedInstance.currentUser = Mapper<User>().map(response.result.value)!
                            fulfill("OK")
                        } else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    } else {
                        reject(response.result.error!)
                    }
                }
            }
        }
    }
    
    func listFriend() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FriendService.listFriend(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        Log.debug(String(data: response.data!, encoding: 4))
                        if response.response?.statusCode == 200 {
                            strongSelf.friends = Mapper<User>().mapArray(response.result.value) ?? []
                            fulfill("OK")
                        } else {
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                    else{
                        reject(response.result.error!)
                        strongSelf.handleApiResponseError(response, reject: reject)
                    }
                }
            }
        }
    }
    
    
    //MARK: Collection View methods and delegates
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.convList.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("IMViewCell", forIndexPath: indexPath) as! IMViewCell
        let conv = self.convList[indexPath.row]
        let user = self.userList[indexPath.row]
        cell.upperLabel.text = user.displayName
        cell.lowerLabel.text = conv.convKey
        cell.setImage(user.profileImage, imageCategory: .User)
        
        cell.diamondImageView.hidden = (user.IsCuratorUser == 0)
        cell.imageView.layer.cornerRadius = cell.imageView.frame.height / 2
        cell.imageView.layer.borderWidth = 1.0
        
        if user.IsCuratorUser == 1 {
            cell.diamondImageView.hidden = false
            cell.imageView.layer.cornerRadius = cell.imageView.frame.height / 2
            cell.imageView.layer.borderWidth = 1.0
        } else {
            cell.diamondImageView.hidden = true
            cell.imageView.layer.borderWidth = 0.0
            
        }
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.ShortStyle
        formatter.timeStyle = .ShortStyle
        let dateString = formatter.stringFromDate(conv.timestamp)
        cell.rightLabel.text = dateString
        
        cell.rightMenuItems = [
            SwipeActionMenuCellData(
                text: String.localize("LB_CA_DELETE"),
                icon: UIImage(named: "icon_swipe_delete"),
                backgroundColor: UIColor(hexString: "#7A848C"),
                defaultAction: true,
                action: { [weak self, user] () -> Void in
                    
                    Log.debug("remove: \(user)")
                    //TO-DO: remove conversation
                    
                    
                }
            ),
        ]
        
        return cell
       
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSizeMake(self.view.frame.size.width , CellHeight)
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    private var itemDataSouce = [MessageModel]()
    
    private func fetchData() {
        guard let JSONData = NSData.dataFromJSONFile("message") else {
            return
        }
        let jsonObject = JSON(data: JSONData)
        if jsonObject != JSON.null {
            var list = [MessageModel]()
            for dict in jsonObject["data"].arrayObject! {
                guard let model = TSMapper<MessageModel>().map(dict) else {
                    continue
                }
                list.insert(model, atIndex: list.count)
            }
            //Insert more data, make the UITableView long and long.  XD
            self.itemDataSouce.insertContentsOf(list, at: 0)
            self.itemDataSouce.insertContentsOf(list, at: 0)
            self.itemDataSouce.insertContentsOf(list, at: 0)
            self.itemDataSouce.insertContentsOf(list, at: 0)
            //            self.listTableView.reloadData()
        }
    }
    
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            
            
            let viewController = TSChatViewController.initFromNib() as! TSChatViewController
            viewController.conversationObject = self.convList[indexPath.row]
            viewController.targetUser = self.userList[indexPath.row]
            self.ts_pushAndHideTabbar(viewController)
            
            Log.debug("didSelectItemAtIndexPath: \(indexPath.row)")
    }
    
    func userChat (sender : UIBarButtonItem){
        self.navigationController?.pushViewController(UserChatViewController(), animated: true)
    }
    
    func addButtonClicked(sender : UIBarButtonItem){
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let findFriendAction = UIAlertAction(title: String.localize("LB_CA_IM_FIND_USER_ADD"), style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.navigationController?.pushViewController(AddFriendViewController(), animated: true)
        })
        let scanAction = UIAlertAction(title: String.localize("LB_CA_IM_SCAN_QR"), style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            Log.debug(String.localize("LB_CA_IM_SCAN_QR"))
            
            if QRCodeReader.supportsMetadataObjectTypes() {
                self.navigationController?.pushViewController(MMScanQRCodeController(), animated: true)
            }
        })
        let cancelAction = UIAlertAction(title: String.localize("LB_CA_CANCEL"), style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(findFriendAction)
        optionMenu.addAction(scanAction)
        optionMenu.addAction(cancelAction)
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    
    func imFriend (sender : UIBarButtonItem){
        self.navigationController?.pushViewController(FriendListViewController(), animated: true)
    }
    
    func reloadDataSource() {
        self.collectionView.reloadData()
    }
}
