//
//  MMScanQRCodeController.swift
//  QRCodeReader.swift
//
//  Created by Tony Fung on 8/3/2016.
//  Copyright © 2016年 Yannick Loriot. All rights reserved.
//

import UIKit
import AVFoundation

class MMScanQRCodeController: MmViewController {
    
    private var cameraView = ReaderOverlayView()
    private var cancelButton = UIButton()
  
    var codeReader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createBackButton()
        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(title: String.localize("LB_CA_IM_MY_QR"), style: UIBarButtonItemStyle.Plain, target: self, action: "myQRCode:")
        
        self.title = String.localize("LB_CA_IM_SCAN_QRCODE")
        
    }
    
    
    override func shouldHideTabBar() -> Bool {
        return true
    }
    
    override func shouldHaveCollectionView() -> Bool {
        return true
    }

    
    func myQRCode (sender : UIBarButtonItem){
        MyQRCodeViewController.presentQRCodeController(self)
    }
    
    deinit {
        codeReader.stopScanning()
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    required init() {
        super.init(nibName: nil, bundle: nil)
        
        view.backgroundColor = .blackColor()
        
        codeReader.didFindCodeBlock = { [weak self] resultAsObject in
            if let weakSelf = self {
                
                let optionMenu = UIAlertController(title: nil, message: resultAsObject.value, preferredStyle: .Alert)
                let cancelAction = UIAlertAction(title: String.localize("LB_CANCEL"), style: .Cancel, handler: {
                    (alert: UIAlertAction!) -> Void in
                })
                optionMenu.addAction(cancelAction)
                weakSelf.presentViewController(optionMenu, animated: true, completion: nil)
                
            }
        }
        
        setupUIComponentsWithCancelButtonTitle()
        setupAutoLayoutConstraints()
        
        cameraView.layer.insertSublayer(codeReader.previewLayer, atIndex: 0)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "orientationDidChanged:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Managing the Orientation
    func orientationDidChanged(notification: NSNotification) {
        cameraView.setNeedsDisplay()
        
        if codeReader.previewLayer.connection != nil {
            let orientation = UIApplication.sharedApplication().statusBarOrientation
            
            codeReader.previewLayer.connection.videoOrientation = QRCodeReader.videoOrientationFromInterfaceOrientation(orientation)
        }
    }
    
    // MARK: - Initializing the AV Components
    
    private func setupUIComponentsWithCancelButtonTitle() {
        cameraView.clipsToBounds = true
        cameraView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(cameraView)
        
        codeReader.previewLayer.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)
        
        if codeReader.previewLayer.connection.supportsVideoOrientation {
            let orientation = UIApplication.sharedApplication().statusBarOrientation
            
            codeReader.previewLayer.connection.videoOrientation = QRCodeReader.videoOrientationFromInterfaceOrientation(orientation)
        }
        
    }

    
    private func setupAutoLayoutConstraints() {
        let views = ["cameraView": cameraView]
        
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[cameraView]|", options: [], metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[cameraView]|", options: [], metrics: nil, views: views))
     
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        startScanning()
    }

    
    /// Starts scanning the codes.
    func startScanning() {
        codeReader.startScanning()
    }
    
    /// Stops scanning the codes.
    func stopScanning() {
        codeReader.stopScanning()
    }
    

    
}
