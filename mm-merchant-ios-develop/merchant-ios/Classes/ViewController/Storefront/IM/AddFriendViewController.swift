//
//  AddFriendViewController.swift
//  merchant-ios
//
//  Created by Tony Fung on 4/3/2016.
//  Copyright © 2016年 WWE & CO. All rights reserved.
//

import Foundation

private let reuseIdentifier = "FriendMenuViewCell"

class AddFriendViewController: MmViewController {
    
    
    
    private final let CellHeight : CGFloat = 54
    private var titleText: [[String]] = [["LB_CA_IM_SEARCH_USER"],["LB_CA_IM_SCAN_QR","LB_CA_IM_INVITE_FRD"]]
    private var subTitleText: [[String]] = [[""], ["LB_CA_IM_SCAN_QR_NOTE", "LB_CA_IM_INVITE_FRD_NOTE"]]
    private var iconImagePaths: [[String]] = [["search_icon"],["scan_icon","phonebook copy 2"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = String.localize("LB_CA_IM_FIND_USER_ADD")
        
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(title: String.localize("LB_CA_IM_MY_QR"), style: UIBarButtonItemStyle.Plain, target: self, action: "myQRCode:")
        self.collectionView!.registerClass(FriendMenuViewCell.self, forCellWithReuseIdentifier: "FriendMenuViewCell")
    }
    
    //MARK: Collection View methods and delegates
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.titleText[section].count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return titleText.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FriendMenuViewCell", forIndexPath: indexPath) as! FriendMenuViewCell
        cell.textLabel.text = String.localize(titleText[indexPath.section][indexPath.item])
        cell.lowerLabel.text = String.localize(subTitleText[indexPath.section][indexPath.item])
        cell.imageView.image = UIImage(named: iconImagePaths[indexPath.section][indexPath.item])
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSizeMake(self.view.frame.size.width , CellHeight)
    }
    
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {

            if section == 0 {
                return UIEdgeInsets(top: 20.0, left: 0.0, bottom: 0, right: 0.0)
            } else{
                return UIEdgeInsets(top: 40.0, left: 0.0, bottom: 0, right: 0.0)
            }
            
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        if section == 0 {
            return 22
        } else{
            return 50
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            Log.debug("didSelectItemAtIndexPath: \(indexPath.row)")
            
            if indexPath.section == 1 && indexPath.row == 0 && QRCodeReader.supportsMetadataObjectTypes() {
                self.navigationController?.pushViewController(MMScanQRCodeController(), animated: true)
            }
            if indexPath.section == 0 && indexPath.row == 0 {
                self.navigationController?.pushViewController(SearchFriendViewController(), animated: true)
            }
    }
    
    
    func myQRCode (sender : UIBarButtonItem){
        MyQRCodeViewController.presentQRCodeController(self)
    }
    
    func reloadDataSource() {
        self.collectionView.reloadData()
    }


}
