//
//  SearchFiendViewController.swift
//  merchant-ios
//
//  Created by HVN_Pivotal on 3/8/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import ObjectMapper
import PromiseKit
import AlamofireImage

class SearchFriendViewController : MmViewController, UISearchBarDelegate, UISearchResultsUpdating, SearchFriendViewCellDelegate {
    var searchController: UISearchController!
    private final let CellHeight : CGFloat = 70
    private var isSearching : Bool = false
    var searchString : String = ""
    private var dataSource: [User] = []
    private var contentInset = UIEdgeInsetsZero;
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.registerClass(SearchFriendViewCell.self, forCellWithReuseIdentifier: "SearchFriendViewCell")
        self.collectionView!.backgroundColor = UIColor.whiteColor();
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.sizeToFit()
        definesPresentationContext = true
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        self.searchController.searchBar.showsCancelButton = true
        self.showSearchBar()
        contentInset = self.collectionView.contentInset
        let gesture = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        self.collectionView.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        if searchController.searchBar.text!.length < 1 && self.dataSource.count == 0 {
            self.searchController.searchBar.becomeFirstResponder()
            self.doSearch("")
        }
    }
    

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.collectionView.contentInset = contentInset
    }
    
    func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            if let keyboardSize: CGSize =  userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size {
                let heightOfset = ((keyboardSize.height + self.collectionView.bounds.height + 64) - self.view.bounds.size.height)
                if heightOfset > 0 {
                    let edgeInset = UIEdgeInsetsMake(contentInset.top, contentInset.left,contentInset.bottom + heightOfset,  contentInset.right);
                    self.collectionView.contentInset = edgeInset
                }
            }
        }
    }
        
    //MARK: Collection View methods and delegates
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("SearchFriendViewCell", forIndexPath: indexPath) as! SearchFriendViewCell
        cell.addFriendButton.tag = indexPath.row
        cell.addFollowButton.tag = indexPath.row
        cell.setData(dataSource[indexPath.row])
        cell.searchFriendViewCellDelegate = self
        return cell
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSizeMake(self.view.frame.size.width , CellHeight)
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
        Log.debug("didSelectItemAtIndexPath: \(indexPath.row)")
    }
    //MARK: Search Controller methods
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if searchController.searchBar.text?.length < 1{
            return
        }
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "doSearch", userInfo: nil, repeats: false)
    }
    
    //MARK: Search bar methods
    func showSearchBar(){
        self.searchController.searchBar.alpha = 0
        navigationItem.titleView = self.searchController.searchBar
        navigationItem.setLeftBarButtonItem(nil, animated: false)
        let uiButton = self.searchController.searchBar.valueForKey("cancelButton") as! UIButton
        uiButton.setTitle(String.localize("LB_CANCEL"), forState: UIControlState.Normal)
        self.searchController.searchBar.setImage(UIImage(named: "icon_search"), forSearchBarIcon: UISearchBarIcon.Clear, state: UIControlState.Normal)
        self.searchController.searchBar.tintColor = UIColor.secondary2()
        for view in self.searchController.searchBar.subviews {
            for subsubView in view.subviews  {
                if let textField = subsubView as? UITextField {
                    textField.borderStyle = UITextBorderStyle.None
                    textField.layer.cornerRadius = 11
                    textField.backgroundColor = UIColor.backgroundGray()
                    textField.layer.borderColor = UIColor.backgroundGray().CGColor
                    textField.layer.borderWidth = 1.0
                    textField.placeholder = String.localize("LB_SEARCH")
                }
                
            }
        }
    }
    
    //MARK: UISearchBarDelegate
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        dismissKeyboard()
        let string = searchController.searchBar.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
            ).lowercaseString
        if string.length > 0 {
            doSearch(string)
        }
    }
    func doSearch(){
        let string = searchController.searchBar.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
            ).lowercaseString
        doSearch(string)
    }
    func doSearch(string:String){
        
        if isSearching {
            return
        }
        isSearching = true
        firstly{
            return searchFriend(string)
            }.then { _ -> Void in
                self.reloadDataSource()
            }.always {
                self.isSearching = false
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    //MARK: Filter API Promise Call
    func searchFriend(string: String) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FriendService.findFriend(string){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        Log.debug(String(data: response.data!, encoding: 4))
                        if response.response?.statusCode == 200 {
                            strongSelf.dataSource = Mapper<User>().mapArray(response.result.value) ?? []
                            fulfill("OK")
                        } else {
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                    else{
                        reject(response.result.error!)
                        strongSelf.handleApiResponseError(response, reject: reject)
                    }
                }
            }
        }
    }
    
    func reloadDataSource() {
        self.collectionView.reloadData()
    }
    
    //MARK: SearchFriendViewCellDelegate
    func addFriendClicked(rowIndex: Int){
        Log.debug("addFriendClicked: \(rowIndex)")
        let user = self.dataSource[rowIndex]
        if user.friendStatus.length == 0 || user.friendStatus == String.localize("LB_CA_ADD_FRIEND") { //Not friend
            self.addFriend(user)
        } else if user.friendStatus == String.localize("LB_CA_FRD_REQ_CANCEL") {
            Alert.alert(self, title: "", message: String.localize("LB_CA_FRD_REQ_CANCEL_CONF"), okActionComplete: { () -> Void in
                self.deleteRequest(user)
                }, cancelActionComplete:nil)
            
        }
    }
    func followClicked(rowIndex: Int){
        Log.debug("followClicked: \(rowIndex)")
        let user = self.dataSource[rowIndex]
        if user.followStatus.length == 0 || user.followStatus == String.localize("LB_CA_FOLLOW") { //
            self.saveCurator(user)
        }
    }
    
    
    func deleteRequest(user:User) {
        self.showLoading()
        firstly{
            return self.deleteFriendRequest(user)
            }.then
            { _ -> Void in
                user.friendStatus = String.localize("LB_CA_ADD_FRIEND")
                self.reloadDataSource()
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    func deleteFriendRequest(user:User) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FriendService.deleteRequest(user, completion:
                {
                    [weak self] (response) in
                    if let strongSelf = self {
                        Log.debug(String(data: response.data!, encoding: 4))
                        if response.result.isSuccess {
                            if response.response?.statusCode == 200 {
                                fulfill("OK")
                            } else {
                                strongSelf.handleApiResponseError(response, reject: reject)
                            }
                        }
                        else{
                            reject(response.result.error!)
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                })
        }
    }
    
    func addFriend(user:User) {
        self.showLoading()
        firstly{
            return self.addFriendRequest(user)
            }.then
            { _ -> Void in
                user.friendStatus = String.localize("LB_CA_FRD_REQ_CANCEL")
                self.showSuccessPopupWithText(String.localize("MSG_SUC_FRIEND_REQ_SENT"))
                self.reloadDataSource()
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    func addFriendRequest(user:User) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            FriendService.addFriendRequest(user, completion:
                {
                    [weak self] (response) in
                    if let strongSelf = self {
                        Log.debug(String(data: response.data!, encoding: 4))
                        if response.result.isSuccess {
                            if response.response?.statusCode == 200 {
                                fulfill("OK")
                            } else {
                                strongSelf.handleApiResponseError(response, reject: reject)
                            }
                        }
                        else{
                            reject(response.result.error!)
                            strongSelf.handleApiResponseError(response, reject: reject)
                        }
                    }
                })
        }
    }
    
    func saveCurator(user : User)-> Promise<AnyObject> {
        self.showLoading()
        return Promise{ fulfill, reject in
            FollowService.saveCurator([user]){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.response?.statusCode == 200 {
                        Log.debug(String(data: response.data!, encoding : 4))
                        user.followStatus = String.localize("LB_CA_FOLLOWED")
                        strongSelf.showSuccessPopupWithText(String.localize("MSG_SUC_FOLLOWED"))
                        strongSelf.reloadDataSource()
                        fulfill("OK")
                    } else {
                        strongSelf.handleError(response, animated: true)
                    }
                    strongSelf.stopLoading()
                }
                
            }
        }
    }

    func dismissKeyboard() {
        searchController.searchBar.resignFirstResponder()
    }
    
}
