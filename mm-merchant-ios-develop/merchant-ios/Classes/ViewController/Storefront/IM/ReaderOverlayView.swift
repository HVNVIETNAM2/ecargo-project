/*
 * QRCodeReader.swift
 *
 * Copyright 2014-present Yannick Loriot.
 * http://yannickloriot.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

import UIKit

/// Overlay over the camera view to display the area (a square) where to scan the code.
final class ReaderOverlayView: UIView {
  private var overlay: CAShapeLayer = {
    var overlay = CAShapeLayer()
    overlay.backgroundColor = UIColor.clearColor().CGColor
    overlay.fillColor       = UIColor.clearColor().CGColor
    overlay.strokeColor     = UIColor.whiteColor().CGColor
    overlay.lineWidth       = 3
    overlay.lineDashPattern = [7.0, 7.0]
    overlay.lineDashPhase   = 0

    return overlay
    }()

  init() {
    super.init(frame: CGRectZero)  // Workaround for init in iOS SDK 8.3
    
    self.backgroundColor = UIColor(white: 0, alpha: 0.3)
    instructionLabel.text = String.localize("LB_CA_IM_SCAN_NOTE")
    instructionLabel.formatSize(14)
    instructionLabel.textColor = UIColor.whiteColor()
    instructionLabel.textAlignment = .Center
  }

    let scanFrameSize = CGSize(width: 230, height: 230)
    let scanFrameImageView = UIImageView(image: UIImage(named: "scan_target"))
    let scanningEffect = UIImageView(image: UIImage(named: "scan_effect"))
    let instructionLabel = UILabel()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        

        scanFrameImageView.frame = CGRectMake(self.frame.midX - scanFrameSize.width/2, self.frame.midY - scanFrameSize.height/2, scanFrameSize.width, scanFrameSize.height)
        self.addSubview(scanFrameImageView)
        
        createScaningAnimation()

        instructionLabel.frame = CGRectMake(self.frame.midX - 250/2, self.frame.midY - scanFrameSize.height/2 - 60, 250, 30)
        mask(self, alpha: 0.3)
        
        self.addSubview(instructionLabel)
        
    }
    
      override init(frame: CGRect) {
        super.init(frame: frame)

      }
    
    func createScaningAnimation(){
        
        scanningEffect.frame = CGRectMake(0, -84, 240, 84)
        
        let animatingArea = UIView(frame: CGRect(x: self.frame.midX - clearMaskSize.width/2, y: self.frame.midY - clearMaskSize.height/2, width: clearMaskSize.width, height: clearMaskSize.height))
        animatingArea.backgroundColor = UIColor.clearColor()

        self.addSubview(animatingArea)
        animatingArea.clipsToBounds = true
        
        animatingArea.addSubview(scanningEffect)
        
        UIView.animateWithDuration(1.3, delay: 0, options: UIViewAnimationOptions.Repeat, animations: { () -> Void in
            self.scanningEffect.frame = CGRectMake(0, self.clearMaskSize.height, 240, 84)
            }, completion: nil)
        
    }
    
    
    let clearMaskSize = CGSize(width: 240, height: 240)
    
    func mask(view: UIView, alpha: CGFloat) {
        
        let maskLayer = CAShapeLayer()
        let maskPath = CGPathCreateMutable()
        
        CGPathAddRect(maskPath, nil, CGRectMake(0, 0, self.frame.width, self.frame.height))
        CGPathAddRect(maskPath, nil, CGRect(x: self.frame.midX - clearMaskSize.width/2, y: self.frame.midY - clearMaskSize.height/2, width: clearMaskSize.width, height: clearMaskSize.height))
        maskLayer.fillRule = kCAFillRuleEvenOdd
        maskLayer.fillColor = UIColor(white: 0, alpha: alpha).CGColor
        maskLayer.opacity = 0.5
        maskLayer.path = maskPath
    
        view.layer.addSublayer(maskLayer)
    }


  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)

    layer.addSublayer(overlay)
  }
    
    var fillColor = UIColor(white: 0, alpha: 0.8)

  override func drawRect(rect: CGRect) {
//
//    self.fillColor.setFill()
//    UIRectFill(rect);
//    
//    let context = UIGraphicsGetCurrentContext();
//    CGContextSetBlendMode(context, CGBlendMode.DestinationOut)
//    
//    let pathRect = CGRect(x: 150, y: 150, width: 300, height: 300)
//    let path = UIBezierPath(rect: pathRect)
//    path.fill()
//    CGContextSetBlendMode(context, CGBlendMode.Normal)
  }
    
    
}
