//
//  WishListItemCell.swift
//  merchant-ios
//
//  Created by HungPM on 1/25/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

class WishListItemCell: SwipeActionMenuCell {
    
    private var productImageView: UIImageView!
    private var brandImageView: UIImageView!
    private var productNameLabel: UILabel!
    private var productPriceLabel: UILabel!
    private var productAddToCartButton: UIButton!

    var brandLogoHandle: ((data: CartItem) -> Void)?
    var addToCartHandle: ((data: CartItem) -> Void)?
    var productImageHandler: ((data: CartItem) -> Void)?

    var data: CartItem? {
        didSet {
            if let data = self.data {

                var productImageKey = ""
                var brandImageKey = ""
                
                var imageSku: Sku?
                if data.skuId != 0 {
                    // add from sku
                    if let skuList = data.style?.skuList {
                        for sku in skuList {
                            if sku.skuId == data.skuId {
                                imageSku = sku
                                break
                            }
                        }
                    }
                }
                
                if imageSku == nil {
                    // add from style
                    if let sku = data.style?.skuList.first {
                        imageSku = sku
                    }
                }
                
                if let sku = imageSku {
                    self.productNameLabel.text = sku.skuName
                }
                
                if let imageKey = data.style?.findImageKeyByColorKey(data.colorKey) {
                    productImageKey = imageKey
                }
                
                if productImageKey.isEmpty {
                    self.productNameLabel.text = ""
                }
                
                if let style = data.style {
                    brandImageKey = style.brandHeaderLogoImage
                }
                
                if productImageKey != "" {
                    self.productImageView.af_setImageWithURL(
                        ImageURLFactory.get(productImageKey),
                        placeholderImage: UIImage(named: "Spacer"),
                        filter: nil,
                        imageTransition: .CrossDissolve(0.3),
                        completion: nil
                    )
                }

                self.brandImageView.af_setImageWithURL(
                    ImageURLFactory.get(brandImageKey, category: .Brand),
                    placeholderImage: UIImage(named: "holder"),
                    filter: nil,
                    imageTransition: .CrossDissolve(0.3),
                    completion: { (response) -> Void in
                        
                        if let image = response.result.value {
                            let imageWidth = image.size.width
                            let imageHeight = image.size.height
                            let imageRatio = imageWidth / imageHeight
                            
                            self.brandImageView.frame = CGRectMake(self.brandImageView.frame.origin.x, self.brandImageView.frame.origin.y, self.brandImageView.frame.size.height * imageRatio, self.brandImageView.frame.size.height)
                        }
                        
                })
                
                var targetSku: Sku?
                if (data.skuId != 0) {
                    if let sku = data.style?.findSkuBySkuId(data.skuId) {
                        targetSku = sku
                    } else {
                        targetSku = data.style?.lowestPriceSku()
                    }
                } else {
                    targetSku = data.style?.lowestPriceSku()
                }
                
                if let sku = targetSku {
                    fillPrice(sku.priceSale, priRetail: sku.priceRetail)
                }
                
                productAddToCartButton.hidden = !data.isProductValid()
            }
        }
    }
    
    func fillPrice(priSale : Int, priRetail : Int) {
        let priceText = NSMutableAttributedString()
        
        let saleFont = UIFont.systemFontOfSize(16)
        let retailFont = UIFont.systemFontOfSize(11)

        if priSale > 0 {
            if let priceSale = priSale.formatPrice() {
                
                let saleText = NSAttributedString(
                    string: priceSale + " ",
                    attributes: [
                        NSForegroundColorAttributeName: UIColor(hexString: "#DB1E49"),
                        NSFontAttributeName: saleFont,
                        NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleNone.rawValue
                    ]
                )
                priceText.appendAttributedString(saleText)
            }
            
            if let priceRetail = priRetail.formatPrice() where priRetail != priSale {
                
                let retailText = NSAttributedString(
                    string: priceRetail,
                    attributes: [
                        NSForegroundColorAttributeName: UIColor(hexString: "#757575"),
                        NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue,
                        NSFontAttributeName: retailFont,
                        NSBaselineOffsetAttributeName: (saleFont.capHeight - retailFont.capHeight) / 2
                    ]
                )
                
                priceText.appendAttributedString(retailText)
            }
            
        } else {
            
            if let priceRetail = priRetail.formatPrice() where priRetail != priSale {
                
                let retailText = NSAttributedString(
                    string: priceRetail,
                    attributes: [
                        NSForegroundColorAttributeName: UIColor(hexString: "#4A4A4A"),
                        NSFontAttributeName: saleFont
                    ]
                )
                priceText.appendAttributedString(retailText)
            }
        }
        self.productPriceLabel.attributedText = priceText
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let ContainerPaddingTop = CGFloat(23)
        let ContainerHeight = CGFloat(142)
        
        let containerFrame = CGRectMake(0, ContainerPaddingTop, frame.width, ContainerHeight)
        let containerView = UIView(frame: containerFrame)

        self.contentView.addSubview(containerView)
        
        let MarginLeft = CGFloat(15)
        let productImageFrame = CGRectMake(MarginLeft, 0, 110, 114)
        let productImageView = UIImageView(frame: productImageFrame)
        productImageView.image = UIImage(named: "comment_demo2.jpg")
        productImageView.contentMode = .ScaleAspectFit
        let singleTap = UITapGestureRecognizer(target: self, action: "productImageTapped")
        productImageView.addGestureRecognizer(singleTap)
        productImageView.userInteractionEnabled = true
        containerView.addSubview(productImageView)
        self.productImageView = productImageView
        
        let leftPadding = CGFloat(10)
        let rightPadding = CGFloat(5)
        
        let xPos = productImageFrame.maxX + leftPadding
        
        let brandImageView = { () -> UIImageView in
            
            let imageView = UIImageView(frame: CGRectMake(xPos, 0, 0, 28))
            imageView.userInteractionEnabled = true
            let singleTap = UITapGestureRecognizer(target: self, action: "brandLogoTapped")
            imageView.addGestureRecognizer(singleTap)
            
            self.brandImageView = imageView
            
            return imageView
        } ()
        containerView.addSubview(brandImageView)
        
        let yPadding = CGFloat(10)
        let productNameHeight = CGFloat(30)
        let productNameFrame = CGRectMake(xPos, brandImageView.frame.maxY + yPadding, frame.width - xPos - rightPadding, productNameHeight)
        let productNameLabel = UILabel(frame: productNameFrame)
        productNameLabel.formatSize(14)
        productNameLabel.textColor = UIColor(hexString: "#757575")
        productNameLabel.text = "productNameLabel text text text text text text text text text text text"
        self.productNameLabel = productNameLabel
        containerView.addSubview(productNameLabel)
        
        let priceLabelHeight = CGFloat(30)
        let priceLableFrame = CGRectMake(xPos, productNameLabel.frame.maxY, frame.width - xPos - rightPadding, priceLabelHeight)
        let priceLabel = UILabel(frame: priceLableFrame)
        priceLabel.escapeFontSubstitution = true
        self.productPriceLabel = priceLabel
        
        containerView.addSubview(priceLabel)
        
        let priceText = NSMutableAttributedString()
        let saleFont = UIFont.systemFontOfSize(16)
        let retailFont = UIFont.systemFontOfSize(11)

        let salesPeice = NSAttributedString(
            string: "",
            attributes: [
                NSForegroundColorAttributeName: UIColor(hexString: "#DB1E49"),
                NSFontAttributeName: saleFont,
                NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleNone.rawValue
            ]
        )
        priceText.appendAttributedString(salesPeice)
        
        let retailPrice = NSAttributedString(
            string: "",
            attributes: [
                NSForegroundColorAttributeName: UIColor(hexString: "#757575"),
                NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue,
                NSFontAttributeName: retailFont,
                NSBaselineOffsetAttributeName: (saleFont.capHeight - retailFont.capHeight) / 2
            ]
        )
        
        priceText.appendAttributedString(retailPrice)

        priceLabel.attributedText = priceText
        
        let addToCartButtonWidth = CGFloat(90)
        let addToCartButtonHeight = CGFloat(26)

        let addToCartButtonFrame = CGRectMake(containerView.frame.width - rightPadding - addToCartButtonWidth, containerView.frame.height - addToCartButtonHeight, addToCartButtonWidth, addToCartButtonHeight)
        let addToCartButton = UIButton(type: .Custom)
        addToCartButton.frame = addToCartButtonFrame
        addToCartButton.setTitle(String.localize("LB_CA_ADD2CART"), forState: .Normal)
        addToCartButton.titleLabel?.formatSize(13)
        addToCartButton.setTitleColor(UIColor.primary1(), forState: .Normal)
        addToCartButton.layer.cornerRadius = 5
        addToCartButton.layer.borderColor = UIColor.backgroundGray().CGColor
        addToCartButton.layer.borderWidth = 1.5
        addToCartButton.addTarget(self, action: "addToCart", forControlEvents: .TouchUpInside)
        self.productAddToCartButton = addToCartButton
        containerView.addSubview(addToCartButton)
        
        let lineHeight = CGFloat(1)
        let line = UIView(frame: CGRectMake(0, self.contentView.frame.maxY - lineHeight, self.contentView.frame.width, lineHeight))
        line.backgroundColor = UIColor.backgroundGray()
        self.contentView.addSubview(line)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func brandLogoTapped() {
        if let callback = self.brandLogoHandle, let data = self.data {
            callback(data: data)
        }
    }
    
    func addToCart() {
        if let callback = self.addToCartHandle, let data = self.data {
            callback(data: data)
        }
    }

    func productImageTapped() {
        if let callback = self.productImageHandler, let data = self.data {
            callback(data: data)
        }
    }
    
    func accessibilityIdentifierIndex(index: Int) {
        accessibilityIdentifier = "WishlistCell-\(index)"
    }
}