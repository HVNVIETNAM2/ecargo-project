//
//  WishlistAnimation.swift
//  merchant-ios
//
//  Created by HungPM on 2/23/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

class WishlistAnimation {
    
    private var heartImage : UIImageView!
    private var redDotButton : ButtonRedDot?
    
    init() {}
    
    convenience init(
        heartImage : UIImageView,
        redDotButton: ButtonRedDot?
        ) {
            self.init()
            
            self.redDotButton = redDotButton
            self.heartImage = heartImage
    }
    
    func showAnimation(completion complete : (() -> Void)? = nil) {
        
        if let redDotButton = self.redDotButton {
            redDotButton.animateHeartIcon()
        }
        
        let redHeartImage = UIImageView(frame: self.heartImage.bounds)
        redHeartImage.contentMode = .ScaleAspectFit
        
        let prepareAnimation = {
            redHeartImage.image = UIImage(named: "like_on")
            redHeartImage.alpha = 0.0
            self.heartImage.addSubview(redHeartImage)
        }
        prepareAnimation()
        
        let duration = NSTimeInterval(0.2)
        
        let step2 = {
            UIView.animateWithDuration(
                duration,
                delay: 0,
                options: .CurveEaseIn,
                animations: { () -> Void in
                    redHeartImage.alpha = 1.0
                    redHeartImage.transform = CGAffineTransformIdentity
                },
                completion: { (success) -> Void in
                    redHeartImage.removeFromSuperview()
                    
                    if let completeAction = complete {
                        completeAction()
                    }
                }
            )
        }
        
        let step1 = {
            UIView.animateWithDuration(
                duration,
                delay: 0,
                options: .CurveEaseOut,
                animations: { () -> Void in
                    redHeartImage.alpha = 0.5
                    redHeartImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.7, 1.7)
                },
                completion: { (success) -> Void in
                    step2()
                }
            )
        }
        step1()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}