//
//  WishListCartViewController.swift
//  merchant-ios
//
//  Created by Alan YU on 28/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class WishListCartViewController: MmCartViewController {

    enum IndexPathSection: Int {
        case Header
    }

    private final let CellId = "WishlistItemCell"

    private final let CellHeight : CGFloat = 175
    private var dataSource: [CartItem]!
    private var wishlist : Wishlist?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.backgroundGray()
        
        setupNavigationBar()
        
        self.collectionView!.registerClass(WishListItemCell.self, forCellWithReuseIdentifier: CellId)
        self.collectionView.backgroundColor = UIColor.clearColor()
        self.collectionView.alwaysBounceVertical = true
        
        self.dataSource = [CartItem]()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if Context.getAuthenticatedUser() || (Context.anonymousWishlistKey() != nil && Context.anonymousWishlistKey() != "0") {
            self.showLoading()
            firstly {
                return self.listWishlistItem()
                }.then { _ -> Void in
                    self.wishlist = CacheManager.sharedManager.wishlist
                    self.reloadDataSource()
                }.always {
                    self.stopLoading()
                }.error { _ -> Void in
                    Log.error("error")
            }
        }
    }
    
    func setupNavigationBar() {
        setupNavigationBarCartButton()

        self.navigationController!.navigationBarHidden = false
        
        var rightButtonItems = [UIBarButtonItem]()
        rightButtonItems.append(UIBarButtonItem(customView: buttonCart!))
        
        buttonCart!.addTarget(self, action: "cartButtonTapped", forControlEvents: .TouchUpInside)
        
        self.title = String.localize("LB_CA_WISHLIST")
        self.navigationItem.rightBarButtonItems = rightButtonItems

        self.createBackButton()
    }
            
    func cartButtonTapped() {
        Log.debug("cart button tapped")
        let shoppingcart = ShoppingCartViewController()
        self.navigationController?.pushViewController(shoppingcart, animated: true)
    }
    
    func reloadDataSource() {
        self.dataSource.removeAll()

        if let cartItems = self.wishlist?.cartItems {
            for cartItem in cartItems {
                self.dataSource.append(cartItem)
            }
        }
        
        self.collectionView!.reloadData()
    }
    
    func indexPathForData(data: AnyObject, found: ((indexPath: NSIndexPath) -> Void)) -> NSIndexPath? {
        var indexPath: NSIndexPath!
        for row in 0 ..< self.dataSource.count {
            let item = self.dataSource[row]
            if item === data {
                
                indexPath = NSIndexPath(forRow: row, inSection: 0)
                found(indexPath: indexPath)
                break
            }
        }
        
        return indexPath
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    //MARK: Draw Cell
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath) as! WishListItemCell
        let data = self.dataSource[indexPath.row] as CartItem
        
        cell.leftMenuItems = [
            SwipeActionMenuCellData(
                text: String.localize("LB_CA_ADD2CART"),
                icon: UIImage(named: "icon_swipe_addToCart"),
                backgroundColor: UIColor(hexString: "#c0c0c0"),
                defaultAction: true,
                action: { [weak self, data] () -> Void in
                    Log.debug("Add To Cart")
                    if let strongSelf = self {
                        
                        strongSelf.indexPathForData(data, found: { (indexPath) in
                            
                            let cartItem = strongSelf.dataSource[indexPath.row] as CartItem
                            var selectedColorId = -1
                            var selectedSizeId = -1
                            if let filteredSkuList = cartItem.style?.skuList.filter({$0.skuId == cartItem.skuId}) where filteredSkuList.count > 0 {
                                selectedSizeId = filteredSkuList[0].sizeId
                            }
                            
                            if let filteredColorList = cartItem.style?.colorList.filter({$0.colorKey.lowercaseString == cartItem.colorKey.lowercaseString}) where filteredColorList.count > 0 {
                                selectedColorId = filteredColorList[0].colorId
                            }
                            
                            let checkout = CheckOutViewController(
                                style: cartItem.style,
                                selectedColorId: selectedColorId,
                                selectedSizeId: selectedSizeId,
                                redDotButton: strongSelf.buttonCart
                            )
                            checkout.cartItem = cartItem
                            checkout.checkOutMode = CheckOutMode.StyleAndSku
                            checkout.didDismissHandler = {
                                strongSelf.updateButtonCartState()
                            }
                            strongSelf.presentViewController(checkout, animated: false, completion: nil)
                            
                        })
                    }
                    
                }
            ),
        ]
        
        cell.rightMenuItems = [
            SwipeActionMenuCellData(
                text: String.localize("LB_CA_DELETE"),
                icon: UIImage(named: "icon_swipe_delete"),
                backgroundColor: UIColor(hexString: "#7A848C"),
                defaultAction: true,
                action: { [weak self, data] () -> Void in
                    Log.debug("Remove Wishlist Item")
                    if let strongSelf = self {
                        Alert.alert(strongSelf, title: "", message: String.localize("MSG_CA_CONFIRM_REMOVE"), okActionComplete: { () -> Void in
                            
                            strongSelf.indexPathForData(data, found: { (indexPath) in
                                
                                let cartItem = strongSelf.dataSource[indexPath.row] as CartItem
                                
                                firstly{
                                    return strongSelf.removeWishlistItem(cartItem.cartItemId)
                                    }.then { _ -> Void in
                                        strongSelf.showSuccessPopupWithText(String.localize("LB_CA_DEL_WISHLIST_ITEM_SUCCESS"))
                                        strongSelf.wishlist = CacheManager.sharedManager.wishlist
                                        strongSelf.reloadDataSource()
                                    }.error { _ -> Void in
                                        Log.error("error")
                                        strongSelf.showFailPopupWithText(String.localize("LB_CA_DEL_WISHLIST_ITEM_FAILED"))
                                }
                                
                            })
                            
                            }, cancelActionComplete:nil)
                    }
                    
                }
            )
        ]
        
        cell.addToCartHandle = { (data) in
            Log.debug("addToCartHandle")
            
            var selectedColorId = -1
            var selectedSizeId = -1
            
            if let filteredSkuList = data.style?.skuList.filter({$0.skuId == data.skuId}) where filteredSkuList.count > 0 {
                selectedSizeId = filteredSkuList[0].sizeId
            }
            
            if let filteredColorList = data.style?.colorList.filter({$0.colorKey.lowercaseString == data.colorKey.lowercaseString}) where filteredColorList.count > 0 {
                selectedColorId = filteredColorList[0].colorId
            }
            
            let checkout = CheckOutViewController(
                style: data.style,
                selectedColorId: selectedColorId,
                selectedSizeId: selectedSizeId,
                redDotButton: self.buttonCart
            )
            
            checkout.cartItem = data
            checkout.checkOutMode = CheckOutMode.StyleAndSku
            checkout.didDismissHandler = {
                self.updateButtonCartState()
            }
            self.presentViewController(checkout, animated: false, completion: nil)
            
        }
        
        cell.productImageHandler = { (data) in
            Log.debug("productImageHandler")
            
            let detailVC = DetailViewController()
            if let style = data.style {
                detailVC.style = style
            }
            else {
                detailVC.styleCode = data.styleCode
            }
            self.navigationController?.pushViewController(detailVC, animated: true)
            
        }
        
        cell.brandLogoHandle = { (data) in
            Log.debug("brandLogoHandle")
        }
        
        cell.data = data
        
        cell.accessibilityIdentifierIndex(indexPath.row)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        if section == IndexPathSection.Header.rawValue {
            return UIEdgeInsetsMake(10, 0, 0, 0)
        }
        return UIEdgeInsetsZero
    }
    
    //MARK: Item Size Delegate for Collection View
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.view.frame.size.width , CellHeight)
    }
    
    override func shouldHideTabBar() -> Bool {
        return true
    }
    
}