//
//  IDCardCollectionPageViewController.swift
//  merchant-ios
//
//  Created by HungPM on 2/22/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import AVFoundation
import Photos

class IDCardCollectionPageViewController : MmViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    enum IndexPathRow: Int {
        case InfoHeader = 0
        case FirstCardImage
        case SecondCardImage
        case Firstname
        case Lastname
        case IdCardNumber
//        case IdTickbox
        case Count
    }

    private final let IDLabelCellID = "IDLabelCellID"
    private final let IDCardImageCellID = "IDCardImageCellID"
    private final let IDNameCellID = "IDNameCellID"
    private final let IDCardNumberCellID = "IDCardNumberCellID"
//    private final let IDTickCellID = "IDTickCellID"
    private final let DefaultCellID = "DefaultCellID"
    
    private final let ButtonViewHeight = CGFloat(64)
    
    private var buttonOK : UIButton!
    private var activeTextField : UITextField!
    
    private var idCardImageFront : UIImageView!
    private var idCardImageBack : UIImageView!
    private var isFrontImagePicked = false
    private var isBackImagePicked = false
    
    private var isFrontImageChose = true
    
    private let imagePicker = UIImagePickerController()

    private var tfFirstName : UITextField!
    private var tfLastName : UITextField!
    private var tfIDNumber : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBarHidden = false

        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = false

        self.title = String.localize("LB_CA_ID_CARD_VER")
        
        setupNavigationBar()
        
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: self.DefaultCellID)
        
        self.collectionView!.registerClass(IDLabelCell.self, forCellWithReuseIdentifier: self.IDLabelCellID)
        self.collectionView!.registerClass(IDCardImageCell.self, forCellWithReuseIdentifier: self.IDCardImageCellID)
        self.collectionView!.registerClass(IDNameCell.self, forCellWithReuseIdentifier: self.IDNameCellID)
        self.collectionView!.registerClass(IDCardNumberCell.self, forCellWithReuseIdentifier: self.IDCardNumberCellID)
//        self.collectionView!.registerClass(IDTickCell.self, forCellWithReuseIdentifier: self.IDTickCellID)
        
        let buttonView = { () -> UIView in
            
            let frame = CGRectMake(0, self.collectionView.frame.maxY, self.collectionView.frame.width, ButtonViewHeight)
            
            let view = UIView(frame: frame)
            
            //
            let okButton = { () -> UIButton in
                
                let margin = CGFloat(11)
                let buttonSize = CGSizeMake(frame.width - 2 * margin, frame.height - 2 * margin)
                
                let button = UIButton(type: .Custom)
                button.frame = CGRectMake(margin, margin, buttonSize.width, buttonSize.height)
                button.setTitle("确定", forState: .Normal)
                button.addTarget(self, action: "okButtonTapped", forControlEvents: .TouchUpInside)
                button.layer.cornerRadius = 3.0
                button.formatPrimary()
                return button
                
            } ()
            view.addSubview(okButton)
            self.buttonOK = okButton
            
            return view
        } ()
        
        self.view.addSubview(buttonView)
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "viewDidTap"))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    func setupNavigationBar() {
        self.createBackButton()
    }
    
    //MARK: Actions
    func okButtonTapped() {
        Log.debug("okButtonTapped")
        
        // Vailidate info
        guard isFrontImagePicked else {
            Alert.alertWithSingleButton(self, title: "Title", message: String.localize("MSG_ERR_ID_FRONT_IMAGE_NIL"), buttonString: "OK")
            return
        }
        
        guard isBackImagePicked else {
            Alert.alertWithSingleButton(self, title: "Title", message: String.localize("MSG_ERR_ID_BACK_IMAGE_NIL"), buttonString: "OK")
            return
        }

        guard tfIDNumber.text != "" else {
            Alert.alertWithSingleButton(self, title: "Title", message: String.localize("MSG_ERR_ID_NUMBER_NIL"), buttonString: "OK")
            return
        }
        
        guard tfFirstName.text != "" else {
            Alert.alertWithSingleButton(self, title: "Title", message: String.localize("MSG_ERR_MERCHANT_FIRSTNAME_NIL"), buttonString: "OK")
            return
        }

        guard tfLastName.text != "" else {
            Alert.alertWithSingleButton(self, title: "Title", message: String.localize("MSG_ERR_LASTNAME_NIL"), buttonString: "OK")
            return
        }
        
        let imageFrontData = UIImageJPEGRepresentation(idCardImageFront.image!, 0.1)
        let imageBackData = UIImageJPEGRepresentation(idCardImageBack.image!, 0.1)
        
        if let imgFrontData = imageFrontData, imgBackData = imageBackData {
            
            self.showLoading()
            
            IDCardService.uploadIDCardInfo(
                "",
                firstName: self.tfFirstName.text!,
                lastName: self.tfLastName.text!,
                idNumber: self.tfIDNumber.text!,
                frontImage: imgFrontData,
                backImage: imgBackData,
                success: { [weak self] (response) in
                    
                    if let strongSelf = self {
                        if response.result.isSuccess {
                            if response.response?.statusCode == 200 {
                                let viewController = PaymentMethodSelectionViewController()
                                strongSelf.navigationController?.pushViewController(viewController, animated: true)
                                strongSelf.stopLoading()
                                return
                            }
                        }
                        Log.debug("error")
                        strongSelf.stopLoading()
                    }
                },
                fail: { [weak self] encodingError in
                    Log.debug("encodingError")
                    if let strongSelf = self {
                        strongSelf.stopLoading()
                    }
                })
        }
    }
    
    func toggleCheckbox(sender : UIButton) {
        Log.debug("toggleCheckbox")
        sender.selected = !sender.selected
    }
    
    func linkTapped() {
        Log.debug("Link tapped")
    }
    
    func accessPhotoLibrary() {
        viewDidTap()
        if UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary) {
            self.imagePicker.sourceType = .PhotoLibrary
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        else {
            Alert.alertWithSingleButton(self, title: "Access error", message: "Your device does not support Photo Library", buttonString: "OK")
        }
    }
    
    func accessCamera() {
        viewDidTap()
        if (UIImagePickerController.isSourceTypeAvailable(.Camera)) {
            if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
                
                self.imagePicker.sourceType = .Camera
                self.imagePicker.cameraCaptureMode = .Photo
                self.presentViewController(self.imagePicker, animated: true, completion: {})
                
            } else {
                Alert.alertWithSingleButton(self, title: "Rear camera doesn't exist", message: "Application cannot access the camera", buttonString: "OK")
            }
        } else {
            Alert.alertWithSingleButton(self, title: "Camera inaccessable", message: "Application cannot access the camera", buttonString: "OK")
        }
    }
    
    func viewDidTap() {
        self.view.endEditing(true)
    }
    
    func submitOrder() {
        Log.debug("submit button Tapped")
    }
    
    //MARK: CollectionView
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        switch collectionView {
        case self.collectionView!:
            return 1
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.collectionView!:
            return IndexPathRow.Count.rawValue
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
            
        case self.collectionView!:
            
            var reuseIdentifier : String!
            
            switch indexPath.row {
                
            case IndexPathRow.InfoHeader.rawValue:
                reuseIdentifier = self.IDLabelCellID
                
            case IndexPathRow.FirstCardImage.rawValue, IndexPathRow.SecondCardImage.rawValue:
                reuseIdentifier = self.IDCardImageCellID

            case IndexPathRow.Firstname.rawValue, IndexPathRow.Lastname.rawValue:
                reuseIdentifier = self.IDNameCellID
                
            case IndexPathRow.IdCardNumber.rawValue:
                reuseIdentifier = self.IDCardNumberCellID
                
//            case IndexPathRow.IdTickbox.rawValue:
//                reuseIdentifier = self.IDTickCellID

            default:
                reuseIdentifier = ""
            }
            
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)
            
            if indexPath.row == IndexPathRow.FirstCardImage.rawValue || indexPath.row == IndexPathRow.SecondCardImage.rawValue {
                let cellItem = cell as! IDCardImageCell
                
                cellItem.imageHandler = {
                    
                    if indexPath.row == IndexPathRow.FirstCardImage.rawValue {
                        self.isFrontImageChose = true
                    }
                    else {
                        self.isFrontImageChose = false
                    }
                    
                    Log.debug("image tapped")
                    
                    let alert = UIAlertController(title: "Title", message: "Message", preferredStyle: .Alert)
                    
                    let photoAction = UIAlertAction(title: "Access to Photos", style: .Default, handler: { UIAlertAction in
                        
                        let status = PHPhotoLibrary.authorizationStatus()
                        
                        if status == .NotDetermined {
                            PHPhotoLibrary.requestAuthorization({ (authorizationStatus) -> Void in
                                if authorizationStatus == .Authorized {
                                    self.accessPhotoLibrary()
                                }
                            })
                        }
                        else if status == .Authorized {
                            self.accessPhotoLibrary()
                        }
                        else {
                            Alert.alertWithSingleButton(self, title: "Access error", message: "You can't access to the photo library, please turn it on in Privacy", buttonString: "OK")
                        }
                    })
                    
                    let cameraAction = UIAlertAction(title: "Using Camera", style: .Default, handler: { UIAlertAction in
                        
                        let status = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
                        
                        if status == .NotDetermined {
                            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted) -> Void in
                                if granted {
                                    self.accessCamera()
                                }
                            })
                        }
                        else if status == .Authorized {
                            self.accessCamera()
                        }
                        else {
                            Alert.alertWithSingleButton(self, title: "Access error", message: "You can't access to the camera, please turn it on in Privacy", buttonString: "OK")
                        }
                    })
                    
                    alert.addAction(photoAction)
                    alert.addAction(cameraAction)
                    
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                
                if indexPath.row == IndexPathRow.FirstCardImage.rawValue {
                    idCardImageFront = cellItem.imageView
                    idCardImageFront.image = UIImage(named: "idcard_holder")
                    cellItem.label.text = "编辑"
                }
                else {
                    idCardImageBack = cellItem.imageView
                    idCardImageBack.image = UIImage(named: "idcard_back")
                    cellItem.label.text = "编辑"
                }
            }
//            else if indexPath.row == self.collectionView.numberOfItemsInSection(0) - 1 {
//                let cellItem = cell as! IDTickCell
//                cellItem.checkboxButton.addTarget(self, action: "toggleCheckbox:", forControlEvents: .TouchUpInside)
//                cellItem.linkButton.addTarget(self, action: "linkTapped", forControlEvents: .TouchUpInside)
//            }
            else if indexPath.row >= IndexPathRow.Firstname.rawValue && indexPath.row <= IndexPathRow.IdCardNumber.rawValue {
                if indexPath.row == IndexPathRow.Firstname.rawValue {
                    let cellItem = cell as! IDNameCell
                    cellItem.textField.delegate = self
                    cellItem.titleLabel.text = "姓氏"
                    tfFirstName = cellItem.textField
                } else if indexPath.row == IndexPathRow.Lastname.rawValue {
                    let cellItem = cell as! IDNameCell
                    cellItem.textField.delegate = self
                    cellItem.titleLabel.text = "名字"
                    tfLastName = cellItem.textField
                } else if indexPath.row == IndexPathRow.IdCardNumber.rawValue {
                    let cellItem = cell as! IDCardNumberCell
                    cellItem.textField.delegate = self
                    tfIDNumber = cellItem.textField
                }
            }
            
            return cell
            
        default:
            return self.defaultCell(collectionView, cellForItemAtIndexPath: indexPath)
        }
        
    }
    
    func defaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCellWithReuseIdentifier(DefaultCellID, forIndexPath: indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        switch collectionView {
        case self.collectionView!:
            
            var height : CGFloat!
            var width = self.view.frame.size.width
            switch indexPath.row {
                
            case IndexPathRow.InfoHeader.rawValue:
                height = IDLabelViewHeight
                
            case IndexPathRow.FirstCardImage.rawValue, IndexPathRow.SecondCardImage.rawValue:
                height = IDCardImagelViewHeight
                width = self.view.frame.size.width / 2.0
                
            case IndexPathRow.Firstname.rawValue, IndexPathRow.Lastname.rawValue:
                height = IDNameViewHeight
                
            case IndexPathRow.IdCardNumber.rawValue:
                height = IDCardNumberViewHeight
                
//            case IndexPathRow.IdTickbox.rawValue:
//                height = IDTickViewHeight
                
            default:
                height = 0
                
            }
            
            return CGSizeMake(width, height)
        default:
            return CGSizeZero
        }
        
    }
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {

        if let pickedImage = (info[UIImagePickerControllerOriginalImage]) as? UIImage {
            if isFrontImageChose {
                self.idCardImageFront.image = pickedImage.normalizedImage()
                isFrontImagePicked = true
            }
            else {
                self.idCardImageBack.image = pickedImage.normalizedImage()
                isBackImagePicked = true
            }

        }
        dismissViewControllerAnimated(true, completion: nil)
    
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: View Config
    // config tab bar
    override func shouldHideTabBar() -> Bool {
        return true
    }
    
    override func collectionViewBottomPadding() -> CGFloat {
        return ButtonViewHeight
    }
    
    //MARK: UITextField Delegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    //MARK: Keyboard
    func keyboardWillShow(notification: NSNotification) {
        if let info = notification.userInfo, let kbObj = info[UIKeyboardFrameEndUserInfoKey] {
            var kbRect = kbObj.CGRectValue
            kbRect = self.view.convertRect(kbRect, fromView: nil)
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height - ButtonViewHeight, 0.0)
            
            self.collectionView.contentInset = contentInsets
            self.collectionView.scrollIndicatorInsets = contentInsets;
            
            var aRect = self.view.frame;
            aRect.size.height -= kbRect.size.height - ButtonViewHeight;
            
            if !CGRectContainsPoint(aRect, activeTextField.frame.origin) {
                self.collectionView.scrollRectToVisible(activeTextField.frame, animated:true);
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.collectionView.contentInset = UIEdgeInsetsZero;
        self.collectionView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }
}