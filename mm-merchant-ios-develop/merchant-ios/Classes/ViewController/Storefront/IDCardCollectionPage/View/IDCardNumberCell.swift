//
//  IDCardNumberCell.swift
//  merchant-ios
//
//  Created by HungPM on 2/22/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

var IDCardNumberViewHeight = CGFloat(84)

class IDCardNumberCell : UICollectionViewCell {
    
    var textField: UITextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRectMake(0, 0, frame.width, IDCardNumberViewHeight)
        self.backgroundColor = UIColor.whiteColor()
        
        let marginLeft = CGFloat(11)
        let marginTop = CGFloat(19)

        let viewContainer = { () -> UIView in
            let view = UIView(frame: CGRectMake(marginLeft, marginTop, frame.width - 2 * marginLeft, IDCardNumberViewHeight - 2 * marginTop))
            view.layer.borderWidth = 1.0
            view.layer.borderColor = UIColor.secondary1().CGColor
            
            self.textField = { () -> UITextField in
                let Padding = CGFloat(22)
                let view = UITextField(frame: CGRectMake(Padding, 0, view.frame.width - 2 * Padding, view.frame.height))
                view.placeholder = "身份证号码"
                view.textColor = UIColor.secondary3()
                return view
                } ()
            view.addSubview(self.textField)
            
            return view
        } ()
        addSubview(viewContainer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}