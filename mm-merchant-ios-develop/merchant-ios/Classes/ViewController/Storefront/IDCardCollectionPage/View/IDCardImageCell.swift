//
//  IDCardImageCell.swift
//  merchant-ios
//
//  Created by HungPM on 2/22/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

let IDCardImagelViewHeight = CGFloat(174)

class IDCardImageCell : UICollectionViewCell {
    
    var imageView: UIImageView!
    var label: UILabel!

    var imageHandler: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRectMake(0, 0, frame.width, IDCardImagelViewHeight)
        self.backgroundColor = UIColor.whiteColor()
        
        let marginBot = CGFloat(18)
        let LabelHeight = CGFloat(16)
        
        self.label = { () -> UILabel in
            let view = UILabel(frame: CGRectMake(0, IDCardImagelViewHeight - marginBot - LabelHeight, self.frame.width, LabelHeight))
            view.textAlignment = .Center
            view.formatSize(10)
            view.textColor = UIColor.secondary2()
            return view
        } ()
        
        addSubview(self.label)

        let marginTop = CGFloat(25)
        let marginLeft = CGFloat(7)
        let marginRight = CGFloat(7)
        let marginWithLabel = CGFloat(5)
        
        self.imageView = { () -> UIImageView in
            let view = UIImageView(frame: CGRectMake(marginLeft, marginTop, self.frame.width - marginLeft - marginRight, self.label.frame.origin.y - marginWithLabel - marginTop))
            view.contentMode = .ScaleAspectFit
            view.image = UIImage(named: "Spacer")

            view.userInteractionEnabled = true
            let singleTap = UITapGestureRecognizer(target: self, action: "imageTapped")
            view.addGestureRecognizer(singleTap)

            return view
            } ()
        addSubview(self.imageView)
        
        let separatorHeight = CGFloat(1)
        let paddingTop = CGFloat(7)
        
        let separatorView = { () -> UIView in
            let view = UIView(frame: CGRectMake(0, self.label.frame.maxY + paddingTop, frame.width, separatorHeight))
            view.backgroundColor = UIColor.backgroundGray()
            
            return view
        } ()
        addSubview(separatorView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func imageTapped() {
        if let callback = self.imageHandler {
            callback()
        }
    }
}