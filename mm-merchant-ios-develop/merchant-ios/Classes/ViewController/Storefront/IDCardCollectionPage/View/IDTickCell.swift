//
//  IDTickCell.swift
//  merchant-ios
//
//  Created by HungPM on 2/22/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

var IDTickViewHeight = CGFloat(28)

class IDTickCell : UICollectionViewCell {
    
    var checkboxButton : UIButton!
    var linkButton : UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRectMake(0, 0, frame.width, IDTickViewHeight)
        self.backgroundColor = UIColor.whiteColor()
        
        //Create checkbox button
        self.checkboxButton = { () -> UIButton in
            let MarginLeft = CGFloat(20)
            
            let btn = UIButton(type: .Custom)
            btn.frame = CGRectMake(MarginLeft, 0, 0, 0)
            btn.setImage(UIImage(named: "square_check_box"), forState: UIControlState.Normal)
            btn.setImage(UIImage(named: "square_check_box_selected"), forState: UIControlState.Selected)
            btn.setTitle(" " + String.localize("LB_CA_TNC_CHECK"), forState: UIControlState.Normal)
            btn.setTitleColor(UIColor.secondary2(), forState: UIControlState.Normal)
            btn.titleLabel?.formatSize(11)
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
            btn.sizeToFit()
            
            btn.frame = CGRectMake(MarginLeft, (IDTickViewHeight - btn.frame.height) / 2, btn.frame.width, btn.frame.height)

            return btn
        } ()
        addSubview(self.checkboxButton)
        
        //Create link button
        self.linkButton = { () -> UIButton in
            let btn = UIButton(type: .Custom)
            btn.frame = CGRectMake(self.checkboxButton.frame.maxX, 0, 0, 0)
            
            btn.setTitle(String.localize("LB_CA_TNC_LINK"), forState: UIControlState.Normal)
            btn.titleLabel?.formatSize(11)
            btn.setTitleColor(UIColor.primary1(), forState: UIControlState.Normal)
            btn.sizeToFit()
            
            btn.frame = CGRectMake(self.checkboxButton.frame.maxX, (IDTickViewHeight - btn.frame.height) / 2, btn.frame.width, btn.frame.height)

            return btn
        } ()
        addSubview(self.linkButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}