//
//  IDNameCell.swift
//  merchant-ios
//
//  Created by HungPM on 2/22/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

var IDNameViewHeight = CGFloat(46)

class IDNameCell : UICollectionViewCell {
    
    var textField: UITextField!
    var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRectMake(0, 0, frame.width, IDNameViewHeight)
        self.backgroundColor = UIColor.whiteColor()
        
        let margin = CGFloat(11)
        
        let viewContainer = { () -> UIView in
            let view = UIView(frame: CGRectMake(margin, 0, frame.width - 2 * margin, IDNameViewHeight))
            view.layer.borderWidth = 1.0
            view.layer.borderColor = UIColor.secondary1().CGColor
            
            titleLabel = { () -> UILabel in
                let view = UILabel(frame: CGRectMake(0, 0, 73, IDNameViewHeight))
                view.textAlignment = .Center
                view.formatSize(14)
                view.textColor = UIColor.secondary2()
                return view
            } ()
            
            view.addSubview(titleLabel)
            
            let separatorWidth = CGFloat(1)
            let Padding = CGFloat(7)
            
            let separatorView = { () -> UIView in
                let view = UIView(frame: CGRectMake(titleLabel.frame.maxX, Padding, separatorWidth, IDNameViewHeight - 2 * Padding))
                view.backgroundColor = UIColor.backgroundGray()
                return view
            } ()
            view.addSubview(separatorView)

            self.textField = { () -> UITextField in
                let xPos = separatorView.frame.maxX + Padding
                let view = UITextField(frame: CGRectMake(xPos, 0, view.frame.width - xPos - Padding, IDNameViewHeight))
                view.textColor = UIColor.secondary3()
                return view
            } ()
            view.addSubview(self.textField)
            
            return view
        } ()
        addSubview(viewContainer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}