//
//  IDLabelCell.swift
//  merchant-ios
//
//  Created by HungPM on 2/22/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

var IDLabelViewHeight = CGFloat(24)

class IDLabelCell : UICollectionViewCell {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRectMake(0, 0, frame.width, IDLabelViewHeight)
        backgroundColor = UIColor.backgroundGray()
        
        let label = UILabel(frame: self.frame)
        label.text = "身份认证之后可以进行支付购买"
        label.textAlignment = .Center
        label.formatSize(10)
        label.textColor = UIColor.secondary2()
        self.contentView.addSubview(label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}