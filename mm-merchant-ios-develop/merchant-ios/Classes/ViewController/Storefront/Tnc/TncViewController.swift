//
//  TncViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 3/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class TncViewController : MmViewController, UIWebViewDelegate {
    var isNavigationBarHidden = false
    private final let WebviewMarginTop : CGFloat = 65
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = String.localize("LB_CA_TNC")
        let webview = UIWebView(frame: CGRect(x: 0, y: WebviewMarginTop, width: self.view.bounds.width, height: self.view.bounds.height - WebviewMarginTop))
        webview.delegate = self
        let url = NSBundle.mainBundle().URLForResource("mm_tnc", withExtension:"html")
        let request = NSURLRequest(URL: url!)
        webview.loadRequest(request)
        self.view.addSubview(webview)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        isNavigationBarHidden = (self.navigationController?.navigationBarHidden)!
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(isNavigationBarHidden, animated: false)
    }
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.LinkClicked {
            let webViewController = WebViewController()
            webViewController.url = request.URL! //NSURL(string:"http://www.google.com")
            self.navigationController?.pushViewController(webViewController, animated: true)
            return false
        }
        return true
    }
}