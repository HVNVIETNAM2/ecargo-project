//
//  WebViewController.swift
//  merchant-ios
//
//  Created by Sang on 2/4/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class WebViewController : MmViewController {
    var isNavigationBarHidden = false
    private final let WebviewMarginTop : CGFloat = 65
    var url: NSURL?
    var webview = UIWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        webview.frame = CGRect(x: 0, y: WebviewMarginTop, width: self.view.bounds.width, height: self.view.bounds.height - WebviewMarginTop)
        self.view.addSubview(webview)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        isNavigationBarHidden = (self.navigationController?.navigationBarHidden)!
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        if self.url != nil {
            self.title = url?.host
            webview.loadRequest(NSURLRequest(URL: url!))
        }
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(isNavigationBarHidden, animated: false)
    }
}