//
//  ShoppingCartSectionHeaderView.swift
//  merchant-ios
//
//  Created by Alan YU on 7/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

let ShoppingCartSectionViewID = "ShoppingCartSectionViewID"
let ShoppingCartSectionViewHeight = CGFloat(54)
let ShoppingCartSectionCheckBoxWidth = CGFloat(50)
let ShoppingCartSectionMerchantImageWidth = CGFloat(110)
let ShoppingCartSectionArrowWidth = CGFloat(32)

class ShoppingCartSectionHeaderView: UICollectionReusableView {
    
    var selectAllCartItemButton: UIButton!
    var merchantImageView: UIImageView!
    var merchantNameLabel: UILabel!
    var cartItemSelectHandler: ((data: ShoppingCartSectionData) -> Void)?
    
    var headerTappedHandler: ((data: ShoppingCartSectionData) -> Void)?

    var data: ShoppingCartSectionData? {
        didSet {
            if let  data = self.data {
                self.allCartItemSelected = data.sectionSelected
                
                if let merchantImageKey = self.data?.merchant?.merchantImage {
                    self.merchantImageView.af_setImageWithURL(ImageURLFactory.get(merchantImageKey, category: .Merchant), placeholderImage : UIImage(named: "holder"))
                }
                
                merchantNameLabel.text = self.data?.merchant?.merchantName
            }
        }
    }
    
    private var allCartItemSelected = false {
        didSet {
            self.selectAllCartItemButton.selected = self.allCartItemSelected
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.backgroundGray()
        self.frame = CGRectMake(0, 0, frame.width, ShoppingCartSectionViewHeight)
        
        let containerView = { () -> UIView in
            
            let yPadding = CGFloat(10)
            let containerHeight = ShoppingCartSectionViewHeight - yPadding
            let view = UIView(frame: CGRectMake(0, yPadding, frame.width, containerHeight))
            
            view.backgroundColor = UIColor.whiteColor()
            
            let merchantNameWidth = view.bounds.width - ShoppingCartSectionCheckBoxWidth - ShoppingCartSectionMerchantImageWidth - ShoppingCartSectionArrowWidth
            
            //
            let checkBoxContainer = { () -> UIView in
                
                let view = CenterLayoutView(frame: CGRectMake(0, 0, ShoppingCartSectionCheckBoxWidth, containerHeight))
            
                let button = UIButton(type: .Custom)
                button.config(
                    normalImage: UIImage(named: "icon_checkbox_unchecked"),
                    selectedImage: UIImage(named: "icon_checkbox_checked")
                )
                button.sizeToFit()
                button.addTarget(self, action: "toggleSelectAllCartItems:", forControlEvents: .TouchUpInside)
                
                view.addSubview(button)
                self.selectAllCartItemButton = button
                
                return view
            } ()
            view.addSubview(checkBoxContainer)
            
            //
            let merchantImageConatinerView = { () -> UIView in
                
                let view = UIView(frame: CGRectMake(checkBoxContainer.frame.maxX, 0, ShoppingCartSectionMerchantImageWidth, containerHeight))
                
                let merchantImageTopPadding = CGFloat(2)
                let merchantImageLeftPadding = CGFloat(10)
                let merchantImageRightPadding = CGFloat(20)
                
                let imageView = UIImageView(frame:
                    UIEdgeInsetsInsetRect(
                        view.bounds,
                        UIEdgeInsetsMake(merchantImageTopPadding, merchantImageLeftPadding, merchantImageTopPadding, merchantImageRightPadding)
                    )
                )
                
                imageView.contentMode = .ScaleAspectFit
                //FIXME: uncomment and replace placeholder image here if need
//                imageView.image = UIImage(named: "placeholder")
                
                view.addSubview(imageView)
                self.merchantImageView = imageView
                
                return view
            } ()
            view.addSubview(merchantImageConatinerView)
            
            //
            self.merchantNameLabel = { () -> UILabel in
                
                let label = UILabel(frame: CGRectMake(merchantImageConatinerView.frame.maxX, 0, merchantNameWidth, containerHeight))
                label.adjustsFontSizeToFitWidth = true
                label.formatSmall()
                return label
            } ()
            view.addSubview(self.merchantNameLabel)
            
            //
            let arrowRightMargin = CGFloat(16)
            let arrowView = { () -> UIImageView in
                let imageView = UIImageView(frame: CGRectMake(frame.width - ShoppingCartSectionArrowWidth - arrowRightMargin, (containerHeight - ShoppingCartSectionArrowWidth) / 2, ShoppingCartSectionArrowWidth, ShoppingCartSectionArrowWidth))
                imageView.image = UIImage(named: "icon_arrow_small")
                imageView.contentMode = .ScaleAspectFit
                return imageView
            } ()
            view.addSubview(arrowView)
            
            // clear button cover from Merchant image to right arrow. Tap to go to the Merchant Public Profile Page
            let clearButton = { () -> UIButton in
                let button = UIButton(type: .Custom)
                button.frame = CGRectMake(merchantImageConatinerView.frame.minX, 0, frame.width - merchantImageConatinerView.frame.minX, frame.height)
                button.backgroundColor = UIColor.clearColor()
                button.addTarget(self, action: "headerTapped", forControlEvents: .TouchUpInside)
                return button
            } ()
            view.addSubview(clearButton)
            
            let separatorHeight = CGFloat(1)
            let separatorView = { () -> UIView in
                let view = UIView(frame: CGRectMake(0, self.merchantNameLabel.frame.maxY - separatorHeight, frame.width, separatorHeight))
                view.backgroundColor = UIColor.backgroundGray()
                
                return view
            } ()
            view.addSubview(separatorView)

            return view
        } ()
        self.addSubview(containerView)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func toggleSelectAllCartItems(button: UIButton) {
        
        if self.allCartItemSelected {
            self.allCartItemSelected = false
        } else {
            self.allCartItemSelected = true
        }
        
        if let data = self.data {
            for row in data.dataSource {
                (row as! CartItem).selected = self.allCartItemSelected
            }
            
            if let callback = self.cartItemSelectHandler {
                callback(data: data)
            }
        }
        
    }
    
    func headerTapped() {
        if let callback = self.headerTappedHandler, let data = self.data {
            callback(data: data)
        }
    }

}