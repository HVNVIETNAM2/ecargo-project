//
//  ShoppingCartItemCell.swift
//  merchant-ios
//
//  Created by Alan YU on 5/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

let ShoppingCartCellHeight : CGFloat = 155

class ShoppingCartItemCell: SwipeActionMenuCell {
    
    private var productImageView: UIImageView!
    private var productNameLabel: UILabel!
    private var productColorLabel: UILabel!
    private var productSizeLabel: UILabel!
    private var productQtyLabel: UILabel!
    private var productPriceLabel: UILabel!
    private var productSelectButton: UIButton!
    private var productBadgeView: UIView!
    private var productEditButton: UIButton!
    private var brandImageView : UIImageView!
    
    private var observerContext = 0
    
    var productImageHandler: ((data: CartItem) -> Void)?
    var editHandler: ((data: CartItem) -> Void)?
    var cartItemSelectHandler: ((data: CartItem) -> Void)?
    
    var data: CartItem? {
        didSet {
            if let data = self.data {
                self.productNameLabel.text = data.skuName

                let existOrEmpty = { (value: String?) -> String in
                    if value != nil {
                        return value!
                    }
                    return ""
                }
                
                self.productSizeLabel.text = String.localize("LB_CA_PI_SIZE") + " : " + existOrEmpty(data.sizeName)
                self.productColorLabel.text = String.localize("LB_CA_PI_COLOR") + " : " + existOrEmpty(data.colorName)
                self.productQtyLabel.text = String.localize("LB_CA_PI_QTY") + " : " + existOrEmpty(data.qty.formatQuantity())
                self.productImageView.af_setImageWithURL(
                    ImageURLFactory.get(data.productImage, category: .Product),
                    placeholderImage: UIImage(named: "holder"),
                    filter: nil,
                    imageTransition: .CrossDissolve(0.3),
                    completion: nil
                )
                self.brandImageView.af_setImageWithURL(
                    ImageURLFactory.get(data.brandImage, category: .Brand),
                    placeholderImage: UIImage(named: "holder"),
                    filter: nil,
                    imageTransition: .CrossDissolve(0.3),
                    completion: { (response) -> Void in
                    
                    if let image = response.result.value {
                        let imageWidth = image.size.width
                        let imageHeight = image.size.height
                        let imageRatio = imageWidth / imageHeight

                        self.brandImageView.frame = CGRectMake(self.brandImageView.frame.origin.x, self.brandImageView.frame.origin.y, self.brandImageView.frame.size.height * imageRatio, self.brandImageView.frame.size.height)
                    }
                    
                })
                self.setCartItemSelected(data.selected)
                
                let priceText = NSMutableAttributedString()
                
                let saleFont = UIFont.systemFontOfSize(16)
                let retailFont = UIFont.systemFontOfSize(11)
                
                if data.priceSale > 0 {
                    if let priceSale = data.priceSale.formatPrice() {
                        
                        let saleText = NSAttributedString(
                            string: priceSale + " ",
                            attributes: [
                                NSForegroundColorAttributeName: UIColor(hexString: "#DB1E49"),
                                NSFontAttributeName: saleFont,
                                NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleNone.rawValue
                            ]
                        )
                        priceText.appendAttributedString(saleText)
                    }
                    
                    if let priceRetail = data.priceRetail.formatPrice() where data.priceRetail != data.priceSale {
                        
                        let retailText = NSAttributedString(
                            string: priceRetail,
                            attributes: [
                                NSForegroundColorAttributeName: UIColor(hexString: "#757575"),
                                NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue,
                                NSFontAttributeName: retailFont,
                                NSBaselineOffsetAttributeName: (saleFont.capHeight - retailFont.capHeight) / 2
                            ]
                        )
                        
                        priceText.appendAttributedString(retailText)
                    }
                    
                } else {
                    
                    if let priceRetail = data.priceRetail.formatPrice() where data.priceRetail != data.priceSale {
                        
                        let retailText = NSAttributedString(
                            string: priceRetail,
                            attributes: [
                                NSForegroundColorAttributeName: UIColor(hexString: "#4A4A4A"),
                                NSFontAttributeName: saleFont
                            ]
                        )
                        priceText.appendAttributedString(retailText)
                    }
                }
                self.productPriceLabel.attributedText = priceText
                
                productBadgeView.hidden = data.isProductAvailabel()
                productEditButton.hidden = !data.isProductValid()
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        /*
        ___________________
        |  |______________|
        |  |   |_C________|
        |A |B  |_D________|
        |  |   |_E________|
        |  |___|_F________|
        |__|______________|
        */
        
        let separatorHeight = CGFloat(1)
        let contentHeight = ShoppingCartCellHeight - separatorHeight
        let badgeHeight = CGFloat(50)
        let containerTopPadding = CGFloat(16)

        // A
        let checkBoxContainer = { () -> UIView in
            
            let view = UIView(frame: CGRectMake(0, 0, ShoppingCartSectionCheckBoxWidth, contentHeight))
            
            let badgeViewContainer = UIView(frame: CGRectMake(0, containerTopPadding, ShoppingCartSectionCheckBoxWidth, badgeHeight))
            badgeViewContainer.backgroundColor = UIColor.backgroundGray()
            
            let badgeItemLabel = UILabel(frame: badgeViewContainer.bounds)
            badgeItemLabel.format()
            badgeItemLabel.textAlignment = .Center
            //FIXME: Change the label id here
            badgeItemLabel.text = String.localize("失效")
            badgeViewContainer.hidden = true
            
            badgeViewContainer.addSubview(badgeItemLabel)
            view.addSubview(badgeViewContainer)
            self.productBadgeView = badgeViewContainer
            
            let button = UIButton(type: .Custom)
            button.config(
                normalImage: UIImage(named: "icon_checkbox_unchecked"),
                selectedImage: UIImage(named: "icon_checkbox_checked")
            )
            button.addTarget(self, action: "toggleCartItemSelected", forControlEvents: .TouchUpInside)
            button.sizeToFit()
            button.frame = CGRectMake((ShoppingCartSectionCheckBoxWidth - button.frame.width) / 2, badgeViewContainer.frame.maxY, button.frame.width, button.frame.height)
            
            view.addSubview(button)
            self.productSelectButton = button
            
            return view
        } ()
        self.contentView.addSubview(checkBoxContainer)
        
        //
        let rightContainer = { () -> UIView in
            
            let containerHeight = CGFloat(100)
            
            let view = UIView(frame: CGRectMake(checkBoxContainer.frame.maxX, containerTopPadding, frame.width - ShoppingCartSectionCheckBoxWidth, containerHeight))
            
            // B
            let productImageContainerView = { () -> UIView in
                
                let view = UIView(frame: CGRectMake(0, 0, ShoppingCartSectionMerchantImageWidth, containerHeight))
                let merchantImageRightPadding = CGFloat(20)
                
                let imageView = UIImageView(frame:
                    UIEdgeInsetsInsetRect(
                        view.bounds,
                        UIEdgeInsetsMake(0, 0, 0, merchantImageRightPadding)
                    )
                )
                
                imageView.contentMode = .ScaleAspectFit
                let singleTap = UITapGestureRecognizer(target: self, action: "productImageTapped")
                view.addGestureRecognizer(singleTap)
                
                view.addSubview(imageView)
                self.productImageView = imageView
                
                return view
            } ()
            view.addSubview(productImageContainerView)
            
            //
            let detailWidth = view.frame.width - ShoppingCartSectionMerchantImageWidth
            let halfDetailWidth = detailWidth / 2
            let detailHeight = CGFloat(18)
            let detailFontSize = 14
            
            let detailViewContainer = { () -> UIView in
                
                let view = UIView(frame: CGRectMake(productImageContainerView.frame.maxX, 0, detailWidth, containerHeight))
                
                //
                let brandImageView = { () -> UIImageView in
                    
                    let imageView = UIImageView(frame: CGRectMake(0, 0, 100, 28))
                    self.brandImageView = imageView
                    
                    return imageView
                } ()
                view.addSubview(brandImageView)
                
                // C
                let productNameLabel = { () -> UILabel in
                    
                    let label = UILabel(frame: CGRectMake(0, brandImageView.frame.maxY, detailWidth, detailHeight))
                    label.formatSize(detailFontSize)
                    label.text = "Kate Spade"
                    
                    return label
                } ()
                view.addSubview(productNameLabel)
                self.productNameLabel = productNameLabel
                
                // D
                let colorLabel = { () -> UILabel in
                    
                    let label = UILabel(frame: CGRectMake(0, productNameLabel.frame.maxY, halfDetailWidth, detailHeight))
                    label.formatSize(detailFontSize)
                    label.text = "Color: Red"
                    
                    return label
                } ()
                view.addSubview(colorLabel)
                self.productColorLabel = colorLabel
                
                let sizeLabel = { () -> UILabel in
                    
                    let label = UILabel(frame: CGRectMake(colorLabel.frame.maxX, productNameLabel.frame.maxY, halfDetailWidth, detailHeight))
                    label.formatSize(detailFontSize)
                    label.text = "Size: 6"
                    
                    return label
                } ()
                view.addSubview(sizeLabel)
                self.productSizeLabel = sizeLabel
                
                // E
                let qtyLabel = { () -> UILabel in
                    
                    let label = UILabel(frame: CGRectMake(0, colorLabel.frame.maxY, detailWidth, detailHeight))
                    label.formatSize(detailFontSize)
                    label.text = "Qty: 2"
                    
                    return label
                } ()
                view.addSubview(qtyLabel)
                self.productQtyLabel = qtyLabel
                
                // F
                let priceLabel = { () -> UILabel in
                    let label = UILabel(frame: CGRectMake(0, qtyLabel.frame.maxY, detailWidth, containerHeight - qtyLabel.frame.maxY))
                    label.escapeFontSubstitution = true
                    label.text = "$4,000"
                    return label
                } ()
                view.addSubview(priceLabel)
                self.productPriceLabel = priceLabel
                
                return view
                
            } ()
            view.addSubview(detailViewContainer)
            
            return view
        } ()
        self.contentView.addSubview(rightContainer)
        
        //
        let editButton = { () -> UIButton in
            
            let rightMargin = CGFloat(18)
            let topMargin = CGFloat(5)
            let width = CGFloat(64)
            let height = CGFloat(25)
            let xPos = frame.width - width - rightMargin
            let yPos = rightContainer.frame.maxY + topMargin
            
            let button = UIButton(type: .Custom)
            button.frame = CGRectMake(xPos, yPos, width, height)
            button.setTitle(String.localize("LB_CA_EDIT"), forState: .Normal)
            button.setTitleColor(UIColor.primary1(), forState: .Normal)
            button.titleLabel?.formatSize(14)
            button.addTarget(self, action: "edit", forControlEvents: .TouchUpInside)
            button.layer.cornerRadius = 5
            button.layer.borderColor = UIColor.secondary2().CGColor
            button.layer.borderWidth = 0.7
            button.hidden = true
            self.productEditButton = button
            self.productEditButton.accessibilityIdentifier = "cart_cell_edit_button"
            
            return button
        } ()
        self.contentView.addSubview(editButton)
        
        //
        let separatorView = { () -> UIView in
            
            let view = UIView(frame: CGRectMake(0, checkBoxContainer.frame.maxY, frame.width, separatorHeight))
            view.backgroundColor = UIColor.backgroundGray()
            
            return view
        } ()
        self.contentView.addSubview(separatorView)
    }
       
    func toggleCartItemSelected() {
        if let value = self.data {
            self.setCartItemSelected(!value.selected)
        }
        
        if let callback = self.cartItemSelectHandler, let data = self.data {
            callback(data: data)
        }
    }
    
    private func setCartItemSelected(value: Bool) {
        self.data?.selected = value
        self.productSelectButton.selected = value
    }
    
    func edit() {
        if let callback = self.editHandler, let data = self.data {
            callback(data: data)
        }
    }
    
    func productImageTapped() {
        if let callback = self.productImageHandler, let data = self.data {
            callback(data: data)
        }
    }
    
    func accessibilityIdentifierIndex(index: Int) {
        accessibilityIdentifier = "ShoppingCartCell-\(index)"
        productQtyLabel.accessibilityIdentifier = "ShoppingCartCellQtyLabel-\(index)"
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}