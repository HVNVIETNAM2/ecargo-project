//
//  ShoppingCartPromotionCell.swift
//  merchant-ios
//
//  Created by Alan YU on 6/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit
import Foundation

class ShoppingCartPromotionView : UIView {
    
    var promotionBannerImageView: UIImageView!
    var promotionIcon: UIImageView!!
    var promotionTextLabel: UILabel!
    var promotionTextField: UITextField!
    var promotionTextFieldImage: UIImageView!
    
    let bannerRatio = CGFloat(100.0 / 375.0)
    let LowerViewHeight = CGFloat(59)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.whiteColor()
        
        // upper
        self.promotionBannerImageView = { () -> UIImageView in
            let view = UIImageView(frame: CGRectMake(0, 0, self.frame.width, self.frame.width * self.bannerRatio))
            view.contentMode = .ScaleAspectFit
            view.image = UIImage(named:"banner_shoppingCart")
            return view
            } ()
        self.addSubview(self.promotionBannerImageView)
        
        self.frame = CGRectMake(0, 0, frame.width, self.promotionBannerImageView.frame.maxY + LowerViewHeight)

        // lower
        let promotionCodeContainerView = { () -> UIView in
            let view = UIView(
                frame: CGRectMake(
                    0,
                    self.promotionBannerImageView.frame.maxY,
                    self.frame.width,
                    self.frame.height - self.promotionBannerImageView.frame.maxY
                )
            )
            return view
        } ()
        self.addSubview(promotionCodeContainerView)
        
        // lower left
        let leftWidthRatio = CGFloat(0.4)
        let promotionCodeLeftView = { () -> UIView in
            let view = CenterLayoutView(frame: CGRectMake(0, 0, promotionCodeContainerView.bounds.width * leftWidthRatio, promotionCodeContainerView.bounds.height))
            
            let icon = UIImageView(image: UIImage(named: "icon_gift_small"))
            view.addSubview(icon)
            
            let label = UILabel(frame: CGRectZero)
            label.formatSize(13)
            label.text = String.localize("LB_CA_USE_COUPON") + ":"
            label.sizeToFit()
            view.addSubview(label)
            
            return view
        } ()
        promotionCodeContainerView.addSubview(promotionCodeLeftView)
        
        // lower right
        let promotionCodeRightView = { () -> UIView in
            
            let view = CenterLayoutView(frame: CGRectMake(promotionCodeLeftView.frame.maxX, 0, promotionCodeContainerView.bounds.width - promotionCodeLeftView.frame.maxX, promotionCodeContainerView.bounds.height))
            view.mode = .Central
            
            let background = UIImageView(image: UIImage(named: "input_coupon"))
            view.addSubview(background)
            
            let padding = CGFloat(10)
            let inputField = UITextField(frame: UIEdgeInsetsInsetRect(background.bounds, UIEdgeInsetsMake(0, padding, 0, padding)))
            inputField.textAlignment = .Center
            inputField.font = UIFont.systemFontOfSize(13)
            
            let attPlaceholder = NSAttributedString(string: String.localize("LB_CA_INPUT_COUPON"), attributes: [NSForegroundColorAttributeName : UIColor.secondary2(),
                NSFontAttributeName: UIFont.systemFontOfSize(13)])
            inputField.attributedPlaceholder = attPlaceholder
            view.addSubview(inputField)
            Log.debug("frame = \(inputField.frame)")
            Log.debug("frame background = \(background.frame)")

            return view
        } ()
        promotionCodeContainerView.addSubview(promotionCodeRightView)
        Log.debug("frame promotionCodeRightView = \(promotionCodeRightView.frame)")

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
