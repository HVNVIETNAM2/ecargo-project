//
//  ShoppingCartViewController.swift
//  merchant-ios
//
//  Created by Alan YU on 28/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class ShoppingCartViewController: MmCartViewController, UIGestureRecognizerDelegate {
    
    private final let ShoppingCartItemCellID = "ShoppingCartItemCellID"
    private final let ShoppingCartPromotionCellID = "ShoppingCartPromotionCellID"
    private final let DefaultCellID = "DefaultCellID"
    
    private final let SummaryViewHeight = CGFloat(62)
    
    private var promotionView: ShoppingCartPromotionView!
    private var dataSource: [ShoppingCartSectionData]!
    private var selectAllCartItemButton: UIButton!
    private var allCartItemSelected = false {
        didSet {
            self.selectAllCartItemButton.selected = allCartItemSelected
        }
    }
    private var summaryLabel: UILabel!
    private var buttonConfirmPurchase : UIButton!
        
    private var cart : Cart?
    private var listMerchant : [CartMerchant] = [CartMerchant]()

    private var listCartItemIdSelected : [Int] = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBarHidden = false
        self.title = String.localize("LB_CA_CART")
        self.view.backgroundColor = UIColor.backgroundGray()
        
        self.promotionView = ShoppingCartPromotionView.viewWithScreenSize()
        
        setupNavigationBar()

        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: self.ShoppingCartPromotionCellID)
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: self.DefaultCellID)
        self.collectionView!.registerClass(ShoppingCartItemCell.self, forCellWithReuseIdentifier: self.ShoppingCartItemCellID)
        self.collectionView!.registerClass(ShoppingCartSectionHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ShoppingCartSectionViewID)
       
        self.collectionView.backgroundColor = UIColor.clearColor()
        self.collectionView.alwaysBounceVertical = true
        self.dataSource = [ShoppingCartSectionData]()
        
        // FIXME: Delete me!!
        for section in self.dataSource {
            section.sectionSelected = self.allCartItemSelected
        }
        //
                
        let summaryView = { () -> UIView in
            
            let frame = CGRectMake(0, self.collectionView.frame.maxY, self.collectionView.frame.width, SummaryViewHeight)
            
            let view = UIView(frame: frame)
            view.backgroundColor = UIColor.whiteColor()
            
            let separatorView = { () -> UIView in
                
                let view = UIView(frame: CGRectMake(0, 0, frame.width, 1))
                view.backgroundColor = UIColor.backgroundGray()
                
                return view
            } ()
            view.addSubview(separatorView)

            //
            let checkBoxContainer = { () -> UIView in
                
                let view = CenterLayoutView(frame: CGRectMake(0, 0, ShoppingCartSectionCheckBoxWidth, frame.height))
                
                let button = UIButton(type: .Custom)
                button.config(
                    normalImage: UIImage(named: "icon_checkbox_unchecked"),
                    selectedImage: UIImage(named: "icon_checkbox_checked")
                )
                button.sizeToFit()
                button.addTarget(self, action: "toggleSelectAllCartItems:", forControlEvents: .TouchUpInside)
                view.addSubview(button)
                self.selectAllCartItemButton = button
                
                return view
            } ()
            view.addSubview(checkBoxContainer)
            
            //
            let selectAllLabel = { () -> UILabel in
                let label = UILabel(frame: CGRectZero)
                label.text = String.localize("LB_CA_SELECT_ALL_PI")
                label.formatSmall()
                label.sizeToFit()
                
                label.frame = CGRectMake(checkBoxContainer.frame.maxX, 0, label.frame.width, frame.height)
                return label
                
            } ()
            view.addSubview(selectAllLabel)
            
            //
            let confirmButton = { () -> UIButton in
                
                let rightPadding = CGFloat(15)
                let buttonSize = CGSizeMake(105, 38)
                let xPos = frame.width - buttonSize.width - rightPadding
                let yPos = (frame.height - buttonSize.height) / 2
                
                let button = UIButton(type: .Custom)
                button.frame = CGRectMake(xPos, yPos, buttonSize.width, buttonSize.height)
                button.setTitle(String.localize("LB_CA_CFM_PURCHASE"), forState: .Normal)
                button.addTarget(self, action: "confirmPurchase", forControlEvents: .TouchUpInside)
                button.formatPrimary()
                return button
                
            } ()
            view.addSubview(confirmButton)
            self.buttonConfirmPurchase = confirmButton
            
            //
            let summaryLabel = { () -> UILabel in
                
                let padding = CGFloat(15)
                let label = UILabel(
                    frame:
                    UIEdgeInsetsInsetRect(
                        CGRectMake(
                            selectAllLabel.frame.maxX,
                            0,
                            confirmButton.frame.minX - selectAllLabel.frame.maxX,
                            frame.height
                        ),
                        UIEdgeInsetsMake(0, padding, 0, padding)
                    )
                    
                )
                label.textAlignment = .Right
                label.formatSingleLine(16)
                return label
                
            } ()
            view.addSubview(summaryLabel)
            self.summaryLabel = summaryLabel
            
            return view
        } ()
        
        self.view.addSubview(summaryView)
        self.updateSummaryPrice()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "viewDidTap"))
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadDataSource()
        if Context.getAuthenticatedUser() || (Context.anonymousShoppingCartKey() != nil && Context.anonymousShoppingCartKey() != "0") {
            self.showLoading()
            firstly {
                return self.listCartItem()
                }.then { _ -> Void in
                    self.cart = CacheManager.sharedManager.cart
                    self.reloadDataSource()
                    self.loadSelectedCache()
                }.always {
                    self.stopLoading()
                }.error { _ -> Void in
                    Log.error("error")
            }
        }
    }
    
    func loadSelectedCache() {
        for section in self.dataSource {
            for row in section.dataSource {
                if row.dynamicType == CartItem.self {
                    for cartItemIdSelected in listCartItemIdSelected {
                        if (row as! CartItem).cartItemId == cartItemIdSelected {
                            (row as! CartItem).selected = true
                        }
                    }
                }
            }
        }
        self.reloadSelectStatus()
    }
    
    func saveSelectedCache() {
        listCartItemIdSelected.removeAll()
        for section in self.dataSource {
            for row in section.dataSource {
                if row.dynamicType == CartItem.self {
                    if (row as! CartItem).selected == true {
                        listCartItemIdSelected.append((row as! CartItem).cartItemId)
                    }
                }
            }
        }
        self.reloadSelectStatus()
    }
    
    func setupNavigationBar() {
        self.createBackButton()
    }
    
    func toggleSelectAllCartItems(button: UIButton) {
        
        if self.allCartItemSelected {
            self.allCartItemSelected = false
        } else {
            self.allCartItemSelected = true
        }
        
        for section in self.dataSource {
            section.sectionSelected = self.allCartItemSelected
        }
        
        self.cartItemSelectDidChanged()
    }
    
    
    func cartItemSelectDidChanged() {
        self.updateSummaryPrice()
        self.collectionView.reloadData()
    }
    
    func updateSummaryPrice() {
        
        var sum: Int = 0
        var numOfItems = 0
        
        for section in self.dataSource {
            for row in section.dataSource {
                if row.dynamicType == CartItem.self {
                    let item = row as! CartItem
                    if item.selected {
                        sum += item.price() * item.qty
                        numOfItems += item.qty
                    }
                }
            }
        }
        
        //FIXME: should change background color for different status ?
        if numOfItems > 0 {
            buttonConfirmPurchase.enabled = true
        }
        else {
            buttonConfirmPurchase.enabled = false
        }
        
        self.summaryLabel.text = String.localize("LB_SELECTED_PI_NO") + "(" + numOfItems.formatQuantity()! + ") " + sum.formatPrice()!
    }
    
    func reloadDataSource() {
        self.dataSource.removeAll()
        self.dataSource.append(ShoppingCartSectionData(sectionHeader: nil, reuseIdentifier: self.ShoppingCartPromotionCellID, dataSource: [self.promotionView]))
        if let merList = self.cart?.merchantList {
            for merchant in merList {
                
                if let itemList = merchant.itemList {
                    let data = ShoppingCartSectionData(sectionHeader: [], reuseIdentifier: self.ShoppingCartItemCellID, dataSource: itemList)
                    data.merchant = merchant
                    self.dataSource.append(data)
                }
                
            }
        }
        
        self.collectionView.reloadData()
    }

    //MARK: Action
    func viewDidTap() {
        self.view.endEditing(true)
    }
    
    func onBackButton() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func confirmPurchase() {
        Log.debug("confirmPurchase")
        
        self.saveSelectedCache()
        
        if (Context.getAuthenticatedUser()){
            let checkoutConfirmVC = CheckoutConfirmationViewController()
            checkoutConfirmVC.listData = listDataSelected()
            self.navigationController?.pushViewController(checkoutConfirmVC, animated: true)
        } else {
            promtLogin()
        }
    }

    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        switch collectionView {
        case self.collectionView!:
            return self.dataSource.count
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.collectionView!:
            return self.dataSource[section].dataSource.count
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
            
        case self.collectionView!:
            
            let sectionData = self.dataSource[indexPath.section]
            let data = sectionData.dataSource[indexPath.row]
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(sectionData.reuseIdentifier, forIndexPath: indexPath)
            
            if data.dynamicType == ShoppingCartPromotionView.self {
                cell.addSubview(self.promotionView)
                
            } else if data.dynamicType == CartItem.self {
                
                let edit = { [weak self] (data: CartItem) in
                    
                    if let strongSelf = self {
                        let checkOutViewController = CheckOutViewController(
                            style: nil,
                            selectedColorId: data.colorId,
                            selectedSizeId: data.sizeId,
                            redDotButton: strongSelf.buttonCart
                        )
                        checkOutViewController.cartItem = data
                        checkOutViewController.checkOutMode = CheckOutMode.SkuOnly
                        
                        checkOutViewController.didDismissHandler = { [weak self] in
                        
                            if let strongSelf = self {
                                
                                if Context.getAuthenticatedUser() || (Context.anonymousShoppingCartKey() != nil && Context.anonymousShoppingCartKey() != "0") {
                                    
                                    strongSelf.showLoading()
                                    firstly{
                                        return strongSelf.listCartItem()
                                        }.then { _ -> Void in
                                            strongSelf.cart = CacheManager.sharedManager.cart
                                            strongSelf.allCartItemSelected = false
                                            strongSelf.reloadDataSource()
                                            //strongSelf.updateSummaryPrice()
                                            strongSelf.loadSelectedCache()
                                        }.always {
                                            strongSelf.stopLoading()
                                        }.error { _ -> Void in
                                            Log.error("error")
                                    }
                                }
                                
                                strongSelf.reloadSelectStatus()
                            }
                        }
                        strongSelf.saveSelectedCache()
                        strongSelf.presentViewController(checkOutViewController, animated: false, completion: nil)
                    }
                }
                
                let  itemCell = cell as! ShoppingCartItemCell
                itemCell.leftMenuItems = [
                    SwipeActionMenuCellData(
                        text: String.localize("LB_CA_MOVE2WISHLIST"),
                        icon: UIImage(named: "icon_swipe_addToWishlist"),
                        backgroundColor: UIColor(hexString: "#c0c0c0"),
                        defaultAction: true,
                        action: { [weak self, data] () -> Void in
                            
                            Log.debug("Add To Wishlist")
                            if let strongSelf = self {
                                
                                strongSelf.indexPathForData(data, found: { (indexPath) in
                                    
                                    let sectionData = strongSelf.dataSource[indexPath.section]
                                    if let cartItem = sectionData.dataSource[indexPath.row] as? CartItem {
                                        
                                        strongSelf.saveSelectedCache()
                                        
                                        firstly{
                                            return strongSelf.moveItemToWishlist(cartItem.cartItemId)
                                            }.then { _ -> Void in
                                                
                                                strongSelf.showSuccessPopupWithText(String.localize("LB_CA_MOVE2WISHLIST_SUCCESS"))

                                                strongSelf.cart = CacheManager.sharedManager.cart
                                                strongSelf.reloadDataSource()
                                                //strongSelf.reloadSelectStatus()
                                                Log.debug("move to wishlist successfully")
                                                strongSelf.loadSelectedCache()
                                                
                                            }.error { _ -> Void in
                                                strongSelf.showFailPopupWithText(String.localize("LB_CA_MOVE2WISHLIST_FAILED"))
                                                Log.debug("move to wishlist fail")
                                        }
                                        
                                    }
                                })
                            }
                        }
                    ),
                ]
                
                itemCell.rightMenuItems = [
                    SwipeActionMenuCellData(
                        text: String.localize("LB_CA_EDIT"),
                        icon: UIImage(named: "icon_swipe_edit"),
                        backgroundColor: UIColor(hexString: "#E86763"),
                        action: { _ -> Void in
                            Log.debug("Edit style")
                            edit(data as! CartItem)
                        }
                    ),
                    SwipeActionMenuCellData(
                        text: String.localize("LB_CA_DELETE"),
                        icon: UIImage(named: "icon_swipe_delete"),
                        backgroundColor: UIColor(hexString: "#7A848C"),
                        defaultAction: true,
                        action: { [weak self, data] () -> Void in
                            
                            if let strongSelf = self {
                                Alert.alert(strongSelf, title: "", message: String.localize("MSG_CA_CONFIRM_REMOVE"), okActionComplete: { () -> Void in
                                    
                                    strongSelf.indexPathForData(data, found: { (indexPath) in
                                        
                                        let sectionData = strongSelf.dataSource[indexPath.section]
                                        if let cartItem = sectionData.dataSource[indexPath.row] as? CartItem {
                                            
                                            strongSelf.saveSelectedCache()
                                            
                                            firstly {
                                                return strongSelf.removeCartItem(cartItem.cartItemId)
                                                }.then { _ -> Void in
                                                    strongSelf.showSuccessPopupWithText(String.localize("LB_CA_DEL_CART_ITEM_SUCCESS"))
                                                    strongSelf.cart = CacheManager.sharedManager.cart
                                                    strongSelf.reloadDataSource()
                                                    //strongSelf.reloadSelectStatus()
                                                    strongSelf.loadSelectedCache()
                                                    
                                                }.error { _ -> Void in
                                                    strongSelf.showFailPopupWithText(String.localize("LB_CA_CART_ITEM_FAILED"))
                                            }
                                            
                                        }
                                    })

                                    }, cancelActionComplete:nil)
                            }
                        }
                    ),
                ]
                
                itemCell.editHandler = { (data) in
                    Log.debug("editHandler")
                    edit(data)
                }
                
                itemCell.cartItemSelectHandler = { [weak self] (data) in
                    if let strongSelf = self {
                        strongSelf.reloadSelectStatus()
                    }
                }
                
                itemCell.productImageHandler = { (data) in
                    Log.debug("productImageHandler")
                    
                    let detailVC = DetailViewController()
                    detailVC.styleCode = data.styleCode
                    self.navigationController?.pushViewController(detailVC, animated: true)
                    
                }

                itemCell.data = self.dataSource[indexPath.section].dataSource[indexPath.row] as? CartItem
                itemCell.accessibilityIdentifierIndex(indexPath.row)
                
            }
            
            return cell
            
        default:
            return self.defaultCell(collectionView, cellForItemAtIndexPath: indexPath)
        }
        
    }
    
    func reloadSelectStatus() {
        
        var allSelected = true
        var count = 0
        
        for section in self.dataSource {
            
            count += section.cartItemCount
            
            if section.sectionSelected == false {
                allSelected = false
                break
            }
        }
        
        self.allCartItemSelected = allSelected && count > 0
        self.cartItemSelectDidChanged()
    }
    
    func indexPathForData(data: AnyObject, found: ((indexPath: NSIndexPath) -> Void)) -> NSIndexPath? {
        
        var indexPath: NSIndexPath!
        section: for section in 0..<self.dataSource.count {
            for row in 0..<self.dataSource[section].dataSource.count {
                let item = self.dataSource[section].dataSource[row]
                if item === data {
                    
                    indexPath = NSIndexPath(forRow: row, inSection: section)
                    found(indexPath: indexPath)
                    
                    break section
                }
            }
        }
        
        return indexPath
    }
    
    func listDataSelected() -> [ShoppingCartSectionData] {
        var listData = [ShoppingCartSectionData]()
        
        if let merList = self.cart?.merchantList {
            for merchant in merList {
                
                if let itemList = merchant.itemList {
                    var cartItems = [CartItem]()
                    for cartItem in itemList {
                        if listCartItemIdSelected.contains(cartItem.cartItemId) {
                            cartItems.append(cartItem)
                        }
                    }
                    
                    if !cartItems.isEmpty {
                        let data = ShoppingCartSectionData(sectionHeader: [], sectionFooter: [], reuseIdentifier: ProductCellID, dataSource: cartItems)
                        data.merchant = merchant
                        data.commentText = CommentPlaceHolder
                        data.commentBoxColor = UIColor.secondary3()
                        data.fapiaoText = ""
                        listData.append(data)
                    }
                }
                
            }
        }

        return listData
    }
    
    //MARK: CollectionView
    func defaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCellWithReuseIdentifier(DefaultCellID, forIndexPath: indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let sectionData = self.dataSource[section]
        if sectionData.sectionHeader != nil && sectionData.dataSource.count > 0 {
            return CGSizeMake(self.view.bounds.width, ShoppingCartSectionViewHeight)
        }
        return CGSizeZero
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: ShoppingCartSectionViewID, forIndexPath: indexPath) as! ShoppingCartSectionHeaderView
        view.data = self.dataSource[indexPath.section]
        view.cartItemSelectHandler = { [weak self] (data) in
            if let strongSelf = self {
                strongSelf.reloadSelectStatus()
            }
        }
        
        view.headerTappedHandler = { (data) in
            Log.error("headerTappedHandler")
        }
        
        return view
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        switch collectionView {
        case self.collectionView!:
            
            let sectionData = self.dataSource[indexPath.section]
            let data = sectionData.dataSource[indexPath.row]
            var height = CGFloat(0)
            
            if data.dynamicType == ShoppingCartPromotionView.self {
                height =  (data as! UIView).bounds.size.height
            } else {
                height = ShoppingCartCellHeight
            }
            
            return CGSizeMake(self.view.frame.size.width, height)
        default:
            return CGSizeZero
        }
        
    }
    
    //MARK: Config tab bar
    override func shouldHideTabBar() -> Bool {
        return true
    }
    
    override func collectionViewBottomPadding() -> CGFloat {
        return SummaryViewHeight
    }
}

