//
//  CheckOutViewController.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 8/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

enum CheckOutMode: Int {
    case StyleOnly
    case SkuOnly
    case StyleAndSku
}


class CheckOutViewController: MmCartViewController, UIGestureRecognizerDelegate, UITextFieldDelegate {
    
    enum IndexPathRow: Int {
        case Color
        case Size
        case Quantity
        case Address
        case PaymentMethod
        case Fapiao
    }

    var checkOutMode = CheckOutMode.SkuOnly
    var isSwipeToPay = false
    
    private final let CellId : String = "Cell"
    private final let ColorCollectionCellId : String = "ColorCollectionCell"
    private final let QuantityCellId : String = "QuantityCell"
    private final let MenuCellId : String  = "MenuCell"
    private final let HeaderViewID = "HeaderView"
    
    private final let AddressCellId : String = "AddressCellId"
    private final let FapiaoCellId : String  = "FapiaoCellId"
    
    private let sectionInsets = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
    private final let DefaultUIEdgeInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    private final let HeaderHeight : CGFloat = 1
    private final let CheckOutInfoViewHeight : CGFloat = 98
    private final let SizeCellHeight : CGFloat = 61
    private final let ColorCellHeight : CGFloat = 63
    private final let CheckOutSummaryViewHeight : CGFloat = 65
    private final let QuantityCellHeight : CGFloat = 44
    private final let MaxOfQuantity : Int = 99
    
    private final var isKeyboardShowing = false
    
    var style : Style = Style()
    var styleCode = ""
    var qty : Int = 1
    var totalPrice : Int = 0
    var price : Int = 0
    var sizeIndexSelected : Int = -1
    var colorIndexSelected : Int  = -1
    var selectedSizeId : Int = -1
    var selectedColorId : Int = -1
    var colorKey = ""
    
    var redDotButton: ButtonRedDot?
    
    var quantityCell: QuantityCell?
    
    var filteredColorImageList : [Img] = []
    var featuredImageList : [Img] = []
    
    var contentView : UIView!
    
    var checkOutInfoCell :CheckOutInfoCell!
    var checkOutSummaryCell :CheckOutSummaryCell!
    
    var cartItem: CartItem? {
        didSet {
            
            if let cartItem = self.cartItem {
                if cartItem.qty > 0 {
                    self.qty = cartItem.qty
                }
                
                if cartItem.priceSale > 0 {
                    price = cartItem.priceSale
                } else {
                    price = cartItem.priceRetail
                }
                
                totalPrice = self.qty * price
                
                if cartItem.colorId > 0 {
                    self.selectedColorId = cartItem.colorId
                }
                
                if cartItem.sizeId > 0 {
                    self.selectedSizeId = cartItem.sizeId
                }
                
                self.colorKey = cartItem.colorKey
            }
        }
    }
    
    var tapGesture: UITapGestureRecognizer?
    
    var didDismissHandler: (() -> ())?
    
    private var transparentView = UIView()
    
    init(style : Style?, selectedColorId: Int = -1, selectedSizeId: Int = -1, redDotButton: ButtonRedDot?, colorKey: String = "") {
        
        super.init(nibName: nil, bundle: nil)
        
        if let myStyle = style {
            self.style = myStyle
            self.featuredImageList = myStyle.featuredImageList
        }
        
        self.selectedColorId = selectedColorId
        self.selectedSizeId = selectedSizeId
        self.redDotButton = redDotButton
        self.colorKey = colorKey

        self.modalPresentationStyle = .OverFullScreen
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isSwipeToPay {
            checkOutMode = .StyleOnly
        }
        
        setupLayout()
    }
    
    func setupLayout() {
        
        self.view.backgroundColor = UIColor.clearColor()
        
        self.title = String.localize("LB_DETAILS")
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo_demo_L"))
        
        let contentViewFrame =  CGRectMake(0, self.view.bounds.height, self.view.bounds.width, self.view.bounds.height * 2 / 3)
        contentView =  UIView(frame:contentViewFrame)
        view.addSubview(contentView)
        
        // checkout info cell
        let checkOutInfoCellFrame =  CGRectMake(0, 0, contentView.bounds.width, CheckOutInfoViewHeight)
        checkOutInfoCell = CheckOutInfoCell(frame: checkOutInfoCellFrame)
        checkOutInfoCell.sizeGridTappedHandler = {
            Log.error("sizeGridTappedHandler")
        }
        
        var priceString = ""
        
        switch checkOutMode {
    
        case CheckOutMode.SkuOnly:
            if let cartItem = self.cartItem  {
                
                if cartItem.priceSale > 0 {
                    priceString = cartItem.priceSale.formatPrice()!
                } else {
                    if let priceRetail = cartItem.priceRetail.formatPrice() where cartItem.priceRetail != cartItem.priceSale {
                        priceString = priceRetail
                    }
                }
                
                checkOutInfoCell.data = (cartItem.productImage, cartItem.brandImage, cartItem.skuName, priceString)
                
                self.showLoading()
                firstly{
                    return self.searchStyle()
                    }.then {_ -> Void in
                        self.reloadAllData()
                    }.always {
                        self.stopLoading()
                    }.error { _ -> Void in
                        Log.error("error")
                }
            }

            break
        case CheckOutMode.StyleOnly:
            if style.priceSale > 0 {
                price = style.priceSale
            } else {
                price = style.priceRetail
            }
            totalPrice = self.qty * price
            
            if style.priceSale > 0 {
                priceString = style.priceSale.formatPrice()!
            } else {
                if let priceRetail = style.priceRetail.formatPrice() where style.priceRetail != style.priceSale {
                    priceString = priceRetail
                }
            }
            
            let productImageName = style.findImageKeyByColorKey(colorKey)
            
            checkOutInfoCell.data = (productImageName, style.brandHeaderLogoImage, style.skuName, priceString)
            break
            
            
         case CheckOutMode.StyleAndSku:
            if style.priceSale > 0 {
                price = style.priceSale
            } else {
                price = style.priceRetail
            }
            totalPrice = self.qty * price
            
            if style.priceSale > 0 {
                priceString = style.priceSale.formatPrice()!
            } else {
                if let priceRetail = style.priceRetail.formatPrice() where style.priceRetail != style.priceSale {
                    priceString = priceRetail
                }
            }
            
            let productImageName = style.findImageKeyByColorKey(colorKey)
            
            checkOutInfoCell.data = (productImageName, style.brandHeaderLogoImage, style.skuName, priceString)
            break
        }

        
        contentView.addSubview(checkOutInfoCell)
        
        self.collectionView.frame = CGRectMake(0, checkOutInfoCell.frame.maxY, contentView.bounds.width, contentView.bounds.height - CheckOutInfoViewHeight - CheckOutSummaryViewHeight)
        
        self.collectionView!.backgroundColor = UIColor.whiteColor()
        self.collectionView!.registerClass(ColorCollectionCell.self, forCellWithReuseIdentifier: ColorCollectionCellId)
        self.collectionView!.registerClass(QuantityCell.self, forCellWithReuseIdentifier: QuantityCellId)
        self.collectionView!.registerClass(MenuCell.self, forCellWithReuseIdentifier: MenuCellId)
        self.collectionView!.registerClass(SeparatorHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: HeaderViewID)
        
        if isSwipeToPay {
            self.collectionView!.registerClass(CheckoutAddressCell.self, forCellWithReuseIdentifier: AddressCellId)
            self.collectionView!.registerClass(CheckoutFapiaoCell.self, forCellWithReuseIdentifier: FapiaoCellId)
        }
        
        contentView.addSubview(collectionView)
        
        self.transparentView.backgroundColor = UIColor.init(white: 0.0, alpha: 0.6)
        self.transparentView.frame = self.view.bounds
        self.transparentView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.transparentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "dismiss"))
        
        self.view.insertSubview(self.transparentView, belowSubview: contentView)
        
        // checkout summary cell
        let checkOutSummaryCellFrame =  CGRectMake(0, contentView.bounds.height - CheckOutSummaryViewHeight, contentView.bounds.width, CheckOutSummaryViewHeight)
        
        checkOutSummaryCell = CheckOutSummaryCell(frame: checkOutSummaryCellFrame)
        checkOutSummaryCell.checkOutButton.addTarget(self, action: "buy:", forControlEvents: UIControlEvents.TouchUpInside)
        
        checkOutSummaryCell.checkOutButton.accessibilityIdentifier = "checkout_confirm_button"
        
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = NSLocale(localeIdentifier: "zh_Hans_CN")
        checkOutSummaryCell.priceLabel.text = totalPrice.formatPrice()
        if isSwipeToPay {
            checkOutSummaryCell.checkOutButton.setTitle(String.localize("LB_CA_PDP_SWIPE2PAY_PURCHASE"), forState: .Normal)
        } else if checkOutMode == CheckOutMode.SkuOnly {
            checkOutSummaryCell.checkOutButton.setTitle(String.localize("LB_CA_CONFIRM"), forState: .Normal)
        } else {
            checkOutSummaryCell.checkOutButton.setTitle(String.localize("LB_CA_ADD2CART"), forState: .Normal)
        }
        
        contentView.addSubview(checkOutSummaryCell)
        
        tapGesture = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        reloadAllData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animateWithDuration(0.3) { () -> Void in
            self.contentView.transform = CGAffineTransformMakeTranslation(0, -self.contentView.bounds.height)
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
        
        if isSwipeToPay {
            self.navigationController?.navigationBarHidden = true
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    //MARK: Search Style
    func searchStyle() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            SearchService.searchStyleByStyleCode(self.cartItem!.styleCode){[weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        
                        if let styleResponse = Mapper<SearchResponse>().map(response.result.value){
                            
                            if let styles = styleResponse.pageData {
                                if styles.count > 0 {
                                    strongSelf.style = styles[0]
                                    strongSelf.featuredImageList = styles[0].featuredImageList
                                }
                            }
                        }
                        fulfill("OK")
                    }
                    else{
                        reject(response.result.error!)
                    }
                    
                }
            }
        }
    }
    
    
    //MARK: CollectionView Data Source, Delegate Method
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if isSwipeToPay && Context.getAuthenticatedUser() {
            return 7
        }
        return 4
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case IndexPathRow.Color.rawValue:
            return self.style.colorList.count
        case IndexPathRow.Size.rawValue:
            return self.style.sizeList.count
        default:
            return 1
            
        }
    }
    
    //MARK: Draw Cell
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        switch (indexPath.section){
            
        case IndexPathRow.Color.rawValue: // Select Color
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ColorCollectionCellId, forIndexPath: indexPath) as! ColorCollectionCell
            let filteredColorImageList = self.style.colorImageList.filter({$0.colorKey.lowercaseString == self.style.colorList[indexPath.row].colorKey.lowercaseString})
            if filteredColorImageList.isEmpty{
                cell.imageView.af_setImageWithURL(ImageURLFactory.get(self.style.colorList[indexPath.row].colorImage,category: .Color),placeholderImage : UIImage(named: "holder"))
            } else {
                cell.imageView.af_setImageWithURL(ImageURLFactory.get((filteredColorImageList[0].imageKey)), placeholderImage : UIImage(named: "holder"))
            }
            
            cell.label.text = ""
            
            if indexPath.row == colorIndexSelected {
                cell.border()
            } else {
                cell.unBorder()
            }
            
            var filteredSkuList = self.style.skuList.filter({$0.sizeId == self.selectedSizeId})
            filteredSkuList = filteredSkuList.filter({$0.colorId == self.style.colorList[indexPath.row].colorId})
            if filteredSkuList.isEmpty && self.sizeIndexSelected != -1 {
                cell.cross()
                self.style.colorList[indexPath.row].isValid = false
            } else {
                cell.unCross()
                self.style.colorList[indexPath.row].isValid = true
                
            }
            
            cell.accessibilityIdentifier = "checkout_color_cell"
            
            return cell
            
            
        case IndexPathRow.Size.rawValue: // Select Size
            
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ColorCollectionCellId, forIndexPath: indexPath) as! ColorCollectionCell
            cell.imageView.image = nil
            cell.label.text = self.style.sizeList[indexPath.row].sizeName
            
            if indexPath.row == sizeIndexSelected{
                self.selectedSizeId = self.style.sizeList[indexPath.row].sizeId
                cell.border()
            } else {
                cell.unBorder()
            }
            
            var filteredSkuList = self.style.skuList.filter({$0.colorId == self.selectedColorId})
            filteredSkuList = filteredSkuList.filter({$0.sizeId == self.style.sizeList[indexPath.row].sizeId})
            if filteredSkuList.isEmpty && self.colorIndexSelected != -1 {
                cell.cross()
                self.style.sizeList[indexPath.row].isValid = false
                
            } else {
                cell.unCross()
                self.style.sizeList[indexPath.row].isValid = true
            }
            
            cell.accessibilityIdentifier = "checkout_size_cell"
            
            return cell
            
        case IndexPathRow.Quantity.rawValue: //select quantity
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(QuantityCellId, forIndexPath: indexPath) as! QuantityCell
            
            self.quantityCell = cell
            
            cell.minusStepButton.addTarget(self, action: "stepperValueChanged:", forControlEvents: .TouchUpInside)
            
            cell.minusStepButton.accessibilityIdentifier = "checkout_quantity_minus_button"
            
            cell.addStepButton.addTarget(self, action: "stepperValueChanged:", forControlEvents: .TouchUpInside)
            
            cell.addStepButton.accessibilityIdentifier = "checkout_quantity_add_button"
            
            cell.qtyTextField.text = String(self.qty)
            
            if cell.qtyTextField.delegate == nil {
                cell.qtyTextField.delegate = self
            }
            
            cell.qtyTextField.keyboardType = .DecimalPad
            
            cell.qtyTextField.accessibilityIdentifier = "checkout_quantity_textfield"
            
            return cell
            
        case IndexPathRow.Address.rawValue:
            if isSwipeToPay && Context.getAuthenticatedUser() {
                //Address
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier(AddressCellId, forIndexPath: indexPath) as! CheckoutAddressCell
                
                cell.leftLabel.text = String.localize("LB_CA_EDITITEM_SHIPADDR")
                //cell.rightLabel.text = String.localize("LB_CA_EDITITEM_SHIPADD_NULLVAL")

                cell.rightViewTapHandler = {
                    Log.debug("address tapped")
                    
                    let viewController : MmViewController
                    if true {
                        viewController = AddressSelectionViewController()
                    }
                    else {
                        viewController = AddressAdditionViewController()
                    }
                    
                    self.navigationController?.navigationBarHidden = false
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                
                return cell
            }
            //Gap
            return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
            
        case IndexPathRow.PaymentMethod.rawValue:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(AddressCellId, forIndexPath: indexPath) as! CheckoutAddressCell
            
            cell.leftLabel.text = String.localize("LB_CA_EDITITEM_PAYMENT_METHOD")
            //cell.rightLabel.text = String.localize("LB_CA_PAYMENT_METHOD_NULLVAL")

            cell.rightViewTapHandler = {
                let viewController : MmViewController
                
                viewController = PaymentMethodSelectionViewController()
                
                self.navigationController?.navigationBarHidden = false
                self.navigationController?.pushViewController(viewController, animated: true)
            }

            return cell

        case IndexPathRow.Fapiao.rawValue:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FapiaoCellId, forIndexPath: indexPath) as! CheckoutFapiaoCell
            
            //cell.textField.text = "Last Name + First Name"
            
            if cell.textField.delegate == nil {
                cell.textField.delegate = self
            }
            
            return cell

        default:
            return getDefaultCell(collectionView, cellForItemAtIndexPath: indexPath)
            
        }
    }
    
    func getDefaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellId, forIndexPath: indexPath)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: HeaderViewID, forIndexPath: indexPath) as! SeparatorHeaderView
        if indexPath.section == IndexPathRow.Color.rawValue {
            headerView.separatorView.hidden = true
        } else if indexPath.section == IndexPathRow.Size.rawValue {
            if self.style.colorList.count > 0 && self.style.sizeList.count > 0 {
                headerView.separatorView.hidden = false
            } else {
                headerView.separatorView.hidden = true
            }
        }
        else {
            headerView.separatorView.hidden = false
        }
        
        return headerView
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSizeMake(self.view.bounds.width, HeaderHeight)
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            
            switch section {
                
            case IndexPathRow.Color.rawValue:
                if self.style.colorList.count > 0 {
                    return sectionInsets
                } else {
                    return DefaultUIEdgeInsets
                }
            case IndexPathRow.Size.rawValue:
                if self.style.sizeList.count > 0 {
                    return sectionInsets
                } else {
                    return DefaultUIEdgeInsets
                }
            default:
                return DefaultUIEdgeInsets
                
            }
    }
    
    //MARK: Item Size Delegate for Collection View
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            switch indexPath.section {
            case IndexPathRow.Color.rawValue:
                return CGSizeMake(ColorCellHeight, ColorCellHeight)
            case IndexPathRow.Size.rawValue:
                return CGSizeMake(SizeCellHeight, SizeCellHeight)
            case IndexPathRow.Quantity.rawValue:
                return CGSizeMake(self.view.frame.size.width, QuantityCellHeight)
            case self.collectionView.numberOfSections() - 1:
                return CGSizeMake(self.view.frame.size.width, 21)
            default:
                return CGSizeMake(self.view.frame.size.width, 44)
                
            }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    //MARK: Collection View Delegate methods
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            
            switch indexPath.section {
            case IndexPathRow.Color.rawValue:
                if self.style.colorList[indexPath.row].isValid {
                    self.colorIndexSelected = indexPath.row
                    self.selectedColorId = self.style.colorList[indexPath.row].colorId
                    self.filteredColorImageList = self.style.colorImageList.filter({$0.colorKey.lowercaseString == self.style.colorList[indexPath.row].colorKey.lowercaseString })
                    
                    if filteredColorImageList.isEmpty {
                        if !featuredImageList.isEmpty {
                            checkOutInfoCell.productImageView.af_setImageWithURL(ImageURLFactory.get((self.featuredImageList[0].imageKey)), placeholderImage : UIImage(named: "holder"))
                        }
                    } else {
                        checkOutInfoCell.productImageView.af_setImageWithURL(ImageURLFactory.get((filteredColorImageList[0].imageKey)), placeholderImage : UIImage(named: "holder"))
                    }
                    
                    reloadAllData()
                }
                break
            case IndexPathRow.Size.rawValue:
                if self.style.sizeList[indexPath.row].isValid {
                    
                    self.sizeIndexSelected = indexPath.row
                    self.selectedSizeId = self.style.sizeList[indexPath.row].sizeId
                    reloadAllData()
                    
                    break
                }
                break
            default:
                break
            }
            
    }
    
    func reloadAllData(){
        if self.selectedColorId != -1 && self.style.colorList.count > 0 {
            self.colorIndexSelected = self.style.colorList.indexOf({$0.colorId == selectedColorId})!
        } else if self.style.colorList.count == 1 { //auto select if 1 option only
            if self.style.colorList[0].isValid {
                self.colorIndexSelected = 0 //directly pick the first color
                self.selectedColorId = self.style.colorList[colorIndexSelected].colorId
                self.filteredColorImageList = self.style.colorImageList.filter({$0.colorKey.lowercaseString == self.style.colorList[0].colorKey.lowercaseString })
                
                if !self.filteredColorImageList.isEmpty {
                    checkOutInfoCell.productImageView.af_setImageWithURL(ImageURLFactory.get((self.filteredColorImageList[0].imageKey)), placeholderImage : UIImage(named: "holder"))
                }
            }
        }

        if self.selectedSizeId != -1 && self.style.sizeList.count > 0 {
            self.sizeIndexSelected = self.style.sizeList.indexOf({$0.sizeId == selectedSizeId})!
        } else if self.style.sizeList.count == 1 { //auto select if 1 option only
            if self.style.sizeList[0].isValid {
                self.sizeIndexSelected = 0
                self.selectedSizeId = self.style.sizeList[sizeIndexSelected].sizeId
            }
        }
        
        self.collectionView?.reloadData()
    }
    
    func buy(sender:UIButton!){
        if isSwipeToPay {
            Log.debug("buy tapped")
            
            if (self.style.colorList.count == 0 || self.selectedColorId != -1) && (self.style.sizeList.count == 0 || self.selectedSizeId != -1) {
                if Context.getAuthenticatedUser() {
                    
                }
                else {
                    promtLogin(.CheckoutSwipeToPay)
                }

            } else {
                guard (self.sizeIndexSelected != -1 && self.sizeIndexSelected < self.style.sizeList.count) && (self.colorIndexSelected != -1 && self.colorIndexSelected < self.style.colorList.count) else {
                    Alert.alertWithSingleButton(self, title: String.localize("LB_MC_COLORS_SIZE_TITLE"), message: "", buttonString:String.localize("LB_OK"))
                    return
                }
            }
            
        }
        else {
            if let quantityCell = self.quantityCell {
                if let textInput = quantityCell.qtyTextField.text {
                    if textInput.isEmpty == false {
                        self.qty = Int(textInput)!
                    }
                }
                quantityCell.qtyTextField.text = String(self.qty)
                updateTotalPrice()
            }
            
            
            switch checkOutMode {
            case CheckOutMode.SkuOnly:
                if let cartItem = self.cartItem {
                    
                    let sku = self.style.searchSku(self.selectedSizeId, colorId: self.selectedColorId)
                    
                    guard (sku != nil) else {
                        // missing handling
                        self.showFailPopupWithText(String.localize("Fail to add shopping cart: Missing Sku"))
                        return
                    }
                    
                    firstly{
                        self.updateCartItem(cartItem.cartItemId, skuId: sku!.skuId, qty: self.qty)
                        }.then { _ -> Void in
                            Log.debug("Update cart item successfully")
                        }.always{
                            self.dismiss()
                        }.error { _ -> Void in
                            Log.debug("error")
                    }
                    
                } else {
                    self.dismiss()
                }
                
                break
            case CheckOutMode.StyleOnly, CheckOutMode.StyleAndSku:
                if (self.style.colorList.count == 0 || self.selectedColorId != -1) && (self.style.sizeList.count == 0 || self.selectedSizeId != -1) {
                    
                    let sku = self.style.searchSku(self.selectedSizeId, colorId: self.selectedColorId)
                    
                    guard (sku != nil) else {
                        // missing handling
                        self.showFailPopupWithText(String.localize("Fail to add shopping cart: Missing Sku"))
                        return
                    }
                    
                    firstly{
                        self.addCartItem(sku!.skuId, qty: self.qty)
                        }.then { _ -> Void in
                            
                            if let colorImage = self.checkOutInfoCell.productImageView.image {
                                
                                if let view = UIApplication.sharedApplication().windows.first {
                                    
                                    let statPoint = self.checkOutSummaryCell.checkOutButton.center
                                    
                                    let animation = CheckOutAnimation(
                                        itemImage: colorImage,
                                        itemSize: CGSizeMake(self.ColorCellHeight, self.ColorCellHeight),
                                        itemStartPos: self.checkOutSummaryCell.convertPoint(statPoint, toView: view),
                                        redDotButton: self.redDotButton
                                    )
                                    
                                    view.addSubview(animation)
                                    animation.showAnimation()
                                }
                            }
                            
                        }.always{
                            self.dismiss()
                        }.error { _ -> Void in
                            self.showFailPopupWithText(String.localize("LB_CA_ADD2CART_FAIL"))
                            Log.debug("error")
                    }
                } else {
                    guard (self.sizeIndexSelected != -1 && self.sizeIndexSelected < self.style.sizeList.count) && (self.colorIndexSelected != -1 && self.colorIndexSelected < self.style.colorList.count) else {
                        Alert.alertWithSingleButton(self, title: String.localize("LB_MC_COLORS_SIZE_TITLE"), message: "", buttonString:String.localize("LB_OK"))
                        return
                    }
                }
                break
            }
        }
    }
    
    func dismiss(){
        
        UIView.animateWithDuration(
            0.3,
            animations: { () -> Void in
                self.contentView!.transform = CGAffineTransformIdentity
            },
            completion: { (success) -> Void in
                self.dismissViewControllerAnimated(false, completion: nil)
            }
        )
        
        if let callback = self.didDismissHandler {
            callback()
        }
    }
    
    //MARK: Stepper delegate
    func stepperValueChanged(sender:UIButton!)
    {
        var qtyValue = self.qty
        if sender.tag == TagOfMinusStepButton {
            qtyValue--
        } else if sender.tag == TagOfAddStepButton {
            qtyValue++
        }
        
        if qtyValue < 1 {
            qtyValue = 1
        }
        
        if qtyValue > MaxOfQuantity {
            qtyValue = MaxOfQuantity
        }
        
        self.qty = qtyValue
        
        
        if let quantityCell = self.quantityCell {
            quantityCell.qtyTextField.text = String(self.qty)
            updateTotalPrice ()
        }
    }
    
    
    func updateTotalPrice () {
        totalPrice = self.qty * price
        checkOutSummaryCell.priceLabel.text = totalPrice.formatPrice()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK: KeyboardWilShow/Hide callback
    func keyboardWillShow(notification: NSNotification) {
        if !isKeyboardShowing {
            isKeyboardShowing = true
            self.collectionView.addGestureRecognizer(tapGesture!)
            
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                self.contentView.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        isKeyboardShowing = false
        
        self.collectionView.removeGestureRecognizer(tapGesture!)
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.contentView.frame.origin.y += keyboardSize.height
        }
    }
    
    //MARK: Text Field delegate methods
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == self.quantityCell!.qtyTextField {
            if let textInput = textField.text {
                if textInput.isEmpty == false {
                    self.qty = Int(textInput)!
                }
            }
            textField.text = String(self.qty)
            updateTotalPrice()
        }
        else {
            
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == self.quantityCell!.qtyTextField {

            let currentText = textField.text ?? ""
            let prospectiveText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            if prospectiveText.characters.count == 1 {
                let zeroString = "0"
                let isEqualToZeroString = (string == zeroString)
                if isEqualToZeroString {
                    return false
                }
            }
            
            if Int(prospectiveText) > MaxOfQuantity {
                return false
            }
            
            if prospectiveText.isNumberic() && prospectiveText != "" {
                self.qty = Int(prospectiveText)!
                updateTotalPrice()
            }
            
            return prospectiveText.isNumberic()
        }
        else {
            return true
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}