//
//  CheckoutConfirmationViewController.swift
//  merchant-ios
//
//  Created by Phan Manh Hung on 2/19/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

let ProductCellID = "ProductCellID"

class CheckoutConfirmationViewController: MmCartViewController {
    
    private final let AddressCellID = "AddressCellID"
    private final let BrandCellID = "BrandCellID"
    private final let SummaryBrandCelllID = "SummaryBrandCelllID"
    private final let DefaultCellID = "DefaultCellID"

    private final let SummaryViewHeight = CGFloat(60)

    private var addressView: CheckoutConfirmationAddressView!
    
    private var dataSource: [ShoppingCartSectionData]!

    private var summaryLabel: UILabel!
    private var buttonSubmitOrder : UIButton!
    private var activeTextField : UITextField!
    private var activeTextView : UITextView!

    private var isTextViewEditting = false
    
    private var order : Order?

    var listData : [ShoppingCartSectionData]!
    var selectedAddress =  Address ()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = String.localize("LB_CA_ORDER_CONFIRMATION")

        setupNavigationBar()

        self.addressView = CheckoutConfirmationAddressView.viewWithScreenSize()
        self.addressView.addressHandler = {
            
            if self.selectedAddress.userAddressKey == "" {
                
                let addressAdditionViewController = AddressAdditionViewController()
                self.navigationController?.pushViewController(addressAdditionViewController, animated: true)
                self.addressView.lblAddress.text = String.localize("LB_CA_NEW_SHIPPING_ADDR")
            
            } else {
                let addressSelectionViewController = AddressSelectionViewController()
                
                addressSelectionViewController.selectedAddress = self.selectedAddress
                
                addressSelectionViewController.didAddressSelectedHandler = {[weak self] (data) in
                    Log.debug(data)
                    if let strongSelf = self {
                        
                        strongSelf.selectedAddress = data
                        
                        strongSelf.addressView.lblName.text = data.recipientName
                        strongSelf.addressView.lblAddress.text = data.address + ", \(data.city), \(data.province), \(data.country)"
                        strongSelf.addressView.lblPhoneNumber.text = data.phoneCode + " " + data.phoneNumber
                    }
                }
                self.navigationController?.pushViewController(addressSelectionViewController, animated: true)
            }
        }

        self.collectionView.alwaysBounceVertical = true

        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: self.AddressCellID)
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: self.DefaultCellID)
        
        self.collectionView!.registerClass(CheckoutConfirmationProductCell.self, forCellWithReuseIdentifier: ProductCellID)
        
        self.collectionView!.registerClass(CheckoutConfirmationBrandView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: self.BrandCellID)
        
        self.collectionView!.registerClass(CheckoutConfirmationSummaryBrandView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: self.SummaryBrandCelllID)

        self.dataSource = [ShoppingCartSectionData]()
        
        self.dataSource.append(ShoppingCartSectionData(sectionHeader: nil, reuseIdentifier: self.AddressCellID, dataSource: [self.addressView]))
        
        for data in listData {
            self.dataSource.append(data)
        }
        
        self.collectionView.reloadData()

        let summaryView = { () -> UIView in
            
            let frame = CGRectMake(0, self.collectionView.frame.maxY, self.collectionView.frame.width, SummaryViewHeight)
            
            let view = UIView(frame: frame)
            
            let separatorView = { () -> UIView in
                
                let view = UIView(frame: CGRectMake(0, 0, frame.width, 1))
                view.backgroundColor = UIColor.backgroundGray()
                
                return view
            } ()
            view.addSubview(separatorView)

            //
            let submitButton = { () -> UIButton in
                
                let rightPadding = CGFloat(18)
                let buttonSize = CGSizeMake(106, 40)
                let xPos = frame.width - buttonSize.width - rightPadding
                let yPos = (frame.height - buttonSize.height) / 2
                
                let button = UIButton(type: .Custom)
                button.frame = CGRectMake(xPos, yPos, buttonSize.width, buttonSize.height)
                button.setTitle(String.localize("LB_CA_SUBMIT_ORDER"), forState: .Normal)
                button.addTarget(self, action: "submitOrder", forControlEvents: .TouchUpInside)
                button.layer.cornerRadius = 3.0
                button.formatPrimary()
                return button
                
            } ()
            view.addSubview(submitButton)
            self.buttonSubmitOrder = submitButton
            
            //
            let summaryLabel = { () -> UILabel in
                
                let leftPadding = CGFloat(18)
                let label = UILabel(frame: CGRectMake(leftPadding, 0, submitButton.frame.minX - leftPadding, frame.height))
                label.formatSingleLine(16)
                return label
                
            } ()
            view.addSubview(summaryLabel)
            self.summaryLabel = summaryLabel
            
            return view
        } ()
        
        self.view.addSubview(summaryView)
        self.updateSummaryPrice()

        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "viewDidTap"))
        
        self.showLoading()
        firstly{
            return self.viewDefaultAddress()
            }.then { _ -> Void in
                
                self.updateAddressView(self.selectedAddress)

            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    
    func updateAddressView(address : Address) {
        if (address.userAddressKey != "") {
            self.addressView.lblName.text = address.recipientName
            self.addressView.lblAddress.text = address.address + ", \(address.city), \(address.province), \(address.country)"
            self.addressView.lblPhoneNumber.text = address.phoneCode + " " + address.phoneNumber
        } else {
            self.addressView.lblAddress.text = String.localize("LB_CA_NEW_SHIPPING_ADDR")
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        updateAddressView(self.selectedAddress)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    //MARK: Service
    
    func viewDefaultAddress(completion complete:(() -> Void)? = nil) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            AddressService.viewDefault({
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            if let address = Mapper<Address>().map(response.result.value) {
                                strongSelf.selectedAddress = address
                            }

                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
                })
        }
    }

    
    func setupNavigationBar() {
        self.createBackButton()
    }
    
    func viewDidTap() {
        self.view.endEditing(true)
    }

    func updateSummaryPrice() {
        let priceText = NSMutableAttributedString()
        
        let textFont = UIFont.systemFontOfSize(14)
        let valueFont = UIFont.systemFontOfSize(15)
        
        let text = NSAttributedString(
            string: String.localize("LB_CA_TOTAL") + "：",
            attributes: [
                NSForegroundColorAttributeName: UIColor(hexString: "#8e8e8e"),
                NSFontAttributeName: textFont
            ]
        )
        priceText.appendAttributedString(text)
        
        var sumPrice = 0
        for sectionData in self.dataSource {
            let firstItem = sectionData.dataSource[0]
            if firstItem.dynamicType == CartItem.self {
                sumPrice += priceWithData(sectionData.dataSource as! [CartItem])
            }
        }
        
        let value = NSAttributedString(
            string: sumPrice.formatPrice()!,
            attributes: [
                NSForegroundColorAttributeName: UIColor(hexString: "#4a4a4a"),
                NSFontAttributeName: valueFont,
                NSBaselineOffsetAttributeName: (textFont.capHeight - valueFont.capHeight) / 2
            ]
        )
        priceText.appendAttributedString(value)
        
        self.summaryLabel.attributedText = priceText
    }
    
    func submitOrder() {
        Log.debug("submit button Tapped")
        
        guard selectedAddress.userAddressKey != "" else {
            self.showError(String.localize("Please select address first"), animated: true) //TODO: need to change correct string
            return
        }
        
        var skus = [Dictionary<String,AnyObject>]()
        
        for sectionData in self.dataSource {
            let firstItem = sectionData.dataSource[0]
            if firstItem.dynamicType == CartItem.self {
                
                for cartItem in sectionData.dataSource {
                    let item = cartItem as! CartItem
                    let sku : Dictionary<String,AnyObject> = ["SkuId": item.skuId, "Qty": item.qty]
                    skus.append(sku)
                }

            }
        }

        var viewController : UIViewController?
        self.showLoading()
        firstly {
            return createOrder(selectedAddress.userAddressKey, skus: skus)
            }.then { _ -> Void in
                
                if let order = self.order {
                    
                    if order.isCrossBorder && !order.isUserIdentificationExists {
                        viewController = IDCardCollectionPageViewController()
                    }
                    else {
                        viewController = PaymentMethodSelectionViewController()
                    }
                    
                    if viewController != nil {
                        self.navigationController?.pushViewController(viewController!, animated: true)
                    }

                }
                
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
        

    }
    
    func createOrder(userAddressKey : String, skus : [Dictionary<String,AnyObject>]) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            OrderService.createOrder(userAddressKey, skus: skus, completion: {
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            if let order = Mapper<Order>().map(response.result.value) {
                                strongSelf.order = order
                            }
                            
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            })
        }
    }

    
    func priceWithData(data : [CartItem]) -> Int {
        var sumPrice = 0
        for cartItem in data {
            sumPrice += cartItem.price() * cartItem.qty
        }
        return sumPrice
    }
    
    //MARK: CollectionView
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        switch collectionView {
        case self.collectionView!:
            return self.dataSource.count
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.collectionView!:
            return self.dataSource[section].dataSource.count
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
            
        case self.collectionView!:
            
            let sectionData = self.dataSource[indexPath.section]
            let data = sectionData.dataSource[indexPath.row]
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(sectionData.reuseIdentifier, forIndexPath: indexPath)
            
            if data.dynamicType == CheckoutConfirmationAddressView.self {
                cell.addSubview(self.addressView)
                
            } else {
                let itemCell = cell as! CheckoutConfirmationProductCell
                itemCell.data = self.dataSource[indexPath.section].dataSource[indexPath.row] as? CartItem
            }
            
            return cell
            
        default:
            return self.defaultCell(collectionView, cellForItemAtIndexPath: indexPath)
        }
        
    }
    
    func defaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCellWithReuseIdentifier(DefaultCellID, forIndexPath: indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let sectionData = self.dataSource[section]
        if sectionData.sectionHeader != nil && sectionData.dataSource.count > 0 {
            return CGSizeMake(self.view.bounds.width, CheckoutConfirmationBrandSectionViewHeight)
        }
        return CGSizeZero
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let sectionData = self.dataSource[section]
        if sectionData.sectionFooter != nil && sectionData.dataSource.count > 0 {
            return CGSizeMake(self.view.bounds.width, CheckoutConfirmationBrandFapioViewHeight + CheckoutConfirmationBrandTotalPriceViewHeight + CheckoutConfirmationCommentBoxViewHeight)
        }
        return CGSizeZero
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader {
            let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: BrandCellID, forIndexPath: indexPath) as! CheckoutConfirmationBrandView
            view.data = self.dataSource[indexPath.section]

            return view
        }
        else /*if kind == UICollectionElementKindSectionFooter*/ {
            let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: SummaryBrandCelllID, forIndexPath: indexPath) as! CheckoutConfirmationSummaryBrandView
            
            let data = self.dataSource[indexPath.section]
            if data.sectionFooter != nil {
                view.tfFapiao.text = data.fapiaoText
                view.tvComment.text = data.commentText
                view.tvComment.textColor = data.commentBoxColor
                view.data = data
                
                view.textViewBeginEditting = {
                    self.activeTextView = view.tvComment
                    self.isTextViewEditting = true
                    
                    if view.tvComment.textColor == UIColor.secondary3() {
                        view.tvComment.text = nil
                        view.tvComment.textColor = UIColor.blackColor()
                    }
                }
                
                view.textViewEndEditting = {
                    if view.tvComment.text.isEmpty {
                        view.tvComment.text = CommentPlaceHolder
                        view.tvComment.textColor = UIColor.secondary3()
                    }
                    
                    view.data.commentText = view.tvComment.text
                    view.data.commentBoxColor = view.tvComment.textColor
                }
                
                view.textFieldBeginEditting = {
                    self.activeTextField = view.tfFapiao
                    self.isTextViewEditting = false
                }
                
                view.textFieldEndEditting = {
                    view.data.fapiaoText = view.tfFapiao.text
                }
            }

            if let cartItem = data.dataSource as? [CartItem] {
                view.lblPrice.text = priceWithData(cartItem).formatPrice()
            }
            
            return view
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        switch collectionView {
        case self.collectionView!:
            
            let sectionData = self.dataSource[indexPath.section]
            let data = sectionData.dataSource[indexPath.row]
            var height = CGFloat(0)
            
            if data.dynamicType == CheckoutConfirmationAddressView.self || data.dynamicType == CheckoutConfirmationSummaryBrandView.self {
                height =  (data as! UIView).bounds.size.height
                
            } else {
                height = CheckoutConfirmationProductCellHeight
            }
            
            return CGSizeMake(self.view.frame.size.width, height)
        default:
            return CGSizeZero
        }
        
    }

    //MARK: View Config
    // config tab bar
    override func shouldHideTabBar() -> Bool {
        return true
    }
    
    override func collectionViewBottomPadding() -> CGFloat {
        return SummaryViewHeight
    }
    
    //MARK: Keyboard
    func keyboardWillShow(notification: NSNotification) {
        if let info = notification.userInfo, let kbObj = info[UIKeyboardFrameEndUserInfoKey] {
            
            var kbRect = kbObj.CGRectValue
            kbRect = self.view.convertRect(kbRect, fromView: nil)

            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height - SummaryViewHeight, 0.0)
            
            self.collectionView.contentInset = contentInsets
            self.collectionView.scrollIndicatorInsets = contentInsets

            if isTextViewEditting {
                var rect = activeTextView.frame
                rect = self.collectionView.convertRect(rect, fromView: activeTextView)
                
                let naviAndStatusBarHeight = CGFloat(64)
                let remainingViewHeight = kbRect.minY - naviAndStatusBarHeight
                var height =  remainingViewHeight - ((remainingViewHeight - activeTextView.frame.height) / 2)
                
                if rect.origin.y + height > self.collectionView.contentSize.height {
                    height = self.collectionView.contentSize.height - rect.origin.y
                }
                
                rect.size.height = height
                self.collectionView.scrollRectToVisible(rect, animated:false)
            } else {
                var rect = activeTextField.frame
                rect = self.collectionView.convertRect(rect, fromView: activeTextField)
                
                let naviAndStatusBarHeight = CGFloat(64)
                let remainingViewHeight = kbRect.minY - naviAndStatusBarHeight
                var height =  remainingViewHeight - ((remainingViewHeight - activeTextField.frame.height) / 2)
                
                if rect.origin.y + height > self.collectionView.contentSize.height {
                    height = self.collectionView.contentSize.height - rect.origin.y
                }

                rect.size.height = height
                self.collectionView.scrollRectToVisible(rect, animated:false)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        isTextViewEditting = false
        activeTextView = nil
        activeTextField = nil
        
        self.collectionView.contentInset = UIEdgeInsetsZero
        self.collectionView.scrollIndicatorInsets = UIEdgeInsetsZero
    }
}