//
//  CheckOutInfoCell.swift
//  merchant-ios
//
//  Created by hungvo on 1/19/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit
import Alamofire

class CheckOutInfoCell: UICollectionViewCell {
    
    var productImageView : UIImageView!
    var brandLogoImageView : UIImageView!
    var productNameLabel : UILabel!
    var priceLabel: UILabel!
    var sizeGridButton : UIButton!
    
    var sizeGridTappedHandler: (() -> Void)?
    
    private final let ProductImageViewWidth: CGFloat = 72
    private final let BrandLogoImageViewWidth: CGFloat = 80
    private final let BrandLogoImageViewHeight: CGFloat = 35
    private final let PriceLabelHeight: CGFloat = 20
    private final let SizeGridViewWidth: CGFloat = 70
    private final let SizeGridViewHeight: CGFloat = 30
    
    private let paddingTop : CGFloat = 10
    private let paddingLeft : CGFloat = 10
    private let paddingRight : CGFloat = 7
    private let ySpacing : CGFloat = 1.5
    private let paddingLeftOfBorderView : CGFloat = 18
    
    var data: (productImageName : String?, brandLogoImageName : String?, productName: String?, priceString: String?)? {
        didSet {
            if let data = self.data {
                if let productImageName = data.productImageName, let brandLogoImageName =  data.brandLogoImageName, let productName =  data.productName, let priceString = data.priceString {
                    
                    self.productNameLabel.text = productName
                    
                    priceLabel.text = priceString
                
                    self.productImageView.af_setImageWithURL(ImageURLFactory.get(productImageName, category: .Product), placeholderImage : UIImage(named: "holder"))
        
                    self.brandLogoImageView.af_setImageWithURL(ImageURLFactory.get(brandLogoImageName, category: .Brand), placeholderImage: nil, filter: nil, imageTransition: UIImageView.ImageTransition.None, completion: { (response : Alamofire.Response<UIImage, NSError>) -> Void in
                        if let image = response.result.value {
                            let ratioOfImage = image.size.width / image.size.height
                            self.brandLogoImageView.frame = CGRectMake(self.brandLogoImageView.frame.origin.x, self.brandLogoImageView.frame.origin.y, self.brandLogoImageView.frame.height * ratioOfImage, self.brandLogoImageView.frame.height)
                        }
                    })
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = UIColor.whiteColor()
        
        let heightOfProductImageView = CGFloat(frame.height - 2 * paddingTop)
        productImageView = UIImageView (frame: CGRect(x:paddingLeft, y: paddingTop, width: ProductImageViewWidth, height: heightOfProductImageView))
        //productImageView.image = UIImage(named: "holder")
        productImageView.contentMode = .ScaleAspectFit
        productImageView.clipsToBounds = true
        contentView.addSubview(productImageView)
        
        brandLogoImageView = UIImageView (frame: CGRect(x:productImageView!.frame.maxX + paddingRight, y: paddingTop, width: BrandLogoImageViewWidth, height: BrandLogoImageViewHeight))
        //brandLogoImageView.image = UIImage(named: "holder")
        brandLogoImageView.contentMode = .ScaleAspectFit
        productImageView.clipsToBounds = true
        contentView.addSubview(brandLogoImageView!)
                
        let sizeGridView = { () -> UIView in
            
            let xPosOfGridView = frame.size.width - SizeGridViewWidth - paddingRight
            let view = UIView(frame: CGRectMake(xPosOfGridView, paddingTop, SizeGridViewWidth, SizeGridViewHeight))
            
            sizeGridButton = UIButton(frame: CGRectMake(0, 0, SizeGridViewWidth, SizeGridViewHeight))
            sizeGridButton!.setTitle(String.localize("LB_CA_SIZEGRID"), forState: .Normal)
            sizeGridButton!.titleLabel?.formatSmall()
            sizeGridButton!.setTitleColor(UIColor.secondary2(), forState: .Normal)
            sizeGridButton.contentHorizontalAlignment = .Left
            sizeGridButton?.addTarget(self, action: "sizeGridTapped:", forControlEvents: .TouchUpInside)
            view.addSubview(sizeGridButton!)
            
            let arrowImageView = UIImageView(frame: CGRectMake(SizeGridViewWidth * 2/3, 0, SizeGridViewWidth * 1/3, SizeGridViewHeight))
            arrowImageView.image = UIImage(named: "icon_arrow_small")
            arrowImageView.contentMode = .ScaleAspectFit
            view.addSubview(arrowImageView)
            
            return view
        } ()
        contentView.addSubview(sizeGridView)
        
        let xPosOfProductNameLabel = CGFloat(productImageView!.frame.maxX + paddingRight)
        productNameLabel = UILabel (frame: CGRect(x:xPosOfProductNameLabel, y: brandLogoImageView!.frame.maxY + ySpacing, width: frame.width - xPosOfProductNameLabel - paddingRight, height: PriceLabelHeight))
        productNameLabel!.formatSize(15)
        contentView.addSubview(productNameLabel!)
        
        priceLabel = UILabel (frame: CGRect(x:xPosOfProductNameLabel, y: productNameLabel!.frame.maxY + ySpacing, width: frame.width - xPosOfProductNameLabel - paddingRight, height: PriceLabelHeight))
        priceLabel!.formatSize(15)
        contentView.addSubview(priceLabel!)
        
        let borderView = UIView(frame: CGRect(x: frame.minX + paddingLeftOfBorderView, y: frame.height - 1, width: frame.width - 2*paddingLeftOfBorderView, height: 1))
        borderView.backgroundColor = UIColor.secondary1()
        contentView.addSubview(borderView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func sizeGridTapped(sender : AnyObject) {
        if let callback = self.sizeGridTappedHandler {
            callback()
        }
    }
}