//
//  CheckoutConfirmationProductCell.swift
//  merchant-ios
//
//  Created by Phan Manh Hung on 2/19/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

let CheckoutConfirmationProductCellHeight : CGFloat = 175

class CheckoutConfirmationProductCell : UICollectionViewCell {
    
    private var productImageView: UIImageView!
    private var productNameLabel: UILabel!
    private var productColorLabel: UILabel!
    private var productSizeLabel: UILabel!
    private var productPriceLabel: UILabel!
    private var brandImageView : UIImageView!
    private var lblQuantity: UILabel!
    private var lblQuantityValue: UILabel!
    
    var data: CartItem? {
        didSet {
            if let data = self.data {
                self.productNameLabel.text = data.skuName
                
                let existOrEmpty = { (value: String?) -> String in
                    if value != nil {
                        return value!
                    }
                    return ""
                }
                
                self.productSizeLabel.text = String.localize("LB_CA_PI_SIZE") + " : " + existOrEmpty(data.sizeName)
                self.productColorLabel.text = String.localize("LB_CA_PI_COLOR") + " : " + existOrEmpty(data.colorName)
                self.productImageView.af_setImageWithURL(
                    ImageURLFactory.get(data.productImage, category: .Product),
                    placeholderImage: UIImage(named: "spacer"),
                    filter: nil,
                    imageTransition: .CrossDissolve(0.3),
                    completion: nil
                )
                self.brandImageView.af_setImageWithURL(
                    ImageURLFactory.get(data.brandImage, category: .Brand),
                    placeholderImage: UIImage(named: "spacer"),
                    filter: nil,
                    imageTransition: .CrossDissolve(0.3),
                    completion: { (response) -> Void in
                        
                        if let image = response.result.value {
                            let imageWidth = image.size.width
                            let imageHeight = image.size.height
                            let imageRatio = imageWidth / imageHeight
                            
                            self.brandImageView.frame = CGRectMake(self.brandImageView.frame.origin.x, self.brandImageView.frame.origin.y, self.brandImageView.frame.size.height * imageRatio, self.brandImageView.frame.size.height)
                        }
                        
                })
                
                let priceText = NSMutableAttributedString()
                
                let saleFont = UIFont.systemFontOfSize(16)
                let retailFont = UIFont.systemFontOfSize(11)
                
                if data.priceSale > 0 {
                    if let priceSale = data.priceSale.formatPrice() {
                        
                        let saleText = NSAttributedString(
                            string: priceSale + " ",
                            attributes: [
                                NSForegroundColorAttributeName: UIColor(hexString: "#DB1E49"),
                                NSFontAttributeName: saleFont,
                                NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleNone.rawValue
                            ]
                        )
                        priceText.appendAttributedString(saleText)
                    }
                    
                    if let priceRetail = data.priceRetail.formatPrice() where data.priceRetail != data.priceSale {
                        
                        let retailText = NSAttributedString(
                            string: priceRetail,
                            attributes: [
                                NSForegroundColorAttributeName: UIColor(hexString: "#757575"),
                                NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue,
                                NSFontAttributeName: retailFont,
                                NSBaselineOffsetAttributeName: (saleFont.capHeight - retailFont.capHeight) / 2
                            ]
                        )
                        
                        priceText.appendAttributedString(retailText)
                    }
                    
                } else {
                    
                    if let priceRetail = data.priceRetail.formatPrice() where data.priceRetail != data.priceSale {
                        
                        let retailText = NSAttributedString(
                            string: priceRetail,
                            attributes: [
                                NSForegroundColorAttributeName: UIColor(hexString: "#4A4A4A"),
                                NSFontAttributeName: saleFont
                            ]
                        )
                        priceText.appendAttributedString(retailText)
                    }
                }
                self.productPriceLabel.attributedText = priceText
                
                lblQuantityValue.text = "X" + existOrEmpty(data.qty.formatQuantity())
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let SeparatorHeight = CGFloat(1)
        let ProductContentHeight = CGFloat(125)
        let ProductMarginTop = CGFloat(14)
        let MarginLeft = CGFloat(14)
        let DetailFontSize = 14

        //
        let productContainer = { () -> UIView in
            
            let view = UIView(frame: CGRectMake(0, 0, frame.width, ProductContentHeight))
            
            let productImageView = { () -> UIImageView in
                
                let imageView = UIImageView(frame:CGRectMake(MarginLeft, ProductMarginTop, 80, 94))
                imageView.contentMode = .ScaleAspectFit
                
                return imageView
            } ()
            view.addSubview(productImageView)
            self.productImageView = productImageView

            //
            let detailPaddingLeft = CGFloat(16)
            let detailWidth = view.frame.width - productImageView.frame.maxX - detailPaddingLeft
            let halfDetailWidth = detailWidth / 2
            let detailHeight = CGFloat(18)
            
            let detailViewContainer = { () -> UIView in
                
                let view = UIView(frame: CGRectMake(productImageView.frame.maxX + detailPaddingLeft, ProductMarginTop, detailWidth, productImageView.frame.height))
                
                //
                let brandImageView = { () -> UIImageView in
                    
                    let imageView = UIImageView(frame: CGRectMake(0, 0, detailWidth, 30))
                    self.brandImageView = imageView
                    
                    return imageView
                } ()
                view.addSubview(brandImageView)
                
                //
                let productNameLabel = { () -> UILabel in
                    
                    let label = UILabel(frame: CGRectMake(0, brandImageView.frame.maxY, detailWidth, detailHeight))
                    label.formatSize(DetailFontSize)
                    label.textColor = UIColor.secondary3()
                    label.text = "Kate Spade"
                    
                    return label
                } ()
                view.addSubview(productNameLabel)
                self.productNameLabel = productNameLabel
                
                //
                let colorLabel = { () -> UILabel in
                    
                    let label = UILabel(frame: CGRectMake(0, productNameLabel.frame.maxY, halfDetailWidth, detailHeight))
                    label.formatSize(DetailFontSize)
                    label.textColor = UIColor.secondary2()
                    label.text = "Color: Red"
                    
                    return label
                } ()
                view.addSubview(colorLabel)
                self.productColorLabel = colorLabel
                
                let sizeLabel = { () -> UILabel in
                    
                    let label = UILabel(frame: CGRectMake(colorLabel.frame.maxX, productNameLabel.frame.maxY, halfDetailWidth, detailHeight))
                    label.formatSize(DetailFontSize)
                    label.textColor = UIColor.secondary2()
                    label.text = "Size: 6"
                    
                    return label
                } ()
                view.addSubview(sizeLabel)
                self.productSizeLabel = sizeLabel
                
                //
                let priceLabel = { () -> UILabel in
                    let label = UILabel(frame: CGRectMake(0, colorLabel.frame.maxY, detailWidth, detailHeight))
                    label.escapeFontSubstitution = true
                    label.text = "$4,000"
                    return label
                } ()
                view.addSubview(priceLabel)
                self.productPriceLabel = priceLabel
                
                return view
                
            } ()
            view.addSubview(detailViewContainer)
            
            return view
        } ()
        self.contentView.addSubview(productContainer)
        
        //
        let separatorView = { () -> UIView in
            
            let view = UIView(frame: CGRectMake(0, productContainer.frame.maxY - SeparatorHeight, frame.width, SeparatorHeight))
            view.backgroundColor = UIColor.backgroundGray()
            
            return view
        } ()
        self.contentView.addSubview(separatorView)
        
        //
        let otherViewContainer = { () -> UIView in
            let PaddingLeftFirstLabel = CGFloat(17)
            let PaddingLeftSecondLabel = CGFloat(98)
            let PaddingTop = CGFloat(16)
            let LabelSize = CGSizeMake(70, 18)
            let OtherViewHeight = CheckoutConfirmationProductCellHeight - ProductContentHeight
            
            let view = UIView(frame: CGRectMake(0, productContainer.frame.maxY, frame.width, OtherViewHeight))
            
            //
            let quantityLabel = { () -> UILabel in
                let label = UILabel(frame: CGRectMake(PaddingLeftFirstLabel, PaddingTop, LabelSize.width, LabelSize.height))
                label.formatSize(DetailFontSize)
                label.textColor = UIColor.secondary3()
                label.text = String.localize("LB_CA_PI_QTY")
                return label
            } ()
            view.addSubview(quantityLabel)
            self.lblQuantity = quantityLabel

            //
            let quantityValueLabel = { () -> UILabel in
                let label = UILabel(frame: CGRectMake(PaddingLeftSecondLabel, PaddingTop, LabelSize.width, LabelSize.height))
                label.formatSize(DetailFontSize)
                label.textColor = UIColor.secondary3()
                return label
            } ()
            view.addSubview(quantityValueLabel)
            self.lblQuantityValue = quantityValueLabel
            
            let separatorView = { () -> UIView in
                let viewSep = UIView(frame: CGRectMake(0, OtherViewHeight - SeparatorHeight, frame.width, SeparatorHeight))
                viewSep.backgroundColor = UIColor.backgroundGray()
                return viewSep
            } ()
            view.addSubview(separatorView)

            return view
        } ()
        self.contentView.addSubview(otherViewContainer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}