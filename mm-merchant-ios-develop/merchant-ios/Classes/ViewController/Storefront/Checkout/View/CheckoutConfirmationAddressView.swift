//
//  CheckoutConfirmationAddressView.swift
//  merchant-ios
//
//  Created by Phan Manh Hung on 2/19/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

let CheckoutConfirmationAddressViewHeight = CGFloat(118)

class CheckoutConfirmationAddressView : UIView {
    
    var lblName: UILabel!
    var lblAddress: UILabel!
    var lblPhoneNumber: UILabel!
    
    var addressHandler: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRectMake(0, 0, frame.width, CheckoutConfirmationAddressViewHeight)
        backgroundColor = UIColor.whiteColor()
        
        let viewGrayHeight = CGFloat(10)
        // top gap
        let viewTopBackgroundGray = { () -> UIView in
            let view = UIView(frame: CGRectMake(0, 0, self.frame.width, viewGrayHeight))
            view.backgroundColor = UIColor.backgroundGray()
            return view
        } ()
        addSubview(viewTopBackgroundGray)
        
        let LabelHeight = CGFloat(22)
        let PaddingLeft = CGFloat(28)
        let PaddingTop = CGFloat(12)
        let PaddingBetweenLabels = CGFloat(5)

        self.lblName = { () -> UILabel in
            let view = UILabel(frame: CGRectMake(PaddingLeft, viewTopBackgroundGray.frame.maxY + PaddingTop, self.frame.width, LabelHeight))
            view.textColor = UIColor.blackTitleColor()
            view.formatSmall()
            return view
            } ()
        addSubview(self.lblName)
        
        self.lblAddress = { () -> UILabel in
            let view = UILabel(frame: CGRectMake(PaddingLeft, self.lblName.frame.maxY + PaddingBetweenLabels, self.frame.width, LabelHeight))
            view.textColor = UIColor.blackTitleColor()
            view.formatSmall()
            return view
            } ()
        addSubview(self.lblAddress)
        
        self.lblPhoneNumber = { () -> UILabel in
            let view = UILabel(frame: CGRectMake(PaddingLeft, self.lblAddress.frame.maxY + PaddingBetweenLabels, self.frame.width, LabelHeight))
            view.textColor = UIColor.blackTitleColor()
            view.formatSmall()
            return view
            } ()
        addSubview(self.lblPhoneNumber)
        
        let arrowRightMargin = CGFloat(17)
        let ArrowWidth = CGFloat(32)

        let arrowView = { () -> UIImageView in
            let imageView = UIImageView(frame: CGRectMake(frame.width - ArrowWidth - arrowRightMargin, (CheckoutConfirmationAddressViewHeight - ArrowWidth) / 2, ArrowWidth, ArrowWidth))
            imageView.image = UIImage(named: "icon_arrow_small")
            imageView.contentMode = .ScaleAspectFit
            return imageView
        } ()
        addSubview(arrowView)

        // bottom gap
        let viewBotBackgroundGray = { () -> UIView in
            let view = UIView(frame: CGRectMake(0, CheckoutConfirmationAddressViewHeight - viewGrayHeight, self.frame.width, viewGrayHeight))
            view.backgroundColor = UIColor.backgroundGray()
            return view
        } ()
        addSubview(viewBotBackgroundGray)

        let singleTap = UITapGestureRecognizer(target: self, action: "viewDidTapped")
        addGestureRecognizer(singleTap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func viewDidTapped() {
        if let callback = self.addressHandler {
            callback()
        }
    }
}