//
//  CheckOutSummaryCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 8/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class CheckOutSummaryCell : UICollectionViewCell {
    
    var textLabel : UILabel!
    var priceLabel : UILabel!
    var checkOutButton : UIButton!
    
    private final let TextLabelWidth: CGFloat = 40
    private final let PriceLabelWidth: CGFloat = 200
    private final let CheckOutButtonWidth: CGFloat = 117
    
    private let paddingTop : CGFloat = 20
    private let paddingLeft : CGFloat = 20
    private let paddingTopOfCheckoutButton : CGFloat = 11
    private let paddingRightOfCheckoutButton : CGFloat = 13
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        
        textLabel = UILabel(frame: CGRect(x: paddingLeft, y: paddingTop, width: TextLabelWidth, height: bounds.height - 2 * paddingTop))
        textLabel.formatSize(15)
        textLabel.text = String.localize("LB_CA_EDITITEM_SUBTOTAL") + ":"
        addSubview(textLabel)
        
        priceLabel = UILabel(frame: CGRect(x: textLabel.frame.maxX, y: paddingTop, width: PriceLabelWidth, height: bounds.height - 2 * paddingTop))
        priceLabel.font = UIFont.boldSystemFontOfSize(17.0)
        addSubview(priceLabel)
        
        let xPosOfCheckOutButton = frame.width - CheckOutButtonWidth - paddingRightOfCheckoutButton
        checkOutButton = UIButton(frame: CGRect(x: xPosOfCheckOutButton , y:  paddingTopOfCheckoutButton, width: CheckOutButtonWidth, height: bounds.height - 2 * paddingTopOfCheckoutButton))
        checkOutButton.formatPrimary()
        checkOutButton.setTitle(String.localize("LB_OK"), forState: .Normal)
        addSubview(checkOutButton)
        
        let borderView = UIView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: 1))
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}