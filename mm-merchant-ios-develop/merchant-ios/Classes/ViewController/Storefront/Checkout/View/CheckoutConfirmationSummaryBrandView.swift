//
//  CheckoutConfirmationSummaryBrandView.swift
//  merchant-ios
//
//  Created by hungvo on 3/4/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import UIKit

let CheckoutConfirmationBrandFapioViewHeight = CGFloat(50)
let CheckoutConfirmationBrandTotalPriceViewHeight = CGFloat(50)
let CheckoutConfirmationCommentBoxViewHeight = CGFloat(110)

let CommentPlaceHolder = "注明"

class CheckoutConfirmationSummaryBrandView: UICollectionReusableView, UITextFieldDelegate, UITextViewDelegate {
    
    var lblPrice: UILabel!
    var tfFapiao: UITextField!
    var tvComment: UITextView!
    
    var data : ShoppingCartSectionData!
    var textViewBeginEditting : (() -> Void)?
    var textViewEndEditting : (() -> Void)?
    var textFieldBeginEditting : (() -> Void)?
    var textFieldEndEditting : (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let DetailFontSize = 14
        
        self.frame = CGRectMake(0, 0, frame.width, CheckoutConfirmationBrandTotalPriceViewHeight + CheckoutConfirmationCommentBoxViewHeight + CheckoutConfirmationBrandFapioViewHeight)
        backgroundColor = UIColor.whiteColor()
        
        let fapioView = { () -> UIView in
            
            let fapioLabelPaddingLeft = CGFloat(17)
            let fapioTitlePaddingLeft = CGFloat(98)
            let fapioLabelWidth = CGFloat(70)
            
            let view = UIView(frame: CGRectMake(0 , 0 , frame.size.width , CheckoutConfirmationBrandFapioViewHeight))

            let lblfapiao = { () -> UILabel in
                let view = UILabel(frame: CGRectMake(fapioLabelPaddingLeft, 0, fapioLabelWidth, view.frame.height))
                view.text = String.localize("LB_CA_FAPIAO_TITLE")
                view.formatSize(DetailFontSize)
                view.textColor = UIColor.secondary3()
                return view
            } ()
            view.addSubview(lblfapiao)
            
            self.tfFapiao = { () -> UITextField in
                let textField = UITextField(frame: CGRectMake(fapioTitlePaddingLeft, 0, view.frame.width - fapioTitlePaddingLeft, view.frame.height))
                textField.placeholder = "输入指定抬头"
                textField.textColor = UIColor.secondary3()
                if textField.delegate == nil {
                    textField.delegate = self
                }
                return textField
            } ()
            view.addSubview(self.tfFapiao)

            let separatorView  = UIView(frame: CGRectMake(0, view.frame.height - 1, view.frame.width, 1))
            separatorView.backgroundColor = UIColor.backgroundGray()
            view.addSubview(separatorView)
            
            return view
            } ()
        addSubview(fapioView)
        
        let totalPriceView = { () -> UIView in
            let view = UIView(frame: CGRectMake(0 , CheckoutConfirmationBrandFapioViewHeight , frame.size.width , CheckoutConfirmationBrandTotalPriceViewHeight))
            
            
            let MarginTop = CGFloat(17)
            let MarginLeft = CGFloat(17)
            let MarginRight = CGFloat(29)
            
            let LabelWidth = CGFloat(100)
            let LabelHeight = CGFloat(20)
            
            let lblTotalPrice = { () -> UILabel in
                let label = UILabel(frame: CGRectMake(MarginLeft, MarginTop, LabelWidth, LabelHeight))
                label.text = String.localize("LB_CA_MERCHANT_TOTAL");
                label.adjustsFontSizeToFitWidth = true
                label.formatSmall()
                label.textColor = UIColor.blackTitleColor()
                return label
                } ()
            view.addSubview(lblTotalPrice)
            
            self.lblPrice = { () -> UILabel in
                let label = UILabel(frame: CGRectMake(view.frame.width - LabelWidth - MarginRight, MarginTop, LabelWidth, LabelHeight))
                label.adjustsFontSizeToFitWidth = true
                label.textAlignment = .Right
                label.formatSmall()
                label.textColor = UIColor.secondary2()
                return label
                } ()
            view.addSubview(self.lblPrice)
            

            let separatorView  = UIView(frame: CGRectMake(0, view.frame.height - 1, view.frame.width, 1))
            separatorView.backgroundColor = UIColor.backgroundGray()
            view.addSubview(separatorView)
            
            return view
        } ()
        addSubview(totalPriceView)
        
        let commentBoxView = { () -> UIView in
            let view = UIView(frame: CGRectMake(0 , CheckoutConfirmationBrandFapioViewHeight + CheckoutConfirmationBrandTotalPriceViewHeight , frame.size.width , CheckoutConfirmationCommentBoxViewHeight))
            
            let PaddingLeft = CGFloat(18)
            let PaddingTop = CGFloat(13)
            let BoxHeight = CGFloat(74)
            
            self.tvComment = { () -> UITextView in
                let textView = UITextView(frame: CGRectMake(PaddingLeft, PaddingTop, view.frame.width - 2 * PaddingLeft, BoxHeight))
                textView.layer.borderWidth = 1.0
                textView.layer.borderColor = UIColor.lightGrayColor().CGColor
                textView.textColor = UIColor.secondary3()
                textView.text = CommentPlaceHolder
                if textView.delegate == nil {
                    textView.delegate = self
                }
                return textView
                } ()
            view.addSubview(self.tvComment)
            
            // bottom gap
            let viewGrayHeight = CGFloat(10)
            let viewBackgroundGray = { () -> UIView in
                let view = UIView(frame: CGRectMake(0, view.frame.height - viewGrayHeight, view.frame.width, viewGrayHeight))
                view.backgroundColor = UIColor.backgroundGray()
                return view
            } ()
            view.addSubview(viewBackgroundGray)
            
            return view
        } ()
        addSubview(commentBoxView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: UITextView Delegate
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        if let callback = self.textViewBeginEditting {
            callback()
        }
        return true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if let callback = self.textViewEndEditting {
            callback()
        }
    }
    
    //MARK: UITextField Delegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if let callback = self.textFieldBeginEditting {
            callback()
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if let callback = self.textFieldEndEditting {
            callback()
        }
    }
}