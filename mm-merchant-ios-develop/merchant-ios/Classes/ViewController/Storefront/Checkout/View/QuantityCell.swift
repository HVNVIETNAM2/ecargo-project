//
//  QuantityCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 9/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

let TagOfAddStepButton = 1001
let TagOfMinusStepButton = 1002

class QuantityCell : UICollectionViewCell{
    
    var textLabel : UILabel!
    var qtyTextField : UITextField!
    var addStepButton : UIButton!
    var minusStepButton : UIButton!
    
    private final let TextLabelWidth: CGFloat = 40
    private final let StepButtonWidth: CGFloat = 40
    private final let QtyTextFieldWidth: CGFloat = 48
    
    private let paddingTop : CGFloat = 20
    private let paddingLeft : CGFloat = 20
    private let paddingTopOfStepButton : CGFloat = 10
    private let xSpacing : CGFloat = 5
    private let paddingLeftOfBorderView : CGFloat = 18

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        
        textLabel = UILabel(frame: CGRect(x: bounds.minX + paddingLeft, y: 0, width: TextLabelWidth, height: bounds.height))
        textLabel.formatSize(15)
        textLabel.text = String.localize("LB_CA_EDITITEM_QTY")
        addSubview(textLabel)
        
        let MarginRight = CGFloat(10)

        addStepButton = UIButton(frame: CGRectMake(frame.width - MarginRight - StepButtonWidth, paddingTopOfStepButton, StepButtonWidth, bounds.height - 2*paddingTopOfStepButton))
        addStepButton.tag = TagOfAddStepButton
        addStepButton.setTitle("+", forState: .Normal)
        addStepButton.setTitleColor(UIColor.secondary1(), forState: .Normal)
        addStepButton.titleLabel?.formatSize(35)
        addSubview(addStepButton)

        qtyTextField = UITextField(frame: CGRectMake(addStepButton.frame.minX - xSpacing - QtyTextFieldWidth, bounds.minY + paddingTopOfStepButton, QtyTextFieldWidth, bounds.height - 2*paddingTopOfStepButton))
        qtyTextField.layer.borderColor = UIColor.secondary1().CGColor
        qtyTextField.layer.borderWidth = Constants.TextField.BorderWidth
        qtyTextField.textAlignment = .Center
        addSubview(qtyTextField)

        minusStepButton = UIButton(frame: CGRectMake(qtyTextField.frame.minX - xSpacing - StepButtonWidth, paddingTopOfStepButton, StepButtonWidth, bounds.height - 2*paddingTopOfStepButton))
        minusStepButton.tag = TagOfMinusStepButton
        minusStepButton.setTitle("-", forState: .Normal)
        minusStepButton.setTitleColor(UIColor.secondary1(), forState: .Normal)
        minusStepButton.titleLabel?.formatSize(35)
        addSubview(minusStepButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}