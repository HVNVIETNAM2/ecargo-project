//
//  SelectSizeCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 8/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

class SelectCircleCell : UICollectionViewCell{
    var textLabel : UILabel!
    var collectionView : UICollectionView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        textLabel = UILabel(frame: CGRect(x: bounds.minX + 10, y: bounds.minY + 10, width: bounds.width, height: 40))
        textLabel.formatSmall()
        addSubview(textLabel)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        collectionView = UICollectionView(frame: bounds, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.whiteColor()
        collectionView.showsHorizontalScrollIndicator = false
        addSubview(collectionView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}