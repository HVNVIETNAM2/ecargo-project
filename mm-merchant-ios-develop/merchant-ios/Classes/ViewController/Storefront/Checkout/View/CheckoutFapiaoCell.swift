//
//  CheckoutFapiaoCell.swift
//  merchant-ios
//
//  Created by HungPM on 3/4/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

class CheckoutFapiaoCell: UICollectionViewCell {
    
    var textField: UITextField!
    
    var rightViewTapHandler: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRectMake(0, 0, frame.width, CheckoutCellHeight)
        
        let MarginLeft = CGFloat(20)
        let lblLeft = { () -> UILabel in
            
            let label = UILabel(frame: CGRectMake(MarginLeft, 0, 70, frame.height))
            label.text = String.localize("LB_CA_FAPIAO_TITLE")
            label.formatSize(15)
            
            return label
        }()
        self.contentView.addSubview(lblLeft)
        
        let fapiaoTitle = { () -> UITextField in
            
            let textField = UITextField(frame: CGRectMake(lblLeft.frame.maxX, 0, frame.width - lblLeft.frame.maxX, frame.height))
            textField.textColor = UIColor.secondary2()
            
            return textField
        } ()
        self.contentView.addSubview(fapiaoTitle)
        self.textField = fapiaoTitle
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func viewDidTapped() {
        if let callback = self.rightViewTapHandler {
            callback()
        }
    }
}