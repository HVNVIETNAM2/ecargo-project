//
//  CheckoutAddressCell.swift
//  merchant-ios
//
//  Created by HungPM on 3/4/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

let CheckoutCellHeight = CGFloat(44)

class CheckoutAddressCell: UICollectionViewCell {
    
    var leftLabel: UILabel!
    var rightLabel: UILabel!

    var rightViewTapHandler: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRectMake(0, 0, frame.width, CheckoutCellHeight)

        let MarginLeft = CGFloat(20)
        let lblLeft = { () -> UILabel in
            
            let label = UILabel(frame: CGRectMake(MarginLeft, 0, 70, frame.height))
            label.formatSize(15)
            
            return label
        }()
        self.contentView.addSubview(lblLeft)
        self.leftLabel = lblLeft
        
        let viewRight = { () -> UIView in
            
            let view = UIView(frame: CGRectMake(lblLeft.frame.maxX, 0, frame.width - lblLeft.frame.maxX, frame.height))
            
            let arrowRightMargin = CGFloat(10)
            let ArrowWidth = CGFloat(32)
            let arrowView = { () -> UIImageView in
                let imageView = UIImageView(frame: CGRectMake(view.frame.width - ArrowWidth - arrowRightMargin, (view.frame.height - ArrowWidth) / 2, ArrowWidth, ArrowWidth))
                imageView.image = UIImage(named: "icon_arrow_small")
                imageView.contentMode = .ScaleAspectFit
                return imageView
            } ()
            view.addSubview(arrowView)

            let label = { () -> UILabel in
                
                let label = UILabel(frame: CGRectMake(0, 0, arrowView.frame.minX, view.frame.height))
                label.formatSize(15)
                return label
                
            }()
            view.addSubview(label)
            self.rightLabel = label
            
            let singleTap = UITapGestureRecognizer(target: self, action: "viewDidTapped")
            view.addGestureRecognizer(singleTap)
            
            return view
        }()
        self.contentView.addSubview(viewRight)        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func viewDidTapped() {
        if let callback = self.rightViewTapHandler {
            callback()
        }
    }
}