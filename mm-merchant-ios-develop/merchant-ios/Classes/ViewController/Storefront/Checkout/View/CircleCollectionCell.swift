//
//  SelectSizeCollectionCell.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 8/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

class CircleCollectionCell : UICollectionViewCell {

    var imageView : UIImageView!
    var label : UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        imageView = UIImageView(frame: CGRect(x: bounds.minX + 5, y: bounds.minY + 5, width: bounds.width - 10, height: bounds.height - 10))
        imageView.backgroundColor = UIColor.primary1()
        imageView.round()
        addSubview(imageView)
        label = UILabel(frame: CGRect(x: bounds.minX + 5, y: bounds.minY + 5, width: bounds.width - 10, height: bounds.height - 10))
        label.formatSmall()
        label.textAlignment = .Center
        addSubview(label)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}