//
//  CheckoutConfirmationBrandView.swift
//  merchant-ios
//
//  Created by Phan Manh Hung on 2/19/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

let CheckoutConfirmationBrandSectionViewHeight = CGFloat(44)
let CheckoutConfirmationBrandMerchantImageWidth = CGFloat(80)
let CheckoutConfirmationBrandContentMarginLeft = CGFloat(15)

class CheckoutConfirmationBrandView : UICollectionReusableView {
 
    var merchantImageView: UIImageView!
    var merchantNameLabel: UILabel!
    
    var data: ShoppingCartSectionData? {
        didSet {
            if let data = self.data {
                
                if let merchantImageKey = data.merchant?.merchantImage {
                    self.merchantImageView.af_setImageWithURL(ImageURLFactory.get(merchantImageKey, category: .Merchant), placeholderImage : UIImage(named: "spacer"))
                }
                
                merchantNameLabel.text = data.merchant?.merchantName
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = CGRectMake(0, 0, frame.width, CheckoutConfirmationBrandSectionViewHeight)
        backgroundColor = UIColor.whiteColor()

        let MarginTop = CGFloat(10)
        let MarginLeft = CGFloat(14)
        let MarginBot = CGFloat(5)
        let ContentHeight = CheckoutConfirmationBrandSectionViewHeight - MarginTop - MarginBot
        
        //
        let imageView = UIImageView(frame:CGRectMake(MarginLeft, MarginTop, CheckoutConfirmationBrandMerchantImageWidth, ContentHeight))
        imageView.contentMode = .ScaleAspectFit
        addSubview(imageView)
        self.merchantImageView = imageView
        
        //
        self.merchantNameLabel = { () -> UILabel in
            let xPos = imageView.frame.maxX + CheckoutConfirmationBrandContentMarginLeft
            let label = UILabel(frame: CGRectMake(xPos, MarginTop, frame.width - xPos, ContentHeight))
            label.adjustsFontSizeToFitWidth = true
            label.formatSmall()
            label.textColor = UIColor.blackTitleColor()
            return label
            } ()
        addSubview(self.merchantNameLabel)
        
        let separatorHeight = CGFloat(1)
        let separatorView = { () -> UIView in
            let view = UIView(frame: CGRectMake(0, CheckoutConfirmationBrandSectionViewHeight - separatorHeight, frame.width, separatorHeight))
            view.backgroundColor = UIColor.backgroundGray()
            
            return view
        } ()
        addSubview(separatorView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}