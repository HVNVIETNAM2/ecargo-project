//
//  CheckOutAnimation.swift
//  merchant-ios
//
//  Created by HungPM on 2/4/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

class CheckOutAnimation : UIView {
    
    private var selectedColorImageView : UIImageView!
    private var itemStartPos : CGPoint!
    private var itemSize : CGSize!
    private var redDotButton : ButtonRedDot?
    
    convenience init(
        itemImage : UIImage,
        itemSize: CGSize,
        itemStartPos: CGPoint,
        redDotButton: ButtonRedDot?
        ) {
            
            self.init(frame: UIScreen.mainScreen().bounds)
            
            self.redDotButton = redDotButton
            self.itemSize = itemSize
            self.itemStartPos = itemStartPos
            
            let frame = CGRectMake(
                self.itemStartPos.x - self.itemSize.width / 2,
                self.itemStartPos.y - self.itemSize.height / 2,
                self.itemSize.width,
                self.itemSize.height
            )
            
            let imageView = UIImageView(frame: frame)
            imageView.image = itemImage
            imageView.viewBorder()
            imageView.round()
            
            self.selectedColorImageView = UIImageView(frame: frame)
            self.selectedColorImageView.image = imageView.imageValue()
            
    }
    
    func frameFromCenter(center: CGPoint, width: CGFloat, height: CGFloat) -> CGRect {
        let halfWidth = width / 2
        let halfHeight = height / 2
        
        return CGRectMake(center.x - halfWidth, center.y - halfHeight, width, height)
    }
    
    func showAnimation() {
        
        if let redDotButton = self.redDotButton {
            let prepareAnimation = {
                
                let initRatio = CGFloat(0.5)
                
                self.selectedColorImageView.alpha = 0.5
                self.selectedColorImageView.frame = self.frameFromCenter(
                    self.itemStartPos,
                    width: self.itemSize.width * initRatio,
                    height: self.itemSize.height * initRatio
                )
                
                self.addSubview(self.selectedColorImageView)
            }
            prepareAnimation()
            
            let step3 = {
                
                let destPoint = redDotButton.convertPoint(redDotButton.redDotCenter(), toView: self)
                let targetFrame = self.frameFromCenter(destPoint, width: redDotButton.redDotSize().width, height: redDotButton.redDotSize().height)
                
                UIView.animateWithDuration(0.3, delay: 0,
                    options: .CurveEaseIn,
                    animations: { () -> Void in
                        self.selectedColorImageView.frame = targetFrame
                        self.selectedColorImageView.alpha = 0.3
                    },
                    completion: { (success) -> Void in
                        self.redDotButton?.animateRedDot()
                        self.removeFromSuperview()
                    }
                )
            }
            
            let step2 = {
                
                let initRatio = CGFloat(0.8)
                let moveUpDelta = CGFloat(80)
                let targetCenter = CGPointMake(self.selectedColorImageView.center.x, self.selectedColorImageView.center.y - moveUpDelta)
                let targetFrame = self.frameFromCenter(targetCenter, width: self.itemSize.width * initRatio, height: self.itemSize.height * initRatio)
                
                UIView.animateWithDuration(0.25, delay: 0,
                    options: .CurveLinear,
                    animations: { () -> Void in
                        self.selectedColorImageView.frame = targetFrame
                    },
                    completion: { (success) -> Void in
                        step3()
                    }
                )
                
            }
            
            let step1 = {
                
                let moveUpDelta = CGFloat(30)
                let targetCenter = CGPointMake(self.itemStartPos.x, self.itemStartPos.y - moveUpDelta)
                let targetFrame = self.frameFromCenter(targetCenter, width: self.itemSize.width, height: self.itemSize.height)
                
                UIView.animateWithDuration(0.15, delay: 0,
                    options: .CurveLinear,
                    animations: { () -> Void in
                        self.selectedColorImageView.alpha = 1
                        self.selectedColorImageView.frame = targetFrame
                    },
                    completion: { (success) -> Void in
                        step2()
                    }
                )
                
            }
            
            step1()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}