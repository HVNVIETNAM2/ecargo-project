//
//  MobileForgotPasswordViewController.swift
//  merchant-ios
//
//  Created by Sang Nguyen on 24/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//
import Foundation
import ObjectMapper
import PromiseKit


class MobileForgotPasswordViewController : MmViewController, SwipeSMSDelegate, UITextFieldDelegate, SelectGeoCountryDelegate {
    private final let WidthItemBar : CGFloat = 30
    private final let HeightItemBar : CGFloat = 25
    var mobileForgotPasswordInputView : MobileForgotPasswordInputView!
    var mobileVerification = MobileVerification()
    var isValidCode : Bool = false
    var geoCountries : [GeoCountry] = []
    var isDetectingCountry : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createSubviews()
        self.createBackButton()
        self.title = String.localize("LB_CA_RESET_PW")
        let tapGesture = UITapGestureRecognizer(target: self, action: "dissmissKeyboard")
        mobileForgotPasswordInputView.addGestureRecognizer(tapGesture)
        loadGeo()
    }
    
    func createSubviews() {
        mobileForgotPasswordInputView = MobileForgotPasswordInputView(frame: CGRect(x: 0, y: 64, width: self.view.bounds.width , height: 420))
        self.view.addSubview(mobileForgotPasswordInputView)
        mobileForgotPasswordInputView.swipeSMSView!.timeCountdown = 60
        mobileForgotPasswordInputView.swipeSMSView!.isEnableSwipe = false
        mobileForgotPasswordInputView.swipeSMSView!.swipeSMSDelegate = self
        
        mobileForgotPasswordInputView.signupInputView.countryButton.addTarget(self, action: "selectCountry:", forControlEvents: .TouchUpInside)
        mobileForgotPasswordInputView.signupInputView.codeTextField.text = "+86"
        mobileForgotPasswordInputView.signupInputView.mobileNumberTextField.becomeFirstResponder()
        mobileForgotPasswordInputView.signupInputView.codeTextField.delegate = self
        mobileForgotPasswordInputView.signupInputView.mobileNumberTextField.delegate = self
        mobileForgotPasswordInputView.passwordConfirmTextField.delegate = self
        mobileForgotPasswordInputView.passwordTextField.delegate = self
        mobileForgotPasswordInputView.pinCodeTextField.delegate = self
        self.mobileForgotPasswordInputView.passwordTextField.enabled = false
        self.mobileForgotPasswordInputView.passwordConfirmTextField.enabled = false
        self.mobileForgotPasswordInputView.viewPinCode.hidden = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    //MARK: Keyboard Management
    func dissmissKeyboard() {
        self.view.endEditing(true)
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let contentInset = UIEdgeInsetsZero;
        mobileForgotPasswordInputView.scrollView.contentInset = contentInset
        mobileForgotPasswordInputView.scrollView.scrollIndicatorInsets = contentInset
        mobileForgotPasswordInputView.scrollView.contentOffset = CGPointMake(mobileForgotPasswordInputView.scrollView.contentOffset.x, mobileForgotPasswordInputView.scrollView.contentOffset.y)
    }
    
    func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            if let keyboardSize: CGSize =  userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size {
                let heightOfset = ((keyboardSize.height + mobileForgotPasswordInputView.bounds.height + 64) - self.view.bounds.size.height) + 150 //Add more space 150 px
                if heightOfset > 0 {
                    let contentInset = UIEdgeInsetsMake(0.0, 0.0, heightOfset,  0.0);
                    mobileForgotPasswordInputView.scrollView.contentInset = contentInset
                    mobileForgotPasswordInputView.scrollView.scrollIndicatorInsets = contentInset

                }
            }
        }
    }
    
    //MARK: SwipeSMSDelegate
    func startSMS() {
        Log.debug("startSMS")
        isValidCode = false
        if self.isValidPhoneNumber() {
            firstly{
                return sendMobileVerifcation()
                }.then { _ -> Void in
                self.mobileForgotPasswordInputView.viewPinCode.hidden = false
            }
        }
        else {
            self.mobileForgotPasswordInputView.swipeSMSView?.resetWithoutCallback()
        }
    }

    func resetSMS() {
        Log.debug("resetSMS")
        if isValidCode == false {
            self.mobileForgotPasswordInputView.viewPinCode.hidden = true
        }
    }
    
    //MARK: API Service
    func sendMobileVerifcation()-> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            var parameters = [String : AnyObject]()
            parameters["MobileNumber"] = mobileForgotPasswordInputView.signupInputView.mobileNumberTextField.text
            parameters["MobileCode"] = mobileForgotPasswordInputView.signupInputView.codeTextField.text
            AuthService.sendMobileVerification(parameters) { [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            if let mobileVerification = Mapper<MobileVerification>().map(response.result.value){
                            strongSelf.mobileVerification = mobileVerification
                            }
                            Log.debug(String(data: response.data!, encoding: 4))
                            fulfill("OK")
                        } else {
                            strongSelf.handleApiResponseError(response, reject: reject)
                            strongSelf.mobileForgotPasswordInputView.swipeSMSView?.resetWithoutCallback()
                        }
                       
                    } else{
                        strongSelf.mobileForgotPasswordInputView.swipeSMSView?.resetWithoutCallback()
                        strongSelf.handleError(response, animated: true, reject: reject)
                    }
                }
                
            }
        }
    }
    
    func sendResetPassword()-> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            var parameters = [String : AnyObject]()
            parameters["MobileNumber"] = mobileForgotPasswordInputView.signupInputView.mobileNumberTextField.text
            parameters["MobileCode"] = mobileForgotPasswordInputView.signupInputView.codeTextField.text
            parameters["MobileVerificationId"] = self.mobileVerification.mobileVerificationId
            parameters["MobileVerificationToken"] = self.mobileForgotPasswordInputView.pinCodeTextField.text
            parameters["Password"] = self.mobileForgotPasswordInputView.passwordConfirmTextField.text
            AuthService.resetPassword(parameters) { [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        Log.debug(String(data: response.data!, encoding: 4))
                        
                        if response.response?.statusCode == 200 {
                            fulfill("OK")
                        } else {
                            strongSelf.handleApiResponseError(response)
                        }
                       
                    } else{
                        strongSelf.handleError(response, animated: true, reject: reject)
                    }
                }
                
            }
        }
    }

    func checkMobileVerifcation()-> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            var parameters = [String : AnyObject]()
            parameters["MobileNumber"] = mobileForgotPasswordInputView.signupInputView.mobileNumberTextField.text
            parameters["MobileCode"] = mobileForgotPasswordInputView.signupInputView.codeTextField.text
            parameters["MobileVerificationId"] = self.mobileVerification.mobileVerificationId
            parameters["MobileVerificationToken"] = mobileForgotPasswordInputView.pinCodeTextField.text
            AuthService.checkMobileVerification(parameters) { [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            Log.debug(String(data: response.data!, encoding: 4))
                            fulfill("OK")
                        } else{
                            strongSelf.handleError(response, animated: true, reject: reject)
                        }} else {
                        reject(response.result.error!)
                    }
                }
                
            }
        }
    }

    
    func checkMobileVerification() {
        firstly{
            return checkMobileVerifcation()
            }.then { _ -> Void in
                self.mobileForgotPasswordInputView.passwordTextField.enabled = true
                self.mobileForgotPasswordInputView.passwordConfirmTextField.enabled = true
                self.mobileForgotPasswordInputView.passwordTextField.becomeFirstResponder()
        }
    }
    
    func doneClicked(){
        firstly{
            return sendResetPassword()
            }.then { _ -> Void in
                self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    //MARK: Validation Region
    func isValidPhoneNumber() -> Bool{
        if mobileForgotPasswordInputView.signupInputView.mobileNumberTextField.text?.length < 1 {
            self.showError(String.localize("MSG_ERR_CA_ACCOUNT_NIL"), animated: true)
            return false
        }
        if mobileForgotPasswordInputView.signupInputView.codeTextField.text?.length < 1 {
            self.showError(String.localize("MSG_ERR_COO_NIL"), animated: true)
            return false
        }
        return true
    }
    
    func isValidData(tag: NSInteger) -> Bool {
        switch tag {
        case mobileForgotPasswordInputView.passwordTextField.tag:
            if mobileForgotPasswordInputView.passwordTextField.text?.length < 1 {
                self.showError(String.localize("MSG_ERR_CA_PW_NIL"), animated: true)
                return false
            }
            if mobileForgotPasswordInputView.passwordTextField.text!.length > 0 && RegexManager.matchesForRegexInText("(?=^.{8,50}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\\s).*$", text: mobileForgotPasswordInputView.passwordTextField.text).isEmpty {
                self.showError(String.localize("MSG_ERR_CA_PW_PATTERN"), animated: true)
                return false
            }
        case mobileForgotPasswordInputView.passwordConfirmTextField.tag:
            if mobileForgotPasswordInputView.passwordConfirmTextField.text?.length < 1 {
                self.showError(String.localize("MSG_ERR_CA_PW_NIL"), animated: true)
                return false
            }
            if mobileForgotPasswordInputView.passwordConfirmTextField.text!.length > 0 && RegexManager.matchesForRegexInText("(?=^.{8,50}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\\s).*$", text: mobileForgotPasswordInputView.passwordConfirmTextField.text).isEmpty {
                self.showError(String.localize("MSG_ERR_CA_CFM_PW_NOT_MATCH"), animated: true)
                return false
            }
            if mobileForgotPasswordInputView.passwordConfirmTextField.text != mobileForgotPasswordInputView.passwordTextField.text {
                self.showError(String.localize("MSG_ERR_CA_CFM_PW_NOT_MATCH"), animated: true)
                return false
            }
        default:
            return true
        }
        return true
    }
    
    func checkValidCode() {
        if self.mobileForgotPasswordInputView.pinCodeTextField.text?.length > 0 {
            if self.mobileForgotPasswordInputView.pinCodeTextField.text == self.mobileVerification.mobileVerificationToken {
                self.mobileForgotPasswordInputView.passwordTextField.enabled = true
                self.mobileForgotPasswordInputView.passwordConfirmTextField.enabled = true
                self.mobileForgotPasswordInputView.passwordTextField.becomeFirstResponder()
                isValidCode = true
            }
            else {
                self.showError(String.localize("LB_CA_VERCODE_INVALID"), animated: true)
            }
        }
    }
    func updateSwipeButton() {
        mobileForgotPasswordInputView.swipeSMSView!.isEnableSwipe = mobileForgotPasswordInputView.signupInputView.mobileNumberTextField.text?.length > 7 ? true : false
    }

    //MARK: UITextFieldDelegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        Log.debug("location: \(range.location) string: \(string)")
        switch textField.tag {
        case mobileForgotPasswordInputView.signupInputView.mobileNumberTextField.tag: //Mobile number
            NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "updateSwipeButton", userInfo: nil, repeats: false)
            break
        case mobileForgotPasswordInputView.signupInputView.codeTextField.tag: //Mobile Code
            if string == "" && range.location == 0 { //Don't allow todelete +
                return false
            }
            if !isDetectingCountry {
                isDetectingCountry = true
                NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "detectCountry", userInfo: nil, repeats: false)
            }
            break
        default:
            break
        }
        
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        switch textField.tag {
        case mobileForgotPasswordInputView.passwordTextField.tag:
            self.isValidData(mobileForgotPasswordInputView.passwordTextField.tag)
            break
        case mobileForgotPasswordInputView.passwordConfirmTextField.tag:
            self.isValidData(mobileForgotPasswordInputView.passwordConfirmTextField.tag)
            break
        case mobileForgotPasswordInputView.pinCodeTextField.tag:
            self.checkValidCode()
            break
        default:
            break
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        if textField.tag == mobileForgotPasswordInputView.passwordConfirmTextField.tag {
            if self.isValidData(textField.tag) {
                self.doneClicked()
            }
        }
        textField.resignFirstResponder()
        return true
    }

    
    //MARK: Button delegate
    func selectCountry(button : UIButton){
        let mobileSelectCountryViewController = MobileSelectCountryViewController()
        mobileSelectCountryViewController.selectGeoCountryDelegate = self
        self.navigationController?.pushViewController(mobileSelectCountryViewController, animated: true)
    }
    
    func selectGeoCountry(geoCountry: GeoCountry) {
        mobileForgotPasswordInputView.signupInputView.codeTextField.text = geoCountry.mobileCode
        mobileForgotPasswordInputView.signupInputView.countryButton.setTitle(geoCountry.geoCountryName, forState: .Normal)
    }
    
    func loadGeo() {
        self.showLoading()
        firstly{
            return self.listGeo()
            }.then
            { _ -> Void in
                
            }.always {
                self.stopLoading()
            }.error { _ -> Void in
                Log.error("error")
        }
        
    }
    
    func listGeo() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            GeoService.storefrontCountries(){
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response!.statusCode == 200 {
                            strongSelf.geoCountries = Mapper<GeoCountry>().mapArray(response.result.value) ?? []
                            fulfill("OK")
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            }
        }
    }

    func detectCountry() {
        let code = mobileForgotPasswordInputView.signupInputView.codeTextField.text
        for geoCountry : GeoCountry in self.geoCountries {
            Log.debug("code : \(code) countryCode: \(geoCountry.mobileCode)")
            if code == geoCountry.mobileCode {
                mobileForgotPasswordInputView.signupInputView.countryButton.setTitle(geoCountry.geoCountryName, forState: .Normal)
                isDetectingCountry = false
                return
            }
        }
        isDetectingCountry = false
        mobileForgotPasswordInputView.signupInputView.countryButton.setTitle(String.localize("LB_COUNTRY_PICK"), forState: .Normal)
    }
}