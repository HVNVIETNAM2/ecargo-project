//
//  MobileForgotPasswordInputView.swift
//  merchant-ios
//
//  Created by Sang Nguyen on 24/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class MobileForgotPasswordInputView : UIView{
    var scrollView = UIScrollView()
    var accountLabel = UILabel()
    var descriptionLabel = UILabel()
    var signupInputView : SignupInputView!
    var pinCodeTextField = UITextField()
    var viewPinCode = UIView()
    
    var passwordTextField = UITextField()
    var passwordConfirmTextField = UITextField()
    
    private final let ShortLabelWidth : CGFloat = 75
    private final let LabelHeight : CGFloat = 46
    private final let InputViewMarginLeft : CGFloat = 11
    private final let MarginTop : CGFloat = 24
    var swipeSMSView : SwipeSMSView?
    override init(frame: CGRect) {
        super.init(frame: frame)
        scrollView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        scrollView.contentSize = frame.size
        descriptionLabel.text = String.localize("LB_CA_RESET_PW_DESC")
        descriptionLabel.textAlignment = .Center
        descriptionLabel.formatSize(14)
        scrollView.addSubview(descriptionLabel)
        
        signupInputView = SignupInputView(frame: CGRect(x: InputViewMarginLeft, y: 75, width: bounds.width - InputViewMarginLeft * 2, height: 95))
        scrollView.addSubview(signupInputView!)
        swipeSMSView = SwipeSMSView(frame: CGRect(x: InputViewMarginLeft, y: signupInputView.frame.maxY + 20, width: frame.width - InputViewMarginLeft * 2, height: 45))
        
        scrollView.addSubview(swipeSMSView!)
        pinCodeTextField.format()
        pinCodeTextField.tag = 1003
        pinCodeTextField.placeholder = String.localize("LB_CA_INPUT_VERCODE")
        pinCodeTextField.keyboardType = .NumberPad
        pinCodeTextField.returnKeyType = UIReturnKeyType.Done;
        viewPinCode.addSubview(pinCodeTextField)
        
        //Create password
        passwordTextField.format()
        passwordTextField.textAlignment = .Left
        passwordTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        passwordTextField.secureTextEntry = true
        passwordTextField.tag = 1004
        passwordTextField.placeholder = String.localize("LB_ENTER_PW")
        viewPinCode.addSubview(passwordTextField)
        
        //Create password confirm
        passwordConfirmTextField.textAlignment = .Left
        passwordConfirmTextField.format()
        passwordConfirmTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        passwordConfirmTextField.secureTextEntry = true
        passwordConfirmTextField.placeholder = String.localize("LB_CA_CONF_PW")
        passwordConfirmTextField.tag = 1005
        passwordConfirmTextField.returnKeyType = UIReturnKeyType.Done;
        viewPinCode.addSubview(passwordConfirmTextField)
        scrollView.addSubview(viewPinCode)
        self.addSubview(scrollView)
        layout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
    
    func layout(){
        descriptionLabel.frame = CGRect(x: InputViewMarginLeft, y: MarginTop, width: bounds.width - InputViewMarginLeft * 2, height: LabelHeight)
        viewPinCode.frame = CGRect(x: 0, y: swipeSMSView!.frame.maxY + 20, width:frame.width, height: 140)
        pinCodeTextField.frame = CGRect(x: InputViewMarginLeft, y: 0, width:frame.width - InputViewMarginLeft * 2, height: LabelHeight )
        passwordTextField.frame = CGRect(x: InputViewMarginLeft , y: pinCodeTextField.frame.maxY + 20, width: bounds.width - InputViewMarginLeft * 2, height: LabelHeight)
        passwordConfirmTextField.frame = CGRect(x: InputViewMarginLeft , y: passwordTextField.frame.maxY - 1, width: bounds.width - InputViewMarginLeft * 2, height: LabelHeight)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}