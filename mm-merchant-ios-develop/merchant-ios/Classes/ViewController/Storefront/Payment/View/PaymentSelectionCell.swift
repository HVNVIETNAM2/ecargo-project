//
//  PaymentSelectionCell.swift
//  merchant-ios
//
//  Created by HungPM on 2/25/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

let PaymentCellHeight = CGFloat(55)

class PaymentSelectionCell : UICollectionViewCell {
    
    private var imageViewLogo: UIImageView!
    private var labelName: UILabel!
    private var paymentSelectButton: UIButton!
    
    func fill() {
        imageViewLogo.image = UIImage(named: "logo4")
        labelName.text = "支付宝"
    }
    
    func fill2() {
        imageViewLogo.image = UIImage(named: "logo4")
        labelName.text = "货到付款"
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.contentView.backgroundColor = UIColor.whiteColor()
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "paymentSelectionNotification:",
            name: "PaymentSelectionNotification",
            object: nil
        )

        self.frame = CGRectMake(0, 0, frame.width, PaymentCellHeight)

        let logo = { () -> UIImageView in
            let MarginLeft = CGFloat(18)
            let ImageWidth = CGFloat(34)
            
            let imageView = UIImageView(frame: CGRectMake(MarginLeft, (PaymentCellHeight - ImageWidth) / 2, 34, 34))
            imageView.contentMode = .ScaleAspectFit
            
            return imageView
        }()
        self.contentView.addSubview(logo)
        self.imageViewLogo = logo

        let selectButton = { () -> UIButton in
            let MarginRight = CGFloat(21)

            let button = UIButton(type: .Custom)
            button.config(
                normalImage: UIImage(named: "icon_checkbox_unchecked"),
                selectedImage: UIImage(named: "icon_checkbox_checked")
            )
            button.addTarget(self, action: "paymentItemSelect", forControlEvents: .TouchUpInside)
            button.sizeToFit()
            button.frame = CGRectMake(frame.width - MarginRight - button.frame.width, (PaymentCellHeight - button.frame.height) / 2, button.frame.width, button.frame.height)
            
            return button
        }()
        self.contentView.addSubview(selectButton)
        self.paymentSelectButton = selectButton

        let name = { () -> UILabel in
            let MarginLeft = CGFloat(35)
            let xPos = logo.frame.maxX + MarginLeft
            let label = UILabel(frame: CGRectMake(xPos, 0, selectButton.frame.minX - xPos, PaymentCellHeight))
            label.formatSingleLine(15)
            label.textColor = UIColor.blackColor()
            
            return label
        }()
        self.contentView.addSubview(name)
        self.labelName = name

        let separatorHeight = CGFloat(1)
        let separatorView = { () -> UIView in
            
            let view = UIView(frame: CGRectMake(0, PaymentCellHeight - separatorHeight, frame.width, separatorHeight))
            view.backgroundColor = UIColor.backgroundGray()
            
            return view
        } ()
        self.contentView.addSubview(separatorView)
    }
    
    func paymentSelectionNotification(notification: NSNotification) {
        if let object = notification.object where object !== self {
            self.paymentSelectButton.selected = false
        }
    }

    func paymentItemSelect() {
        NSNotificationCenter.defaultCenter().postNotificationName("PaymentSelectionNotification", object: self)
        self.paymentSelectButton.selected = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}