//
//  PaymentMethodSelectionViewController.swift
//  merchant-ios
//
//  Created by HungPM on 2/25/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

class PaymentMethodSelectionViewController : MmViewController {
    
    private final let PaymentSelectionCellID = "PaymentSelectionCellID"
    private final let DefaultCellID = "DefaultCellID"
    
    private final let TopGapViewHeight = CGFloat(11)
    private final let ButtonViewHeight = CGFloat(64)
    
    private var buttonConfirm : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBarHidden = false

        self.view.backgroundColor = UIColor.backgroundGray()
        self.title = String.localize("LB_CA_ORDER_CONFIRMATION")
        
        setupNavigationBar()
        
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: self.DefaultCellID)
        
        self.collectionView!.registerClass(PaymentSelectionCell.self, forCellWithReuseIdentifier: self.PaymentSelectionCellID)
        self.collectionView.backgroundColor = UIColor.clearColor()
        self.collectionView.alwaysBounceVertical = true
        
        let buttonView = { () -> UIView in
            
            let frame = CGRectMake(0, self.collectionView.frame.maxY, self.collectionView.frame.width, ButtonViewHeight)
            
            let view = UIView(frame: frame)
            view.backgroundColor = UIColor.whiteColor()
            
            let confirmButton = { () -> UIButton in
                
                let marginTop = CGFloat(12)
                let marginRight = CGFloat(15)
                let buttonSize = CGSizeMake(96, 41)
                
                let button = UIButton(type: .Custom)
                button.frame = CGRectMake(frame.width - marginRight - buttonSize.width, marginTop, buttonSize.width, buttonSize.height)
                button.setTitle(String.localize("LB_CA_CONFIRM_PAYMENT"), forState: .Normal)
                button.addTarget(self, action: "confirmButtonTapped", forControlEvents: .TouchUpInside)
                button.layer.cornerRadius = 3.0
                button.formatPrimary()
                return button
            } ()
            view.addSubview(confirmButton)
            self.buttonConfirm = confirmButton
            
            let labelPrice = { () -> UILabel in
                let marginLeft = CGFloat(22)

                let label = UILabel(frame: CGRectMake(marginLeft, 0, confirmButton.frame.minX - marginLeft, frame.height))
                
                let priceText = NSMutableAttributedString()
                
                let textFont = UIFont.systemFontOfSize(14)
                let valueFont = UIFont.systemFontOfSize(15)
                
                let text = NSAttributedString(
                    string: String.localize("LB_CA_TOTAL") + "：",
                    attributes: [
                        NSForegroundColorAttributeName: UIColor(hexString: "#8e8e8e"),
                        NSFontAttributeName: textFont
                    ]
                )
                priceText.appendAttributedString(text)
                
                let value = NSAttributedString(
                    string: "¥9,000",
                    attributes: [
                        NSForegroundColorAttributeName: UIColor(hexString: "#4a4a4a"),
                        NSFontAttributeName: valueFont,
                        NSBaselineOffsetAttributeName: (textFont.capHeight - valueFont.capHeight) / 2
                    ]
                )
                priceText.appendAttributedString(value)
                
                label.attributedText = priceText

                return label
            } ()
            view.addSubview(labelPrice)
            
            return view
        } ()
        
        self.view.addSubview(buttonView)
    }
    
    func setupNavigationBar() {
        self.createBackButton()
    }
    
    //MARK: Actions
    func confirmButtonTapped() {
        Log.debug("confirmButtonTapped")
    }
    
    //MARK: CollectionView
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        switch collectionView {
        case self.collectionView!:
            return 1
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.collectionView!:
            return 2
        default:
            return 0
        }
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch collectionView {
            
        case self.collectionView!:
            
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PaymentSelectionCellID, forIndexPath: indexPath)
            
            if cell.dynamicType == PaymentSelectionCell.self {
                let itemCell = cell as! PaymentSelectionCell
                
                switch (indexPath.row) {
                case 0:
                    itemCell.fill()
                    break;
                case 1:
                    itemCell.fill2()
                    break;
                default:
                    break;
                    
                }
            }
            
            return cell
            
        default:
            return self.defaultCell(collectionView, cellForItemAtIndexPath: indexPath)
        }
        
    }
    
    func defaultCell(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCellWithReuseIdentifier(DefaultCellID, forIndexPath: indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        switch collectionView {
        case self.collectionView!:
            return CGSizeMake(self.view.frame.size.width, PaymentCellHeight)
        default:
            return CGSizeZero
        }
        
    }
    
    //MARK: View Config
    // config tab bar
    override func shouldHideTabBar() -> Bool {
        return true
    }
    
    override func collectionViewBottomPadding() -> CGFloat {
        return ButtonViewHeight
    }
    
    override func collectionViewTopPadding() -> CGFloat {
        return TopGapViewHeight
    }
}