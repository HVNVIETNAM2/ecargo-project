//
//  VerticalSnapFlowLayout.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 11/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class CenterCellCollectionViewFlowLayout: UICollectionViewFlowLayout {
    var spacingLine : CGFloat = 30
    var cellHeight : CGFloat = 60
    override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        if let cv = self.collectionView {
            
            let cvBounds = cv.bounds
            let halfHeight = cvBounds.size.height * 0.5;
            let proposedContentOffsetCenterY = proposedContentOffset.y + halfHeight
            
            if let attributesForVisibleCells = (self.layoutAttributesForElementsInRect(cvBounds))! as[UICollectionViewLayoutAttributes]? {
                
                var candidateAttributes : UICollectionViewLayoutAttributes?
                if attributesForVisibleCells.count > 0 {
                    for attributes in attributesForVisibleCells {
                        
                        // == Skip comparison with non-cell items (headers and footers) == //
                        if attributes.representedElementCategory != UICollectionElementCategory.Cell {
                            continue
                        }
                        
                        if let candAttrs = candidateAttributes {
                            
                            let a = attributes.center.y - proposedContentOffsetCenterY
                            let b = candAttrs.center.y - proposedContentOffsetCenterY
                            
                            if fabsf(Float(a)) < fabsf(Float(b)) {
                                candidateAttributes = attributes;
                            }
                            
                        }
                        else { // == First time in the loop == //
                            
                            candidateAttributes = attributes;
                            continue;
                        }
                        
                        
                    }
                } else {
                    candidateAttributes = UICollectionViewLayoutAttributes()
                }
                let itemIndex = ceil (((halfHeight - cellHeight) + (candidateAttributes!.center.y - halfHeight)) / (cellHeight + spacingLine))
                let postY = (-(halfHeight - cellHeight) + itemIndex * (cellHeight + spacingLine)) - 4
                Log.debug("*************** \(candidateAttributes!.center.y - halfHeight)  postY \(itemIndex) temp \(postY)")
                return CGPoint(x: proposedContentOffset.x, y: round(postY))
                
            }
            
        }
        
        // Fallback
        return super.targetContentOffsetForProposedContentOffset(proposedContentOffset)
    }
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        if let layoutAttributes = super.layoutAttributesForElementsInRect(rect){
            for layoutAttribute : UICollectionViewLayoutAttributes in layoutAttributes {
                layoutAttribute.transform = CGAffineTransformIdentity
            }
            return layoutAttributes
        }
        return nil
    }
    
}