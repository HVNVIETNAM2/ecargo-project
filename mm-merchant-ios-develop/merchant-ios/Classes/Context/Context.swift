//
//  Context.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

class Context {
    class func getTokenHeader() -> [String : String]{
        if (NSUserDefaults.standardUserDefaults().objectForKey("token") == nil){
            return ["x-access-token" : ""]
        }
        return["x-access-token" : (NSUserDefaults.standardUserDefaults().objectForKey("token") as! String),]

    }
    
    class func setToken(tokenString : String) -> Void {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(tokenString,forKey: "token")
        preferences.synchronize()
    }
    
    class func getDeviceToken() -> String {
        if (NSUserDefaults.standardUserDefaults().objectForKey("deviceToken") == nil){
            return ""
        }
        return NSUserDefaults.standardUserDefaults().objectForKey("deviceToken") as! String
    }
    
    class func setDeviceToken(tokenString : String) -> Void {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(tokenString,forKey: "deviceToken")
        preferences.synchronize()
    }
    
    class func getUserId() -> Int {
        if (NSUserDefaults.standardUserDefaults().objectForKey("userId") == nil){
            return 0
        }
        return NSUserDefaults.standardUserDefaults().objectForKey("userId") as! Int
    }
    
    class func setUserId(userId : Int) -> Void {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(userId,forKey: "userId")
        preferences.synchronize()
    }
    
    class func getUserKey() -> String {
        if (NSUserDefaults.standardUserDefaults().objectForKey("userKey") == nil){
            return "0"
        }
        return NSUserDefaults.standardUserDefaults().objectForKey("userKey") as! String
    }
    
    class func setUserKey(userKey : String) -> Void {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(userKey,forKey: "userKey")
        preferences.synchronize()
    }
    
    class func getAuthenticatedUser() ->Bool {
        if (NSUserDefaults.standardUserDefaults().objectForKey("authenticated") == nil){
            return false
        }
        return NSUserDefaults.standardUserDefaults().objectForKey("authenticated") as! Bool
    }
    
    class func setAuthenticatedUser(authenticated : Bool){
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(authenticated,forKey: "authenticated")
        preferences.synchronize()
    }
    
    class func getCc() -> String{
        if (NSUserDefaults.standardUserDefaults().objectForKey("cc") == nil){
            return "CHS"
        }
        return NSUserDefaults.standardUserDefaults().objectForKey("cc") as! String
    }

    class func setCc(cc : String){
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(cc,forKey: "cc")
        switch cc.lowercaseString {
        case "en":
            preferences.setObject(["en", "zh-Hans", "zh_HK", ], forKey: "AppleLanguages")
            break
        case "cht":
            preferences.setObject(["zh_HK", "zh-Hans", "en", ], forKey: "AppleLanguages")
            break
        case "chs":
            preferences.setObject(["zh-Hans", "zh_HK", "en", ], forKey: "AppleLanguages")
            break
        default:
            break
        }
        preferences.synchronize()
    }
  
    class func getUsername() -> String{
        let username = NSUserDefaults.standardUserDefaults().objectForKey("Username")
        return (username ?? "") as! String
    }
    
    class func setUsername(username : String){
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(username,forKey: "Username")
        preferences.synchronize()
    }
    
    class func getPassword() -> String{
        let password = NSUserDefaults.standardUserDefaults().objectForKey("Password")
        return (password ?? "") as! String
        
    }
    
    class func setPassword(username : String){
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(username,forKey: "Password")
        preferences.synchronize()
    }
    
    class func getHistory() -> [String]{
        
        if let historyWeight = NSUserDefaults.standardUserDefaults().dictionaryForKey("History_weight") as? [String: Int]{
            let histories = historyWeight.keys.sort({ (firstKey, secondKey) -> Bool in
                Log.debug("first: \(firstKey): \(historyWeight[firstKey]) vs second: \(secondKey): \(historyWeight[secondKey])")
                return historyWeight[firstKey] > historyWeight[secondKey]
            })
            
            return Array(histories.prefix(5))
            
        }else {
            return []
        }
    }
    
    class func setHistoryWeight(historyWeight : [String: Int]) {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(historyWeight,forKey: "History_weight")
        preferences.synchronize()
    }
    
    class func addHistory(history : String){
        var historyWeight : [String: Int] = [:]
        
        if NSUserDefaults.standardUserDefaults().dictionaryForKey("History_weight") != nil {
            historyWeight = NSUserDefaults.standardUserDefaults().dictionaryForKey("History_weight") as! [String : Int]
        }
        
        if let existWeight = historyWeight[history] { //if we got the item
            historyWeight[history] = existWeight + 1
        }else {
            historyWeight[history] = 1
        }

        for history : String in historyWeight.keys{
            Log.debug("\(history): \(historyWeight[history])")
        }
        setHistoryWeight(historyWeight)
    }
    
    static let ShoppingCartKey = "ShoppingCartKey"
    
    class func setAnonymousShoppingCartKey(cartKey: String) {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(cartKey, forKey: ShoppingCartKey)
        preferences.synchronize()
    }
    
    class func anonymousShoppingCartKey() -> String? {
        return NSUserDefaults.standardUserDefaults().stringForKey(ShoppingCartKey)
    }
    
    static let WishlistKey = "WishlistKey"
    
    class func setAnonymousWishlistKey(wishlistKey: String) {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(wishlistKey, forKey: WishlistKey)
        preferences.synchronize()
    }
    
    class func anonymousWishlistKey() -> String? {
        return NSUserDefaults.standardUserDefaults().stringForKey(WishlistKey)
    }

}
