//
//  Constant.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 22/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
struct Constants {
    struct Path {
        static let Host = "http://52.76.112.250/api"
        static let WebSocketHost = "ws://52.76.113.145:7600/"
//        static let WebSocketHost = "ws://52.76.113.145:7500/"
//        static let WebSocketHost = "ws://192.168.214.151:9000/"
        static let ParseHost = "https://api.parse.com/1/classes/"
        static let CountryHost = "https://restcountries.eu/rest/v1/"
        static let ParseHeaders = [
            "X-Parse-Application-Id" : "tSF6abJ4ZwUM6pjpVhDF4poWYmxbPQQg7DbYh8GS",
            "X-Parse-REST-API-Key" : "jib3yX9B3E4WV6iViDL8kbBQWKQWEkB66juY5Fr1",
            "Content-Type" : "application/json"
        ]
    }
    
    struct Button {
        static let Radius = CGFloat(5)
        static let BorderWidth = CGFloat(1)
    }
    
    struct TextField {
        static let BorderWidth = CGFloat(1)
        static let LeftPaddingWidth = CGFloat(15)
    }
    
    struct Font {
        static let Size = CGFloat(16)
        static let Normal = ".SFUIText-Light"
        static let Bold = ".SFUIText-SemiBold"
    }
    
    struct iOS8Font {
        static let Size = CGFloat(16)
        static let Normal = UIFont.systemFontOfSize(16).fontName
        static let Bold = UIFont.boldSystemFontOfSize(16).fontName
    }
    
    struct Value {
        static let MaxLoginAttempts = Int(3)
        static let ProductBottomViewHeight = CGFloat(150.0)
        static let CatCellHeight = CGFloat(40.0)
        static let FilterColorWidth = CGFloat(49.0)
        static let BrandImageHeight = CGFloat(80.0)
        static let BackButtonWidth : CGFloat = 30
        static let BackButtonHeight : CGFloat = 25
        static let BackButtonMarginLeft : CGFloat = -15
    }
    
    struct PlaceHolder {
        static let FieldName = "<field name>"
        static let CurrentEmail = "<current email>"
        static let CurrentMobileNumber = "<current mobile number>"
    }
    
    struct Notification {
        static let LangaugeChanged = "LanguageChanged"
    }
    
    struct Margin {
        static let Top = CGFloat(0.0)
        static let Left = CGFloat(5.0)
        static let Right = CGFloat(5.0)
        static let Bottom = CGFloat(0.0)
    }
    struct LineSpacing {
        static let ImageCell = CGFloat(5.0)
        static let SubCatCell = CGFloat(20.0)
    }
    struct Ratio
    {
        static let ProductImageHeight = CGFloat(7.0 / 6.0)
        static let PanelImageHeight = CGFloat(3.0 / 5.0)
    }
    
    struct Price {
        static let Highest : Float = 10000
    }
}