//
//  ButtonRedDot.swift
//  merchant-ios
//
//  Created by HungPM on 1/29/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import UIKit

let RedDotAnimationNotification = "RedDotAnimationNotification"

class ButtonRedDot : UIButton {
    
    private let redDot = UIImageView()
    
    override var frame : CGRect {
        didSet {
            let frameRedDot = CGRectMake(frame.width - 10, 0, 10, 10)
            redDot.frame = frameRedDot
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "animateRedDot",
            name: RedDotAnimationNotification,
            object: nil
        )
        
        let frameRedDot = CGRectMake(frame.width - 10, 0, 10, 10)
        redDot.frame = frameRedDot
        redDot.clipsToBounds = true
        redDot.alpha = 0
        redDot.backgroundColor = UIColor.redColor()
        redDot.round()
        
        addSubview(redDot)
    }
    
    func redDotCenter() -> CGPoint {
        return redDot.center
    }
    
    func redDotSize() -> CGSize {
        return redDot.bounds.size
    }
    
    func hasRedDot(value: Bool) {
        UIView.animateWithDuration(0.2) { () -> Void in
            if value {
                self.redDot.alpha = 1
            } else {
                self.redDot.alpha = 0
            }
        }
    }
    
    func animateRedDot() {
        
        self.redDot.alpha = 1
        
        let animationDuration = NSTimeInterval(0.2)
        UIView.animateWithDuration(
            animationDuration,
            delay: 0,
            options: .CurveEaseOut,
            animations: { () -> Void in
                self.redDot.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5)
            },
            completion: { (success) -> Void in
                UIView.animateWithDuration(
                    animationDuration,
                    delay: 0,
                    options: .CurveEaseIn,
                    animations: { () -> Void in
                        self.redDot.transform = CGAffineTransformIdentity
                    },
                    completion: { (success) -> Void in
                        
                    }
                )
            }
        )
        
    }
    
    func animateHeartIcon() {
        
        let redButton = UIButton(type: .Custom)
        let duration = NSTimeInterval(0.2)
        
        let prepareAnimation = {
            
            redButton.frame = self.bounds
            redButton.setImage(UIImage(named: "icon_heart_filled"), forState: .Normal)
            redButton.alpha = 0.0
            self.addSubview(redButton)
        }
        prepareAnimation()
        
        let step2 = {
            UIView.animateWithDuration(
                duration,
                delay: 0,
                options: .CurveEaseIn,
                animations: { () -> Void in
                    redButton.alpha = 1.0
                    redButton.transform = CGAffineTransformIdentity
                },
                completion: { (success) -> Void in
                    redButton.removeFromSuperview()
                }
            )
        }
        
        let step1 = {
            UIView.animateWithDuration(
                duration,
                delay: 0,
                options: .CurveEaseOut,
                animations: { () -> Void in
                    redButton.alpha = 0.5
                    redButton.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.7, 1.7)
                },
                completion: { (success) -> Void in
                    step2()
                }
            )
        }
        step1()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
}