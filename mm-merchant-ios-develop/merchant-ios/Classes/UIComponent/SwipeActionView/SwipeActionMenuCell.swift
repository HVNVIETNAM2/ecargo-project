//
//  SwipeActionMenuCell.swift
//  merchant-ios
//
//  Created by Alan YU on 5/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

enum MenuOptionPosition: Int {
    case Left
    case Right
}

class SwipeActionMenuCellData {
    
    var text: String
    var icon: UIImage?
    var backgroundColor: UIColor
    var textColor: UIColor
    var action: (() -> Void)?
    var defaultAction: Bool
    
    init(text: String = "", icon: UIImage? = nil, backgroundColor: UIColor = UIColor.grayColor(), textColor: UIColor = UIColor.whiteColor(), defaultAction: Bool = false, action: (() -> Void)? = nil) {
        self.text = text
        self.icon = icon
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.action = action
        self.defaultAction = defaultAction
    }
}

class SwipeActionMenuCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    var defaultLeftData: SwipeActionMenuCellData?
    var defaultRightData: SwipeActionMenuCellData?
    
    var leftMenuItems: [SwipeActionMenuCellData]? {
        didSet {
            self.removeFromSuperview(self.leftViewPool)
            self.leftViewPool = nil
        }
    }
    
    var rightMenuItems: [SwipeActionMenuCellData]? {
        didSet {
            self.removeFromSuperview(self.rightViewPool)
            self.rightViewPool = nil
        }
    }
    
    
    private var leftViewPool: [SwipeMenuOptionView]?
    private var rightViewPool: [SwipeMenuOptionView]?
    
    private lazy var panRecognizer: UIPanGestureRecognizer = {
        
        let recognizer = UIPanGestureRecognizer(target: self, action: "handlePan:")
        recognizer.delegate = self
        return recognizer
        
    } ()
    
    var optionWidth = CGFloat(88)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UIColor.whiteColor()
        self.addGestureRecognizer(self.panRecognizer)
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "swipeActionMenuCellDismissNotification:",
            name: "SwipeActionMenuCellDismissNotification",
            object: nil
        )
    }
    
    func swipeActionMenuCellDismissNotification(notification: NSNotification) {
        if let object = notification.object where object !== self {
            self.dismissMenu()
        }
    }
    
    func removeFromSuperview(viewPool: [SwipeMenuOptionView]?) {
        if let pool = viewPool {
            for v in pool {
                v.removeFromSuperview()
            }
        }
    }
    
    func prepareAtionMenu() {
        
        let size = self.bounds.size
        
        let prepareOptionView = { (menuItems: [SwipeActionMenuCellData], position: MenuOptionPosition) -> [SwipeMenuOptionView] in
            var viewPool = [SwipeMenuOptionView]()
            for item in menuItems {
                
                var xPos = size.width
                
                if position == .Left {
                    
                    xPos = -size.width
                    if item.defaultAction {
                        self.defaultLeftData = item
                    }
                    
                } else {
                    
                    if item.defaultAction {
                        self.defaultRightData = item
                    }
                    
                }
                
                let option = SwipeMenuOptionView(frame: CGRectMake(xPos, 0, size.width, size.height), position: position)
                option.data = item
                option.optionWidth = self.optionWidth
                
                option.actionHander = {
                    self.dismissMenu()
                }
                
                viewPool.append(option)
                self.addSubview(option)
                
            }
            
            return viewPool
            
        }
        
        if let leftMenuItems = self.leftMenuItems
            where self.leftViewPool == nil && leftMenuItems.count > 0 {
                self.leftViewPool = prepareOptionView(leftMenuItems, .Left)
        }
        
        if let rightMenuItems = self.rightMenuItems
            where self.rightViewPool == nil && rightMenuItems.count > 0 {
                self.rightViewPool = prepareOptionView(rightMenuItems, .Right)
        }
        
    }
    
    override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if gestureRecognizer === self.panRecognizer {
            let translation = self.panRecognizer.translationInView(self.superview)
            // Check for horizontal gesture
            if (fabsf(Float(translation.x)) > fabsf(Float(translation.y))) {
                return true
            }
            
            dismissMenu()
            NSNotificationCenter.defaultCenter().postNotificationName("SwipeActionMenuCellDismissNotification", object: self)
            
            return false
        }
        
        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
    
    var originalCenter = CGPointZero
    var rightCompleteOnDragRelease = false
    var leftCompleteOnDragRelease = false
    var defaultRightActionCompleteOnDrag = false
    var defaultLeftActionCompleteOnDrag = false
    
    func layoutMenuViews(referenceView: UIView, viewPool: [SwipeMenuOptionView], position: MenuOptionPosition, animated: Bool = false) {
        
        let spacing = (self.bounds.size.width - referenceView.frame.maxX) / CGFloat(viewPool.count)
        for i in 0 ..< viewPool.count {
            let view = viewPool[i]
            var delta = spacing * CGFloat(i)
            if position == .Left {
                
                if self.defaultLeftActionCompleteOnDrag {
                    delta = 0
                }
                
                UIView.animateWithDuration(
                    animated ? 0.2 : 0.0,
                    animations: { () -> Void in
                        view.frame = CGRectMake(referenceView.frame.minX - view.bounds.size.width + delta, 0, view.bounds.size.width, view.bounds.size.height)
                    }
                )
                
            } else {
                
                if self.defaultRightActionCompleteOnDrag {
                    delta = 0
                }
                
                UIView.animateWithDuration(
                    animated ? 0.2 : 0.0,
                    animations: { () -> Void in
                        view.frame = CGRectMake(referenceView.frame.maxX + delta, 0, view.bounds.size.width, view.bounds.size.height)
                    }
                )
                
            }
        }
        
    }
    
    func dismissMenu() {
        let referenceView = self.contentView
        referenceView.frame = CGRectMake(
            0,
            referenceView.frame.origin.y,
            referenceView.bounds.size.width,
            referenceView.bounds.size.height
        )
        UIView.animateWithDuration(
            0.2,
            animations: { () -> Void in
                if let viewPool = self.rightViewPool {
                    for view in viewPool {
                        let width = referenceView.bounds.size.width
                        view.frame = CGRectMake(width, 0, view.bounds.size.width, view.bounds.size.height)
                    }
                }
                
                if let viewPool = self.leftViewPool {
                    for view in viewPool {
                        view.frame = CGRectMake(-view.bounds.size.width, 0, view.bounds.size.width, view.bounds.size.height)
                    }
                }
                
            },
            completion: nil
        )
        
    }

    
    func handlePan(recognizer: UIPanGestureRecognizer) {
        
        let defaultActionPercentage = CGFloat(0.7)
        let referenceView = self.contentView
        
        if recognizer.state == .Began {
            self.originalCenter = referenceView.center
            self.prepareAtionMenu()
            NSNotificationCenter.defaultCenter().postNotificationName("SwipeActionMenuCellDismissNotification", object: self)
        }
        
        if recognizer.state == .Changed {
            
            let translation = recognizer.translationInView(self)
            
            referenceView.center = CGPointMake(originalCenter.x + translation.x, originalCenter.y)
            
            if let viewPool = self.leftViewPool {
                let delta = referenceView.frame.origin.x
                self.leftCompleteOnDragRelease = delta > (self.optionWidth * CGFloat(viewPool.count))
                
                let defaultLeftCompleteOnDrag = delta > self.bounds.size.width * defaultActionPercentage
                let animated = defaultLeftCompleteOnDrag != self.defaultLeftActionCompleteOnDrag
                self.defaultLeftActionCompleteOnDrag = defaultLeftCompleteOnDrag
                
                self.layoutMenuViews(referenceView, viewPool: viewPool, position: .Left, animated: animated)
            }
            
            if let viewPool = self.rightViewPool {
                let delta = self.bounds.size.width - referenceView.frame.maxX
                self.rightCompleteOnDragRelease = delta > (self.optionWidth * CGFloat(viewPool.count))
                
                let defaultRightCompleteOnDrag = delta > self.bounds.size.width * defaultActionPercentage
                let animated = defaultRightCompleteOnDrag != self.defaultRightActionCompleteOnDrag
                self.defaultRightActionCompleteOnDrag = defaultRightCompleteOnDrag
                
                self.layoutMenuViews(referenceView, viewPool: viewPool, position: .Right, animated: animated)
            }

        }
        
        if recognizer.state == .Ended {
            
            if self.defaultRightActionCompleteOnDrag {
                if let data = self.defaultRightData, let action = data.action {
                    action()
                }
                self.dismissMenu()
                
            } else if self.defaultLeftActionCompleteOnDrag {
                if let data = self.defaultLeftData, let action = data.action {
                    action()
                }
                self.dismissMenu()
                
            } else if self.rightCompleteOnDragRelease {
                
                UIView.animateWithDuration(
                    0.2,
                    animations: { () -> Void in
                        if let viewPool = self.rightViewPool {
                            referenceView.frame = CGRectMake(-(self.optionWidth * CGFloat(viewPool.count)), 0, referenceView.bounds.size.width, referenceView.bounds.size.height)
                            self.layoutMenuViews(referenceView, viewPool: viewPool, position: .Right)
                        }
                    },
                    completion: { (success) in

                    }
                )
                
            } else if self.leftCompleteOnDragRelease {
                
                UIView.animateWithDuration(
                    0.2,
                    animations: { () -> Void in
                        if let viewPool = self.leftViewPool {
                            referenceView.frame = CGRectMake(self.optionWidth * CGFloat(viewPool.count), 0, referenceView.bounds.size.width, referenceView.bounds.size.height)
                            self.layoutMenuViews(referenceView, viewPool: viewPool, position: .Left)
                        }
                    },
                    completion: { (success) in
                    }
                )

            } else {
                
                self.dismissMenu()
                
            }
            
        }
        
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}
