//
//  PlainHeaderView.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 15/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class PlainHeaderView : UICollectionReusableView{

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}