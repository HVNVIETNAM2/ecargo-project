//
//  SeparatorHeaderView.swift
//  merchant-ios
//
//  Created by hungvo on 1/19/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class SeparatorHeaderView : UICollectionReusableView{

    let separatorView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        
        let separatorHeight = CGFloat(1)
        let padding = CGFloat(18)
        
        separatorView.frame = CGRectMake(padding, frame.height - separatorHeight, frame.width - (2 * padding), separatorHeight)
        separatorView.backgroundColor = UIColor.secondary1()
        addSubview(separatorView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}