//
//  SearchHeaderView.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 16/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class HeaderView : UICollectionReusableView{
    var label = UILabel()
    var imageView = UIImageView()
    var borderView = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.whiteColor()
        label.text = String.localize("LB_CA_COLOUR")
        label.formatSmall()
        addSubview(label)
        addSubview(imageView)
        borderView.backgroundColor = UIColor.secondary1()
        addSubview(borderView)
        imageView.frame = CGRect(x: bounds.maxX - 60, y: bounds.minY + 5, width: 40, height: 40)
        label.frame = CGRect(x: bounds.minX + 10, y: bounds.minY, width: bounds.width - 10, height: bounds.height)
        borderView.frame = CGRect(x: bounds.minX, y: bounds.minY, width: bounds.width, height: 1)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}