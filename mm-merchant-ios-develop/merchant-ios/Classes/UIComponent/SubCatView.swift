//
//  SubCatView.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 25/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation

class SubCatView : UICollectionReusableView{
    var subCatCollectionView : UICollectionView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        let layout: DoubleSnapFlowLayout = DoubleSnapFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 50, height: bounds.height)
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        subCatCollectionView = UICollectionView(frame: bounds, collectionViewLayout: layout)
        subCatCollectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        subCatCollectionView!.backgroundColor = UIColor.whiteColor()
        subCatCollectionView!.frame = bounds
        subCatCollectionView.showsHorizontalScrollIndicator = false
        addSubview(subCatCollectionView!)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}