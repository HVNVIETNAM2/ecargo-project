//
//  ContainerView.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 6/1/2016.
//  Copyright © 2016 Koon Kit Chan. All rights reserved.
//

import Foundation
class ContainerView : UIView{
    func hide(){
        UIView.animateWithDuration(0.5, animations: {self.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.0)}, completion: {(finished : Bool) -> Void in self.hidden = true})
        
    }
    
    func show(){
        self.hidden = false
        UIView.animateWithDuration(0.5, animations: {self.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.7)}, completion: nil)
        
    }

}