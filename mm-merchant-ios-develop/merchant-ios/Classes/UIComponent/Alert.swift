//
//  Alert.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import UIKit

class Alert {
    class func alert(viewController: UIViewController, title :String, message : String, okActionComplete : (() -> Void)? = nil, cancelActionComplete : (() -> Void)? = nil){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        // Create the actions
        let okAction = UIAlertAction(title: String.localize("LB_CA_CONFIRM"), style: UIAlertActionStyle.Default) {
            UIAlertAction in
            
            if let ok = okActionComplete {
                ok()
            }
            
            Log.debug("OK Pressed")
        }
        
        let cancelAction = UIAlertAction(title: String.localize("LB_CA_CANCEL"), style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            
            if let cancel = cancelActionComplete {
                cancel()
            }

            Log.debug("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    class func alertWithSingleButton(viewController: UIViewController, title :String, message : String, buttonString : String, actionComplete : (() -> Void)? = nil){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        // Create the actions
        let alertAction = UIAlertAction(title: buttonString, style: UIAlertActionStyle.Default) {
            UIAlertAction in
            
            if let action = actionComplete {
                action()
            }
        }
        
        // Add the actions
        alertController.addAction(alertAction)
        
        // Present the controller
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }

}