//
//  CenterLayoutView.swift
//  merchant-ios
//
//  Created by Alan YU on 6/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

enum CenterLayoutMode: Int {
    case Flow
    case Central
}

class CenterLayoutView: UIView {

    var mode = CenterLayoutMode.Flow {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        let views = self.subviews
        let frame = self.bounds
        
        switch self.mode {
        case .Flow:
            
            let centerY = self.bounds.height / 2
            
            var width = CGFloat(0)
            
            for view in views {
                width += view.bounds.width
            }
            
            var lastXPos = (frame.width - width) / 2
            for view in views {
                view.center = CGPointMake(lastXPos + view.bounds.width / 2, centerY)
                lastXPos += view.bounds.width
            }
            
        case .Central:
            
            for view in views {
                view.center = CGPointMake(frame.width / 2, frame.height / 2)
            }
        }
        
    }
}