//
//  WebSocketManager.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 7/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController
import Starscream
import ObjectMapper

class WebSocketManager : WebSocketDelegate, WebSocketPongDelegate {
    static let sharedInstance = WebSocketManager()
    private var socket = WebSocket(url: NSURL(string: Constants.Path.WebSocketHost)!)
    private var timer = NSTimer()
    private var missPong = 0
    internal var isConnected: Bool {
        get {
            return socket.isConnected
        }
    }
    var convList = [Conv]() {
        didSet{
             NSNotificationCenter.defaultCenter().postNotificationName("IMDidUpdateConvList", object: nil)
        }
    }

    private init(){
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "checkConnect:", userInfo: nil, repeats: true)
    }
    
    dynamic private func checkConnect(timer : NSTimer){
        socket.writePing(NSData())
        if missPong > 3{
            reconnect()
        }
        missPong++
    }

    //MARK: Expose functions
    
    func connect(){
        socket.delegate = self
        socket.pongDelegate = self
        socket.connect()
    }
    
    func reconnect(){
        Log.debug("Reconnect")
        socket.disconnect()
        socket = WebSocket(url: NSURL(string: Constants.Path.WebSocketHost)!)
        connect()
        missPong = 0
    }
    
    func writeData(data: NSData){
        socket.writeData(data)
    }
    
    func writeMessage(socketMessage : SocketMessage){
        socket.writeMessage(socketMessage)
    }
    
    func writePing(data : NSData){
        socket.writePing(data)
    }

    //MARK: WebSocketDelegate methods
    
    func websocketDidConnect(socket: WebSocket){
        print("websocket is connected")
        sendAnnounce()
    }
    
    func websocketDidDisconnect(socket: WebSocket, error: NSError?){
        print("websocket is disconnected: \(error?.localizedDescription ?? "")")
        
    }
    
    func websocketDidReceiveMessage(socket: WebSocket, text: String){
        print("got some text: \(text)")
        let socketMessage = Mapper<SocketMessage>().map(text) ?? SocketMessage()
        websocketDidReceiveMessage(socket, socketMessage: socketMessage)
    }
    
    func websocketDidReceiveMessage(socket : WebSocket, socketMessage : SocketMessage){
        switch socketMessage.type{
        case "Init":
            convList = socketMessage.convList
            break
        case "Conv":
            //Started the conversation here
            Log.debug("Started the conversation here")
            
            if !socketMessage.convList.isEmpty{
                let conv = socketMessage.convList[0]
                convList.append(conv)
                if self.enterConversationBlock != nil {
                    self.enterConversationBlock!(conversation: conv)
                    self.enterConversationBlock = nil
                }
            }
            break
            
        default:
            let message = ChatModel()
            message.chatSendId = "2"
            message.messageContent = socketMessage.body
            message.inConversationKey = socketMessage.convKey
            NSNotificationCenter.defaultCenter().postNotificationName("IMDidReceiveMessage", object: message)
            
        }
    }
    
    
    
    
    func websocketDidReceiveData(socket: WebSocket, data: NSData){
        print("got some data: \(data.length)")
        let text = String(data: data, encoding: NSUTF8StringEncoding)
        let message = JSQMessage(senderId: "2", displayName: "Alan Yu", text: text)
        NSNotificationCenter.defaultCenter().postNotificationName("IMDidReceiveData", object: message)
    }
    
    //MARK: WebSocketDelegatePong methods

    func websocketDidReceivePong(socket: WebSocket){
        print("Got pong!")
        missPong = 0
    }
    
    //MARK: Application level function
    
    func sendAnnounce(){
        let socketMessage = SocketMessage()
        socketMessage.type = "Announce"
        socketMessage.userKey = Context.getUserKey()
        writeMessage(socketMessage)
    }
    
    func sendMessage(body: String, toConversation: Conv){
        let socketMessage = SocketMessage()
        socketMessage.type = "Msg"
        socketMessage.body = body
        socketMessage.convKey = toConversation.convKey
        writeMessage(socketMessage)
    }
    
    func sendMessage(body : String){
        if let lastConv = convList.last() {
            self.sendMessage(body, toConversation: lastConv)
        }
    }
    
    private func sendConvStart(userList : [String]){
        let socketMessage = SocketMessage()
        socketMessage.type = "ConvStart"
        socketMessage.userList = userList
        writeMessage(socketMessage)
    }
    
    
    func startConversationWithUser(userKeys: [String], completion: (conversation: Conv) -> Void){
        
        for conv in convList {
            var convUsers = Set(conv.userList)
            convUsers.remove(Context.getUserKey())
            
            if (convUsers == Set(userKeys)){
                completion(conversation: conv)
                return
            }
        }
        sendConvStart(userKeys)
        enterConversationBlock = completion
    }
    
    var enterConversationBlock : ((conversation: Conv) -> Void)?
    
    func sendMsgList(){
        let socketMessage = SocketMessage()
        socketMessage.type = "MsgList"
        socketMessage.convKey = convList[0].convKey
        writeMessage(socketMessage)
    }
    
    var currentUser : User?
    
}

