//
//  LoginManager.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 3/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class LoginManager {
    class func login(token : Token, username : String = "", password : String = ""){
        Context.setToken(token.token)
        Context.setUserId(token.userId)
        Context.setUsername(username)
        Context.setPassword(password)
        Context.setUserKey(token.userKey)
        Context.setAuthenticatedUser(true)
        MobClick.profileSignInWithPUID(username)
        
        CartHelper.upgradeShoppingCart()
        CartHelper.upgradeWishlistCart()
    }
    
    class func logout(){
        Context.setToken("")
        Context.setUserId(0)
        Context.setUsername("")
        Context.setPassword("")
        Context.setUserKey("0")
        Context.setAuthenticatedUser(false)
        
        CartHelper.cleanShoppingCart()
        CartHelper.cleanWishlistCart()
    }
    
    class func goToStorefront(){
        UIApplication.sharedApplication().delegate!.window!!.rootViewController = StorefrontController()
    }
    
    class func goToLogin(){
        let loginNavController = UINavigationController()
        loginNavController.viewControllers = [LoginViewController()]
        UIApplication.sharedApplication().delegate!.window!!.rootViewController = loginNavController
    }
}