//
//  RegexManager.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 16/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
class RegexManager {
    class func matchesForRegexInText(regex: String!, text: String!) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}