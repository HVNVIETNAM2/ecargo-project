//
//  DataManager.swift
//  merchant-ios
//
//  Created by HungPM on 1/29/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper

class CacheManager {
    class var sharedManager: CacheManager {
        get {
            struct Singleton {
                static let instance = CacheManager()
            }
            return Singleton.instance
        }
    }
    
    //private init
    private init() {}
    
    var wishlist : Wishlist?
    var cart : Cart?
    
    //MARK: Shopping Cart
    func refreshCart() {
        if !Context.getAuthenticatedUser() && (Context.anonymousShoppingCartKey() == nil || Context.anonymousShoppingCartKey() == "0") {
            return
        }
    
        firstly {
            return listCartItem()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    func hasCartItem() -> Bool {
        if self.cart?.merchantList?.count > 0 {
            return true
        }
        return false
    }
    
    func listCartItem() -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            CartService.list(completion: {
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess {
                        if response.response?.statusCode == 200 {
                            let cart = Mapper<Cart>().map(response.result.value)
                            strongSelf.cart = cart
                            fulfill("OK")
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            })
        }
    }
 
    
    //MARK: Wishlist
    func refreshWishlist() {
        if !Context.getAuthenticatedUser() && (Context.anonymousWishlistKey() == nil || Context.anonymousWishlistKey() == "0") {
            return
        }

        firstly {
            return self.listWishlistItem()
            }.error { _ -> Void in
                Log.error("error")
        }
    }
    
    func hasWishlistItem() -> Bool {
        if self.wishlist?.cartItems?.count > 0 {
            return true
        }
        return false
    }
    
    func listWishlistItem(completion complete:(() -> Void)? = nil) -> Promise<AnyObject> {
        return Promise{ fulfill, reject in
            WishlistService.list(completion: {
                [weak self] (response) in
                if let strongSelf = self {
                    if response.result.isSuccess{
                        if response.response?.statusCode == 200 {
                            let wishlist = Mapper<Wishlist>().map(response.result.value)
                            strongSelf.wishlist = wishlist
                            fulfill("OK")
                            if let completeBlock = complete {
                                completeBlock()
                            }
                        }
                        else {
                            var statusCode = 0
                            if let code = response.response?.statusCode {
                                statusCode = code
                            }
                            
                            let error = NSError(domain: "", code: statusCode, userInfo: nil)
                            reject(error)
                        }
                    }
                    else{
                        reject(response.result.error!)
                    }
                }
            })
        }
    }
    
    // MARK: Mapping a style with wishlist item
    func cartItemIdForStyle(style : Style) -> Int {
        if let cartItems = CacheManager.sharedManager.wishlist?.cartItems {
            for cartItem in cartItems {
                if cartItem.styleCode == style.styleCode {
                    return cartItem.cartItemId
                }
            }
        }
        return NSNotFound
    }

}

