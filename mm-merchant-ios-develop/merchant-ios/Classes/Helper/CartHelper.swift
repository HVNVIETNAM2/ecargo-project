//
//  CartHelper.swift
//  merchant-ios
//
//  Created by HungPM on 2/23/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class CartHelper {
    
    class func upgradeShoppingCart() {
        CartService.viewUser(completion: { (response) in
            
            let actionHandler = { (response: Response<AnyObject, NSError>) in
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        
                        let cart = Mapper<Cart>().map(response.result.value)
                        CacheManager.sharedManager.cart = cart
                        
                    }
                    else {
                        listShoppingCart()
                    }
                    
                    // remove the guest cart key
                    Context.setAnonymousShoppingCartKey("0")
                }
            }
            
            var isExistingCartKey = false
            
            if response.result.isSuccess {
                if response.response?.statusCode == 200 {
                    
                    isExistingCartKey = true
                    
                    if let anonymousShoppingCartKey = Context.anonymousShoppingCartKey() where anonymousShoppingCartKey != "0" {
                        
                        CartService.mergeCart(anonymousShoppingCartKey, completion: actionHandler)
            
                    }
                    else {
                        listShoppingCart()
                    }
                }
            }
            
            if (!isExistingCartKey) {
                let apiResponse = Mapper<ApiResponse>().map(response.result.value)
                if let res = apiResponse where res.appCode == "MSG_ERR_CART_NOT_FOUND" {
                    
                    if let anonymousShoppingCartKey = Context.anonymousShoppingCartKey() where anonymousShoppingCartKey != "0" {
                        
                        CartService.userUpdate(anonymousShoppingCartKey, completion: actionHandler)
                        
                    }
                    else {
                        listShoppingCart()
                    }
                }
            }
        })
    }
    
    class func upgradeWishlistCart() {
        WishlistService.listByUser(completion: { (response) in
            
            let actionHandler = { (response: Response<AnyObject, NSError>) in
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        
                        let wishlist = Mapper<Wishlist>().map(response.result.value)
                        CacheManager.sharedManager.wishlist = wishlist
                        
                    }
                    else {
                        listWishlistCart()
                    }
                    
                    // remove the guest wishlist cart key
                    Context.setAnonymousWishlistKey("0")
                }
            }

            var isExistingWishlistKey = false
            
            if response.result.isSuccess {
                if response.response?.statusCode == 200 {
                    
                    isExistingWishlistKey = true
                    
                    if let anonymousWishlistCartKey = Context.anonymousWishlistKey() where anonymousWishlistCartKey != "0" {
                        
                        WishlistService.mergeWishlist(anonymousWishlistCartKey, completion: actionHandler)
                        
                    }
                    else {
                        listWishlistCart()
                    }
                }
            }
            
            if (!isExistingWishlistKey) {
                let apiResponse = Mapper<ApiResponse>().map(response.result.value)
                if let res = apiResponse where res.appCode == "MSG_ERR_WISHLIST_NOT_FOUND" {
                    
                    if let anonymousWishlistCartKey = Context.anonymousWishlistKey() where anonymousWishlistCartKey != "0" {
                        
                        WishlistService.userUpdate(anonymousWishlistCartKey, completion: actionHandler)
                        
                    }
                    else {
                        listWishlistCart()
                    }
                }
            }
            
        })
    }
    
    class func cleanShoppingCart() {
        Context.setAnonymousShoppingCartKey("0")
        CacheManager.sharedManager.cart = nil
    }
    
    class func cleanWishlistCart() {
        Context.setAnonymousWishlistKey("0")
        CacheManager.sharedManager.wishlist = nil
    }
    
    private class func listShoppingCart() {
        CartService.list { (response) -> Void in
            
            if response.result.isSuccess {
                if response.response?.statusCode == 200 {
                    
                    let cart = Mapper<Cart>().map(response.result.value)
                    CacheManager.sharedManager.cart = cart
                    
                }
                
                // remove the guest cart key
                Context.setAnonymousShoppingCartKey("0")
                
            }

        }
    }
    
    private class func listWishlistCart() {
        WishlistService.list { (response) -> Void in
            
            if response.result.isSuccess {
                if response.response?.statusCode == 200 {
                    
                    let wishlist = Mapper<Wishlist>().map(response.result.value)
                    CacheManager.sharedManager.wishlist = wishlist
                    
                }
                
                // remove the guest wishlist cart key
                Context.setAnonymousWishlistKey("0")
                
            }
            
        }
    }
}
