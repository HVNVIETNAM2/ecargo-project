//
//  CollectionViewSectionData.swift
//  merchant-ios
//
//  Created by Alan YU on 6/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

class CollectionViewSectionData {
    
    private(set) var sectionHeader: AnyObject?
    private(set) var sectionFooter: AnyObject?
    var dataSource: [AnyObject]
    private(set) var reuseIdentifier: String
    
    init(sectionHeader: AnyObject?, sectionFooter: AnyObject? = nil, reuseIdentifier: String, dataSource: [AnyObject]) {
        self.sectionHeader = sectionHeader
        self.sectionFooter = sectionFooter
        self.dataSource = dataSource
        self.reuseIdentifier = reuseIdentifier
    }
    
}