//
//  File.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 16/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import ObjectMapper
class Conv : Mappable{
    var convKey = ""
    var timestamp = NSDate()
    var userList = [String]()
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        convKey     <-  map["ConvKey"]
        timestamp   <-  (map["TimeStamp"], DateTransform())
        userList    <-  map["UserList"]
    }
}
    