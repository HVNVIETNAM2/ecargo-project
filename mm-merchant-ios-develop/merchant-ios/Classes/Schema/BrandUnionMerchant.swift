//
//  BrandUnionMerchant.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 31/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectMapper


class BrandUnionMerchant : Mappable {
    var entityId = 0
    var entity = ""
    var name = ""
    var nameInvariant = ""
    var description = ""
    var isListed = false
    var isFeatured = false
    var isRecommended = false
    var headerLogoImage = ""
    var smallLogoImage = ""
    var largeLogoImage = ""
    var profileBannerImage = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        entityId            <- map["EntityId"]
        entity              <- map["Entity"]
        name                <- map["Name"]
        nameInvariant       <- map["NameInvariant"]
        description         <- map["Description"]
        isListed            <- map["IsListed"]
        isFeatured          <- map["IsFeatured"]
        isRecommended       <- map["IsRecommended"]
        headerLogoImage     <- map["HeaderLogoImage"]
        smallLogoImage      <- map["SmallLogoImage"]
        largeLogoImage      <- map["LargeLogoImage"]
        profileBannerImage  <- map["ProfileBannerImage"]
    }

    private(set) var imageCategory: ImageCategory {
        get {
            var category: ImageCategory  = .Merchant
            if entity == "Brand" {
                category = .Brand
            }
            return category
        }
        set {
            
        }
    }
    
}
