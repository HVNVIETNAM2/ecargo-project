//
//  CartItem.swift
//  merchant-ios
//
//  Created by Alan YU on 10/1/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import ObjectMapper

class CartItem: NSObject, Mappable {
    
    var skuId = 0
    var cartItemId = 0
    var qty = 0
    var styleCode = ""
    var skuCode = ""
    var bar = ""
    var brandId = 0
    var brandName = ""
    var brandNameInvariant = ""
    var brandImage = ""
    var badgeId = 0
    var seasonId = 0
    var sizeId = 0
    var colorId = 0
    var geoCountryId = 0
    var launchYear = 0
    var priceRetail = 0
    var priceSale = 0
    var saleFrom = ""
    var saleTo = ""
    var availableFrom = ""
    var availableTo = ""
    var qtySafetyThreshold = 0
    var weightKg = 0
    var heightCm = 0
    var widthCm = 0
    var lengthCm = 0
    var merchantId = 0
    var statusId = 0
    var primaryCategoryId = 0
    var sizeName = ""
    var colorKey = ""
    var colorName = ""
    var skuColor = ""
    var skuName = ""
    var locationCount = 0
    var qtyAts = ""
    var inventoryStatusId = 0
    var productImage = ""
    var lastCreated = ""
    var lastModified = ""
    var style : Style?
    
    // custom
    dynamic var selected = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        skuId                           <- map["SkuId"]
        cartItemId                      <- map["CartItemId"]
        qty                             <- map["Qty"]
        styleCode                       <- map["StyleCode"]
        skuCode                         <- map["SkuCode"]
        bar                             <- map["Bar"]
        brandId                         <- map["BrandId"]
        brandName                       <- map["BrandName"]
        brandNameInvariant              <- map["BrandNameInvariant"]
        brandImage                      <- map["BrandImage"]
        badgeId                         <- map["BadgeId"]
        seasonId                        <- map["SeasonId"]
        sizeId                          <- map["SizeId"]
        colorId                         <- map["ColorId"]
        geoCountryId                    <- map["GeoCountryId"]
        launchYear                      <- map["LaunchYear"]
        priceRetail                     <- map["PriceRetail"]
        priceSale                       <- map["PriceSale"]
        saleFrom                        <- map["SaleFrom"]
        saleTo                          <- map["SaleTo"]
        availableFrom                   <- map["AvailableFrom"]
        availableTo                     <- map["AvailableTo"]
        qtySafetyThreshold              <- map["QtySafetyThreshold"]
        weightKg                        <- map["WeightKg"]
        heightCm                        <- map["HeightCm"]
        widthCm                         <- map["WidthCm"]
        lengthCm                        <- map["LengthCm"]
        merchantId                      <- map["MerchantId"]
        statusId                        <- map["StatusId"]
        primaryCategoryId               <- map["PrimaryCategoryId"]
        sizeName                        <- map["SizeName"]
        colorKey                        <- map["ColorKey"]
        colorName                       <- map["ColorName"]
        skuColor                        <- map["SkuColor"]
        skuName                         <- map["SkuName"]
        locationCount                   <- map["LocationCount"]
        qtyAts                          <- map["QtyAts"]
        inventoryStatusId               <- map["InventoryStatusId"]
        productImage                    <- map["ProductImage"]
        lastCreated                     <- map["LastCreated"]
        lastModified                    <- map["LastModified"]
        style                           <- map["Style"]
    }

    func price() -> Int {
        if priceSale != 0 {
            return priceSale
        } else {
            return priceRetail
        }
    }
    
    //FIXME: update when get response
    func isProductAvailabel() -> Bool {
        return true
//        if Int(qtyAts) >= qtySafetyThreshold || productStatus {
//            return true
//        }
//        else {
//            return false
//        }
    }
    
    func isProductValid() -> Bool {
        return true
    }
}