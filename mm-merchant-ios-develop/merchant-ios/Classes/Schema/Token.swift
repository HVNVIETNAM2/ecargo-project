//
//  Token.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Token: Object, Mappable {
    
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }
    dynamic var token = ""
    dynamic var userId = 0
    dynamic var userKey = ""
    dynamic var isSignup = false

    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        token       <- map["Token"]
        userId      <- map["UserId"]
        userKey     <- map["UserKey"]
        isSignup    <- map["IsSignup"]
    }
    
    override static func primaryKey() -> String? {
        return "token"
    }
}