//
//  InventoryLocation.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 30/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class InventoryLocation : Object, Mappable{
    
    dynamic var inventoryLocationId = 0
    dynamic var merchantId = 0
    dynamic var locationExternalCode = ""
    dynamic var geoCountryId = 0
    dynamic var geoIdProvince = 0
    dynamic var geoIdCity = 0
    dynamic var inventoryLocationTypeName = ""
    dynamic var geoCountryName = ""
    dynamic var geoProvinceName = ""
    dynamic var geoCityName = ""
    dynamic var locationName = ""
    dynamic var district = ""
    
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        inventoryLocationId         <- map["InventoryLocationId"]
        merchantId                  <- map["MerchantId"]
        locationExternalCode        <- map["LocationExternalCode"]
        geoCountryId                <- map["GeoCountryId"]
        geoIdProvince               <- map["GeoIdProvince"]
        geoIdCity                   <- map["GeoIdCity"]
        inventoryLocationTypeName   <- map["InventoryLocationTypeName"]
        geoCountryName              <- map["GeoCountryName"]
        geoProvinceName             <- map["GeoProvinceName"]
        geoCityName                 <- map["GeoCityName"]
        locationName                <- map["LocationName"]
        district                    <- map["District"]
    }
}