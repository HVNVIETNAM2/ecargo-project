//
//  Order.swift
//  merchant-ios
//
//  Created by HungPM on 3/11/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import ObjectMapper

class Order : Mappable {
    var orderKey = ""
    var isCrossBorder = false
    var isUserIdentificationExists = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        orderKey                        <- map["OrderKey"]
        isCrossBorder                   <- map["IsCrossBorder"]
        isUserIdentificationExists      <- map["IsUserIdentificationExists"]
    }
}

