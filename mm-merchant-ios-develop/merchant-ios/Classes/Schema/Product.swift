//
//  Product.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 22/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import RealmSwift
import ObjectMapper

class Product: Object, Mappable {
    
    dynamic var color = [""]
    dynamic var size = [0]
    dynamic var price = 0
    dynamic var originalPrice = 0
    dynamic var descript = ""
    dynamic var id = ""
    dynamic var name = ""
    dynamic var brand = ""
    dynamic var coverPhoto = ""
    dynamic var categoryId = 0
    var like = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        color                   <- map["color"]
        price                   <- map["price"]
        originalPrice           <- map["originalPrice"]
        descript                <- map["description"]
        id                      <- map["objectId"]
        name                    <- map["name"]
        brand                   <- map["brand"]
        size                    <- map["size"]
        coverPhoto              <- map["coverPhoto"]
        categoryId              <- map["categoryId"]
    }
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["color", "size", "like"]
    }
}
