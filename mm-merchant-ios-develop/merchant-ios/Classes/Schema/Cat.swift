//
//  Cat.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 18/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectMapper

class Cat : Mappable{
    var categoryId = 0
    var parentCategoryId = 0
    var defaultCommissionRate = 0.0
    var categoryName = ""
    var categoryList : [Cat]?
    var level = 0
    var categoryImage = ""
    var categoryBrandMerchantList : [BrandUnionMerchant] = []
    var isSelected = false
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        categoryId                  <- map["CategoryId"]
        parentCategoryId            <- map["ParentCategoryId"]
        defaultCommissionRate       <- map["DefaultCommissionRate"]
        categoryName                <- map["CategoryName"]
        categoryList                <- map["CategoryList"]
        level                       <- map["Level"]
        categoryImage               <- map["CategoryImage"]
        categoryBrandMerchantList   <- map["CategoryBrandMerchantList"]
    }

}