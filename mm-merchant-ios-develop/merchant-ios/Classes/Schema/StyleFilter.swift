//
//  StyleFilter.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 15/12/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
class StyleFilter{
    var queryString : String = ""
    var pricefrom : Int = 0
    var priceto : Int = 10000
    var badges : [Badge] = []
    var brands : [Brand] = []
    var cats : [Cat] = []
    var colors : [Color] = []
    var sizes : [Size] = []
    var merchants : [Merchant] = []
    var sort : String = ""
    var order : String = ""
    var isnew : Int = -1
    var issale : Int = -1
    
    func count() -> Int {
        return badges.count + brands.count + cats.count + colors.count + sizes.count + merchants.count + priceCount() + newCount() + saleCount()
    }
    
    func priceCount() -> Int {
        if pricefrom != 0 || priceto != 10000 {
            return 1
        }
        return 0
    }
    
    func newCount() -> Int {
        if isnew == 1 {
            return 1
        }
        return 0
    }
    
    func saleCount() -> Int {
        if issale == 1 {
            return 1
        }
        return 0
    }
    
    func reset(){
        queryString = ""
        pricefrom = 0
        priceto = 10000
        badges = []
        brands = []
        cats = []
        colors = []
        sizes = []
        merchants = []
        sort = ""
        order = ""
        isnew = -1
        issale = -1
    }
}