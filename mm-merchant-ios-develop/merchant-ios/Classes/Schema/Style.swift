//
//  Style.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 27/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectMapper

class Style : Mappable{
    var styleCode = ""
    var merchantId = 0
    var imageDefault = ""
    var priceRetail = 0
    var priceSale = 0
    var saleFrom = NSDate()
    var saleTo = NSDate()
    var lastCreated = ""
    var availableFrom = NSDate()
    var availableTo = NSDate()
    var manufacturerName = ""
    var launchYear = ""
    var brandId = 0
    var brandName = ""
    var brandNameInvariant = ""
    var brandHeaderLogoImage = ""
    var brandSmallLogoImage = ""
    var skuName = ""
    var skuNameInvariant = ""
    var skuDesc = ""
    var skuDescInvariant = ""
    var skuFeature = ""
    var skuFeatureInvariant = ""
    var primaryCategoryId = 0
    var statusId = 0
    var statusName = ""
    var statusNameInvariant = ""
    var seasonId = 0
    var seasonName = ""
    var seasonNameInvariant = ""
    var badgeId = 0
    var badgeName = ""
    var badgeNameInvariant = ""
    var geoCountryId = 0
    var geoCountryName = ""
    var geoCountryNameInvariant = ""
    var primarySkuId = 0
    var isNew = 0
    var isSale = 0
    var primaryCategoryPathList : [Cat] = []
    var sizeList : [Size] = []
    var colorList : [Color] = []
    var featuredImageList : [Img] = []
    var descriptionImageList : [Img] = []
    var colorImageList : [Img] = []
    var skuList : [Sku] = []
    var totalLocationCount = 0
    var categoryPriorityList : [Category]?
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        styleCode                   <- map["StyleCode"]
        merchantId                  <- map["MerchantId"]
        imageDefault                <- map["ImageDefault"]
        priceRetail                 <- map["PriceRetail"]
        priceSale                   <- map["PriceSale"]
        saleFrom                    <- map["SaleFrom"]
        saleTo                      <- map["SaleTo"]
        availableFrom               <- map["AvailableFrom"]
        availableTo                 <- map["AvailableTo"]
        brandId                     <- map["BrandId"]
        brandName                   <- map["BrandName"]
        brandNameInvariant          <- map["BrandNameInvariant"]
        brandHeaderLogoImage        <- map["BrandHeaderLogoImage"]
        brandSmallLogoImage         <- map["BrandSmallLogoImage"]
        skuName                     <- map["SkuName"]
        skuNameInvariant            <- map["SkuNameInvariant"]
        primaryCategoryId           <- map["PrimaryCategoryId"]
        statusId                    <- map["StatusId"]
        statusName                  <- map["StatusName"]
        statusNameInvariant         <- map["StatusNameInvariant"]
        primaryCategoryPathList     <- map["PrimaryCategoryPathList"]
        sizeList                    <- map["SizeList"]
        colorList                   <- map["ColorList"]
        featuredImageList           <- map["FeaturedImageList"]
        descriptionImageList        <- map["DescriptionImageList"]
        colorImageList              <- map["ColorImageList"]
        skuDesc                     <- map["SkuDesc"]
        skuList                     <- map["SkuList"]
        lastCreated                 <- map["LastCreated"]
        manufacturerName            <- map["ManufacturerName"]
        launchYear                  <- map["LaunchYear"]
        skuDescInvariant            <- map["SkuDescInvariant"]
        skuFeature                  <- map["SkuFeature"]
        skuFeatureInvariant         <- map["SkuFeatureInvariant"]
        seasonId                    <- map["SeasonId"]
        seasonName                  <- map["SeasonName"]
        seasonNameInvariant         <- map["SeasonNameInvariant"]
        badgeId                     <- map["BadgeId"]
        badgeName                   <- map["BadgeName"]
        badgeNameInvariant          <- map["BadgeNameInvariant"]
        geoCountryId                <- map["GeoCountryId"]
        geoCountryName              <- map["GeoCountryName"]
        geoCountryNameInvariant     <- map["geoCountryNameInvariant"]
        primarySkuId                <- map["PrimarySkuId"]
        isNew                       <- map["IsNew"]
        isSale                      <- map["IsSale"]
        totalLocationCount          <- map["totalLocationCount"]
        categoryPriorityList        <- map["CategoryPriorityList"]
    }
    
    func searchSku(sizeId: Int, colorId: Int) -> Sku? {
        var sku: Sku?
        for skuObj in self.skuList {
            if (skuObj.sizeId == sizeId || sizeId == -1) && (skuObj.colorId == colorId || colorId == -1) {
                sku = skuObj
                break
            }
        }
        return sku
    }
    
    func isWished() -> Bool {
        if let cartItems = CacheManager.sharedManager.wishlist?.cartItems {
            for cartItem in cartItems {
                if cartItem.styleCode == self.styleCode {
                    return true
                }
            }
        }
        return false
    }
    
    func lowestPriceSku() -> Sku? {
        
        if skuList.count > 0 {
            
            var target: Sku = skuList.first!
            var lowestPrice = target.priceRetail
            
            for sku in skuList {
                if sku.priceRetail > 0 && sku.priceRetail < lowestPrice {
                    lowestPrice = sku.priceRetail
                    target = sku
                }
                
                if sku.priceSale > 0 && sku.priceSale < lowestPrice {
                    lowestPrice = sku.priceSale
                    target = sku
                }
            }
            
            return target
            
        }
        
        return nil
    }
    
    func findSkuBySkuId(skuId: Int) -> Sku? {
        for sku in skuList {
            if sku.skuId == skuId {
                return sku
            }
        }
        return nil
    }
    
    func findImageKeyByColorKey(colorKey: String) -> String? {
        
        for color in colorImageList {
            if color.colorKey.lowercaseString == colorKey.lowercaseString {
                return color.imageKey
            }
        }
        
        if let featureImage = featuredImageList.first{
            return featureImage.imageKey
        }
        
        return ""
    }
}
