//
//  User.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 24/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import RealmSwift
import ObjectMapper

class User: Object, Mappable {
    
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }
    dynamic var displayName = ""
    dynamic var email = ""
    dynamic var firstName = ""
    dynamic var lastLogin = NSDate()
    dynamic var lastModified = NSDate()
    dynamic var lastName = ""
    dynamic var middleName = ""
    dynamic var mobileCode = ""
    dynamic var mobileNumber = ""
    dynamic var profileImage = ""
    dynamic var statusID = 0
    dynamic var userId = 0
    dynamic var languageId = 1
    dynamic var timeZoneId = 1
    dynamic var userTypeId = 1
    dynamic var inventoryLocationId = 0
    dynamic var merchantId = 0
    dynamic var userInventoryLocationArray = [0]
    dynamic var userSecurityGroupArray = [0]
    dynamic var password = ""
    dynamic var passwordOld = ""
    var merchant = Merchant()
    dynamic var userKey = ""
    dynamic var userName = ""
    dynamic var count = 0
    dynamic var coverImage = ""
    dynamic var IsCuratorUser = 0
    var isSelected = false
    var isClicking = false
    dynamic var followerCount = 0
    dynamic var friendCount = 0
    dynamic var followingUserCount = 0
    dynamic var followingMerchantCount = 0
    dynamic var followingBrandCount = 0
    dynamic var followingCuratorCount = 0
    dynamic var friendStatus = ""
    dynamic var followStatus = ""
    dynamic var isFriendUser = false
    dynamic var isFollowUser = false
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        displayName                 <- map["DisplayName"]
        email                       <- map["Email"]
        firstName                   <- map["FirstName"]
        lastLogin                   <- (map["LastLogin"], DateTransform())
        lastModified                <- (map["LastModified"], DateTransform())
        lastName                    <- map["LastName"]
        middleName                  <- map["MiddleName"]
        mobileCode                  <- map["MobileCode"]
        mobileNumber                <- map["MobileNumber"]
        profileImage                <- map["ProfileImage"]
        statusID                    <- map["StatusId"]
        userId                      <- map["UserId"]
        languageId                  <- map["LanguageId"]
        timeZoneId                  <- map["TimeZoneId"]
        userTypeId                  <- map["UserTypeId"]
        inventoryLocationId         <- map["InventoryLocationId"]
        merchantId                  <- map["MerchantId"]
        userInventoryLocationArray  <- map["UserInventoryLocationArray"]
        userSecurityGroupArray      <- map["UserSecurityGroupArray"]
        password                    <- map["Password"]
        passwordOld                 <- map["PasswordOld"]
        merchant                    <- map["Merchant"]
        userKey                     <- map["UserKey"]
        userName                    <- map["UserName"]
        count                       <- map["Count"]
        coverImage                  <- map["CoverImage"]
        IsCuratorUser               <- map["IsCuratorUser"]
        followerCount               <- map["FollowerCount"]
        friendCount                 <- map["FriendCount"]
        followingUserCount          <- map["FollowingUserCount"]
        followingMerchantCount      <- map["FollowingMerchantCount"]
        followingBrandCount         <- map["FollowingBrandCount"]
        followingCuratorCount       <- map["FollowingCuratorCount"]
    }
    
    override static func primaryKey() -> String? {
        return "userId"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["userInventoryLocationArray", "userSecurityGroupArray", "password", "passwordOld", "merchant"]
    }

}
