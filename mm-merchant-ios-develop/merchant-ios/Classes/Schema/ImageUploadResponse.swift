//
//  ImageUploadResponse.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 28/10/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectMapper

class ImageUploadResponse : Mappable{
    
    dynamic var imageKey = ""
    dynamic var imageMimeType = ""
    dynamic var profileImage = ""
    dynamic var coverImage = ""
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        imageKey            <- map["ImageKey"]
        imageMimeType       <- map["ImageMimeType"]
        profileImage        <- map["ProfileImage"]
        coverImage          <- map["CoverImage"]
    }

}
