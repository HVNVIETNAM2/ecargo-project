//
//  Brand.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 23/11/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectMapper

class Brand : Mappable{
    var brandId = 0
    var brandName = ""
    var brandNameInvariant = ""
    var headerLogoImage = ""

    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        brandId             <- map["BrandId"]
        brandName           <- map["BrandName"]
        brandNameInvariant  <- map["BrandNameInvariant"]
        headerLogoImage     <- map["HeaderLogoImage"]
    }
    
}