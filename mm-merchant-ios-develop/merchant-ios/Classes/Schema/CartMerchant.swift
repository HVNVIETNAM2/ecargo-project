//
//  CartMerchant.swift
//  merchant-ios
//
//  Created by HungPM on 1/15/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import ObjectMapper

class CartMerchant : Mappable {
    
    var merchantId = 0
    var merchantName = ""
    var merchantNameInvariant = ""
    var merchantImage = ""
    var itemList : [CartItem]?
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        merchantId                  <- map["MerchantId"]
        merchantName                <- map["MerchantName"]
        merchantNameInvariant       <- map["MerchantNameInvariant"]
        merchantImage               <- map["MerchantImage"]
        itemList                    <- map["ItemList"]
    }
}
