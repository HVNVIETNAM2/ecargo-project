//
//  Merchant.swift
//  merchant-ios
//
//  Created by Hang Yuen on 9/11/2015.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import Foundation
import ObjectMapper

class Merchant : Mappable {
    
    var merchantId = 0
    var merchantTypeId = 0
    var merchantName = ""
    var merchantDisplayName = ""
    var merchantCompanyName = ""
    var merchantNameInvariant = ""
    var businessRegistrationNo = ""     // API return String?
    var merchantSubdomain = ""
    var merchantDesc = ""
    var logoImage = ""
    var backgroundImage = ""
    var geoCountryId = 0
    var geoIdProvince = 0
    var geoIdCity = 0
    var district = ""
    var postalCode = ""
    var apartment = ""
    var floor = ""
    var blockNo = ""
    var building = ""
    var streetNo = ""
    var street = ""
    var statusId = 0
    var lastStatus = NSDate()
    var lastModified = NSDate()
    var merchantTypeName = ""
    var statusNameInvariant = ""
    var geoCountryName = ""
    var headerLogoImage = ""
    var smallLogoImage = ""
    var largeLogoImage = ""
    var profileBannerImage = ""
    var isListedMerchant = false
    var isFeaturedMerchant = false
    var isRecommendedMerchant = false
    var isSearchableMerchant = false
    var isSelected = false
    var count = 0
    var isClicking = false
    var followerCount = 0
    var followStatus = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        merchantId              <- map["MerchantId"]
        merchantTypeId          <- map["MerchantTypeId"]
        merchantDisplayName     <- map["MerchantDisplayName"]
        merchantCompanyName     <- map["MerchantCompanyName"]
        businessRegistrationNo  <- map["BusinessRegistrationNo"]
        merchantSubdomain       <- map["MerchantSubdomain"]
        merchantDesc            <- map["MerchantDesc"]
        logoImage               <- map["LogoImage"]
        backgroundImage         <- map["BackgroundImage"]
        geoCountryId            <- map["GeoCountryId"]
        geoIdProvince           <- map["GeoIdProvince"]
        geoIdCity               <- map["GeoIdCity"]
        district                <- map["District"]
        postalCode              <- map["PostalCode"]
        apartment               <- map["Apartment"]
        floor                   <- map["Floor"]
        blockNo                 <- map["BlockNo"]
        building                <- map["Building"]
        streetNo                <- map["StreetNo"]
        street                  <- map["Street"]
        statusId                <- map["StatusId"]
        lastStatus              <- (map["LastStatus"], DateTransform())
        lastModified            <- (map["LastModified"], DateTransform())
        merchantTypeName        <- map["MerchantTypeName"]
        statusNameInvariant     <- map["StatusNameInvariant"]
        geoCountryName          <- map["GeoCountryName"]
        headerLogoImage         <- map["HeaderLogoImage"]
        smallLogoImage          <- map["SmallLogoImage"]
        largeLogoImage          <- map["LargeLogoImage"]
        profileBannerImage      <- map["ProfileBannerImage"]
        isListedMerchant        <- map["IsListedMerchant"]
        isFeaturedMerchant      <- map["IsFeaturedMerchant"]
        isRecommendedMerchant   <- map["IsRecommendedMerchant"]
        isSearchableMerchant    <- map["IsSearchableMerchant"]
        merchantNameInvariant   <- map["MerchantNameInvariant"]
        merchantName            <- map["MerchantName"]
        count                   <- map["Count"]
        followerCount           <- map["FollowerCount"]
    }
    
}