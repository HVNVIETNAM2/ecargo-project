//
//  Category.swift
//  merchant-ios
//
//  Created by HungPM on 1/27/16.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import ObjectMapper

class Category : Mappable{
    var categoryId = 0
    var categoryCode = ""
    var parentCategoryId = 0
    var isMerchCanSelect = 0
    var priority = 0
    var categoryName = ""
    var categoryNameInvariant = ""
    var categoryImage = ""
    var skuCount = 0
    var statusId = 0
    var level = 0

    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        categoryId                  <- map["CategoryId"]
        categoryCode                <- map["CategoryCode"]
        parentCategoryId            <- map["ParentCategoryId"]
        isMerchCanSelect            <- map["IsMerchCanSelect"]
        priority                    <- map["Priority"]
        categoryName                <- map["CategoryName"]
        categoryNameInvariant       <- map["CategoryNameInvariant"]
        categoryImage               <- map["CategoryImage"]
        skuCount                    <- map["SkuCount"]
        statusId                    <- map["StatusId"]
        level                       <- map["Level"]

    }
}
