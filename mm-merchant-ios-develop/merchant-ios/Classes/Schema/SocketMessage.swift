//
//  ChatMessage.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 4/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation
import ObjectMapper

class SocketMessage : Mappable{
    var type = ""
    var userKey = ""
    var userList = [String]()
    var convList = [Conv]()
    var convKey = ""
    var body = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        type        <-  map["Type"]
        userKey     <-  map["UserKey"]
        userList    <-  map["UserList"]
        convList    <-  map["ConvList"]
        convKey     <-  map["ConvKey"]
        body        <-  map["Body"]
    }
}