//
//  AppDelegate.swift
//  merchant-ios
//
//  Created by Koon Kit Chan on 17/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import UIKit
import CoreData
import Realm
import RealmSwift
import Parse
import Fabric
import Crashlytics
import ChameleonFramework
import KSToastView
import netfox
import ObjectMapper

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WXApiDelegate {

    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
    #if !MERCHANTAPP
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
    #endif
            
        configRealm()
        configParsePush(application, launchOptions: launchOptions)
        configUMeng()
        configCrashlytics()
        configTheme()
        configWeChat()
        
        KSToastView.ks_setAppearanceMaxLines(3)
        KSToastView.ks_setAppearanceTextFont(UIFont.systemFontOfSize(14))
        NFX.sharedInstance().start()
        
        WebSocketManager.sharedInstance.connect()
        
        // Entry point depends on the login status
    #if MERCHANTAPP
        
        if (Context.getAuthenticatedUser()){
            let mainNavViewController = UIStoryboard(name: "User", bundle: nil).instantiateViewControllerWithIdentifier("UserNavigation")
            self.window?.rootViewController = mainNavViewController
        } else {
            let loginNavViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewControllerWithIdentifier("LoginNavigation")
            self.window?.rootViewController = loginNavViewController
        }
        
    #else
        
        if (Context.getAuthenticatedUser()){
            LoginManager.goToStorefront()

        } else {
            LoginManager.goToLogin()
        }
        
         window?.makeKeyAndVisible()
        
    #endif
        
        return true
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        Context.setDeviceToken(installation.deviceToken!)
        Log.debug(Context.getDeviceToken())
        installation.saveInBackground()
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        if error.code == 3010 {
            Log.debug("Push notifications are not supported in the iOS Simulator.")
        } else {
            Log.debug("application:didFailToRegisterForRemoteNotificationsWithError: \(error)")
        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        PFPush.handlePush(userInfo)
        if application.applicationState == UIApplicationState.Inactive {
            PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
        }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        CacheManager.sharedManager.refreshCart()
        CacheManager.sharedManager.refreshWishlist()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "mm.merchant_ios" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("merchant_ios", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            Log.debug("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                Log.debug("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    func configRealm(){
        // Override point for customization after application launch.
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 3,
            inMemoryIdentifier : "mm",
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 3) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
                
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
    }
    
    func configParsePush(application : UIApplication, launchOptions: [NSObject: AnyObject]?){
        Parse.setApplicationId("tSF6abJ4ZwUM6pjpVhDF4poWYmxbPQQg7DbYh8GS",
            clientKey: "qO75K5Zk7jk3sEYxr0i5hX78Ri35Fh4LZf2MxpsN")
        
        // Register for Push Notitications
        if application.applicationState != UIApplicationState.Background {
            // Track an app open here if we launch with a push, unless
            // "content_available" was used to trigger a background push (introduced in iOS 7).
            // In that case, we skip tracking here to avoid double counting the app-open.
            
            let preBackgroundPush = !application.respondsToSelector("backgroundRefreshStatus")
            let oldPushHandlerOnly = !self.respondsToSelector("application:didReceiveRemoteNotification:fetchCompletionHandler:")
            var pushPayload = false
            if let options = launchOptions {
                pushPayload = options[UIApplicationLaunchOptionsRemoteNotificationKey] != nil
            }
            if (preBackgroundPush || oldPushHandlerOnly || pushPayload) {
                PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
            }
        }
        if application.respondsToSelector("registerUserNotificationSettings:") {
            
            let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }

    }
    
    func configUMeng(){
        // Register for UMeng Analytics
        MobClick.startWithAppkey("5614cdda67e58e446900279f", reportPolicy: REALTIME, channelId: "Debug")
    }
    
    func configCrashlytics(){
        // Register for Fabrics and Crashlytics
        Fabric.with([Crashlytics.self()])
    }
    
    func configTheme(){
        
        self.window?.tintColor = UIColor.primary1()
        self.window?.backgroundColor = UIColor.whiteColor()
        
        // Sets the default color of the background of the UITabBar
        UITabBar.appearance().barTintColor = UIColor.whiteColor()
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        if let _ = UIFont(name: Constants.Font.Normal, size: Constants.Font.Size) {//#available(iOS 9, *) {
            UILabel.appearance().substituteAllFontName = Constants.Font.Normal
            UILabel.appearance().substituteAllFontNameBold = Constants.Font.Bold
        } else {
            UILabel.appearance().substituteAllFontName = Constants.iOS8Font.Normal
            UILabel.appearance().substituteAllFontNameBold = Constants.iOS8Font.Bold
        }
        
    }
    
    func configWeChat(){
        WXApi.registerApp("wx5f856e676f618a69")
    }
    
    func application(application: UIApplication, handleOpenURL url: NSURL) -> Bool {
        return WXApi.handleOpenURL(url, delegate: self)
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        if (url.host == "safepay") {
            //跳转支付宝钱包进行支付，处理支付结果
//            AlipaySDK.defaultService().processOrderWithPaymentResult(url, standbyCallback: nil)
            AlipaySDK.defaultService().processOrderWithPaymentResult(url, standbyCallback: { (resultDic) -> Void in
                Log.debug("Finish payment")
                Log.debug(resultDic)
            })
        }
        return WXApi.handleOpenURL(url, delegate: self)
    }
    
    //MARK: WXApiDelegate method
    func onResp(resp: BaseResp!) {
        Log.debug("on Resp from WeChat")
        Log.debug("Err Code from WeChat : \(resp.errCode)")
        Log.debug("Err String from WeChat : \(resp.errStr)")
        let sendAuthResp = resp as! SendAuthResp
        Log.debug("Auth code from WeChat : \(sendAuthResp.code)")
        Log.debug("State from WeChat : \(sendAuthResp.state)")
        Log.debug("Lang from WeChat : \(sendAuthResp.lang)")
        Log.debug("Country from WeChat : \(sendAuthResp.country)")
        NSNotificationCenter.defaultCenter().postNotificationName("LoginWeChat", object: nil, userInfo: ["Code" : sendAuthResp.code])
    }

    func onReq(req: BaseReq!) {
        Log.debug("on Request from WeChat")
        
    }

}

