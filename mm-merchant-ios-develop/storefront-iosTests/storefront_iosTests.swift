//
//  storefront_iosTests.swift
//  storefront-iosTests
//
//  Created by Markus Chow on 17/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import XCTest
@testable import storefront_ios

class storefront_iosTests: XCTestCase {
	
	var storefrontController = StorefrontController()
	
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
		
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
        
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
	
	func testStorefrontControllerShouldHaveFiveTabItems() {
		XCTAssertTrue(storefrontController.viewControllers!.count == 5)
	}
	
	func testGuestLogonButtonShouldShownWhenAppLaunchWithoutLogon() {
	
		// Make sure its not logged on
		LoginManager.logout()
		
		if var topController = UIApplication.topViewController() {
			if String(topController.classForCoder) == "LoginViewController" {
				if let loginView = topController as? LoginViewController {

					let btn = loginView.loginButtonView.lowerButton
					XCTAssertTrue(btn.titleLabel!.text == String.localize("LB_CA_GUEST_LOGIN"), "Guest logon button should be showing on Login page if user not logged in")
					
					btn.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
					
					topController = UIApplication.topViewController()!
					
					XCTAssertTrue(String(topController.classForCoder) == "HomeViewController", "Guest mode should show HomeViewController when first pressed")
					
//					topController = UIApplication.topViewController()!					
//					let vc = storefrontController.viewControllers
//					storefrontController.tabBarController(storefrontController, shouldSelectViewController: vc!.last!)
//					XCTAssertTrue(String(topController.classForCoder) == "ProfileViewController", "Should show top on the MyProfile tab")
					
				}
			}
		}

	}
}
