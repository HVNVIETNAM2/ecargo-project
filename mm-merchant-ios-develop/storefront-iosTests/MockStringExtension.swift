//
//  MockStringExtension.swift
//  merchant-ios
//
//  Created by Markus Chow on 3/3/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import Foundation

extension String {
	
	static func localize(key : String, byClass: AnyClass) -> String{
		let bundle: NSBundle = NSBundle(forClass: byClass)
		return bundle.localizedStringForKey(key, value: nil, table: Context.getCc().lowercaseString)
	}
	
}
