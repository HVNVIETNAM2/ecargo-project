//
//  storefront_iosUITests.swift
//  storefront-iosUITests
//
//  Created by Markus Chow on 17/2/2016.
//  Copyright © 2016 WWE & CO. All rights reserved.
//

import XCTest

class storefront_iosUITests: XCTestCase {

	let app = XCUIApplication()

	var loginStr = String()
	var guestLogonStr = String()
	var newsFeedStr = String()

    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
		
		loginStr = String.localize("LB_CA_LOGIN", byClass: self.classForCoder)
		guestLogonStr = String.localize("LB_CA_GUEST_LOGIN", byClass: self.classForCoder)
		newsFeedStr = String.localize("LB_CA_NEWSFEED", byClass: self.classForCoder)

    }
	
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
	
//	func testGuestloginButtonShouldResponseToTap() {
//
//		app.buttons[guestLogonStr].tap()
//		
//		XCTAssert(app.navigationBars[newsFeedStr].staticTexts[newsFeedStr].exists)
//	}
	
//	func testLoginViewButtonsShouldHaveCorrectPadding() {
//		
//		let upperBtnY: CGFloat = app.buttons[loginStr].frame.origin.y
//		let lowerBtnY: CGFloat = app.buttons[guestLogonStr].frame.origin.y
//		let buttonHeight: CGFloat = app.buttons[guestLogonStr].frame.size.height
//		
//		print("upperBtnY : \(upperBtnY)")
//		print("lowerBtnY : \(lowerBtnY)")
//		
//		XCTAssertTrue(((lowerBtnY - buttonHeight) - upperBtnY ) == 14)
//	}

}
