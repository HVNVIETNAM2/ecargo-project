//
//  merchant_iosTests.swift
//  merchant-iosTests
//
//  Created by Koon Kit Chan on 17/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import XCTest
@testable import merchant_ios

class merchant_iosTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUserService() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        AuthService.request(AuthService.Op.Email, parameters: ["Username" : "adallow@hotmail.com", "Password" : "Bart",]){ response in
            XCTAssert(response.response?.statusCode == 200)
        }
    }
    
    func testPerformanceUserService() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
            AuthService.request(AuthService.Op.Email, parameters: ["Username" : "adallow@hotmail.com", "Password" : "Bart",]){ response in
                XCTAssert(response.response?.statusCode == 200)
                
            }
        }
    }
    
}
