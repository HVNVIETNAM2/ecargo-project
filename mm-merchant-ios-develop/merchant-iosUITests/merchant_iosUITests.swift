//
//  merchant_iosUITests.swift
//  merchant-iosUITests
//
//  Created by Koon Kit Chan on 17/9/15.
//  Copyright © 2015 Koon Kit Chan. All rights reserved.
//

import XCTest

class merchant_iosUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testViewUserList() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCUIDevice.sharedDevice().orientation = .Portrait
        XCUIDevice.sharedDevice().orientation = .Portrait
        
        let app = XCUIApplication()
        let userNameTextField = app.textFields["User Name"]
        userNameTextField.tap()
        userNameTextField.typeText("adallow@hotmail.com")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        app.buttons["shift"].tap()
        passwordSecureTextField.typeText("Bart")
        app.buttons["Login"].tap()
        app.buttons["Product Table View"].tap()
        
        let tablesQuery = app.tables
        XCTAssertEqual(tablesQuery.count, 1)


        


        
    }
    
}
