//
//  AppDelegate.swift
//  AppTranslationTools
//
//  Created by Hang Yuen on 2/11/2015.
//  Copyright © 2015 eCargo. All rights reserved.
//

import Cocoa
import Alamofire

let K_API_DOMAIN: String = "http://platform-mm.eastasia.cloudapp.azure.com/"
let K_API_PATH: String = "api/reference/translation"


@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!
    
    @IBOutlet weak var ivEnTick: NSImageView!
    @IBOutlet weak var ivCHTTick: NSImageView!
    @IBOutlet weak var ivCHSTick: NSImageView!
    
    @IBOutlet weak var lblProcessing: NSTextField!
    
    var en : [String:AnyObject] = [:]
    var tc : [String:AnyObject] = [:]
    var sc : [String:AnyObject] = [:]

///reference/translation?cc=CHS
///reference/translation?cc=EN
///reference/translation?cc=CHT
    
    func exitAlertHelper(message: String) {
        let myAlert:NSAlert = NSAlert()
        myAlert.messageText = message
        if myAlert.runModal() == 0 {
            NSApplication.sharedApplication().terminate(self)
        }
    }
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
        var twinePath = "/usr/bin/twine"
        if NSFileManager.defaultManager().fileExistsAtPath(twinePath) == false {
            twinePath = "/usr/local/bin/twine"
            if NSFileManager.defaultManager().fileExistsAtPath(twinePath) == false {
                self.exitAlertHelper("Command not found. Please install twine by \n\ngem install twine \nor\ngem install -n /usr/local/bin twine\n\nwebsite: https://github.com/mobiata/twine")
                return
            }
        }

        let group = dispatch_group_create()
        
        dispatch_group_enter(group)
        let request = Alamofire.request(.GET, K_API_DOMAIN + K_API_PATH, parameters: ["cc":"EN"], encoding: ParameterEncoding.URLEncodedInURL, headers: nil)
            .responseJSON() {[weak self] (response) -> Void in
                if let strongSelf = self {
                    switch response.result {
                    case .Success(let JSON):
                        strongSelf.en = JSON as! [String : AnyObject]
                        strongSelf.ivEnTick.hidden = false
                        //                    print("Success with JSON: \(strongSelf.en)")
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                    }
                    dispatch_group_leave(group)
                }
        }
        
        NSLog("\(request)")
        
        dispatch_group_enter(group)
        Alamofire.request(.GET, K_API_DOMAIN + K_API_PATH, parameters: ["cc":"CHT"], encoding: ParameterEncoding.URLEncodedInURL, headers: nil)
            .responseJSON() {[weak self] (response) -> Void in
                if let strongSelf = self {
                    switch response.result {
                    case .Success(let JSON):
                        strongSelf.tc = JSON as! [String : AnyObject]
                        strongSelf.ivCHTTick.hidden = false
                        //                    print("Success with JSON: \(strongSelf.tc)")
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                    }
                    dispatch_group_leave(group)
                }
        }
        
        dispatch_group_enter(group)
        Alamofire.request(.GET, K_API_DOMAIN + K_API_PATH, parameters: ["cc":"CHS"], encoding: ParameterEncoding.URLEncodedInURL, headers: nil)
            .responseJSON() {[weak self] (response) -> Void in
                if let strongSelf = self {
                    switch response.result {
                    case .Success(let JSON):
                        strongSelf.sc = JSON as! [String : AnyObject]
                        strongSelf.ivCHSTick.hidden = false
                        //                    print("Success with JSON: \(strongSelf.sc)")
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                    }
                    dispatch_group_leave(group)
                }
        }
        
        dispatch_group_notify(group, dispatch_get_main_queue(), {[weak self] in
            print("processing")

            let xmlCharacterSet = NSMutableCharacterSet()
            
            xmlCharacterSet.addCharactersInRange(NSMakeRange(0x9, 1))
            xmlCharacterSet.addCharactersInRange(NSMakeRange(0x9, 1))
            xmlCharacterSet.addCharactersInRange(NSMakeRange(0xA, 1))
            xmlCharacterSet.addCharactersInRange(NSMakeRange(0xD, 1))
            xmlCharacterSet.addCharactersInRange(NSMakeRange(0x20, 0xD7FF - 0x20))
            xmlCharacterSet.addCharactersInRange(NSMakeRange(0xE000, 0xFFFD - 0xE000))
            xmlCharacterSet.addCharactersInRange(NSMakeRange(0x10000, 0x10FFFF - 0x10000))
            
            // Then create and retain an inverted set, which will thus contain all invalid XML characters.
            let invalidXMLCharacterSet = xmlCharacterSet.invertedSet
            
            if let strongSelf = self {
                let res : NSMutableArray = NSMutableArray()
                res.addObject("[[TEXT]]")
                
                if strongSelf.en["TranslationMap"] == nil || strongSelf.tc["TranslationMap"] == nil || strongSelf.sc["TranslationMap"] == nil {
                    strongSelf.exitAlertHelper("Error when getting text strings")
                    return
                }
                
                strongSelf.lblProcessing.hidden = false
                
                if let data : NSDictionary! = strongSelf.en["TranslationMap"] as! NSDictionary {
                    let tdata : NSDictionary! = strongSelf.tc["TranslationMap"] as! NSDictionary
                    let sdata : NSDictionary! = strongSelf.sc["TranslationMap"] as! NSDictionary
                    
                        // Character Range
                        // #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
                        for key in data.allKeys {
                            if let estr = data.objectForKey(key) as? String {
                                if let tstr = tdata.objectForKey(key) as? String {
                                    if let sstr = sdata.objectForKey(key) as? String {
                                        var enTxt:String! = self?.cleanString(estr, cset: invalidXMLCharacterSet)
                                        var tcTxt:String! = self?.cleanString(tstr, cset: invalidXMLCharacterSet)
                                        var scTxt:String! = self?.cleanString(sstr, cset: invalidXMLCharacterSet)
                                        
                                        if let matches:[String] = self?.matchesForRegexInText("<LB_+[a-zA-z_]*+>", text: enTxt) {
                                            if matches.count > 0 {
                                                print(matches)
                                                for match in matches {
                                                    let replace = (match as NSString).substringWithRange(NSMakeRange(1, match.characters.count - 2))
                                                    if var ustr = data.objectForKey(replace) as? String {
                                                        if ustr != replace {
                                                            ustr = (self?.cleanString(ustr, cset: invalidXMLCharacterSet))!
                                                            enTxt = enTxt.stringByReplacingOccurrencesOfString(match, withString: ustr)
                                                        }
                                                    }
                                                    if var tstr = tdata.objectForKey(replace) as? String {
                                                        if tstr != replace {
                                                            tstr = (self?.cleanString(tstr, cset: invalidXMLCharacterSet))!
                                                            tcTxt = tcTxt.stringByReplacingOccurrencesOfString(match, withString: tstr)
                                                        }
                                                    }
                                                    if var sstr = sdata.objectForKey(replace) as? String {
                                                        if sstr != replace {
                                                            sstr = (self?.cleanString(sstr, cset: invalidXMLCharacterSet))!
                                                            scTxt = scTxt.stringByReplacingOccurrencesOfString(match, withString: sstr)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // add text to file
                                        res.addObject("[\((self?.cleanString(key as! String, cset: invalidXMLCharacterSet)) as String!)]")
                                        res.addObject("en = \(enTxt)")
                                        res.addObject("zh-Hant = \(tcTxt)")
                                        res.addObject("zh-Hans = \(scTxt)")
                                    }
                                }
                            }
                        }
                }
                
                // get URL to the the documents directory in the sandbox
                let documentsUrl = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as NSURL
                // add a filename
                let folderUrl = documentsUrl//.URLByAppendingPathComponent("eCargo").URLByAppendingPathComponent("languages")
                let fileUrl = folderUrl.URLByAppendingPathComponent("strings.txt")
                do {
                    let nsRes = res.componentsJoinedByString("\n") as NSString
                    try nsRes.writeToURL(fileUrl, atomically: true, encoding: NSUTF8StringEncoding)
                } catch {
                    strongSelf.exitAlertHelper("Error when saving text file")
                    return
                }
                
                let iOSUrl = folderUrl.URLByAppendingPathComponent("language/ios")
                let andUrl = folderUrl.URLByAppendingPathComponent("language/android")
                
                do {
                    try NSFileManager.defaultManager().createDirectoryAtURL(iOSUrl, withIntermediateDirectories: true, attributes: nil)
                    try NSFileManager.defaultManager().createDirectoryAtURL(andUrl, withIntermediateDirectories: true, attributes: nil)
                    
                    // generate iOS text resource files
                    var task = NSTask()
                    task.launchPath = twinePath
                    task.arguments = ["generate-string-file", fileUrl.path!, "--lang", "en",  iOSUrl.path! + "/en.strings"]
                    task.launch()
                    task.waitUntilExit()
                    
                    task = NSTask()
                    task.launchPath = twinePath
                    task.arguments = ["generate-string-file", fileUrl.path!, "--lang", "zh-Hant", iOSUrl.path! + "/cht.strings"]
                    task.launch()
                    task.waitUntilExit()
                    
                    task = NSTask()
                    task.launchPath = twinePath
                    task.arguments = ["generate-string-file", fileUrl.path!, "--lang", "zh-Hans", iOSUrl.path! + "/chs.strings"]
                    task.launch()
                    task.waitUntilExit()
                    
                    // generate Android text resource files
                    try NSFileManager.defaultManager().createDirectoryAtURL(andUrl.URLByAppendingPathComponent("values"), withIntermediateDirectories: true, attributes: nil)
                    
                    task = NSTask()
                    task.launchPath = twinePath
                    task.arguments = ["generate-string-file", fileUrl.path!, "--lang", "en", andUrl.path! + "/values/strings.xml"]
                    task.launch()
                    task.waitUntilExit()
                    
                    try NSFileManager.defaultManager().createDirectoryAtURL(andUrl.URLByAppendingPathComponent("values-zh"), withIntermediateDirectories: true, attributes: nil)
                    
                    task = NSTask()
                    task.launchPath = twinePath
                    task.arguments = ["generate-string-file", fileUrl.path!, "--lang", "zh-Hant", andUrl.path! + "/values-zh/strings.xml"]
                    task.launch()
                    task.waitUntilExit()
                    
                    try NSFileManager.defaultManager().createDirectoryAtURL(andUrl.URLByAppendingPathComponent("values-zh-rCN"), withIntermediateDirectories: true, attributes: nil)
                    
                    task = NSTask()
                    task.launchPath = twinePath
                    task.arguments = ["generate-string-file", fileUrl.path!, "--lang", "zh-Hans", andUrl.path! + "/values-zh-rCN/strings.xml"]
                    task.launch()
                    task.waitUntilExit()
                    
                } catch {
                    strongSelf.exitAlertHelper("Error when generate resource files")
                    return
                }
                
                strongSelf.exitAlertHelper("Done. Resource files are generated in your 'Documents' folder")
            }
        })
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }
    
    // method to clean up characters
    func cleanString(string: String, cset : NSCharacterSet) -> String {
        // Are there any invalid characters in this string?
        let cstring = string.stringByReplacingOccurrencesOfString("\n", withString: "\\n").stringByReplacingOccurrencesOfString("\\\"", withString: "\"")
        
        if let _ = cstring.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) {
            //
        } else {
            return cstring
        }
        
        // Otherwise go through and remove any illegal XML characters from a copy of the string.
        let cleanedString = NSMutableString(string: cstring)
        
        var range = cleanedString.rangeOfCharacterFromSet(cset)
        
        while (range.length > 0)
        {
            cleanedString.deleteCharactersInRange(range)
            range = cleanedString.rangeOfCharacterFromSet(cset)
        }
        return String(cleanedString)
    }
    
    func matchesForRegexInText(regex: String!, text: String!) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
}

