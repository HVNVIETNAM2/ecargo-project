'use strict';

var config = require('./test-suites/config.json'),
    data = require('./data/data.json'),
    loginForm = require('./pages/login.form.js'),
    homeForm = require('./pages/home.form.js'),
    marvel = require('marvel-characters'),
    path = require('path');

exports.isMerchant = false;

exports.beforeEach = function (opts) {
    if (!opts) {
        opts = {}
    }

    opts.login = ( typeof opts.login !== 'undefined' ) ? opts.login : true;
    opts.isMerchant = ( typeof opts.isMerchant !== 'undefined' ) ? opts.isMerchant : false;

    exports.isMerchant = opts.isMerchant;

    var width = 1024;
    var height = 600;
    browser.driver.manage().window().setSize(width, height);

    if (opts.login) {
        this.helpers.login(exports.isMerchant);
    } else {
        this.helpers.logout(exports.isMerchant);
    }

    browser.sleep(200);
}

exports.afterEach = function () {
}

exports.users = data.users;

exports.helpers = {
    selectValues: {
        locationType: {
            all: '',
            store: 1,
            warehouse: 2
        },
        country: {
            all: '',
            china: 47
        },
        province: {
            all: '',
            shanghai: 11000008
        },
        city: {
            all: '',
            shanghaiXian: 11000101
        },

        merchantType: {
            all: -1,
            brandFlagship: 1,
            monoBrandAgent: 2,
            multiBrandAgent: 3
        },
        userStatus: {
            all: -1,
            deleted: 1,
            active: 2,
            pending: 3,
            inactive: 4
        },

        seasonLaunched: {
            none: 0,
            spring: 1,
            summer: 2,
            autmn: 3,
            winter: 4
        }
    },
    login: function () {
        var user = exports.isMerchant ? data.users.merchant.active : data.users.admin.active;

        homeForm.browse();

        homeForm.isLoaded().then(function (result) {
            if (result) {
                // already logged in
                return;
            } else {
                loginForm.setEmail(user.email);
                loginForm.setPassword(user.password);
                loginForm.submitForm();
                homeForm.checkLoaded();
                homeForm.switchLanguage("EN");
            }
        });
    },
    logout: function () {
        homeForm.browse();

        homeForm.isLoaded().then(function (result) {
            if (result) {
                homeForm.logout();
            }

            // already logged out
        });
    },
    createRandomUserInfo: function () {
        return {
            firstName: marvel(),
            lastName: marvel(),
            middleName: marvel(),
            displayName: marvel(),
            email: "auto." + marvel().replace(/[:\s\/]/ig, '').toLowerCase() + '' + Math.floor(Math.random() * 10000000) + '@wweco.com',
            mobileNumber: 20000000 + Math.floor(Math.random() * 10000000)
        };
    },
    createRandomMerchantInfo: function () {
        return {
            displayName: marvel(),
            subdomain: marvel().replace(/ /ig, '').toLowerCase(),
            description: 'Hello World',
            companyName: marvel().replace(/ /ig, '').toLowerCase() + ':' + Math.floor(Math.random() * 100000000),
            district: marvel(),
            bizRegNo: marvel().replace(/ /ig, '').toLowerCase() + ':' + Math.floor(Math.random() * 100000000),
            streetNo: Math.floor(Math.random() * 100000000),
            street: marvel().replace(/ /ig, ''),
        };
    },
    createRandomLocationInfo: function () {
        return {
            name: 'autotest ' + Math.floor(Math.random() * 100000000),
            code: 'autotest' + Math.floor(Math.random() * 100000000)
        };
    },

    createRandomProductInfo: function () {
        return {
            name: 'autoProduct' + Math.floor(Math.random() * 100000),
            manufacturerName: marvel(),
            styleCode: Math.floor(Math.random() * 100000).toString(),
            retailPrice: Math.floor(Math.random() * 1000),
            height: Math.floor(Math.random() * 10),
            length: Math.floor(Math.random() * 10),
            weight: Math.floor(Math.random() * 10),
            width: Math.floor(Math.random() * 10),
            skuCode: Math.floor(Math.random() * 10000000).toString(),
            skuCodeWithLetter: 'sku' + Math.floor(Math.random() * 1000000),
            barCode: Math.floor(Math.random() * 10000000).toString(),
            barCode2: Math.floor(Math.random() * 10000000).toString(),
            redColorName: 'red' + Math.floor(Math.random() * 10000),
            pinkColorName: 'pink' + Math.floor(Math.random() * 10000),

        };
    },

    setUpload: function (elem, relativePath) {
        var fileToUpload = relativePath,
            absolutePath = path.resolve(".", "../../data", fileToUpload);
        elem.sendKeys(absolutePath);
    },
    setField: function (elem, val) {
        elem.clear();
        elem.sendKeys(val);
    },
    setCb: function (elem, val) {
        var v = elem.evaluate('checked');
        v.then(function (c) {
            if (c !== val) {
                elem.click();
            }
        });
    },
    setAllCbs: function (elem, val) {
        if (elem == undefined) {
            return;
        }

        var currentScope = this;
        elem.each(function (element, index) {
            currentScope.setCb(element, val);
        });
    },
    setSelect: function (elem, val) {
        if (val == undefined || val.length <= 0) {
            elem.all(by.tagName('option')).first().click();
        } else {
            elem.element(by.css('[value="' + val + '"]')).click();
        }
    },
    waitUntilPresent: function (elem) {
        browser.wait(function () {
            try {
                return elem && elem.isPresent();
            } catch (e) {
            }
        });
    },
    takeScreenshot: function (filename) {
        browser.takeScreenshot().then(function (data) {
            var stream = fs.createWriteStream(filename);

            stream.write(new Buffer(data, 'base64'));
            stream.end();
        });
    }
}
