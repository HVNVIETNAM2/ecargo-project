'use strict';

var testCommon = require('../test.common.js'),
    homeForm = require('../pages/home.form.js'),
    userEdit = require('../pages/admin/user/user.edit.js');


var tests = function (isMerchant) {
    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });
        homeForm.hoverUserAvatar();
        homeForm.clickEditProfileBtn();
    });

    describe('Edit Profile', function () {
        it('should be success for valid information', function () {
            userEdit.submitForm();

            userEdit.checkUpdateUserSuccessMessage();
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}

describe('MC Edit Own Profile Tests', function () {
    tests(true);
});

describe('AC Edit Own Profile Tests', function () {
    tests(false);
});



