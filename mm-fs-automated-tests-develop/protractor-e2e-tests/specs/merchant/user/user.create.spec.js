'use strict';

var testCommon = require('../../../test.common'),
    userList = require('../../../pages/admin/user/user.list'),
    merchantUserList = require('../../../pages/merchant/user/user.list'),
    userForm = require('../../../pages/admin/user/user.form');


var tests = function (isMerchant) {

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        merchantUserList.browse();

        userList.clickCreateNewUserBtn();

        var newUser = testCommon.helpers.createRandomUserInfo();

        userForm.setFirstName(newUser.firstName);
        userForm.setLastName(newUser.lastName);
        userForm.setMiddleName(newUser.middleName);
        userForm.setDisplayName(newUser.displayName);
        userForm.setEmail(newUser.email);
        userForm.setMobileNumber(newUser.mobileNumber);
        userForm.setProfilePicture();
        userForm.checkOneRole();
    });

    describe('Create User', function () {
        it('should show email already exists error', function () {
            userForm.setEmail('admin@email.com');

            userForm.clickSaveNewUserBtn();

            userForm.checkEmailExistsMessage();
        });

        it('should show mobile number already exists error', function () {
            userForm.setMobileNumber(testCommon.users.admin.active.mobileNumber);

            userForm.clickSaveNewUserBtn();

            userForm.checkMobileExistsMessage();
        });

        it('should create new merchant user successfully', function () {

            userForm.clickSaveNewUserBtn();

            userForm.checkCreateUserSuccessMessage();
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}

describe('MC Create Merchant Users Tests', function () {
    tests(true);
});

describe('AC Create Merchant Users Tests', function () {
    tests(false);
});