'use strict';

var testCommon = require('../../../test.common.js'),
    userList = require('../../../pages/admin/user/user.list.js'),
    merchantUserList = require('../../../pages/merchant/user/user.list.js');

var tests = function(isMerchant) {

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        merchantUserList.browse();
    });

    describe('List User by search filter', function () {
        it('should show only the user with specified email', function () {
            var u = testCommon.users.merchant.pending;

            userList.setSearchText(u.email);

            userList.shouldHaveOnlyUser(u.email);
        });
    });


    describe('List User by status filter', function () {
        it('should show only the user with status pending', function () {
            userList.setStatus(testCommon.helpers.selectValues.userStatus.pending);

            userList.shouldAllBeStatus('Pending');
        });

        it('should show only the user with status active', function () {
            userList.setStatus(testCommon.helpers.selectValues.userStatus.active);

            userList.shouldAllBeStatus('Active');
        });

    });

    describe('List User by page size', function () {
        it('should show a total user of 10', function () {
            userList.setPageSize(10);

            userList.shouldHaveItemsOf(10);
        });

        it('should show a total user of 25', function () {
            userList.setPageSize(25);

            userList.shouldHaveItemsOf(25);
        });

        it('should show a total user of 100', function () {
            userList.setPageSize(100);

            userList.shouldHaveItemsOf(100);
        });
    });


    describe('List User by search text and then status filter', function () {
        it('should show only the user with status active and email', function () {
            var u = testCommon.users.merchant.pending;

            userList.setSearchText(u.email);

            userList.setStatus(testCommon.helpers.selectValues.userStatus.pending);

            userList.shouldAllBeStatus('Pending');

            userList.shouldHaveOnlyUser(u.email);
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}


describe('MC List Merchant User Tests', function () {
    tests(true);
});

describe('AC List Merchant User Tests', function () {
    tests(false);
});