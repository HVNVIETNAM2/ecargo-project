'use strict';

var testCommon = require('../../../test.common.js'),
    merchantList = require('../../../pages/merchant/user/user.list'),
    userEdit = require('../../../pages/admin/user/user.edit');


var tests = function (isMerchant) {

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        merchantList.edit();
    });

    describe('Edit Merchant User', function () {
        it('should show error for no roles chosen', function () {
            userEdit.toggleCheckAllRoles(false);

            userEdit.submitForm();

            userEdit.shouldShowSecurityGroupSettingError();
        });


        it('should show error for no location chosen', function () {
            userEdit.toggleCheckAllRoles(true);

            userEdit.toggleCheckAllLocation(false);

            userEdit.submitForm();

            userEdit.shouldShowLocationError();
        });

        it('should be success for valid information', function () {
            userEdit.toggleCheckAllRoles(false);

            userEdit.toggleAdministratorRole(true);

            userEdit.submitForm();

            userEdit.checkUpdateUserSuccessMessage();
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}


describe('MC Edit Merchant Users Tests', function () {
    tests(true);
});

describe('AC Edit Merchant Users Tests', function () {
    tests(false);
});
