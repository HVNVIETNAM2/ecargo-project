'use strict';

var testCommon = require('../../../test.common.js'),
    locationForm = require('../../../pages/merchant/location/location.form.js'),
    merchantLocationList = require('../../../pages/merchant/location/location.list.js');

var tests = function (isMerchant) {

    var newLocation = testCommon.helpers.createRandomLocationInfo();

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        merchantLocationList.browse();

        locationForm.clickCreateNewLocationBtn();

        locationForm.setLocationName(newLocation.name);

        locationForm.setLocationCode(newLocation.code);

        locationForm.setLocationType(testCommon.helpers.selectValues.locationType.store);

        locationForm.setCountry(testCommon.helpers.selectValues.country.china);

        locationForm.setProvince(testCommon.helpers.selectValues.province.shanghai);

        locationForm.setCity(testCommon.helpers.selectValues.city.shanghaiXian);
    });

    describe('Create Location', function () {
        it('should show city required (missing city info)', function () {
            locationForm.setCity(testCommon.helpers.selectValues.city.all);

            locationForm.clickSaveNewLocationBtn();

            locationForm.checkNoCityMessage();
        });

        it('should show country required (missing country info)', function () {
            locationForm.setCountry(testCommon.helpers.selectValues.country.all);

            locationForm.clickSaveNewLocationBtn();

            locationForm.checkNoCountryMessage();
        });

        it('should create new merchant location successfully', function () {
            locationForm.clickSaveNewLocationBtn();

            merchantLocationList.checkCreateOrUpdateLocationSuccessByCode(newLocation.code);
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}

describe('MC Create Merchant Locations Tests:', function () {
    tests(true);
});

describe('AC Create Merchant Locations Tests:', function () {
    tests(false);
});