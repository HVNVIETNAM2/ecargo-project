'use strict';

var testCommon = require('../../../test.common.js'),
    merchantLocationList = require('../../../pages/merchant/location/location.list.js');

var tests = function (isMerchant) {

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        merchantLocationList.browse();
    });

    describe('List Location by search filter individually', function () {
        it('should show only the user with specified merchant code', function () {
            merchantLocationList.setSearchText('autotest');

            merchantLocationList.shouldHaveOnlyLocationLike('autotest');
        });

        it('should show at most 10 items for setting page size of 10', function () {
            merchantLocationList.setPageSize(10);

            merchantLocationList.shouldHaveItemsOf(10);
        });
    });

    describe('List Location by search text and then country filter', function () {
        it('should show only the user with country china and location like autotest', function () {
            merchantLocationList.setSearchText('autotest');

            merchantLocationList.setCountry(testCommon.helpers.selectValues.country.china);

            merchantLocationList.shouldHaveOnlyLocationLike('autotest');
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}


describe('MC List Merchant Location Tests', function () {
    tests(true);
});

describe('AC List Merchant Location Tests', function () {
    tests(false);
});