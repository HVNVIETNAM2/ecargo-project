'use strict';

var testCommon = require('../../../test.common.js'),
    locationForm = require('../../../pages/merchant/location/location.form.js'),
    merchantLocationList = require('../../../pages/merchant/location/location.list.js');

var tests = function (isMerchant) {

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        merchantLocationList.edit();
    });

    describe('Edit Location', function () {
        it('should show error for no country chosen', function () {
            locationForm.setCountry('');

            locationForm.clickSaveNewLocationBtn();

            browser.waitForAngular();

            locationForm.checkNoCountryMessage();
        });

        it('should be successfully updated for valid information', function () {
            var newLocation = testCommon.helpers.createRandomLocationInfo();

            locationForm.setLocationCode(newLocation.code);

            locationForm.clickSaveNewLocationBtn();

            merchantLocationList.browse();

            merchantLocationList.checkCreateOrUpdateLocationSuccessByCode(newLocation.code);
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}

describe('MC Edit Merchant Location Tests', function () {
    tests(true);
});

describe('AC Edit Merchant Location Tests', function () {
    tests(false);
});