'use strict';

var testCommon = require('../../../test.common.js'),
    inventoryForm = require('../../../pages/merchant/inventory/inventory.form.js'),
    productInventoryList = require('../../../pages/merchant/inventory/product.inventory.list.js'),
    merchantLocationList = require('../../../pages/merchant/location/location.list.js'),
    locationForm = require('../../../pages/merchant/location/location.form.js');


var tests = function (isMerchant) {

    var newLocation = testCommon.helpers.createRandomLocationInfo();
    var skuId = testCommon.users.merchant.active.locations.active.skuId;

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        // create location first:
        newLocation = testCommon.helpers.createRandomLocationInfo();
        merchantLocationList.browse();
        locationForm.clickCreateNewLocationBtn();
        locationForm.setLocationName(newLocation.name);
        locationForm.setLocationCode(newLocation.code);
        locationForm.setLocationType(testCommon.helpers.selectValues.locationType.store);
        locationForm.setCountry(testCommon.helpers.selectValues.country.china);
        locationForm.setProvince(testCommon.helpers.selectValues.province.shanghai);
        locationForm.setCity(testCommon.helpers.selectValues.city.shanghaiXian);
        locationForm.clickSaveNewLocationBtn();

        // create inventory for the new location
        productInventoryList.browse(skuId);
        productInventoryList.setSearchText(newLocation.code);
        productInventoryList.clickCreateBtn();
    });

    describe('Create inventory', function () {
        it('should show allocation required if unlimited is set to false', function () {
            inventoryForm.setUnlimited(false);

            inventoryForm.clickSaveBtn();

            inventoryForm.checkNoAllocationError();
        });

        it('should create new inventory successfully', function () {
            inventoryForm.setUnlimited(false);

            inventoryForm.setAllocation(50);

            inventoryForm.clickSaveBtn();

            productInventoryList.checkInventoryExists(skuId, newLocation.code);
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}

describe('MC Create Merchant Inventory E2E Tests:', function () {
    tests(true);
});

describe('AC Create Merchant Inventory E2E Tests:', function () {
    tests(false);
});