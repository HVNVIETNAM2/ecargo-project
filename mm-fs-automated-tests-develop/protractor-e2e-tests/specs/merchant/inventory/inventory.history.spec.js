'use strict';

var testCommon = require('../../../test.common.js'),
    inventoryForm = require('../../../pages/merchant/inventory/inventory.form.js'),
    productInventoryList = require('../../../pages/merchant/inventory/product.inventory.list.js');

var tests = function (isMerchant) {

    var activeLocation = testCommon.users.merchant.active.locations.active;

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });
    });

    it('should show at least one history entry', function () {
        productInventoryList.browse(activeLocation.skuId);

        productInventoryList.setSearchText(activeLocation.locationId);

        productInventoryList.clickEditBtn();

        inventoryForm.clickHistoryLink();

        inventoryForm.checkHistoryExists();
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}


describe('MC View Merchant Inventory E2E Tests:', function () {
    tests(true);
});

describe('AC View Merchant Inventory E2E Tests:', function () {
    tests(false);
});
