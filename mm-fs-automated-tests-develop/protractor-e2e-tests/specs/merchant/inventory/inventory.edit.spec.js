'use strict';

var testCommon = require('../../../test.common.js'),
    inventoryForm = require('../../../pages/merchant/inventory/inventory.form.js'),
    productInventoryList = require('../../../pages/merchant/inventory/product.inventory.list.js');


var tests = function (isMerchant) {

    var activeLocation = testCommon.users.merchant.active.locations.active;

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        productInventoryList.browse(activeLocation.skuId);

        productInventoryList.setSearchText(activeLocation.locationId);

        productInventoryList.clickEditBtn();
    });

    describe('Save inventory - unlimited', function () {
        it('should save inventory successfully', function () {
            inventoryForm.setUnlimited(true);

            inventoryForm.clickSaveBtn();

            productInventoryList.checkInventoryExists(activeLocation.skuId, activeLocation.locationId);
        });
    });

    describe('Save inventory - certain amount', function () {
        it('should save inventory successfully', function () {
            inventoryForm.setUnlimited(false);

            inventoryForm.setAllocation(100);

            inventoryForm.clickSaveBtn();

            productInventoryList.checkInventoryExists(activeLocation.skuId, activeLocation.locationId);
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}


describe('MC Save Merchant Inventory E2E Tests:', function () {
    tests(true);
});

describe('AC Save Merchant Inventory E2E Tests:', function () {
    tests(false);
});