'use strict';

var productImport = require('../../../pages/merchant/product/product.import.js');
var testCommon = require('../../../test.common.js')
var tests = function (isMerchant) {
    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        productImport.browse();
    });
    describe('Upload valid excel', function () {
        it('should upload successfully', function () {
            productImport.uploadExcel();
            productImport.clickImportBtn();

            productImport.checkImportStatus("Product created: 0");
            productImport.checkImportStatus("Products updated: 10");
            productImport.checkImportStatus("Products skipped: 0");
            productImport.checkImportStatus("Record failed to upload: 0");

            productImport.clickImportHistory();
            productImport.checkHistoryListErrors("0");
            productImport.checkHistoryListSkipped("0");
            productImport.checkHistoryListInserted("0");
            productImport.checkHistoryListUpdated("10");
            productImport.checkHistoryListTotal("10");
        });
    });

    describe('Upload non excel file', function () {
        it('should show error message', function () {
            productImport.uploadNonExcel();
            productImport.checkInvalidTypeMessage();
        });
    });

    describe('Upload excel file without sku and style code', function () {
        it('should show record failed', function () {
            productImport.uploadExcelWithError();
            productImport.clickImportBtn();
            productImport.checkImportStatus("Product created: 0");
            productImport.checkImportStatus("Products updated: 10");
            productImport.checkImportStatus("Products skipped: 0");
            productImport.checkImportStatus("Record failed to upload: 1");
            productImport.clickImportHistory();
            productImport.checkHistoryListErrors("1");
            productImport.checkHistoryListSkipped("0");
            productImport.checkHistoryListInserted("0");
            productImport.checkHistoryListUpdated("10");
            productImport.checkHistoryListTotal("11");
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
}

describe('MC Import product Tests:', function () {
    tests(true);
});

describe('AC Create Merchant Locations Tests:', function () {
    tests(false);
});
