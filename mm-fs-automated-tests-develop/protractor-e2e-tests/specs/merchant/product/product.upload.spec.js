'use strict';

var testCommon = require('../../../test.common.js'),
    productList = require('../../../pages/merchant/product/product.list.js'),
    productUploadImage = require('../../../pages/merchant/product/product.upload.js')


var tests = function (isMerchant) {

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        productUploadImage.browse();

    });

    describe('Upload valid product image', function () {

        it('Upload result should show success', function () {
            productUploadImage.uploadValid();
            productUploadImage.shouldShowThumb(1);
            productUploadImage.shouldShowSuccess(1);
            productUploadImage.assertProductName("autoProduct92901");
            productUploadImage.assertStyleCode("16324");


        });
    });
    
    describe('Upload invalid image', function () {
        it('Upload result should show failed', function () {
            productUploadImage.uploadInvalid();
            productUploadImage.shouldShowThumb(1);
            productUploadImage.shouldShowSuccess(0);
            productUploadImage.shouldShowFailed(1);

        });
    });

    describe('Upload unrelated file', function () {
        it('Upload result should show failed', function () {
            productUploadImage.uploadUnrelated();
            productUploadImage.shouldShowThumb(1);
            productUploadImage.shouldShowSuccess(0);
            productUploadImage.shouldShowFailed(1);
        });
    });


    afterEach(function () {
        testCommon.afterEach();
    });
}

describe('MC Create Merchant Locations Tests:', function () {
    tests(true);
});


describe('AC Create Merchant Locations Tests:', function () {
    tests(false);
});


