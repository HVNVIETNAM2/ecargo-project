/**
 * Created by suechan on 12/3/2016.
 */
'use strict';

var testCommon = require('../../../test.common.js'),
    locationForm = require('../../../pages/merchant/location/location.form.js'),
    productList = require('../../../pages/merchant/product/product.list.js'),
    productForm = require('../../../pages/merchant/product/product.form.js'),
    merchantLocationList = require('../../../pages/merchant/location/location.list.js');

var tests = function (isMerchant) {
    var productInfo= testCommon.helpers.createRandomProductInfo();
    var newProductInfo = testCommon.helpers.createRandomProductInfo();

    console.log("product info: " + productInfo);
    console.log("new product info:" + newProductInfo);

    beforeEach(function () {
        testCommon.beforeEach({

            isMerchant: isMerchant
        });

        productList.browse();
        productList.clickCreateProduct();
    });

    describe('Edit product ', function () {

        it('should save the updated product info and go to product list', function () {

            // create product
            productForm.setProductNameSimpliifiedChinese(productInfo.name);
            productForm.setManufacturerName(productInfo.manufacturerName);
            productForm.selectCountry(testCommon.helpers.selectValues.country.china);
            productForm.selectYearLunched("1970");
            productForm.setStyleCode(productInfo.styleCode);
            productForm.setRetailPrice(productInfo.retailPrice);
            productForm.selectPrimaryChildCategory("Shoes", "Sandals");
            productForm.clickCreateSku();
            productForm.selectRedColor(productInfo.redColorName);
            productForm.selectPinkColor(productInfo.pinkColorName);
            productForm.clickGenerateSku();
            productForm.setSkuCode(0, productInfo.skuCode);
            productForm.setSkuCode(1, productInfo.skuCodeWithLetter);
            productForm.setBarCode(0, productInfo.barCode);
            productForm.setBarCode(1, productInfo.barCode2);
            productForm.clickCreateAndPublish();

            // search product
            productList.browse();
            productList.setSearch(productInfo.name);
            productList.selectProduct(productInfo.name);

            // edit product
            productForm.setProductNameSimpliifiedChinese(newProductInfo.name);
            productForm.setStyleCode(newProductInfo.styleCode);

            // check duplicated sku code error message
            productForm.setSkuCode(0, "111111");
            productForm.setSkuCode(1, "111111");
            productForm.clickSaveBtn();
            productForm.checkDuplicatedSkuCodeMessage();

            // check duplicated bar code error message
            productForm.setSkuCode(0, newProductInfo.skuCode);
            productForm.setBarCode(0, "111111");
            productForm.setSkuCode(1, newProductInfo.skuCodeWithLetter);
            productForm.setBarCode(1, "111111");
            productForm.clickSaveBtn();
            productForm.checkDuplicatedBarcodeMessage();

            // set valid sku and bar code
            productForm.setBarCode(0, newProductInfo.barCode);
            productForm.setBarCode(1, newProductInfo.barCode2);

            // save
            productForm.clickSaveBtn();
            productForm.checkEditProductSuccessMessage();

            // search edited product
            productList.browse();
            productList.setSearch(newProductInfo.name);
            productList.selectProduct(newProductInfo.name);

            // check updated product
            productForm.assertProdcutNameSimplifiedChinese(newProductInfo.name);
            productForm.assertStyleCode(newProductInfo.styleCode);
            productForm.assertSkuCode(0, newProductInfo.skuCode);
            productForm.assertSkuCode(1, newProductInfo.skuCodeWithLetter);
            productForm.assertBarCode(0, newProductInfo.barCode);
            productForm.assertBarCode(1, newProductInfo.barCode2);

        });


    });

    afterEach(function () {
        testCommon.afterEach();
    });
}

describe('MC Create Merchant Locations Tests:', function () {
    tests(true);
});


describe('AC Create Merchant Locations Tests:', function () {
    tests(false);
});


