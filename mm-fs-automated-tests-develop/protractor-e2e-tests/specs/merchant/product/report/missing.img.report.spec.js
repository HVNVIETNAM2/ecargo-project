'use strict';

var _ = require('lodash'),
    merchant = require('../../../../pages/merchant/merchant'),
    report = require('../../../../pages/merchant/product/report/missing.img.report.js');


describe('Product Missing Image Report E2E Tests:', function () {

    beforeEach(function () {
        browser.sleep(200);
    });

    describe('View Report by file filters', function () {
        it('should only show feature & description columns', function () {
            report.browse();
            report.setFileFilters([{
                index: 1,
                on: true
            }, {
                index: 2,
                on: true
            }, {
                index: 3,
                on: false
            }]);
            report.setStatus('all status');
            report.getReport();
            report.shouldShowOnlyColumns(['feature', 'description']);
        });

        it('should only feature & description & color columns', function () {
            report.setFileFilters([{
                index: 1,
                on: true
            }, {
                index: 2,
                on: true
            }, {
                index: 3,
                on: true
            }]);
            report.setStatus('all status');
            report.getReport();
            report.shouldShowOnlyColumns(['feature', 'description', 'color']);
        });
    });

    describe('View Report by adding missing feature images', function () {

    });

    describe('View Report by adding missing desc images', function () {

    });

    describe('View Report by adding missing color images', function () {

    });

    afterEach(function () {
        browser.takeScreenshot();
    });
});
