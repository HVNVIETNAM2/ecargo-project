'use strict';

var testCommon = require('../../../test.common.js'),
    locationForm = require('../../../pages/merchant/location/location.form.js'),
    productList = require('../../../pages/merchant/product/product.list.js'),
    productForm = require('../../../pages/merchant/product/product.form.js'),
    merchantLocationList = require('../../../pages/merchant/location/location.list.js');

var tests = function (isMerchant) {

    var productInfo = testCommon.helpers.createRandomProductInfo();
    console.log(productInfo);

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        productList.browse();

    });

      describe('Search in product list', function () {

     it('show products with specified keywords and filter ', function () {

     productList.setSearch("auto");
     productList.shouldHaveOnlyProductNameLike("auto");
     productList.setSearch("");
     productList.setStatus(3);
     productList.shouldFilterByStatusResultCorrect(3, "Pending");
     productList.setStatus(2);
     productList.shouldFilterByStatusResultCorrect(2, "Active");

     });

     });

      describe('Filter product list', function (){
     it('show products with specified filters', function () {


     productList.setCategory(7);
     productList.shouldFilterByCategoryResultCorrect(7, "Shoes > Sandals");
     productList.setStatus(2);
     productList.shouldFilterByCategoryResultCorrect(7, "Shoes > Sandals");
     productList.shouldFilterByStatusResultCorrect(2, "Active");

     // create a product without any images
     productList.clickCreateProduct();
     productForm.setProductNameSimpliifiedChinese(productInfo.name);
     productForm.setManufacturerName(productInfo.manufacturerName);
     productForm.selectCountry(testCommon.helpers.selectValues.country.china);
     productForm.selectYearLunched("1970");
     productForm.setStyleCode(productInfo.styleCode);
     productForm.setRetailPrice(productInfo.retailPrice);
     productForm.selectPrimaryChildCategory("Shoes", "Sandals");
     productForm.clickCreateSku();
     productForm.selectRedColor(productInfo.redColorName);
     productForm.clickGenerateSku();
     productForm.setSkuCode(0, productInfo.skuCodeWithLetter);
     productForm.setBarCode(0, productInfo.barCode);
     productForm.clickCreateAndPublish();

     productList.browse();

     // select Missing Feature Images
     productList.setMissingImageType(1);
     productList.setSearch(productInfo.name);
     //browser.pause();
     productList.shouldHaveOnlyProductNameLike(productInfo.name);

     // select Missing Color Images
     productList.clearSearch();
     productList.setMissingImageType(-1);
     productList.setMissingImageType(2);
     productList.setSearch(productInfo.name);
     productList.shouldHaveOnlyProductNameLike(productInfo.name);

     // select Missing Description Images
     productList.clearSearch();
     productList.setMissingImageType(-1);
     productList.setMissingImageType(3);
     productList.setSearch(productInfo.name);
     productList.shouldHaveOnlyProductNameLike(productInfo.name);

     });

     });

    describe('Change pagination product list', function () {
        it('should show proper product items per page', function () {
            // show 100 product items or less
            productList.setPageSize(100);
            productList.shouldHaveItemsOf(100);

            // show 25 product items
            productList.setPageSize(25);
            productList.shouldPageSizeCorrect(25);

            // Filter active status and show 10 items
            productList.setStatus(2);
            productList.setPageSize(10);
            productList.shouldPageSizeCorrect(10);
            productList.shouldFilterByStatusResultCorrect(2, "Active");
        });
    });

    describe('Change product status', function () {
        it('should show correct products with specified status', function () {
            productList.setStatus(2);
            productList.shouldFilterByStatusResultCorrect(2, "Active");

            productList.setStatus(3);
            productList.shouldFilterByStatusResultCorrect(3, "Pending");

            productList.setStatus(4);
            productList.shouldFilterByStatusResultCorrect(4, "Inactive");

        });
    });

    describe('select product page', function () {
        it('should show proper product page', function() {
            productList.clickNextpage();
            productList.clickPreviousPage();
            productList.clickLastPage();
            productList.clickFirstPage();
        });

    });

    afterEach(function () {
        testCommon.afterEach();
    });
}


describe('MC Create Merchant Locations Tests:', function () {
    tests(true);
});


 describe('AC Create Merchant Locations Tests:', function () {
 tests(false);
 });
