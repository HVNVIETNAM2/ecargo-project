/**
 * Created by suechan on 4/3/2016.
 */
'use strict';

var testCommon = require('../../../test.common.js'),
    productList = require('../../../pages/merchant/product/product.list.js'),
    productForm = require('../../../pages/merchant/product/product.form.js')


var tests = function (isMerchant) {

    var productInfo= testCommon.helpers.createRandomProductInfo();

    beforeEach(function () {
        testCommon.beforeEach({
            isMerchant: isMerchant
        });

        productList.browse();
        productList.clickCreateProduct();

    });

    describe('Create product', function () {

        it('should save product and go to product list', function () {


            // check mandatary field
            productForm.clickSaveDraft();
            productForm.checkNoProductName();
            productForm.checkNoStyleCodeMessage();
            productForm.checkNoRetailPriceMessage();
            productForm.checkNoCategoryMessage();
            productForm.checkNoStyleCodeMessage();

            // set product information
            productForm.setProductNameSimpliifiedChinese(productInfo.name);
            productForm.setManufacturerName(productInfo.manufacturerName);
            productForm.selectCountry(testCommon.helpers.selectValues.country.china);
            productForm.selectYearLunched("1970");
            productForm.setStyleCode(productInfo.styleCode);
            productForm.setRetailPrice(productInfo.retailPrice);
            productForm.selectPrimaryChildCategory("Shoes", "Sandals");

            // set shipping information
            productForm.setHeight(productInfo.height);
            productForm.setLength(productInfo.length);
            productForm.setWeight(productInfo.weight);
            productForm.setWidth(productInfo.width);

            // set production description
            productForm.setDescription("testing product description");
            productForm.setFeature("testing feature");
            productForm.setProductMaterial("testing product material");
            productForm.setSizeComment("testing size comment");

            // create product variant
            productForm.clickCreateSku();
            productForm.selectRedColor(productInfo.redColorName);
            productForm.selectPinkColor(productInfo.pinkColorName);
            productForm.clickGenerateSku();

            // check duplicated sku code error message
            productForm.setSkuCode(0, "111111");
            productForm.setBarCode(0, productInfo.barCode);
            productForm.setSkuCode(1, "111111");
            productForm.setBarCode(1, productInfo.barCode2);
            productForm.clickCreateAndPublish();
            productForm.checkDuplicatedSkuCodeMessage();

            // check duplicated bar code error message
            productForm.setSkuCode(0, productInfo.skuCode);
            productForm.setBarCode(0, "111111");
            productForm.setSkuCode(1, productInfo.skuCodeWithLetter);
            productForm.setBarCode(1, "111111");

            // upload images
            productForm.uploadFeaturedImage();
            productForm.uploadDescriptionImages();
            productForm.uploadColorImages();

            productForm.clickCreateAndPublish();
            productForm.checkDuplicatedBarcodeMessage();

            // set valid sku code and barcode
            productForm.setBarCode(0, productInfo.barCode);
            productForm.setBarCode(1, productInfo.barCode2);
            productForm.clickSaveDraft();

            // save draft
            productList.browse();
            productList.setSearch(productInfo.name);
            productList.selectProduct(productInfo.name);

            // save product
            productForm.clickSaveBtn();
            // check edit product success message
            productForm.checkEditProductSuccessMessage();

        });


    });

    afterEach(function () {
        testCommon.afterEach();
    });
}

describe('MC Create Merchant Locations Tests:', function () {
    tests(true);
});

describe('AC Create Merchant Locations Tests:', function () {
    tests(false);
});

