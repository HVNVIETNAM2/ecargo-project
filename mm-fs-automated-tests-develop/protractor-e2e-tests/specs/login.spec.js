'use strict';

var testCommon = require('../test.common.js'),
    loginForm = require('../pages/login.form.js'),
    homeForm = require('../pages/home.form.js');

var tests = function (isMerchant) {
    beforeEach(function () {
        testCommon.beforeEach({
            login: false,
            isMerchant: isMerchant
        });
    });

    describe('Login Validation', function () {
        it('Should report invalid credentials', function () {
            var fakeUser = testCommon.isMerchant ? testCommon.users.merchant.fake : testCommon.users.admin.fake;

            loginForm.browse();

            loginForm.setEmail(fakeUser.email);

            loginForm.setPassword(fakeUser.password);

            loginForm.submitForm();

            loginForm.checkInvalidCredentialsError();
        });

        it('Verify that the user is logged in', function () {
            var validUser = testCommon.isMerchant ? testCommon.users.merchant.active : testCommon.users.admin.active;

            loginForm.browse();

            loginForm.setEmail(validUser.email);

            loginForm.setPassword(validUser.password);

            loginForm.submitForm();

            homeForm.checkLoaded();
        });

        it('Verify that the user is logged out', function () {
            // login
            testCommon.helpers.login();

            // logout
            testCommon.helpers.logout();

            // check that we're back in login page
            loginForm.checkLoaded();
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
};

describe('MC Login Tests', function () {
    tests(true);
});

describe('AC Login Tests', function () {
    tests(false);
});