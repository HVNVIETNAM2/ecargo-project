'use strict';

var testCommon = require('../../../test.common'),
    merchantList = require('../../../pages/admin/merchant/merchant.list.js');


describe('AC List Merchant Tests', function () {

    beforeEach(function () {
        testCommon.beforeEach();

        merchantList.browse();

    });

    var u = testCommon.users.merchant.active;

    describe('List Merchant by search filters individually', function () {
        it('should show only the user with specified company name', function () {
            merchantList.setSearchText(u.companyName);

            merchantList.shouldHaveOnlyUser(u.companyName);
        });

        it('should show only the user with specified merchant type', function () {
            merchantList.setMerchantType(testCommon.helpers.selectValues.merchantType.multiBrandAgent);

            merchantList.shouldAllBeType('Multi-Brand Agent');
        });

        it('should show only the user with specified status', function () {
            merchantList.setStatus(testCommon.helpers.selectValues.userStatus.active);

            merchantList.shouldAllBeStatus('Active');
        });

        it('should show at most 10 items for setting page size of 10', function () {
            merchantList.setPageSize(10);

            merchantList.shouldHaveItemsOf(10);
        });

        it('should show at most 25 items for setting page size of 25', function () {
            merchantList.setPageSize(25);

            merchantList.shouldHaveItemsOf(25);
        });

        it('should show at most 100 items for setting page size of 100', function () {
            merchantList.setPageSize(100);

            merchantList.shouldHaveItemsOf(100);
        });
    });

    describe('List Merchant by search text and then status filter', function () {
        it('should show only the user with status active and company name: ' + u.companyName, function () {
            merchantList.setSearchText(u.companyName);

            merchantList.setStatus(testCommon.helpers.selectValues.userStatus.active);

            merchantList.shouldAllBeStatus('Active');

            merchantList.shouldHaveOnlyUser(u.companyName);
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
});
