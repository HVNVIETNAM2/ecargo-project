'use strict';

var testCommon = require('../../../test.common'),
    merchantForm = require('../../../pages/admin/merchant/merchant.form.js'),
    merchantList = require('../../../pages/admin/merchant/merchant.list.js');


describe('AC Create Merchant Tests', function () {
    var newMerchant = testCommon.helpers.createRandomMerchantInfo();

    beforeEach(function () {
        testCommon.beforeEach();

        merchantList.browse();

        merchantList.clickCreateNewMerchantBtn();

        merchantForm.setLogo();
        merchantForm.setBackground();
        merchantForm.setDisplayName(newMerchant.displayName);
        merchantForm.setSubdomain(newMerchant.subdomain);
        merchantForm.setDescription(newMerchant.description);
        merchantForm.setCompanyName(newMerchant.companyName);
        merchantForm.setBusinessRegNo(newMerchant.bizRegNo);
        merchantForm.setCountry(testCommon.helpers.selectValues.country.china);
        merchantForm.setProvince(testCommon.helpers.selectValues.province.shanghai);
        merchantForm.setCity(testCommon.helpers.selectValues.city.shanghaiXian);
        merchantForm.setDistrict(newMerchant.district);
        merchantForm.setStreetNo(newMerchant.streetNo);
        merchantForm.setStreet(newMerchant.street);
        merchantForm.setType(testCommon.helpers.selectValues.merchantType.brandFlagship);
        merchantForm.setRandomBrand();
    });

    describe('Create Merchant', function () {
        it('should show error for missing subdomain field information', function () {
            merchantForm.setSubdomain('');

            merchantForm.clickSaveNewUserBtn();

            merchantForm.checkSubdomainRequiredErrorMessage();
        });

        it('should create merchant successfully for valid information', function () {
            merchantForm.clickSaveNewUserBtn();

            merchantList.checkLoaded();

            merchantList.checkMerchantExists(newMerchant.companyName);
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
});
