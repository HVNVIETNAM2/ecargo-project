'use strict';

var testCommon = require('../../../test.common'),
    merchantList = require('../../../pages/admin/merchant/merchant.list.js'),
    merchantForm = require('../../../pages/admin/merchant/merchant.form.js');


describe('AC Edit Merchant Profile Tests', function () {

    var newMerchant = testCommon.helpers.createRandomMerchantInfo();

    beforeEach(function () {
        testCommon.beforeEach();

        merchantList.edit();

        merchantForm.setCompanyName(newMerchant.companyName);
    });

    describe('Edit Merchant Profile', function () {
        it('should show error for company name provided', function () {

            merchantForm.setCompanyName('');

            merchantForm.clickSaveNewUserBtn();

            merchantForm.checkCompanyNameRequiredErrorMessage();
        });

        it('should be success for valid information', function () {
            merchantForm.clickSaveNewUserBtn();

            merchantList.browse();

            merchantList.checkMerchantExists(newMerchant.companyName);
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
});
