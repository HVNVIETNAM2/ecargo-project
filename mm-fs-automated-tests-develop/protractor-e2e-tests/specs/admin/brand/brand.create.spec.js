'use strict';

var _ = require('lodash'),
    brand = require('../../../pages/brand/brand'),
    brandForm = require('../../../pages/brand/brand.form'),
    brandList = require('../../../pages/brand/brand.list');


describe('Create Brand E2E Tests:', function () {

    beforeEach(function () {
        browser.sleep(200);
    });

    var newBrand = brand.createRandomBrandInfo();

    describe('Create Brand ', function () {
        it('should show error for missing subdomain field information', function () {
            brandForm.browse();
            brandForm.setBrandCode(newBrand.brandCode);
            brandForm.setDisplayName(newBrand.displayName);
            brandForm.setCompanyDescription(newBrand.companyDescription);

            brandForm.clickSaveBtn();
            brandForm.checkSubdomainRequiredErrorMessage();
        });

        it('should create brand successfully for valid information', function () {
            brandForm.setSubdomain(newBrand.subdomain);
            brandForm.clickSaveBtn();
            brandForm.checkBrandExists(newBrand.brandCode);
        });
    });

    afterEach(function () {
        browser.takeScreenshot();
    });
});
