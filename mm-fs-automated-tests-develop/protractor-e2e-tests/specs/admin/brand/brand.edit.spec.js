'use strict';

var _ = require('lodash'),
    brand = require('../../../pages/brand/brand'),
    brandForm = require('../../../pages/brand/brand.form'),
    brandList = require('../../../pages/brand/brand.list');


describe('Edit Brand Profile E2E Tests:', function () {

    beforeEach(function () {
        browser.sleep(200);
    });

    var newBrand = brand.createRandomBrandInfo();

    describe('Edit Brand Profile', function () {
        it('should show error for no subdomain name provided', function () {
            brandList.edit();
            brandForm.setSubdomain('');
            brandForm.clickSaveBtn(true);
            brandForm.checkSubdomainRequiredErrorMessage();
        });

        it('should be success for valid information', function () {
            brandForm.setSubdomain(newBrand.subdomain);
            brandForm.clickSaveBtn(true);
            brandForm.checkBrandExists(newBrand.subdomain);
        });
    });

    afterEach(function () {
        browser.takeScreenshot();
    });
});
