'use strict';

var marvel = require('marvel-characters');
var _ = require('lodash');
var categoryCreate = require('../../../pages/category/category.create');


describe('Create Category E2E Tests:', function() {

  beforeEach(function() {
    browser.sleep(200);
  });

  describe('Create Category ', function() {

    var categoryName1 = marvel();
    var categoryCode1 = marvel().replace(/ /ig, '');

    var categoryName2 = marvel();
    var categoryCode2 = marvel().replace(/ /ig, '');

    it('should show error for missing field information', function() {
      categoryCreate.browse(0);
      categoryCreate.clickSaveAsDraftBtn(true);
      categoryCreate.checkCategoryNameRequiredErrorMessage();
    });

    it('should create category with save mode successfully for valid information and status should be active', function() {
      categoryCreate.setCategoryName(categoryName1);
      categoryCreate.setCategoryCode(categoryCode1);
      categoryCreate.setCommissionRate(0.1);
      categoryCreate.setPublishDate(new Date(), new Date());
      categoryCreate.clickSaveAsDraftBtn(false);
      categoryCreate.checkCategoryNameExistedInList(categoryName1, 'Active');
    });

    it('should create category with the same category name succefully? it should be failed', function() {
      categoryCreate.browse(0);
      categoryCreate.setCategoryName(categoryName1);
      categoryCreate.setCategoryCode(categoryCode1);
      categoryCreate.setCommissionRate(0.1);
      categoryCreate.setPublishDate(new Date(), new Date());
      categoryCreate.clickSaveAsDraftBtn(false);
      categoryCreate.checkSaveCategoryErrorMessage();
    });

    it('should create category with save as draft mode successfully for valid information and status should be pending', function() {
      categoryCreate.browse(0);
      categoryCreate.setCategoryName(categoryName2);
      categoryCreate.setCategoryCode(categoryCode2);
      categoryCreate.setCommissionRate(0.1);
      categoryCreate.setPublishDate(new Date(), new Date());
      categoryCreate.clickSaveAsDraftBtn(true);
      categoryCreate.checkCategoryNameExistedInList(categoryName2, 'Pending');
    });

    it('should create category as draft with the same category name succefully? it should be failed', function() {
      categoryCreate.browse(0);
      categoryCreate.setCategoryName(categoryName2);
      categoryCreate.setCategoryCode(categoryCode2);
      categoryCreate.setCommissionRate(0.1);
      categoryCreate.setPublishDate(new Date(), new Date());
      categoryCreate.clickSaveAsDraftBtn(true);
      categoryCreate.checkSaveCategoryErrorMessage();
    });

  });


});
