'use strict';

var testCommon = require('../../../test.common'),
    userList = require('../../../pages/admin/user/user.list');


describe('AC List User Tests', function () {

    beforeEach(function () {
        testCommon.beforeEach();

        userList.browse();
    });

    var u = testCommon.users.admin.active;

    describe('List User by search filters individually', function () {
        it('should show only the user with specified email', function () {
            userList.setSearchText(u.email);

            userList.shouldHaveOnlyUser(u.email);
        });

        it('should show only the user with status pending', function () {
            userList.setStatus(testCommon.helpers.selectValues.userStatus.pending);

            userList.shouldAllBeStatus('Pending');
        });

        it('should show at most 10 items for setting page size of 10', function () {
            userList.setPageSize(10);

            userList.shouldHaveItemsOf(10);
        });

        it('should show at most 25 items for setting page size of 25', function () {
            userList.setPageSize(25);

            userList.shouldHaveItemsOf(25);
        });

        it('should show at most 100 items for setting page size of 100', function () {
            userList.setPageSize(100);

            userList.shouldHaveItemsOf(100);
        });
    });

    describe('List User by search text and then status filter', function () {
        it('should show only the user with status active and email:' + u.email, function () {
            userList.setSearchText(u.email);

            userList.setStatus(testCommon.helpers.selectValues.userStatus.active);

            userList.shouldAllBeStatus('Active');

            userList.shouldHaveOnlyUser(u.email);
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
});
