'use strict';

var testCommon = require('../../../test.common'),
    userList = require('../../../pages/admin/user/user.list'),
    userForm = require('../../../pages/admin/user/user.form');


describe('AC Create User Tests', function () {

    var newUser = testCommon.helpers.createRandomUserInfo();

    beforeEach(function () {
        testCommon.beforeEach();

        // go to user list and click create
        userList.browse();
        userList.clickCreateNewUserBtn();

        // fill in the entire form
        userForm.setFirstName(newUser.firstName);
        userForm.setLastName(newUser.lastName);
        userForm.setMiddleName(newUser.middleName);
        userForm.setDisplayName(newUser.displayName);
        userForm.setEmail(newUser.email);
        userForm.setMobileNumber(newUser.mobileNumber);
        userForm.setProfilePicture();
        userForm.checkAllRoles();
    });

    describe('Create User', function () {
        it('should show email already exists error', function () {
            userForm.setEmail(testCommon.users.admin.active.email);

            userForm.clickSaveNewUserBtn();

            userForm.checkEmailExistsMessage();
        });

        it('should show mobile number already exists error', function () {
            userForm.setMobileNumber(testCommon.users.admin.active.mobileNumber);

            userForm.clickSaveNewUserBtn();

            userForm.checkMobileExistsMessage();
        });

        it('should create new user successfully', function () {
            userForm.clickSaveNewUserBtn();

            userForm.checkCreateUserSuccessMessage();
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
});
