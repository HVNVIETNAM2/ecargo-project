'use strict';

var testCommon = require('../../../test.common'),
    userEdit = require('../../../pages/admin/user/user.edit.js');


describe('AC Edit User Tests', function () {

    beforeEach(function () {
        testCommon.beforeEach();

        userEdit.browse(testCommon.users.admin.pending.userId);
    });

    describe('Edit User', function () {
        it('should be success for valid information', function () {
            userEdit.submitForm();

            userEdit.checkUpdateUserSuccessMessage();
        });

        it('should show error for no roles chosen', function () {
            userEdit.toggleCheckAllRoles(false);

            userEdit.submitForm();

            userEdit.shouldShowSecurityGroupSettingError();
        });

        it('should be success for valid information', function () {
            userEdit.toggleCheckAllRoles(true);

            userEdit.submitForm();

            userEdit.checkUpdateUserSuccessMessage();
        });
    });

    afterEach(function () {
        testCommon.afterEach();
    });
});
