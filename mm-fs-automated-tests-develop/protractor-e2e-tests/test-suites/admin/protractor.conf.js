'use strict';

var common = require('../common');

var protractorConfig = common.commonConfig;
protractorConfig.specs = [
    common.RELATIVE_PATH + 'specs/login.spec.js',
    common.RELATIVE_PATH + 'specs/admin/user/user.create.spec.js',
    common.RELATIVE_PATH + 'specs/admin/user/user.edit.spec.js',
    common.RELATIVE_PATH + 'specs/admin/user/user.list.spec.js',
    common.RELATIVE_PATH + 'specs/profile.edit.spec.js',
    common.RELATIVE_PATH + 'specs/admin/merchant/merchant.create.spec.js',
    common.RELATIVE_PATH + 'specs/admin/merchant/merchant.edit.spec.js',
    common.RELATIVE_PATH + 'specs/admin/merchant/merchant.list.spec.js',
    common.RELATIVE_PATH + 'specs/merchant/user/user.create.spec.js',
    common.RELATIVE_PATH + 'specs/merchant/user/user.edit.spec.js',
    common.RELATIVE_PATH + 'specs/merchant/user/user.list.spec.js',
    common.RELATIVE_PATH + 'specs/merchant/location/location.create.spec.js',
    common.RELATIVE_PATH + 'specs/merchant/location/location.edit.spec.js',
    common.RELATIVE_PATH + 'specs/merchant/location/location.list.spec.js',
    common.RELATIVE_PATH + 'specs/merchant/inventory/inventory.create.spec.js',
    common.RELATIVE_PATH + 'specs/merchant/inventory/inventory.edit.spec.js',
    common.RELATIVE_PATH + 'specs/merchant/inventory/inventory.history.spec.js',

    //common.RELATIVE_PATH + 'admin/brands/brand.create.spec.js',
    //common.RELATIVE_PATH + 'admin/brands/brand.edit.spec.js',
    //common.RELATIVE_PATH + 'admin/category/category.create.spec.js',

    //common.RELATIVE_PATH + 'specs/merchant/product/product.upload.spec.js',
    //common.RELATIVE_PATH + 'specs/merchant/product/report/missing.img.report.spec.js',
    //config.RELATIVE_PATH + 'specs/merchant/product/product.import.spec.js',
];

exports.config = protractorConfig;

