'use strict';


/**
 * this file is used to execute specified test cases at once
 */

/**
 * Module dependencies.
 */
var common = require('./common');

module.exports = function (grunt) {
    grunt.initConfig({
        grunt: {
            admin: {
                gruntfile: [
                    './admin/gruntfile.js'
                ]
            }
        }
    });

    /**
     * loading npm node modules tasks
     */
    common.loadGruntGrunt(grunt);


    /**
     * register tasks
     */
    grunt.registerTask('admin', ['grunt:admin']);
};


