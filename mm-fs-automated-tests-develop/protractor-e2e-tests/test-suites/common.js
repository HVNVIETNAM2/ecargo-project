'use strict';

var config = require("./config.json");

exports.RELATIVE_PATH = '../../';

function loadProtractor(grunt) {
    var cwd = process.cwd();
    process.chdir(__dirname + '/..');
    grunt.loadNpmTasks('grunt-protractor-runner');
    grunt.loadNpmTasks('grunt-protractor-coverage');
    process.chdir(cwd);
};

exports.loadShell = function (grunt) {
    var cwd = process.cwd();
    process.chdir(__dirname + '/..');
    grunt.loadNpmTasks('grunt-shell');
    process.chdir(cwd);
};

exports.loadGruntGrunt = function (grunt) {
    var cwd = process.cwd();
    process.chdir(__dirname + '/..');
    grunt.loadNpmTasks('grunt-grunt');
    process.chdir(cwd);
};

exports.commonConfig = {
    keepAlive: true,
    baseUrl: config.url,
    params: {},
    framework: 'jasmine2',
    multiCapabilities: [
        {
            browserName: config.browser,
            //shardTestFiles: true,
            //maxInstances: config.instances
        }
    ],
    onPrepare: function () {
        var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter'),
            reporters = require('jasmine-reporters');

        jasmine.getEnv().addReporter(new reporters.JUnitXmlReporter({
            savePath: 'junitReport/',
            consolidateAll: true
        }));

        jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
            savePath: 'htmlReport/',
            screenshotsFolder: 'images'
        }));
    }
};

exports.gruntTask = function (grunt) {
    // Project Configuration
    grunt.initConfig({
        protractor: {
            options: {
                configFile: 'protractor.conf.js',
                noColor: false,
                webdriverManagerUpdate: true
            },
            e2e: {
                options: {
                    args: {} // Target-specific arguments
                }
            }
        }
    });

    loadProtractor(grunt);

    grunt.registerTask('default', ['protractor']);
};
