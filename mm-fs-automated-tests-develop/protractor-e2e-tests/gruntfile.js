'use strict';

/**
 * Module dependencies.
 */
//var _ = require('lodash'),
//    path = require('path');

/**
 * global variables
*/
var jshintFiles = ['admin/**/*.js', 'shared/**/*.js'];

module.exports = function (grunt) {
    // Project Configuration
    grunt.initConfig({
        jshint: {
            options: {
                jshintrc: true
            },
            all: jshintFiles
        },
        watch: {
            jshint: {
                files: jshintFiles,
                tasks: ['jshint']
            }
        }
    });

    /**
     * loading npm node modules tasks
     */
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');


    /**
     * register tasks
     */
    grunt.registerTask('default', ['jshint', 'watch']);
};


