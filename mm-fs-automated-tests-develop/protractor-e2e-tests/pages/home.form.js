'use strict';

var testCommon = require('../test.common.js');

module.exports = {
    elements: {
        logoutConfirm: $('[ng-click="confirm()"]'),
        language: $('[ng-change="changeLanguage(language)"]'),
        userAvatar: $('.top-menu').$('.dropdown-user'),
        editProfileLink: $('[ui-sref="userEdit({userId:User.UserId})"]'),
        logoutLink: $('[ui-sref="logout"]')
    },
    browse: function () {
        browser.get('/' + (testCommon.isMerchant ? 'merchant' : 'admin') + '/#/home');
    },
    switchLanguage: function (lang) {
        this.elements.language.$('[value="' + lang + '"]').click();
    },
    hoverUserAvatar: function () {
        browser.actions().mouseMove(this.elements.userAvatar).perform();
    },
    clickEditProfileBtn: function () {
        this.elements.editProfileLink.click();
    },
    logout: function () {
        this.hoverUserAvatar();
        this.elements.logoutLink.click();
        this.elements.logoutConfirm.click();
    },
    checkLoaded: function () {
        testCommon.helpers.waitUntilPresent(this.elements.userAvatar);
    },
    isLoaded: function () {
        browser.waitForAngular();
        return this.elements.userAvatar.isDisplayed();
    },
};
