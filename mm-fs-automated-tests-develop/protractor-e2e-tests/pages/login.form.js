'use strict';

var testCommon = require('../test.common.js');

module.exports = {
    elements: {
        email: element(by.model('user.email')),
        password: element(by.model('user.password')),
        login: $('.mm_login_btn'),
        invalidCredentialsError: $('[ng-show="errCode && validationForm.$submitted"]'),
    },
    browse: function () {
        browser.get('/' + (testCommon.isMerchant ? 'merchant' : 'admin') + '/#/login');
    },
    setEmail: function (val) {
        testCommon.helpers.setField(this.elements.email, val);
    },
    setPassword: function (val) {
        testCommon.helpers.setField(this.elements.password, val);
    },
    submitForm: function () {
        this.elements.login.click();
    },
    checkInvalidCredentialsError: function () {
        expect(this.elements.invalidCredentialsError.isDisplayed()).toBeTruthy();
    },
    checkLoaded: function() {
        testCommon.helpers.waitUntilPresent(this.elements.login);
    }
};



