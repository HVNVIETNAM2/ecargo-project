'use strict';

var testCommon = require('../../../test.common');

module.exports = {
    elements: {
        firstNameTf: element(by.model('form.FirstName')),
        lastNameTf: element(by.model('form.LastName')),
        middleNameTf: element(by.model('form.MiddleName')),
        displayNameTf: element(by.model('form.DisplayName')),
        emailTf: element(by.model('form.Email')),
        mobileNumberTf: element(by.model('form.MobileNumber')),
        roleCb: element.all(by.css('input[checklist-value="item.SecurityGroupId"]')),
        alertText: element.all(by.repeater('alert in alerts')).first(),
        messageText: element.all(by.repeater('message in messages')).first(),
        saveBtn: $('[ng-disabled=isConfirmBtnDisabled]'),
        profileUpload: $('input[type="file"]')
    },
    setFirstName: function (val) {
        testCommon.helpers.setField(this.elements.firstNameTf, val);
    },

    setLastName: function (val) {
        testCommon.helpers.setField(this.elements.lastNameTf, val);
    },

    setMiddleName: function (val) {
        testCommon.helpers.setField(this.elements.middleNameTf, val);
    },

    setDisplayName: function (val) {
        testCommon.helpers.setField(this.elements.displayNameTf, val);
    },

    setEmail: function (val) {
         testCommon.helpers.setField(this.elements.emailTf, val);
    },

    setMobileNumber: function (val) {
        testCommon.helpers.setField(this.elements.mobileNumberTf, val);
    },
    setProfilePicture: function () {
        testCommon.helpers.setUpload(this.elements.profileUpload, 'images/logo.png');
    },
    checkOneRole: function () {
        testCommon.helpers.setCb(this.elements.roleCb.first(), true);
    },
    checkAllRoles: function () {
        testCommon.helpers.setAllCbs(this.elements.roleCb, true);
    },
    clickSaveNewUserBtn: function () {
        this.elements.saveBtn.click();
    },
    checkEmailExistsMessage: function () {
        expect(this.elements.alertText.getText()).toMatch('Email address already registered');
    },
    checkMobileExistsMessage: function () {
        expect(this.elements.alertText.getText()).toMatch('Mobile no. is already registered');
    },
    checkCreateUserSuccessMessage: function () {
        expect(this.elements.messageText.getText()).toContain('Created user successfully');
    }
};

