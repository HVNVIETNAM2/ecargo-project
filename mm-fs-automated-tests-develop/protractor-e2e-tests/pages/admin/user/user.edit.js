'use strict';

var testCommon = require('../../../test.common.js')

module.exports = {
    elements: {
        rolesCb: element.all(by.css('[checklist-model="form.UserSecurityGroupArray"]')),
        inventoryLocationCb: element.all(by.css('[checklist-model="form.UserInventoryLocationArray"]')),
        roleError: element(by.css('[ng-show=isShowSecurityGroupError]')),
        inventoryLocationError: element(by.css('[ng-show="$parent.isShowInventoryLocationError"]')),
        messageText: element.all(by.repeater('message in messages')).first(),
        saveBtn: element(by.css('button[ng-disabled=isSaveBtnDisabled]'))
    },
    browse: function (uid) {
        browser.get('/admin/#/user/edit/' + uid);
    },
    toggleAdministratorRole: function (val) {
        testCommon.helpers.setCb(this.elements.rolesCb.first(), val);
    },
    toggleCheckAllRoles: function (val) {
        testCommon.helpers.setAllCbs(this.elements.rolesCb, val);
    },
    toggleCheckAllLocation: function (val) {
        testCommon.helpers.setAllCbs(this.elements.inventoryCb, val);
    },
    shouldShowSecurityGroupSettingError: function () {
        expect(this.elements.roleError.isDisplayed()).toBeTruthy();
    },
    shouldShowLocationError: function () {
        expect(this.elements.inventoryLocationError.isDisplayed()).toBeTruthy();
    },
    checkUpdateUserSuccessMessage: function () {
        expect(this.elements.messageText.getText()).toMatch('Updated user successfully');
    },
    submitForm: function () {
        this.elements.saveBtn.click();
    }
};