'use strict';

var testCommon = require('../../../test.common')

module.exports = {
    elements: {
        roleSelect: element(by.model('filter.role')),
        userSearchTerm: element(by.model('userSearchTerm')),
        selectUserStatus: element(by.model('filter.status')),
        itemsPerPage: element(by.model('itemsPerPage')),
        newUserBtn: $('[ng-click="openCreateUserDialog()"]'),
        userList: element.all(by.repeater('item in allUserData')),
        emailList: element.all(by.binding('userInfo.Email')),
        statusList: element.all(by.repeater('item in allUserData').column('item.StatusId')),
    },
    browse: function () {
        browser.get('/admin/#/user');
    },
    setRole: function (val) {
        testCommon.helpers.setSelect(this.elements.roleSelect, val);
    },
    setSearchText: function (val) {
        testCommon.helpers.setField(this.elements.userSearchTerm, val);
    },
    setStatus: function (val) {
        testCommon.helpers.setSelect(this.elements.selectUserStatus, val);
    },
    setPageSize: function (val) {
        testCommon.helpers.setSelect(this.elements.itemsPerPage, 'number:' + val);
    },
    clickCreateNewUserBtn: function () {
        this.elements.newUserBtn.click();
    },
    shouldHaveOnlyUser: function (val) {
        expect(this.elements.emailList.count()).toBe(1);
        expect(this.elements.emailList.first().getText()).toBe(val);
    },
    shouldHaveItemsOf: function (val) {
        expect(this.elements.userList.count()).toBeLessThan(val + 1);
    },
    shouldAllBeStatus: function (status) {
        if (this.elements.statusList == undefined) {
            return;
        }

        this.elements.statusList.each(function (elem) {
            expect(elem.getText()).toBe(status);
        });
    }
};