'use strict';

var marvel = require('marvel-characters');

module.exports = {
    createRandomBrandInfo: function () {
        return {
            displayName: marvel(),
            brandCode: marvel().replace(/ /ig, ''),
            subdomain: marvel().replace(/ /ig, '').toLowerCase(),
            companyDescription: marvel().replace(/ /ig, '').toLowerCase() + ':' + Math.floor(Math.random() * 100000000),

        };
    }
};
