'use strict';

var _ = require('lodash'),
    path = require('path'),
    brandList = require('./brand.list.js');

module.exports = {
    browse: function (brandId) {
        browser.get('/admin/#/brands' + (brandId !== undefined ? '/' + brandId + '/edit' : '/create'));
    },
    setDisplayName: function (val) {
        element.all(by.model('brand.DisplayName[item.CultureCode]')).clear();
        element.all(by.model('brand.DisplayName[item.CultureCode]')).sendKeys(val);
    },
    setSubdomain: function (val) {
        element(by.model('brand.BrandSubdomain')).clear();
        element(by.model('brand.BrandSubdomain')).sendKeys(val);
    },
    setBrandCode: function (val) {
        element(by.model('brand.BrandCode')).clear();
        element(by.model('brand.BrandCode')).sendKeys(val);
    },
    setCompanyDescription: function (val) {
        element(by.model('brand.CompanyDesc')).clear();
        element(by.model('brand.CompanyDesc')).sendKeys(val);
    },
    clickSaveBtn: function (isEdit) {
        if (isEdit) {
            $('button span[ng-if="isEdit"]').click();
        } else {
            $('button span[ng-if="!isEdit"]').click();
        }
    },
    checkSubdomainRequiredErrorMessage: function () {
        expect($('[ng-show="validationForm.BrandSubdomain.$error.required"]').isDisplayed()).toBeTruthy();
    },
    checkBrandExists: function (brandCode) {
        brandList.browse();
        brandList.setSearchText(brandCode);
        brandList.shouldHaveOnlyUser(brandCode);
    }
};

