'use strict';

var _ = require('lodash'),
    brand = require('./brand');

module.exports = {
    browse: function () {
        browser.get('/admin/#/brands');
    },
    edit: function () {
        this.browse();
        element.all(by.binding('item.BrandName')).get(0).click();
        //$$('a.widget-box').first().click();
    },
    setSearchText: function (val) {
        element(by.model('searchString')).clear();
        element(by.model('searchString')).sendKeys(val);
    },
    shouldHaveOnlyUser: function (val) {
        expect(element.all(by.binding('item.BrandCode')).count()).toBe(1);
        expect(element.all(by.binding('item.BrandCode')).get(0).getText()).toBe(val);
    }
};