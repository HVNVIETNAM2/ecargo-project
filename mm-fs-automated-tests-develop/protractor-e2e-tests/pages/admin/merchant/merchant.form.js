'use strict';

var path = require('path'),
    testCommon = require('../../../test.common.js');

module.exports = {
    elements: {
        logoUpload: element.all(by.css('input[type="file"]')).get(0),
        backgroundUpload: element.all(by.css('input[type="file"]')).get(1),
        subdomainTf: element(by.model('form.Merchant.MerchantSubdomain')),
        companyNameTf: element(by.model('form.Merchant.MerchantCompanyName')),
        businessRegNoTf: element(by.model('form.Merchant.BusinessRegistrationNo')),
        countrySelect: element(by.model('form.Merchant.GeoCountryId')),
        provinceSelect: element(by.model('form.Merchant.GeoIdProvince')),
        citySelect: element(by.model('form.Merchant.GeoIdCity')),
        districtTf: element(by.model('form.Merchant.District')),
        postalCodeTf: element(by.model('form.Merchant.PostalCode')),
        apartmentTf: element(by.model('form.Merchant.Apartment')),
        floorTf: element(by.model('form.Merchant.Floor')),
        blockNoTf: element(by.model('form.Merchant.BlockNo')),
        buildingTf: element(by.model('form.Merchant.Building')),
        streetNoTf: element(by.model('form.Merchant.StreetNo')),
        streetTf: element(by.model('form.Merchant.Street')),
        subdomainRequiredError: $('[ng-show="validationForm.MerchantSubdomain.$error.required"]'),
        companyNameRequiredError: $('[ng-show="validationForm.MerchantCompanyName.$error.required"]'),
        saveMerchantBtn: $('[ng-click="saveMerchant(validationForm)"]'),
        merchantTypeSelect: element(by.model('form.Merchant.MerchantTypeId')),
        brandCombobox: element.all(by.css('input[role="combobox"]')).get(0),
        brandSelectList: element.all(by.css('li[ng-click="$select.select(brand,false,$event)"]')),
        staffCombobox: element.all(by.css('input[role="combobox"]')).get(1),
        staffSelectList: element.all(by.css('li[ng-click="$select.select(user,false,$event)"]')),
        rateTfList: element.all(by.css('input[ng-model="BillingCatory.newRate"]')),
        displayNameEnTf: element(by.model('form.Merchant.MerchantNameEN')),
        displayNameChtTf: element(by.model('form.Merchant.MerchantNameCHT')),
        displayNameChsTf: element(by.model('form.Merchant.MerchantNameCHS')),
        merchantDescEnTf: element(by.model('form.Merchant.MerchantDescEN')),
        merchantDescChtTf: element(by.model('form.Merchant.MerchantDescCHT')),
        merchantDescChsTf: element(by.model('form.Merchant.MerchantDescCHS')),
    },
    setLogo: function () {
        testCommon.helpers.setUpload(this.elements.logoUpload, '../../data/images/logo.png')
    },
    setBackground: function () {
        testCommon.helpers.setUpload(this.elements.backgroundUpload, '../../data/images/background.png')
    },
    setDisplayName: function (val) {
        testCommon.helpers.setField(this.elements.displayNameEnTf, val);
        testCommon.helpers.setField(this.elements.displayNameChtTf, val);
        testCommon.helpers.setField(this.elements.displayNameChsTf, val);
    },
    setSubdomain: function (val) {
        testCommon.helpers.setField(this.elements.subdomainTf, val);
    },
    setDescription: function (val) {
        testCommon.helpers.setField(this.elements.merchantDescEnTf, val);
        testCommon.helpers.setField(this.elements.merchantDescChtTf, val);
        testCommon.helpers.setField(this.elements.merchantDescChsTf, val);
    },
    setCompanyName: function (val) {
        testCommon.helpers.setField(this.elements.companyNameTf, val);
    },
    setBusinessRegNo: function (val) {
        testCommon.helpers.setField(this.elements.businessRegNoTf, val);
    },
    setCountry: function (val) {
        testCommon.helpers.setSelect(this.elements.countrySelect, 'number:' + val);
    },
    setProvince: function (val) {
        testCommon.helpers.setSelect(this.elements.provinceSelect, 'number:' + val);
    },
    setCity: function (val) {
        testCommon.helpers.setSelect(this.elements.citySelect, 'number:' + val);
    },
    setDistrict: function (val) {
        testCommon.helpers.setField(this.elements.districtTf, val);
    },
    setPostalCode: function (val) {
        testCommon.helpers.setField(this.elements.postalCodeTf, val);
    },
    setApartment: function (val) {
        testCommon.helpers.setField(this.elements.apartmentTf, val);
    },
    setFloor: function (val) {
        testCommon.helpers.setField(this.elements.floorTf, val);
    },
    setBlockNo: function (val) {
        testCommon.helpers.setField(this.elements.blockNoTf, val);
    },
    setBuilding: function (val) {
        testCommon.helpers.setField(this.elements.buildingTf, val);
    },
    setStreetNo: function (val) {
        testCommon.helpers.setField(this.elements.streetNoTf, val);
    },
    setStreet: function (val) {
        testCommon.helpers.setField(this.elements.streetTf, val);
    },
    setType: function (val) {
        testCommon.helpers.setSelect(this.elements.merchantTypeSelect, 'number:' + val);
    },
    setRandomBrand: function () {
        this.elements.brandCombobox.click();
        this.elements.brandSelectList.first().click();
    },
    assignRandomStaff: function () {
        this.elements.staffCombobox.click();
        this.elements.staffSelectList.first().click();
    },
    setRate: function (val) {
        this.elements.rateTfList.each(function(elem) {
            testCommon.helpers.setField(elem, val);
        });
    },
    clickSaveNewUserBtn: function () {
        this.elements.saveMerchantBtn.click();
    },
    checkSubdomainRequiredErrorMessage: function () {
        expect(this.elements.subdomainRequiredError.isDisplayed()).toBeTruthy();
    },
    checkCompanyNameRequiredErrorMessage: function () {
        expect(this.elements.companyNameRequiredError.isDisplayed()).toBeTruthy();
    }
};

