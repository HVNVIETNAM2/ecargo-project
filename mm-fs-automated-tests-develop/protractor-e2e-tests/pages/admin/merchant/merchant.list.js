'use strict';

var testCommon = require('../../../test.common.js');

module.exports = {
    elements: {
        userSearchField: element(by.model('userSearchTerm')),
        merchantTypeSelect: element(by.model('filter.merchantTypes')),
        statusSelect: element(by.model('filter.status')),
        createUserBtn: $('[ng-click="openCreateUserDialog()"]'),
        merchantCompanyNameList: element.all(by.binding('item.MerchantCompanyName')),
        itemPerPageSelect: element(by.model('itemsPerPage')),
        allUserDataList: element.all(by.repeater('item in allUserData')),
        merchantNameList: element.all(by.binding('item.MerchantName')),
        merchantTypeList: element.all(by.repeater('item in allUserData').column('item.MerchantTypeId')),
        statusList: element.all(by.repeater('item in allUserData').column('item.StatusId')),
    },
    selectValues: {
        merchantType: {
            all: -1,
            brandFlagship: 1,
            monoBrandAgent: 2,
            multiBrandAgent: 3
        },
        status: {
            all: -1,
            deleted: 1,
            active: 2,
            pending: 3,
            inactive: 4
        }
    },
    browse: function () {
        browser.get('/admin/#/merchant');
    },
    checkLoaded: function () {
        testCommon.helpers.waitUntilPresent(this.elements.userSearchField);
    },
    edit: function (userId) {
        if (userId) {
            browser.get('admin/#/merchant/edit/' + userId);
        } else {
            this.browse();
            this.elements.merchantNameList.first().click();
            element.all(by.css('a.widget-box')).first().click();
        }
    },
    setSearchText: function (val) {
        testCommon.helpers.setField(this.elements.userSearchField, val);
    },
    setMerchantType: function (val) {
        testCommon.helpers.setSelect(this.elements.merchantTypeSelect, val);
    },
    setStatus: function (val) {
        testCommon.helpers.setSelect(this.elements.statusSelect, val);
    },
    clickCreateNewMerchantBtn: function () {
        this.elements.createUserBtn.click();
    },
    shouldHaveOnlyUser: function (val) {
        expect(this.elements.merchantCompanyNameList.count()).toBe(1);
        expect(this.elements.merchantCompanyNameList.get(0).getText()).toBe(val);
    },
    shouldAllBeType: function (val) {
        if (this.elements.merchantTypeList == undefined) {
            return;
        }

        this.elements.merchantTypeList.each(function (elem) {
                expect(elem.getText()).toBe(val);
        });
    },
    shouldAllBeStatus: function (val) {
        if (this.elements.statusList == undefined) {
            return;
        }

        this.elements.statusList.each(function (elem) {
            expect(elem.getText()).toBe(val);
        });
    },
    setPageSize: function (val) {
        testCommon.helpers.setSelect(this.elements.itemPerPageSelect, 'number:' + val);
    },
    shouldHaveItemsOf: function (val) {
        expect(this.elements.allUserDataList.count()).toBeLessThan(val + 1);
    },
    checkMerchantExists: function (companyName) {
        this.setSearchText(companyName);
        this.shouldHaveOnlyUser(companyName);
    }
};