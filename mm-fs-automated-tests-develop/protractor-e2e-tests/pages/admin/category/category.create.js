'use strict';

module.exports = {

  browse: function(parentCategoryId) {
    browser.get('/admin/#/category/create/' + parentCategoryId);
  },

  setCategoryName: function(val) {
    element.all(by.model('Category.CategoryNameEN')).clear();
    element.all(by.model('Category.CategoryNameEN')).sendKeys(val);
    element.all(by.model('Category.CategoryNameCHS')).clear();
    element.all(by.model('Category.CategoryNameCHS')).sendKeys(val);
    element.all(by.model('Category.CategoryNameCHT')).clear();
    element.all(by.model('Category.CategoryNameCHT')).sendKeys(val);
  },

  setCategoryCode: function(val) {
    element(by.model('Category.CategoryCode')).clear();
    element(by.model('Category.CategoryCode')).sendKeys(val);
  },

  setCommissionRate: function(val) {
    element(by.model('Category.DefaultCommissionRate')).clear();
    element(by.model('Category.DefaultCommissionRate')).sendKeys(val);
  },

  setPublishDate: function(startDateTime, endDateTime) {

    element.all(by.model('Category.IsForever')).get(1).click();

    // element(by.model('Category.StartPublishTime')).clear();
    // element(by.model('Category.StartPublishTime')).sendKeys(startDateTime);
    // element(by.model('Category.EndPublishTime')).clear();
    // element(by.model('Category.EndPublishTime')).sendKeys(endDateTime);
  },

  clickSaveAsDraftBtn: function(isSaveAsDraft) {
    if (isSaveAsDraft) {
      $('#btnSaveAsDraft').click();
    } else {
      $('#btnSave').click();
    }
  },

  checkCategoryNameRequiredErrorMessage: function() {
    expect($('[ng-show="validationForm.CategoryNameEN.$error.required"]').isDisplayed()).toBeTruthy();
    expect($('[ng-show="validationForm.CategoryNameCHS.$error.required"]').isDisplayed()).toBeTruthy();
    expect($('[ng-show="validationForm.CategoryNameCHT.$error.required"]').isDisplayed()).toBeTruthy();
  },

  checkSaveCategoryErrorMessage: function() {
    element.all(by.repeater('message in messages')).then(function(elements) {
      expect(elements.length>0).toBe(true);
    });
  },

  checkCategoryNameExistedInList: function(categoryName, statusName) {
    element.all(by.repeater('node in data')).then(function(elements) {
      var newEle = elements[elements.length - 1];
      expect(newEle.$$('td').get(1).getText()).toBe(categoryName);
      expect(newEle.$$('td').get(4).getText()).toBe(statusName);
    });
  }

};
