'use strict';

var testCommon = require('../../../test.common');

module.exports = {
    elements: {
        firstNameList: element.all(by.binding('userInfo.FirstName'))
    },
    browse: function (merchantId) {
        if (testCommon.isMerchant) {
            browser.get('/merchant/#/user');
        } else {
            if (merchantId == undefined) {
                merchantId = testCommon.users.merchant.pending.merchantId;
            }
            browser.get('/admin/#/merchant/' + merchantId + '/users');
        }
    },
    edit: function (merchantId, userId) {
        if (userId == undefined) {
            userId = testCommon.users.merchant.pending.userId;
        }

        if (merchantId == undefined) {
            merchantId = testCommon.users.merchant.pending.merchantId;
        }

        if (testCommon.isMerchant) {
            browser.get('/merchant/#/user/edit/' + userId);
        } else {
            browser.get('/admin/#/merchant/' + merchantId + '/users/edit/' + userId);
        }
    }
};