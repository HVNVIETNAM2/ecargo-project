'use strict';

var testCommon = require('../../../test.common');

module.exports = {
    elements: {
        searchTf: element(by.model('inventorySearchTerm')),
        countrySelect: element(by.model('filter.country')),
        provinceSelect: element(by.model('filter.province')),
        citySelect: element(by.model('filter.city')),
        locationTypeSelect: element(by.model('filter.type')),
        pageSizeSelect: element(by.model('itemsPerPage')),
        locationExternalCodeList: element.all(by.binding('item.LocationExternalCode')),
        allLocationList: element.all(by.repeater('item in allData')),
        locationNameList: element.all(by.binding('item.LocationName'))
    },
    browse: function (merchantId) {
        merchantId = merchantId || testCommon.users.merchant.pending.merchantId;

        if (testCommon.isMerchant) {
            browser.get('/merchant/#/inventoryLocation');
        } else {
            browser.get('/admin/#/merchant/' + merchantId + '/location');
        }
    },
    edit: function (merchantId, locationCode) {
        this.browse(merchantId);

        this.setSearchText(locationCode || '');

        this.elements.locationNameList.first().click();
    },
    setSearchText: function (val) {
        testCommon.helpers.setField(this.elements.searchTf, val);
    },
    setCountry: function (val) {
        testCommon.helpers.setSelect(this.elements.countrySelect, val);
    },
    setProvince: function (val) {
        testCommon.helpers.setSelect(this.elements.provinceSelect, val);
    },
    setCity: function (val) {
        testCommon.helpers.setSelect(this.elements.citySelect, val);
    },
    setType: function (val) {
        testCommon.helpers.setSelect(this.elements.locationTypeSelect, val);
    },
    setPageSize: function (val) {
        testCommon.helpers.setSelect(this.elements.pageSizeSelect, 'number:' + val);
    },
    shouldHaveOnlyLocation: function (val) {
        expect(this.elements.locationExternalCodeList.count()).toBe(1);
        expect(this.elements.locationExternalCodeList.get(0).getText()).toBe(val);
    },
    shouldHaveOnlyLocationLike: function (val) {
        this.elements.locationExternalCodeList.each(function (code) {
                expect(code.getText()).toContain(val);
        });

        expect(this.elements.locationExternalCodeList.count()).toBeGreaterThan(0);
    },
    shouldHaveItemsOf: function (val) {
        expect(this.elements.allLocationList.count()).toBeLessThan(val + 1);
    },
    checkCreateOrUpdateLocationSuccessByCode: function (code) {
        this.setSearchText(code);
        this.shouldHaveOnlyLocation(code);
    }
};


