'use strict';

var testCommon = require('../../../test.common');

module.exports = {
    elements: {
        locationNameTfList: element.all(by.model('location.Culture[item.CultureCode].LocationName')),
        locationIdTf: element(by.model('location.LocationExternalCode')),
        locationTypeSelect: element(by.model('location.InventoryLocationTypeId')),
        countrySelect: element(by.model('location.GeoCountryId')),
        provinceSelect: element(by.model('location.GeoIdProvince')),
        citySelect: element(by.model('location.GeoIdCity')),
        createNewLocationBtn: $('[ng-click="CreateNewInventoryLocation()"]'),
        saveLocationBtn: $('[ng-disabled=isConfirmBtnDisabled]'),
        countryRequiredError: element(by.css('[ng-show="validationForm.GeoCountryId.$error.required"]')),
        cityRequiredError: element(by.css('[ng-show="validationForm.GeoIdCity.$error.required"]')),
    },
    setLocationName: function (val) {
        this.elements.locationNameTfList.each(function (elem) {
            testCommon.helpers.setField(elem, val);
        });
    },
    setLocationCode: function (val) {
        testCommon.helpers.setField(this.elements.locationIdTf, val);
    },
    setLocationType: function (val) {
        testCommon.helpers.setSelect(this.elements.locationTypeSelect, val);
    },
    setCountry: function (val) {
        testCommon.helpers.setSelect(this.elements.countrySelect, val);
    },
    setProvince: function (val) {
        testCommon.helpers.setSelect(this.elements.provinceSelect, val);
    },

    setCity: function (val) {
        testCommon.helpers.setSelect(this.elements.citySelect, val);
    },
    clickCreateNewLocationBtn: function() {
        this.elements.createNewLocationBtn.click();
    },
    clickSaveNewLocationBtn: function () {
        this.elements.saveLocationBtn.click();
    },
    checkNoCountryMessage: function () {
        expect(this.elements.countryRequiredError.isDisplayed()).toBeTruthy();
    },
    checkNoCityMessage: function () {
        expect(this.elements.cityRequiredError.isDisplayed()).toBeTruthy();
    }
};

