'use strict';

var testCommon = require('../../../test.common.js');

module.exports = {
    elements: {
        createBtn: element.all(by.css('a[ng-if="!inventory.SkuId"]')).get(0),
        editBtn: element.all(by.css('a[ng-if="inventory.SkuId"]')).get(0),
        locationExternalCodeList: element.all(by.binding('inventory.LocationExternalCode')),
        searchTf: element(by.model('query'))
    },
    browse: function (skuId, merchantId) {
        var url;
        skuId = skuId || testCommon.users.merchant.active.locations.active.skuId;

        if (testCommon.isMerchant) {
            url = '/merchant/#';
        } else {
            merchantId = merchantId || testCommon.users.merchant.active.merchantId;
            url = '/admin/#/merchant/' + merchantId;
        }
        url += '/sku/' + skuId + '/inventory-list';

        browser.get(url);
    },
    create: function(location, merchantId, isFromLocation) {
        location.inventoryId = undefined;
        this.edit(location, merchantId);
    },
    edit: function (location, merchantId, isFromLocation) {
        var url = '';
        location = location || {};

        if (testCommon.isMerchant) {
            url = '/merchant/#';
        } else {
            merchantId = merchantId || testCommon.users.merchant.active.merchantId;
            url = '/admin/#/merchant/' + merchantId
        }

        location.skuId = location.skuId || testCommon.users.merchant.active.locations.active.skuId;
        location.locationInventoryId = location.locationInventoryId || testCommon.users.merchant.active.locations.active.locationInventoryId;

        url += '/sku/' + location.skuId + '/';
        url += (isFromLocation ? 'inventoryLocation' : 'inventory-list' );
        url += '/' + location.locationInventoryId;
        url += (location.inventoryId == undefined ? '/create-inventory' : '/edit-inventory/' + location.inventoryId);

        browser.get(url);
    },
    setSearchText: function (val) {
        testCommon.helpers.setField(this.elements.searchTf, val);
    },
    clickCreateBtn: function () {
        this.elements.createBtn.click();
    },
    clickEditBtn: function () {
        this.elements.editBtn.click();
    },
    shouldHaveOnlyLocation: function (val) {
        expect(this.elements.locationExternalCodeList.count()).toBe(1);
        expect(this.elements.locationExternalCodeList.get(0).getText()).toBe(val);
    },
    checkInventoryExists: function (skuId, locationId, merchantId) {
        this.browse(skuId, merchantId);

        this.setSearchText(locationId);

        this.shouldHaveOnlyLocation(locationId);
    },
}
;


