'use strict';

var testCommon = require('../../../test.common'),
    list = require('./product.inventory.list.js');

module.exports = {
    elements: {
        isPerpetualCb: element.all(by.model('isPerpetual')),
        qtyAllocatedTf: element(by.model('qtyAllocated')),
        submitBtn: $('button[type="submit"]'),
        historyBtn: $('a[ng-click="listHistory()"]'),
        noAllocationError: element(by.css('[ng-show="validationForm.QtyAllocated.$invalid && (validationForm.$submitted || validationForm.QtyAllocated.$touched)"]')),
        historyList: element.all(by.repeater('history in data.InventoryJournalList')),
    },
    setUnlimited: function (isPerpetual) {
        this.elements.isPerpetualCb.get(isPerpetual ? 0 : 1).click();
    },
    setAllocation: function (val) {
        testCommon.helpers.setField(this.elements.qtyAllocatedTf, val);
    },
    clickSaveBtn: function () {
        this.elements.submitBtn.click();
    },
    clickHistoryLink: function () {
        this.elements.historyBtn.click();
    },
    checkNoAllocationError: function () {
        expect(this.elements.noAllocationError.isDisplayed()).toBeTruthy();
    },
    checkHistoryExists: function () {
        expect(this.elements.historyList.count()).toBeGreaterThan(0);
    }
};

