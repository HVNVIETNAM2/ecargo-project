'use strict';
var testCommon = require('../../../test.common.js');

module.exports = {
    elements: {
        // Tf: text field
        // Select: drop-down list
        // Rbtn: radio button
        // Cb: check box
        excelUploadBtn: element.all(by.css('input[type="file"]')),
        importBtn: element(by.css('[ng-click="isBtnClicked=true;canImport && !importing && importNow()"]')),
        importStatus: element(by.cssContainingText('.ng-binding')),
        invalidTypeError: element(by.cssContainingText('.ng-binding', 'Please upload a excel file')),
        importHistoryBtn: element(by.css('[ng-click="shownTab = \'ImportHistory\'"]')),
        historyList: element.all(by.repeater('item in allData | orderBy:sortBy:reverse | offset: (currentPage-1)*itemsPerPage | limitTo: itemsPerPage'))
    },

    browse: function () {
        if (!testCommon.isMerchant) {
            browser.get('/admin/#/merchant/13/product/import');
        }
        else browser.get('/merchant/#/product/import');
    },

    uploadExcel: function () {
        testCommon.helpers.setUpload(this.elements.excelUploadBtn, 'products/products_valid.xlsx');
    },

    uploadNonExcel: function () {
        testCommon.helpers.setUpload(this.elements.excelUploadBtn, 'images/logo.png');
    },

    uploadExcelWithError: function () {
        testCommon.helpers.setUpload(this.elements.excelUploadBtn, 'products/products_invalid.xlsx');
    },

    clickImportBtn: function () {
        this.elements.importBtn.click();
    },

    checkImportStatus: function (status) {
        expect(element(by.cssContainingText('.ng-binding', status)).isDisplayed()).toBeTruthy();
    },

    checkInvalidTypeMessage: function () {
        expect(this.elements.invalidTypeError.isDisplayed()).toBeTruthy();
    },

    clickImportHistory: function () {
        this.elements.importHistoryBtn.click();
    },

    checkHistoryListErrors: function (numberOfError) {
        expect(this.elements.historyList.get(0).$$('td').get(1).getText()).toEqual(numberOfError);
    },

    checkHistoryListSkipped: function (numberOfSkipped) {
        expect(this.elements.historyList.get(0).$$('td').get(2).getText()).toEqual(numberOfSkipped);
    },

    checkHistoryListUpdated: function (numberOfUpdated) {

        expect(this.elements.historyList.get(0).$$('td').get(3).getText()).toEqual(numberOfUpdated);
    },

    checkHistoryListInserted: function (numberOfInserter) {
        expect(this.elements.historyList.get(0).$$('td').get(4).getText()).toEqual(numberOfInserter);
    },

    checkHistoryListTotal: function (numberOfTotal) {
        expect(this.elements.historyList.get(0).$$('td').get(5).getText()).toEqual(numberOfTotal);
    }

}
