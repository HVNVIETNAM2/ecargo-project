'use strict';

var _ = require('lodash');

module.exports = {
    browse: function () {
        browser.get('/merchant/#/product/missing-img-report');
    },
    setFileFilters: function (filters) {
        _.each(filters, function (filter) {
            var element = $$('input[checklist-model="form.filefilter"]').get(filter.index - 1);
            element.evaluate('checked').then(function (c) {
                if (c === !filter.on) {
                    element.click();
                }
            });
        });
    },
    setStatus: function (val) {
        var statusMapping = {
            'all status': -1,
            'active': 2,
            'pending': 3
        };

        var select = element(by.model('form.status'));
        select.$('[value="number:' + statusMapping[val] + '"]').click();
    },
    getReport: function () {
        $$('[ng-click="run()"]').get(0).click();
    },
    shouldShowOnlyColumns: function (cols) {
        var map = {
            'feature': 1,
            'description': 2,
            'color': 3
        };
        _.each(_.reduce(cols, function (result, col) {
            return result.concat(map[col]);
        }, []), function (index) {
            expect($('[ng-if="fileFilterExists(' + index + ')"]').isPresent()).toBeTruthy();
        });
    }
};


