/**
 * Created by suechan on 4/3/2016.
 */
'use strict';
var path = require('path');
var testCommon = require('../../../test.common.js');
module.exports = {
    elements: {
        // Tf: text field
        // Select: drop-down list
        // Rbtn: radio button
        // Cb: check box
        productNameEnTf: element(by.name('SkuNameEN')),
        productNameTcTf: element(by.name('SkuNameCHT')),
        productNameScTf: element(by.name('SkuNameCHS')),
        manufacturerNameTf: element(by.model('style.ManufacturerName')),
        countrySelect: element(by.model('style.GeoCountryId')),
        yearLaunchedSelect: element(by.model('style.LaunchYear')),
        seasonLunchedSelect: element(by.model('style.SeasonId')),
        styleCodeTf: element(by.model('style.StyleCode')),
        badgeCodeSelect: element(by.model('style.BadgeId')),
        retailPriceTf: element(by.model('style.PriceRetail')),
        salesPriceTf: element(by.model('style.PriceSale')),

        // product category
        primaryCategoryBtn: $('[ng-click="openCategoryDialog(productCategory)"]'),
        merchandizeCategoryBtn: $('[ng-click="openCategoryDialog(productCategory)"]'),
        saveCategoryBtn: element(by.css('[ng-click="confirm()"]')),

        // shipping information
        lengthTf: element(by.model('style.LengthCm')),
        heightTf: element(by.model('style.HeightCm')),
        widthTf: element(by.model('style.WidthCm')),
        weightTf: element(by.model('style.WeightKg')),

        // product description
        descriptionTf: element(by.name('SkuDescCHS')),
        featureTf: element(by.name('SkuFeatureCHS')),
        productMaterialTf: element(by.name('SkuMaterialCHS')),
        sizeCommentTf: element(by.name('SkuSizeCommentCHS')),

        // create sku
        createSkubtn: $('[ng-click="openCreateSkuDialog(style)"]'),
        noColorSizeCb: element(by.model('isNoColorSize')),
        colorCb: element.all(by.css('[ng-click="operateColor(colorCode, color.isChoose)"]')),
        redTf: element(by.css('[name="REDColorNameCHS"]')),
        pinkTf: element(by.css('[name="PINKColorNameCHS"]')),
        skuGenerateBtn: $('[ng-click="generateSkus()"]'),
        skuCodeTf: element.all(by.model('skuObj.SkuCode')),
        barCodeTf: element.all(by.model('skuObj.Bar')),
        safetyThresholdTf: element.all(by.model('skuObj.QtySafetyThreshold')),

        // message
        productNameRequiredError: element(by.css('[ng-show="$showErrorMultiLingual(validationForm, \'SkuName\', language, \'required\')"]')),
        styleCodeRequiredError: element(by.css('[ng-show="validationForm.StyleCode.$error.required"]')),
        // retailPriceRequiredError: element(by.css('[ng-show="validationForm.PriceRetail.$error.required"]')),
        retailPriceRequiredError: element(by.css('[ng-show="validationForm.$submitted || validationForm[\'PriceRetail\'].$touched"]')),

        categoryRequiredError: element(by.css('[ng-show="validationForm.requireCategory.$error.required"]')),
        skuRequiredError: element(by.css('[ng-show="style.SkuList.length===0"]')),
        duplicatedSkuCodeError: element(by.cssContainingText('.ng-binding', 'SKU code needs to be unique')),
        duplicatedBarCodeError: element(by.cssContainingText('.ng-binding', 'Barcode must be unique')),
        editProductSuccessMsg: element(by.cssContainingText('.ng-binding', 'Edited product successfully')),

        // image
        ImageUpload: element.all(by.css('input[type="file"]')),

        // save button
        saveDraftBtn: element(by.css('[ng-click="saveProduct(validationForm, true)"]')),
        publishBtn: $('[ng-click="saveProduct(validationForm)"]'),
        saveBtn: element(by.css('[ng-show="!saveMode"]'))

    },

    // set product information
    setPrductNameEnglish: function (val) {
        testCommon.helpers.setField(this.elements.productNameEnTf, val);
    },

    setProductNameTraditionChinese: function (val) {
        testCommon.helpers.setField(this.elements.productNameTcTf, val);
    },

    setProductNameSimpliifiedChinese: function (val) {
        testCommon.helpers.setField(this.elements.productNameScTf, val);
    },

    selectCountry: function (val) {
        testCommon.helpers.setSelect(this.elements.countrySelect, 'number:' + val);
    },

    setManufacturerName: function (val) {
        testCommon.helpers.setField(this.elements.manufacturerNameTf, val);
    },

    selectYearLunched: function (val) {
        testCommon.helpers.setSelect(this.elements.yearLaunchedSelect, 'number:' + val);
    },

    selectSeasonLunched: function (val) {
        testCommon.helpers.setSelect(this.elements.seasonLunchedSelect, 'number:' + val);
    },

    setStyleCode: function (val) {
        testCommon.helpers.setField(this.elements.styleCodeTf, val);
    },

    setRetailPrice: function (val) {
        testCommon.helpers.setField(this.elements.retailPriceTf, val);
    },

    selectPrimaryMainCategory: function (mainCategory) {
        this.elements.primaryCategoryBtn.click();

        var categories = element.all(by.repeater('node in data')).each(function (elem) {
            elem.getText().then(function (text) {
                if (text == mainCategory) {
                    elem.click();
                    return;
                }
            });
        });

        this.elements.saveCategoryBtn.click();
    },

    selectPrimaryChildCategory: function (mainCategory, childCategory) {
        this.elements.primaryCategoryBtn.click();
        var categories = element.all(by.repeater('node in data')).each(function (elem) {
            elem.getText().then(function (text) {
                if (text == mainCategory) {
                    elem.element(by.css('[ng-click="node.expanded = !node.expanded;"]')).click();
                    var subCategories = element.all(by.repeater('node in node.CategoryList')).each(function (subElem) {
                        subElem.getText().then(function (subText) {
                            if (subText == childCategory) {
                                subElem.click();
                            }
                        });
                    });
                }
            });
        });

        this.elements.saveCategoryBtn.click();
    },

    // set shipping information
    setLength: function (val) {
        testCommon.helpers.setField(this.elements.lengthTf, val);
    },

    setWidth: function (val) {
        testCommon.helpers.setField(this.elements.widthTf, val);
    },

    setHeight: function (val) {
        testCommon.helpers.setField(this.elements.heightTf, val);
    },

    setWeight: function (val) {
        testCommon.helpers.setField(this.elements.weightTf, val);
    },

    // set product description
    setDescription: function (val) {
        testCommon.helpers.setField(this.elements.descriptionTf, val);
    },

    setFeature: function (val) {
        testCommon.helpers.setField(this.elements.featureTf, val);
    },

    setProductMaterial: function (val) {
        testCommon.helpers.setField(this.elements.productMaterialTf, val);
    },

    setSizeComment: function (val) {
        testCommon.helpers.setField(this.elements.sizeCommentTf, val);
    },

    // product variations
    clickCreateSku: function () {
        this.elements.createSkubtn.click();
    },

    checkNoColorAndSize: function () {
        this.elements.noColorSizeCb.click();
    },

    selectRedColor: function (colorName) {
        browser.waitForAngular(this.elements.colorCb.get(0).isDisplayed());
        this.elements.colorCb.get(0).click();
        browser.waitForAngular(this.elements.redTf.isDisplayed());
        testCommon.helpers.setField(this.elements.redTf.isDisplayed(), colorName);

    },

    selectPinkColor: function (colorName) {
        browser.waitForAngular(this.elements.colorCb.get(1).isDisplayed());
        this.elements.colorCb.get(1).click();
        browser.waitForAngular(this.elements.pinkTf.isDisplayed());
        testCommon.helpers.setField(this.elements.pinkTf.isDisplayed(), colorName);

    },


    clickGenerateSku: function () {
        this.elements.skuGenerateBtn.click();
    },

    setSkuCode: function (index, val) {
        testCommon.helpers.setField(this.elements.skuCodeTf.get(index), val);
    },

    setBarCode: function (index, val) {
        testCommon.helpers.setField(this.elements.barCodeTf.get(index), val);
    },

    setThreshold: function (index, val) {
        testCommon.helpers.setField(this.elements.safetyThresholdTf.get(index), val);
    },

    // click create and publish
    clickCreateAndPublish: function () {
        this.elements.publishBtn.click();
    },

    clickSaveDraft: function () {
        this.elements.saveDraftBtn.click();
    },

    clickSaveBtn: function () {
        this.elements.saveBtn.click();
    },

    // error message
    checkNoProductName: function () {
        expect(this.elements.productNameRequiredError.isDisplayed()).toBeTruthy();
    },

    checkNoStyleCodeMessage: function () {
        expect(this.elements.styleCodeRequiredError.isDisplayed()).toBeTruthy();
    },

    checkNoRetailPriceMessage: function () {
        expect(this.elements.retailPriceRequiredError.isDisplayed()).toBeTruthy();
    },

    checkNoCategoryMessage: function () {
        expect(this.elements.categoryRequiredError.isDisplayed()).toBeTruthy();
    },

    checkNoSkuMessage: function () {
        expect(this.elements.skuRequiredError.isDisplayed()).toBeTruthy();
    },

    checkNoCountryMessage: function () {
        expect(this.elements.countryRequiredError.isDisplayed()).toBeTruthy();
    },

    checkDuplicatedSkuCodeMessage: function () {
        expect(this.elements.duplicatedSkuCodeError.isDisplayed()).toBeTruthy();
    },

    checkDuplicatedBarcodeMessage: function () {
        expect(this.elements.duplicatedBarCodeError.isDisplayed()).toBeTruthy();
    },

    checkEditProductSuccessMessage: function () {
        expect(this.elements.editProductSuccessMsg.isDisplayed()).toBeTruthy();
    },

    uploadFeaturedImage: function () {
        testCommon.helpers.setUpload(this.elements.ImageUpload.get(0), 'images/logo.png');
    },

    uploadDescriptionImages: function () {
        testCommon.helpers.setUpload(this.elements.ImageUpload.get(1), 'images/logo.png');
    },

    uploadColorImages: function () {
        testCommon.helpers.setUpload(this.elements.ImageUpload.get(2), 'images/logo.png');
    },

    assertProdcutNameSimplifiedChinese: function (productName) {
        expect(this.elements.productNameScTf.getAttribute('value')).toEqual(productName);

    },

    assertStyleCode: function (styleCode) {
        expect(this.elements.styleCodeTf.getAttribute('value')).toBe(styleCode);

    },

    assertSkuCode: function (index, skuCode) {
        expect(this.elements.skuCodeTf.get(index).getAttribute('value')).toBe(skuCode);
    },

    assertBarCode: function (index, barCode) {
        expect(this.elements.barCodeTf.get(index).getAttribute('value')).toBe(barCode);
    }

}





