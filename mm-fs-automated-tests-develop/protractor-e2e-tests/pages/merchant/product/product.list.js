'use strict';
var testCommon = require('../../../test.common.js');

var _ = require('lodash');

module.exports = {
  elements: {


    createProductBtn: element(by.id("btnCreateNewProduct")),
    productSearchTf: element(by.model('productSearchTerm')),
    productSearchBtn: element(by.id("btnSearch")),
    missingImgTypeSelect: element(by.model("missingImgType")),
    categorySelect: element(by.model('categoryId')),
    statusSelect: element(by.model('statusId')),
    itemPerPageSelect: element(by.model('itemsPerPage')),
    productList: element.all(by.repeater("style in styles")),
    firstPageBtn: element(by.css('[ng-click="firstPage()"]')),
    lastPageBtn: element(by.css('[ng-click="lastPage()"]')),
    previousPageBtn: element(by.css('[ng-click="prevPage()"]')),
    nextPageBtn: element(by.css('[ng-click="nextPage()"]')),
    pageBtn: element.all(by.css('[ng-repeat="n in pageRanges"]'))


  },

  browse: function() {
    if (!testCommon.isMerchant) {
      browser.get('/admin/#/merchant/13/product/list/');
    } else browser.get('/merchant/#/product/list');

  },

  clickCreateProduct: function(){
    this.elements.createProductBtn.click();
  },

  setSearch: function(keyWords) {
    testCommon.helpers.setField(this.elements.productSearchTf, keyWords);
    this.elements.productSearchBtn.click();
  },

  clearSearch: function() {
    this.elements.productSearchTf.clear();
    this.elements.productSearchBtn.click();
  },

  setMissingImageType: function(missingImgTypeID) {
    testCommon.helpers.setSelect(this.elements.missingImgTypeSelect, missingImgTypeID);
  },

  setCategory: function(cagetoryId) {
    testCommon.helpers.setSelect(this.elements.categorySelect, cagetoryId);
  },

  setStatus: function(statusId) {
    testCommon.helpers.setSelect(this.elements.statusSelect, statusId);
  },

  setPageSize: function (val) {
    testCommon.helpers.setSelect(this.elements.itemPerPageSelect, 'number:' + val);
  },

  shouldHaveOnlyProductNameLike: function(keyWords) {
    this.elements.productList.then(function(elements){
      _.each(elements, function(elem){
        var productName = elem.$$('td').get(1).getText();
        expect(productName).toContain(keyWords);
      })
    });
  },

  selectProduct: function (productName) {
    this.elements.productList.then(function(elements){
      _.each(elements, function(elem){
            var productName = elem.$$('td').get(1).getText();
        if (productName==productName){
          browser.waitForAngular(elem.isDisplayed());

          elem.click();
        }
      })
    });
  },

  shouldFilterByCategoryResultCorrect: function(categoryId, categoryName) {
    this.elements.productList.then(function(elements){
      _.each(elements, function(elem){
        var tmpcategoryName = elem.$$('td').get(2).getText();
        expect(tmpcategoryName).toMatch(categoryName);
      })
    });
  },

  shouldFilterByStatusResultCorrect: function(statusId, statusName) {
    this.elements.productList.then(function(elements){
      _.each(elements, function(elem){
        var tmpstatusName = elem.$$('td').get(6).getText();
        expect(tmpstatusName).toMatch(statusName);
      })
    });
  },


  shouldPageSizeCorrect: function(pageSize){
    expect(this.elements.productList.count()).toBe(pageSize);
  },

  shouldHaveItemsOf: function(val) {
    expect(this.elements.productList.count()).toBeLessThan(val + 1);
  },

  clickNextpage: function() {
    var productName = this.elements.productList.$$('td').get(2).getText();
    this.elements.nextPageBtn.click();
    expect(this.elements.productList.$$('td').get(2).getText()).not.toEqual(productName);
  },

  clickFirstPage: function () {
    this.elements.firstPageBtn.click();
    expect(element(by.css('[ng-class="prevPageDisabled()"]')).getAttribute('class')).toContain('disabled');
  },

  clickPreviousPage: function () {
    var productName = this.elements.productList.$$('td').get(2).getText();
    this.elements.previousPageBtn.click();
    expect(this.elements.productList.$$('td').get(2).getText()).not.toEqual(productName);
  },

  clickLastPage: function () {
    this.elements.lastPageBtn.click();
    expect(element(by.css('[ng-class="nextPageDisabled()"]')).getAttribute('class')).toContain('disabled');
  }


};
