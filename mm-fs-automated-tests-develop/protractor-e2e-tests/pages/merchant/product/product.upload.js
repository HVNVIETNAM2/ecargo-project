var path = require('path');

var testCommon = require('../../../test.common.js');
module.exports = {
    elements: {
        uploadBtn: element(by.css('input[type="file"]')),
        ignoreRbtn: element(by.model('form.existsAction')),
        overwriteRbtn: element(by.model('form.existsAction')),
        successUploadList: element.all(by.repeater('file in successFiles')),
        failUploadList: element.all(by.repeater('file in failedFiles')),
        imageThumb: $$('.thumb')

    },


    browse: function () {
        if (!testCommon.isMerchant) {
            browser.get('/admin/#/merchant/13/product/upload');
        }
        else browser.get('/merchant/#/product/upload');
    },

    clickIgnore: function () {
        this.elements.ignoreRbtn.click();
    },

    clickOverwrite: function () {
        this.elements.overwriteRbtn.click();
    },

    uploadInvalid: function () {
        testCommon.helpers.setUpload(this.elements.uploadBtn, 'images/logo.png');
    },

    uploadValid: function () {
        testCommon.helpers.setUpload(this.elements.uploadBtn, 'images/16324_1.jpg');
        browser.waitForAngular($$('.thumb'));
    },

    uploadUnrelated: function () {
        testCommon.helpers.setUpload(this.elements.uploadBtn, 'images/products_valid.xlsx');
    },

    shouldShowThumb: function (count) {
        expect(this.elements.imageThumb.count()).toBe(count);
    },

    shouldShowSuccess: function (count) {
        expect(
            this.elements.successUploadList.count()
        ).toBe(count);
    },

    shouldShowFailed: function (count) {
        expect(
            this.elements.failUploadList.count()
        ).toBe(count);
    },

    assertProductName: function (productName){
        expect(this.elements.successUploadList.$$('td').get(0).getText()).toEqual(productName);

    },

    assertStyleCode: function(styleCode) {
        expect(this.elements.successUploadList.$$('td').get(1).getText()).toEqual(styleCode);
    }

}