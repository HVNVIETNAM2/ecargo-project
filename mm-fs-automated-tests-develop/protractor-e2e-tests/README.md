# mm-automated-tests

#########Setup#########

1. install nodejs in your local working environment
2. install grunt command line, type “sudo npm install -g grunt-cli” in your terminal
3. install java jdk 7 or later
4. install chrome and firefox browser
5. start your MM nodejs server in vagrant
6. open a terminal and switch directory to the folder “mm-e2e-tests"
7. install required node modules for the protractor e2e tests: “sudo npm install”

#########Start Testing#########
You may run each test case individually by
1. switch to any directory inside test-cases, e.g. admin inside "test-cases"
2. type "grunt" in the terminal

You may run all the test cases at once by
1. switch to directory "test-cases"
2. type “grunt” in the terminal